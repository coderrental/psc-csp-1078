﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainView.ascx.cs" Inherits="QuantoreMigration.MainView" %>
<%@ Register Assembly="Telerik.Web.UI, Version=2012.2.703.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4"
	Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxLoadingPanel ID="ajaxLoadingPanel" runat="server"/>
<telerik:RadAjaxManager runat="server" ID="ajaxManager" DefaultLoadingPanelID="ajaxLoadingPanel">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="grInfo">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grInfo" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>
<div id="cr_maincontainer">
	<div id="cr_uploadform" class="cr_uploadform" runat="server">
		<p>Please upload excel file for create users automatically</p>
		<telerik:RadUpload ID="uploadfile" ControlObjectsVisibility="None" runat="server" AllowedFileExtensions="xls" MaxFileInputsCount="1" OverwriteExistingFiles="True"></telerik:RadUpload>
		<telerik:RadButton ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_OnClick">
			<Icon PrimaryIconCssClass="rbUpload" PrimaryIconTop="4" PrimaryIconLeft="4"/>
		</telerik:RadButton>
		<asp:CustomValidator ID="validExtension" runat="server" Display="Dynamic" ClientValidationFunction="validateUpload">
			<span class="invalidLabel">Invalid extensions.</span>
		</asp:CustomValidator>
	</div>
	<div id="cr_gridInfo" runat="server" class="cr_gridInfo">
		<telerik:RadGrid AutoGenerateColumns="False" ID="grInfo" runat="server" 
			AllowPaging="True" PageSize="20" CellSpacing="0" GridLines="None">
			<MasterTableView CommandItemDisplay="Top">
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

<RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column"></RowIndicatorColumn>

<ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column"></ExpandCollapseColumn>

				<Columns>
					<telerik:GridBoundColumn DataField="CompanyId" 
						FilterControlAltText="Filter CompanyId column" HeaderText="CompanyID" 
						UniqueName="CompanyId">
					</telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="UserName" 
						FilterControlAltText="Filter UserName column" HeaderText="Username" 
						UniqueName="UserName">
					</telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="Email" 
						FilterControlAltText="Filter Email column" HeaderText="Email" 
						UniqueName="Email">
					</telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="Password" 
						FilterControlAltText="Filter Password column" HeaderText="Password" 
						UniqueName="Password">
					</telerik:GridBoundColumn>
					<telerik:GridCheckBoxColumn DataField="ReadyToInsert" DataType="System.Boolean" 
						FilterControlAltText="Filter ReadyToInsert column" HeaderText="Ready To Insert" 
						UniqueName="ReadyToInsert">
					</telerik:GridCheckBoxColumn>
				</Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
<CommandItemSettings ShowExportToExcelButton="true" />
			</MasterTableView>
			<PagerStyle Mode="Slider"></PagerStyle>

<FilterMenu EnableImageSprites="False"></FilterMenu>

		</telerik:RadGrid>
		<p id="crAction" class="crAction">
			<telerik:RadButton ID="btnAdd" runat="server" AutoPostBack="False" Text="Add users">
				<Icon PrimaryIconCssClass="rbNext" PrimaryIconLeft="4" PrimaryIconTop="4"/>
			</telerik:RadButton>
		</p>
	</div>
	<div id="cr_proccessbar">
		<pre>Inserting....</pre>
		<p class="msgHolder"></p>
	</div>
</div>
<telerik:RadScriptBlock runat="server">
	<script type="text/javascript">
		$(function () {
			function proccess() {
				$.post(document.URL, { action: "create" }, function (data) {
					if (data != "done") {
						$("#cr_proccessbar pre").append(data + "<br />");
						proccess();
					}
					else if (data == "done") {
						$('.msgHolder').html("Users have been added successfuly!").fadeIn();
						return;
					}
				});
			}
			$('#<%= btnAdd.ClientID %>').click(function () {
				$('.cr_gridInfo').slideUp();
				$('#cr_proccessbar').slideDown();
				proccess();
			});
		});

		function validateUpload(source, arguments) {
			arguments.IsValid = $find('<%= uploadfile.ClientID %>').validateExtensions();
		}
	</script>
</telerik:RadScriptBlock>