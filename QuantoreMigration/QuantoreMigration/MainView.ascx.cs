﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.OleDb;
using CR.DnnModules.Common;
using DotNetNuke.Entities.Profile;
using QuantoreMigration.Common;
using QuantoreMigration.Entities;
using QuantoreMigration.Data;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using Telerik.Web.UI;

namespace QuantoreMigration
{
	public partial class MainView : DnnModuleBase
	{
		private string _fullPathofFile;
		private List<UserToInsert> _userListToInsert;
		private QuantoreDataContext _dataContext;
		DateTime now = DateTime.Now;
		/// <summary>
		/// Handles the Init event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected override void Page_Init(object sender, EventArgs e)
		{
			now = DateTime.Now;
			base.Page_Init(sender, e);

			_userListToInsert = new List<UserToInsert>();
			_dataContext = new QuantoreDataContext(Utils.GetConnectionString(PortalId));

			if (!string.IsNullOrEmpty(Request.Params["action"]) && Request.Params["action"] == "create")
			{
				Proccess();
				Response.StatusCode = 200;
				Response.Flush();
				Response.End();
			}

			grInfo.Visible = false;
			cr_gridInfo.Visible = false;

			if (IsPostBack)
			{
				if (DataCache.GetCache(Cons.LISTUSERCACHENAME) != null)
				{
					grInfo.Visible = true;
					grInfo.DataSource = (List<UserToInsert>)DataCache.GetCache(Cons.LISTUSERCACHENAME);
					grInfo.DataBind();
					
				}
			}
		}

		/// <summary>
		/// Proccesses this instance.
		/// </summary>
		private void Proccess()
		{
			Response.Clear();
			if (DataCache.GetCache(Cons.LISTUSERCACHENAME) == null)
			{
				Response.Write("Error. Please upload excel file and try again");
			}
			else
			{
				try
				{
					
					TimeSpan ts;

					var m_List = (List<UserToInsert>)DataCache.GetCache(Cons.LISTUSERCACHENAME);
					if (m_List.Any())
					{
						//WriteToResponse(DateTime.Now.Subtract(now), "Process " + m_List[0].CompanyId);
						Response.Write(string.Format("Processing: {0}, {1}, {2}, {3}", m_List[0].CompanyId, m_List[0].OgCompId, m_List[0].HpCompId, m_List[0].OxCompId));
						if (m_List[0].ReadyToInsert)
						{
							//Insert ...
							var m_ObjUser = new DotNetNuke.Entities.Users.UserInfo();
							m_ObjUser.AffiliateID = Null.NullInteger;
							m_ObjUser.Email = m_List[0].Email;
							m_ObjUser.IsSuperUser = false;
							m_ObjUser.PortalID = PortalId;
							m_ObjUser.Username = m_List[0].UserName;
							m_ObjUser.DisplayName = m_List[0].UserName;
							m_ObjUser.Membership.Password = m_List[0].Password;
							m_ObjUser.Membership.Approved = true;
							m_ObjUser.Membership.UpdatePassword = true;

							var m_ObjCreateStatus = DotNetNuke.Entities.Users.UserController.CreateUser(ref m_ObjUser);
							if (m_ObjCreateStatus == DotNetNuke.Security.Membership.UserCreateStatus.Success)
							{
								//Update profile cspid
								m_ObjUser.Profile.SetProfileProperty("CspId", m_List[0].CompanyId.ToString());
								ProfileController.UpdateUserProfile(m_ObjUser);
								//everything fine
								Response.Write(string.Format("Insert user {0} successfully at {1}", m_List[0].UserName, DateTime.Now));
							}
							else
							{
								//show error
								Response.Write(string.Format("Error when creating user: {0}. Error message: {1}", m_List[0].UserName, m_ObjCreateStatus.ToString()));
							}
						}

						//WriteToResponse(DateTime.Now.Subtract(now), "Inserted");
						var user = DotNetNuke.Entities.Users.UserController.GetUserByName(PortalId, m_List[0].UserName);
						if (user != null && user.Email != m_List[0].Email)
						{
							Response.Write(string.Format("<p>Update User Email From {0} to {1}</p>", user.Email, m_List[0].Email));
							user.Email = m_List[0].Email;
							DotNetNuke.Entities.Users.UserController.UpdateUser(PortalId, user);
						}

						//Merge consumer
						Response.Write(MergeConsumer(m_List[0]));

						//WriteToResponse(DateTime.Now.Subtract(now), "Merge Consumer");

						//write contact info to the consumer parameter
						Response.Write(WriteContactInfo(m_List[0]));

						//WriteToResponse(DateTime.Now.Subtract(now), "Write Contact");

						//Merge consumer parameters
						Response.Write(MergeConsumerParameter(m_List[0]));

						//WriteToResponse(DateTime.Now.Subtract(now), "Merge Parameters");

						Response.Write(string.Format("<p><b>{0}</b></p>",m_List.Count));

						m_List.RemoveAt(0);

						DataCache.SetCache(Cons.LISTUSERCACHENAME, m_List);
					}
					else
					{
						Response.Write("done");
					}	
				}
				catch (Exception m_Exception)
				{

					Response.Write("Error: " + m_Exception);
				}
				
			}
			//WriteToResponse(DateTime.Now.Subtract(now), "End Of Process");
		}

		private void WriteToResponse(TimeSpan ts, string text)
		{
			Response.Write(string.Format("<p>{0}:{1}:{2}{3}</p>", ts.Minutes, ts.Seconds, ts.Milliseconds, text.PadLeft(10)));
		}

		/// <summary>
		/// Merges the consumer.
		/// </summary>
		/// <param name="userToInsert">The user to insert.</param>
		/// <returns>Response msg</returns>
		private string MergeConsumer(UserToInsert userToInsert)
		{
			var m_ReturnStr = string.Empty;
			var m_Comid = userToInsert.CompanyId;
			var m_OgComId = userToInsert.OgCompId;
			var m_HpComId = userToInsert.HpCompId;
			var m_OxComId = userToInsert.OxCompId;
			
			if (m_HpComId > 0 && m_HpComId != m_Comid)
			{
				List<companies_consumer> m_CompaniesConsumer = _dataContext.companies_consumers.Where(a => a.companies_Id == m_HpComId).ToList();
				if (m_CompaniesConsumer.Any())
				{
					foreach (var m_Consumer in m_CompaniesConsumer)
					{
						m_Consumer.companies_Id = m_Comid;
						m_ReturnStr += string.Format("<br />-----------Update consumer id: {0} with companyId: {1} to new companyId: {2}", m_Consumer.companies_Id, m_HpComId, m_Comid);
						try
						{
							_dataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
						}
						catch (Exception m_Exception)
						{

							m_ReturnStr = "Error when try to merge consumer's companyId: " + m_Exception.Message;
						}
					}
				}
			}

			if (m_OgComId > 0 && m_OgComId != m_Comid)
			{
				List<companies_consumer> m_CompaniesConsumer = _dataContext.companies_consumers.Where(a => a.companies_Id == m_OgComId).ToList();
				if (m_CompaniesConsumer.Any())
				{
					foreach (var m_Consumer in m_CompaniesConsumer)
					{
						m_Consumer.companies_Id = m_Comid;
						m_ReturnStr += string.Format("<br />-----------Update consumer id: {0} with companyId: {1} to new companyId: {2}", m_Consumer.companies_Id, m_OgComId, m_Comid);
						try
						{
							_dataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
						}
						catch (Exception m_Exception)
						{

							m_ReturnStr = "Error when try to merge consumer's companyId: " + m_Exception.Message;
						}
					}
					
				}
			}

			if (m_OxComId > 0 && m_OxComId != m_Comid)
			{
				List<companies_consumer> m_CompaniesConsumer = _dataContext.companies_consumers.Where(a => a.companies_Id == m_OxComId).ToList();
				if (m_CompaniesConsumer.Any())
				{
					foreach (var m_Consumer in m_CompaniesConsumer)
					{
						m_Consumer.companies_Id = m_Comid;
						m_ReturnStr += string.Format("<br />-----------Update consumer id: {0} with companyId: {1} to new companyId: {2}", m_Consumer.companies_Id, m_OxComId, m_Comid);	
					}
					try
					{
						_dataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
					}
					catch (Exception m_Exception)
					{

						m_ReturnStr = "Error when try to merge consumer's companyId: " + m_Exception.Message;
					}
					
				}
			}
			
			return m_ReturnStr;
		}

		/// <summary>
		/// Writes the contact info.
		/// </summary>
		/// <param name="userToInsert">The user to insert.</param>
		/// <returns>Response msg</returns>
		private string WriteContactInfo(UserToInsert userToInsert)
		{
			string m_ReturnStr = string.Empty;
			var m_Comid = userToInsert.CompanyId;
			var m_CompaniesContacts = _dataContext.companies_contacts.Where(a => a.companies_Id == m_Comid).ToList();
			if (m_CompaniesContacts.Any())
			{
				var m_CompaniesContact = m_CompaniesContacts[0]; //Get first contact information
				var m_ListParameterToInsert = new List<companies_parameter>();
				//Write firstname
				var m_CompaniesParameterTypeFirstname = _dataContext.companies_parameter_types.SingleOrDefault(a => a.parametername == Cons.PERSONALIZE_CONTACT_FIRSTNAME);
				if (m_CompaniesParameterTypeFirstname != null)
				{
					var m_CompaniesFirstName = _dataContext.companies_parameters.SingleOrDefault(a => a.companies_Id == m_Comid && a.companies_parameter_types_Id == m_CompaniesParameterTypeFirstname.companies_parameter_types_Id);
					if (m_CompaniesFirstName != null) //if exist contact info, try update
					{
						m_CompaniesFirstName.value_text = m_CompaniesContact.firstname;
					}
					else
					{
						m_ListParameterToInsert.Add(new companies_parameter
						{
							companies_Id = m_Comid,
							value_text = m_CompaniesContact.firstname,
							companies_parameter_types_Id = m_CompaniesParameterTypeFirstname.companies_parameter_types_Id

						});	
					}
					
				}
				
				//Write lastname
				var m_CompaniesParameterTypeLastname = _dataContext.companies_parameter_types.SingleOrDefault(a => a.parametername == Cons.PERSONALIZE_CONTACT_LASTNAME);
				if (m_CompaniesParameterTypeLastname != null)
				{
					var m_CompaniesLastname = _dataContext.companies_parameters.SingleOrDefault(a =>a.companies_Id == m_Comid && a.companies_parameter_types_Id == m_CompaniesParameterTypeLastname.companies_parameter_types_Id);
					if (m_CompaniesLastname != null) //if exist contact info, try update
					{
						m_CompaniesLastname.value_text = m_CompaniesContact.lastname;
					}
					else //else, create new contact info and insert
					{
						m_ListParameterToInsert.Add(new companies_parameter
						{
							companies_Id = m_Comid,
							value_text = m_CompaniesContact.lastname,
							companies_parameter_types_Id = m_CompaniesParameterTypeLastname.companies_parameter_types_Id

						});
					}
					
				}


				//Write emailaddress
				var m_CompaniesParameterTypeEmail = _dataContext.companies_parameter_types.SingleOrDefault(a => a.parametername == Cons.PERSONALIZE_CONTACT_EMAILADDRESS);
				if (m_CompaniesParameterTypeEmail != null)
				{
					var m_CompaniesEmail = _dataContext.companies_parameters.SingleOrDefault(a => a.companies_Id == m_Comid && a.companies_parameter_types_Id == m_CompaniesParameterTypeEmail.companies_parameter_types_Id);
					if (m_CompaniesEmail != null) //if exist contact info, try update
					{
						m_CompaniesEmail.value_text = m_CompaniesContact.emailaddress;
					}
					else
					{
						m_ListParameterToInsert.Add(new companies_parameter
						{
							companies_Id = m_Comid,
							value_text = m_CompaniesContact.emailaddress,
							companies_parameter_types_Id = m_CompaniesParameterTypeEmail.companies_parameter_types_Id

						});
					}
				}

				//Write phone number
				var m_CompaniesParameterTypePhone = _dataContext.companies_parameter_types.SingleOrDefault(a => a.parametername == Cons.PERSONALIZE_CONTACT_PHONENUMBER);
				if (m_CompaniesParameterTypePhone != null)
				{
					var m_CompaniesPhone = _dataContext.companies_parameters.SingleOrDefault(a => a.companies_Id == m_Comid && a.companies_parameter_types_Id == m_CompaniesParameterTypePhone.companies_parameter_types_Id);
					if (m_CompaniesPhone != null) //if exist contact info, try update
					{
						m_CompaniesPhone.value_text = m_CompaniesContact.telephone;
					}
					else
					{
						m_ListParameterToInsert.Add(new companies_parameter
						{
							companies_Id = m_Comid,
							value_text = m_CompaniesContact.telephone,
							companies_parameter_types_Id = m_CompaniesParameterTypePhone.companies_parameter_types_Id

						});
					}
					
				}

				try
				{
					if (m_ListParameterToInsert.Any())
					{
						_dataContext.companies_parameters.InsertAllOnSubmit(m_ListParameterToInsert);

					}
					_dataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
					m_ReturnStr += "<br />-----------Write contact info to the consumer parameter successfully!<br/>";
				}
				catch (Exception m_Exception)
				{
					m_ReturnStr = string.Format("Error when try to write contact info to the consumer parameter: {0}<br/>", m_Exception.Message);
				}
					
			}
			return m_ReturnStr;
		}

		/// <summary>
		/// Merges the consumer parameter.
		/// </summary>
		/// <param name="userToInsert">The user to insert.</param>
		/// <returns></returns>
		private string MergeConsumerParameter(UserToInsert userToInsert)
		{
			string m_ReturnStr = string.Empty;
			var m_Comid = userToInsert.CompanyId;
			var m_OgComId = userToInsert.OgCompId;
			var m_HpComId = userToInsert.HpCompId;
			var m_OxComId = userToInsert.OxCompId;

			var m_MainComParams = _dataContext.companies_parameters.Where(a => a.companies_Id == m_Comid).ToList();
			var m_OgComParams = _dataContext.companies_parameters.Where(a => a.companies_Id == m_OgComId).ToList();
			var m_HpComParams = _dataContext.companies_parameters.Where(a => a.companies_Id == m_HpComId).ToList();
			var m_OxComParams = _dataContext.companies_parameters.Where(a => a.companies_Id == m_OxComId).ToList();

			bool m_IsMerge = false;
			
			if (m_OgComParams.Any())
			{
				foreach (var m_OgComParam in m_OgComParams)
				{
					bool m_IsMatch = m_MainComParams.Any(mainComParam => m_OgComParam.companies_parameter_type.companies_parameter_types_Id == mainComParam.companies_parameter_type.companies_parameter_types_Id);
					if (!m_IsMatch)
					{
						var m_ParamToInsert = new companies_parameter
						                      	{
						                      		companies_Id = m_Comid,
						                      		companies_parameter_types_Id = m_OgComParam.companies_parameter_types_Id,
						                      		value_text = m_OgComParam.value_text
						                      	};
						_dataContext.companies_parameters.InsertOnSubmit(m_ParamToInsert);
						m_IsMerge = true;
					}

				}
			}

			if (m_HpComParams.Any())
			{
				foreach (var m_HpComParam in m_HpComParams)
				{
					bool m_IsMatch = m_MainComParams.Any(mainComParam => m_HpComParam.companies_parameter_type.companies_parameter_types_Id == mainComParam.companies_parameter_type.companies_parameter_types_Id);
					if (!m_IsMatch)
					{
						var m_ParamToInsert = new companies_parameter
						{
							companies_Id = m_Comid,
							companies_parameter_types_Id = m_HpComParam.companies_parameter_types_Id,
							value_text = m_HpComParam.value_text
						};
						_dataContext.companies_parameters.InsertOnSubmit(m_ParamToInsert);
						m_IsMerge = true;
					}

				}
			}

			if (m_OxComParams.Any())
			{
				foreach (var m_OxComParam in m_OxComParams)
				{
					bool m_IsMatch = m_MainComParams.Any(mainComParam => m_OxComParam.companies_parameter_type.companies_parameter_types_Id == mainComParam.companies_parameter_type.companies_parameter_types_Id);
					if (!m_IsMatch)
					{
						var m_ParamToInsert = new companies_parameter
						{
							companies_Id = m_Comid,
							companies_parameter_types_Id = m_OxComParam.companies_parameter_types_Id,
							value_text = m_OxComParam.value_text
						};
						_dataContext.companies_parameters.InsertOnSubmit(m_ParamToInsert);
						m_IsMerge = true;
					}

				}
			}

			try
			{
				if (m_IsMerge)
				{
					_dataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
					m_ReturnStr += "<br />-----------Merge consumer parameters successfully!<br/>";
				}
				
			}
			catch (Exception m_Exception)
			{
				m_ReturnStr = string.Format("Error when try to Merge consumer parameters: {0}<br/>", m_Exception.Message);
			}
			return m_ReturnStr;
		}

		/// <summary>
		/// Saves the file.
		/// </summary>
		private void SaveFile()
		{
			if (uploadfile.UploadedFiles.Count > 0)
			{
				UploadedFile m_ValidFile  = uploadfile.UploadedFiles[0];
				string m_TargetPath = Server.MapPath(ControlPath + Cons.UPLOADDIR);
				if (!Directory.Exists(m_TargetPath)) // If folder does not exist, create it!
					Directory.CreateDirectory(m_TargetPath);
				try
				{
					m_ValidFile.SaveAs(Path.Combine(m_TargetPath, m_ValidFile.GetName()), true);
					_fullPathofFile = Path.Combine(m_TargetPath, m_ValidFile.GetName());
				}
				catch (Exception)
				{
					// do nothing here
				}
			}
		}

		/// <summary>
		/// Reads the file.
		/// </summary>
		private void ReadFile()
		{
			var m_Connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties=Excel 8.0;Data Source=" + _fullPathofFile);

			string m_ExcelQuery = @"Select * FROM [Sheet1$]";
			m_Connection.Open();
			var m_Cmd = new OleDbCommand(m_ExcelQuery, m_Connection);
			var m_Reader = m_Cmd.ExecuteReader();
			if (m_Reader != null)
			{
				while (m_Reader.Read())
				{
					var m_IsExist = false;
					var m_ObjUserInfo = DotNetNuke.Entities.Users.UserController.GetUserByName(PortalId, m_Reader["username portal"].ToString());
					if (m_ObjUserInfo != null) // does not exist
						m_IsExist = true;
					var m_User = new UserToInsert
					             	{
										CompanyId = int.Parse(m_Reader["CompanyID"].ToString()),
										UserName = m_Reader["username portal"].ToString(),
										Password = m_Reader["password"].ToString(),
										Email = m_Reader["email portal"].ToString(),
										ReadyToInsert = !m_IsExist
					             	};

					//Get companies ID
					int m_HpCompId, m_OxCompId, m_OgCompId;
					int.TryParse(m_Reader["OG csp compID"].ToString(), out m_OgCompId);
					int.TryParse(m_Reader["HP csp compID"].ToString(), out m_HpCompId);
					int.TryParse(m_Reader["OX csp compID"].ToString(), out m_OxCompId);

					m_User.HpCompId = m_HpCompId;
					m_User.OgCompId = m_OgCompId;
					m_User.OxCompId = m_OxCompId;

					_userListToInsert.Add(m_User);
				}
			}
			
			m_Connection.Close();

			if (_userListToInsert.Any())
			{
				foreach (var m_UserToInsert in _userListToInsert)
				{
					//Get Email from contact table
					UserToInsert m_Insert = m_UserToInsert;
					var m_CompaniesContact = _dataContext.companies_contacts.Where(a => a.companies_Id == m_Insert.CompanyId).ToList();
					if (m_CompaniesContact.Any() && !string.IsNullOrEmpty(m_CompaniesContact[0].emailaddress))
						m_UserToInsert.Email = m_CompaniesContact[0].emailaddress;
				}

				//Cache the _userList (set time catch to 15m)
				DataCache.SetCache(Cons.LISTUSERCACHENAME, _userListToInsert, new TimeSpan(0, 30, 0));
				grInfo.DataSource = _userListToInsert;
				grInfo.DataBind();

				// save the list to a file
				string m_TargetPath = Server.MapPath(ControlPath + Cons.UPLOADDIR);
				using (StreamWriter sw = new StreamWriter(m_TargetPath+"\\"+DateTime.Now.Ticks+".csv",false))
				{
					sw.AutoFlush = true;
					sw.WriteLine("UserName,Email,Password,CompanyId");
					foreach (var u in _userListToInsert)
					{
						sw.WriteLine(string.Format("{0},{1},{2},{3}", u.UserName, u.Email, u.Password, u.CompanyId));
					}
					sw.Close();
				}
			}
		}

		#region [ Event Handlers ]
		/// <summary>
		/// Handles the OnClick event of the btnSubmit control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void btnSubmit_OnClick(object sender, EventArgs e)
		{
			grInfo.Visible = true;
			cr_gridInfo.Visible = true;
			//Clear cache
			DataCache.ClearCache(Cons.LISTUSERCACHENAME);
			//Save file
			SaveFile();

			if (_fullPathofFile != string.Empty)
			{
				//Read excel file
				ReadFile();
			}

			//Proccess
		}
		#endregion
	}
}