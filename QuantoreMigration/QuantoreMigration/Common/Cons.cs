﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuantoreMigration.Common
{
	public class Cons
	{
		/// <summary>
		/// Name of upload directory
		/// </summary>
		public const string UPLOADDIR = "upload";

		public const string LISTUSERCACHENAME = "UserList";

		public const string PERSONALIZE_CONTACT_FIRSTNAME = "Personalize_Contact_Firstname";

		public const string PERSONALIZE_CONTACT_LASTNAME = "Personalize_Contact_Lastname";

		public const string PERSONALIZE_CONTACT_EMAILADDRESS = "Personalize_Contact_Emailaddress";

		public const string PERSONALIZE_CONTACT_PHONENUMBER = "Personalize_Contact_Phonenumber";
	}
}