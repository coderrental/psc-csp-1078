﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuantoreMigration.Entities
{
	/// <summary>
	/// User info to insert
	/// </summary>
	public class UserToInsert
	{
		/// <summary>
		/// Gets or sets the company ID.
		/// </summary>
		/// <value>
		/// The company ID.
		/// </value>
		public int CompanyId { get; set; }

		/// <summary>
		/// Gets or sets the OG comp ID.
		/// </summary>
		/// <value>
		/// The OG comp ID.
		/// </value>
		public int OgCompId { get; set; }

		/// <summary>
		/// Gets or sets the hp comp id.
		/// </summary>
		/// <value>
		/// The hp comp id.
		/// </value>
		public int HpCompId { get; set; }

		/// <summary>
		/// Gets or sets the ox comp id.
		/// </summary>
		/// <value>
		/// The ox comp id.
		/// </value>
		public int OxCompId { get; set; }

		/// <summary>
		/// Gets or sets the name of the user.
		/// </summary>
		/// <value>
		/// The name of the user.
		/// </value>
		public string UserName { get; set; }

		/// <summary>
		/// Gets or sets the email.
		/// </summary>
		/// <value>
		/// The email.
		/// </value>
		public string Email { get; set; }

		/// <summary>
		/// Gets or sets the password.
		/// </summary>
		/// <value>
		/// The password.
		/// </value>
		public string Password { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [ready to insert].
		/// </summary>
		/// <value>
		///   <c>true</c> if [ready to insert]; otherwise, <c>false</c>.
		/// </value>
		public bool ReadyToInsert { get; set; }
	}
}