﻿<%@ Control Language="C#" AutoEventWireup="false" Explicit="true" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="CONTROLPANEL" Src="~/Admin/Skins/controlpanel.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="NAV" Src="~/Admin/Skins/Nav.ascx" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="DotNetNuke.Common" %>
<%@ Import Namespace="DotNetNuke.Services.Localization" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.DDRMenu.TemplateEngine" Assembly="DotNetNuke.Web.DDRMenu" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<div id="wrapper">
	<div id="header-section">
		<div id="header-content">
			<div id="icons-links">
				<!-- Left icons -->
				<div id="header-icons">
					<div id="available-lang-list" class="hide">
						<dnn:LANGUAGE runat="server" ID="MDMLanguage" ShowLinks="True" ShowMenu="False"/>
						<dnn:LANGUAGE runat="server" ID="dnnLANGUAGE" ShowLinks="False" ShowMenu="True"/>
					</div>
					<ul>
						<li id="lang-list-trigger" class="misc-item">
							<a id="misc-global" href="javascript: void(0);"></a>
							<div id="fake-lang-list-outer">
								<ul id="fake-lang-list">
							
								</ul>
							</div>
						</li>
						<li class="misc-item">
							<a id="misc-home" href="javascript: void(0);"></a>
						</li>
						<li class="misc-item">
							<a id="misc-help" href="javascript: void(0);"></a>
						</li>
					</ul>
				</div>
				<!-- EOF Left icons -->
					
				<!-- Logo & User links -->
				<div id="header-logos-links">
					<div id="the-logo">
						<a id="logo-link" href="<%="http://" + PortalSettings.PortalAlias.HTTPAlias%>">
							<!-- Need to use ControlPath when deploy -->
							<img src="<%=SkinPath %>Images/tie-logo.png" />
						</a>
					</div>
					<div id="user-links">
						<dnn:USER ID="dnnUser" runat="server" class="usr-label"/> <span id="misc_user_sep">|</span> <dnn:LOGIN ID="dnnLogin" runat="server"/>
					</div>
				</div>
				<!-- EOF Logo & User links -->
				<div class="clear"></div>
			</div>
				
			<!-- Main menu -->
			<div id="tabmodule-menu">
				<dnn:MENU ID="dnnMenu" ProviderName="EfforityMenuNavigationProvider" MenuStyle="SelfServiceStandard" runat="server" />
			</div>
			<!-- EOF Main menu -->
		</div>
	</div>
	<div id="body-section">
		<div id="body-content">
			<div runat="server" id="ContentPane"></div>	
		</div>
	</div>
</div>
<script type="text/javascript">
	WebFontConfig = {
		google: { families: ['Exo:400,400italic,500,500italic,600,600italic,700,700italic,300italic,300:latin'] }
	};
	(function () {
		var wf = document.createElement('script');
		wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
		wf.type = 'text/javascript';
		wf.async = 'true';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(wf, s);
	})();
</script>