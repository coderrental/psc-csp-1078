<%@ Control Language="C#" AutoEventWireup="false" Explicit="true" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="CONTROLPANEL" Src="~/Admin/Skins/controlpanel.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="NAV" Src="~/Admin/Skins/Nav.ascx" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="DotNetNuke.Common" %>
<%@ Import Namespace="DotNetNuke.Services.Localization" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.DDRMenu.TemplateEngine" Assembly="DotNetNuke.Web.DDRMenu" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<dnn:DnnJsInclude runat="server" FilePath="Js/themefunctions.js" PathNameAlias="SkinPath" />
<div id="wrapper">
<div id="left-shadow">
<div id="right-shadow">
	<!-- Header section -->
	<div id="header">
		<div id="the-logo">
			<a id="logo-link" href="<%="http://" + PortalSettings.PortalAlias.HTTPAlias%>">
				<img src="<%=SkinPath %>Images/logo.png" />
			</a>
		</div>
		<div id="misc-left" class="float-left">
			<div id="available-lang-list" class="hide">
				<dnn:LANGUAGE runat="server" ID="MDMLanguage" ShowLinks="True" ShowMenu="False"/>
				<dnn:LANGUAGE runat="server" ID="dnnLANGUAGE" ShowLinks="False" ShowMenu="True"/>
			</div>
			<ul>
				<li class="misc-item hide" id="lang-list-trigger" style="display: none !important">
					<a class="misc_global" href="javascript: void(0)"></a>
					<div id="fake-lang-list-outer">
						<ul id="fake-lang-list">
							
						</ul>
					</div>
				</li>
				<li class="misc-item">
					<a class="misc_home" href="<%="http://" + PortalSettings.PortalAlias.HTTPAlias%>"></a>
				</li>
				<li class="misc-item">
					<a class="misc_help" href="<%="http://" + PortalSettings.PortalAlias.HTTPAlias%>/ContactSupport.aspx"></a>
				</li>
				<div class="clear"></div>
			</ul>
		</div>
		<div id="misc_user"><dnn:USER ID="dnnUser" runat="server" class="usr-label"/> <span id="misc_user_sep">|</span> <dnn:LOGIN ID="dnnLogin" runat="server"/></div>
		<div class="clear"></div>
	</div>
	<!-- EOF Header section -->
		
	<!-- Main Nav Section -->
	<div id="main-nav">
		<div id="main-nav-right">
			<div id="main-nav-mid">
				<dnn:MENU ID="dnnMenu" ProviderName="EfforityMenuNavigationProvider" MenuStyle="SvedexStandard" runat="server" />
			</div>
		</div>
	</div>
	<!-- EOF Main Nav Section -->
		
	<div id="main-content">
		<div runat="server" id="ContentPane"></div>	
	</div>
		
	<div id="footer">
		<div id="bottom-nav">
		</div>
		<div id="footer-info">
			<div id="powered-by-logo">
				<img src="<%=SkinPath %>Images/powered-by-tie-kinetix.jpg" height="26px" />
			</div>
				
			<div id="copyright-section">
				<div id="terms-privacy-section">
					<ul>
						<li>
							<a id="privacy-link" href="http://contentsyndication.tiekinetix.com/node/64"><%= Localization.GetString("lbPrivacyPolicy.Text", Localization.GlobalResourceFile)%></a>
						</li>
						<li>
							|
						</li>
						<li>
							<a id="terms-link" href="http://global.syndication.tiekinetix.net/ui/subscription/TIE/tc.htm"><%= Localization.GetString("lbTermsofUse.Text", Localization.GlobalResourceFile)%></a>
						</li>
						<div class="clear"></div>
					</ul>
				</div>
				<div id="copyright-text">
					<%= !String.IsNullOrEmpty(Localization.GetString("lbCopyright.Text", Localization.GlobalResourceFile)) ? Localization.GetString("lbCopyright.Text", Localization.GlobalResourceFile).Replace("{year}", DateTime.Now.Year.ToString()) : ""%>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>
</div>
<div id="bottom-shadow">
	<div id="bottom-right-shadow">
		<div id="bottom-mid-shadow">
			
		</div>
	</div>
</div>
</div>
<div id="txtPolicy-text" class="hide"><%= Localization.GetString("AcceptAgreement.Text", Localization.GlobalResourceFile)%></div>
<div id="kendoWnd" style="overflow:hidden;"></div>
<dnn:DnnCssInclude ID="DnnCssInclude1" runat="server" FilePath="Css/kendo.common.min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude ID="DnnCssInclude2" runat="server" FilePath="Css/kendo.metro.min.css" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude ID="DnnJsInclude2" runat="server" FilePath="Js/kendo.web.min.js" PathNameAlias="SkinPath"/>
<script type="text/javascript">
	var CheckboxIssueInterval;
	$(function () {
	    var w = $("#kendoWnd").kendoWindow({ modal: true, visible: false, close: function () { $("#kendoWnd").html(""); } }).data("kendoWindow");
	    $("#privacy-link,#terms-link,a.kwnd").click(function (ev) {
	        ev.preventDefault();
	        var el = $(this);
	        w.refresh({ url: el.attr("href") });
	        w.title(el.text());
	        w.wrapper.css({ width: 970, height: 600 });
	        w.center();
	        w.open();
	        return false;
	    });

	    InitMenu();

	    CheckboxIssueInterval = setInterval(function () { DetectAgreementCheckboxIssue(); }, 1000);

	    ValidateAcceptAgreement();

	    DisableUserAcceptAgreement();
	});

	$(window).load(function() {
		InitPolicyBox();
	});

    function DisableUserAcceptAgreement() {
        if ($("a[href$='_ManageUsers_dnnProfileDetails']").length == 0)
            return;
        $("[id$='_ManageUsers_Profile_ProfileProperties_TermsAndConditions']").remove();
        $("[id$='_ManageUsers_Profile_ProfileProperties_AcceptAgreement']").remove();
    }
	
	function InitMenu() {
		$('ul.topLevel > li').hover(function() {
			if ($(this).hasClass('haschild')) {
				$(this).find('div.subLevel').show();
			}
		}, function() {
			if ($(this).hasClass('haschild')) {
				$(this).find('div.subLevel').hide();
			}
		});

		if ($('#main-nav li.selected').parents('div.subLevel').length == 1) {
			$('li.selected').parents('li').addClass('selected');
		}
	}

	function InitPolicyBox() {
		if ($('.dnnRegistrationForm').length == 1 || $('#dnnManageUsers').length == 1) {
			var thePolicyTextarea = $('textarea.dnnFormRequired');
			if (thePolicyTextarea.length > 0) {
				thePolicyTextarea.val($('#txtPolicy-text').html());
				thePolicyTextarea.attr({ readonly: 'readonly' });
			}
		} else {
            $('#txtPolicy-text').remove();
		}
	}

	function InitLanguageBox() {
		$('#available-lang-list select').find('option').each(function () {
			var newLi = $('<li>');
			var srcValue = 'javascript: void(0)'; // $(this).find('a').attr('src');
			var selectedClass = '';
			var langText = $(this).html();
			var langCode = $(this).val();
			var theLangSymbol = $('#available-lang-list').find('img[alt="' + langCode + '"]');
			if (theLangSymbol.length == 1 && theLangSymbol.parents('a').length == 1)
				srcValue = theLangSymbol.parents('a').attr('href');
			else {
				selectedClass = 'class="MDMLang-Selected"';
			}

			var newA = $('<a ' + selectedClass + ' href="' + srcValue + '"><span>' + langText + '</span></a>');
			newLi.append(newA);
			$('#fake-lang-list').append(newLi);
		});
		$('#fake-lang-list').append($('<div class="clear">'));
		
		$('#lang-list-trigger').hover(function () {
			if ($('#fake-lang-list-outer').find('li').length == 0)
				return;
			$('#fake-lang-list-outer').stop().show();
		}, function () {
			$('#fake-lang-list-outer').stop().hide();
		});
	}
	
	function DetectAgreementCheckboxIssue() {
		if ($('.dnnRegistrationForm').length == 0) {
			clearInterval(CheckboxIssueInterval);
			return;
		}

		$('input[type="checkbox"]').each(function () {
			var parentNode = $(this).closest('div');
			if ($(this).is(":checked"))
				parentNode.find('span.dnnFormError').hide();
		});
	}
	
	function ValidateAcceptAgreement() {
		if ($('.dnnRegistrationForm').length == 0) {
			return;
		}
		$('a.dnnPrimaryAction').click(function () {
			$('input[type="checkbox"]').each(function () {
				var parentNode = $(this).closest('div');
				if ($(this).is(":checked") == false) {
					parentNode.find('span.dnnFormError').show();
				}
			});
		});
		$('a.dnnPrimaryAction').mouseover(function () {
			$('input[type="checkbox"]').each(function () {
				var parentNode = $(this).closest('div');
				if ($(this).is(":checked") == false) {
					parentNode.find('span.dnnFormError').show();
				}
			});
		});
	}
</script>