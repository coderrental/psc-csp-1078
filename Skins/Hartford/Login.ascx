﻿<%@ Control Language="C#" AutoEventWireup="false" Explicit="true" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="CONTROLPANEL" Src="~/Admin/Skins/controlpanel.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="DotNetNuke.Services.Localization" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.DDRMenu.TemplateEngine" Assembly="DotNetNuke.Web.DDRMenu" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<dnn:DnnJsInclude runat="server" FilePath="Js/themefunctions.js" PathNameAlias="SkinPath" />
<div id="wrapper">
	<div id="login-page">
		<!-- Left Pane -->
		<div id="left-pane">
			<div id="leftPaneContent" runat="server"></div>
		</div>
		<!-- EOF Left Pane -->
			
		<!-- Right Pane -->
		<div id="right-pane">
			<div id="available-lang-list" class="hide">
				<dnn:LANGUAGE runat="server" ID="MDMLanguage" ShowLinks="True" ShowMenu="False"/>
				<dnn:LANGUAGE runat="server" ID="dnnLANGUAGE" ShowLinks="False" ShowMenu="True"/>
			</div>
			<div id="misc-left">
				<ul id="misc-ul">
					<li class="misc-item" id="lang-list-trigger" style="display: none !important">
						<a class="misc_global" href="javascript: void(0)"></a>
						<div id="fake-lang-list-outer">
							<ul id="fake-lang-list">
							
							</ul>
						</div>
					</li>
					<li class="misc-item">
						<a class="misc_home" href="<%="http://" + PortalSettings.PortalAlias.HTTPAlias%>"></a>
					</li>
					<li class="misc-item">
						<a class="misc_help" href="<%="http://" + PortalSettings.PortalAlias.HTTPAlias%>/ContactSupport.aspx"></a>
					</li>
					<div class="clear"></div>
				</ul>
			</div>
			<div id="rightPaneContent" runat="server">

			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div id="footer">
		
		<div id="footer-info">
			<div id="powered-by-logo">
				<img src="<%=SkinPath %>Images/powered-by-tie-kinetix.jpg" height="26px" />
			</div>
				
			<div id="copyright-section">
				<div id="terms-privacy-section">
					<ul>
						<li>
							<a id="privacy-link" href="http://contentsyndication.tiekinetix.com/node/64"><%= Localization.GetString("lbPrivacyPolicy.Text", Localization.GlobalResourceFile)%></a>
						</li>
						<li>
							|
						</li>
						<li>
							<a id="terms-link" href="http://thehartford.tiekinetix.com/interface/portals/3/TermsOfUse.htm"><%= Localization.GetString("lbTermsofUse.Text", Localization.GlobalResourceFile)%></a>
						</li>
						<div class="clear"></div>
					</ul>
				</div>
				<div id="copyright-text">
					<%= !String.IsNullOrEmpty(Localization.GetString("lbCopyright.Text", Localization.GlobalResourceFile)) ? Localization.GetString("lbCopyright.Text", Localization.GlobalResourceFile).Replace("{year}", DateTime.Now.Year.ToString()) : ""%>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>
<div id="txtPolicy-text" class="hide"><%= Localization.GetString("AcceptAgreement.Text", Localization.GlobalResourceFile)%></div>
<div id="kendoWnd" style="overflow:hidden;"></div>
<dnn:DnnCssInclude ID="DnnCssInclude1" runat="server" FilePath="Css/kendo.common.min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude ID="DnnCssInclude2" runat="server" FilePath="Css/kendo.metro.min.css" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude ID="DnnJsInclude2" runat="server" FilePath="Js/kendo.web.min.js" PathNameAlias="SkinPath"/>
<script type="text/javascript">
	var loginedDetector;
	var ie7Detector;

	$(function () {
		var w = $("#kendoWnd").kendoWindow({ modal: true, visible: false, close: function () { $("#kendoWnd").html(""); } }).data("kendoWindow");
		$("#privacy-link,#terms-link,a.kwnd").click(function (ev) {
			ev.preventDefault();
			var el = $(this);
			w.refresh({ url: el.attr("href") });
			w.title(el.text());
			w.wrapper.css({ width: 970, height: 600 });
			w.center();
			w.open();
			return false;
		});

		//InitLanguageBox();
		LoginedDetect();
		loginedDetector = setInterval(function () { LoginedDetect(); }, 1000);

		if ($.browser.msie && parseInt($.browser.version, 10) === 7)
			ie7Detector = setInterval(function () { ReAlignLoginInput(); }, 1000);

		AlreadyLoginedDetect();
	});

	function InitLanguageBox() {
		$('#available-lang-list select').find('option').each(function () {
			var newLi = $('<li>');
			var srcValue = 'javascript: void(0)'; // $(this).find('a').attr('src');
			var selectedClass = '';
			var langText = $(this).html();
			var langCode = $(this).val();
			var theLangSymbol = $('#available-lang-list').find('img[alt="' + langCode + '"]');
			if (theLangSymbol.length == 1 && theLangSymbol.parents('a').length == 1)
				srcValue = theLangSymbol.parents('a').attr('href');
			else {
				selectedClass = 'class="MDMLang-Selected"';
			}

			var newA = $('<a ' + selectedClass + ' href="' + srcValue + '"><span>' + langText + '</span></a>');
			newLi.append(newA);
			$('#fake-lang-list').append(newLi);
		});
		$('#fake-lang-list').append($('<div class="clear">'));
		$('#lang-list-trigger').hover(function () {
			if ($('#fake-lang-list-outer').find('li').length == 0)
				return;
			$('#fake-lang-list-outer').stop().show();
		}, function () {
			$('#fake-lang-list-outer').stop().hide();
		});
	}

	function addRegSep() {
		var newSpan = $('<span>').addClass('reg-sep').html('|');
		$('ul.dnnActions').find('li:first').after(newSpan);
	}

	var logoImg = '';
	function LoginLogo() {
		logoImg = $('#dnn_dnnLogo_imgLogo').attr('src');
		$('#dnn_dnnLogo_imgLogo').attr('src', $('#white-logo').attr('src'));
		$('#white-logo').error(function () {
			$('#dnn_dnnLogo_imgLogo').attr('src', logoImg);
		});
	}
	
	function LoginedDetect() {
		if ($('div.dnnProfile').length == 1) {
			InitPolicyBox();
			clearInterval(loginedDetector);
		}
	}

	function InitPolicyBox() {
		if ($('div.dnnProfile').length == 0)
			return;
		var thePolicyTextarea = $('textarea[class="dnnFormRequired"]');
		if (thePolicyTextarea.length == 0)
			return;
		thePolicyTextarea.val($('#txtPolicy-text').html());
		thePolicyTextarea.attr({ readonly: 'readonly' });
	}

	function ReAlignLoginInput() {
		if ($('input.ie7inspected').length > 0)
			return;
		$('.dnnLoginService div.dnnFormItem input').addClass('ie7inspected');
		$('.dnnLoginService div.dnnFormItem input').css({ 'padding-top': '3px', 'height': '20px' });
		$('.dnnLoginService input.dnnPrimaryAction').css({ 'padding': '5px 20px'});
	}
	
	function AlreadyLoginedDetect() {
		var redirectURL = $('a.misc_home').attr('href');
		if ($('input[type="password"]').length == 0 && $('.dnnProfile').length == 0) {
			window.location = redirectURL;
		}
	}
</script>