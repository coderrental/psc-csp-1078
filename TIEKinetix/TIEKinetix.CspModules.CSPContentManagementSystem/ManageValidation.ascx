﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageValidation.ascx.cs" Inherits="TIEKinetix.CspModules.CSPContentManagementSystem.ManageValidation" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI, Version=2012.2.703.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<%@ Register assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.WebControls" tagprefix="asp" %>
<telerik:RadAjaxLoadingPanel ID="ajaxLoadingPanel" runat="server" IsSticky="True" Transparency="50" CssClass="ajax-loading-panel"/>
<telerik:RadWindowManager ID="windowManager" runat="server"/>
<telerik:RadAjaxManager runat="server" ID="ajaxManager" DefaultLoadingPanelID="ajaxLoadingPanel">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="rgValidate">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="rgValidate" />
                <telerik:AjaxUpdatedControl ControlID="windowManager" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>
<!-- Toolbar -->
<telerik:RadToolBar ID="toptoolbar" runat="server" Width="100%" CssClass="property-toolbar"/>
<!-- /End toolbar -->
<telerik:RadGrid ID="rgValidate" DataSourceID="lqsValidate" 
	AutoGenerateColumns="False" runat="server" AllowAutomaticDeletes="True" 
	AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AllowPaging="True" 
	OnItemInserted="rgValidate_OnItemInserted"
	OnItemDeleted="rgValidate_OnItemDeleted"
	OnItemUpdated="rgValidate_OnItemUpdated"
	PageSize="20" CellSpacing="0" GridLines="None">
	<MasterTableView EditMode="InPlace" CommandItemDisplay="Top" DataKeyNames="CSPCMS_Validation_Id">
		<CommandItemSettings ShowAddNewRecordButton="True" ShowRefreshButton="True" />

<RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column"></RowIndicatorColumn>

<ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column"></ExpandCollapseColumn>
		<Columns>
			<telerik:GridBoundColumn DataField="CSPCMS_Validation_Id" UniqueName="CSPCMS_Validation_Id" HeaderText="ID" ReadOnly="true" ForceExtractValue="Always" />
			<telerik:GridBoundColumn DataField="ValidationName" UniqueName="ValidationName" HeaderText="Validation Name" />
			<telerik:GridBoundColumn DataField="ValidationRule" UniqueName="ValidationRule" HeaderText="Validation Rule" />
			<telerik:GridDateTimeColumn DataField="DateCreated" UniqueName="DateCreated" HeaderText="Date Created" ReadOnly="True" />
			<telerik:GridDateTimeColumn DataField="DateChanged" UniqueName="DateChanged" HeaderText="Date Changed" ReadOnly="True" />
			<telerik:GridEditCommandColumn ButtonType="ImageButton" />
			<telerik:GridButtonColumn ConfirmText='Delete this Validation?' ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" />                  
		</Columns>
		<EditFormSettings>
			<EditColumn ButtonType="ImageButton" />
		</EditFormSettings>
		<PagerStyle Mode="Slider"/>
	</MasterTableView>

<FilterMenu EnableImageSprites="False"></FilterMenu>
</telerik:RadGrid>
<asp:LinqDataSource ID="lqsValidate" runat="server" OnContextCreating="lqsValidate_OnContextCreating"
	ContextTypeName="TIEKinetix.CspModules.Data.DnnDataContext" EnableDelete="True" 
	EnableInsert="True" EnableUpdate="True" TableName="CSPCMS_Validations" OnUpdating="lqsValidate_OnUpdating" OnInserting="lqsValidate_OnInserting">
</asp:LinqDataSource>

