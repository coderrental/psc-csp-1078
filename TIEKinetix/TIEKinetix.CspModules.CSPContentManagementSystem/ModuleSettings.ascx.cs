﻿using System;
using DotNetNuke.Entities.Modules;
using TIEKinetix.CspModules.CSPContentManagementSystem.Components;

namespace TIEKinetix.CspModules.CSPContentManagementSystem
{
	public partial class ModuleSettings : ModuleSettingsBase
	{
		public override void LoadSettings()
		{
			if (TabModuleSettings[Const.UPLOAD_ROOT_DIRECTORY] != null)
				tbUploadRootDir.Text = TabModuleSettings[Const.UPLOAD_ROOT_DIRECTORY].ToString();
			if (TabModuleSettings[Const.DOMAIN_NAME] != null)
				tbDomainName.Text = TabModuleSettings[Const.DOMAIN_NAME].ToString();
			if (TabModuleSettings[Const.IGNORE_CATEGORY_TEXT] != null)
				tbIgnoreCategoryText.Text = TabModuleSettings[Const.IGNORE_CATEGORY_TEXT].ToString();
            if (TabModuleSettings[Const.SETTING_CATEGORYCONTENT_CT_ID] != null)
                tbCategoryContentCTId.Text = TabModuleSettings[Const.SETTING_CATEGORYCONTENT_CT_ID].ToString();
            if (TabModuleSettings[Const.UPLOAD_COBRANDED_DIRECTORY] != null)
                tbUploadCobrandedDir.Text = TabModuleSettings[Const.UPLOAD_COBRANDED_DIRECTORY].ToString();
            if (TabModuleSettings[Const.INSTANCE_NAME] != null)
                tbInstanceName.Text = TabModuleSettings[Const.INSTANCE_NAME].ToString();
            if (TabModuleSettings[Const.SETTING_DEFAULT_LANGUAGE_ID] != null)
                tbxDefaultLanguageId.Text = TabModuleSettings[Const.SETTING_DEFAULT_LANGUAGE_ID].ToString();
		}

		public override void UpdateSettings()
		{
			ModuleController objModules = new ModuleController();
			objModules.UpdateTabModuleSetting(TabModuleId, Const.UPLOAD_ROOT_DIRECTORY, tbUploadRootDir.Text);
			objModules.UpdateTabModuleSetting(TabModuleId, Const.DOMAIN_NAME, tbDomainName.Text);
			objModules.UpdateTabModuleSetting(TabModuleId, Const.IGNORE_CATEGORY_TEXT, tbIgnoreCategoryText.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, Const.SETTING_CATEGORYCONTENT_CT_ID, tbCategoryContentCTId.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, Const.UPLOAD_COBRANDED_DIRECTORY, tbUploadCobrandedDir.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, Const.INSTANCE_NAME, tbInstanceName.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, Const.SETTING_DEFAULT_LANGUAGE_ID, tbxDefaultLanguageId.Text);
			//refresh cache
			ModuleController.SynchronizeModule(ModuleId);
		}
	}
}