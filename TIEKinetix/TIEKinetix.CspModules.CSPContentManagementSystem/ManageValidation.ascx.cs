﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Data;
using Telerik.Web.UI;
using TIEKinetix.CspModules.CSPContentManagementSystem.Components;
using TIEKinetix.CspModules.CSPContentManagementSystem.Components.Custom;
using TIEKinetix.CspModules.Data;

namespace TIEKinetix.CspModules.CSPContentManagementSystem
{
	public partial class ManageValidation : DnnModuleBase
	{
		/// <summary>
		/// Handles the Init event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected override void Page_Init(object sender, EventArgs e)
		{
			base.Page_Init(sender, e);

			//Init toolbar
			InitToolbar();
		}

		#region [ Init Components ]
		/// <summary>
		/// Inits the toolbar.
		/// </summary>
		private void InitToolbar()
		{
			toptoolbar.Items.Add(new RadToolBarButton
			{
				Text = GetLocalizedText("Label.BackToContentType"),
				PostBack = false,
				ImageUrl = ControlPath + "images/arrow_left.png",
				NavigateUrl = Globals.NavigateURL(TabId, ""),
				ToolTip = GetLocalizedText("Label.BackToContentType")
			});
		}
		#endregion

		#region [ Private Methods ]
		/// <summary>
		/// Shows a <see cref="RadWindow"/> alert if an error occurs
		/// </summary>
		private void ShowErrorMessage(string msg)
		{
			ajaxManager.ResponseScripts.Add(string.IsNullOrEmpty(msg)
												? string.Format("window.radalert(\"Please enter valid data!\")")
												: string.Format("window.radalert(\"{0}\")", msg));
		}
		/// <summary>
		/// Shows the error message.
		/// </summary>
		private void ShowErrorMessage()
		{
			ShowErrorMessage(string.Empty);
		}

		#endregion

		#region
		/// <summary>
		/// Handles the OnItemInserted event of the rgValidate control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Telerik.Web.UI.GridInsertedEventArgs"/> instance containing the event data.</param>
		protected void rgValidate_OnItemInserted(object sender, GridInsertedEventArgs e)
		{
			if (e.Exception != null)
			{
				e.ExceptionHandled = true;
				ShowErrorMessage();
			}
		}

		/// <summary>
		/// Handles the OnItemDeleted event of the rgValidate control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Telerik.Web.UI.GridDeletedEventArgs"/> instance containing the event data.</param>
		protected void rgValidate_OnItemDeleted(object sender, GridDeletedEventArgs e)
		{
			if (e.Exception != null)
			{
				e.ExceptionHandled = true;
				ShowErrorMessage();
			}
		}

		/// <summary>
		/// Handles the OnItemUpdated event of the rgValidate control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Telerik.Web.UI.GridUpdatedEventArgs"/> instance containing the event data.</param>
		protected void rgValidate_OnItemUpdated(object sender, GridUpdatedEventArgs e)
		{
			if (e.Exception != null)
			{
				e.ExceptionHandled = true;
				ShowErrorMessage();
			}
		}


		/// <summary>
		/// Handles the OnUpdating event of the lqsValidate control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.LinqDataSourceUpdateEventArgs"/> instance containing the event data.</param>
		protected void lqsValidate_OnUpdating(object sender, LinqDataSourceUpdateEventArgs e)
		{
			var m_NewObject = (CSPCMS_Validation)e.NewObject;
			m_NewObject.DateChanged = DateTime.Now;
		}

		/// <summary>
		/// Handles the OnInserting event of the lqsValidate control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.LinqDataSourceInsertEventArgs"/> instance containing the event data.</param>
		protected void lqsValidate_OnInserting(object sender, LinqDataSourceInsertEventArgs e)
		{
			var m_NewObject = (CSPCMS_Validation)e.NewObject;
			m_NewObject.CSPCMS_Validation_Id = Guid.NewGuid();

			m_NewObject.DateCreated = DateTime.Now;
			m_NewObject.DateChanged = DateTime.Now;
		}
		#endregion

		/// <summary>
		/// Handles the OnContextCreating event of the lqsValidate control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.LinqDataSourceContextEventArgs"/> instance containing the event data.</param>
		protected void lqsValidate_OnContextCreating(object sender, LinqDataSourceContextEventArgs e)
		{
			e.ObjectInstance = new DnnDataContext(Config.GetConnectionString());
		}
	}
}