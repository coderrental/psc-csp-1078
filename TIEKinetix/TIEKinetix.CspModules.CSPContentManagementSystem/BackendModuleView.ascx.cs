﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Security.Roles;
using TIEKinetix.CspModules.CSPContentManagementSystem.Components;
using TIEKinetix.CspModules.Data;

namespace TIEKinetix.CspModules.CSPContentManagementSystem
{
    /// <summary>
    /// todo: back end / allow admin to select suppliers and categories for the given content type per users
    /// </summary>
	public partial class BackendModuleView : DnnModuleBase
    {
        protected override void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);

            //Register jquery color animate plugin
            Page.ClientScript.RegisterClientScriptInclude("jcoloranimate",ControlPath + "js/jcoloranimated.js");

            #region Check and create roles if not exist
            RoleController roleController = new RoleController();

            RoleInfo roleInfo = roleController.GetRoleByName(PortalId, ModuleRole.ViewOnlyRoleName);
            if(roleInfo == null)
            {
                roleController.AddRole(new RoleInfo
                                           {
                                               AutoAssignment = false,
                                               IsPublic = false,
                                               PortalID = PortalId,
                                               ServiceFee = 0.00f,
                                               BillingFrequency = "N",
                                               BillingPeriod = -1,
                                               Description = "Users of this role can view and search content only (no edit).",
                                               TrialFee = 0.00f,
                                               TrialFrequency = "N",
                                               TrialPeriod = -1,
                                               RoleName = ModuleRole.ViewOnlyRoleName,
                                               RoleGroupID = -1,
                                               Status = RoleStatus.Approved
                                           });
            }
            roleInfo = roleController.GetRoleByName(PortalId, ModuleRole.ViewEditRoleName);
            if (roleInfo == null)
            {
                roleController.AddRole(new RoleInfo
                {
                    AutoAssignment = false,
                    IsPublic = false,
                    PortalID = PortalId,
                    ServiceFee = 0.00f,
                    BillingFrequency = "N",
                    BillingPeriod = -1,
                    Description = "Users of this role can view and edit content.",
                    TrialFee = 0.00f,
                    TrialFrequency = "N",
                    TrialPeriod = -1,
                    RoleName = ModuleRole.ViewEditRoleName,
                    RoleGroupID = -1,
                    Status = RoleStatus.Approved
                });
            }
            roleInfo = roleController.GetRoleByName(PortalId, ModuleRole.EditContentRoleName);
            if (roleInfo == null)
            {
                roleController.AddRole(new RoleInfo
                {
                    AutoAssignment = false,
                    IsPublic = false,
                    PortalID = PortalId,
                    ServiceFee = 0.00f,
                    BillingFrequency = "N",
                    BillingPeriod = -1,
                    Description = "Users of this role can see edit content button.",
                    TrialFee = 0.00f,
                    TrialFrequency = "N",
                    TrialPeriod = -1,
                    RoleName = ModuleRole.EditContentRoleName,
                    RoleGroupID = -1,
                    Status = RoleStatus.Approved
                });
            }
            roleInfo = roleController.GetRoleByName(PortalId, ModuleRole.EditContentExceptionRoleName);
            if (roleInfo == null)
            {
                roleController.AddRole(new RoleInfo
                {
                    AutoAssignment = false,
                    IsPublic = false,
                    PortalID = PortalId,
                    ServiceFee = 0.00f,
                    BillingFrequency = "N",
                    BillingPeriod = -1,
                    Description = "Users of this role can see edit content exception button.",
                    TrialFee = 0.00f,
                    TrialFrequency = "N",
                    TrialPeriod = -1,
                    RoleName = ModuleRole.EditContentExceptionRoleName,
                    RoleGroupID = -1,
                    Status = RoleStatus.Approved
                });
            }
            roleInfo = roleController.GetRoleByName(PortalId, ModuleRole.EditAllContentConsumerExceptionRoleName);
            if (roleInfo == null)
            {
                roleController.AddRole(new RoleInfo
                {
                    AutoAssignment = false,
                    IsPublic = false,
                    PortalID = PortalId,
                    ServiceFee = 0.00f,
                    BillingFrequency = "N",
                    BillingPeriod = -1,
                    Description = "Users of this role can edit all content exception for all consumers (without this role, users can only edit their own content exception).",
                    TrialFee = 0.00f,
                    TrialFrequency = "N",
                    TrialPeriod = -1,
                    RoleName = ModuleRole.EditAllContentConsumerExceptionRoleName,
                    RoleGroupID = -1,
                    Status = RoleStatus.Approved
                });
            }
            #endregion
            if (string.IsNullOrEmpty(Request.QueryString.Get("retry")))
            {
                DnnDataContext _dnnContext = new DnnDataContext(Config.GetConnectionString());
                CSPCMS_ContentType[] contentTypes = _dnnContext.CSPCMS_ContentTypes.Where(a => a.PortalId == PortalId && a.TabModuleId == TabModuleId).ToArray();

                // return active content type if available
                if (contentTypes.Count(a => a.Active) > 0)
                {
                    Response.Redirect(GetUrl(contentTypes.First(a => a.Active).ContentTypeId), true);
                }
                // return the first one
                else if (contentTypes.Length > 0)
                {
                    Response.Redirect(GetUrl(contentTypes[0].ContentTypeId), true);
                }
            }

			listView.DataSource = CspDataContext.content_types.Where(c => !c.description.Contains("PlaceHolder")).ToArray();
        }

        /// <summary>
        /// get redirect url
        /// </summary>
        /// <param name="id">content type id</param>
        /// <returns>url</returns>
        protected string GetUrl(object id)
        {
            return Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Configure", "mid", ModuleId.ToString(), "id", id.ToString());
        }
    }
}