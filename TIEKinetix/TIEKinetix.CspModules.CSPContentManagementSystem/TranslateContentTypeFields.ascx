﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TranslateContentTypeFields.ascx.cs" Inherits="TIEKinetix.CspModules.CSPContentManagementSystem.TranslateContentTypeFields" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadToolBar ID="toolBar" runat="server" CssClass="property-toolbar" OnButtonClick="ToolBarButtonClick">
    <Items></Items>
</telerik:RadToolBar>
<div class="content-field-group">
    <asp:Panel runat="server" ID="TranslatePanel">
    </asp:Panel>
</div>