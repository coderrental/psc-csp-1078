﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using DotNetNuke.Common.Utilities;
using TIEKinetix.CspModules.Data;

namespace TIEKinetix.CspModules.CSPContentManagementSystem.Components
{
    /// <summary>
    /// Common Class: contains all common function calls and query string
    /// </summary>
    public class Common
    {
        private static TripleDESCryptoServiceProvider _tripleDes = null;
        public static string PublishStage = "Publish";
        public static string UnPublishStage = "UnPublish";
        public static string CreateTablesSql = @"";
        public static string GetContentTypeConfigurationSql = @"";
        public static string GetRelationTreeQuery = @"
;with RelationTree (id, pid, cid, lvl) as
(
	select a.CSPCMS_ContentTypeRelation_Id, a.CSPCMS_ContentType_Parent_Id, a.CSPCMS_ContentType_Child_Id, 0
	from CSPCMS_ContentTypeRelation a
	where a.CSPCMS_ContentType_Parent_Id={0}
	
	union all
	
	select b.CSPCMS_ContentTypeRelation_Id, b.CSPCMS_ContentType_Parent_Id, b.CSPCMS_ContentType_Child_Id, rt.lvl+1
	from CSPCMS_ContentTypeRelation b
		join RelationTree rt on b.CSPCMS_ContentType_Parent_Id = rt.cid
)
select a.*
from RelationTree rt
	join CSPCMS_ContentTypeRelation a on a.CSPCMS_ContentTypeRelation_Id = rt.id
order by rt.lvl desc
";
        public static string CurrentContentMainId = "cmId";
        private static byte[] TripleDesKey = new byte[] {125, 235, 186, 152, 140, 99, 40, 51, 37, 223, 154, 46, 69, 98, 70, 188, 31, 135, 132, 225, 226, 1, 120, 55};
        private static byte[] TripleDesVector = new byte[] {73, 164, 87, 186, 60, 97, 164, 79};

        public static string GetConnectionString(string name)
        {
            return Config.GetConnectionString(name);
        }

        public static string GetConnectionString()
        {
            return Config.GetConnectionString();
        }

        public static string GetSharedResourcePath()
        {
            return "SharedResource.ascx.resx";
        }

        public static string Encode(string queryString)
        {
            if (_tripleDes == null)
                _tripleDes = new TripleDESCryptoServiceProvider();
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, _tripleDes.CreateEncryptor(TripleDesKey, TripleDesVector), CryptoStreamMode.Write);
            byte[] input = Encoding.UTF8.GetBytes(queryString);
            cryptoStream.Write(input, 0, input.Length);
            cryptoStream.FlushFinalBlock();
            memoryStream.Position = 0;
            byte[] result = new byte[memoryStream.Length];
            memoryStream.Read(result, 0, result.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Convert.ToBase64String(result);
        }

        public static string Decode(string s)
        {
            if (_tripleDes == null)
                _tripleDes = new TripleDESCryptoServiceProvider();
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, _tripleDes.CreateDecryptor(TripleDesKey, TripleDesVector), CryptoStreamMode.Write);
            byte[] input = Convert.FromBase64String(s);
            cryptoStream.Write(input, 0, input.Length);
            cryptoStream.FlushFinalBlock();
            memoryStream.Position = 0;
            byte[] result = new byte[memoryStream.Length];
            memoryStream.Read(result, 0, result.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(result);

        }

        public static void AddCssReference(string cssFile, Page page)
        {
            HtmlLink link = new HtmlLink();
            link.Href = cssFile;
            link.Attributes.Add("type", "text/css");
            link.Attributes.Add("rel", "stylesheet");
            page.Header.Controls.Add(link);
        }

        /// <summary>
        /// get translation for CSPCMS Object
        /// </summary>
        /// <param name="cspCmsObject">Object to translate: Content Type or Content Field</param>
        /// <param name="cultureCode">Current Language Code</param>
        /// <returns></returns>
        public static string GetTranslation(object cspCmsObject, string cultureCode)
        {
            if (cspCmsObject is CSPCMS_ContentType)
            {
                CSPCMS_ContentType contentType = (CSPCMS_ContentType)cspCmsObject;
                CSPCMS_Translation translation = contentType.CSPCMS_Translations.FirstOrDefault(a => a.CultureCode == cultureCode);
                return (translation != null ? translation.ShortDesc : contentType.ContentTypeName);
            }
            if (cspCmsObject is CSPCMS_ContentTypeField)
            {
                CSPCMS_ContentTypeField field = (CSPCMS_ContentTypeField)cspCmsObject;
                CSPCMS_Translation translation = field.CSPCMS_Translations.FirstOrDefault(a => a.CultureCode == cultureCode);
                return (translation != null ? translation.ShortDesc : field.ContentTypeFieldName);
            }
            return cspCmsObject.ToString();
        }
    }

	//Common consts
	public class Const
	{
		public static string IGNORE_CATEGORY_TEXT = "IgnoreCategoryText";
		public const string UPLOAD_ROOT_DIRECTORY = "UploadRootDirectory";
	    public const string INSTANCE_NAME = "InstanceName";
	    public const string UPLOAD_COBRANDED_DIRECTORY = "UploadCobranedDirectory";
		public const string DOMAIN_NAME = "DomainName";
		public const string CONTENTGRID_SEARCHITEM_SUPPLIER = "Supplier";
		public const string CONTENTGRID_SEARCHITEM_IDENTIFIER = "content_identifier";
	    public const int PUBLISHED_CB_VALUE = 1;
	    public const int UNPUBLISHED_CB_VALUE = 2;
	    public const int ALL_CB_VALUE = 0;
	    public const int PUBLISHED_STAGES = 50;
	    public const string SETTING_CATEGORYCONTENT_CT_ID = "CategoryContentCTId";
	    public const string SETTING_DEFAULT_LANGUAGE_ID = "DefaultLanguageId";
	}
}