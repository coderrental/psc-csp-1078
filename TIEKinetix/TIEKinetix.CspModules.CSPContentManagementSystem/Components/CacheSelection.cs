﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TIEKinetix.CspModules.Data;

namespace TIEKinetix.CspModules.CSPContentManagementSystem.Components
{
    public class CacheSelection
    {
        /// <summary>
        /// Gets or sets the language id.
        /// </summary>
        /// <value>
        /// The language id.
        /// </value>
        /// Author: Vu Dinh
        /// 1/23/2013 - 5:48 PM
        public int LanguageId { get; set; }

        /// <summary>
        /// Gets or sets the supplier id.
        /// </summary>
        /// <value>
        /// The supplier id.
        /// </value>
        /// Author: Vu Dinh
        /// 1/23/2013 - 5:49 PM
        public int SupplierId { get; set; }

        /// <summary>
        /// Gets or sets the stages id.
        /// </summary>
        /// <value>
        /// The stages id.
        /// </value>
        /// Author: Vu Dinh
        /// 1/23/2013 - 5:49 PM
        public int StagesId { get; set; }

        /// <summary>
        /// Gets or sets the category id.
        /// </summary>
        /// <value>
        /// The category id.
        /// </value>
        /// Author: Vu Dinh
        /// 2/1/2013 - 2:25 PM
        public int CategoryId { get; set; }

        /// <summary>
        /// Gets or sets the type of the content.
        /// </summary>
        /// <value>
        /// The type of the content.
        /// </value>
        /// Author: Vu Dinh
        /// 1/23/2013 - 5:50 PM
        public CSPCMS_ContentType ContentType { get; set; }

        /// <summary>
        /// Gets or sets the search text.
        /// </summary>
        /// <value>
        /// The search text.
        /// </value>
        /// Author: Vu Dinh
        /// 1/31/2013 - 7:28 PM
        public string SearchText { get; set; }

        /// <summary>
        /// Gets or sets the time.
        /// </summary>
        /// <value>The time.</value>
        public DateTime Time { get; set; }
    }
}