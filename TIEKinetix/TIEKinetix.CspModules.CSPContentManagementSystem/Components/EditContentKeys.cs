﻿namespace TIEKinetix.CspModules.CSPContentManagementSystem
{
    /// <summary>
    /// hard-coded values to avoid typo
    /// </summary>
    public class EditContentKeys
    {
        public static string PortalId = "portalId";
        public static string TabModuleId = "tabModuleId";
        public static string ContentMainId = "cmId";
        public static string ContentTypeId = "ctId";
        public static string UserId = "userId";
        public static string DomainUrlForUploadedFiles = "path";
        public static string Action = "action";
        public static string GridId = "gridId";
        public static string IntegrationKey = "CspId";
        public static string ContentId = "cid";
    	public static string IsSave = "issaved";
    	public static string LangId = "langid";
    	public static string FileRoot = "fileroot";
    	public static string StaticDomain = "staticdomain";
    }
}