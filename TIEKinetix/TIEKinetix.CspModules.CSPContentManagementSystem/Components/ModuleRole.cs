﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DotNetNuke.Entities.Users;

namespace TIEKinetix.CspModules.CSPContentManagementSystem.Components
{
    /// <summary>
    /// Module Role: contains role function calls
    /// </summary>
    public class ModuleRole
    {
        public static string ViewEditRoleName = "CSPCMS_ViewEdit";
        public static string ViewOnlyRoleName = "CSPCMS_ViewOnly";
        public static string EditContentRoleName = "CSPCMS_EditContent";
        public static string EditContentExceptionRoleName = "CSPCMS_EditContentException";
        public static string EditAllContentConsumerExceptionRoleName = "CSPCMS_EditAllContentConsumerException";

        public static bool IsBackEndRole(UserInfo userInfo)
        {
            return IsInRole(userInfo, "Administrators");
        }
        public static bool IsViewOnlyRole(UserInfo userInfo)
        {
            return IsInRole(userInfo, ViewOnlyRoleName);
        }
        public static bool IsViewEditRole(UserInfo userInfo)
        {
            return IsInRole(userInfo, ViewEditRoleName);
        }
        public static bool IsEditContentRole(UserInfo userInfo)
        {
            return IsInRole(userInfo, EditContentRoleName);
        }
        public static bool IsEditContentExceptionRole(UserInfo userInfo)
        {
            return IsInRole(userInfo, EditContentExceptionRoleName);
        }
        public static bool IsEditAllContentConsumerExceptionRole(UserInfo userInfo)
        {
            return IsInRole(userInfo, EditAllContentConsumerExceptionRoleName);
        }
        public static bool IsInRole(UserInfo userInfo, params string[] roles)
        {
            bool flag = true;
            foreach (string role in roles)
            {
                flag &= userInfo.IsInRole(role);
                if (!flag)
                    break;
            }
            return flag;
        }
    }
}