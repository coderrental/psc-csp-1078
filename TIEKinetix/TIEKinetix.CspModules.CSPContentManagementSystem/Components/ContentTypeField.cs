﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TIEKinetix.CspModules.Data;

namespace TIEKinetix.CspModules.CSPContentManagementSystem.Components
{
    public class ContentTypeField
    {
        public ContentTypeField()
        {
        }

        public CSPCMS_ContentTypeField Field { get; set; }

        public content_field DataField { get; set; }

    }
}