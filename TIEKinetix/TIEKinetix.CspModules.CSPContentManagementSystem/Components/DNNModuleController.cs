using System;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Search;
using DotNetNuke.Services.Scheduling;

namespace TIEKinetix.CspModules.CSPContentManagementSystem.Components
{

    public class DnnModuleController : DnnModuleBase, ISearchable
    {
        public SearchItemInfoCollection GetSearchItems(ModuleInfo modInfo)
        {
            var searchItemCollection = new SearchItemInfoCollection();
            return searchItemCollection;
        }
    }

    public class DnnModuleScheduler : SchedulerClient
    {
        private ScheduleHistoryItem oLogger = null;
        public DnnModuleScheduler(ScheduleHistoryItem logger)
            : base()
        {
            oLogger = logger;

        }

        public override void DoWork()
        {
            try
            {
                oLogger.Succeeded = true;

            }
            catch (Exception exp)
            {
                oLogger.Succeeded = false;
                oLogger.AddLogNote(exp.ToString());
                Errored(ref exp);
                DotNetNuke.Services.Exceptions.Exceptions.LogException(exp);
            }
        }
    }
}

