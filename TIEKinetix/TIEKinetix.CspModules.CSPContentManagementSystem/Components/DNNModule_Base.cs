using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Framework;
using DotNetNuke.Security;
using DotNetNuke.Services.Localization;
using DotNetNuke.Services.Log.EventLog;
using TIEKinetix.CspModules.Data;

namespace TIEKinetix.CspModules.CSPContentManagementSystem.Components
{
    [Serializable]

    #region Class DnnModuleBase
    
    public class DnnModuleBase : PortalModuleBase, IActionable
    {
        #region "Private Vars"

        private readonly ModuleController _moduleController = new ModuleController();
        private string _sharedResourceFile;

        #endregion

        protected ModuleActionCollection MyActions = new ModuleActionCollection();
        protected string ConnectionString = "", CurrentLanguage = "";
        protected CspDataContext CspDataContext;

        #endregion

        public CDefault BasePage
        {
            get
            {
                try
                {
                    return (CDefault)Page;
                }
                catch
                {
                    return null;
                }
            }
        }

        public void SetModuleConfiguration(ModuleInfo config)
        {
            ModuleConfiguration = config;
        }

        protected virtual void Page_Init(Object sender, EventArgs e)
        {
            try
            {
                _sharedResourceFile = LocalResourceFile.Substring(0, LocalResourceFile.LastIndexOf("/") + 1) + "SharedResource";
                ConnectionString = Config.GetConnectionString("CspPortal" + PortalId);
                CurrentLanguage = Thread.CurrentThread.CurrentCulture.Name;
                CspDataContext = new CspDataContext(ConnectionString);

                /*
                // add settings
                MyActions.Add(
                        GetNextActionID(),
                        "Settings",
                        "Settings",
                        "",
                        ControlPath + "images/text_rich.png",
                        EditUrl("Settings"),
                        false,
                        SecurityAccessLevel.Edit,
                        true, // visible
                        false // new window
                    );
                */

                ModuleConfiguration.ModuleTitle = Localization.GetString("ModuleTitle", _sharedResourceFile);
            }
            catch (ThreadAbortException taexp)
            {
            }
            catch (Exception exp)
            {
            }
        }


        #region Optional Interfaces

        public virtual string GetUrl(string moduleKey, string key, object id)
        {
            return Globals.NavigateURL(PortalSettings.ActiveTab.TabID, moduleKey, "mid", ModuleId.ToString(), key, id.ToString());
        }

        /// <summary>
        /// Prints to log.
        /// </summary>
        /// <param name="logLabel">The log label.</param>
        /// <param name="logMessage">The log message.</param>
        public void PrintToLog(string logLabel, string logMessage)
        {
            var eventLog = new EventLogController();
            var logInfo = new LogInfo();
            logInfo.LogUserID = UserId;
            logInfo.LogPortalID = PortalSettings.PortalId;
            logInfo.LogTypeKey = EventLogController.EventLogType.ADMIN_ALERT.ToString();
            logInfo.AddProperty(string.Format("{0} : ",logLabel),logMessage);
            eventLog.AddLog(logInfo);
        }

        public virtual string GetUrl(string moduleKey, params KeyValuePair<string, string>[] parameters)
        {
            List<string> paramList = new List<string>();
            paramList.Add("mid");
            paramList.Add(ModuleId.ToString());
            foreach (KeyValuePair<string, string> pair in parameters)
            {
                paramList.Add(pair.Key);
                paramList.Add(pair.Value);
            }
            return Globals.NavigateURL(PortalSettings.ActiveTab.TabID, moduleKey, paramList.ToArray());
        }

        public virtual string GetLocalizedText(string resourceKey)
        {
            return Localization.GetString(resourceKey, _sharedResourceFile);
        }


        public virtual ModuleActionCollection ModuleActions
        {
            get
            {
                return MyActions;
            }
        }

        public string ReadSetting(string setting)
        {
            setting = setting.Trim().ToLower();
            try
            {
                Hashtable ht = _moduleController.GetModuleSettings(ModuleId);
                if (ht[setting] != null)
                {
                    return (string)ht[setting];
                }
            }
            catch
            {
                return "";
            }
            return "";
        }

        public void SaveSetting(string setting, string settingValue)
        {
            setting = setting.Trim().ToLower();
            try
            {
                _moduleController.UpdateModuleSetting(ModuleId, setting, settingValue);
            }
            catch (Exception)
            {
            }
        }

    	public string IgnoredCategoryText
    	{
    		get
    		{
    			string ignoredCategoryText = Settings[Const.IGNORE_CATEGORY_TEXT] as string;
    			return string.IsNullOrEmpty(ignoredCategoryText) ? "AutoGen" : ignoredCategoryText;
			}
    	}

        public int DefaultLanguageId
        {
            get
            {
                var defaultLanguageId = Settings[Const.SETTING_DEFAULT_LANGUAGE_ID] as string;
                return string.IsNullOrEmpty(defaultLanguageId) ? 1 : int.Parse(defaultLanguageId);
            }
        }

        #endregion
    }
}