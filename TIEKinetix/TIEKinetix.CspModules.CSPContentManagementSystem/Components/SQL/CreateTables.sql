﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CSPCMS_CodeList]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CSPCMS_CodeList](
	[CSPCMS_CodeList_Id] [uniqueidentifier] NOT NULL,
	[CodeListName] [nvarchar](100) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateChanged] [datetime] NULL,
 CONSTRAINT [PK_CSPCMS_CodeList] PRIMARY KEY CLUSTERED 
(
	[CSPCMS_CodeList_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CSPCMS_ContentType]    Script Date: 04/08/2012 14:08:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CSPCMS_ContentType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CSPCMS_ContentType](
	[CSPCMS_ContentType_Id] [uniqueidentifier] NOT NULL,
	[PortalId] [int] NOT NULL,
	[TabModuleId] [int] NOT NULL,
	[ContentTypeId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateChanged] [datetime] NULL,
 CONSTRAINT [PK_CSPCMS_ContentType] PRIMARY KEY CLUSTERED 
(
	[CSPCMS_ContentType_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CSPCMS_Validation]    Script Date: 04/08/2012 14:08:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CSPCMS_Validation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CSPCMS_Validation](
	[CSPCMS_Validation_Id] [uniqueidentifier] NOT NULL,
	[ValidationName] [nvarchar](100) NOT NULL,
	[ValidationRule] [nvarchar](1000) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateChanged] [datetime] NULL,
 CONSTRAINT [PK_CSPCMS_Validation] PRIMARY KEY CLUSTERED 
(
	[CSPCMS_Validation_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CSPCMS_ContentTypeFieldTranslation]    Script Date: 04/08/2012 14:08:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CSPCMS_ContentTypeFieldTranslation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CSPCMS_ContentTypeFieldTranslation](
	[CSPCMS_ContentTypeFieldTranslationId] [uniqueidentifier] NOT NULL,
	[ContentTypeFieldName] [nvarchar](150) NOT NULL,
	[FieldName] [nvarchar](250) NULL,
	[FieldDescription] [nvarchar](500) NULL,
	[CultureCode] [nvarchar](50) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateChanged] [datetime] NULL,
 CONSTRAINT [PK_CSPCMS_ContentTypeFieldTranslation] PRIMARY KEY CLUSTERED 
(
	[CSPCMS_ContentTypeFieldTranslationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CSPCMS_ContentTypeField]    Script Date: 04/08/2012 14:08:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CSPCMS_ContentTypeField]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CSPCMS_ContentTypeField](
	[CSPCMS_ContentTypeField_Id] [uniqueidentifier] NOT NULL,
	[CSPCMS_ContentType_Id] [uniqueidentifier] NOT NULL,
	[ContentTypeFieldId] [int] NOT NULL,
	[ContentTypeFieldName] [nvarchar](150) NOT NULL,
	[ContentTypeFieldType] [nvarchar](25) NULL,
	[Required] [bit] NOT NULL,
	[Readonly] [bit] NOT NULL,
	[DefaultValue] [nvarchar](max) NULL,
	[CodeListId] [uniqueidentifier] NULL,
	[ValidationId] [uniqueidentifier] NULL,
	[DisplayOrder] [int] NOT NULL,
	[DisplayGroupOrder] [int] NOT NULL,
 CONSTRAINT [PK_CSPCMS_ContentTypeField] PRIMARY KEY CLUSTERED 
(
	[CSPCMS_ContentTypeField_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CSPCMS_CodeListValue]    Script Date: 04/08/2012 14:08:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CSPCMS_CodeListValue]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CSPCMS_CodeListValue](
	[CSPCMS_CodeListValueId] [uniqueidentifier] NOT NULL,
	[CSPCMS_CodeList_Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Value] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_CSPCMS_CodeListValue] PRIMARY KEY CLUSTERED 
(
	[CSPCMS_CodeListValueId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Default [DF_CSPCMS_CodeList_CSPCMS_CodeList_Id]    Script Date: 04/08/2012 14:08:07 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CSPCMS_CodeList_CSPCMS_CodeList_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[CSPCMS_CodeList]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CSPCMS_CodeList_CSPCMS_CodeList_Id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CSPCMS_CodeList] ADD  CONSTRAINT [DF_CSPCMS_CodeList_CSPCMS_CodeList_Id]  DEFAULT (newid()) FOR [CSPCMS_CodeList_Id]
END


End
GO
/****** Object:  Default [DF_CSPCMS_CodeList_DateCreated]    Script Date: 04/08/2012 14:08:07 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CSPCMS_CodeList_DateCreated]') AND parent_object_id = OBJECT_ID(N'[dbo].[CSPCMS_CodeList]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CSPCMS_CodeList_DateCreated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CSPCMS_CodeList] ADD  CONSTRAINT [DF_CSPCMS_CodeList_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
END


End
GO
/****** Object:  Default [DF_CSPCMS_CodeListValue_CSPCMS_CodeListValueId]    Script Date: 04/08/2012 14:08:07 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CSPCMS_CodeListValue_CSPCMS_CodeListValueId]') AND parent_object_id = OBJECT_ID(N'[dbo].[CSPCMS_CodeListValue]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CSPCMS_CodeListValue_CSPCMS_CodeListValueId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CSPCMS_CodeListValue] ADD  CONSTRAINT [DF_CSPCMS_CodeListValue_CSPCMS_CodeListValueId]  DEFAULT (newid()) FOR [CSPCMS_CodeListValueId]
END


End
GO
/****** Object:  Default [DF_CSPCMS_ContentType_Id]    Script Date: 04/08/2012 14:08:07 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CSPCMS_ContentType_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[CSPCMS_ContentType]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CSPCMS_ContentType_Id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CSPCMS_ContentType] ADD  CONSTRAINT [DF_CSPCMS_ContentType_Id]  DEFAULT (newid()) FOR [CSPCMS_ContentType_Id]
END


End
GO
/****** Object:  Default [DF_CSPCMS_ContentType_DateCreated]    Script Date: 04/08/2012 14:08:07 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CSPCMS_ContentType_DateCreated]') AND parent_object_id = OBJECT_ID(N'[dbo].[CSPCMS_ContentType]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CSPCMS_ContentType_DateCreated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CSPCMS_ContentType] ADD  CONSTRAINT [DF_CSPCMS_ContentType_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
END


End
GO
/****** Object:  Default [DF_CSPCMS_ContentTypeField_CSPCMS_ContentTypeField_Id]    Script Date: 04/08/2012 14:08:07 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CSPCMS_ContentTypeField_CSPCMS_ContentTypeField_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[CSPCMS_ContentTypeField]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CSPCMS_ContentTypeField_CSPCMS_ContentTypeField_Id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CSPCMS_ContentTypeField] ADD  CONSTRAINT [DF_CSPCMS_ContentTypeField_CSPCMS_ContentTypeField_Id]  DEFAULT (newid()) FOR [CSPCMS_ContentTypeField_Id]
END


End
GO
/****** Object:  Default [DF_CSPCMS_ContentTypeFieldTranslation_CSPCMS_ContentTypeFieldTranslationId]    Script Date: 04/08/2012 14:08:07 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CSPCMS_ContentTypeFieldTranslation_CSPCMS_ContentTypeFieldTranslationId]') AND parent_object_id = OBJECT_ID(N'[dbo].[CSPCMS_ContentTypeFieldTranslation]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CSPCMS_ContentTypeFieldTranslation_CSPCMS_ContentTypeFieldTranslationId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CSPCMS_ContentTypeFieldTranslation] ADD  CONSTRAINT [DF_CSPCMS_ContentTypeFieldTranslation_CSPCMS_ContentTypeFieldTranslationId]  DEFAULT (newid()) FOR [CSPCMS_ContentTypeFieldTranslationId]
END


End
GO
/****** Object:  Default [DF_CSPCMS_ContentTypeFieldTranslation_DateCreated]    Script Date: 04/08/2012 14:08:07 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CSPCMS_ContentTypeFieldTranslation_DateCreated]') AND parent_object_id = OBJECT_ID(N'[dbo].[CSPCMS_ContentTypeFieldTranslation]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CSPCMS_ContentTypeFieldTranslation_DateCreated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CSPCMS_ContentTypeFieldTranslation] ADD  CONSTRAINT [DF_CSPCMS_ContentTypeFieldTranslation_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
END


End
GO
/****** Object:  Default [DF_CSPCMS_Validation_CSPCMS_Validation_Id]    Script Date: 04/08/2012 14:08:07 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CSPCMS_Validation_CSPCMS_Validation_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[CSPCMS_Validation]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CSPCMS_Validation_CSPCMS_Validation_Id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CSPCMS_Validation] ADD  CONSTRAINT [DF_CSPCMS_Validation_CSPCMS_Validation_Id]  DEFAULT (newid()) FOR [CSPCMS_Validation_Id]
END


End
GO
/****** Object:  Default [DF_CSPCMS_Validation_DateCreated]    Script Date: 04/08/2012 14:08:07 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CSPCMS_Validation_DateCreated]') AND parent_object_id = OBJECT_ID(N'[dbo].[CSPCMS_Validation]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CSPCMS_Validation_DateCreated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CSPCMS_Validation] ADD  CONSTRAINT [DF_CSPCMS_Validation_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
END


End
GO
/****** Object:  ForeignKey [FK_CSPCMS_CodeListValue_CSPCMS_CodeList]    Script Date: 04/08/2012 14:08:07 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CSPCMS_CodeListValue_CSPCMS_CodeList]') AND parent_object_id = OBJECT_ID(N'[dbo].[CSPCMS_CodeListValue]'))
ALTER TABLE [dbo].[CSPCMS_CodeListValue]  WITH CHECK ADD  CONSTRAINT [FK_CSPCMS_CodeListValue_CSPCMS_CodeList] FOREIGN KEY([CSPCMS_CodeList_Id])
REFERENCES [dbo].[CSPCMS_CodeList] ([CSPCMS_CodeList_Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CSPCMS_CodeListValue_CSPCMS_CodeList]') AND parent_object_id = OBJECT_ID(N'[dbo].[CSPCMS_CodeListValue]'))
ALTER TABLE [dbo].[CSPCMS_CodeListValue] CHECK CONSTRAINT [FK_CSPCMS_CodeListValue_CSPCMS_CodeList]
GO
/****** Object:  ForeignKey [FK_CSPCMS_ContentTypeField_CSPCMS_ContentType]    Script Date: 04/08/2012 14:08:07 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CSPCMS_ContentTypeField_CSPCMS_ContentType]') AND parent_object_id = OBJECT_ID(N'[dbo].[CSPCMS_ContentTypeField]'))
ALTER TABLE [dbo].[CSPCMS_ContentTypeField]  WITH CHECK ADD  CONSTRAINT [FK_CSPCMS_ContentTypeField_CSPCMS_ContentType] FOREIGN KEY([CSPCMS_ContentType_Id])
REFERENCES [dbo].[CSPCMS_ContentType] ([CSPCMS_ContentType_Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CSPCMS_ContentTypeField_CSPCMS_ContentType]') AND parent_object_id = OBJECT_ID(N'[dbo].[CSPCMS_ContentTypeField]'))
ALTER TABLE [dbo].[CSPCMS_ContentTypeField] CHECK CONSTRAINT [FK_CSPCMS_ContentTypeField_CSPCMS_ContentType]
GO
