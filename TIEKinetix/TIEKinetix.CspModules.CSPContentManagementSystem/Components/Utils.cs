using System;
using System.Diagnostics;
using System.Threading;

namespace TIEKinetix.CspModules.CSPContentManagementSystem.Components
{
    public class Utils
    {
        public static void LogMessage(string moduleName, string moduleSection, Exception exp)
        {
            if (exp is ThreadAbortException) return;
            if (exp.Message.ToLower().Contains("thread") && exp.Message.ToLower().Contains("abort")) return;
            LogMessage(moduleName, moduleSection, exp.Message, exp.StackTrace);
        }

        public static void LogMessage(string moduleName, string moduleSection, string message, string stackTrace)
        {
            try
            {
                // Do other things to log the message
                Debug.WriteLine(moduleName + ":" + moduleSection + ":" + message, stackTrace);
            }
            catch
            {
            }
        }
    }
}