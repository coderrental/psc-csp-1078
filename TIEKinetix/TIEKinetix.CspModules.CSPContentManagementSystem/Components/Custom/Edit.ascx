﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Edit.ascx.cs" Inherits="TIEKinetix.CspModules.CSPContentManagementSystem.Components.Custom.Edit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

    <table class="edit-table" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th colspan="4">
                    <telerik:RadToolBar runat="server" ID="toolBar" Height="24px" AutoPostBack="true">
                        <Items>
                            <telerik:RadToolBarButton ImageUrl="../../images/16x16/disk_blue.png" Text="Update" CommandName="Update" Value="Update"></telerik:RadToolBarButton>
                            <telerik:RadToolBarButton IsSeparator="true"></telerik:RadToolBarButton>
                            <telerik:RadToolBarButton ImageUrl="../../images/16x16/disk_blue_error.png" Text="Cancel" CommandName="Cancel" Value="Cancel"></telerik:RadToolBarButton>
                        </Items>                        
                    </telerik:RadToolBar>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="edit-table-label"><label>Display Order</label></td>
                <td><telerik:RadNumericTextBox runat="server" ID="tbDisplayOrder" Width="75px" Type="Number" NumberFormat-DecimalDigits="0">
                        <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                        <EnabledStyle HorizontalAlign="Right" />
                    </telerik:RadNumericTextBox></td>
                <td  class="edit-table-label"><label>Validation</label></td>
                <td><telerik:RadComboBox ID="cbValidation" runat="server"  EmptyMessage="-"></telerik:RadComboBox></td>
            </tr>
            <tr>
                <td class="edit-table-label"><label>Display Group</label></td>
                <td><telerik:RadNumericTextBox runat="server" ID="tbDisplayGroupOrder" Width="75px" Type="Number" NumberFormat-DecimalDigits="0">
                    <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                    <EnabledStyle HorizontalAlign="Right" />
                    </telerik:RadNumericTextBox>
                </td>
                <td class="edit-table-label"><label>Code List</label></td>
                <td><telerik:RadComboBox ID="cbCodeList" runat="server"  EmptyMessage="-"></telerik:RadComboBox></td>
            </tr>
            <tr>
                <td class="edit-table-label"><label>Readonly</label></td>
                <td><asp:CheckBox runat="server" ID="cbReadonly" /></td>
                <td class="edit-table-label"><label>Field Type</label></td>
                <td><telerik:RadComboBox ID="cbPropertyType" runat="server" EmptyMessage="-" Text='<%#Bind("ContentTypeFieldType") %>'></telerik:RadComboBox></td>
            </tr>
            <tr>
                <td class="edit-table-label"><label>Required</label></td>
                <td colspan="3"><asp:CheckBox runat="server" ID="cbRequired"/></td>
            </tr>            
            <tr>
                <td class="edit-table-label"><label>Visible</label></td>
                <td colspan="3"><asp:CheckBox runat="server" ID="cbVisible"/></td>
            </tr>            
            <tr>
                <td class="edit-table-label"><label>Searchable?</label></td>
                <td colspan="3"><asp:CheckBox runat="server" ID="cbSearchable"/></td>
            </tr>            
            <tr>
                <td class="edit-table-label"><label>Is Consumer Exception Field?</label></td>
                <td colspan="3"><asp:CheckBox runat="server" ID="cbConsumerExceptionField"/></td>
            </tr>            
            <tr>
                <td class="edit-table-label"><label>Default Value</label></td>
                <td colspan="3">
                    <telerik:RadEditor ID="reDefaultValue" runat="server" ToolbarMode="Default" EditModes="All" ContentAreaMode="Div" AutoResizeHeight="true" Width="100%">
                    <Tools>
                        <telerik:EditorToolGroup>
                            <telerik:EditorTool Name="Copy" />
                            <telerik:EditorTool Name="Cut" />
                            <telerik:EditorTool Name="Paste" />                           
                            <telerik:EditorSeparator />
                            <telerik:EditorTool Name="Bold" />
                            <telerik:EditorTool Name="Italic" />
                            <telerik:EditorTool Name="Underline" />                            
                            <telerik:EditorSeparator />
                            <telerik:EditorTool Name="JustifyLeft" />
                            <telerik:EditorTool Name="JustifyCenter" />
                            <telerik:EditorTool Name="JustifyRight" />
                        </telerik:EditorToolGroup>
                    </Tools>                    
                    <Content>
</Content>                    
                    </telerik:RadEditor> 
                </td>
            </tr>
        </tbody>
    </table>
