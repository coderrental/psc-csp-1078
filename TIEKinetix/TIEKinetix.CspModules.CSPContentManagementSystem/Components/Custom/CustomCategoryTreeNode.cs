﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TIEKinetix.CspModules.Data;

namespace TIEKinetix.CspModules.CSPContentManagementSystem.Components.Custom
{
    /// <summary>
    /// Custom Category Tree Node
    /// </summary>
    public class CustomCategoryTreeNode
    {
        public int ParentId { get; set; }
        public int Id { get; set; }
        public string Text { get; set; }
        public CustomCategoryTreeNode Parent { get; set; }
        public List<CustomCategoryTreeNode> Children { get; set; }

        public CustomCategoryTreeNode(int parentId, int id, string text)
        {
            ParentId = parentId;
            Id = id;
            Text = text;
            Parent = null;
            Children = new List<CustomCategoryTreeNode>();
        }
        public CustomCategoryTreeNode() : this(-1, -1, string.Empty)
        {
        }

        public CustomCategoryTreeNode(CustomCategoryTreeNode parent, int id, string text)
        {
            Parent = parent;
            ParentId = parent.Id;
            Id = id;
            Text = text;
            Children = new List<CustomCategoryTreeNode>();
        }

        /// <summary>
        /// create a nested tree from a flat array
        /// </summary>
        /// <param name="categories">category array</param>
        /// <returns>nested tree node</returns>
        public static CustomCategoryTreeNode Create(List<category> categories)
        {
            if (categories.Count == 0)
                return null;

            //create parent node
            CustomCategoryTreeNode node = new CustomCategoryTreeNode(-1, -1, "All");
            CustomCategoryTreeNode temp = null;
            foreach (category category in categories.OrderBy(a=>a.depth))
            {
                temp = node.Find(category.parentId, category.lineage);
            	string slabel = string.IsNullOrEmpty(category.description) ? category.categoryText : category.description;
							//slabel += " (" + category.categoryId + ")";
								if (temp == null)
								{
									node.Children.Add(new CustomCategoryTreeNode(-1, category.categoryId, slabel));
								}
								else
								{
									temp.Children.Add(new CustomCategoryTreeNode(temp, category.categoryId, slabel));
								}
            }
            return node;
        }

        /// <summary>
        /// recursive find function
        /// </summary>
        /// <param name="parentId">category parent id to look for</param>
        /// <param name="lineage">lineage to help with missing levels</param>
        /// <returns>tree node if found else null</returns>
        private CustomCategoryTreeNode Find(int? parentId, string lineage)
        {
            if (!parentId.HasValue)
                return null;
            if (Id == parentId.Value)
                return this;
            
            CustomCategoryTreeNode node = null;
            foreach (CustomCategoryTreeNode child in Children)
            {
                node = child.Find(parentId, lineage);
                if (node != null)
                    return node;
            }

            // attempt to find parent node using lineage
            //string[] lineages = lineage.Split(new string[] {"/"}, StringSplitOptions.RemoveEmptyEntries);

            //if (lineages.Length > 1)
            //{
            //    for (int i = lineages.Length - 1; i >= 0; i--)
            //    {
            //        node = this.Find(Convert.ToInt32(lineages[i]), "");
            //        if (node != null)
            //            return node;
            //    }
            //}

            return node;
        }
    }
}