﻿using System;
using System.Collections;
using System.Web.UI;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Localization;
using Telerik.Web.UI;
using TIEKinetix.CspModules.Data;

namespace TIEKinetix.CspModules.CSPContentManagementSystem.Components.Custom
{
    public partial class Edit : UserControl
    {
        private object _dataItem;
        private DnnDataContext _dnnContext;

        /// <summary>
        /// Telerik DataBinding Object
        /// </summary>
        public object DataItem
        {
            get { return _dataItem; }
            set { _dataItem = value; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            _dnnContext = new DnnDataContext(Common.GetConnectionString());

            // load validation values
            cbValidation.Items.Add(new RadComboBoxItem("", "")); // default value
            foreach (CSPCMS_Validation val in _dnnContext.CSPCMS_Validations)
            {
                cbValidation.Items.Add(new RadComboBoxItem(val.ValidationName, val.CSPCMS_Validation_Id.ToString()));
            }

            //load code list
            cbCodeList.Items.Add(new RadComboBoxItem("", ""));
            foreach (CSPCMS_CodeList codeList in _dnnContext.CSPCMS_CodeLists)
            {
                cbCodeList.Items.Add(new RadComboBoxItem(codeList.CodeListName, codeList.CSPCMS_CodeList_Id.ToString()));
            }

            //load type
            cbPropertyType.Items.Add(new RadComboBoxItem("", ""));
            cbPropertyType.Items.Add(new RadComboBoxItem("Text", "Text"));
            cbPropertyType.Items.Add(new RadComboBoxItem("LongText", "LongText"));
            cbPropertyType.Items.Add(new RadComboBoxItem("Date", "Date"));
            cbPropertyType.Items.Add(new RadComboBoxItem("Image", "Image"));
            cbPropertyType.Items.Add(new RadComboBoxItem("File Upload", "File"));
            cbPropertyType.Items.Add(new RadComboBoxItem("Html", "Html"));
            cbPropertyType.Items.Add(new RadComboBoxItem("Number", "Number"));

            cbValidation.DataBind();
            cbCodeList.DataBind();
            DataBinding += EditDataBinding;

            //update button image
            //todo: back end / Custom Edit Form / update the button image paths
            //toolBar.FindItemByValue("Update").ImageUrl = ControlPath + "images/"
        }

        private void EditDataBinding(object sender, EventArgs e)
        {
            object obj;

            //default value
            obj = DataBinder.Eval(DataItem, "DefaultValue");
            reDefaultValue.Content = obj != null ? obj.ToString() : "";

            //display order
            obj = DataBinder.Eval(DataItem, "DisplayOrder");
            tbDisplayOrder.Value = Convert.ToDouble(obj ?? 0);
            obj = DataBinder.Eval(DataItem, "DisplayGroupOrder");
            tbDisplayGroupOrder.Value = Convert.ToDouble(obj ?? 0);

            // checkboxes
            obj = DataBinder.Eval(DataItem, "Readonly");
            cbReadonly.Checked = (bool) (obj != DBNull.Value ? obj : false);
            obj = DataBinder.Eval(DataItem, "Required");
            cbRequired.Checked = (bool) (obj != DBNull.Value ? obj : false);
            obj = DataBinder.Eval(DataItem, "Visible");
            cbVisible.Checked = (bool)(obj != DBNull.Value ? obj : false);
            obj = DataBinder.Eval(DataItem, "Searchable");
            cbSearchable.Checked = (bool)(obj != DBNull.Value ? obj : false);
            obj = DataBinder.Eval(DataItem, "ConsumerExceptionField");
            cbConsumerExceptionField.Checked = (bool)(obj != DBNull.Value ? obj : false);

            //combo boxes
            obj = DataBinder.Eval(DataItem, "ValidationId");
            if (obj != null)
            {
                foreach (RadComboBoxItem item in cbValidation.Items)
                {
                    if (item.Value != obj.ToString()) continue;
                    cbValidation.SelectedValue = item.Value;
                    break;
                }
            }
            obj = DataBinder.Eval(DataItem, "CodeListId");
            if (obj != null)
            {
                foreach (RadComboBoxItem item in cbCodeList.Items)
                {
                    if (item.Value != obj.ToString()) continue;
                    cbCodeList.SelectedValue = item.Value;
                    break;
                }
            }
            obj = DataBinder.Eval(DataItem, "ContentTypeFieldType");
            if (obj != null)
            {
                foreach (RadComboBoxItem item in cbPropertyType.Items)
                {
                    if (item.Value == obj.ToString())
                    {
                        cbPropertyType.SelectedValue = item.Value;
                        break;
                    }
                }
            }
        }
    }
}