﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleSettings.ascx.cs" Inherits="TIEKinetix.CspModules.CSPContentManagementSystem.ModuleSettings" %>

<div>
	<table>
		<tr>
			<td>Upload root directory (e.g: path/to/upload/file) </td>
			<td><asp:TextBox runat="server" ID="tbUploadRootDir"></asp:TextBox> </td>
		</tr>
        <tr>
            <td>Upload Cobranded Directory (e.g: path/to/upload/file)</td>
            <td><asp:TextBox runat="server" ID="tbUploadCobrandedDir"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Instance name</td>
            <td><asp:TextBox runat="server" ID="tbInstanceName"></asp:TextBox></td>
        </tr>
		<tr>
			<td>Domain name (e.g: http://domainname.com)</td>
			<td><asp:TextBox runat="server" ID="tbDomainName"></asp:TextBox> </td>
		</tr>
        <tr>
			<td>Category Text to ignore</td>
			<td><asp:TextBox runat="server" ID="tbIgnoreCategoryText"></asp:TextBox> </td>
		</tr>
         <tr>
			<td>CategoryContent content type id</td>
			<td><asp:TextBox runat="server" ID="tbCategoryContentCTId"></asp:TextBox> </td>
		</tr>
        <tr>
            <td>Default Language id (for Edit manage asset form)</td>
            <td><asp:TextBox runat="server" ID="tbxDefaultLanguageId"></asp:TextBox></td>
        </tr>
	</table>
</div>