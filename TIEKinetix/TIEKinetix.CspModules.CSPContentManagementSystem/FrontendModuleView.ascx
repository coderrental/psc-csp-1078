﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FrontendModuleView.ascx.cs" Inherits="TIEKinetix.CspModules.CSPContentManagementSystem.FrontendModuleView" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxLoadingPanel ID="loadingPanel" IsSticky="True" Transparency="50" CssClass="ajax-loading-panel" runat="server"></telerik:RadAjaxLoadingPanel>
<telerik:RadAjaxManager runat="server" DefaultLoadingPanelID="loadingPanel">
    <AjaxSettings>
        <telerik:AjaxSetting runat="server" AjaxControlID="contentGrid">
            <UpdatedControls >
                <telerik:AjaxUpdatedControl ControlID="contentGrid"></telerik:AjaxUpdatedControl>
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>
<asp:Panel runat="server" ID="pnContentTypeSelection">
    <div class="listview-container">
        <telerik:RadListView ID="listView" runat="server" ItemPlaceholderID="content_type_container">
            <LayoutTemplate>
                <div class="container" runat="server" id="content_type_container"></div>
            </LayoutTemplate>
            <ItemTemplate>
                <div class="content-type-container">
                    <a class="content_type" href='<%# GetUrl(Eval("ContentTypeId")) %>'>
                        <span><%#Eval("ContentTypeName")%></span>
                    </a>
                    <div class="clear"></div>
                </div>
            </ItemTemplate>
        </telerik:RadListView>
        <div class="clear"></div>
    </div>
    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script language="javascript" type="text/javascript">
	    function SetTileWidth() {
		    var tiles = $("div.content-type-container", "div.listview-container");
		    var max = 0;
		    tiles.each(function () {
			    if (max < $(this).width())
				    max = $(this).width();
		    });

		    if (max > 0 && max < 120) {
			    tiles.each(function () { $(this).css("width", max + "px"); });
		    }
	    }
	    if ($.browser.msie && parseInt($.browser.version, 10) < 8) {
		    $(document).ready(function () { SetTileWidth(); });
	    } else {
		    SetTileWidth();
	    }

	    jQuery(function () {
		    jQuery('div.content-type-container a').hover(function () {
			    jQuery(this).stop().animate({ backgroundColor: "#0054A6" });
		    }, function () {
			    jQuery(this).stop().animate({ backgroundColor: "#00AEEF" });
		    });
	    });
    </script>
</telerik:RadScriptBlock>
</asp:Panel>
<asp:Panel runat="server" ID="moduleContainer">
    <telerik:RadWindow runat="server" ID="editWindow" Behaviors="Close,Move" Modal="true" VisibleStatusbar="false" OnClientClose="EditWindowOnClientClose"
                       EnableShadow="true" Width="900px" Height="600px" ShowContentDuringLoad="false" OnClientShow="FixedEditor">                
    </telerik:RadWindow>
    <div class="cmsNavigation">
        <table cellpadding="4" cellspacing="4">
            <tr>
                <td>
                    <asp:Panel runat="server" ID="pnLanguage">
                        <div class="csp-nav-title"><%= GetLocalizedText("Label.Language") %></div>
                        <telerik:RadComboBox runat="server" ID="cbLanguage" OnSelectedIndexChanged="ComboBoxOnSelectedIndexChanged" AutoPostBack="True"></telerik:RadComboBox>
                    </asp:Panel>
                </td>
                <td>
                    <asp:Panel runat="server" ID="pnSupplier">
                        <div class="csp-nav-title"><%= GetLocalizedText("Label.Supplier") %></div>
                        <telerik:RadComboBox runat="server" ID="cbSupplier" OnSelectedIndexChanged="ComboBoxOnSelectedIndexChanged" AutoPostBack="True"></telerik:RadComboBox>
                    </asp:Panel>
                </td>
                <td>
                    <asp:Panel runat="server" ID="pnStages">
                        <div class="csp-nav-title"><%= GetLocalizedText("Label.Stages") %></div>
                        <telerik:RadComboBox runat="server" ID="cbStages" OnSelectedIndexChanged="ComboBoxOnSelectedIndexChanged" AutoPostBack="True"></telerik:RadComboBox>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <table cellpadding="4" cellspacing="4">
            <tr>
                <td>
                    <asp:Panel runat="server" ID="pnContentType">
                        <span class="csp-nav-title"><%= GetLocalizedText("Label.SelectContentType") %></span>
                        <telerik:RadComboBox runat="server" ID="cbContentType" OnSelectedIndexChanged="ComboBoxOnSelectedIndexChanged" AutoPostBack="true"></telerik:RadComboBox>
                    </asp:Panel>
                </td>
                <td>
                    <asp:Panel runat="server" ID="pnCategories">
                        <telerik:RadComboBox runat="server"  ID="cbCategoriesTree" ShowToggleImage="true" Width="400px" OnClientDropDownOpened="OnClientDropDownOpenedHandler">
                            <ItemTemplate>
                                <telerik:RadTreeView runat="server" ID="categoryTree" OnClientNodeClicking="ClientNodeClicking" OnNodeClick="TreeViewNodeClickEventHandler" OnClientNodeExpanded="CategoryClientNodExplanded">
                                    <DataBindings>
										<telerik:RadTreeNodeBinding Expanded="true" />
									</DataBindings>
                                </telerik:RadTreeView>
                            </ItemTemplate>
                            <Items>
                                <telerik:RadComboBoxItem Text="" />
                            </Items>
                            <CollapseAnimation Type="None" />
                        </telerik:RadComboBox>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div class="contentGridContainer">
        <div class="commandBlock">
            <telerik:RadButton runat="server" OnClick="btnAddContent_OnClick" ID="btnAddContent">
                <Icon PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconCssClass="rbAdd"/>
            </telerik:RadButton>
            <telerik:RadComboBox runat="server" ID="cbboxSearchField"/>

                <telerik:RadTextBox runat="server" ID="tbxSearch" onkeydown = "return (event.keyCode!=13);"/>
                <asp:Button runat="server" ID="btnCancelSearch" CssClass="cancel-search" OnClick="OnCancelSearchClick" ToolTip="Reset search field"/>
                <telerik:RadButton runat="server" ID="btnSearch" OnClick="btnSearch_OnClick">
                    <Icon PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconCssClass="rbSearch"/>
                </telerik:RadButton>

        </div>
        <telerik:RadGrid ID="contentGrid" runat="server" AllowPaging="true" PageSize="20" Height="99%" Width="100%" OnItemCommand="GridOnItemCommand" 
                         AllowFilteringByColumn="false" OnColumnCreated="GridOnColumnCreated" OnItemDataBound="GridOnItemDataBound" AllowSorting="true">
            <MasterTableView AutoGenerateColumns="true" DataKeyNames="content_main_Id,content_Id,content_identifier,content_types_languages_Id" CommandItemDisplay="None">
                <SortExpressions>
                    <telerik:GridSortExpression FieldName="cmID" SortOrder="Descending" />
                </SortExpressions>
                <Columns>
                    <telerik:GridButtonColumn UniqueName="EditContent" ButtonType="ImageButton" HeaderStyle-Width="30px" CommandName="EditContent"></telerik:GridButtonColumn>
					<telerik:GridButtonColumn UniqueName="CloneContent" ButtonCssClass="clone-content" ButtonType="ImageButton" HeaderStyle-Width="30px" CommandName="CloneContent"></telerik:GridButtonColumn>
                    <telerik:GridButtonColumn UniqueName="EditConsumerException" ButtonType="ImageButton" HeaderStyle-Width="30px" CommandName="EditConsumerException"></telerik:GridButtonColumn>						
                </Columns>
                <NoRecordsTemplate>
                    <h1 style="margin:5px;">No item available</h1>
                </NoRecordsTemplate>                                        
            </MasterTableView>
            <ClientSettings>
                <Selecting AllowRowSelect="true" />                    
            </ClientSettings> 
            <PagerStyle Mode="NextPrevAndNumeric"></PagerStyle>               
        </telerik:RadGrid>    
    </div>
	
	<telerik:RadWindowManager ID="windowManager" runat="server">
		<ConfirmTemplate>
			<!-- Clone Content Main -->
			<div class="confirm-section" id="clonemainlang-confirm">
				<div id="msg-clonemainlang-confirm">
					<p><%= GetLocalizedText("Confirm.CloneMainLanguage") %></p>
				</div>
				<div id="newmaintitle-wrapper">
					<span id="label-newmainid"><%= GetLocalizedText("Confirm.ContentMainId") %></span><input type="text" id="new-contentmain-title" name="new-contentmain-title"/>
					<input type="hidden" id="src-id" name="src-id"/>
				</div>
				<div id="clonemainlang-confirm-btn">
					<input class="Button" type="button" id="dialogCloneMainLang" value="Clone" onclick="javascript: DialogClickedHandler('clonemainlang');"/>
					<input class="Button" type="button" id="dialogCloneMainLangCancel" value="Cancel" onclick="javascript: DialogClickedHandler('cancel');"/>
				</div>
			</div>
		</ConfirmTemplate>
	</telerik:RadWindowManager>
	
	<telerik:RadAjaxLoadingPanel runat="server" ID="CustomLoadingPanel" Transparency="50" Style="background-color: #AAA"></telerik:RadAjaxLoadingPanel>
	<div style="display: none">
		<asp:Button runat="server" ID="btnCloneContentMain" ClientIDMode="Static" OnClick="CloneContentMain"/>
	</div>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
        	var errorMsg = '<%= errorMsg %>';

        	jQuery(window).load(function() {
        		if (errorMsg != '') {
        			var rnObj = $find('<%= customNotification.ClientID %>');
        			if (rnObj == null) {
        				alert(errorMsg);
        				return;
        			}
        			rnObj.set_text(errorMsg);
        			rnObj.set_autoCloseDelay(5000);
        			rnObj.show();
        		}
        	});
        	
	        jQuery(function() {
		        //animate when hover container
		        jQuery(".cmsNavigation").hover(function() {
			        jQuery(this).stop().animate({ boxShadow: "0 0 10px 5px #AAAAAA" });
		        }, function() {
			        jQuery(this).stop().animate({ boxShadow: "0 0 10px #CCCCCC" });
		        });

		        jQuery(".contentGridContainer .RadGrid").hover(function() {
			        jQuery(this).stop().animate({ boxShadow: "0 0 10px 5px #AAAAAA" });
		        }, function() {
			        jQuery(this).stop().animate({ boxShadow: "0 0 10px #CCCCCC" });
		        });

		        $('.clone-content').click(function(e) {
		        	e.preventDefault();
		        	var tmp = radconfirm("", null, 400, 200, null, $(this).attr('itemidentifier'), null);
		        	$('#new-contentmain-title').val($(this).attr('itemidentifier') + ' Copy');
		        	$('#src-id').val($(this).attr('rel'));
		        	//Getting rad window manager 
		        	var oManager = GetRadWindowManager();
		        	//Call GetActiveWindow to get the active window 
		        	var oActive = oManager.getActiveWindow();
		        	$(oActive._contentElement).parents('div.RadWindow').addClass('cr-custom-confirm');
		        	$(oActive._contentElement).parents('div.RadWindow').addClass('cr-clonemainlang-confirm');
		        	$('#new-contentmain-title').unbind('keypress').keypress(function (e) {
		        		if (e.which == 13) {
		        			e.preventDefault();
		        			DialogClickedHandler('clonemainlang');
		        		}
			        });
			        return false;
		        });
	        });

	        function EditWindowOnClientClose(sender, args) {
		        var grid = $find("<%= contentGrid.ClientID %>").get_masterTableView();
		        grid.fireCommand('Page', 'First');
		        ;
	        }

	        function ClientNodeClicking(sender, args) {
		        var cb = $find("<%=cbCategoriesTree.ClientID %>");
		        var node = args.get_node();
		        cb.set_text(node.get_text());
		        cb.set_value(node.get_value());
		        cb.trackChanges();
		        cb.get_items().getItem(0).set_text(node.get_text());
		        cb.get_items().getItem(0).set_value(node.get_value());
		        cb.commitChanges();
		        cb.hideDropDown();
		        cb.attachDropDown();
	        }
            
	        function CategoryClientNodExplanded(sender, args) {
		        var combo = $find("<%=cbCategoriesTree.ClientID %>");
		        setTimeout(function () {
			        if ($(combo._childListElementWrapper).height() < 30)
				        combo.showDropDown();
                    
		        }, 100);
	        }

	        function FixedEditor(sender, args) {
	        }

	        // fix an issue in chrome/ie where design and preview buttons dont work
	        /* ref: http://www.telerik.com/help/aspnet-ajax/window-troubleshooting-radeditor-in-radwindow.html */

	        function RadEditorOnClientLoad(sender, args) {
		        var editor = sender;
		        setTimeout(function() {
			        var e = $find(editor.get_id());
			        if (e != null) {
				        e.onParentNodeChanged();
				        var s = e.get_contentArea().style;
				        s.backgroundImage = 'none';
				        s.backgroundColor = 'white';
			        }
		        }, 1000);
	        }

	        function OnClientDropDownOpenedHandler(sender, eventArgs) {
		        var tree = sender.get_items().getItem(0).findControl("categoryTree");
		        var selectedNode = tree.get_selectedNode();
		        if (selectedNode) {
			        selectedNode.scrollIntoView();
		        }
	        }
            
	        function DialogClickedHandler(eventValue) {
		        var loadingPanel = $find('<%= CustomLoadingPanel.ClientID %>');
		        if (loadingPanel && eventValue != 'cancel')
			        loadingPanel.show('<%= moduleContainer.ClientID %>');
		        switch (eventValue) {
		        	case 'clonemainlang':
		        	if ($('#new-contentmain-title').val().trim() == '') {
		        		alert('<%= GetLocalizedText("Msg.InvalidIdentifier") %>');
		        		loadingPanel.hide('<%= moduleContainer.ClientID %>');
		        		return;
		        	}
		        	$('#btnCloneContentMain').click();
			        break;
		        }
		        var oManager = GetRadWindowManager();
		        //Call GetActiveWindow to get the active window 
		        var oActive = oManager.getActiveWindow();
		        oActive.close();
	        }
        </script>
    </telerik:RadCodeBlock>     
    <telerik:RadNotification runat="server" ID="customNotification" VisibleOnPageLoad="false">
    </telerik:RadNotification>
</asp:Panel>
<asp:Panel ID="moduleError" runat="server" Visible="false" Enabled="false">    
</asp:Panel>