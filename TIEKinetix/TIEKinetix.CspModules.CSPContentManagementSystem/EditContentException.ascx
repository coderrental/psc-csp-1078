﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditContentException.ascx.cs" Inherits="TIEKinetix.CspModules.CSPContentManagementSystem.EditContentException1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
        <telerik:RadNotification runat="server" ID="notification" VisibleOnPageLoad="false"></telerik:RadNotification>
        <asp:Panel runat="server" ID="contentContainer" Width="100%">
            <telerik:RadToolBar runat="server" ID="toolBar" Width="100%" OnButtonClick="ToolBarClickEventHandler">
            </telerik:RadToolBar>
            <asp:Panel runat="server" ID="contentPanel" CssClass="content-field-group" >
            </asp:Panel>
        </asp:Panel>