﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using TIEKinetix.CspModules.CSPContentManagementSystem.Components;
using TIEKinetix.CspModules.CSPContentManagementSystem.Components.Custom;
using TIEKinetix.CspModules.Data;
using Telerik.Web.UI;

namespace TIEKinetix.CspModules.CSPContentManagementSystem
{
    /// <summary>
    /// this page is designed to handle front end users requests
    /// </summary>
    public partial class FrontendModuleView : DnnModuleBase
    {
        private const string ADMIN_ROLE = "Admin Role";
        private string _cacheName;
        private string _selectionSession;
        private RadTreeView _categoryTree;
        private CSPCMS_ContentType _contentType;
    	private DnnDataContext _dnnContext;
        private int _languageId;
        private int _stagesId;
        private int _supplierId;
        private bool m_IsSearching;
        private string m_SearchField;
        private string m_SearchText;
	    public string errorMsg = "";

        /// <summary>
        /// append the domain url to the upload files url: www.tiekinetix.com/[upload file path]
        /// </summary>
        protected string DomainUrlForUploadFiles
        {
            get
            {
                return Config.GetSetting("Portal" + PortalId + "DomainUrlForUploadFiles");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Control docType = Page.FindControl("skinDocType");
                if (docType != null)
                {
                    ((Literal) docType).Text = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";
                }
                if (_contentType != null)
                    contentGrid.DataBind();
            }

            _languageId = int.Parse(cbLanguage.SelectedValue);
            _stagesId = int.Parse(cbStages.SelectedValue);
            _supplierId = int.Parse(cbSupplier.SelectedValue);

            TranslateCategoriesText();
        }

        private void TranslateCategoriesText()
        {
            if (Settings[Const.SETTING_CATEGORYCONTENT_CT_ID] != null && Settings[Const.SETTING_CATEGORYCONTENT_CT_ID].ToString() != string.Empty)
            {
                foreach (RadTreeNode node in _categoryTree.GetAllNodes())
                {
                    var categoryId = int.Parse(node.Value);
                    if (categoryId >= 0)
                    {
                        var categoryContent = CspDataContext.contents.FirstOrDefault(a => a.content_main.content_categories.Any(b => b.category_Id == categoryId) && a.content_main.content_types_Id == int.Parse(Settings[Const.SETTING_CATEGORYCONTENT_CT_ID].ToString()) && a.content_types_languages_Id == _languageId && a.stage_Id == Const.PUBLISHED_STAGES);
                        if (categoryContent != null) //display node text with category content - Content_Title
                        {
                            var contentTitle = CspDataContext.content_fields.FirstOrDefault(a => a.content_Id == categoryContent.content_Id && a.content_types_field.fieldname == "Content_Title");
                            if (contentTitle != null && !string.IsNullOrEmpty(contentTitle.value_text))
                            {
                                try
                                {
                                    var text = WebUtility.HtmlDecode(contentTitle.value_text);
                                    text = Regex.Replace(text, "<[^>]*(>|$)", "").Replace("\r", "").Replace("\n", "");
                                    node.Text = text;
                                }
                                catch
                                {
                                    node.Text = contentTitle.value_text;
                                }
                            }
                        }
                    }

                    //vu-dinh 08/15/14 make sure to strip out html tag from category text
                    node.Text = HtmlRemoval.StripTagsRegex(HttpUtility.HtmlDecode(node.Text));
                }
            }
        }

        /// <summary>
        /// Inits the components.
        /// </summary>
        /// Author: Vu Dinh
        /// 1/23/2013 - 3:43 PM
        private void InitComponents()
        {
            if (Request.Params["viewdetail"] != null)
            {
                pnContentTypeSelection.Visible = false;
            }
            else
            {
                var listContentType = new List<CSPCMS_ContentType> {_contentType};
                listContentType.AddRange(_contentType.CSPCMS_ContentTypeRelations.SelectMany(relation => relation.CSPCMS_ChildContentTypes));
                moduleContainer.Visible = false;
                listView.DataSource = listContentType;
            }

            contentGrid.NeedDataSource += GridNeedDataSource;
            contentGrid.PreRender += GridOnPreRender;

            //render command toolbar
            RenderCommandToolbar();

            // render navigation tree
            RenderCategoryTree();

            // render content type dropdow
            RenderContentTypeDropdown();

            // render language dropdow
            RenderLanguageDropdown();

            // render supplier dropdow
            RenderSupplierDropdown();

            // render stages dropdown
            RenderStagesDropdown();

            //Set component state if have cache
            SetComponentState();
        }

        /// <summary>
        /// Renders the command toolbar.
        /// </summary>
        /// Author: Vu Dinh
        /// 1/31/2013 - 5:26 PM
        private void RenderCommandToolbar()
        {
            btnAddContent.Text = GetLocalizedText("ContentGrid.Add");
            btnSearch.Text = GetLocalizedText("Label.Search");
            if (!ModuleRole.IsViewEditRole(UserInfo))
                btnAddContent.Visible = false;

            //Render search field combobox
            if (_contentType.CSPCMS_ContentTypeFields.Count(a => a.Searchable) > 0) // add only searchable fields
            {
                foreach (CSPCMS_ContentTypeField field in _contentType.CSPCMS_ContentTypeFields.Where(a => a.Searchable).OrderBy(a => a.DisplayOrder))
                {
                    cbboxSearchField.Items.Add(new RadComboBoxItem(field.ContentTypeFieldName, field.ContentTypeFieldName));
                }
            }

            cbboxSearchField.Items.Add(new RadComboBoxItem(GetLocalizedText("ContentGrid.SearchItemContentIdentifier"), Const.CONTENTGRID_SEARCHITEM_IDENTIFIER));
            //Set current search field when in search stage - DLV
            if (!string.IsNullOrEmpty(m_SearchField))
            {
                cbboxSearchField.SelectedIndex = cbboxSearchField.Items.FindItemByValue(m_SearchField).Index;
            }

        }

        /// <summary>
        /// Sets the state of the component.
        /// </summary>
        /// Author: Vu Dinh
        /// 1/23/2013 - 6:01 PM
        private void SetComponentState()
        {
            var objModules = new ModuleController();
            try
            {
                //set default language id
                if (!IsPostBack)
                {
                    var cbLgItem = cbLanguage.FindItemByValue(DefaultLanguageId.ToString());
                    if (cbLgItem != null)
                        cbLgItem.Selected = true;
                }
                if (!string.IsNullOrEmpty(_selectionSession))
                {
                    var hashSelection = HttpUtility.ParseQueryString(_selectionSession);
                    var time = DateTime.Parse(hashSelection["date"]);
                    if (time.AddHours(1) >= DateTime.Now)
                    {
                        cbContentType.FindItemByValue(hashSelection["contenttypeid"]).Selected = true;
                        cbLanguage.FindItemByValue(hashSelection["languageid"]).Selected = true;
                        cbSupplier.FindItemByValue(hashSelection["supplierid"]).Selected = true;
                        cbStages.FindItemByValue(hashSelection["stagesid"]).Selected = true;
                        tbxSearch.Text = (string.IsNullOrEmpty(hashSelection["searchtext"])) ? string.Empty :hashSelection["searchtext"];

                        //set state for categories tree
                        if (_categoryTree.FindNodeByValue(hashSelection["categoryid"]) != null)
                        {
                            cbCategoriesTree.SelectedValue =hashSelection["categoryid"];
                            cbCategoriesTree.Text = _categoryTree.FindNodeByValue(hashSelection["categoryid"]).Text;
                            _categoryTree.FindNodeByValue(hashSelection["categoryid"]).Selected = true;
                        }

                        //CacheState();
                    }
                    else
                    {
                        _selectionSession = null;
                        objModules.UpdateTabModuleSetting(TabModuleId, _cacheName, _selectionSession);
                        ModuleController.SynchronizeModule(ModuleId);
                    }

                }
                int ctId;
                if (Request.Params["ctid"] != null && int.TryParse(Request.Params["ctid"], out ctId))
                {
                    cbContentType.FindItemByValue(ctId.ToString()).Selected = true;
                }
            }
            catch (Exception exception)
            {
                PrintToLog("Session state Problem: ", exception.ToString());
                objModules.UpdateTabModuleSetting(TabModuleId, _cacheName, null);
                ModuleController.SynchronizeModule(ModuleId);
            }
            
        }

        /// <summary>
        /// Renders the stages dropdown.
        /// </summary>
        /// Author: Vu Dinh
        /// 1/23/2013 - 4:19 PM
        private void RenderStagesDropdown()
        {
            cbStages.Items.Add(new RadComboBoxItem(GetLocalizedText("Label.All"), Const.ALL_CB_VALUE.ToString()));
            cbStages.Items.Add(new RadComboBoxItem(GetLocalizedText("Label.Published"), Const.PUBLISHED_CB_VALUE.ToString()));
            cbStages.Items.Add(new RadComboBoxItem(GetLocalizedText("Label.UnPublished"), Const.UNPUBLISHED_CB_VALUE.ToString()));
        }

        /// <summary>
        /// Renders the supplier dropdow.
        /// </summary>
        /// Author: Vu Dinh
        /// 1/23/2013 - 4:12 PM
        private void RenderSupplierDropdown()
        {
            //supplier exception
            if (_contentType.CSPCMS_UserExceptionSuppliers.Count(a => a.UserId == UserId) > 0)
            {
                List<CSPCMS_UserExceptionSupplier> suppliers = _contentType.CSPCMS_UserExceptionSuppliers.Where(a => a.UserId == UserId).ToList();
                foreach (CSPCMS_UserExceptionSupplier supplier in suppliers)
                {
                    company supp = CspDataContext.companies.SingleOrDefault(a => a.companies_Id == supplier.SupplierId);
                    if (supp != null)
                        cbSupplier.Items.Add(new RadComboBoxItem(supp.companyname, supp.companies_Id.ToString()));
                }
            }
            else if (_contentType.CSPCMS_ExceptionSuppliers.Count > 0)
            {
                List<CSPCMS_ExceptionSupplier> exptionSuppliers = _contentType.CSPCMS_ExceptionSuppliers.ToList();
                foreach (CSPCMS_ExceptionSupplier supplier in exptionSuppliers)
                {
                    company supp = CspDataContext.companies.SingleOrDefault(a => a.companies_Id == supplier.SupplierId);
                    if (supp != null)
                        cbSupplier.Items.Add(new RadComboBoxItem(supp.companyname, supp.companies_Id.ToString()));
                }
            }
            else
            {
                foreach (company company in CspDataContext.companies.Where(a => a.is_supplier == true))
                {
                    cbSupplier.Items.Add(new RadComboBoxItem(company.companyname, company.companies_Id.ToString()));
                }
            }
        }

        /// <summary>
        /// Renders the language dropdow.
        /// </summary>
        /// Author: Vu Dinh
        /// 1/23/2013 - 4:05 PM
        private void RenderLanguageDropdown()
        {
            // language exceptions
            if (_contentType.CSPCMS_UserExceptionLanguages.Count(a => a.UserId == UserId) > 0)
            {
                IEnumerable<CSPCMS_UserExceptionLanguage> listLangs = _contentType.CSPCMS_UserExceptionLanguages.Where(a => a.UserId == UserId);
                foreach (CSPCMS_UserExceptionLanguage language in listLangs)
                {
                    language lang = CspDataContext.languages.SingleOrDefault(a => a.languages_Id == language.LanguageId);
                    if (lang!= null)
                        cbLanguage.Items.Add(new RadComboBoxItem(lang.description, lang.languages_Id.ToString()));
                }
            }
            else if (_contentType.CSPCMS_ExceptionLanguages.Any())
            {
                List<CSPCMS_ExceptionLanguage> listLangs = _contentType.CSPCMS_ExceptionLanguages.ToList();
                foreach (CSPCMS_ExceptionLanguage language in listLangs)
                {
                    language lang = CspDataContext.languages.SingleOrDefault(a => a.languages_Id == language.LanguageId);
                    if (lang != null)
                        cbLanguage.Items.Add(new RadComboBoxItem(lang.description, lang.languages_Id.ToString()));
                }
            }
            else
            {
                foreach (language lang in CspDataContext.languages.Where(a => a.active == true).OrderBy(a => a.description))
                {
                    cbLanguage.Items.Add(new RadComboBoxItem(lang.description, lang.languages_Id.ToString()));
                }   
            }
        }

        /// <summary>
        /// Renders the content type dropdow.
        /// </summary>
        /// Author: Vu Dinh
        /// 1/23/2013 - 3:43 PM
        private void RenderContentTypeDropdown()
        {
            cbContentType.Items.Add(new RadComboBoxItem
            {
                Text = Common.GetTranslation(_contentType, CurrentLanguage),
                Value = _contentType.ContentTypeId.ToString(),
                DataItem = _contentType
            });
            foreach (CSPCMS_ContentType childContentType in _contentType.CSPCMS_ContentTypeRelations.SelectMany(relation => relation.CSPCMS_ChildContentTypes))
            {
                cbContentType.Items.Add(new RadComboBoxItem
                {
                    Text = Common.GetTranslation(childContentType, CurrentLanguage),
                    Value = childContentType.ContentTypeId.ToString(),
                    DataItem = childContentType
                });
            }
        }

        /// <summary>
        /// Caches the state.
        /// </summary>
        /// Author: Vu Dinh
        /// 1/23/2013 - 5:54 PM
        private void CacheState()
        {
            var objModules = new ModuleController();
            _selectionSession =
                string.Format(
                    "contenttypeid={0}&languageid={1}&stagesid={2}&supplierid={3}&searchtext={4}&categoryid={5}&date={6}",
                    ((CSPCMS_ContentType) cbContentType.SelectedItem.DataItem).ContentTypeId, 
                    _languageId, 
                    _stagesId,
                    _supplierId, 
                    tbxSearch.Text.Trim(),
                    _categoryTree.SelectedValue == string.Empty ? -1 : int.Parse(_categoryTree.SelectedValue),
                    DateTime.Now);
            objModules.UpdateTabModuleSetting(TabModuleId, _cacheName, HttpUtility.UrlEncode(_selectionSession));
            ModuleController.SynchronizeModule(ModuleId);
        }

        #region Event Handlers


        /// <summary>
        /// grid item bound event handler
        /// - populate search fields using fields that are marked as searchable
        /// - show/hide edit content and edit consumer exception buttons
        /// </summary>
        /// <param name="sender">content grid</param>
        /// <param name="e"></param>
        protected void GridOnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
	            if (!ModuleRole.IsEditContentRole(UserInfo))
	            {
		            (((GridDataItem)e.Item)["EditContent"].Controls[0]).Visible = false;
					(((GridDataItem)e.Item)["CloneContent"].Controls[0]).Visible = false;
	            }
	            if (!ModuleRole.IsEditContentExceptionRole(UserInfo))
                    (((GridDataItem)e.Item)["EditConsumerException"].Controls[0]).Visible = false;
            }
        }

        /// <summary>
        /// grid pre render event handler
        /// - hide columns for edit content and/or edit consumer exception buttons
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void GridOnPreRender(object sender, EventArgs e)
        {
            var grid = (RadGrid) sender;
            foreach (GridColumn column in grid.MasterTableView.Columns)
            {
                if (column.UniqueName == "EditContent" && !ModuleRole.IsEditContentRole(UserInfo))
                    column.Visible = false;
                if (column.UniqueName == "EditConsumerException" && !ModuleRole.IsEditContentExceptionRole(UserInfo))
                    column.Visible = false;
            }
        }

        /// <summary>
        /// localize columns when these columns are created
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridOnColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
            var excludeColumns = new[] { "ExpandColumn", "content_main_Id", "content_Id", "companyname", "content_types_languages_Id", "cmID" };

            if (excludeColumns.Count(a => a == e.Column.UniqueName) == 0)
            {
                string v = GetLocalizedText("ContentGrid." + e.Column.UniqueName);
                e.Column.HeaderText = !string.IsNullOrEmpty(v) ? v : e.Column.HeaderText;
            }
            else
            {
                e.Column.Visible = false;
            }
        }



        /// <summary> 
        /// bind grid data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var grid = ((RadGrid) sender);
            if (!string.IsNullOrEmpty(tbxSearch.Text)) // Continue search result - DLV
            {
                GetContentMains(grid, tbxSearch.Text.Trim(), cbboxSearchField.SelectedValue);
            }
            else
            {
                GetContentMains(grid, "", "");		
            }
        }

        /// <summary>
        /// Gets the child cat ids.
        /// </summary>
        /// <param name="catId">The cat id.</param>
        /// <param name="firstCall">if set to <c>true</c> [first call].</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 2/1/2013 - 11:57 AM
        private IEnumerable<int> GetChildCatIds(int catId, bool firstCall)
        {
            var listChildCatIds = new List<int> ();
            if (firstCall)
                listChildCatIds.Add(catId);

            foreach (var childCatId in CspDataContext.categories.Where(a => a.parentId == catId && a.active == true && !a.categoryText.StartsWith(IgnoredCategoryText)).Select(a => a.categoryId))
            {
                listChildCatIds.Add(childCatId);
                listChildCatIds.AddRange(GetChildCatIds(childCatId, false));
            }
            return listChildCatIds;
        }

        /// <summary>
        /// function to query for content mains. this function helps grid datasource as needed
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="searchText"></param>
        /// <param name="fieldId"></param>
        private void GetContentMains(RadGrid grid, string searchText, string fieldName)
        {
            editWindow.VisibleOnPageLoad = false; // prevent edit window from poping on search
            int contentTypeId;
            if (!int.TryParse(cbContentType.SelectedValue, out contentTypeId))
                contentTypeId = _contentType.ContentTypeId;

            var contentType = (CSPCMS_ContentType) cbContentType.SelectedItem.DataItem;

            grid.ItemDataBound += GridItemDataBound;

            var temp = new StringBuilder();

            var listCats = GetChildCatIds((_categoryTree.SelectedValue == string.Empty || _categoryTree.SelectedValue == "-1")? 0: int.Parse(_categoryTree.SelectedValue),true);
            foreach (var catId in listCats)
            {
                temp.Append(string.Format("({0}),", catId));
            }

            string query = "insert into @temp(categoryId) values " + temp.ToString(0, temp.Length -1);
            query = @"
declare @temp table (
categoryId int primary key
)" + query;

            // bring searchable fields to the grid
            CSPCMS_ContentTypeField[] searchableFields = contentType.CSPCMS_ContentTypeFields.Where(a => a.Searchable).ToArray();
            if (searchableFields.Length > 0)  
            {
                query += @"
select * from (
select cf.value_text, ctf.fieldname, cm.content_identifier, cp.companyname + ' (' + cast(cm.supplier_Id as varchar(10)) + ')' as Supplier, c.content_types_languages_Id, c.content_Id, cm.content_main_Id, case when c.stage_Id = 50 then 'Published' else 'UnPublished' end as [Stage], cm.cmID
from content_main cm
join content_categories cc on cc.content_main_Id=cm.content_main_Id
join companies cp on cm.supplier_Id = cp.companies_Id
join @temp tmp on tmp.categoryId = cc.category_Id
join content c on c.content_main_Id=cm.content_main_Id
join content_fields cf on cf.content_Id=c.content_Id
inner loop join content_types_fields ctf on ctf.content_types_fields_Id=cf.content_types_fields_Id
where cm.content_types_Id = {0} and c.content_types_languages_Id = {1} and cm.supplier_Id = {2} [stages]
) as p
pivot
(
max(value_text) for fieldname in ([pivot columns])
) as p1
";
                string pivotCols = "";
                foreach (CSPCMS_ContentTypeField field in searchableFields)
                {
                    pivotCols += "["+field.ContentTypeFieldName+"],";
                }
                pivotCols = pivotCols.Remove(pivotCols.Length - 1, 1);
                query = query.Replace("[pivot columns]", pivotCols);

                //search
                if (!string.IsNullOrEmpty(fieldName) && !string.IsNullOrEmpty(searchText))
                {
                    query += string.Format("where p1.[{0}] like '%{1}%'",fieldName,searchText);
                }
            } // end of search block
            else
            {
                query += @"
select * from (
select distinct cm.content_main_Id, cm.content_identifier, cp.companyname + ' (' + cast(cm.supplier_Id as varchar(10)) + ')' as Supplier, c.content_Id, case when c.stage_Id = 50 then 'Published' else 'UnPublished' end as [Stage], c.content_types_languages_Id, cm.cmID
from content_main cm
join content c on c.content_main_Id=cm.content_main_Id
join companies cp on cm.supplier_Id = cp.companies_Id
join content_categories cc on cc.content_main_Id=cm.content_main_Id
join @temp tmp on tmp.categoryId = cc.category_Id	
where cm.content_types_Id = {0} and c.content_types_languages_Id = {1} and cm.supplier_Id = {2} [stages]
) as p1
";
                if (!string.IsNullOrEmpty(searchText))
                {
                    if (fieldName == Const.CONTENTGRID_SEARCHITEM_IDENTIFIER)
                        query += string.Format("where p1.[content_identifier] LIKE '%{0}%'",searchText);
                }
            }

                

            // stages
            string stages = string.Empty;
            if (_stagesId == Const.PUBLISHED_CB_VALUE)
            {
                stages = string.Format(" and c.stage_Id = {0}", Const.PUBLISHED_STAGES);
            }
            else if (_stagesId == Const.UNPUBLISHED_CB_VALUE)
            {
                stages = string.Format(" and c.stage_Id not in ({0})",Const.PUBLISHED_STAGES);
            }

            query = query.Replace("[stages]", stages);
                
            var dataTable = new DataTable();
            var connection = (SqlConnection) CspDataContext.Connection;
            if (connection.State != ConnectionState.Open)
                connection.Open();
			SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted);
			try
			{
				using (var command = new SqlCommand("", connection, transaction))
				{
					command.CommandText = string.Format(query, contentTypeId, _languageId, _supplierId);
					var dataAdapter = new SqlDataAdapter(command);
					dataAdapter.Fill(dataTable);
				}
				transaction.Commit();
			}
			catch (Exception ex)
			{
				transaction.Rollback();
				throw new Exception(ex.Message + "." + query, ex);
			}
			finally
			{
				
				connection.Close();
			}
        	contentGrid.DataSource = dataTable;
        }

        protected void GridItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = (GridDataItem) e.Item;
                ((ImageButton) item["EditContent"].Controls[0]).ImageUrl = ControlPath + "images/16x16/pencil-field.png";
				((ImageButton)item["CloneContent"].Controls[0]).ImageUrl = ControlPath + "images/16x16/clone.png";
				((ImageButton)item["CloneContent"].Controls[0]).Attributes["rel"] = item.GetDataKeyValue("content_main_Id").ToString();
				((ImageButton)item["CloneContent"].Controls[0]).Attributes["itemidentifier"] = item.GetDataKeyValue("content_identifier").ToString();
                ((ImageButton) item["EditConsumerException"].Controls[0]).ImageUrl = ControlPath + "images/16x16/pencil-minus.png";
            }
        }


        protected void ComboBoxOnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            editWindow.VisibleOnPageLoad = false;
            contentGrid.CurrentPageIndex = 0;
            contentGrid.Rebind();
        }

        protected void TreeViewEventHandler(object sender, RadTreeNodeEventArgs e)
        {
            editWindow.VisibleOnPageLoad = false;
            contentGrid.CurrentPageIndex = 0;
            contentGrid.Rebind();
        }

        protected void TreeViewNodeClickEventHandler(object sender, RadTreeNodeEventArgs e)
        {
            cbCategoriesTree.SelectedValue = e.Node.Value;
            contentGrid.CurrentPageIndex = 0;
            contentGrid.Rebind();
        }

        /// <summary>
        /// grid buttons event handlers
        /// - handle on click event for edit content
        /// - handle on click event for edit consumer exception
        /// 
        /// open up a new window and encrypt request string that contains name/value pair variables.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridOnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.Item is GridFilteringItem)
            {
            }
            else if (e.Item is GridDataItem)
            {
                var item = (GridDataItem) e.Item;
                Guid contentTypeId;
                string queryString;
                string hash;
                switch (e.CommandName)
                {
                    case "EditContent":
                        contentTypeId = ((CSPCMS_ContentType) cbContentType.SelectedItem.DataItem).CSPCMS_ContentType_Id;
                        queryString = string.Format("cmId={0}&ctId={1}&path={2}&action=edit&gridId={3}&langid={4}&cid={5}",
                                                    item.GetDataKeyValue("content_main_Id"),
                                                    contentTypeId, 
                                                    DomainUrlForUploadFiles,
                                                    contentGrid.ClientID,
                                                    item.GetDataKeyValue("content_types_languages_Id"),
                                                    item.GetDataKeyValue("content_Id")
                            );


                        hash = Common.Encode(queryString);
                        //Cache state before leave this form
                        CacheState();
                        Response.Redirect(Globals.NavigateURL(TabId, "EditContent", "mid=" + ModuleId + "&hash=" + HttpUtility.UrlEncode(hash)), false);
                        break;
                    case "EditConsumerException":
                        contentTypeId = ((CSPCMS_ContentType) cbContentType.SelectedItem.DataItem).CSPCMS_ContentType_Id;
                        queryString = string.Format("cmId={0}&ctId={1}&path={2}&action=edit&cid={3}",
                                                    item.GetDataKeyValue("content_main_Id"),
                                                    contentTypeId, 
                                                    DomainUrlForUploadFiles,
                                                    item.GetDataKeyValue("content_Id"));

                        hash = Common.Encode(queryString);
                        //Cache state before leave this form
                        CacheState();
                        Response.Redirect(Globals.NavigateURL(TabId, "EditContentException", "mid=" + ModuleId + "&hash=" + HttpUtility.UrlEncode(hash)), false);
                        break;
                    default:
                        break;
                }
            } // end of grid data item
        }

        #endregion

        #region Category Tree View Functions

        /// <summary>
        /// if user category exceptions exist:
        ///     render user exception categories
        /// else if category exceptions exist
        ///     render exception categories
        /// else 
        ///     render all categories
        /// </summary>
        private void RenderCategoryTree()
        {
            _categoryTree = (RadTreeView)cbCategoriesTree.Items[0].FindControl("categoryTree");
            cbCategoriesTree.EmptyMessage = GetLocalizedText("Label.CategoriesTreeShowAll");
            
            var categories = new List<category>();
            category[] tempCategories = CspDataContext.categories.Where(a => a.active.HasValue && a.active.Value && !a.categoryText.StartsWith(IgnoredCategoryText)).ToArray();
            if (_contentType.CSPCMS_UserExceptionCategories.Count(a=>a.UserId == UserId) > 0)
            {
                // user category exceptions
                categories.AddRange(tempCategories.Where(a => _contentType.CSPCMS_UserExceptionCategories.Count(b => b.CategoryId == a.categoryId && b.UserId == UserId) > 0));
            }
            else if (_contentType.CSPCMS_ExceptionCategories.Count > 0)
            {
                // category exceptions
                categories.AddRange(tempCategories.Where(a => _contentType.CSPCMS_ExceptionCategories.Count(b => b.CategoryId == a.categoryId) > 0));
            }
            else
            {
                categories = tempCategories.ToList();
            }

            CustomCategoryTreeNode root = CustomCategoryTreeNode.Create(categories);
            
            RenderNestedTree(null, root);
        }

        /// <summary>
        /// recursive function to create a nest tree
        /// </summary>
        /// <param name="radTreeNode">current tree node</param>
        /// <param name="node">data tree node</param>
        private void RenderNestedTree(RadTreeNode radTreeNode, CustomCategoryTreeNode node)
        {
            if (node == null)
                return;
            var tmp = new RadTreeNode(node.Text, node.Id.ToString());
            tmp.Expanded = true;
            if (radTreeNode == null)
            {
                _categoryTree.Nodes.Add(tmp);
            }
            else
            {
                radTreeNode.Nodes.Add(tmp);
            }

            foreach (CustomCategoryTreeNode child in node.Children)
            {
                RenderNestedTree(tmp, child);
            }
        }
        #endregion

        #region Overrides of DnnModuleBase

        /// <summary>
        /// page init event handler: handle page initialization
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void Page_Init(object sender, EventArgs e)
        {
            //redirect to the main page if the role is backend.
            if (ModuleRole.IsBackEndRole(UserInfo))
                Response.Redirect(Globals.NavigateURL(), true);

            base.Page_Init(sender, e);

            //Include jquery libraries
            Page.ClientScript.RegisterClientScriptInclude("jboxshadowanimated", ControlPath + "js/jboxshadowanimated.js");
            Page.ClientScript.RegisterClientScriptInclude("jcoloranimate", ControlPath + "js/jcoloranimated.js");

            //Get cache selection
            _cacheName = string.Format("SelectionCache_{0}_{1}_{2}", UserId, PortalId, TabId);
            _selectionSession = Settings[_cacheName] == null ? null :HttpUtility.UrlDecode(Settings[_cacheName].ToString());
            //Get data context
            _dnnContext = new DnnDataContext(Config.GetConnectionString());

            _contentType = _dnnContext.CSPCMS_ContentTypes.FirstOrDefault(a => a.Active && a.PortalId == PortalId && a.TabModuleId == TabModuleId);

            if (_contentType == null)
            {
                moduleContainer.Enabled = moduleContainer.Visible = false;
                moduleError.Enabled = moduleError.Visible = true;
                moduleError.Controls.Add(new Literal { Text = GetLocalizedText("FrontEndModuleView.ConfigurationError") });
                return;
            }

            InitComponents();
            ModuleConfiguration.ModuleTitle = GetLocalizedText("ModuleTitle");
        }

        /// <summary>
        /// Gets the URL.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 2/1/2013 - 11:26 AM
        public string GetUrl(object obj)
        {
            var contentTypeId = int.Parse(obj.ToString());
            return Globals.NavigateURL(TabId, "Content", "mid", ModuleId.ToString(), "ctid", contentTypeId.ToString(), "viewdetail", "1");
        }

        #endregion

        /// <summary>
        /// Handles the OnClick event of the btnAddContent control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 1/31/2013 - 5:43 PM
        protected void btnAddContent_OnClick(object sender, EventArgs e)
        {
            if (m_IsSearching)
                return;
            RadComboBoxItem selectedContentType = cbContentType.SelectedItem;
            string queryString = string.Format("cmId={0}&ctId={1}&action=add&gridId={2}&langid={3}",
                                               Guid.Empty,
                                               ((CSPCMS_ContentType)selectedContentType.DataItem).CSPCMS_ContentType_Id,
                                               contentGrid.ClientID,
                                               cbLanguage.SelectedValue);

            string hash = Common.Encode(queryString);
            //Cache state before leave this form
            CacheState();
            Response.Redirect(Globals.NavigateURL(TabId, "EditContent", "mid=" + ModuleId + "&hash=" + HttpUtility.UrlEncode(hash)), false);
        }

        /// <summary>
        /// Handles the OnClick event of the btnSearch control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 1/31/2013 - 6:32 PM
        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            m_IsSearching = true;
            //GetContentMains(contentGrid, tbxSearch.Text, cbboxSearchField.SelectedValue);
            contentGrid.CurrentPageIndex = 0;
            contentGrid.Rebind();
        }

        protected void OnCancelSearchClick(object sender, EventArgs e)
        {
            m_IsSearching = false;
            GetContentMains(contentGrid, string.Empty, cbboxSearchField.SelectedValue);
            tbxSearch.Text = string.Empty;
            contentGrid.CurrentPageIndex = 0;
            contentGrid.Rebind();
        }

	    protected void CloneContentMain(object sender, EventArgs e)
	    {
		    errorMsg = "";
			String newIdentifier = Request.Form["new-contentmain-title"];
		    newIdentifier = newIdentifier.Substring(0, newIdentifier.Length - 1);
		    newIdentifier = newIdentifier.Trim();
			String srcIdStr = Request.Form["src-id"].Replace(",", "");
		    int srcLang = int.Parse(cbLanguage.SelectedValue);
			CSPCMS_ContentType contentType = _dnnContext.CSPCMS_ContentTypes.SingleOrDefault(a => a.CSPCMS_ContentType_Id == ((CSPCMS_ContentType)cbContentType.SelectedItem.DataItem).CSPCMS_ContentType_Id);

			Guid srcContentMainId = new Guid(srcIdStr);

			if (String.IsNullOrEmpty(newIdentifier))
			{
				errorMsg = GetLocalizedText("Msg.InvalidIdentifier");
				return;
			}

		    content_main existContentMain =
			    CspDataContext.content_mains.FirstOrDefault(a => a.content_identifier == newIdentifier);
			if (existContentMain != null)
			{
				errorMsg = GetLocalizedText("Msg.ContentMainDuplicated");
				return;
			}

			//Get source data
			//Content Main
		    content_main srcCTMain = CspDataContext.content_mains.FirstOrDefault(a => a.content_main_Id == srcContentMainId);

			if (srcCTMain == null)
			{
				errorMsg = GetLocalizedText("Msg.ContentMainNotFound");
				return;
			}

		    List<content_category> ccList = CspDataContext.content_categories.Where(a => a.content_main == srcCTMain).ToList();

			if (!ccList.Any())
			{
				errorMsg = GetLocalizedText("Msg.ContentCategoryNotFound");
				return;
			}
			
			//Content
		    content srcContent =
			    CspDataContext.contents.FirstOrDefault(
				    a => a.content_main == srcCTMain && a.content_types_languages_Id == srcLang);

			if (srcContent == null)
			{
				errorMsg = GetLocalizedText("Msg.ContentNotFound");
				return;
			}

			//Content fields
			List<ContentTypeField> srcContentFields = GetContentTypeFields(srcCTMain, srcContent, contentType);

			//Start CLONING
			//Create new content main
			content_main newContentMain = new content_main
				{
					content_main_Id = Guid.NewGuid(),
					content_main_key = newIdentifier,
					content_identifier = newIdentifier,
					content_types_Id = contentType.ContentTypeId,
					supplier_Id = Convert.ToInt32(cbSupplier.SelectedValue),
					consumer_Id = srcCTMain.consumer_Id,
					content_staging_scheme_Id = srcCTMain.content_staging_scheme_Id
				};
			CspDataContext.content_mains.InsertOnSubmit(newContentMain);

			//Create new content
		    content newContent = new content
			    {
				    content_Id = Guid.NewGuid(),
				    content_main = newContentMain,
				    stage_Id = srcContent.stage_Id,
				    active = srcContent.active,
					content_types_languages_Id = srcContent.content_types_languages_Id
			    };
			CspDataContext.contents.InsertOnSubmit(newContent);

			//Create new content categories
		    foreach (content_category contentCategory in ccList)
		    {
			    content_category newContentCategory = new content_category
				    {
					    content_main = newContentMain,
					    category = contentCategory.category,
					    publication_scheme_Id = contentCategory.publication_scheme_Id
				    };
				CspDataContext.content_categories.InsertOnSubmit(newContentCategory);
		    }

			//Add content fields
			foreach (var field in srcContentFields)
			{
				if (field.DataField != null)
				{
					CspDataContext.content_fields.InsertOnSubmit(new content_field
					{
						content_Id = newContent.content_Id,
						content_types_fields_Id = field.Field.ContentTypeFieldId,
						value_text = field.DataField.value_text

					});
				}
			}

			CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);

		    string queryString;
            string hash;
			queryString = string.Format("cmId={0}&ctId={1}&path={2}&action=edit&gridId={3}&langid={4}&cid={5}",
													newContentMain.content_main_Id,
													contentType.CSPCMS_ContentType_Id,
													DomainUrlForUploadFiles,
													contentGrid.ClientID,
													newContent.content_types_languages_Id,
													newContent.content_Id
							);


			hash = Common.Encode(queryString);
			//Cache state before leave this form
			CacheState();
			Response.Redirect(Globals.NavigateURL(TabId, "EditContent", "mid=" + ModuleId + "&hash=" + HttpUtility.UrlEncode(hash)), true);
	    }

		/// <summary>
		/// get content for the requested content main id and language id
		/// </summary>
		/// <param name="contentType">current content type</param>
		/// <param name="lngId">requested language id</param>
		/// <param name="contentMainId">requested content main id</param>
		/// <param name="contentId">The content id.</param>
		/// <returns>List{ContentTypeField}.</returns>
		private List<ContentTypeField> GetContentTypeFields(content_main contentMain, content theContent, CSPCMS_ContentType contentType)
		{
			var result = new List<ContentTypeField>();

			try
			{


				content_field[] fields = (from cf in CspDataContext.content_fields
										  join c in CspDataContext.contents on cf.content_Id equals c.content_Id
										  where c.content_main_Id == contentMain.content_main_Id && c.content_types_languages_Id == theContent.content_types_languages_Id && c.content_Id == theContent.content_Id
										  select cf).ToArray();
				foreach (CSPCMS_ContentTypeField field in contentType.CSPCMS_ContentTypeFields.Where(a => a.Visible).OrderBy(a => a.DisplayOrder).OrderBy(a => a.DisplayGroupOrder))
				{
					result.Add(new ContentTypeField
					{
						Field = field,
						DataField = fields.SingleOrDefault(a => a.content_types_fields_Id == field.ContentTypeFieldId)
					});
				}
			}
			catch (Exception ex)
			{
				foreach (CSPCMS_ContentTypeField field in contentType.CSPCMS_ContentTypeFields.OrderBy(a => a.DisplayOrder).OrderBy(a => a.DisplayGroupOrder))
				{
					result.Add(new ContentTypeField
					{
						Field = field,
						DataField = null
					});
				}
			}


			return result;
		}
    }
}