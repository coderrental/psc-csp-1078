using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("TIEKinetix.CspModules.CSPContentManagementSystem")]
[assembly: AssemblyDescription("TIEKinetix.CSPModules")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("TIEKinetix")]
[assembly: AssemblyProduct("TIEKinetix.CspModules.CSPContentManagementSystem")]
[assembly: AssemblyCopyright("Copyright © TIEKinetix 2012, All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("b77a4a3e-6500-4487-a84b-7ffa3cea6c30")]
[assembly: AssemblyVersion("1.0.0")]
