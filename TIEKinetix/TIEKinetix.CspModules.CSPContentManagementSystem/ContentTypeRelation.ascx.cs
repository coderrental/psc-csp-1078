﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using Telerik.Web.UI;
using TIEKinetix.CspModules.CSPContentManagementSystem.Components;
using TIEKinetix.CspModules.Data;

namespace TIEKinetix.CspModules.CSPContentManagementSystem
{
    public partial class ContentTypeRelation : DnnModuleBase
    {
        private Guid _cspCmsContentTypeId;
        private DnnDataContext _dnnContext;
        protected CSPCMS_ContentType _contentType;
        private bool _isError = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (_isError)
                {
                    pErrorContainer.Enabled = pErrorContainer.Visible = true;
                    pContainer.Enabled = pContainer.Visible = false;
                }
                else
                {
                    pErrorContainer.Enabled = pErrorContainer.Visible = false;
                    pContainer.Enabled = pContainer.Visible = true;
                }
            }

        }

        #region Overrides of DnnModuleBase

        protected override void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
            _dnnContext = new DnnDataContext(Config.GetConnectionString());
            string id = Request.QueryString["id"];
            _cspCmsContentTypeId = new Guid(id);
            _contentType = _dnnContext.CSPCMS_ContentTypes.First(a => a.CSPCMS_ContentType_Id == _cspCmsContentTypeId);

            if(_contentType == null)
            {
                _isError = true;
                return;
            }

            // find all relational keys for the given content type
            //CSPCMS_ContentTypeRelation[] relations = _contentType.CSPCMS_ContentTypeParentRelations.Where(a => a.CSPCMS_ContentType_Parent_Id == _contentType.CSPCMS_ContentType_Id).ToArray();

            // load parent key fields
            foreach (content_types_field ctf in CspDataContext.content_types_fields.Where(a=>a.content_types_Id == _contentType.ContentTypeId))
            {
                ListItem li = new ListItem(ctf.fieldname, ctf.content_types_fields_Id.ToString());
                //if (relations.Length > 0 && relations[0].CSPCMS_ParentContentTypeField.ContentTypeFieldId == ctf.content_types_fields_Id)
                //{
                //    li.Enabled = true;
                //}
                ddlParentKeyField.Items.Add(li);
            }

            //load child content type
            bool isFirstContentType = true;
            foreach (content_type ct in CspDataContext.content_types.Where(a=>a.content_types_Id != _contentType.ContentTypeId))
            {
                ddlChildContentType.Items.Add(new ListItem(ct.description, ct.content_types_Id.ToString()));
                if (isFirstContentType)
                {
                    isFirstContentType = false;
                    foreach (content_types_field field in CspDataContext.content_types_fields.Where(a=>a.content_types_Id == ct.content_types_Id))
                    {
                        ddlChildKeyField.Items.Add(new ListItem(field.fieldname, field.content_types_fields_Id.ToString()));
                    }
                }
            }
            

            //translation & localization
            lContentTypeName.Text = string.Format(GetLocalizedText("AddRelation.Breadcrumb"), _contentType.CSPCMS_Translations.Count(a => a.CultureCode == CurrentLanguage) > 0 ? _contentType.CSPCMS_Translations.First().ShortDesc : _contentType.ContentTypeName);
            lbChildContentType.Text = GetLocalizedText("AddRelation.ChildContentType");
            lbParentKeyField.Text = GetLocalizedText("AddRelation.ParentKeyField");
            lbChildKeyField.Text = GetLocalizedText("AddRelation.ChildKeyField");
            toolBar.Items.Add(new RadToolBarButton {Text = GetLocalizedText("AddRelation.Save"), ImageUrl = ControlPath + "images/save.png", Value = "Save"});
            toolBar.Items.Add(new RadToolBarButton { 
										Text = GetLocalizedText("AddRelation.Cancel"), 
										ImageUrl = ControlPath + "images/arrow_left.png", 
										Value = "Cancel",
										PostBack = false,
										NavigateUrl = Globals.NavigateURL()
			});
        }

        #endregion

        protected void ToolBarOnButtonClick(object sender, RadToolBarEventArgs e)
        {
            switch (e.Item.Value)
            {
                case "Save":
                    /* 
                     * if relation key not exist
                     *  if CMSCSP content type not exist, create CMSCSP content type record
                     *      create CMSCSP fields if these dont exist
                     *      create relation record
                     *      redirect to main
                     * else
                     *  update relation record
                     *  redirect to main
                     */

                    int childContentTypeId = Convert.ToInt32(ddlChildContentType.SelectedValue);
                    int parentKeyFieldId = Convert.ToInt32(ddlParentKeyField.SelectedValue);
                    int childKeyFieldId = Convert.ToInt32(ddlChildKeyField.SelectedValue);

                    if (_contentType.CSPCMS_ContentTypeRelations.Count(a=>a.CSPCMS_ChildContentTypes.Count(b=>b.ContentTypeId == childContentTypeId)>0) == 0) // no child relation for content type
                    {
                        if (_dnnContext.CSPCMS_ContentTypes.Count(a=>a.PortalId == PortalId && a.TabModuleId == TabModuleId && a.ContentTypeId == childContentTypeId) == 0) // no CMSCSP Content Type
                        {
                            var cspContentType = CspDataContext.content_types.First(a => a.content_types_Id == childContentTypeId);
                            CSPCMS_ContentType ct = new CSPCMS_ContentType
                                                        {
                                                            ContentTypeId = childContentTypeId,
                                                            CSPCMS_ContentType_Id = Guid.NewGuid(),
                                                            Active = false,
                                                            ContentTypeName = cspContentType.description,
                                                            TabModuleId = TabModuleId,
                                                            PortalId = PortalId,
                                                            UserId = UserId,
                                                            DateCreated = DateTime.Now,
                                                            DateChanged = DateTime.Now
                                                        };
                            _dnnContext.CSPCMS_ContentTypes.InsertOnSubmit(ct);
                            int displayOrder = 0;
                            foreach (content_types_field ctf in CspDataContext.content_types_fields.Where(a=>a.content_types_Id==childContentTypeId))
                            {
                                _dnnContext.CSPCMS_ContentTypeFields.InsertOnSubmit(new CSPCMS_ContentTypeField
                                                                                        {
                                                                                            CSPCMS_ContentType = ct,
                                                                                            CSPCMS_ContentTypeField_Id = Guid.NewGuid(),
                                                                                            ContentTypeFieldId = ctf.content_types_fields_Id,
                                                                                            ContentTypeFieldName = ctf.fieldname,
                                                                                            ContentTypeFieldType = "Text",
                                                                                            ContentTypeId = childContentTypeId,
                                                                                            DisplayOrder = (++displayOrder),
                                                                                            DefaultValue = ctf.default_value,
                                                                                            Readonly = false,
                                                                                            Required = false,
                                                                                            Visible = true
                                                                                        });
                            }
                            _dnnContext.SubmitChanges();
                        } // end of creating CSPCMS records
                    } 
                    else
                    {
                        // this cspcms content type exists
                    }

                    //create relation record
                    CSPCMS_ContentType childContentType = _dnnContext.CSPCMS_ContentTypes.First(a => a.PortalId == PortalId && a.TabModuleId == TabModuleId && a.ContentTypeId == childContentTypeId);
                    CSPCMS_ContentTypeField parentKeyField = _dnnContext.CSPCMS_ContentTypeFields.First(a => a.CSPCMS_ContentType_Id == _cspCmsContentTypeId && a.ContentTypeFieldId == parentKeyFieldId);
                    CSPCMS_ContentTypeField childKeyField = _dnnContext.CSPCMS_ContentTypeFields.First(a => a.CSPCMS_ContentType_Id == childContentType.CSPCMS_ContentType_Id && a.ContentTypeFieldId == childKeyFieldId);

                    if (_dnnContext.CSPCMS_ContentTypeRelations.Count(a=>a.CSPCMS_ContentTypeField_Parent_Id == _cspCmsContentTypeId 
                        && a.CSPCMS_ContentType_Child_Id == childContentType.CSPCMS_ContentType_Id) == 0) // key exists
                    {
                        CSPCMS_ContentTypeRelation relation = new CSPCMS_ContentTypeRelation
                        {
                            CSPCMS_ContentTypeRelation_Id = Guid.NewGuid(),
                            CSPCMS_ContentType_Id = childContentType.CSPCMS_ContentType_Id,
                            CSPCMS_ContentType_Parent_Id = _cspCmsContentTypeId,
                            CSPCMS_ContentType_Child_Id = childContentType.CSPCMS_ContentType_Id,
                            CSPCMS_ContentTypeField_Parent_Id = parentKeyField.CSPCMS_ContentTypeField_Id,
                            CSPCMS_ContentTypeField_Child_Id = childKeyField.CSPCMS_ContentTypeField_Id
                        };
                        _dnnContext.CSPCMS_ContentTypeRelations.InsertOnSubmit(relation);
                    }
                    else //key doesnt exist
                    {
                        CSPCMS_ContentTypeRelation relation = _dnnContext.CSPCMS_ContentTypeRelations.First(a => a.CSPCMS_ContentTypeField_Parent_Id == _cspCmsContentTypeId
                                                                                                                 && a.CSPCMS_ContentType_Child_Id == childContentType.CSPCMS_ContentType_Id);
                        relation.CSPCMS_ContentTypeField_Parent_Id = parentKeyField.CSPCMS_ContentTypeField_Id;
                        relation.CSPCMS_ContentTypeField_Child_Id = childKeyField.CSPCMS_ContentTypeField_Id;
                    }
                    

                    _dnnContext.SubmitChanges();
                    Response.Redirect(Globals.NavigateURL());
                    break;

                /*case "Cancel":
                    Response.Redirect(Globals.NavigateURL());
                    break;*/
                default:
                    break;
            }
        }

        protected void ChildContentTypeIndexChanged(object sender, EventArgs e)
        {
            ddlChildKeyField.Items.Clear();
            foreach (var ctf in CspDataContext.content_types_fields.Where(a => a.content_types_Id == Convert.ToInt32(ddlChildContentType.SelectedValue)))
            {
                ddlChildKeyField.Items.Add(new ListItem(ctf.fieldname, ctf.content_types_fields_Id.ToString()));
            }
        }
    }
}