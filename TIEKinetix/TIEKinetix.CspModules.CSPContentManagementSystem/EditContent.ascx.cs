﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Users;
using DotNetNuke.UI.WebControls;
using TIEKinetix.CspModules.CSPContentManagementSystem.Components;
using TIEKinetix.CspModules.CSPContentManagementSystem.Components.Custom;
using TIEKinetix.CspModules.Data;
using Telerik.Web.UI;
using Image = System.Web.UI.WebControls.Image;

namespace TIEKinetix.CspModules.CSPContentManagementSystem
{
    public partial class EditContent : DnnModuleBase
    {
        private string ADMIN_ROLE = "Admin Role";
        private CSPCMS_ContentType _contentType;
        private DnnDataContext _dnnContext;
        private string _fileRoot;
        private string _instanceName;
        private string _cobrandedDir;
        private string _hashQueryString;
        private Dictionary<string, string> _keys = new Dictionary<string, string>();
        private List<language> _languageExceptions;
        private string _staticDomain;
        private List<company> _supplierExceptions;
        private CacheSelection _cacheSelection;
        private Guid _contentId ;
        private Guid _contentMainId;
        private int _languageId;
        private List<category> _listCategories;
	    protected string lastSaved = String.Empty;
	    protected string showCloneConfirm = "0";

        protected override void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);

            // push requested parameters into a _key
            string hash = HttpUtility.HtmlDecode(Request.QueryString["hash"]);
            _hashQueryString = Common.Decode(hash);
            _keys = new Dictionary<string, string>();
            string[] parameters = Common.Decode(hash).Split(new[] { "&" }, StringSplitOptions.None);
            foreach (string parameter in parameters)
            {
                _keys.Add(parameter.Split('=')[0], parameter.Split('=')[1]);
	            if (parameter.Split('=')[0] == "saved")
		            lastSaved = parameter.Split('=')[1];
            }

            if (!Request.IsAuthenticated) return; // stop all responses

            //initialize LINQ contexts
            _dnnContext = new DnnDataContext(Config.GetConnectionString());

            //data
            _contentType = _dnnContext.CSPCMS_ContentTypes.SingleOrDefault(a => a.CSPCMS_ContentType_Id == new Guid(_keys[EditContentKeys.ContentTypeId]));

            if (Settings.ContainsKey(Const.UPLOAD_ROOT_DIRECTORY) && Settings[Const.UPLOAD_ROOT_DIRECTORY].ToString() != string.Empty)
                _fileRoot = Settings[Const.UPLOAD_ROOT_DIRECTORY].ToString();

            //Vu.Dinh - update cobranded dir to upload file
            if (Settings.ContainsKey(Const.UPLOAD_COBRANDED_DIRECTORY) && Settings[Const.UPLOAD_COBRANDED_DIRECTORY].ToString() != string.Empty)
                _cobrandedDir = Settings[Const.UPLOAD_COBRANDED_DIRECTORY].ToString();
            if (Settings.ContainsKey(Const.INSTANCE_NAME) && Settings[Const.INSTANCE_NAME].ToString() != string.Empty)
                _instanceName = Settings[Const.INSTANCE_NAME].ToString();

            if (Settings.ContainsKey(Const.DOMAIN_NAME) && Settings[Const.DOMAIN_NAME].ToString() != string.Empty)
            {
                _staticDomain = Settings[Const.DOMAIN_NAME].ToString();
                if (_staticDomain.EndsWith("/"))
                {
                    _staticDomain.Remove(_staticDomain.Length - 1,0); //remove last slash
                }
            }


            #region [Init radwindow settings]
            if (string.IsNullOrEmpty(_fileRoot)) //if does not set file upload dir, then set default
                _fileRoot = "UploadImages/Portal" + PortalId + "/" + _contentMainId;
            ExplorerWindow.NavigateUrl = ControlPath + "ImageFileDialog.aspx?fileroot=" + _fileRoot;

            rdFileExplorer.Configuration.EnableAsyncUpload = true;
            rdFileExplorer.Configuration.SearchPatterns = new string[] { "*.*" }; //Allow all file
            rdFileExplorer.Configuration.MaxUploadFileSize = 10240000 * 5;
            string physicalPath = Server.MapPath("~/" + _fileRoot); //Create path if this path does not exists
            if (!Directory.Exists(physicalPath))
                Directory.CreateDirectory(physicalPath);
            rdFileExplorer.Configuration.UploadPaths = rdFileExplorer.Configuration.ViewPaths = rdFileExplorer.Configuration.DeletePaths = new[] {"~/" + _fileRoot};

            var cbxCobrandedUpload = (CheckBox)FileExplorerWindow.ContentContainer.FindControl("cbxUseCobrandedDir");
            if (!string.IsNullOrEmpty(_cobrandedDir) && !string.IsNullOrEmpty(_instanceName))
            {
                rdCobrandedFileExplorer.Configuration.EnableAsyncUpload = true;
                rdCobrandedFileExplorer.Configuration.SearchPatterns = new string[] { "*.*" }; //Allow all file
                rdCobrandedFileExplorer.Configuration.MaxUploadFileSize = 10240000 * 5;

                physicalPath = Server.MapPath("~/" + _cobrandedDir); //Create path if this path does not exists
                if (!Directory.Exists(physicalPath))
                    Directory.CreateDirectory(physicalPath);
                rdCobrandedFileExplorer.Configuration.UploadPaths = rdCobrandedFileExplorer.Configuration.ViewPaths = rdCobrandedFileExplorer.Configuration.DeletePaths = new[] { "~/" + _cobrandedDir };

                cbxCobrandedUpload.Visible = true;
                cbxCobrandedUpload.Checked = true;
            }
            else
            {
                cbxCobrandedUpload.Visible = false;
            }
            #endregion

            Page.ClientScript.RegisterClientScriptInclude("jquery", ControlPath + "js/jquery-1.7.2.min.js");

            //Get cache selection
            var  cacheName = string.Format("SelectionCache_{0}_{1}_{2}", UserId, PortalId, TabId);
            _cacheSelection = (CacheSelection)DataCache.GetCache(cacheName);

            #region Load Language & Supplier Exceptions

            _languageExceptions = new List<language>();
            _supplierExceptions = new List<company>();

            if (ModuleRole.IsInRole(UserController.GetCurrentUserInfo(), ADMIN_ROLE))
            {
                //todo: front end / module role / this user can see all languages
                foreach (language lng in CspDataContext.languages.Where(a => a.active.HasValue && a.active.Value))
                {
                    _languageExceptions.Add(lng);
                }
            }
            else if (_contentType.CSPCMS_UserExceptionLanguages.Count(a => a.UserId == UserId) > 0)
            {
                //todo: front end / module role /user language exception list
                foreach (language lng in CspDataContext.languages.Where(a => a.active.HasValue && a.active.Value))
                {
                    if (_contentType.CSPCMS_UserExceptionLanguages.Count(a => a.LanguageId == lng.languages_Id && a.UserId == UserId) > 0)
                        _languageExceptions.Add(lng);
                }
            }
            else if (_contentType.CSPCMS_ExceptionLanguages.Count > 0)
            {
                foreach (language lng in CspDataContext.languages.Where(a => a.active.HasValue && a.active.Value))
                {
                    if (_contentType.CSPCMS_ExceptionLanguages.Count(a => a.LanguageId == lng.languages_Id) > 0)
                        _languageExceptions.Add(lng);
                }
            }
            else
            {
                foreach (language lng in CspDataContext.languages.Where(a => a.active.HasValue && a.active.Value))
                {
                    _languageExceptions.Add(lng);
                }
            }


            //supplier exception
            if (ModuleRole.IsInRole(UserController.GetCurrentUserInfo(), ADMIN_ROLE))
            {
                foreach (company supplier in CspDataContext.companies.Where(a => a.is_supplier.HasValue && a.is_supplier.Value))
                {
                    _supplierExceptions.Add(supplier);
                }
            }
            else if (_contentType.CSPCMS_UserExceptionSuppliers.Count(a => a.UserId == UserId) > 0)
            {
                foreach (CSPCMS_UserExceptionSupplier supplier in _contentType.CSPCMS_UserExceptionSuppliers.Where(a => a.UserId == UserId))
                {
                    _supplierExceptions.Add(CspDataContext.companies.FirstOrDefault(a => a.companies_Id == supplier.SupplierId));
                }
            }
            else if (_contentType.CSPCMS_ExceptionSuppliers.Count > 0)
            {
                foreach (CSPCMS_ExceptionSupplier supplier in _contentType.CSPCMS_ExceptionSuppliers)
                {
                    _supplierExceptions.Add(CspDataContext.companies.FirstOrDefault(a => a.companies_Id == supplier.SupplierId));
                }
            }
            else
            {
                foreach (company supplier in CspDataContext.companies.Where(a => a.is_supplier.HasValue && a.is_supplier.Value))
                {
                    _supplierExceptions.Add(supplier);
                }
            }

            #endregion

            #region Populate data for tool bar

            var ddlLanguages = new RadComboBox
                                           {
                                               ID = "editWindowLanguageDropDownList",
                                               AutoPostBack = true,
											   CausesValidation = false,
											   CssClass = "rcb-lang",
											   OnClientSelectedIndexChanging = "GoToLang",
											   ClientIDMode = ClientIDMode.Static
                                           };
            ddlLanguages.SelectedIndexChanged += DdlLanguagesSelectedIndexChanged;
            foreach (language lng in _languageExceptions)
            {
                ddlLanguages.Items.Add(new RadComboBoxItem(lng.description, lng.languages_Id.ToString()));
            }
            
            if (_keys.ContainsKey(EditContentKeys.LangId) && !IsPostBack) //Set working language if main content has more than one language
            {
                _languageId = int.Parse(_keys[EditContentKeys.LangId]);
                ddlLanguages.SelectedIndex = ddlLanguages.FindItemByValue(_keys[EditContentKeys.LangId]).Index;
            }

            // buttons
            toolBar.Items.Add(new RadToolBarButton
            {
                 Text = GetLocalizedText("EditContentToolBar.Back"),
                 ImageUrl = ControlPath + "images/arrow_left.png",
                 Value = "Back",
                 NavigateUrl = Globals.NavigateURL(TabId,"Content","mid",ModuleId.ToString(), "viewdetail" , "1"),
				 CssClass = "toolbar-back"
            });
            toolBar.Items.Add(new RadToolBarButton { IsSeparator = true });
			toolBar.Items.Add(new RadToolBarButton
			{
				Text = GetLocalizedText("EditContentToolBar.SaveTrigger"),
				ImageUrl = ControlPath + "images/save.png",
				Value = "Save",
				CausesValidation = true,
				Enabled = ModuleRole.IsViewEditRole(UserController.GetCurrentUserInfo()),
				CssClass = "editcontent-btn toolbar-save"
			});
			toolBar.Items.Add(new RadToolBarButton { IsSeparator = true });
			/*
            toolBar.Items.Add(new RadToolBarButton
            {
                 Text = GetLocalizedText("EditContentToolBar.SaveAndBack"),
                 ImageUrl = ControlPath + "images/save.png",
                 Value = "SaveBack",
                 CausesValidation = true,
                 Enabled = ModuleRole.IsViewEditRole(UserController.GetCurrentUserInfo()),
                 CssClass = "editcontent-btn"
            });
            toolBar.Items.Add(new RadToolBarButton{IsSeparator = true});
            toolBar.Items.Add(new RadToolBarButton
            {
                Text = GetLocalizedText("EditContentToolBar.Save"),
                ImageUrl = ControlPath + "images/save.png",
                Value = "Save",
                CausesValidation = true,
                Enabled = ModuleRole.IsViewEditRole(UserController.GetCurrentUserInfo()),
                CssClass = "editcontent-btn"
            });
            toolBar.Items.Add(new RadToolBarButton { IsSeparator = true });
            toolBar.Items.Add(new RadToolBarButton
            {
                Text = GetLocalizedText("EditContentToolBar.SaveAndNew"),
                ImageUrl = ControlPath + "images/save.png",
                Value = "SaveNew",
                CausesValidation = true,
                Enabled = ModuleRole.IsViewEditRole(UserController.GetCurrentUserInfo()),
                CssClass = "editcontent-btn"
            });
			 * */
            var bPublish = new RadToolBarButton
            {
                Text = GetLocalizedText("EditContentToolBar.Publish"),
                ImageUrl = ControlPath + "images/24x24/document_ok.png",
                Value = "Publish",
                CausesValidation = true,
                Enabled = ModuleRole.IsViewEditRole(UserController.GetCurrentUserInfo()),
                CssClass = "editcontent-btn"
            };
            var bPublishContentMain = new RadToolBarButton
                {
                    Text = GetLocalizedText("EditContentToolBar.PublishAll"),
                    ImageUrl = ControlPath + "images/24x24/document_ok.png",
                    Value = "PublishAll",
                    CausesValidation = true,
                    Enabled = ModuleRole.IsViewEditRole(UserController.GetCurrentUserInfo()),
                    CssClass = "editcontent-btn toolbar-publishall"
                };
            var bUnPublishContentMain = new RadToolBarButton
            {
                Text = GetLocalizedText("EditContentToolBar.UnPublishAll"),
                ImageUrl = ControlPath + "images/24x24/document_delete.png",
                Value = "UnPublishAll",
                CausesValidation = true,
                Enabled = ModuleRole.IsViewEditRole(UserController.GetCurrentUserInfo()),
				CssClass = "editcontent-btn toolbar-unpublishall"
            };
            if (_keys[EditContentKeys.Action] != "add")
            {
                toolBar.Items.Add(bPublish);
                toolBar.Items.Add(bPublishContentMain);
                toolBar.Items.Add(bUnPublishContentMain);
            }
            toolBar.Controls.Add(ddlLanguages);

            

            _contentMainId = new Guid(_keys[EditContentKeys.ContentMainId]);
            _languageId = Convert.ToInt32(ddlLanguages.SelectedValue);


            content c = CspDataContext.contents.FirstOrDefault(a => a.content_main_Id == _contentMainId && a.content_types_languages_Id == _languageId);
            if (c != null && c.stage_Id.HasValue)
            {
                _contentId = c.content_Id;
                staging_stage stage = CspDataContext.staging_stages.FirstOrDefault(a => a.description == Common.PublishStage);
                if (stage.staging_stages_Id == c.stage_Id.Value)
                {
                    
                    bPublish.Text = GetLocalizedText("EditContentToolBar.UnPublish");
                    bPublish.ImageUrl = ControlPath + "images/24x24/document_delete.png";
                }
            }
            else
            {
                _contentId = Guid.Empty;
            }

            #region [Vu.dinh 7/29/14 add feature: clone across language]
            //add new toolbar with selected language to clone
            var ddLanguageClone = new RadComboBox
            {
                ID = "LanguageCloneDropDownList",
                AutoPostBack = false,
                CausesValidation = false,
				ClientIDMode = ClientIDMode.Static
            };
            foreach (language lng in _languageExceptions)
            {
                var ct = CspDataContext.contents.FirstOrDefault(a => a.content_main_Id == _contentMainId && a.content_types_languages_Id == lng.languages_Id);
                if (ct != null && lng.languages_Id != _languageId)
                    ddLanguageClone.Items.Add(new RadComboBoxItem(lng.description, lng.languages_Id.ToString()));
            }
            secondToolbar.Controls.Add(ddLanguageClone);
            secondToolbar.Items.Add(new RadToolBarButton
                {
                    Text = GetLocalizedText("EditContentToolBar.Clone"),
                    Value = "Clone",
                    CausesValidation = true,
                    ImageUrl = ControlPath + "images/page_blank_alt.png",
                    CssClass = "editcontent-btn",
                    Enabled = true,
                });
            #endregion

            RenderContent(_contentType, editContent, _contentMainId);

            #region Populate data for general tab

            imageSupplierId.ImageUrl = imageContentIdentifier.ImageUrl = ControlPath + "images/16x16/information.png";
            lbContentIdentifier.Text = GetLocalizedText("EditContent.ContentIdentifier");
            lbSupplierId.Text = GetLocalizedText("EditContent.SupplierId");
            vContentIdentifier.ErrorMessage = GetLocalizedText("Validator.Required");
            foreach (company supplier in _supplierExceptions)
            {
                cbSupplierId.Items.Add(new RadComboBoxItem(supplier.companyname, supplier.companies_Id.ToString()));
            }
            if (_cacheSelection != null) //Set working supllier if main content has more than one language
            {
                cbSupplierId.Items.FindItemByValue(_cacheSelection.SupplierId.ToString()).Selected = true;
            }
            #endregion

            // drop users into General Tab if Add
            // drop users into Field Tab if Edit
            if (_keys[EditContentKeys.Action] == "add")
            {
                tabs.SelectedIndex = pages.SelectedIndex = 0;
            }
            else
            {
                tabs.SelectedIndex = pages.SelectedIndex = 1;
                content_main contentMain = CspDataContext.content_mains.FirstOrDefault(a => a.content_main_Id == _contentMainId);
                if (contentMain != null)
                {
                    tbContentIdentifier.Text = contentMain.content_identifier;
                    RadComboBoxItem supplierItem = cbSupplierId.Items.FindItemByValue(contentMain.supplier_Id.ToString());
                    if (supplierItem != null)
                        supplierItem.Selected = true;
                }
            }

            tbContentIdentifier.Enabled = cbSupplierId.Enabled = ModuleRole.IsViewEditRole(UserController.GetCurrentUserInfo());

            // translation tab names)
            foreach (RadTab tab in tabs.Tabs)
            {
                string s = GetLocalizedText("EditContentTab." + tab.Value);
                tab.Text = !string.IsNullOrEmpty(s) ? s : tab.Text;
            }
            #endregion

            //render category tree (with translated text)
            RenderCategoryTree();

            // check content category
            try
            {
                // edit
                foreach (content_category contentCategory in CspDataContext.content_categories.Where(a => a.content_main_Id == _contentMainId))
                {
                    RadTreeNode node = categoryTree.FindNodeByValue(contentCategory.category_Id.ToString());
                    if (node != null)
                        node.Checked = true;
                }
            }
            catch
            {
                // add
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var ddlLanguages = (RadComboBox)toolBar.FindControl("editWindowLanguageDropDownList");
            _languageId = Convert.ToInt32(ddlLanguages.SelectedValue);


            content c = CspDataContext.contents.FirstOrDefault(a => a.content_main_Id == _contentMainId && a.content_types_languages_Id == _languageId);
            if (c != null)
            {
                _contentId = c.content_Id;
                secondToolbar.Visible = false;
            }
            else
            {
	            if (_contentMainId != Guid.Empty)
	            {
		            secondToolbar.Visible = true;
					showCloneConfirm = "1";
	            }
	            else
	            {
		            secondToolbar.Visible = false;
	            }
            }

            //Translate categories text
            TranslateCategoriesText();
        }

        /// <summary>
        /// Translates the categories text.
        /// </summary>
        /// <remarks>Author - Vu Dinh</remarks>
        private void TranslateCategoriesText()
        {
            if (Settings[Const.SETTING_CATEGORYCONTENT_CT_ID] != null && Settings[Const.SETTING_CATEGORYCONTENT_CT_ID].ToString() != string.Empty)
            {
                foreach (RadTreeNode node in categoryTree.GetAllNodes())
                {
                    var categoryId = int.Parse(node.Value);
                    if (categoryId >= 0)
                    {
                        var categoryContent = CspDataContext.contents.FirstOrDefault(a => a.content_main.content_categories.Any(b => b.category_Id == categoryId) && a.content_main.content_types_Id == int.Parse(Settings[Const.SETTING_CATEGORYCONTENT_CT_ID].ToString()) && a.content_types_languages_Id == _languageId && a.stage_Id == Const.PUBLISHED_STAGES);
                        if (categoryContent != null) //display node text with category content - Content_Title
                        {
                            var contentTitle = CspDataContext.content_fields.FirstOrDefault(a => a.content_Id == categoryContent.content_Id && a.content_types_field.fieldname == "Content_Title");
                            if (contentTitle != null && !string.IsNullOrEmpty(contentTitle.value_text))
                            {
                                //node.Text = contentTitle.value_text;
                                try
                                {
                                    var text = WebUtility.HtmlDecode(contentTitle.value_text);
                                    text = Regex.Replace(text, "<[^>]*(>|$)", "").Replace("\r", "").Replace("\n", "");
                                    node.Text = text;
                                }
                                catch
                                {
                                    node.Text = contentTitle.value_text;
                                }
                            }

                        }
                        else //display node text with original category text
                        {
                            var cat = _listCategories.FirstOrDefault(a => a.categoryId == int.Parse(node.Value));
                            if (cat != null)
                                node.Text = cat.categoryText;
                        }
                    }
                }
            }
        }

        #region Rendering dynamic controls functions

        /// <summary>
        /// Seconds the toolbar_ on button click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="Telerik.Web.UI.RadToolBarEventArgs"/> instance containing the event data.</param>
        /// <author>Vu Dinh</author>
        /// <modified>07/30/2014 15:45:31</modified>
        protected void secondToolbar_OnButtonClick(object sender, RadToolBarEventArgs e)
        {
            switch (e.Item.Value)
            {
                case "Clone":
                    {
                        var ddlLanguages = (RadComboBox)toolBar.FindControl("editWindowLanguageDropDownList");
                        var targetLngId = int.Parse(ddlLanguages.SelectedValue);

                        var cloneLngDropdow = (RadComboBox)secondToolbar.FindControl("LanguageCloneDropDownList") ;
                        var cloneFromLngId = int.Parse(cloneLngDropdow.SelectedValue);

                        var ctFrom =CspDataContext.contents.FirstOrDefault(a =>a.content_main_Id == _contentMainId && a.content_types_languages_Id == cloneFromLngId);
                        if (ctFrom != null)
                        {

                            //create new content
                            var c = new content
                            {
                                active = true,
                                content_main_Id = _contentMainId,
                                stage_Id = (ctFrom.stage_Id ?? 50), // set to publish stage
                                content_types_languages_Id = targetLngId,
                                content_Id = Guid.NewGuid()
                            };
                            CspDataContext.contents.InsertOnSubmit(c);
                            CspDataContext.SubmitChanges();

                            //clone content field value
                            var listCtFromField =  GetContentTypeFields(_contentType, cloneFromLngId.ToString(), ctFrom.content_Id);
                            foreach (var field in listCtFromField)
                            {
                                if (field.DataField != null)
                                {
                                    CspDataContext.content_fields.InsertOnSubmit(new content_field
                                    {
                                        content_Id = c.content_Id,
                                        content_types_fields_Id = field.Field.ContentTypeFieldId,
                                        value_text = field.DataField.value_text
                                        
                                    });
                                }
                                CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                            }


                            var newFieldList = GetContentTypeFields(_contentType, targetLngId.ToString(), c.content_Id);
                            var page = pages.FindPageViewByID("editFieldPage");

                            //re-render content with new field value
                            foreach (ContentTypeField field in newFieldList)
                            {
                                string id = "cf_" + field.Field.CSPCMS_ContentTypeField_Id;
                                Control control = page.FindControl(id);
                                if (control == null) continue;

                                // if code list, then ignore field type
                                if (field.Field.CSPCMS_CodeList != null)
                                {
                                    var comboBox = (RadComboBox)control;
                                    if (field.DataField != null)
                                    {
                                        RadComboBoxItem item = comboBox.FindItemByValue(field.DataField.value_text);
                                        if (item != null)
                                        {
                                            //set all current selected items to false
                                            foreach (RadComboBoxItem cbItem in comboBox.Items.Where(a => a.Selected))
                                                cbItem.Selected = false;
                                            item.Selected = true;
                                        }
                                    }
                                    continue;
                                }

                                string value = field.DataField != null ? field.DataField.value_text : "";
                                Image image;
                                switch (field.Field.ContentTypeFieldType)
                                {
                                    case "Text":
                                        ((RadTextBox)control).Text = value;
                                        break;
                                    case "LongText":
                                        ((RadTextBox)control).Text = value;
                                        break;
                                    case "Html":
                                        //((RadEditor)control).Content = value;
                                        ((DNNRichTextEditControl)control).Value = value;
                                        break;
                                    case "Number":
                                        ((RadNumericTextBox)control).Text = value;
                                        break;
                                    case "File":
                                        /*
                                       var fileUpload = (RadUpload)control;
                                       if (!string.IsNullOrEmpty(fileUpload.Attributes["path"]))
                                           fileUpload.Attributes["path"] = value;
                                       else
                                           fileUpload.Attributes.Add("path", value);
               
                           
                                       var textPv = (Label) page.FindControl("pv_" + id);
                                       if (textPv != null)
                                       {
                                           textPv.Text = (field.DataField != null && !string.IsNullOrEmpty(field.DataField.value_text)) ? "File Url: " + field.DataField.value_text : string.Empty;
                                       }*/
                                        ((TextBox)control).Text = value;
                                        break;
                                    case "Image":
                                        image = (Image)page.FindControl("pv_" + id);
                                        if (string.IsNullOrEmpty(value))
                                            image.Visible = false;
                                        else
                                        {
                                            image.Visible = true;
                                            image.ImageUrl = value;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }

                            //hide clone button
                            secondToolbar.Visible = false;
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// 	language combobox event handler: trigged when language index changed
        /// </summary>
        /// <param name = "sender"></param>
        /// <param name = "e"></param>
        private void DdlLanguagesSelectedIndexChanged(object sender, EventArgs e)
        {
            var ddlLanguages = (RadComboBox)toolBar.FindControl("editWindowLanguageDropDownList");
           _languageId = Convert.ToInt32(ddlLanguages.SelectedValue);

            // update publish/unpublish button
            if (_keys[EditContentKeys.Action] == "edit")
            {

                content c =CspDataContext.contents.FirstOrDefault(a => a.content_main_Id == _contentMainId && a.content_types_languages_Id == _languageId);

                var cloneLngDropdow = secondToolbar.FindControl("LanguageCloneDropDownList") as RadComboBox;
                if (cloneLngDropdow != null)
                {

                    if (c == null)
                    {

                        cloneLngDropdow.Visible = true;
                        cloneLngDropdow.Items.Clear();
                        foreach (language lng in _languageExceptions)
                        {
                            var ct = CspDataContext.contents.FirstOrDefault(a => a.content_main_Id == _contentMainId && a.content_types_languages_Id == lng.languages_Id);
                            if (lng.languages_Id != _languageId && ct != null)
                                cloneLngDropdow.Items.Add(new RadComboBoxItem(lng.description, lng.languages_Id.ToString()));
                        }
                    }
                    else
                    {
                        cloneLngDropdow.Visible = false;
                    }

                }

               
                List<ContentTypeField> fields;
                if (c != null && c.stage_Id.HasValue)
                {
                    var bPublish = (RadToolBarButton) toolBar.FindItemByValue("Publish");
                    staging_stage stage =
                        CspDataContext.staging_stages.FirstOrDefault(a => a.description == Common.PublishStage);
                    if (stage.staging_stages_Id == c.stage_Id.Value)
                    {
                        bPublish.Text = GetLocalizedText("EditContentToolBar.UnPublish");
                        bPublish.ImageUrl = ControlPath + "images/24x24/document_delete.png";
                    }
                    else
                    {
                        bPublish.Text = GetLocalizedText("EditContentToolBar.Publish");
                        bPublish.ImageUrl = ControlPath + "images/24x24/document_ok.png";
                    }
                    fields = GetContentTypeFields(_contentType, ((RadComboBox) sender).SelectedValue, c.content_Id);

                }
                else
                {
                    fields = GetContentTypeFields(_contentType, ((RadComboBox) sender).SelectedValue, Guid.Empty);
                }

                // update content
                RadPageView page = pages.FindPageViewByID("editFieldPage");

                foreach (ContentTypeField field in fields)
                {
                    string id = "cf_" + field.Field.CSPCMS_ContentTypeField_Id;
                    Control control = page.FindControl(id);
                    if (control == null) continue;

                    // if code list, then ignore field type
                    if (field.Field.CSPCMS_CodeList != null)
                    {
                        var comboBox = (RadComboBox) control;
                        if (field.DataField != null)
                        {
                            RadComboBoxItem item = comboBox.FindItemByValue(field.DataField.value_text);
                            if (item != null)
                            {
                                //set all current selected items to false
                                foreach (RadComboBoxItem cbItem in comboBox.Items.Where(a => a.Selected))
                                    cbItem.Selected = false;
                                item.Selected = true;
                            }
                        }
                        continue;
                    }

                    string value = field.DataField != null ? field.DataField.value_text : "";
                    Image image;
                    switch (field.Field.ContentTypeFieldType)
                    {
                        case "Text":
                            ((RadTextBox) control).Text = value;
                            break;
                        case "LongText":
                            ((RadTextBox) control).Text = value;
                            break;
                        case "Html":
                            //((RadEditor)control).Content = value;
                            ((DNNRichTextEditControl) control).Value = value;
                            break;
                        case "Number":
                            ((RadNumericTextBox) control).Text = value;
                            break;
                        case "File":
                            /*
                           var fileUpload = (RadUpload)control;
                           if (!string.IsNullOrEmpty(fileUpload.Attributes["path"]))
                               fileUpload.Attributes["path"] = value;
                           else
                               fileUpload.Attributes.Add("path", value);
               
                           
                           var textPv = (Label) page.FindControl("pv_" + id);
                           if (textPv != null)
                           {
                               textPv.Text = (field.DataField != null && !string.IsNullOrEmpty(field.DataField.value_text)) ? "File Url: " + field.DataField.value_text : string.Empty;
                           }*/
                            ((TextBox) control).Text = value;
                            break;
                        case "Image":
                            image = (Image) page.FindControl("pv_" + id);
                            if (string.IsNullOrEmpty(value))
                                image.Visible = false;
                            else
                            {
                                image.Visible = true;
                                image.ImageUrl = value;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// 	render a form with the pre-defined fields and fill data with Csp data
        /// </summary>
        /// <param name = "contentType">content type</param>
        /// <param name = "container">parent container</param>
        /// <param name = "contentMainId"></param>
        private void RenderContent(CSPCMS_ContentType contentType, Control container, object contentMainId)
        {
            var selectedLanguage = (RadComboBox)toolBar.FindControl("editWindowLanguageDropDownList");

            List<ContentTypeField> fields = GetContentTypeFields(contentType, selectedLanguage.SelectedValue, _contentId);

            var groupPanel = new Panel();
            groupPanel.CssClass = "content-field-group";

            var tbl = new Table { Width = new Unit("100%") };
            groupPanel.Controls.Add(tbl);

            bool isEnabled = ModuleRole.IsViewEditRole(UserController.GetCurrentUserInfo());

            foreach (ContentTypeField field in fields)
            {
                if (!field.Field.Visible) continue;

                CSPCMS_Translation translation = field.Field.CSPCMS_Translations.SingleOrDefault(a => a.CultureCode == CurrentLanguage);

                if (groupPanel.Attributes["gid"] != field.Field.DisplayGroupOrder.ToString())
                {
                    if (groupPanel.Controls.Count == 0)
                        groupPanel.Visible = false;

                    groupPanel = new Panel();
                    groupPanel.CssClass = "content-field-group";
                    groupPanel.Attributes["gid"] = field.Field.DisplayGroupOrder.ToString();
                    container.Controls.Add(groupPanel);
                    tbl = new Table { Width = new Unit("100%") };
                    groupPanel.Controls.Add(tbl);
                }

                var tr = new TableRow();
                tr.VerticalAlign = VerticalAlign.Top;
                var tc = new TableCell();
                tbl.Rows.Add(tr);

                //help icon
                tc = new TableCell();
                tc.Width = new Unit(24);
                tc.Controls.Add(new ImageButton { ImageUrl = ControlPath + "images/16x16/information.png", TabIndex = -1, CausesValidation = false, ID = "hover" + field.Field.CSPCMS_ContentTypeField_Id });
                tc.Controls.Add(new RadToolTip { TargetControlID = "hover" + field.Field.CSPCMS_ContentTypeField_Id, Text = translation != null ? translation.LongDesc : field.Field.ContentTypeFieldName, RelativeTo = ToolTipRelativeDisplay.Element });
                tr.Cells.Add(tc);
                //label
                tc = new TableCell();
                tc.Attributes.Add("nowrap", "nowrap");
                tc.Width = new Unit("15%");
                tc.Controls.Add(new Label { Text = translation != null ? translation.ShortDesc : field.Field.ContentTypeFieldName.Replace("_", " ") });
                tr.Cells.Add(tc);
                //field
                tc = new TableCell();
                string id = "cf_" + field.Field.CSPCMS_ContentTypeField_Id;

                //todo: front end / render content / write custom js validation (asp.net validators throw errors on language changed)

                // if code list, then ignore field type
                if (field.Field.CSPCMS_CodeList != null)
                {
                    var comboBox = new RadComboBox();
                    comboBox.Enabled = !field.Field.Readonly;
                    comboBox.ID = id;
                    foreach (CSPCMS_CodeListValue codeListValue in field.Field.CSPCMS_CodeList.CSPCMS_CodeListValues.OrderBy(a => a.DisplayOrder))
                    {
                        comboBox.Items.Add(new RadComboBoxItem
                                               {
                                                   Text = codeListValue.Name,
                                                   Value = codeListValue.Value,
                                                   Selected = field.DataField != null && field.DataField.value_text == codeListValue.Value,
                                                   Enabled = isEnabled
                                               });
                    }
                    tc.Controls.Add(comboBox);
                    tr.Cells.Add(tc);
                    continue;
                }

                // render fields with field types
                string imagePath = "";
                Image image;
                switch (field.Field.ContentTypeFieldType)
                {
                    case "Text":
                        tc.Controls.Add(new RadTextBox
                                            {
                                                ID = id,
                                                Enabled = !field.Field.Readonly && isEnabled,
                                                Text = field.DataField != null ? field.DataField.value_text : "",
                                                Width = new Unit("50%")
                                            });
                        if (field.Field.Required)
                        {
                            var validator = new RequiredFieldValidator
                                                {
                                                    ControlToValidate = id,
                                                    Display = ValidatorDisplay.Dynamic,
                                                    ErrorMessage = GetLocalizedText("Validator.Required"),
                                                    EnableClientScript = false
                                                };
                            tc.Controls.Add(validator);
                        }
                        break;
                    case "LongText":
                        tc.Controls.Add(new RadTextBox
                                            {
                                                ID = id,
                                                TextMode = InputMode.MultiLine,
                                                Rows = 5,
                                                Width = new Unit("90%"),
                                                Enabled = !field.Field.Readonly && isEnabled,
                                                Text = field.DataField != null ? field.DataField.value_text : ""
                                            });
                        if (field.Field.Required)
                        {
                            var validator = new RequiredFieldValidator
                                                {
                                                    ControlToValidate = id,
                                                    Display = ValidatorDisplay.Dynamic,
                                                    ErrorMessage = GetLocalizedText("Validator.Required"),
                                                    EnableClientScript = false
                                                };
                            tc.Controls.Add(validator);
                        }
                        break;
                    case "Html":
                        var editorPanel = new Panel
                                              {
                                                  Width = new Unit("100%"),
                                                  Height = new Unit(300)
                                              };
                        editorPanel.Style.Add("display", "");
                        editorPanel.Controls.Add(new DNNRichTextEditControl
                                                     {
                                                         ID = id,
                                                         Width = new Unit("700"),
                                                         Visible = true,
                                                         Enabled = !field.Field.Readonly && isEnabled,
                                                         Height = new Unit("350"),
                                                         Value = field.DataField != null ? field.DataField.value_text : "",
                                                         CssClass = "dnnEditor",
                                                         BackColor = Color.White
                                                     });
                        tc.Controls.Add(editorPanel);
                        break;
                    case "Number":
                        tc.Controls.Add(new RadNumericTextBox
                                            {
                                                ID = id,
                                                Enabled = !field.Field.Readonly && isEnabled,
                                                Text = field.DataField != null ? field.DataField.value_text : ""
                                            });
                        if (field.Field.Required)
                        {
                            var validator = new RequiredFieldValidator
                                                {
                                                    ControlToValidate = id,
                                                    Display = ValidatorDisplay.Dynamic,
                                                    ErrorMessage = GetLocalizedText("Validator.Required"),
                                                    EnableClientScript = false
                                                };
                            tc.Controls.Add(validator);
                        }
                        break;
                    case "File":
                        var filePath = field.DataField != null && !string.IsNullOrEmpty(field.DataField.value_text)? field.DataField.value_text: string.Empty;
                        tc.Controls.Add(new TextBox
                            {
                                ID = id,
                                Enabled =  !field.Field.Readonly && isEnabled,
                                Text = filePath,
                                Width = new Unit("50%")
                            });
                        tc.Controls.Add(new RadButton
                            {
                                ID = "btn" + id,
                                Enabled = !field.Field.Readonly && isEnabled,
                                Text = "Choose file",
                                CssClass = "openFileExplorer",
                                AutoPostBack = false,
                            });
                        break;
                    case "Image":
                        imagePath = field.DataField != null ? field.DataField.value_text : "";
                        tc.Controls.Add(new TextBox
                                            {
                                                ID = id,
                                                Enabled = !field.Field.Readonly && isEnabled,
                                                Text = imagePath,
                                                Width = new Unit("50%")
                                            });
                        tc.Controls.Add(new RadButton
                                            {
                                                ID = "btn" + id,
                                                Enabled = !field.Field.Readonly && isEnabled,
                                                Text = "Choose file",
                                                CssClass = "openImgExplorer",
                                                AutoPostBack = false
                                            });
                        //Add break - DLV
                        tc.Controls.Add(new LiteralControl("<br />"));
                        imagePath = field.DataField != null ? field.DataField.value_text : "";
                        image = new Image { ID = "pv_" + id, ImageUrl = imagePath, Width = new Unit(100), AlternateText = imagePath, };
                        tc.Controls.Add(image);
                        if (string.IsNullOrEmpty(imagePath))
                        {
                            image.Visible = false;
                        }
                        break;
                    default:
                        tc.Controls.Add(new RadTextBox
                                            {
                                                ID = id,
                                                Enabled = !field.Field.Readonly && isEnabled,
                                                Text = field.DataField != null ? field.DataField.value_text : ""
                                            });
                        if (field.Field.Required)
                        {
                            var validator = new RequiredFieldValidator
                                                {
                                                    ControlToValidate = id,
                                                    Display = ValidatorDisplay.Dynamic,
                                                    ErrorMessage = GetLocalizedText("Validator.Required"),
                                                    EnableClientScript = false
                                                };
                            tc.Controls.Add(validator);
                        }
                        break;
                }

                tr.Cells.Add(tc);
            }
            container.Controls.Add(groupPanel);
        }


        /// <summary>
        /// get content for the requested content main id and language id
        /// </summary>
        /// <param name="contentType">current content type</param>
        /// <param name="lngId">requested language id</param>
        /// <param name="contentMainId">requested content main id</param>
        /// <param name="contentId">The content id.</param>
        /// <returns>List{ContentTypeField}.</returns>
        private List<ContentTypeField> GetContentTypeFields(CSPCMS_ContentType contentType, string lngId, Guid contentId)
        {
            var result = new List<ContentTypeField>();

            try
            {


                content_field[] fields = (from cf in CspDataContext.content_fields
                                          join c in CspDataContext.contents on cf.content_Id equals c.content_Id
                                          where c.content_main_Id == _contentMainId && c.content_types_languages_Id == Convert.ToInt32(lngId) && c.content_Id == contentId
                                          select cf).ToArray();
                foreach (CSPCMS_ContentTypeField field in contentType.CSPCMS_ContentTypeFields.Where(a => a.Visible).OrderBy(a => a.DisplayOrder).OrderBy(a => a.DisplayGroupOrder))
                {
                    result.Add(new ContentTypeField
                                   {
                                       Field = field,
                                       DataField = fields.SingleOrDefault(a => a.content_types_fields_Id == field.ContentTypeFieldId)
                                   });
                }
            }
            catch (Exception ex)
            {
                foreach (CSPCMS_ContentTypeField field in contentType.CSPCMS_ContentTypeFields.OrderBy(a => a.DisplayOrder).OrderBy(a => a.DisplayGroupOrder))
                {
                    result.Add(new ContentTypeField
                                   {
                                       Field = field,
                                       DataField = null
                                   });
                }
            }


            return result;
        }

        #endregion

        #region Events
		/// <summary>
		/// Does the un publish all.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void DoUnPublishAll(object sender, EventArgs e)
		{
			PublishUnPublishAllAction(false);
		}

		/// <summary>
		/// Does the publish all.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void DoPublishAll(object sender, EventArgs e)
		{
			PublishUnPublishAllAction(true);
		}
		/// <summary>
		/// Clones from language.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void CloneFromLanguage(object sender, EventArgs e)
		{
            var ddlLanguages = (RadComboBox)toolBar.FindControl("editWindowLanguageDropDownList");
            var targetLngId = int.Parse(ddlLanguages.SelectedValue);

            var cloneLngDropdow = (RadComboBox)secondToolbar.FindControl("LanguageCloneDropDownList") ;
            var cloneFromLngId = int.Parse(cloneLngDropdow.SelectedValue);

            var ctFrom =CspDataContext.contents.FirstOrDefault(a =>a.content_main_Id == _contentMainId && a.content_types_languages_Id == cloneFromLngId);
            if (ctFrom != null)
            {
	            showCloneConfirm = "0";
                //create new content
                var c = new content
                {
                    active = true,
                    content_main_Id = _contentMainId,
                    stage_Id = (ctFrom.stage_Id ?? 50), // set to publish stage
                    content_types_languages_Id = targetLngId,
                    content_Id = Guid.NewGuid()
                };
                CspDataContext.contents.InsertOnSubmit(c);
                CspDataContext.SubmitChanges();

                //clone content field value
                var listCtFromField =  GetContentTypeFields(_contentType, cloneFromLngId.ToString(), ctFrom.content_Id);
                foreach (var field in listCtFromField)
                {
                    if (field.DataField != null)
                    {
                        CspDataContext.content_fields.InsertOnSubmit(new content_field
                        {
                            content_Id = c.content_Id,
                            content_types_fields_Id = field.Field.ContentTypeFieldId,
                            value_text = field.DataField.value_text
                                        
                        });
                    }
                    CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                }


                var newFieldList = GetContentTypeFields(_contentType, targetLngId.ToString(), c.content_Id);
                var page = pages.FindPageViewByID("editFieldPage");

                //re-render content with new field value
                foreach (ContentTypeField field in newFieldList)
                {
                    string id = "cf_" + field.Field.CSPCMS_ContentTypeField_Id;
                    Control control = page.FindControl(id);
                    if (control == null) continue;

                    // if code list, then ignore field type
                    if (field.Field.CSPCMS_CodeList != null)
                    {
                        var comboBox = (RadComboBox)control;
                        if (field.DataField != null)
                        {
                            RadComboBoxItem item = comboBox.FindItemByValue(field.DataField.value_text);
                            if (item != null)
                            {
                                //set all current selected items to false
                                foreach (RadComboBoxItem cbItem in comboBox.Items.Where(a => a.Selected))
                                    cbItem.Selected = false;
                                item.Selected = true;
                            }
                        }
                        continue;
                    }

                    string value = field.DataField != null ? field.DataField.value_text : "";
                    Image image;
                    switch (field.Field.ContentTypeFieldType)
                    {
                        case "Text":
                            ((RadTextBox)control).Text = value;
                            break;
                        case "LongText":
                            ((RadTextBox)control).Text = value;
                            break;
                        case "Html":
                            //((RadEditor)control).Content = value;
                            ((DNNRichTextEditControl)control).Value = value;
                            break;
                        case "Number":
                            ((RadNumericTextBox)control).Text = value;
                            break;
                        case "File":
                            /*
                            var fileUpload = (RadUpload)control;
                            if (!string.IsNullOrEmpty(fileUpload.Attributes["path"]))
                                fileUpload.Attributes["path"] = value;
                            else
                                fileUpload.Attributes.Add("path", value);
               
                           
                            var textPv = (Label) page.FindControl("pv_" + id);
                            if (textPv != null)
                            {
                                textPv.Text = (field.DataField != null && !string.IsNullOrEmpty(field.DataField.value_text)) ? "File Url: " + field.DataField.value_text : string.Empty;
                            }*/
                            ((TextBox)control).Text = value;
                            break;
                        case "Image":
                            image = (Image)page.FindControl("pv_" + id);
                            if (string.IsNullOrEmpty(value))
                                image.Visible = false;
                            else
                            {
                                image.Visible = true;
                                image.ImageUrl = value;
                            }
                            break;
                        default:
                            break;
                    }
                }

				_languageId = targetLngId;
				string queryString = string.Format("cmId={0}&ctId={1}&action=edit&langid={2}&cid={3}",
												   _contentMainId,
												   _contentType.CSPCMS_ContentType_Id,
												   _languageId,
												   _contentId
					);

				string hash = Common.Encode(queryString);
				Response.Redirect(
					Globals.NavigateURL(TabId, "EditContent", "mid=" + ModuleId + "&hash=" + HttpUtility.UrlEncode(hash)), false);
            }
        }

		/// <summary>
		/// Saves then go back.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void SaveGoBack(object sender, EventArgs e)
		{
			if (SaveContentAction())
			{
				Response.Redirect(
					Globals.NavigateURL(TabId, "Content", "mid", ModuleId.ToString(), "viewdetail", "1"), true);
			}
		}

		/// <summary>
		/// Saves and go to lang.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void SaveGoToLang(object sender, EventArgs e)
		{
			if (SaveContentAction())
			{
				_languageId = Convert.ToInt32(TargetLang.Value);
				string queryString = string.Format("cmId={0}&ctId={1}&action=edit&langid={2}&cid={3}",
				                                   _contentMainId,
				                                   _contentType.CSPCMS_ContentType_Id,
				                                   _languageId,
				                                   _contentId
					);

				string hash = Common.Encode(queryString);
				Response.Redirect(
					Globals.NavigateURL(TabId, "EditContent", "mid=" + ModuleId + "&hash=" + HttpUtility.UrlEncode(hash)), false);
			}
		}

		/// <summary>
		/// Goes to language without save.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		/// <exception cref="System.NotImplementedException"></exception>
		protected void GoToLang(object sender, EventArgs e)
		{
			_languageId = Convert.ToInt32(TargetLang.Value);
			string queryString = string.Format("cmId={0}&ctId={1}&action=edit&langid={2}&cid={3}",
								  _contentMainId,
								   _contentType.CSPCMS_ContentType_Id,
								   _languageId,
								   _contentId
								   );

			string hash = Common.Encode(queryString);
			Response.Redirect(Globals.NavigateURL(TabId, "EditContent", "mid=" + ModuleId + "&hash=" + HttpUtility.UrlEncode(hash)), false);
		}

		/// <summary>
		/// Saves the content.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void SaveContent(object sender, EventArgs e)
		{
			if (SaveContentAction())
			{
				DateTime nx = new DateTime(1970, 1, 1); // UNIX epoch date
				TimeSpan ts = DateTime.UtcNow - nx;
				string queryString = string.Format("cmId={0}&ctId={1}&action=edit&langid={2}&cid={3}&saved={4}",
									  _contentMainId,
									   _contentType.CSPCMS_ContentType_Id,
									   _languageId,
									   _contentId,
									   DateTime.UtcNow
			   .Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))
			   .TotalMilliseconds //Get Unix Time Stamp
									   );

				string hash = Common.Encode(queryString);
				Response.Redirect(Globals.NavigateURL(TabId, "EditContent", "mid=" + ModuleId + "&hash=" + HttpUtility.UrlEncode(hash)), false);
			}
		}

		/// <summary>
		/// Saves and go back.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void SaveBack(object sender, EventArgs e)
		{
			if (SaveContentAction())
				Response.Redirect(Globals.NavigateURL(TabId, "Content", "mid", ModuleId.ToString(), "viewdetail", "1"));
		}

		/// <summary>
		/// Saves the new.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void SaveNew(object sender, EventArgs e)
		{
			if (SaveContentAction())
			{
				string queryString = string.Format("cmId={0}&ctId={1}&action=add&langid={2}",
									  Guid.Empty,
									   _contentType.CSPCMS_ContentType_Id,
									   _keys.ContainsKey(EditContentKeys.LangId) ? _keys[EditContentKeys.LangId] : string.Empty
									   );

				string hash = Common.Encode(queryString);
				Response.Redirect(Globals.NavigateURL(TabId, "EditContent", "mid=" + ModuleId + "&hash=" + HttpUtility.UrlEncode(hash)), false);
			}
		}

        /// <summary>
        /// 	handle toolbar click events for Save / Publish / Unpublish content
        /// </summary>
        /// <param name = "sender">Toolbar</param>
        /// <param name = "e">Toolbar Onclick event</param>
        protected void ToolBarClickEventHandler(object sender, RadToolBarEventArgs e)
        {
            switch (e.Item.Value)
            {
                case "Save":
                    if (SaveContentAction())
                    {
                        string queryString = string.Format("cmId={0}&ctId={1}&action=edit&langid={2}&cid={3}",
                                              _contentMainId,
                                               _contentType.CSPCMS_ContentType_Id,
                                               _languageId,
                                               _contentId
                                               );

                        string hash = Common.Encode(queryString);
                        Response.Redirect(Globals.NavigateURL(TabId, "EditContent", "mid=" + ModuleId + "&hash=" + HttpUtility.UrlEncode(hash)), false);
                    }
                    break;
                case "SaveBack": //save and go back to main page
                    if (SaveContentAction())
                        Response.Redirect(Globals.NavigateURL(TabId,"Content","mid", ModuleId.ToString(),"viewdetail","1"));
                    break;
                case "SaveNew" : //save and open new form
                    if(SaveContentAction())
                    {
                        string queryString = string.Format("cmId={0}&ctId={1}&action=add&langid={2}",
                                              Guid.Empty,
                                               _contentType.CSPCMS_ContentType_Id,
                                               _keys.ContainsKey(EditContentKeys.LangId) ? _keys[EditContentKeys.LangId] : string.Empty
                                               );

                        string hash = Common.Encode(queryString);
                        Response.Redirect(Globals.NavigateURL(TabId, "EditContent", "mid=" + ModuleId + "&hash=" + HttpUtility.UrlEncode(hash)), false);
                    } 
                    break;
                case "Publish":
                   PublishUnPublishAction();
                    break;
                case "PublishAll":
                    PublishUnPublishAllAction(true);
                    break;
                case "UnPublishAll":
                    PublishUnPublishAllAction(false);
                    break;
            }
        }

        /// <summary>
        /// Publish all - UnPublish all Action.
        /// </summary>
        private void PublishUnPublishAllAction(bool isPublish)
        {
            var bPublish = (RadToolBarButton)toolBar.FindItemByValue("Publish");


            staging_stage publishStage = CspDataContext.staging_stages.SingleOrDefault(a => a.description == Common.PublishStage);
            staging_stage unpublishStage = CspDataContext.staging_stages.SingleOrDefault(a => a.description == Common.UnPublishStage);

            foreach (content c in CspDataContext.contents.Where(a => a.content_main_Id == _contentMainId)) //get all content of current content main
            {
                if (isPublish && c.stage_Id.HasValue && c.stage_Id != publishStage.staging_stages_Id)
                {
                    c.stage_Id = publishStage.staging_stages_Id;
                    bPublish.Text = GetLocalizedText("EditContentToolBar.UnPublish");
                    bPublish.ImageUrl = ControlPath + "images/24x24/document_delete.png";
                }
                if (!isPublish && c.stage_Id.HasValue && c.stage_Id != unpublishStage.staging_stages_Id)
                {
                    c.stage_Id = unpublishStage.staging_stages_Id;
                    bPublish.Text = GetLocalizedText("EditContentToolBar.Publish");
                    bPublish.ImageUrl = ControlPath + "images/24x24/document_ok.png";
                }
            }
            CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
        }

        /// <summary>
        /// Publish - Unpublish Action.
        /// </summary>
        /// Author: Vu Dinh
        /// 1/24/2013 - 7:20 PM
        private void PublishUnPublishAction()
        {
            var selectedLanguage = (RadComboBox)toolBar.FindControl("editWindowLanguageDropDownList");
            int lngId = Convert.ToInt32(selectedLanguage.SelectedValue);

            staging_stage publishStage = CspDataContext.staging_stages.SingleOrDefault(a => a.description == Common.PublishStage);
            staging_stage unpublishStage = CspDataContext.staging_stages.SingleOrDefault(a => a.description == Common.UnPublishStage);
           
            // create unpublish stage is not available
            if (unpublishStage == null)
            {
                // note:  hard-coded values for staging stage
                CspDataContext.staging_stages.InsertOnSubmit(new staging_stage
                {
                    staging_Id = 1,
                    level = 40,
                    description = Common.UnPublishStage,
                    action = 4,
                    language_specific = false,
                    role_Id = Guid.Empty
                });
                CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                unpublishStage = CspDataContext.staging_stages.SingleOrDefault(a => a.description == Common.UnPublishStage);
            }

            var bPublish = (RadToolBarButton)toolBar.FindItemByValue("Publish");
            foreach (content c in CspDataContext.contents.Where(a => a.content_main_Id == _contentMainId
                                                                            && a.content_types_languages_Id == lngId))
            {
                if (!c.stage_Id.HasValue || c.stage_Id == unpublishStage.staging_stages_Id)
                {
                    c.stage_Id = publishStage.staging_stages_Id;
                    bPublish.Text = GetLocalizedText("EditContentToolBar.UnPublish");
                    bPublish.ImageUrl = ControlPath + "images/24x24/document_delete.png";
                }
                else
                {
                    c.stage_Id = unpublishStage.staging_stages_Id;
                    bPublish.Text = GetLocalizedText("EditContentToolBar.Publish");
                    bPublish.ImageUrl = ControlPath + "images/24x24/document_ok.png";
                }
            }
            CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
        }

        /// <summary>
        /// Saves the content action.
        /// </summary>
        /// Author: Vu Dinh
        /// 1/24/2013 - 7:18 PM
        private bool SaveContentAction()
        {
            var selectedLanguage = (RadComboBox)toolBar.FindControl("editWindowLanguageDropDownList");
            int lngId = Convert.ToInt32(selectedLanguage.SelectedValue);
            content_main contentMain;
            staging_stage stage = null; // place hold for publish / unpublish
            Image image; // review image holder
            string id; // control id holder



            List<ContentTypeField> fields = GetContentTypeFields(_contentType, selectedLanguage.SelectedValue, _contentId);
            string value = ""; // place holder for value
            Control control; // place holder for control
            CSPCMS_Translation translation;
            bool validated = true;
            content contentPerSelectedLng = null;

            // validate
            string errorMessages = "";

            #region Validate Content Fields

            if (string.IsNullOrEmpty(tbContentIdentifier.Text))
            {
                errorMessages = AppendToErrorMessage("[Content Identifier] " + GetLocalizedText("Validator.Required"), errorMessages);
                validated = false;
            }

            if (_supplierExceptions.Count(a => a.companies_Id == Convert.ToInt32(cbSupplierId.SelectedValue)) == 0)
            {
                errorMessages = AppendToErrorMessage("[Supplier] " + GetLocalizedText("Validator.FailValidation"), errorMessages);
                validated = false;
            }
            if (categoryTree.CheckedNodes.Count == 0)
            {
                errorMessages = AppendToErrorMessage("[Category] " + GetLocalizedText("Validator.NeedCategory"), errorMessages);
                validated = false;
            }

            foreach (ContentTypeField field in fields)
            {
                id = "cf_" + field.Field.CSPCMS_ContentTypeField_Id;
                control = editContent.FindControl(id);
                if (control == null)
                    continue;
                value = ""; // reset value
                translation = field.Field.CSPCMS_Translations.FirstOrDefault(a => a.CultureCode == CurrentLanguage);

                if (field.Field.CSPCMS_CodeList != null)
                    value = ((RadComboBox)control).SelectedValue;
                else
                {
                    switch (field.Field.ContentTypeFieldType)
                    {
                        case "Text":
                            value = ((RadTextBox)control).Text;
                            break;
                        case "LongText":
                            value = ((RadTextBox)control).Text;
                            break;
                        case "Html":
                            //value = ((RadEditor)control).Content;
                            value = ((DNNRichTextEditControl) control).Value.ToString();
                            break;
                        case "Number":
                            value = ((RadNumericTextBox)control).Text;
                            break;
                        case "File":
                            value = ((TextBox) control).Text;
                            break;
                        default:
                            break;
                    }
                }

                if (field.Field.Required && string.IsNullOrEmpty(value))
                {
                    if (translation != null)
                    {
                        errorMessages = AppendToErrorMessage("[" + translation.ShortDesc + "] " + GetLocalizedText("Validator.Required"), errorMessages);
                    }
                    else
                    {
                        errorMessages = AppendToErrorMessage("[" + field.Field.ContentTypeFieldName + "] " + GetLocalizedText("Validator.Required"), errorMessages);
                    }
                    validated = false;
                }

                if (field.Field.CSPCMS_Validation != null && (!string.IsNullOrEmpty(value) || field.Field.Required) && !Regex.IsMatch(value ?? "", field.Field.CSPCMS_Validation.ValidationRule))
                //if (field.Field.CSPCMS_Validation != null && !System.Text.RegularExpressions.Regex.IsMatch(value ?? "", field.Field.CSPCMS_Validation.ValidationRule))
                {
                    if (translation != null)
                    {
                        errorMessages = AppendToErrorMessage("[" + translation.ShortDesc + "] " + GetLocalizedText("Validator.FailValidation"), errorMessages);
                    }
                    else
                    {
                        errorMessages = AppendToErrorMessage("[" + field.Field.ContentTypeFieldName + "] " + GetLocalizedText("Validator.FailValidation"), errorMessages);
                    }

                    validated = false;
                }
            }


            if (!validated)
            {
                notification.Title = "Error";
                notification.Show(errorMessages);
                return false; // fail validation
            }

            #endregion

            #region Save Content Fields

            if (_keys[EditContentKeys.Action] == "add")
            {
                contentMain = new content_main
                {
                    content_main_Id = Guid.NewGuid(),
                    content_main_key = tbContentIdentifier.Text,
                    content_identifier = tbContentIdentifier.Text,
                    content_types_Id = _contentType.ContentTypeId,
                    supplier_Id = Convert.ToInt32(cbSupplierId.SelectedValue),
                    consumer_Id = 0,
                    content_staging_scheme_Id = 1
                };
                CspDataContext.content_mains.InsertOnSubmit(contentMain);
                _contentMainId = contentMain.content_main_Id;
            }
            else
            {
                contentMain = CspDataContext.content_mains.FirstOrDefault(a => a.content_main_Id == _contentMainId);
                if (contentMain.content_identifier != tbContentIdentifier.Text)
                    contentMain.content_identifier = contentMain.content_main_key = tbContentIdentifier.Text;
                if (contentMain.supplier_Id != Convert.ToInt32(cbSupplierId.SelectedValue))
                    contentMain.supplier_Id = Convert.ToInt32(cbSupplierId.SelectedValue);
            }
            CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
            _contentMainId = contentMain.content_main_Id;

            //check if content exists: DLV
            contentPerSelectedLng = CspDataContext.contents.FirstOrDefault(a => a.content_main_Id == _contentMainId && a.content_types_languages_Id == lngId);
            if (contentPerSelectedLng == null)
            {
                stage = CspDataContext.staging_stages.FirstOrDefault(a => a.description == Common.PublishStage);
                // content doesn't exist in seletction language, create content
                var c = new content
                {
                    active = true,
                    content_main_Id = _contentMainId,
                    stage_Id = (stage != null ? stage.staging_stages_Id : 50), // set to publish stage
                    content_types_languages_Id = lngId,
                    content_Id = Guid.NewGuid()
                };
                CspDataContext.contents.InsertOnSubmit(c);
                CspDataContext.SubmitChanges();
                contentPerSelectedLng = c;
                _contentId = c.content_Id;
            }


            foreach (ContentTypeField field in fields)
            {
                id = "cf_" + field.Field.CSPCMS_ContentTypeField_Id;
                control = editContent.FindControl(id);
                if (control == null)
                    continue;
                value = ""; // reset value
                if (field.Field.CSPCMS_CodeList != null)
                    value = ((RadComboBox)control).SelectedValue;
                else
                {
                    switch (field.Field.ContentTypeFieldType)
                    {
                        case "Text":
                            value = ((RadTextBox)control).Text;
                            break;
                        case "LongText":
                            value = ((RadTextBox)control).Text;
                            break;
                        case "Html":
                            //value = ((RadEditor)control).Content;
                            value = ((DNNRichTextEditControl)control).Value.ToString();
                            if (_staticDomain != string.Empty)
                            {
                                var stringToReplace = HttpContext.Current.Request.ApplicationPath;
                                value = value.Replace("src=\"" +stringToReplace , "src=\"" + _staticDomain);
                            }


                            break;
                        case "Number":
                            value = ((RadNumericTextBox)control).Text;
                            break;
                        case "File":
                            value = ((TextBox)control).Text;
                            if (string.IsNullOrEmpty(value) || value.StartsWith("http") || value.StartsWith("/d9.aspx"))
                            {
                                //do nothing
                            }
                            else
                            {
                                if (!value.StartsWith("/"))
                                {
                                    value = "/" + value;
                                }

                                var strTorep = HttpContext.Current.Request.ApplicationPath;

                                //if upload file pdf to cobranded folder, then use url eg: /d9.aspx?file=webroot/WSAB-WSS_DS_partner_us.pdf
                                if (!string.IsNullOrEmpty(_cobrandedDir) && value.IndexOf(_cobrandedDir, StringComparison.OrdinalIgnoreCase) != -1 &&
                                    value.IndexOf(".pdf", StringComparison.OrdinalIgnoreCase) != -1)
                                {
                                    var fileName = value.Split(new string[] {"/"}, StringSplitOptions.RemoveEmptyEntries).Last();
                                    value = string.Format("/d9.aspx?file={0}/{1}", _instanceName, fileName);
                                }
                                else
                                {
                                    if (_staticDomain != string.Empty)
                                    {
                                        if (strTorep.StartsWith("/"))
                                        {
                                            strTorep = strTorep.Remove(0, 1);
                                        }
                                        if (value.StartsWith("/" + strTorep))
                                        {
                                            value = value.Replace("/" + strTorep, _staticDomain);
                                        }
                                        else
                                        {
                                            value = _staticDomain + value;
                                        }
                                    }
                                    else
                                        value = strTorep + value;
                                }
                                
                            }
                            break;
                           
                            /*
                            var fileUpload = ((RadUpload)control);
                            if (fileUpload.UploadedFiles.Count == 0)
                                continue;
                            var uploadDir = _fileRoot;
                            var useCobrandedDir = (CheckBox)pages.FindControl("cb_" + id);
                            if (useCobrandedDir != null && useCobrandedDir.Checked)
                            {
                                uploadDir = _cobrandedDir;
                            }

                            foreach (UploadedFile uploadedFile in fileUpload.UploadedFiles)
                            {
                                string path;
                                if (!string.IsNullOrEmpty(uploadDir))
                                {
                                    string fileRoot = string.Format("~/{0}", uploadDir);
                                    path = ResolveUrl(fileRoot + "/");
                                }
                                else
                                {
                                    path = ResolveUrl("~/UploadImages/Portal" + PortalId + "/" + _contentMainId + "/");
                                }

                                string fileExt = Path.GetExtension(uploadedFile.FileName);
                                string fileName = DateTime.Now.Ticks + fileExt;
                                string physicalPath = Request.MapPath(path, Request.ApplicationPath, false);
                                if (!Directory.Exists(physicalPath))
                                    Directory.CreateDirectory(physicalPath);

                                uploadedFile.SaveAs(Request.MapPath(path + fileName, Request.ApplicationPath, false));

                                if (string.IsNullOrEmpty(uploadDir))
                                {
                                    if (_staticDomain != string.Empty)
                                    {
                                        value = _staticDomain + ResolveUrl("~/UploadImages/Portal" + PortalId + "/" + _contentMainId + "/");
                                    }
                                    else
                                    {
                                        String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                                        String strUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, string.Empty);
                                        value = strUrl + ResolveUrl("~/UploadImages/Portal" + PortalId + "/" + _contentMainId + "/");
                                    }
                                }
                                else
                                {
                                    if (_staticDomain != string.Empty)
                                    {
                                        value = _staticDomain + "/" + uploadDir + "/";
                                    }
                                    else
                                    {
                                        String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                                        String strUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, string.Empty);
                                        value = strUrl + ResolveUrl("~/" + _fileRoot + "/");
                                    }
                                }

                                value += fileName;
                            }
                             * */
                        case "Image":
                            value = ((TextBox)control).Text;
                            if (!string.IsNullOrEmpty(value) && !value.Contains("http"))
                            {
                                if (!value.StartsWith("/"))
                                {
                                    value = "/" + value;
                                }
                                var strToreplace = HttpContext.Current.Request.ApplicationPath;

                                if (_staticDomain != string.Empty)
                                {
                                    if (strToreplace.StartsWith("/"))
                                    {
                                        strToreplace = strToreplace.Remove(0,1);
                                    }
                                    if (value.StartsWith("/" + strToreplace))
                                    {
                                        value = value.Replace("/" + strToreplace, _staticDomain);
                                    }
                                    else
                                    {
                                        value = _staticDomain + value;
                                    }
                                }  
                                else
                                    value = strToreplace + value;
                            }
                            break;
                        default:
                            break;
                    }
                }

                if (field.DataField == null)
                {
                    CspDataContext.content_fields.InsertOnSubmit(new content_field
                    {
                        content_Id = contentPerSelectedLng.content_Id,
                        content_types_fields_Id = field.Field.ContentTypeFieldId,
                        value_text = value
                    });
                }
                else
                {
                    content_field contentField = CspDataContext.content_fields.SingleOrDefault(a => a.content_fields_Id == field.DataField.content_fields_Id);
                    contentField.value_text = value;
                }
            }

            #endregion

            #region Save Content Categories
            //first , remove all old records
            CspDataContext.content_categories.DeleteAllOnSubmit(CspDataContext.content_categories.Where(a => a.content_main_Id == _contentMainId));
            CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
            //recreate new content_categories records
            foreach (RadTreeNode item in categoryTree.GetAllNodes().Where(a=>a.Checked))
            {
                int categoryId = Convert.ToInt32(item.Value);
                if (categoryId <= 0)
                    continue;

                // note: hardcode publication scheme = 11
                if (CspDataContext.content_categories.Count(a => a.content_main_Id == _contentMainId && a.category_Id == categoryId) == 0)
                {
                    CspDataContext.content_categories.InsertOnSubmit(new content_category
                    {
                        category_Id = categoryId,
                        content_main_Id = _contentMainId,
                        publication_scheme_Id = 11
                    });
                    CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                }
            }

            #endregion

            CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
            notification.Title = GetLocalizedText("EditContentToolBar.Save");
            notification.Show(GetLocalizedText("EditContentToolBar.SaveSuccessful"));
            return true;
        }

        private static string AppendToErrorMessage(string message, string s)
        {
            return s + "<p>" + message + "</p>";
        }

        #endregion

        #region Nested Category Tree

        /// <summary>
        /// 	if user category exceptions exist:
        /// 	render user exception categories
        /// 	else if category exceptions exist
        /// 	render exception categories
        /// 	else 
        /// 	render all categories
        /// </summary>
        private void RenderCategoryTree()
        {
            _listCategories = new List<category>();
            category[] tempCategories = CspDataContext.categories.Where(a => a.active.HasValue && a.active.Value && !a.categoryText.StartsWith(IgnoredCategoryText)).OrderBy(a => a.DisplayOrder).ToArray();
            if (_contentType.CSPCMS_UserExceptionCategories.Count(a => a.UserId == UserId) > 0)
            {
                // user category exceptions
                _listCategories.AddRange(tempCategories.Where(a => _contentType.CSPCMS_UserExceptionCategories.Count(b => b.CategoryId == a.categoryId && b.UserId == UserId) > 0));
            }
            else if (_contentType.CSPCMS_ExceptionCategories.Count > 0)
            {
                // category exceptions
                _listCategories.AddRange(tempCategories.Where(a => _contentType.CSPCMS_ExceptionCategories.Count(b => b.CategoryId == a.categoryId) > 0));
            }
            else
            {
                _listCategories = tempCategories.ToList();
            }
            
            categoryTree.Nodes.Clear();

            //vu-dinh 08/15/14 make sure to strip out html tag from category text
            foreach (var category in _listCategories)
            {
                category.categoryText = HtmlRemoval.StripTagsRegex(HttpUtility.HtmlDecode(category.categoryText));
            }

            CustomCategoryTreeNode root = CustomCategoryTreeNode.Create(_listCategories);
            RenderNestedTree(null, root);

            //Render context menu
            var contentMenu = new RadTreeViewContextMenu();
            contentMenu.Items.Add(new RadMenuItem
                                      {
                                          Value = "Rename",
                                          Text = "Rename ...",
                                          PostBack = false,
                                          Enabled = false,
                                          ImageUrl = ControlPath + "images/rename.gif"
                                      });
            contentMenu.Items.Add(new RadMenuItem
                                    {
                                        Value = "Add",
                                        Text = "Add new Category",
                                        ImageUrl = ControlPath + "images/new.gif"
                                    });
            categoryTree.ContextMenus.Add(contentMenu);
        }

        /// <summary>
        /// 	recursive function to create a nest tree
        /// </summary>
        /// <param name = "radTreeNode">current tree node</param>
        /// <param name = "node">data tree node</param>
        private void RenderNestedTree(RadTreeNode radTreeNode, CustomCategoryTreeNode node)
        {
            var tmp = new RadTreeNode(node.Text, node.Id.ToString());
            tmp.Enabled = ModuleRole.IsViewEditRole(UserController.GetCurrentUserInfo());
            tmp.Expanded = true;
            if (radTreeNode == null)
            {
                categoryTree.Nodes.Add(tmp);
            }
            else
            {
                radTreeNode.Nodes.Add(tmp);
            }

            foreach (CustomCategoryTreeNode child in node.Children)
            {
                RenderNestedTree(tmp, child);
            }
        }

        /// <summary>
        /// Adds the category.
        /// </summary>
        /// <param name="parentId">The parent id.</param>
        /// <param name="catName">Name of the cat.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 2/1/2013 - 5:42 PM
        private int AddCategory(int parentId, string catName)
        {
            var cat = new category
                          {
                              parentId = parentId,
                              categoryText = catName,
                              description = catName,
                              ExternalCategoryCode = catName,
                              dateAdded = DateTime.Now,
                              active = true,
                              publication_schemeID = 11,
                              apply_to_childs = false,
                              IsSubscribable = false
                          };
            CspDataContext.categories.InsertOnSubmit(cat);
            try
            {
                CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                //Update new category to themes_categories and consumers_themes
                AddCategoryToThemeCatAndConsumerThemes(cat);
            }
            catch (Exception)
            {
                return 0;
            }
            return cat.categoryId;
        }

        /// <summary>
        /// Adds the category to theme cat and consumer themes.
        /// </summary>
        /// <param name="cat">The cat.</param>
        /// Author: Vu Dinh
        /// 2/1/2013 - 6:18 PM
        private void AddCategoryToThemeCatAndConsumerThemes(category cat)
        {
            //add category to themes_categories
            var parentCat = CspDataContext.categories.SingleOrDefault(a => a.categoryId == cat.parentId);
            if (parentCat != null)
            {
                var themeCat = CspDataContext.themes_categories.Where(a => a.category_Id == parentCat.categoryId);
                var themeCatToInsert = new List<themes_category>();
                foreach (var themesCategory in themeCat)
                {
                    themeCatToInsert.Add(new themes_category
                                             {
                                                 themes_Id =  themesCategory.themes_Id,
                                                 category_Id = cat.categoryId
                                             });
                }
                if (themeCatToInsert.Any())
                {
                    CspDataContext.themes_categories.InsertAllOnSubmit(themeCatToInsert);
                    CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                }
            }

            //add category to consumers_themes
            var companies = CspDataContext.companies.ToList();
            var listConsumerTheme = new List<consumers_theme>();
            foreach (var company in companies)
            {
                foreach (var consumerTheme in CspDataContext.consumers_themes.Where(a => a.company == company && a.category_Id == parentCat.categoryId))
                {
                    listConsumerTheme.Add(new consumers_theme
                                              {
                                                  category_Id = cat.categoryId,
                                                  consumer_Id = company.companies_Id,
                                                  themes_Id = consumerTheme.themes_Id,
                                                  supplier_Id = consumerTheme.supplier_Id
                                              });
                }
            }
            if (listConsumerTheme.Any())
            {
                CspDataContext.consumers_themes.InsertAllOnSubmit(listConsumerTheme);
                CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
            }
        }

        /// <summary>
        /// Starts the node in edit mode.
        /// </summary>
        /// <param name="nodeValue">The node value.</param>
        /// Author: Vu Dinh
        /// 2/1/2013 - 5:32 PM
        private void StartNodeInEditMode(string nodeValue)
        {
            //find the node by its Value and edit it when page loads
            string js = "Sys.Application.add_load(editNode); function editNode(){ ";
            js += "var tree = $find(\"" + categoryTree.ClientID + "\");";
            js += "var node = tree.findNodeByValue('" + nodeValue + "');";
            js += "if (node) node.startEdit();";
            js += "Sys.Application.remove_load(editNode);};";

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "nodeEdit", js, true);
        }

        #endregion

        /// <summary>
        /// Handles the OnContextMenuItemClick event of the categoryTree control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Telerik.Web.UI.RadTreeViewContextMenuEventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 2/1/2013 - 3:44 PM
        protected void categoryTree_OnContextMenuItemClick(object sender, RadTreeViewContextMenuEventArgs e)
        {
            var clickedNode = e.Node;
            switch (e.MenuItem.Value)
            {
                case "Add" :
                    {
                        var parentId = (e.Node.Value != string.Empty || e.Node.Value != ("-1")) ? int.Parse(e.Node.Value) : 0;
                        //Add category node
                        var newCat = new RadTreeNode(string.Format("New Category {0}", clickedNode.Nodes.Count + 1));
                        newCat.Selected = true;
                        newCat.ImageUrl = clickedNode.ImageUrl;

                        //Add new category into db
                        var catIdReturn = AddCategory(parentId, newCat.Text);
                        if (catIdReturn != 0)
                        {
                            clickedNode.Nodes.Add(newCat);
                            clickedNode.Expanded = true;
                            //set node's value so we can find it in startNodeInEditMode
                            newCat.Value = catIdReturn.ToString();
                            StartNodeInEditMode(newCat.Value);
                        }
                        break;
                    }
            }
        }

        /// <summary>
        /// Handles the OnNodeEdit event of the categoryTree control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Telerik.Web.UI.RadTreeNodeEditEventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 2/1/2013 - 5:03 PM
        protected void categoryTree_OnNodeEdit(object sender, RadTreeNodeEditEventArgs e)
        {
            int catId;
            if (int.TryParse(e.Node.Value, out catId))
            {
                e.Node.Text = e.Text;
                var cat = CspDataContext.categories.SingleOrDefault(a => a.categoryId == catId);
                if (cat!= null)
                {
                    cat.categoryText = e.Text;
                    cat.description = e.Text;
                    cat.ExternalCategoryCode = e.Text + e.Node.Value;
                    CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                }
            }
        }

        /// <summary>
        /// Allows the cobranded upload.
        /// </summary>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
        /// <author>Vu Dinh</author>
        /// <modified>05/21/2014</modified>
        public bool AllowCobrandedUpload()
        {
            return (!string.IsNullOrEmpty(_cobrandedDir) && !string.IsNullOrEmpty(_instanceName));
        }
    }
}