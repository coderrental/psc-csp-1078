﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Users;
using TIEKinetix.CspModules.CSPContentManagementSystem.Components;
using TIEKinetix.CspModules.Data;
using Telerik.Web.UI;

namespace TIEKinetix.CspModules.CSPContentManagementSystem
{
    public partial class EditContentException1 : DnnModuleBase
    {
        private string ADMIN_ROLE = "Admin Role";
        private CSPCMS_ContentType _contentType;
        private DnnDataContext _dnnContext;
        private int _integrationKey;
        Dictionary<string, string> _keys = new Dictionary<string, string>();
        private List<language> _languageExceptions;
        private List<company> _supplierExceptions;

        protected override void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
            if (!Request.IsAuthenticated)
                Response.Redirect(ResolveUrl("~/"), true);
            string hash = HttpUtility.HtmlDecode(Request.Params["hash"]);
            _keys = new Dictionary<string, string>();
            string[] parameters = Common.Decode(hash).Split(new[] { "&" }, StringSplitOptions.None);
            foreach (string parameter in parameters)
            {
                _keys.Add(parameter.Split('=')[0], parameter.Split('=')[1]);
            }

            _dnnContext = new DnnDataContext(Config.GetConnectionString());

            //data
            _contentType = _dnnContext.CSPCMS_ContentTypes.SingleOrDefault(a => a.CSPCMS_ContentType_Id == new Guid(_keys[EditContentKeys.ContentTypeId]));

            #region Load Language & Supplier Exceptions
            _languageExceptions = new List<language>();
            _supplierExceptions = new List<company>();

            if (ModuleRole.IsInRole(UserController.GetCurrentUserInfo(), ADMIN_ROLE))
            {
                //todo: front end / module role / this user can see all languages
                foreach (language lng in CspDataContext.languages.Where(a => a.active.HasValue && a.active.Value))
                {
                    _languageExceptions.Add(lng);
                }
            }
            else if (_contentType.CSPCMS_UserExceptionLanguages.Count(a => a.UserId == UserId) > 0)
            {
                //todo: front end / module role /user language exception list
                foreach (language lng in CspDataContext.languages.Where(a => a.active.HasValue && a.active.Value))
                {
                    if (_contentType.CSPCMS_UserExceptionLanguages.Count(a => a.LanguageId == lng.languages_Id && a.UserId == UserId) > 0)
                        _languageExceptions.Add(lng);
                }
            }
            else if (_contentType.CSPCMS_ExceptionLanguages.Count > 0)
            {
                foreach (language lng in CspDataContext.languages.Where(a => a.active.HasValue && a.active.Value))
                {
                    if (_contentType.CSPCMS_ExceptionLanguages.Count(a => a.LanguageId == lng.languages_Id) > 0)
                        _languageExceptions.Add(lng);
                }
            }
            else
            {
                foreach (language lng in CspDataContext.languages.Where(a => a.active.HasValue && a.active.Value))
                {
                    _languageExceptions.Add(lng);
                }
            }

            //supplier exception
            if (ModuleRole.IsInRole(UserController.GetCurrentUserInfo(), ADMIN_ROLE))
            {
                foreach (company supplier in CspDataContext.companies.Where(a => a.is_supplier.HasValue && a.is_supplier.Value))
                {
                    _supplierExceptions.Add(supplier);
                }
            }
            else if (_contentType.CSPCMS_UserExceptionSuppliers.Count(a => a.UserId == UserId) > 0)
            {
                foreach (CSPCMS_UserExceptionSupplier supplier in _contentType.CSPCMS_UserExceptionSuppliers.Where(a => a.UserId == UserId))
                {
                    _supplierExceptions.Add(CspDataContext.companies.FirstOrDefault(a => a.companies_Id == supplier.SupplierId));
                }
            }
            else if (_contentType.CSPCMS_ExceptionSuppliers.Count > 0)
            {
                foreach (CSPCMS_ExceptionSupplier supplier in _contentType.CSPCMS_ExceptionSuppliers)
                {
                    _supplierExceptions.Add(CspDataContext.companies.FirstOrDefault(a => a.companies_Id == supplier.SupplierId));
                }
            }
            else
            {
                foreach (company supplier in CspDataContext.companies.Where(a => a.is_supplier.HasValue && a.is_supplier.Value))
                {
                    _supplierExceptions.Add(supplier);

                }
            }

            #endregion

            // populate language drop down list
            var ddlLanguages = new RadComboBox {ID = "editWindowLanguageDropDownList", AutoPostBack = true};

            foreach (language lng in _languageExceptions)
            {
                ddlLanguages.Items.Add(new RadComboBoxItem(lng.description, lng.languages_Id.ToString()));
            }

            // buttons
            toolBar.Items.Add(new RadToolBarButton
            {
                Text = GetLocalizedText("EditContentToolBar.Back"),
                ImageUrl = ControlPath + "images/arrow_left.png",
                Value = "Back",
                NavigateUrl = Globals.NavigateURL(TabId, "Content", "mid", ModuleId.ToString(), "viewdetail","1")

            });
            toolBar.Items.Add(new RadToolBarButton { IsSeparator = true });
            toolBar.Items.Add(new RadToolBarButton
            {
                Text = GetLocalizedText("EditContentToolBar.Save"),
                ImageUrl = ControlPath + "images/save.png",
                Value = "Save",
                CausesValidation = true,
                Enabled = ModuleRole.IsViewEditRole(UserController.GetCurrentUserInfo())
            });

            // add consumers to combobox
            var cbConsumers = new RadComboBox {ID = "editWindowComboBoxConsumer", AutoPostBack = true};
            cbConsumers.SelectedIndexChanged += cbConsumers_SelectedIndexChanged;
            if (_contentType.CSPCMS_ExceptionConsumers.Count(a => a.UserId == UserId) == 0)
            {
                string integrationKey = UserController.GetCurrentUserInfo().Profile.GetPropertyValue(EditContentKeys.IntegrationKey);

                if (!string.IsNullOrEmpty(integrationKey) && int.TryParse(integrationKey, out _integrationKey))
                {
                    cbConsumers.Items.Add(new RadComboBoxItem(CspDataContext.companies.First(a => a.companies_Id == _integrationKey).companyname, _integrationKey.ToString()));
                }
            }
            else
            {
                foreach (CSPCMS_ExceptionConsumer consumer in _contentType.CSPCMS_ExceptionConsumers.Where(a => a.UserId == UserId))
                {
                    cbConsumers.Items.Add(new RadComboBoxItem(CspDataContext.companies.First(a => a.companies_Id == consumer.ConsumerId).companyname, consumer.ConsumerId.ToString()));
                }
            }

            toolBar.Controls.Add(cbConsumers);
            //toolBar.Controls.Add(ddlLanguages);

            #region Render Content

            RenderContent();

            #endregion
        }

        void cbConsumers_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (_contentType.CSPCMS_ContentTypeFields.Count(a => a.ConsumerExceptionField) > 0)
            {
                var cbConsumer = (RadComboBox)toolBar.FindControl("editWindowComboBoxConsumer");
                var contentId = new Guid(_keys[EditContentKeys.ContentId]);
                int companyId = Convert.ToInt32(cbConsumer.SelectedValue);
                string value = "", id = "";
                Control control;
                Image image;
                foreach (CSPCMS_ContentTypeField field in _contentType.CSPCMS_ContentTypeFields.Where(a => a.ConsumerExceptionField))
                {
                    value = GetContentTextValue(contentId, field.ContentTypeFieldId, companyId);
                    id = "cf_" + field.CSPCMS_ContentTypeField_Id.ToString(); // control id
                    control = contentPanel.FindControl(id);
                    if (control == null)
                        continue;

                    switch (field.ContentTypeFieldType)
                    {
                        case "Text":
                            ((RadTextBox)control).Text = value;
                            break;
                        case "LongText":
                            ((RadTextBox)control).Text = value;
                            break;
                        case "Html":
                            ((RadEditor)control).Content = value;
                            break;
                        case "Number":
                            ((RadNumericTextBox)control).Text = value;
                            break;
                        case "File":
                            var fileUpload = (RadUpload)control;
                            if (!string.IsNullOrEmpty(fileUpload.Attributes["path"]))
                                fileUpload.Attributes["path"] = value;
                            else
                                fileUpload.Attributes.Add("path", value);
                            image = (Image)contentPanel.FindControl("pv_" + id);
                            if (string.IsNullOrEmpty(value))
                                image.Visible = false;
                            else
                            {
                                image.Visible = true;
                                image.ImageUrl = value;
                            }
                            break;
                        case "Image":
                            image = (Image)contentPanel.FindControl("pv_" + id);
                            if (string.IsNullOrEmpty(value))
                                image.Visible = false;
                            else
                            {
                                image.Visible = true;
                                image.ImageUrl = value;
                            }
                            break;
                        default:
                            ((RadTextBox)control).Text = value;
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// render content
        /// </summary>
        private void RenderContent()
        {
            if (_contentType.CSPCMS_ContentTypeFields.Count(a => a.ConsumerExceptionField) == 0)
            {
                contentPanel.Controls.Add(new Literal { Text = GetLocalizedText("EditContentException.Empty") });
            }
            else
            {
                var selectedLanguage = (RadComboBox)toolBar.FindControl("editWindowLanguageDropDownList");
                var cbConsumer = (RadComboBox)toolBar.FindControl("editWindowComboBoxConsumer");

                var tbl = new Table();
                tbl.Width = new Unit("100%");
                contentPanel.Controls.Add(tbl);

                bool isEnabled = ModuleRole.IsViewEditRole(UserController.GetCurrentUserInfo());
                string textValue = "", id = "";
                var contentId = new Guid(_keys[EditContentKeys.ContentId]);
                int companyId = Convert.ToInt32(cbConsumer.SelectedValue);

                foreach (CSPCMS_ContentTypeField field in _contentType.CSPCMS_ContentTypeFields.Where(a => a.ConsumerExceptionField))
                {
                    if (!field.Visible) continue;
                    textValue = "";

                    CSPCMS_Translation translation = field.CSPCMS_Translations.SingleOrDefault(a => a.CultureCode == CurrentLanguage);

                    var tr = new TableRow();
                    var tc = new TableCell();
                    tbl.Rows.Add(tr);

                    //help icon
                    tc = new TableCell();
                    tc.Width = new Unit(24);
                    tc.Controls.Add(new ImageButton { ImageUrl = ControlPath + "images/16x16/information.png", TabIndex = -1, CausesValidation = false, ID = "hover" + field.CSPCMS_ContentTypeField_Id });
                    tc.Controls.Add(new RadToolTip { TargetControlID = "hover" + field.CSPCMS_ContentTypeField_Id, Text = translation != null ? translation.LongDesc : field.ContentTypeFieldName });
                    tr.Cells.Add(tc);
                    //label
                    tc = new TableCell();
                    tc.Attributes.Add("nowrap", "nowrap");
                    tc.Width = new Unit("15%");
                    tc.Controls.Add(new Label { Text = translation != null ? translation.ShortDesc : field.ContentTypeFieldName.Replace("_", " ") });
                    tr.Cells.Add(tc);
                    //field
                    tc = new TableCell();

                    id = "cf_" + field.CSPCMS_ContentTypeField_Id.ToString(); // control id

                    textValue = GetContentTextValue(contentId, field.ContentTypeFieldId, companyId);

                    // if code list, then ignore field type
                    if (field.CSPCMS_CodeList != null)
                    {
                        var comboBox = new RadComboBox();
                        comboBox.Enabled = !field.Readonly;
                        comboBox.ID = id;
                        foreach (CSPCMS_CodeListValue codeListValue in field.CSPCMS_CodeList.CSPCMS_CodeListValues.OrderBy(a => a.DisplayOrder))
                        {
                            comboBox.Items.Add(new RadComboBoxItem
                            {
                                Text = codeListValue.Name,
                                Value = codeListValue.Value,
                                Enabled = isEnabled,
                                Selected = codeListValue.Value == textValue
                            });

                        }
                        tc.Controls.Add(comboBox);
                        tr.Cells.Add(tc);
                        continue;
                    }

                    // render fields with field types
                    string imagePath = "";
                    Image image;
                    switch (field.ContentTypeFieldType)
                    {
                        case "Text":
                            tc.Controls.Add(new RadTextBox
                            {
                                ID = id,
                                Enabled = !field.Readonly && isEnabled,
                                Width = new Unit("50%"),
                                Text = textValue
                            });
                            if (field.Required)
                            {
                                var validator = new RequiredFieldValidator
                                {
                                    ControlToValidate = id,
                                    Display = ValidatorDisplay.Dynamic,
                                    ErrorMessage = GetLocalizedText("Validator.Required")
                                };
                                tc.Controls.Add(validator);
                            }
                            break;
                        case "LongText":
                            tc.Controls.Add(new RadTextBox
                            {
                                ID = id,
                                TextMode = InputMode.MultiLine,
                                Rows = 5,
                                Width = new Unit("90%"),
                                Enabled = !field.Readonly && isEnabled,
                                Text = textValue
                            });
                            if (field.Required)
                            {
                                var validator = new RequiredFieldValidator
                                {
                                    ControlToValidate = id,
                                    Display = ValidatorDisplay.Dynamic,
                                    ErrorMessage = GetLocalizedText("Validator.Required")
                                };
                                tc.Controls.Add(validator);
                            }
                            break;
                        case "Html":
                            var editorPanel = new Panel
                            {
                                Width = new Unit("100%"),
                                Height = new Unit(300)
                            };
                            var radEditor = new RadEditor
                            {
                                ID = id,
                                ContentAreaMode = EditorContentAreaMode.Div,
                                Width = new Unit("90%"),
                                ToolbarMode = EditorToolbarMode.Default,
                                Visible = true,
                                Enabled = !field.Readonly && isEnabled,
                                Height = new Unit("290"),
                                Content = textValue
                            };
                            radEditor.Tools.Clear();
                            var group = new EditorToolGroup();
                            radEditor.Tools.Add(group);
                            var etCut = new EditorTool { Name = "Cut", Text = GetLocalizedText("HtmlEditor.Cut") };
                            var etCopy = new EditorTool { Name = "Copy", Text = GetLocalizedText("HtmlEditor.Copy") };
                            var etPaste = new EditorTool { Name = "Paste", Text = GetLocalizedText("HtmlEditor.Paste") };
                            group.Tools.Add(etCut);
                            group.Tools.Add(etCopy);
                            group.Tools.Add(etPaste);
                            editorPanel.Style.Add("display", "");
                            editorPanel.Controls.Add(radEditor);
                            tc.Controls.Add(editorPanel);
                            break;
                        case "Number":
                            tc.Controls.Add(new RadNumericTextBox
                            {
                                ID = id,
                                Enabled = !field.Readonly && isEnabled,
                                Text = textValue
                            });
                            if (field.Required)
                            {
                                var validator = new RequiredFieldValidator
                                {
                                    ControlToValidate = id,
                                    Display = ValidatorDisplay.Dynamic,
                                    ErrorMessage = GetLocalizedText("Validator.Required")
                                };
                                tc.Controls.Add(validator);
                            }
                            break;
                        case "File":
                            var fileUpload = new RadUpload
                            {
                                ID = id,
                                Enabled = !field.Readonly && isEnabled,
                                MaxFileInputsCount = 1,
                                ControlObjectsVisibility = ControlObjectsVisibility.None
                            };
                            tc.Controls.Add(fileUpload);
                            // review image
                            imagePath = textValue;
                            image = new Image { ID = "pv_" + id, ImageUrl = imagePath, Width = new Unit(100), AlternateText = imagePath };
                            tc.Controls.Add(image);
                            if (string.IsNullOrEmpty(imagePath))
                            {
                                image.Visible = false;
                            }
                            break;
                        case "Image":
                            tc.Controls.Add(new RadTextBox
                            {
                                ID = id,
                                Enabled = !field.Readonly && isEnabled,
                                Text = imagePath,
                                Width = new Unit("90%")
                            });
                            imagePath = textValue;
                            image = new Image { ID = "pv_" + id, ImageUrl = imagePath, Width = new Unit(100), AlternateText = imagePath };
                            tc.Controls.Add(image);
                            if (string.IsNullOrEmpty(imagePath))
                            {
                                image.Visible = false;
                            }
                            break;
                        default:
                            tc.Controls.Add(new RadTextBox
                            {
                                ID = id,
                                Enabled = !field.Readonly && isEnabled,
                                Text = textValue
                            });
                            if (field.Required)
                            {
                                var validator = new RequiredFieldValidator
                                {
                                    ControlToValidate = id,
                                    Display = ValidatorDisplay.Dynamic,
                                    ErrorMessage = GetLocalizedText("Validator.Required")
                                };
                                tc.Controls.Add(validator);
                            }
                            break;
                    }

                    tr.Cells.Add(tc);

                }
            }
        }



        protected void ToolBarClickEventHandler(object sender, RadToolBarEventArgs e)
        {
            string id, errorMessages, value;
            Control control;
            var cbConsumer = (RadComboBox)toolBar.FindControl("editWindowComboBoxConsumer");



            switch (e.Item.Value)
            {
                case "Save":
                    //DLV - if no consumer selected, exit immediately
                    if (string.IsNullOrEmpty(cbConsumer.SelectedValue))
                        return;
                    int consumerId = Convert.ToInt32(cbConsumer.SelectedValue);
                    if (_contentType.CSPCMS_ContentTypeFields.Count(a => a.ConsumerExceptionField) > 0)
                    {
                        #region Validate user input

                        bool validated = true;
                        errorMessages = "";
                        foreach (CSPCMS_ContentTypeField field in _contentType.CSPCMS_ContentTypeFields.Where((a => a.ConsumerExceptionField)))
                        {
                            value = "";
                            id = "cf_" + field.CSPCMS_ContentTypeField_Id.ToString(); // control id
                            control = contentPanel.FindControl(id);
                            CSPCMS_Translation translation = field.CSPCMS_Translations.FirstOrDefault(a => a.CultureCode == CurrentLanguage);

                            if (field.CSPCMS_CodeList != null)
                                value = ((RadComboBox)control).SelectedValue;
                            else
                            {
                                switch (field.ContentTypeFieldType)
                                {
                                    case "Text":
                                        value = ((RadTextBox)control).Text;
                                        break;
                                    case "LongText":
                                        value = ((RadTextBox)control).Text;
                                        break;
                                    case "Html":
                                        value = ((RadEditor)control).Content;
                                        break;
                                    case "Number":
                                        value = ((RadNumericTextBox)control).Text;
                                        break;
                                    case "File":
                                        var fileUpload = (RadUpload)control;
                                        value = (fileUpload.UploadedFiles.Count > 0 ? fileUpload.UploadedFiles[0].FileName : fileUpload.Attributes["path"]);
                                        break;
                                    default:
                                        break;
                                }
                            }

                            if (field.Required && string.IsNullOrEmpty(value))
                            {
                                errorMessages = AppendToErrorMessage("[" + translation != null ? translation.ShortDesc : field.ContentTypeFieldName + "] " + GetLocalizedText("Validator.Required"), errorMessages);
                                validated = false;
                            }

                            if (field.CSPCMS_Validation != null && (!string.IsNullOrEmpty(value) || field.Required) && !Regex.IsMatch(value ?? "", field.CSPCMS_Validation.ValidationRule))
                            {
                                errorMessages = AppendToErrorMessage("[" + (translation != null ? translation.ShortDesc : field.ContentTypeFieldName) + "] " + GetLocalizedText("Validator.FailValidation"), errorMessages);
                                validated = false;
                            }
                        }

                        if (!validated)
                        {
                            notification.Title = "Error";
                            notification.Show(errorMessages);
                            break; // fail validation
                        }

                        #endregion

                        #region Save user input

                        var contentId = new Guid(_keys[EditContentKeys.ContentId]);

                        errorMessages = "";
                        foreach (CSPCMS_ContentTypeField field in _contentType.CSPCMS_ContentTypeFields.Where((a => a.ConsumerExceptionField)))
                        {
                            value = "";
                            id = "cf_" + field.CSPCMS_ContentTypeField_Id.ToString(); // control id
                            control = contentPanel.FindControl(id);
                            CSPCMS_Translation translation = field.CSPCMS_Translations.FirstOrDefault(a => a.CultureCode == CurrentLanguage);

                            if (field.CSPCMS_CodeList != null)
                                value = ((RadComboBox)control).SelectedValue;
                            else
                            {
                                switch (field.ContentTypeFieldType)
                                {
                                    case "Text":
                                        value = ((RadTextBox)control).Text;
                                        break;
                                    case "LongText":
                                        value = ((RadTextBox)control).Text;
                                        break;
                                    case "Html":
                                        value = ((RadEditor)control).Content;
                                        break;
                                    case "Number":
                                        value = ((RadNumericTextBox)control).Text;
                                        break;
                                    case "File":
                                        var fileUpload = ((RadUpload)control);
                                        foreach (UploadedFile uploadedFile in fileUpload.UploadedFiles)
                                        {
                                            string path = ResolveUrl("~/UploadImages/Portal" + PortalId + "/" + _keys[EditContentKeys.ContentMainId] + "/");
                                            string fileExt = Path.GetExtension(uploadedFile.FileName);
                                            string fileName = DateTime.Now.Ticks.ToString() + fileExt;
                                            string physicalPath = Request.MapPath(path, Request.ApplicationPath, false);
                                            if (!Directory.Exists(physicalPath))
                                                Directory.CreateDirectory(physicalPath);

                                            uploadedFile.SaveAs(Request.MapPath(path + fileName, Request.ApplicationPath, false));

                                            if (string.IsNullOrEmpty(_keys[EditContentKeys.DomainUrlForUploadedFiles]))
                                                value = ResolveUrl("~/UploadImages/Portal" + PortalId+ "/" + _keys[EditContentKeys.ContentMainId] + "/");
                                            else
                                                value = _keys[EditContentKeys.DomainUrlForUploadedFiles] + "/" + _keys[EditContentKeys.ContentMainId] + "/";
                                            value += fileName;

                                            var image = contentPanel.FindControl("pv_" + id) as Image;
                                            if (image != null)
                                            {
                                                image.Visible = true;
                                                image.ImageUrl = value;
                                            }
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }

                            // save
                            content_field contentField = CspDataContext.content_fields.FirstOrDefault(a => a.content_Id == contentId && a.content_types_fields_Id == field.ContentTypeFieldId);
                            if (contentField != null)
                            {
                                custom_content_field exceptionField = CspDataContext.custom_content_fields.FirstOrDefault(a => a.consumer_Id == _integrationKey && a.content_Id == contentId && a.content_fields_Id == contentField.content_fields_Id);
                                if (exceptionField == null)
                                {
                                    CspDataContext.custom_content_fields.InsertOnSubmit(new custom_content_field
                                    {
                                        consumer_Id = consumerId,
                                        content_Id = contentId,
                                        content_fields_Id = (int)contentField.content_fields_Id,
                                        value_text = value
                                    });

                                }
                                else
                                {
                                    exceptionField.value_text = value;
                                }
                            }
                        }

                        CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                        notification.Title = "Saved";
                        notification.Show(GetLocalizedText("EditContentToolBar.SaveSuccessful"));

                        #endregion
                    }
                    break;
                default:
                    break;
            }
        }

        private string GetContentTextValue(Guid contentId, int contentTypeFieldId, int companyId)
        {
            content_field field = CspDataContext.content_fields.FirstOrDefault(a => a.content_Id == contentId && a.content_types_fields_Id == contentTypeFieldId);
            if (field != null)
            {
                custom_content_field exceptionField = CspDataContext.custom_content_fields.FirstOrDefault(a => a.consumer_Id == companyId && a.content_Id == contentId && a.content_fields_Id == field.content_fields_Id);
                if (exceptionField == null)
                    return field.value_text;
                return exceptionField.value_text;
            }
            return "";
        }

        private static string AppendToErrorMessage(string message, string s)
        {
            return s + message + "<p/>";
        }
    }
}