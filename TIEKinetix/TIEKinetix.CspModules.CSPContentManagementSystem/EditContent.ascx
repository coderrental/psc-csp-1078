﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditContent.ascx.cs" Inherits="TIEKinetix.CspModules.CSPContentManagementSystem.EditContent" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
        <telerik:RadAjaxLoadingPanel ID="loadingPanel" IsSticky="true" Style="position: absolute; top: 0; left: 0; height: 100%; width: 100%;" runat="server"></telerik:RadAjaxLoadingPanel>
		<!-- Hidden controls & fields for Confirmation purpose -->
		<div style="display: none">
			<asp:Button runat="server" ID="btnSaveContent" ClientIDMode="Static" OnClick="SaveContent"/>
			<asp:Button runat="server" ID="btnSaveBack" ClientIDMode="Static" OnClick="SaveBack"/>
			<asp:Button runat="server" ID="btnSaveNew" ClientIDMode="Static" OnClick="SaveNew"/>

			<asp:HiddenField runat="server" ClientIDMode="Static" ID="TargetLang"/>
			<asp:Button runat="server" ID="btnGotoLang" ClientIDMode="Static" OnClick="GoToLang"/>
			<asp:Button runat="server" ID="btnSaveGotoLang" ClientIDMode="Static" OnClick="SaveGoToLang"/>
			
			<asp:Button runat="server" ID="btnSaveGoBack" ClientIDMode="Static" OnClick="SaveGoBack"/>
			
			<telerik:RadComboBox runat="server" ID="cbLangtoClone" Width="200" ClientIDMode="Static"></telerik:RadComboBox>
			<asp:Button runat="server" ID="btnCloneFromLang" ClientIDMode="Static" OnClick="CloneFromLanguage"/>
			
			<asp:Button runat="server" ID="btnPublishAll" ClientIDMode="Static" OnClick="DoPublishAll"/>
			<asp:Button runat="server" ID="btnUnPublishAll" ClientIDMode="Static" OnClick="DoUnPublishAll"/>
		</div>

		<telerik:RadWindowManager ID="windowManager" runat="server">
			<ConfirmTemplate>
				<!-- Save options -->
				<div class="confirm-section" id="save-options">
					<p id="msg-save-options"><%= GetLocalizedText("Confirm.SaveContent") %></p>
					<div id="save-options-btn">
						<input class="Button" type="button" id="dialogSave" value="Save" onclick="javascript: DialogClickedHandler('save');"/>
						<input class="Button" type="button" id="dialogSaveBack" value="Save & Back" onclick="javascript: DialogClickedHandler('saveback');"/>
						<input class="Button" type="button" id="dialogSaveNew" value="Save & New" onclick="javascript: DialogClickedHandler('savenew');"/>
						<input class="Button" type="button" id="dialogCancel" value="Cancel" onclick="javascript: DialogClickedHandler('cancel');"/>
					</div>
				</div>	
				
				<!-- Save before changing language -->
				<div class="confirm-section" id="save-confirm">
					<p id="msg-save-confirm"><%= GetLocalizedText("Confirm.SaveBeforeSwichLanguage") %></p>
					<div id="save-confirm-btn">
						<input class="Button" type="button" id="dialogSaveGoLang" value="Yes" onclick="javascript: DialogClickedHandler('savegolang');"/>
						<input class="Button" type="button" id="dialogGoLang" value="No" onclick="javascript: DialogClickedHandler('gotolang');"/>
						<input class="Button" type="button" id="dialogGoLangCancel" value="Cancel" onclick="javascript: DialogClickedHandler('cancel');"/>
					</div>
				</div>	
				
				<!-- Save before going back to content list -->
				<div class="confirm-section" id="saveback-confirm">
					<p id="msg-saveback-confirm"><%= GetLocalizedText("Confirm.SaveBeforeClose") %></p>
					<div id="saveback-confirm-btn">
						<input class="Button" type="button" id="dialogSaveGoBack" value="Yes" onclick="javascript: DialogClickedHandler('savegoback');"/>
						<input class="Button" type="button" id="dialogGoBack" value="No" onclick="javascript: DialogClickedHandler('goback');"/>
						<input class="Button" type="button" id="dialogBackCancel" value="Cancel" onclick="javascript: DialogClickedHandler('cancel');"/>
					</div>
				</div>
				
				<!-- Clone from another language -->
				<div class="confirm-section" id="clonelang-confirm">
					<div id="msg-clonelang-confirm">
						<p><%= GetLocalizedText("Confirm.CloneFromLanguage") %></p>
						<div id="langlist-wrapper">
							<select id="cbCloneSrcLang"></select>
						</div>
					</div>
					<div id="clonelang-confirm-btn">
						<input class="Button" type="button" id="dialogCloneLang" value="Yes" onclick="javascript: DialogClickedHandler('clonelang');"/>
						<input class="Button" type="button" id="dialogCloneLangCancel" value="No" onclick="javascript: DialogClickedHandler('cancel');"/>
					</div>
				</div>
				
				<!-- Publish All -->
				<div class="confirm-section" id="publishall-confirm">
					<p id="msg-publishall-confirm"><%= GetLocalizedText("Confirm.PublishAll") %></p>
					<div id="publishall-confirm-btn">
						<input class="Button" type="button" id="dialogPublishAll" value="Yes" onclick="javascript: DialogClickedHandler('publishall');"/>
						<input class="Button" type="button" id="dialogPublishAllCancel" value="Cancel" onclick="javascript: DialogClickedHandler('cancel');"/>
					</div>
				</div>
				
				<!-- UnPublish All -->
				<div class="confirm-section" id="unpublishall-confirm">
					<p id="msg-unpublishall-confirm"><%= GetLocalizedText("Confirm.UnPublishAll") %></p>
					<div id="unpublishall-confirm-btn">
						<input class="Button" type="button" id="dialogUnPublishAll" value="Yes" onclick="javascript: DialogClickedHandler('unpublishall');"/>
						<input class="Button" type="button" id="dialogUnPublishAllCancel" value="Cancel" onclick="javascript: DialogClickedHandler('cancel');"/>
					</div>
				</div>
			</ConfirmTemplate>
		</telerik:RadWindowManager>
		<asp:Panel runat="server" ID="mainContainer">
			<telerik:RadNotification runat="server" ID="notification" VisibleOnPageLoad="false"></telerik:RadNotification>

			<telerik:RadToolBar runat="server" ID="toolBar" Width="100%" OnButtonClick="ToolBarClickEventHandler" CssClass="pos-relative">
			</telerik:RadToolBar>
            <telerik:RadToolBar runat="server" ID="secondToolbar" ClientIDMode="Static" Width="100%" OnButtonClick="secondToolbar_OnButtonClick"></telerik:RadToolBar>
			<telerik:RadTabStrip runat="server" ID="tabs" SelectedIndex="1" MultiPageID="pages" CausesValidation="false" Skin="Outlook">
				<Tabs>
					<telerik:RadTab runat="server" PageViewID="contentMainPage" Text="General" Value="General"></telerik:RadTab>
					<telerik:RadTab runat="server" PageViewID="editFieldPage" Text="Fields" Value="Fields"></telerik:RadTab>
					<telerik:RadTab runat="server" PageViewID="editCategoryPage" Text="Categories" Value="Categories"></telerik:RadTab>                            
				</Tabs>
			</telerik:RadTabStrip>
			<telerik:RadMultiPage runat="server" ID="pages" SelectedIndex="1">
				<telerik:RadPageView ID="contentMainPage" runat="server">
				    <div class="content-field-group">
					    <table border="0" style="width:100%">
						    <tr>
							    <td style="width:24px">
								    <asp:Image runat="server" ID="imageContentIdentifier" />
							    </td>
							    <td nowrap="nowrap" style="width:15%">
								    <asp:Label runat="server" ID="lbContentIdentifier"></asp:Label>
							    </td>
							    <td>
								    <telerik:RadTextBox ID="tbContentIdentifier" runat="server" Width="50%"></telerik:RadTextBox>
								    <asp:RequiredFieldValidator runat="server" ControlToValidate="tbContentIdentifier" EnableClientScript="False" ID="vContentIdentifier"></asp:RequiredFieldValidator>
							    </td>
						    </tr>
						    <tr>
							    <td style="width:24px">
								    <asp:Image runat="server" ID="imageSupplierId" />
							    </td>
							    <td nowrap="nowrap" style="width:15%">
								    <asp:Label runat="server" ID="lbSupplierId"></asp:Label>                            
							    </td>
							    <td>
								    <telerik:RadComboBox ID="cbSupplierId" runat="server" OnClientSelectedIndexChanged="SupplierUpdated"></telerik:RadComboBox>
							    </td>
						    </tr>
					    </table>
                    </div>
				</telerik:RadPageView>
				<telerik:RadPageView ID="editFieldPage" runat="server">
					<asp:Panel ID="editContent" runat="server" Width="100%"></asp:Panel>
				</telerik:RadPageView>
				<telerik:RadPageView ID="editCategoryPage" runat="server">
				    <div class="content-field-group">
				        <telerik:RadAjaxPanel runat="server" LoadingPanelID="loadingPanel">
					        <telerik:RadTreeView ID="categoryTree" runat="server" 
                                CheckChildNodes="False" 
                                Skin="Default" 
                                OnClientNodeClicked="ClientNodeClicked" 
                                OnClientContextMenuItemClicking="onClientContextMenuItemClicking" 
                                OnClientContextMenuShowing="onClientContextMenuShowing"
                                OnClientNodeChecked="ClientNodeChecked" 
                                OnContextMenuItemClick = "categoryTree_OnContextMenuItemClick"
                                OnNodeEdit = "categoryTree_OnNodeEdit"
                                CheckBoxes="true">
					        </telerik:RadTreeView>
                            <telerik:RadScriptBlock runat="server">
                                <script type="text/javascript">
                                    function ClientNodeClicked(sender, eventArgs) {
                                        var childNodes = eventArgs.get_node().get_nodes();
                                        var isCheck = eventArgs.get_node().get_checked();
                                        if (isCheck) {
                                            eventArgs.get_node().set_checked(false);
                                            if (eventArgs.get_node().get_text() == "All")
                                                UpdateAllChildren(childNodes, false);
                                        } else {
                                            eventArgs.get_node().set_checked(true);
                                            if (eventArgs.get_node().get_text() == "All")
                                                UpdateAllChildren(childNodes, true);
                                        }

                                    }

                                    function ClientNodeChecked(sender, eventArgs) {
                                        var childNodes = eventArgs.get_node().get_nodes();
                                        var isChecked = eventArgs.get_node().get_checked();
                                        if (eventArgs.get_node().get_text() == "All")
                                            UpdateAllChildren(childNodes, isChecked);
                                    }

                                    function UpdateAllChildren(nodes, checked) {
                                        var i;
                                        for (i = 0; i < nodes.get_count(); i++) {
                                            if (checked) {
                                                nodes.getNode(i).check();
                                            }
                                            else {
                                                nodes.getNode(i).set_checked(false);
                                            }

                                            if (nodes.getNode(i).get_nodes().get_count() > 0) {
                                                UpdateAllChildren(nodes.getNode(i).get_nodes(), checked);
                                            }
                                        }
                                    }
                                    function OnClientClick() {
                                        var oWindow = null;
                                        if (window.radWindow)
                                            oWindow = window.radWindow;
                                        else if (window.frameElement.radWindow)
                                            oWindow = window.frameElement.radWindow;

                                        if (oWindow != null) {
                                            console.log(oWindow);
                                            oWindow.set_visibleOnPageLoad(false);
                                            oWindow.close();
                                        }
                                    }
                                    function onClientContextMenuItemClicking(sender, args) {
                                        var menuItem = args.get_menuItem();
                                        var treeNode = args.get_node();
                                        menuItem.get_menu().hide();

                                        switch (menuItem.get_value()) {
                                            case "Rename":
                                                treeNode.startEdit();
                                                break;
                                            case "Add":
                                                break;
                                            case "Delete":
                                                var result = confirm("Are you sure you want to delete this category: " + treeNode.get_text());
                                                args.set_cancel(!result);
                                                break;
                                        }
                                    }
                                    function onClientContextMenuShowing(sender, args) {
                                        var treeNode = args.get_node();
                                        treeNode.set_selected(true);
                                        //enable/disable menu items
                                        setMenuItemsState(args.get_menu().get_items(), treeNode);
                                    }
                                    //this method disables the appropriate context menu items
                                    function setMenuItemsState(menuItems, treeNode) {
                                        for (var i = 0; i < menuItems.get_count(); i++) {
                                            var menuItem = menuItems.getItem(i);
                                            switch (menuItem.get_value()) {
                                                case "Rename":
                                                    formatMenuItem(menuItem, treeNode, 'Rename "{0}"');
                                                    break;
                                                case "Delete":
                                                    formatMenuItem(menuItem, treeNode, 'Delete "{0}"');
                                                    break;
                                                case "Add":
                                                    if (treeNode.get_parent() == treeNode.get_treeView()) {
                                                        menuItem.set_enabled(false);
                                                    }
                                                    else {
                                                        menuItem.set_enabled(true);
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                    //formats the Text of the menu item
                                    function formatMenuItem(menuItem, treeNode, formatString) {
                                        var nodeValue = treeNode.get_value();
                                        if (nodeValue && nodeValue.indexOf("_Private_") == 0) {
                                            menuItem.set_enabled(false);
                                        }
                                        else {
                                            menuItem.set_enabled(true);
                                        }
                                        var newText = String.format(formatString, extractTitleWithoutNumber(treeNode));
                                        menuItem.set_text(newText);
                                    }
                                    //removes the brackets with the numbers,e.g. Inbox (30)
                                    function extractTitleWithoutNumber(treeNode) {
                                        return treeNode.get_text().replace(/\s*\([\d]+\)\s*/ig, "");
                                    }
                                </script>
                            </telerik:RadScriptBlock>
                        </telerik:RadAjaxPanel>
                    </div>
				</telerik:RadPageView>
			</telerik:RadMultiPage>
    		<telerik:RadWindow runat="server" ID="ExplorerWindow" Behaviors="Close,Move" ShowContentDuringLoad="False" VisibleStatusbar="False" Modal="True" Width="800" Height="600"/>
	        <telerik:RadWindow runat="server" ID="FileExplorerWindow" Behaviors="Close,Move" ShowContentDuringLoad="False" VisibleStatusbar="False" Modal="True" AutoSize="True">
	            <ContentTemplate>
	                <div class="usecobranded-cb-wraper">
	                    <asp:CheckBox runat="server" ID="cbxUseCobrandedDir" Checked="False" Text="Use Cobranded directory?"/>
                    </div>
	                <telerik:RadFileExplorer runat="server" ID="rdFileExplorer" Width="800" Height="600" OnClientFileOpen="OnFileUpload"></telerik:RadFileExplorer>
                    <telerik:RadFileExplorer runat="server" ID="rdCobrandedFileExplorer" Width="800" Height="600" OnClientFileOpen="OnFileUpload" CssClass="hide"></telerik:RadFileExplorer>
	            </ContentTemplate>
	        </telerik:RadWindow>
			
			<telerik:RadAjaxLoadingPanel runat="server" ID="CustomLoadingPanel" Transparency="50" Style="background-color: #AAA"></telerik:RadAjaxLoadingPanel>

			<telerik:RadScriptBlock runat="server">
			<script type="text/javascript">
				var elemOpen;
				var lastSaved = '<%= lastSaved %>';
				var contentUpdated = false;
				var currentLangText = '';
				var frontEndUrl = '';
				var showCloneConfirm = '<%= showCloneConfirm %>';

				function DialogClickedHandler(eventValue) {
					var loadingPanel = $find('<%= CustomLoadingPanel.ClientID %>');
					if (loadingPanel && eventValue != 'cancel')
						loadingPanel.show('<%= mainContainer.ClientID %>');
					switch (eventValue) {
						case 'save':
							$('#btnSaveContent').click();
							break;
						case 'saveback':
							$('#btnSaveBack').click();
							break;
						case 'savenew':
							$('#btnSaveNew').click();
							break;
						case 'gotolang':
							$('#btnGotoLang').click();
							break;
						case 'savegolang':
							$('#btnSaveGotoLang').click();
							break;
						case 'goback':
							window.location = frontEndUrl;
							break;
						case 'savegoback':
							$('#btnSaveGoBack').click();
							break;
						case 'clonelang':
							$('#btnCloneFromLang').click();
							break;
						case 'publishall':
							$('#btnPublishAll').click();
							break;
						case 'unpublishall':
							$('#btnUnPublishAll').click();
							break;
						default:
							//loadingPanel.hide();
							break;
					}
					var oManager = GetRadWindowManager();
					//Call GetActiveWindow to get the active window 
					var oActive = oManager.getActiveWindow();
					oActive.close();
				}
				
				function GetLastSaved() {
					var timeNow = new Date().getTime();
					var saveSession = timeNow - lastSaved;
					if (saveSession > 10000) return;
					var rnObj = $find('<%= notification.ClientID %>');
					rnObj.set_text('<%= GetLocalizedText("EditContentToolBar.SaveSuccessful") %>');
					rnObj.set_autoCloseDelay(5000);
					$('#' + rnObj._popupElement.id).addClass('custom-saved-notification');
					rnObj.show();
					setTimeout(function () {
						$('#' + rnObj._popupElement.id).removeClass('custom-saved-notification');
					}, 6000);
				}
				
				function ActivateComfirmSection(sectionId) {
					$('.confirm-section').hide();
					$('#' + sectionId).show();
				}
				
				function GetCurrentLangText() {
					var combo = $find("editWindowLanguageDropDownList");
					if (combo) {
						var selectedItem = combo.get_selectedItem();
						if (selectedItem)
							currentLangText = selectedItem.get_text();
					}
				}

				//Changing Language
				function GoToLang(sender, eventArgs) {
					var changeToItemText = eventArgs._item.get_text();
					var changeToItemValue = eventArgs._item.get_value();
					if (contentUpdated) {
						$('#TargetLang').val(changeToItemValue);
						ActivateComfirmSection("save-confirm");
						
						var tmp = radconfirm("", null, 400, 200, null, currentLangText, null);
						
						//Getting rad window manager 
						var oManager = GetRadWindowManager();
						//Call GetActiveWindow to get the active window 
						var oActive = oManager.getActiveWindow();
						$(oActive._contentElement).parents('div.RadWindow').addClass('cr-custom-confirm');
						$(oActive._contentElement).parents('div.RadWindow').addClass('cr-save-confirm');
						
						eventArgs.set_cancel(true);
					}
				}
				
				//Selected another supplier
				function SupplierUpdated(sender, eventArgs) {
					contentUpdated = true;
				}
				
				function InitCloneConfirm() {
					if (showCloneConfirm == '0') return;
					var combo = $find('LanguageCloneDropDownList');
					var langItems;
					if (combo && $('#cbCloneSrcLang').find('option').length == 0) {
						langItems = combo.get_items().toArray();
						for (var i = 0; i < langItems.length; i++) {
							var newOpt = $('<option>');
							newOpt.val(langItems[i].get_value());
							newOpt.text(langItems[i].get_text());
							$('#cbCloneSrcLang').append(newOpt);
						}
					}

					ActivateComfirmSection("clonelang-confirm");
					var tmp = radconfirm("", null, 400, 200, null, currentLangText, null);
					//Getting rad window manager 
					var oManager = GetRadWindowManager();
					//Call GetActiveWindow to get the active window 
					var oActive = oManager.getActiveWindow();
					$(oActive._contentElement).parents('div.RadWindow').addClass('cr-custom-confirm');
					$(oActive._contentElement).parents('div.RadWindow').addClass('cr-clone-confirm');

					$('#cbCloneSrcLang').change(function () {
						var targetLang = $(this).find('option:selected');
						var realCombo = $find('LanguageCloneDropDownList');
						var item = realCombo.findItemByText(targetLang.text());
						if (item) {
							item.select();
						}
					});
				}

				$(window).load(function () {
					//Btn Save - Toolbar
					$('.toolbar-save').click(function (e) {
						e.preventDefault();
						ActivateComfirmSection("save-options");
						var tmp = radconfirm("", null, 400, 200, null, "Save", null);
						//Getting rad window manager 
						var oManager = GetRadWindowManager();
						//Call GetActiveWindow to get the active window 
						var oActive = oManager.getActiveWindow();
						$(oActive._contentElement).parents('div.RadWindow').addClass('cr-custom-confirm');
						return false;
					});
					
					$('.toolbar-publishall').click(function (e) {
						e.preventDefault();
						ActivateComfirmSection("publishall-confirm");
						var tmp = radconfirm("", null, 400, 200, null, "Publish All", null);
						//Getting rad window manager 
						var oManager = GetRadWindowManager();
						//Call GetActiveWindow to get the active window 
						var oActive = oManager.getActiveWindow();
						$(oActive._contentElement).parents('div.RadWindow').addClass('cr-custom-confirm');
						$(oActive._contentElement).parents('div.RadWindow').addClass('cr-publishall-confirm');
						return false;
					});
					
					$('.toolbar-unpublishall').click(function (e) {
						e.preventDefault();
						ActivateComfirmSection("unpublishall-confirm");
						var tmp = radconfirm("", null, 400, 200, null, "UnPublish All", null);
						//Getting rad window manager 
						var oManager = GetRadWindowManager();
						//Call GetActiveWindow to get the active window 
						var oActive = oManager.getActiveWindow();
						$(oActive._contentElement).parents('div.RadWindow').addClass('cr-custom-confirm');
						$(oActive._contentElement).parents('div.RadWindow').addClass('cr-unpublishall-confirm');
						return false;
					});

					GetLastSaved();

					$('.content-field-group').each(function () {
						$(this).find('input').change(function() {
							contentUpdated = true;
						});
					});

					GetCurrentLangText();

					InitCloneConfirm();
				});
				
				$(function () {
					//Button BACK
					$('.toolbar-back').click(function(e) {
						if (!contentUpdated) return;
						frontEndUrl = $(this).attr('href');
						e.preventDefault();
						ActivateComfirmSection("saveback-confirm");

						var tmp = radconfirm("", null, 400, 200, null, currentLangText, null);

						//Getting rad window manager 
						var oManager = GetRadWindowManager();
						//Call GetActiveWindow to get the active window 
						var oActive = oManager.getActiveWindow();
						$(oActive._contentElement).parents('div.RadWindow').addClass('cr-custom-confirm');
						$(oActive._contentElement).parents('div.RadWindow').addClass('cr-saveback-confirm');
					});
					
			    	$('.openImgExplorer').click(function () {
			            OpenFileExplorerDialog();
			            elemOpen = this;
			            return false;
			        });
			        
			        $('.openFileExplorer').click(function () {
			            var wnd = $find("<%= FileExplorerWindow.ClientID %>");
			            wnd.show();
			            elemOpen = this;
			            return false;
			        });

			        //set width and background color for radeditor
			        jQuery('div.reWrapper').width("700px");
			        jQuery('div.reWrapper iframe body').css("background-color: #fff");

			        var clicked = false;
			        //disable command button when click
			        jQuery('.editcontent-btn').click(function () {
			            if (clicked == true)
			                return false;
			            clicked = true;
			        });

				    //Cobranded section ================================
				    //Toggle cobranded folder when click checkbox
			        $("#<%= cbxUseCobrandedDir.ClientID %>").click(function() {
                        if (this.checked) {
                            $("#<%= rdFileExplorer.ClientID %>").hide();
                            $("#<%= rdCobrandedFileExplorer.ClientID %>").show();
                        } else {
                            $("#<%= rdFileExplorer.ClientID %>").show();
                            $("#<%= rdCobrandedFileExplorer.ClientID %>").hide();
                        }
			        });
				    //Display cobranded folder by default
				    if ($("#<%= cbxUseCobrandedDir.ClientID %>").attr('checked')) {
				        $("#<%= rdFileExplorer.ClientID %>").hide();
				        $("#<%= rdCobrandedFileExplorer.ClientID %>").show();
				    } else {
				        $("#<%= rdFileExplorer.ClientID %>").show();
				        $("#<%= rdCobrandedFileExplorer.ClientID %>").hide();
				    }
				    //End Cobranded section =============================
			       
			    });
			   

			    function OpenFileExplorerDialog() {
			        var wnd = $find("<%= ExplorerWindow.ClientID %>");
			        wnd.show();
			    }
			    
			    function OnFileUpload(sender, args) {
			        var item = args.get_item();
			        if (item && !item.isDirectory()) {
			            args.set_cancel(true);
			            var file = args.get_path();
			            var inputUrl = $(elemOpen).parent().find('input')[0];
			            $(inputUrl).val(file);
			            var wnd = $find("<%= FileExplorerWindow.ClientID %>");
			            wnd.close();
			        }
			    }

			    //This function is called from a code declared on the ImageFileDialog.aspx page
			    function OnFileSelected(fileSelected) {
			        var inputUrl = $(elemOpen).parent().find('input')[0];
			        $(inputUrl).val(fileSelected);
			        //textbox.set_value(fileSelected);
			    }
			</script>
			</telerik:RadScriptBlock>

			<asp:Literal runat="server" ID="postbackScript" Visible="False">
			    <script type="text/javascript">
    			    OnClientClick();
    			</script>
			</asp:Literal>
		</asp:Panel>