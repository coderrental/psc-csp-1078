﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI.FileExplorer;

namespace TIEKinetix.CspModules.CSPContentManagementSystem
{
	public partial class ImageFileDialog : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			fileManager.ExplorerMode = FileExplorerMode.Thumbnails;
			fileManager.Configuration.SearchPatterns = new string[] { "*.jpg", "*.jpeg", "*.gif", "*.png", "*.pdf" }; //Allow user upload image only
			fileManager.Configuration.MaxUploadFileSize = 10240000 *5;
			if (!string.IsNullOrEmpty(Request.Params["fileroot"]))
			{
			    string physicalPath = Server.MapPath("~/" + Request.Params["fileroot"]); //Create path if this path does not exists
                if (!Directory.Exists(physicalPath))
                    Directory.CreateDirectory(physicalPath);
				fileManager.Configuration.UploadPaths =fileManager.Configuration.ViewPaths = fileManager.Configuration.DeletePaths = new[] {"~/"+Request.Params["fileroot"]};
			}
		}
	}
}