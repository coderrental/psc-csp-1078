﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConfigureContentTypeFields.ascx.cs" Inherits="TIEKinetix.CspModules.CSPContentManagementSystem.ConfigureContentTypeFields" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadToolBar ID="toolBar" runat="server" AutoPostBack="true" CssClass="property-toolbar">
    <Items>            
    </Items>
</telerik:RadToolBar>

<div class="content-type-breadcrumb">
    <asp:Literal runat="server" ID="lbCurrentContentType"></asp:Literal>
</div>
    
<telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server" CssClass="ajax-loading-panel" IsSticky="true" Transparency="50"></telerik:RadAjaxLoadingPanel>
<telerik:RadAjaxPanel ID="ajaxPanel" runat="server" LoadingPanelID="loadingPanel">
    <telerik:RadTabStrip ID="tabs" runat="server" MultiPageID="pages" SelectedIndex="0" Skin="Outlook">
        <Tabs>
            <telerik:RadTab PageViewID="pageFieldSettings" Text="Field Settings" Font-Bold="true" Value="FieldSettings"></telerik:RadTab>
            <telerik:RadTab PageViewID="pageContentTypeRelation" Text="Content Type Relation" Font-Bold="true" Value="Relations"></telerik:RadTab>
            <telerik:RadTab PageViewID="pageContentTypeTranslation" Text="Content Definition Translation" Font-Bold="true" Value="Translations"></telerik:RadTab>
            <telerik:RadTab PageViewID="pageContentTypeException" Text="Content Type Exception" Font-Bold="true" Value="Exceptions"></telerik:RadTab>
            <telerik:RadTab PageViewID="pageUserContentTypeException" Text="User Exception" Font-Bold="true" Value="UserExceptions"></telerik:RadTab>
            <telerik:RadTab PageViewID="pageConsumerException" Text="Consumer Exception" Font-Bold="true" Value="ConsumerExceptions"></telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="pages" runat="server" SelectedIndex="0">
        <%--pageFieldSettings--%>
        <telerik:RadPageView ID="pageFieldSettings" runat="server">
            <div class="contentGridContainer">
                <%-- field grid --%>       
                <telerik:RadGrid ID="grid" runat="server" AllowSorting="true" AllowFilteringByColumn="false" Width="90%" OnItemCommand="GridOnItemCommandHandler">
                        <MasterTableView AutoGenerateColumns="false">
                            <Columns>
                                <telerik:GridEditCommandColumn ButtonType="ImageButton" HeaderStyle-Width="50px"></telerik:GridEditCommandColumn>
                                <telerik:GridBoundColumn DataField="ContentTypeFieldName" HeaderStyle-Width="50%" HeaderText="Field Name"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ContentTypeFieldType" HeaderText="Type" ></telerik:GridBoundColumn>
                                <telerik:GridcheckBoxColumn DataField="Required" HeaderText="Required?" ></telerik:GridcheckBoxColumn>
                                <telerik:GridcheckBoxColumn DataField="Readonly" HeaderText="Read Only?" ></telerik:GridcheckBoxColumn >
                                <telerik:GridcheckBoxColumn DataField="Visible" HeaderText="Visible?" ></telerik:GridcheckBoxColumn >
                                <telerik:GridBoundColumn DataField="DisplayOrder" HeaderText="Order" ></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DisplayGroupOrder" HeaderText="Group Order" ></telerik:GridBoundColumn>
                            </Columns>
                            <EditFormSettings EditFormType="WebUserControl"></EditFormSettings>
                        </MasterTableView> 
                    </telerik:RadGrid>
                <%-- end of field grid --%>       
            </div>
        </telerik:RadPageView>
        <%--end of pageFieldSettings--%>
        <%--pageContentTypeRelation--%>
        <telerik:RadPageView ID="pageContentTypeRelation" runat="server">
            <telerik:RadToolBar runat="server" ID="relationToolbar" CssClass="relation-toolbar" Width="100%">
                <Items>                    
                </Items>
            </telerik:RadToolBar>
            <telerik:RadSplitter runat="server" Width="100%" BorderStyle="None" BorderWidth="0" BorderSize="0">
                <telerik:RadPane ID="leftPane" runat="server" Width="300">
                    <div class="content-field-group">
                        <telerik:RadTreeView runat="server" ID="contentTypeRelationTree" Skin="Default" ShowLineImages="true" OnNodeClick="TreeViewOnNodeClick"></telerik:RadTreeView>
                    </div>
                </telerik:RadPane>
                <telerik:RadPane ID="rightPane" runat="server" Scrolling="None">
                    <asp:Panel runat="server" ID="relationPanel" Visible="false">                        
                        <asp:HiddenField runat="server" ID="hfKey" />                        
                        <table>
                            <tbody>
                                <tr>
                                    <td class="relation-label">
                                        <asp:Label runat="server" ID="lbParentKeyField"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlParentKeyField">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="relation-label">
                                        <asp:Label runat="server" ID="lbChildKeyField"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlChildKeyField">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </asp:Panel>
                </telerik:RadPane>
            </telerik:RadSplitter>            
        </telerik:RadPageView>
        <%--end of pageContentTypeRelation--%>
        <%--pageContentTypeTranslation--%>
        <telerik:RadPageView ID="pageContentTypeTranslation" CssClass="page-margin-5" runat="server">
            <table style="width:100%">
                <thead>
                    <tr>
                        <td colspan="2">
                            <telerik:RadToolBar runat="server" ID="translationToolbar" Width="100%"></telerik:RadToolBar>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td nowrap="nowrap" style="width:10%"><asp:Label runat="server" ID="lbContentTypeShortDesc"></asp:Label></td>
                        <td style="width: 80%"><asp:TextBox runat="server" ID="tbContentTypeShortDesc" Width="50%"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td><asp:Label runat="server" ID="lbContentTypeLongDesc"></asp:Label></td>
                        <td><asp:TextBox runat="server" ID="tbContentTypeLongDesc" TextMode="MultiLine" Rows="5"  Width="50%"></asp:TextBox></td>
                    </tr>
                </tbody>
            </table>
        </telerik:RadPageView>
        <%--end of pageContentTypeTranslation--%>
        <%--pageContentTypeException--%>
        <telerik:RadPageView runat="server" ID="pageContentTypeException">
            <telerik:RadToolBar runat="server" ID="contentExceptionToolbar" Width="100%" Height="30px">
                <Items></Items>
            </telerik:RadToolBar>
            <telerik:RadSplitter runat="server" Orientation="Vertical" Width="100%">
                <telerik:RadPane ID="paneSupplier" runat="server">
                    <telerik:RadTreeView ID="supplierTree" runat="server" CheckBoxes="true" CheckChildNodes="true" OnClientNodeClicked="CategoryTreeNodeClicked">
                        <DataBindings>
                            <telerik:RadTreeNodeBinding Expanded="true" />
                        </DataBindings>                        
                    </telerik:RadTreeView>
                </telerik:RadPane>
                <telerik:RadSplitBar ID="RadSplitBar0" runat="server" CollapseMode="None"></telerik:RadSplitBar>
                <telerik:RadPane ID="paneLanguage" runat="server">
                    <telerik:RadTreeView ID="languageTree" runat="server" CheckBoxes="true" CheckChildNodes="true" OnClientNodeClicked="CategoryTreeNodeClicked">
                        <DataBindings>
                            <telerik:RadTreeNodeBinding Expanded="true" />
                        </DataBindings>                                                
                    </telerik:RadTreeView>
                </telerik:RadPane>
                <telerik:RadSplitBar ID="RadSplitBar1" runat="server" CollapseMode="None"></telerik:RadSplitBar>
                <telerik:RadPane ID="paneCategory" runat="server" Width="50%">
                    <telerik:RadTreeView ID="categoryTree" runat="server" CheckBoxes="true" CheckChildNodes="true" Skin="Default" OnClientNodeClicked="CategoryTreeNodeClicked">
                        <DataBindings>
                            <telerik:RadTreeNodeBinding Expanded="true" />
                        </DataBindings>                        
                    </telerik:RadTreeView>                    
                </telerik:RadPane>
            </telerik:RadSplitter>
        </telerik:RadPageView>
        <%--end pageContentTypeException--%>
        <%--pageConsumerException--%>
        <telerik:RadPageView ID="pageConsumerException" runat="server">
            <telerik:RadToolBar runat="server" ID="consumerExceptionToolbar" Width="100%" Height="30px">
                <Items></Items>
            </telerik:RadToolBar>
            <telerik:RadTreeView runat="server" ID="consumerExceptionTreeView" CheckBoxes="true" CheckChildNodes="true" Skin="Default" OnClientNodeClicked="CategoryTreeNodeClicked">
                <DataBindings>
                    <telerik:RadTreeNodeBinding Expanded="true" />
                </DataBindings>                        
            </telerik:RadTreeView>
        </telerik:RadPageView>
        <%--end pageConsumerException--%>
        <%--pageUserContentTypeException--%>
        <telerik:RadPageView ID="pageUserContentTypeException" runat="server">
            <telerik:RadToolBar runat="server" ID="userExceptionToolBar" Width="100%" Height="30px">
                <Items></Items>
            </telerik:RadToolBar>
            <telerik:RadSplitter ID="RadSplitter1" runat="server" Orientation="Vertical" Width="100%">
                <telerik:RadPane ID="panelUserSupplier" runat="server">
                    <telerik:RadTreeView ID="userSupplierTree" runat="server" CheckBoxes="true" CheckChildNodes="true" OnClientNodeClicked="CategoryTreeNodeClicked">
                        <DataBindings>
                            <telerik:RadTreeNodeBinding Expanded="true" />
                        </DataBindings>                        
                    </telerik:RadTreeView>
                </telerik:RadPane>
                <telerik:RadSplitBar ID="RadSplitBar2" runat="server" CollapseMode="None"></telerik:RadSplitBar>
                <telerik:RadPane ID="panelUserLng" runat="server">
                    <telerik:RadTreeView ID="userLngTree" runat="server" CheckBoxes="true" CheckChildNodes="true" OnClientNodeClicked="CategoryTreeNodeClicked">
                        <DataBindings>
                            <telerik:RadTreeNodeBinding Expanded="true" />
                        </DataBindings>                                                
                    </telerik:RadTreeView>
                </telerik:RadPane>
                <telerik:RadSplitBar ID="RadSplitBar3" runat="server" CollapseMode="None"></telerik:RadSplitBar>
                <telerik:RadPane ID="panelUserCategories" runat="server" Width="50%">
                    <telerik:RadTreeView ID="userCategoryTree" runat="server" CheckBoxes="true" CheckChildNodes="true" Skin="Default" OnClientNodeClicked="CategoryTreeNodeClicked">
                        <DataBindings>
                            <telerik:RadTreeNodeBinding Expanded="true" />
                        </DataBindings>                        
                    </telerik:RadTreeView>                    
                </telerik:RadPane>
            </telerik:RadSplitter>
        </telerik:RadPageView>
        <%--end of pageUserContentTypeException--%>
    </telerik:RadMultiPage>    

    <telerik:RadCodeBlock runat="server">
        <script type="text/javascript">
            function onClientDoubleClick(sender, args) {
                console.log(arguments);
                window.radopen(null, "addRelationWindow");
            }
            function CategoryTreeNodeClicked(sender, args) {
                var node = args.get_node();
                node.check();
               }
        </script>
    </telerik:RadCodeBlock>
</telerik:RadAjaxPanel>
