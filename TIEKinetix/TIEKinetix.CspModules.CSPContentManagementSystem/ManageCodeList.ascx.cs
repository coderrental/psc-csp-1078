﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Data;
using Telerik.Web.UI;
using TIEKinetix.CspModules.CSPContentManagementSystem.Components;
using TIEKinetix.CspModules.CSPContentManagementSystem.Components.Custom;
using TIEKinetix.CspModules.Data;

namespace TIEKinetix.CspModules.CSPContentManagementSystem
{
	public partial class ManageCodeList : DnnModuleBase
	{
		private DnnDataContext _dnnDataContext;
		/// <summary>
		/// Handles the Init event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected override void Page_Init(object sender, EventArgs e)
		{
			base.Page_Init(sender, e);
			//Init toolbar
			InitToolbar();
		}

		

		#region [ Init Components ]
		/// <summary>
		/// Inits the toolbar.
		/// </summary>
		private void InitToolbar()
		{
			toptoolbar.Items.Add(new RadToolBarButton
			                     	{
										Text = GetLocalizedText("Label.BackToContentType"),
			                     		PostBack = false,
										ImageUrl = ControlPath + "images/arrow_left.png",
										NavigateUrl = Globals.NavigateURL(TabId,""),
										ToolTip = GetLocalizedText("Label.BackToContentType")
			                     	});
		}
		#endregion


		#region [ Private Methods ]
		/// <summary>
		/// Shows a <see cref="RadWindow"/> alert if an error occurs
		/// </summary>
		private void ShowErrorMessage(string msg)
		{
			ajaxManager.ResponseScripts.Add(string.IsNullOrEmpty(msg)
			                                	? string.Format("window.radalert(\"Please enter valid data!\")")
			                                	: string.Format("window.radalert(\"{0}\")", msg));
		}
		/// <summary>
		/// Shows the error message.
		/// </summary>
		private void ShowErrorMessage()
		{
			ShowErrorMessage(string.Empty);
		}

		#endregion
		
		#region [ Event Handlers]
		/// <summary>
		/// Handles the OnItemInserted event of the grCodeList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Telerik.Web.UI.GridInsertedEventArgs"/> instance containing the event data.</param>
		protected void grCodeList_OnItemInserted(object sender, GridInsertedEventArgs e)
		{
			if (e.Exception != null)
			{
				e.ExceptionHandled = true;
				ShowErrorMessage();
			}
		}

		/// <summary>
		/// Handles the OnItemUpdated event of the grCodeList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Telerik.Web.UI.GridUpdatedEventArgs"/> instance containing the event data.</param>
		protected void grCodeList_OnItemUpdated(object sender, GridUpdatedEventArgs e)
		{
			if (e.Exception != null)
			{
				e.ExceptionHandled = true;
				ShowErrorMessage();
			}
		}

		/// <summary>
		/// Handles the OnItemDeleted event of the grCodeList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Telerik.Web.UI.GridDeletedEventArgs"/> instance containing the event data.</param>
		protected void grCodeList_OnItemDeleted(object sender, GridDeletedEventArgs e)
		{
			if (e.Exception != null)
			{
				e.ExceptionHandled = true;
				if (e.Exception.ToString().Contains("conflict"))
					ShowErrorMessage(GetLocalizedText("Label.CanDeleteCodelist"));
				else
					ShowErrorMessage();
			}
		}

		/// <summary>
		/// Handles the OnUpdating event of the lqsCodeList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.LinqDataSourceUpdateEventArgs"/> instance containing the event data.</param>
		protected void lqsCodeList_OnUpdating(object sender, LinqDataSourceUpdateEventArgs e)
		{
			var m_NewObject = (CSPCMS_CodeList)e.NewObject;
			//Update date changed
			m_NewObject.DateChanged = DateTime.Now;
		}
		/// <summary>
		/// Handles the OnInserting event of the lqsCodeList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.LinqDataSourceInsertEventArgs"/> instance containing the event data.</param>
		protected void lqsCodeList_OnInserting(object sender, LinqDataSourceInsertEventArgs e)
		{
			var m_NewObject = (CSPCMS_CodeList)e.NewObject;
			m_NewObject.CSPCMS_CodeList_Id = Guid.NewGuid();
			//Update date changed, date created
			m_NewObject.DateCreated = DateTime.Now;
			m_NewObject.DateChanged = DateTime.Now;
		}

		/// <summary>
		/// Handles the OnInserting event of the lqsCodeValue control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void lqsCodeValue_OnInserting(object sender, LinqDataSourceInsertEventArgs e)
		{
			var m_NewObject = (CSPCMS_CodeListValue) e.NewObject;
			m_NewObject.CSPCMS_CodeListValue_Id = Guid.NewGuid();
		}
		#endregion

		/// <summary>
		/// Handles the OnContextCreating event of the lqsCodeList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.LinqDataSourceContextEventArgs"/> instance containing the event data.</param>
		protected void lqsCodeList_OnContextCreating(object sender, LinqDataSourceContextEventArgs e)
		{
			e.ObjectInstance = new DnnDataContext(Config.GetConnectionString());
		}

		/// <summary>
		/// Handles the OnContextCreating event of the lqsCodeValue control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.LinqDataSourceContextEventArgs"/> instance containing the event data.</param>
		protected void lqsCodeValue_OnContextCreating(object sender, LinqDataSourceContextEventArgs e)
		{
			e.ObjectInstance = new DnnDataContext(Config.GetConnectionString());
		}
	}
}