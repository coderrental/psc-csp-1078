﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Data;
using Telerik.Web.UI;
using TIEKinetix.CspModules.CSPContentManagementSystem.Components;
using TIEKinetix.CspModules.CSPContentManagementSystem.Components.Custom;
using TIEKinetix.CspModules.Data;

namespace TIEKinetix.CspModules.CSPContentManagementSystem
{
    /// <summary>
    /// this page is designed to handle field configurations for administrators
    /// </summary>
	public partial class ConfigureContentTypeFields : DnnModuleBase
    {
        protected content_types_field[] Fields;
        private CSPCMS_ContentType _contentType;
    	private DnnDataContext _dnnContext;
        private int _contentTypeId;
        private bool _setActive = true;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #region Overrides of DnnModuleBase

        /// <summary>
        /// page init event: Initialize LINQ contexts and render ASP.NET components
        /// </summary>
        /// <param name="sender">Page</param>
        /// <param name="e">Event</param>
        protected override void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
		

            _dnnContext = new DnnDataContext(Config.GetConnectionString());
            if (!string.IsNullOrEmpty(Request.QueryString["setActive"]) && Request.QueryString["setActive"].Equals("false"))
                _setActive = false;

            // get content type id
            try
            { // try to get as int
                _contentTypeId = Convert.ToInt32(Request.QueryString.Get("id"));
                _contentType = _dnnContext.CSPCMS_ContentTypes.FirstOrDefault(a => a.ContentTypeId == _contentTypeId && a.PortalId == PortalId && a.TabModuleId == TabModuleId);
            }
            catch
            { // try to get as guid
                Guid guid = new Guid(Request.QueryString["id"]);
                _contentType = _dnnContext.CSPCMS_ContentTypes.FirstOrDefault(a => a.CSPCMS_ContentType_Id == guid);
                _contentTypeId = _contentType.ContentTypeId;
            }

            //breadcrumb
            if (_contentType != null)
                lbCurrentContentType.Text = string.Format(GetLocalizedText("Configure.Breadcrumb"), _contentType.CSPCMS_Translations.Count(a => a.CultureCode == CurrentLanguage) > 0 ? _contentType.CSPCMS_Translations.First().ShortDesc : _contentType.ContentTypeName);

            // settings
            if (_contentType!=null && !_contentType.Active && _setActive)
            {
                foreach (CSPCMS_ContentType cspcmsContentType in _dnnContext.CSPCMS_ContentTypes.Where(a=>a.Active && a.PortalId == PortalId && a.TabModuleId == TabModuleId))
                {
                    cspcmsContentType.Active = false;
                }
                _contentType.Active = true;
                _dnnContext.SubmitChanges();
            }

            Fields = CspDataContext.content_types_fields.Where(a => a.content_types_Id == _contentTypeId).ToArray();
            CSPCMS_ContentType contentType = ProcessCspFields(Fields, _contentType);

            // bind data source
            grid.DataSource = contentType.CSPCMS_ContentTypeFields.OrderBy(a=>a.DisplayGroupOrder).OrderBy(a=>a.DisplayOrder);
            grid.MasterTableView.DataKeyNames = new string[] { "ContentTypeId", "ContentTypeFieldId", "CSPCMS_ContentTypeField_Id", "CSPCMS_ContentType_Id" };

            // main tool bar
            toolBar.Items.Add(new RadToolBarButton
            {
                Text = GetLocalizedText("ToolBar.TranslateContentTypeFields"),
                ImageUrl = ControlPath + "images/pencil.png",
				NavigateUrl = Globals.NavigateURL(TabId, "Translate", "mid", ModuleId.ToString(), "id", _contentTypeId.ToString()),
				PostBack = false,
                Value = "TranslateContentFields"
            });
            toolBar.Items.Add(new RadToolBarButton { IsSeparator = true });
            toolBar.Items.Add(new RadToolBarButton
                                  {
                                      Text = GetLocalizedText("ToolBar.SelectContentType"),
                                      ImageUrl = ControlPath + "images/arrow_left.png",
									  NavigateUrl = Globals.NavigateURL() + "?retry=true",
									  PostBack = false,
                                      Value = "Cancel"
                                  });
            toolBar.Items.Add(new RadToolBarButton {IsSeparator = true});
            toolBar.Items.Add(new RadToolBarButton
            {
                Text = GetLocalizedText("ToolBar.DeleteCurrentContentTypeSettings"),
                ImageUrl = ControlPath + "images/delete.png",
                Value = "Delete"
            });

			toolBar.Items.Add(new RadToolBarButton { IsSeparator = true });
			toolBar.Items.Add(new RadToolBarButton
			{
				Text = GetLocalizedText("ToolBar.ManageCodeList"),
				ImageUrl = ControlPath + "images/cog.png",
				PostBack = false,
				NavigateUrl = Globals.NavigateURL(TabId,"ManageCodelist","mid",ModuleId.ToString())
			});

			toolBar.Items.Add(new RadToolBarButton { IsSeparator = true });
			toolBar.Items.Add(new RadToolBarButton
			{
				Text = GetLocalizedText("ToolBar.ManageValidate"),
				ImageUrl = ControlPath + "images/cog.png",
				PostBack = false,
				NavigateUrl = Globals.NavigateURL(TabId, "ManageValidation", "mid", ModuleId.ToString())
			});

            toolBar.ButtonClick += new RadToolBarEventHandler(ToolBarButtonClick);

            // tab translation
            tabs.Tabs.FindTabByValue("FieldSettings").Text = GetLocalizedText("Tabs.FieldSettings");
            tabs.Tabs.FindTabByValue("Relations").Text = GetLocalizedText("Tabs.Relations");
            tabs.Tabs.FindTabByValue("Exceptions").Text = GetLocalizedText("Tabs.ContentTypeExceptions");
            tabs.Tabs.FindTabByValue("ConsumerExceptions").Text = GetLocalizedText("Tabs.ConsumerExceptions");
            tabs.Tabs.FindTabByValue("UserExceptions").Text = GetLocalizedText("Tabs.UserExceptions");

            #region Relation Keys Tab
            // content type relation toolbar
            relationToolbar.Items.Add(new RadToolBarButton
            {
                Text = GetLocalizedText("RelationToolBar.Add"),
                ImageUrl = ControlPath + "images/plus.png",
                Value = "Add"
            });
            relationToolbar.Items.Add(new RadToolBarButton
            {
                Text = GetLocalizedText("RelationToolBar.Save"),
                ImageUrl = ControlPath + "images/save.png",
                Value = "Save"
            });
            relationToolbar.Items.Add(new RadToolBarButton
            {
                Text = GetLocalizedText("RelationToolBar.EditContentType"),
                ImageUrl = ControlPath + "images/pencil.png",
                Value = "Edit"
            });
            relationToolbar.Items.Add(new RadToolBarButton { IsSeparator = true });
            relationToolbar.Items.Add(new RadToolBarButton
            {
                Text = GetLocalizedText("RelationToolBar.Delete"),
                ImageUrl = ControlPath + "images/delete.png",
                Value = "Delete"
            });
            relationToolbar.ButtonClick += new RadToolBarEventHandler(RelationToolbarButtonClick);

            RenderNestedTree();

            // content type relation form
            lbParentKeyField.Text = GetLocalizedText("EditRelation.ParentKeyField");
            lbChildKeyField.Text = GetLocalizedText("EditRelation.ChildKeyField");
            #endregion

            #region Translation Tabs

            lbContentTypeShortDesc.Text = GetLocalizedText("Translations.ShortDesc");
            lbContentTypeLongDesc.Text = GetLocalizedText("Translations.LongDesc");

            if (_contentType != null)
            {
                CSPCMS_Translation ctTranslation = _contentType.CSPCMS_Translations.FirstOrDefault(a => a.CultureCode == CurrentLanguage);
                tbContentTypeShortDesc.Text = ctTranslation != null ? ctTranslation.ShortDesc : _contentType.ContentTypeName;
                tbContentTypeLongDesc.Text = ctTranslation != null ? ctTranslation.LongDesc : _contentType.ContentTypeName;
            }

            translationToolbar.Items.Add(new RadToolBarButton
                                             {
                                                 Text = GetLocalizedText("Translations.Save"),
                                                 ImageUrl = ControlPath + "images/save.png",
                                                 Value = "Save"
                                             });
            translationToolbar.ButtonClick += new RadToolBarEventHandler(TranslationToolbarButtonClick);
            #endregion

            #region Exception Tabs

            int userId = -1;
            #region content type exception tab
            // tool bar
            contentExceptionToolbar.Items.Add(new RadToolBarButton
                                                  {
                                                      Text = GetLocalizedText("ContentTypeException.Save"),
                                                      ImageUrl = ControlPath + "images/save.png",
                                                      Value = "Save"
                                                  });
            contentExceptionToolbar.ButtonClick += new RadToolBarEventHandler(ContentExceptionToolbarButtonClick);
            #endregion
            
            #region user exceptions - consumer exeptions
			/*
			 * User Exception
			 */ 
            userExceptionToolBar.Items.Add(new RadToolBarButton
            {
				Text = GetLocalizedText("UserExceptionToolBar.Save"),
                ImageUrl = ControlPath + "images/save.png",
                Value = "Save"
            });

            RadComboBox cbUsers = new RadComboBox
            {
                ID = "UserComboBox",
                Filter = RadComboBoxFilter.Contains,
                AutoPostBack = true
            };
            cbUsers.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ComboBoxUsersSelectedIndexChanged);
			userExceptionToolBar.Controls.Add(cbUsers);
			userExceptionToolBar.ButtonClick += new RadToolBarEventHandler(UserExceptionToolBarButtonClick);

			/*
			 * Consumer exceptions
			 */ 
			// note: should handle the presentation of the consumer exception list better.
			consumerExceptionToolbar.Items.Add(new RadToolBarButton
			{
				Text = GetLocalizedText("ConsumerExceptionToolBar.Save"),
				ImageUrl = ControlPath + "images/save.png",
				Value = "Save"
			});
			consumerExceptionToolbar.Items.Add(new RadToolBarButton
			{
				Text = GetLocalizedText("ConsumerExceptionToolBar.Delete"),
				ImageUrl = ControlPath + "images/24x24/delete2.png",
				Value = "Delete"
			});
			consumerExceptionToolbar.ButtonClick += new RadToolBarEventHandler(ConsumerExceptionToolBarButtonClick);
			// store list of DNN users in a combo box
			RadComboBox cbConsumerExceptionUsers = new RadComboBox
			{
				ID = "ConsumerExceptionUserComboBox",
				Filter = RadComboBoxFilter.Contains,
				AutoPostBack = true
			};
			cbConsumerExceptionUsers.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ComboBoxConsumerExceptionUsersSelectedIndexChanged);
			consumerExceptionToolbar.Controls.Add(cbConsumerExceptionUsers);

			foreach (company company in CspDataContext.companies.Where(a => a.is_consumer.HasValue && a.is_consumer.Value))
			{
				consumerExceptionTreeView.Nodes.Add(new RadTreeNode
				{
					Text = company.companyname,
					Value = company.companies_Id.ToString(),
					Checked = _contentType.CSPCMS_ExceptionConsumers.Count(a => a.ConsumerId == company.companies_Id && a.UserId == userId) > 0
				});
			}

			
			// Fill user info to combobox in user - consumer exeption
			var m_UsersDataReader = DataProvider.Instance().ExecuteSQL(string.Format("select u.Username, u.UserID from users u inner join UserPortals up on u.UserID = up.UserId where up.PortalId = {0}",PortalId));
			userId = -1; // reset userId variable.
			while (m_UsersDataReader.Read())
        	{
				cbUsers.Items.Add(new RadComboBoxItem
				{
					Text = m_UsersDataReader["Username"].ToString(),
					Value = m_UsersDataReader["UserID"].ToString()
				});

				// populate user data
				
					if (userId == -1)
						userId = int.Parse(m_UsersDataReader["UserID"].ToString());
				cbConsumerExceptionUsers.Items.Add(new RadComboBoxItem
				{
					Text = m_UsersDataReader["Username"].ToString(),
					Value = m_UsersDataReader["UserID"].ToString()
				});
        	}
			m_UsersDataReader.Close();
			 
            #endregion

            //load suppliers)
            foreach (var supplier in CspDataContext.companies.Where(a=>a.is_supplier.HasValue && a.is_supplier.Value))
            {
                supplierTree.Nodes.Add(new RadTreeNode
                                           {
                                               Text = supplier.companyname, 
                                               Value = supplier.companies_Id.ToString(),
                                               Checked = _contentType!=null && _contentType.CSPCMS_ExceptionSuppliers.Count(a => a.SupplierId == supplier.companies_Id) > 0
                                           });
                userSupplierTree.Nodes.Add(new RadTreeNode
                {
                    Text = supplier.companyname,
                    Value = supplier.companies_Id.ToString(),
                    Checked = _contentType != null && _contentType.CSPCMS_UserExceptionSuppliers.Count(a => a.SupplierId == supplier.companies_Id && a.UserId == userId) > 0
                });
            }
            //load languages
            foreach (var language in CspDataContext.languages.Where(a=>a.active.HasValue && a.active.Value))
            {
                languageTree.Nodes.Add(new RadTreeNode
                                           {
                                               Text = language.description, 
                                               Value = language.languages_Id.ToString(),
                                               Checked = _contentType != null && _contentType.CSPCMS_ExceptionLanguages.Count(a => a.LanguageId == language.languages_Id) > 0
                                           });
                userLngTree.Nodes.Add(new RadTreeNode
                {
                    Text = language.description,
                    Value = language.languages_Id.ToString(),
                    Checked = _contentType != null && _contentType.CSPCMS_UserExceptionLanguages.Count(a => a.LanguageId == language.languages_Id && a.UserId == userId) > 0
                });
            }

            RenderCategoryTree(categoryTree);
            RenderCategoryTree(userCategoryTree);

            foreach (RadTreeNode node in _contentType.CSPCMS_ExceptionCategories.Select(exceptionCategory => categoryTree.FindNodeByValue(exceptionCategory.CategoryId.ToString())).Where(node => node != null))
            {
                node.Checked = true;
            }

            foreach (var node in _contentType.CSPCMS_UserExceptionCategories.Where(a=>a.UserId==userId))
            {
                userCategoryTree.FindNodeByValue(node.CategoryId.ToString()).Checked = true;
            }


            #endregion
        }


        #endregion

        #region Event Handlers

        /// <summary>
        /// consumer exception combobox event handler: users select a different user. clear the previous selection and re-populate the consumer tree with user data
        /// </summary>
        /// <param name="sender">cbConsumerExceptionUsers</param>
        /// <param name="e">event</param>
        void ComboBoxConsumerExceptionUsersSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox cbUsers = (RadComboBox)consumerExceptionToolbar.FindControl("ConsumerExceptionUserComboBox");
            int userId = Convert.ToInt32(cbUsers.SelectedValue);
            RadTreeNode node;
            consumerExceptionTreeView.UncheckAllNodes();
            foreach (CSPCMS_ExceptionConsumer exceptionConsumer in _contentType.CSPCMS_ExceptionConsumers.Where(a => a.UserId == userId))
            {
                node = consumerExceptionTreeView.FindNodeByValue(exceptionConsumer.ConsumerId.ToString());
                if (node != null)
                    node.Checked = true;
            }
        }

        /// <summary>
        /// user combobox event handler: update user exception suppliers/languages/categories when user index changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ComboBoxUsersSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox cbUsers = (RadComboBox)userExceptionToolBar.FindControl("UserComboBox");
            int userId = Convert.ToInt32(cbUsers.SelectedValue);
            RadTreeNode node;
            //load suppliers)
            userSupplierTree.UncheckAllNodes();
            foreach (CSPCMS_UserExceptionSupplier supplier in _contentType.CSPCMS_UserExceptionSuppliers.Where(a => a.UserId == userId))
            {
                node = userSupplierTree.FindNodeByValue(supplier.SupplierId.ToString());
                if (node != null)
                    node.Checked = true;
            }
            //load languages
            userLngTree.UncheckAllNodes();
            foreach (CSPCMS_UserExceptionLanguage lng in _contentType.CSPCMS_UserExceptionLanguages.Where(a => a.UserId == userId))
            {
                node = userLngTree.FindNodeByValue(lng.LanguageId.ToString());
                if (node != null)
                    node.Checked = true;
            }

            userCategoryTree.UncheckAllNodes();
            foreach (var category in _contentType.CSPCMS_UserExceptionCategories.Where(a => a.UserId == userId))
            {
                node = userCategoryTree.FindNodeByValue(category.CategoryId.ToString());
                if (node != null)
                    node.Checked = true;
            }
        }

        /// <summary>
        /// user exception toolbar event handler
        /// </summary>
        /// <param name="sender">user exception toolbar</param>
        /// <param name="e">event</param>
        void UserExceptionToolBarButtonClick(object sender, RadToolBarEventArgs e)
        {
            RadComboBox cbUsers = (RadComboBox)userExceptionToolBar.FindControl("UserComboBox");
            int userId = Convert.ToInt32(cbUsers.SelectedValue);

            switch (e.Item.Value)
            {
                case "Save":
                    _dnnContext.CSPCMS_UserExceptionSuppliers.DeleteAllOnSubmit(_dnnContext.CSPCMS_UserExceptionSuppliers.Where(a => a.UserId == userId));
                    foreach (RadTreeNode node in userSupplierTree.CheckedNodes)
                    {
                        _dnnContext.CSPCMS_UserExceptionSuppliers.InsertOnSubmit(new CSPCMS_UserExceptionSupplier
                        {
                            CSPCMS_ContentType_Id = _contentType.CSPCMS_ContentType_Id,
                            CSPCMS_UserExceptionSupplier_Id = Guid.NewGuid(),
                            UserId = userId,
                            SupplierId = Convert.ToInt32(node.Value)
                        });
                    }
                    _dnnContext.CSPCMS_UserExceptionLanguages.DeleteAllOnSubmit(_dnnContext.CSPCMS_UserExceptionLanguages.Where(a => a.UserId == userId));
                    foreach (RadTreeNode node in userLngTree.CheckedNodes)
                    {
                        _dnnContext.CSPCMS_UserExceptionLanguages.InsertOnSubmit(new CSPCMS_UserExceptionLanguage
                        {
                            CSPCMS_ContentType_Id = _contentType.CSPCMS_ContentType_Id,
                            CSPCMS_UserExceptionLanguage_Id = Guid.NewGuid(),
                            UserId = userId,
                            LanguageId = Convert.ToInt32(node.Value)
                        });
                    }
                    _dnnContext.CSPCMS_UserExceptionCategories.DeleteAllOnSubmit(_dnnContext.CSPCMS_UserExceptionCategories.Where(a => a.UserId == userId));
                    foreach (RadTreeNode node in userCategoryTree.CheckedNodes)
                    {
                        _dnnContext.CSPCMS_UserExceptionCategories.InsertOnSubmit(new CSPCMS_UserExceptionCategory
                        {
                            CSPCMS_UserExceptionCategory_Id = Guid.NewGuid(),
                            CSPCMS_ContentType_Id = _contentType.CSPCMS_ContentType_Id,
                            CategoryId = Convert.ToInt32(node.Value),
                            UserId = userId
                        });
                    }
                    _dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                    break;
            }
        }
        


        /// <summary>
        /// consumer exception toolbar event handler
        /// </summary>
        /// <param name="sender">consumer exception toolbar</param>
        /// <param name="e">event</param>
        private void ConsumerExceptionToolBarButtonClick(object sender, RadToolBarEventArgs e)
        {
            RadComboBox cbUsers = (RadComboBox)consumerExceptionToolbar.FindControl("ConsumerExceptionUserComboBox");
            int userId = Convert.ToInt32(cbUsers.SelectedValue);

            switch (e.Item.Value)
            {
                case "Save":
                    _dnnContext.CSPCMS_ExceptionConsumers.DeleteAllOnSubmit(_contentType.CSPCMS_ExceptionConsumers.Where(a => a.UserId == userId));
                    foreach (RadTreeNode node in consumerExceptionTreeView.CheckedNodes)
                    {
                        _dnnContext.CSPCMS_ExceptionConsumers.InsertOnSubmit(new CSPCMS_ExceptionConsumer
                        {
                            CSPCMS_ContentType_Id = _contentType.CSPCMS_ContentType_Id,
                            CSPCMS_ExceptionConsumer_Id = Guid.NewGuid(),
                            ConsumerId = Convert.ToInt32(node.Value),
                            UserId = userId
                        });
                    }
                    _dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                    break;
                case "Delete":
                    _dnnContext.CSPCMS_ExceptionConsumers.DeleteAllOnSubmit(_contentType.CSPCMS_ExceptionConsumers.Where(a => a.UserId == userId));
                    _dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                    consumerExceptionTreeView.UncheckAllNodes();
                    break;
            }
        }

        /// <summary>
        /// content type tool bar event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ContentExceptionToolbarButtonClick(object sender, RadToolBarEventArgs e)
        {
            switch (e.Item.Value)
            {
                case "Save":
                    _dnnContext.CSPCMS_ExceptionSuppliers.DeleteAllOnSubmit(_contentType.CSPCMS_ExceptionSuppliers);
                    foreach (RadTreeNode node in supplierTree.CheckedNodes)
                    {
                        _dnnContext.CSPCMS_ExceptionSuppliers.InsertOnSubmit(new CSPCMS_ExceptionSupplier
                        {
                            CSPCMS_ContentType_Id = _contentType.CSPCMS_ContentType_Id,
                            CSPCMS_ExceptionSupplier_Id = Guid.NewGuid(),
                            SupplierId = Convert.ToInt32(node.Value)
                        });
                    }

                    _dnnContext.CSPCMS_ExceptionLanguages.DeleteAllOnSubmit(_contentType.CSPCMS_ExceptionLanguages);
                    foreach (RadTreeNode node in languageTree.CheckedNodes)
                    {
                        _dnnContext.CSPCMS_ExceptionLanguages.InsertOnSubmit(new CSPCMS_ExceptionLanguage
                        {
                            CSPCMS_ExceptionLanguage_Id = Guid.NewGuid(),
                            CSPCMS_ContentType_Id = _contentType.CSPCMS_ContentType_Id,
                            LanguageId = Convert.ToInt32(node.Value)
                        });
                    }

                    _dnnContext.CSPCMS_ExceptionCategories.DeleteAllOnSubmit(_contentType.CSPCMS_ExceptionCategories);
                    foreach (RadTreeNode node in categoryTree.CheckedNodes)
                    {
                        _dnnContext.CSPCMS_ExceptionCategories.InsertOnSubmit(new CSPCMS_ExceptionCategory
                        {
                            CSPCMS_ExceptionCategory_Id = Guid.NewGuid(),
                            CSPCMS_ContentType_Id = _contentType.CSPCMS_ContentType_Id,
                            CategoryId = Convert.ToInt32(node.Value)
                        });
                    }
                    _dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                    break;
                default:
                    break;

            }
        }


        /// <summary>
        /// translation toolbar event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void TranslationToolbarButtonClick(object sender, RadToolBarEventArgs e)
        {
            switch (e.Item.Value)
            {
                case "Save":
                    CSPCMS_Translation ctTranslation = _contentType.CSPCMS_Translations.FirstOrDefault(a => a.CultureCode == CurrentLanguage);
                    if (ctTranslation == null)
                    {
                        _dnnContext.CSPCMS_Translations.InsertOnSubmit(new CSPCMS_Translation
                        {
                            CSPCMS_Translation_Id = Guid.NewGuid(),
                            CSPCMS_Key_Id = _contentType.CSPCMS_ContentType_Id,
                            CultureCode = CurrentLanguage,
                            ShortDesc = tbContentTypeShortDesc.Text,
                            LongDesc = tbContentTypeLongDesc.Text,
                            DateCreated = DateTime.Now,
                            DateChanged = DateTime.Now
                        });
                    }
                    else
                    {
                        ctTranslation.ShortDesc = tbContentTypeShortDesc.Text;
                        ctTranslation.LongDesc = tbContentTypeLongDesc.Text;
                    }
                    _dnnContext.SubmitChanges();
                    break;

                default: break;
            }
        }


        /// <summary>
        /// relation toolbar event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void RelationToolbarButtonClick(object sender, RadToolBarEventArgs e)
        {
            switch (e.Item.Value)
            {
                case "Add":
                    if (!string.IsNullOrEmpty(contentTypeRelationTree.SelectedValue))
                        Response.Redirect(GetUrl("AddRelation", "id", contentTypeRelationTree.SelectedValue));
                    break;
                case "Save":
                    if (relationPanel.Visible && !string.IsNullOrEmpty(hfKey.Value) && !string.IsNullOrEmpty(contentTypeRelationTree.SelectedValue))
                    {
                        Guid id = new Guid(hfKey.Value);
                        CSPCMS_ContentTypeRelation relation = _dnnContext.CSPCMS_ContentTypeRelations.FirstOrDefault(a => a.CSPCMS_ContentTypeRelation_Id == id);
                        if (relation != null)
                        {
                            relation.CSPCMS_ContentTypeField_Parent_Id = new Guid(ddlParentKeyField.SelectedValue);
                            relation.CSPCMS_ContentTypeField_Child_Id = new Guid(ddlChildKeyField.SelectedValue);
                            _dnnContext.SubmitChanges();
                        }
                    }
                    break;
                case "Delete":
                    if (relationPanel.Visible && !string.IsNullOrEmpty(hfKey.Value) && !string.IsNullOrEmpty(contentTypeRelationTree.SelectedValue))
                    {
                        Guid selectedRelationId = new Guid(contentTypeRelationTree.SelectedValue);
                        Guid parentRelationId = new Guid(hfKey.Value);
                        var relations = _dnnContext.ExecuteQuery<CSPCMS_ContentTypeRelation>(Common.GetRelationTreeQuery, selectedRelationId).ToArray();
                        var relation = _dnnContext.CSPCMS_ContentTypeRelations.FirstOrDefault(a => a.CSPCMS_ContentTypeRelation_Id == parentRelationId);
                        _dnnContext.CSPCMS_ContentTypeRelations.DeleteAllOnSubmit(relations);
                        _dnnContext.CSPCMS_ContentTypeRelations.DeleteOnSubmit(relation);
                        _dnnContext.SubmitChanges();
                        
                        RenderNestedTree();
                    }
                    break;
                case "Edit":
                    if (!string.IsNullOrEmpty(contentTypeRelationTree.SelectedValue))
                    {

                        Response.Redirect(GetUrl("Configure", new KeyValuePair<string, string>[]
                                                                  {
                                                                      new KeyValuePair<string, string>("id", contentTypeRelationTree.SelectedValue),
                                                                      new KeyValuePair<string, string>("setActive", "false")
                                                                  }));
                    }
                    break;
                default:
                    break;
            }
        }


        /// <summary>
        /// top toolbar event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ToolBarButtonClick(object sender, RadToolBarEventArgs e)
        {
            switch (e.Item.Value)
            {
				//	Edit by DLV
				//  Link directly to control page without postback
                /*case "Cancel":
                    Response.Redirect(Globals.NavigateURL() + "?retry=true", false);
                    break;*/
                case "Delete":
                    _dnnContext.CSPCMS_ContentTypeFields.DeleteAllOnSubmit(_dnnContext.CSPCMS_ContentTypeFields.Where(a=>a.ContentTypeId == _contentTypeId));
                    _dnnContext.CSPCMS_ContentTypes.DeleteAllOnSubmit(_dnnContext.CSPCMS_ContentTypes.Where(a => a.ContentTypeId == _contentTypeId));
                    _dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                    Response.Redirect(Globals.NavigateURL() + "?retry=true", false);
                    break;
                /*case "TranslateContentFields":
                    Response.Redirect(Globals.NavigateURL(TabId, "Translate", "mid", ModuleId.ToString(), "id", _contentTypeId.ToString()), false);
                    break;*/
                default:
                    break;
            }
        }


        /// <summary>
        /// Handle grid related item commands
        /// </summary>
        /// <param name="sender">Grid</param>
        /// <param name="e">Events</param>
        protected void GridOnItemCommandHandler(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case RadGrid.EditCommandName: // on edit
                    e.Item.OwnerTableView.IsItemInserted = false;
                    e.Item.OwnerTableView.EditFormSettings.UserControlName = ControlPath + "Components/Custom/Edit.ascx";
                    break;
                case RadGrid.InitInsertCommandName: // on insert
                    // no insert command
                    break;
                case RadGrid.UpdateCommandName: // on update
                    GridEditableItem editableItem = (GridEditableItem)e.Item;
                    UserControl userControl = editableItem.FindControl(GridEditFormItem.EditFormUserControlID) as UserControl;
                    if (userControl == null) return;

                    int contentTypeId = (int) editableItem.GetDataKeyValue("ContentTypeId");
                    int contentTypeFieldId = (int)editableItem.GetDataKeyValue("ContentTypeFieldId");
                    var cspCmsContentTypeId = (Guid) editableItem.GetDataKeyValue("CSPCMS_ContentType_Id");
                    var cspCmsContentTypeFieldId = (Guid)editableItem.GetDataKeyValue("CSPCMS_ContentTypeField_Id");
                    CSPCMS_ContentTypeField field = _dnnContext.CSPCMS_ContentTypeFields.FirstOrDefault(a => a.CSPCMS_ContentTypeField_Id == cspCmsContentTypeFieldId && a.CSPCMS_ContentType_Id == cspCmsContentTypeId);
                    
                    if (field == null) // field shouldnt be empty. if it is empty, content might have been modified.
                        break;

                    #region Handle Edit UserControl Update

                    field.DisplayOrder = Convert.ToInt32(((RadNumericTextBox) userControl.FindControl("tbDisplayOrder")).Value);
                    field.DisplayGroupOrder = Convert.ToInt32(((RadNumericTextBox)userControl.FindControl("tbDisplayGroupOrder")).Value);
                    field.Readonly = ((CheckBox) userControl.FindControl("cbReadonly")).Checked;
                    field.Required = ((CheckBox)userControl.FindControl("cbRequired")).Checked;
                    field.Visible = ((CheckBox)userControl.FindControl("cbVisible")).Checked;
                    field.Searchable = ((CheckBox)userControl.FindControl("cbSearchable")).Checked;
                    field.ConsumerExceptionField = ((CheckBox)userControl.FindControl("cbConsumerExceptionField")).Checked;

                    field.DefaultValue = ((RadEditor) userControl.FindControl("reDefaultValue")).Content;
                    

                    RadComboBox comboBox;
                    comboBox = ((RadComboBox) userControl.FindControl("cbValidation"));
                    field.ValidationId = !string.IsNullOrEmpty(comboBox.SelectedValue) ? new Guid(comboBox.SelectedValue) : (Guid?) null;
                    comboBox = ((RadComboBox) userControl.FindControl("cbCodeList"));
                    field.CodeListId = !string.IsNullOrEmpty(comboBox.SelectedValue) ? new Guid(comboBox.SelectedValue) : (Guid?) null;
                    comboBox = ((RadComboBox) userControl.FindControl("cbPropertyType"));
                    field.ContentTypeFieldType = !string.IsNullOrEmpty(comboBox.SelectedValue) ? comboBox.SelectedValue : "Text";

                    _dnnContext.SubmitChanges();

                    #endregion

                    break;
                default:
                    break;
            }
        }


        #endregion

        #region Other functions
        /// <summary>
        /// create DNN set of content type & content type fields if contentType == null
        /// </summary>
        /// <param name="fields">Csp Fields</param>
        /// <param name="contentType">Dnn Content Definition</param>
        private CSPCMS_ContentType ProcessCspFields(content_types_field[] fields, CSPCMS_ContentType contentType)
        {
            // new content)
            if (contentType == null)
            {
                // set current active content type to false only if _setActive is true
                if (_setActive)
                {
                    foreach (CSPCMS_ContentType ct in _dnnContext.CSPCMS_ContentTypes.Where(a => a.Active == true && a.PortalId == PortalId && a.UserId == UserId && a.TabModuleId == TabModuleId))
                    {
                        ct.Active = false;
                    }
                }

                content_type cspContentType = CspDataContext.content_types.FirstOrDefault(a => a.content_types_Id == _contentTypeId);
                contentType = new CSPCMS_ContentType
                                  {
                                      ContentTypeId = _contentTypeId,
                                      PortalId = PortalId,
                                      TabModuleId = TabModuleId,
                                      UserId = UserId,
                                      DateCreated = DateTime.Now,
                                      DateChanged = DateTime.Now,
                                      Active = _setActive,
                                      CSPCMS_ContentType_Id = Guid.NewGuid(),
                                      ContentTypeName = cspContentType!=null?cspContentType.description:""
                                  };
                int displayOrder = 0;
                foreach (content_types_field field in fields)
                {
                    contentType.CSPCMS_ContentTypeFields.Add(new CSPCMS_ContentTypeField
                                                                 {
                                                                     CSPCMS_ContentType = contentType,
                                                                     ContentTypeId = _contentTypeId,
                                                                     ContentTypeFieldId = field.content_types_fields_Id,
                                                                     ContentTypeFieldName = field.fieldname,
                                                                     ContentTypeFieldType = "Text",
                                                                     DefaultValue = field.default_value,
                                                                     DisplayOrder = (++displayOrder),
                                                                     DisplayGroupOrder = 1,
                                                                     Readonly = field.@readonly.HasValue && field.@readonly.Value,
                                                                     Required = field.mandatory.HasValue && field.mandatory.Value,
                                                                     Visible = true,
                                                                     Searchable = false,
                                                                     ConsumerExceptionField = false,
                                                                     CSPCMS_ContentTypeField_Id = Guid.NewGuid()
                                                                 });
                }
                _dnnContext.CSPCMS_ContentTypes.InsertOnSubmit(contentType);
                _dnnContext.SubmitChanges();
            }
            else //async content type field from csp db to dnn db - DLV
            {
                int displayOrder = 0;
                foreach (var contentTypesField in fields)
                {
                    if (contentType.CSPCMS_ContentTypeFields.Any() && contentType.CSPCMS_ContentTypeFields.Count(a => a.ContentTypeFieldId == contentTypesField.content_types_fields_Id) != 0)
                    {
                        continue;
                    }
                    contentType.CSPCMS_ContentTypeFields.Add(new CSPCMS_ContentTypeField
                    {
                        CSPCMS_ContentType = contentType,
                        ContentTypeId = _contentTypeId,
                        ContentTypeFieldId = contentTypesField.content_types_fields_Id,
                        ContentTypeFieldName = contentTypesField.fieldname,
                        ContentTypeFieldType = "text",
                        DefaultValue = contentTypesField.default_value,
                        DisplayOrder = (++displayOrder),
                        DisplayGroupOrder = 1,
                        Readonly = contentTypesField.@readonly.HasValue && contentTypesField.@readonly.Value,
                        Required = contentTypesField.mandatory.HasValue && contentTypesField.mandatory.Value,
                        Visible = true,
                        Searchable = false,
                        ConsumerExceptionField = false,
                        CSPCMS_ContentTypeField_Id = Guid.NewGuid()
                    });
                    _dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                }
            }
            return contentType;
        }


        #region tree view functions
        /// <summary>
        /// render nested relation tree view
        /// </summary>
        private void RenderNestedTree()
        {
            CSPCMS_ContentType topLevelParentContentType = GetTopLevelParentContentType(_contentType);
            contentTypeRelationTree.Nodes.Clear();
            NestTree(null, topLevelParentContentType);
        }

        /// <summary>
        /// recursive function to create nested tree
        /// </summary>
        /// <param name="treeNode">current node</param>
        /// <param name="contentType">current content type</param>
        private void NestTree(RadTreeNode treeNode, CSPCMS_ContentType contentType)
        {
            var query = from r in _dnnContext.CSPCMS_ContentTypeRelations.Where(a => a.CSPCMS_ContentType_Parent_Id == contentType.CSPCMS_ContentType_Id)
                        join ct in _dnnContext.CSPCMS_ContentTypes on r.CSPCMS_ContentType_Child_Id equals ct.CSPCMS_ContentType_Id
                        select ct;

            CSPCMS_Translation t = contentType.CSPCMS_Translations.FirstOrDefault(a => a.CultureCode == CurrentLanguage);
            RadTreeNode node = new RadTreeNode();
            node.Text = t != null ? t.ShortDesc : contentType.ContentTypeName;
            node.Value = contentType.CSPCMS_ContentType_Id.ToString();
            node.Expanded = true;
            if (treeNode == null)
            {
                // top level
                contentTypeRelationTree.Nodes.Add(node);
            }
            else
            {
                //child node
                treeNode.Nodes.Add(node);
            }

            foreach (CSPCMS_ContentType ct in query)
            {
                NestTree(node, ct);
            }
        }

        /// <summary>
        /// recursive function to return the top level content type in a tree
        /// </summary>
        /// <param name="contentType"></param>
        /// <returns></returns>
        private CSPCMS_ContentType GetTopLevelParentContentType(CSPCMS_ContentType contentType)
        {
            CSPCMS_ContentType ct = contentType;

            var query = from r in _dnnContext.CSPCMS_ContentTypeRelations.Where(a => a.CSPCMS_ContentType_Child_Id == ct.CSPCMS_ContentType_Id)
                        join c in _dnnContext.CSPCMS_ContentTypes on r.CSPCMS_ContentType_Parent_Id equals c.CSPCMS_ContentType_Id
                        select c;

            // top level is when count of query return 0
            if (query.Count() == 0)
                return ct;

            return GetTopLevelParentContentType(query.First());
        }

        #endregion
        #endregion

        protected void ContentTypeRelationTreeNodeClick(object sender, RadTreeNodeEventArgs e)
        {
            Response.Redirect(Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Configure", "mid", ModuleId.ToString(), "id", e.Node.Value.ToString()));
        }

        protected void TreeViewOnNodeClick(object sender, RadTreeNodeEventArgs e)
        {
            Guid id = new Guid(e.Node.Value);
            
            CSPCMS_ContentType contentType = _dnnContext.CSPCMS_ContentTypes.First(a => a.CSPCMS_ContentType_Id == id);
            ddlParentKeyField.Items.Clear();
            ddlChildKeyField.Items.Clear();

            // find relation key
            CSPCMS_ContentTypeRelation parentRelation = _dnnContext.CSPCMS_ContentTypeRelations.Where(a => a.CSPCMS_ContentType_Child_Id == id).FirstOrDefault();

            if (parentRelation == null)
            {
                relationPanel.Visible = false;
                return;
            }

            relationPanel.Visible = true;
            hfKey.Value = parentRelation.CSPCMS_ContentTypeRelation_Id.ToString();
            
            // fill dropdown list
            foreach (var field in parentRelation.CSPCMS_ContentType.CSPCMS_ContentTypeFields.OrderBy(a=>a.DisplayOrder))
            {
                ddlParentKeyField.Items.Add(new ListItem {Text = field.ContentTypeFieldName, Value = field.CSPCMS_ContentTypeField_Id.ToString(), Selected = parentRelation.CSPCMS_ContentTypeField_Parent_Id == field.CSPCMS_ContentTypeField_Id});
            }

            //note: first() might be an issue
            foreach (var field in parentRelation.CSPCMS_ChildContentTypes.First().CSPCMS_ContentTypeFields.OrderBy(a => a.DisplayOrder))
            {
                ddlChildKeyField.Items.Add(new ListItem { Text = field.ContentTypeFieldName, Value = field.CSPCMS_ContentTypeField_Id.ToString(), Selected = parentRelation.CSPCMS_ContentTypeField_Child_Id == field.CSPCMS_ContentTypeField_Id });
            }
        }

        #region Nested Category Tree
        /// <summary>
        /// if user category exceptions exist:
        ///     render user exception categories
        /// else if category exceptions exist
        ///     render exception categories
        /// else 
        ///     render all categories
        /// </summary>
        private void RenderCategoryTree(RadTreeView tree)
        {
            List<category> categories = CspDataContext.categories.Where(a => a.active.HasValue && a.active.Value && !a.categoryText.StartsWith(IgnoredCategoryText)).ToList();
            CustomCategoryTreeNode root = CustomCategoryTreeNode.Create(categories);
            RenderNestedTree(tree, null, root);
        }

        /// <summary>
        /// recursive function to create a nest tree
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="radTreeNode">current tree node</param>
        /// <param name="node">data tree node</param>
        private void RenderNestedTree(RadTreeView tree, RadTreeNode radTreeNode, CustomCategoryTreeNode node)
        {
            RadTreeNode tmp = new RadTreeNode(node.Text, node.Id.ToString());
            tmp.Expanded = true;
            if (radTreeNode == null)
            {
                tree.Nodes.Add(tmp);
            }
            else
            {
                radTreeNode.Nodes.Add(tmp);
            }

            foreach (CustomCategoryTreeNode child in node.Children)
            {
                RenderNestedTree(tree, tmp, child);
            }
        }
        #endregion

	}
}