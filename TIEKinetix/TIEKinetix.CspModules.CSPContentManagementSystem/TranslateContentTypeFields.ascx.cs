﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Security.Membership;
using Telerik.Web.UI;
using TIEKinetix.CspModules.CSPContentManagementSystem.Components;
using TIEKinetix.CspModules.Data;
using DotNetNuke.Entities.Users;

namespace TIEKinetix.CspModules.CSPContentManagementSystem
{
    public partial class TranslateContentTypeFields : DnnModuleBase
    {
        private DnnDataContext _dnnContext;
        private int _contentTypeId;
        protected List<CSPCMS_ContentTypeField> Fields;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region Overrides of DnnModuleBase

        /// <summary>
        /// translate fields per selected portal language
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);

            _dnnContext = new DnnDataContext(Config.GetConnectionString());
            _contentTypeId = Convert.ToInt32(Request.QueryString["id"]);
            CSPCMS_ContentType contentType = _dnnContext.CSPCMS_ContentTypes.FirstOrDefault(a => a.ContentTypeId == _contentTypeId && a.PortalId == PortalId && a.TabModuleId == TabModuleId);
            if (contentType == null)
                return; 

            Fields = _dnnContext.CSPCMS_ContentTypeFields.Where(a => a.CSPCMS_ContentType_Id == contentType.CSPCMS_ContentType_Id).OrderBy(a => a.DisplayGroupOrder).OrderBy(a => a.DisplayOrder).ToList();
            string currentCultureCode = Thread.CurrentThread.CurrentCulture.Name;

            // buttons
            toolBar.Items.Add(new RadToolBarButton
                              	{
                              		Text = GetLocalizedText("TranslateToolBar.Back"), 
									ImageUrl = ControlPath + "images/arrow_left.png", 
									Value = "Back",
									PostBack = false,
									NavigateUrl = Globals.NavigateURL(TabId, "Configure", "mid", ModuleId.ToString(), "id", _contentTypeId.ToString())
                              	});
            toolBar.Items.Add(new RadToolBarButton {IsSeparator = true});
            toolBar.Items.Add(new RadToolBarButton { Text = GetLocalizedText("TranslateToolBar.Save"), ImageUrl = ControlPath + "images/save.png", Value = "Save" });
            //toolBar.ButtonClick += new RadToolBarEventHandler(ToolBarButtonClick);

            //render
            if (Fields.Count > 0)
            {
                Table tbl = new Table();
                tbl.CellPadding = tbl.CellSpacing = 0;
                tbl.CssClass = "translation-table";
                TranslatePanel.Controls.Add(tbl);
                int i = 0;
                foreach (CSPCMS_ContentTypeField field in Fields)
                {
                    CSPCMS_Translation translation = field.CSPCMS_Translations.Where(a => a.CultureCode == currentCultureCode).FirstOrDefault();

                    TableRow tr = new TableRow();
                    tr.CssClass = i%2 == 0 ? "even" : "odd";
                    TableCell tc;
                    tbl.Rows.Add(tr);

                    tc = new TableCell();
                    tc.RowSpan = 2;
                    tc.CssClass = "label ";
                    tc.Controls.Add(new Label {Text = field.ContentTypeFieldName});
                    tr.Cells.Add(tc);
                    tc = new TableCell();
                    tc.CssClass = "field";
                    tc.Controls.Add(new RadTextBox {Text = translation != null ? translation.ShortDesc : "", Width = new Unit(300), ID = "fn"+field.CSPCMS_ContentTypeField_Id});
                    tr.Cells.Add(tc);
                    tr = new TableRow();
                    tr.CssClass = (i%2 == 0 ? "even" : "odd");
                    tbl.Rows.Add(tr);
                    tc = new TableCell();
                    tc.CssClass = "field";
                    tc.Controls.Add(new RadTextBox { Text = translation != null ? translation.LongDesc : "", Rows = 5, TextMode = InputMode.MultiLine, Width = new Unit(300), ID = "fd"+field.CSPCMS_ContentTypeField_Id });
                    tr.Cells.Add(tc);

                    i++;
                }
            }
            else
            {
                TranslatePanel.Controls.Add(new Literal {Text = GetLocalizedText("TranslatePanel.NoContent")});
            }
        }

        #region Event Handlers
        protected void ToolBarButtonClick(object sender, RadToolBarEventArgs e)
        {
            switch (e.Item.Value)
            {
				//DLV : disable post back when click Back button, point to Configure page directly
				/*
                case "Back":
                    Response.Redirect(Globals.NavigateURL(TabId, "Configure", "mid", ModuleId.ToString(), "id", _contentTypeId.ToString()), false);
                    break;
				 */
                case "Save":
                    string currentCultureCode = Thread.CurrentThread.CurrentCulture.Name;
                    foreach (CSPCMS_ContentTypeField field in Fields)
                    {
                        
                        RadTextBox fieldName = (RadTextBox) TranslatePanel.FindControl("fn"+field.CSPCMS_ContentTypeField_Id);
                        RadTextBox fieldDesc = (RadTextBox) TranslatePanel.FindControl("fd" + field.CSPCMS_ContentTypeField_Id);

                        CSPCMS_Translation translation = field.CSPCMS_Translations.Where(a => a.CultureCode == currentCultureCode).FirstOrDefault();
                        if (translation == null)
                        {
                            if (string.IsNullOrEmpty(fieldName.Text) && string.IsNullOrEmpty(fieldDesc.Text)) continue;
                            _dnnContext.CSPCMS_Translations.InsertOnSubmit(new CSPCMS_Translation
                                                                                               {
                                                                                                   CSPCMS_Translation_Id = Guid.NewGuid(),
                                                                                                   CSPCMS_Key_Id = field.CSPCMS_ContentTypeField_Id,
                                                                                                   CultureCode = currentCultureCode,
                                                                                                   ShortDesc = fieldName.Text,
                                                                                                   LongDesc = fieldDesc.Text,
                                                                                                   DateCreated = DateTime.Now,
                                                                                                   DateChanged = DateTime.Now
                                                                                               });
                        }
                        else
                        {
                            translation.ShortDesc = fieldName.Text;
                            translation.LongDesc = fieldDesc.Text;
                            translation.DateChanged = DateTime.Now;
                        }
                    }
                    _dnnContext.SubmitChanges();
                    break;
                default: break;
            }
        }
        #endregion


        #endregion
    }
}