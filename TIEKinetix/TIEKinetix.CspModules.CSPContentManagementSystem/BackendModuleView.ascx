﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BackendModuleView.ascx.cs" Inherits="TIEKinetix.CspModules.CSPContentManagementSystem.BackendModuleView" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div class="listview-container">
<telerik:RadListView ID="listView" runat="server" ItemPlaceholderID="content_type_container">
    <LayoutTemplate>
        <div class="container" runat="server" id="content_type_container"></div>
    </LayoutTemplate>
    <ItemTemplate>
        <div class="content-type-container">
            <a class="content_type" href="<%# GetUrl(Eval("content_types_Id")) %>" style="background-image: url('<%# ControlPath + "images/cta_arrow_white_lg_spaced.png" %>'); background-repeat: no-repeat; background-position: 100% 100%;">
                <span><%#Eval("description") %></span>
            </a>
            <div class="clear"></div>
        </div>
    </ItemTemplate>
</telerik:RadListView>
    <div class="clear"></div>
</div>

<telerik:RadScriptBlock runat="server">
    <script language="javascript" type="text/javascript">
        function SetTileWidth() {
            var tiles = $("div.content-type-container", "div.listview-container");
            var max = 0;
            tiles.each(function () {
                if (max < $(this).width())
                    max = $(this).width();
            });

            if (max > 0) {
                tiles.each(function () { $(this).css("width", max + "px"); });
            }
        }
        if ($.browser.msie && parseInt($.browser.version, 10) < 8) {
            $(document).ready(function () { SetTileWidth(); });
        } else {
            SetTileWidth();
        }

        jQuery(function () {
            jQuery('div.content-type-container a').hover(function () {
                jQuery(this).stop().animate({ backgroundColor: "#0054A6" });
            }, function () {
                jQuery(this).stop().animate({ backgroundColor: "#00AEEF" });
            });
        });
    </script>
</telerik:RadScriptBlock>
