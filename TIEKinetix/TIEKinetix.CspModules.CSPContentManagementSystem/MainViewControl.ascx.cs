using System;
using DotNetNuke.Common;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using TIEKinetix.CspModules.Data;

namespace TIEKinetix.CspModules.CSPContentManagementSystem
{
	public partial class MainViewControl : PortalModuleBase, IActionable
    {
        private CspDataContext _context;

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptInclude("jquery", ResolveUrl("~/js/external/jquery/jquery-1.7.2.js"));
            Page.ClientScript.RegisterClientScriptInclude("jquery-ui", ResolveUrl("~/js/external/jquery/jquery-ui-1.8.18.custom.min.js"));

            if (UserInfo.IsInRole("Administrators"))
            {
                Controls.Add(GetCustomDnnView("BackendModuleView"));
            }
            else
            {
                // custom view control messes up ajax update, therefore redirect is needed.
                Response.Redirect(Globals.NavigateURL(TabId, "Content", "mid", ModuleId.ToString()));
            }
        }

        private PortalModuleBase GetCustomDnnView(string customViewName)
        {
            string viewPath = string.Format("{0}{1}.ascx", ControlPath, customViewName);
            PortalModuleBase tmpView = (PortalModuleBase) LoadControl(viewPath);
            //tmpView.ID = Path.GetFileNameWithoutExtension(viewPath);
            tmpView.ID = Guid.NewGuid().ToString();
            tmpView.ModuleContext.Configuration = ModuleContext.Configuration;
            return tmpView;
        }

		#region Implementation of IActionable

		public ModuleActionCollection ModuleActions
		{
			get
			{
				ModuleActionCollection Actions = new ModuleActionCollection();
				try
				{
					
				}
				catch
				{
					// added exception to prevent error in case you guys dont have the newest Permssion Page
				}
				return Actions;
			}
		}

		#endregion
    }
}