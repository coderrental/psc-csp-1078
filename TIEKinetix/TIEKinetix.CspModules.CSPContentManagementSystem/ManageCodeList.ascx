﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageCodeList.ascx.cs" Inherits="TIEKinetix.CspModules.CSPContentManagementSystem.ManageCodeList" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
	Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI, Version=2012.2.703.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4"
	Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register assembly="Telerik.Web.UI, Version=2012.1.411.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<telerik:RadAjaxLoadingPanel ID="ajaxLoadingPanel" IsSticky="True" Transparency="50" CssClass="ajax-loading-panel" runat="server"/>
<telerik:RadWindowManager ID="windowManager" runat="server"/>
<telerik:RadAjaxManager runat="server" ID="ajaxManager" DefaultLoadingPanelID="ajaxLoadingPanel">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="grCodeList">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grCodeList" />
                <telerik:AjaxUpdatedControl ControlID="windowManager" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>
<!-- Toolbar -->
<telerik:RadToolBar ID="toptoolbar" runat="server" Width="100%" CssClass="property-toolbar"/>
<!-- /End toolbar -->

<!-- Code list grid -->
<telerik:RadGrid ID="grCodeList" AutoGenerateColumns="False" OnItemInserted="grCodeList_OnItemInserted" OnItemUpdated="grCodeList_OnItemUpdated" OnItemDeleted="grCodeList_OnItemDeleted" runat="server" DataSourceID="lqsCodeList" AllowPaging="True" PageSize="20" AllowAutomaticDeletes="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True">
	<MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="CSPCMS_CodeList_Id" Name="CodeList" EditMode="InPlace">
		<CommandItemSettings ShowAddNewRecordButton="True" ShowRefreshButton="True" />
		<Columns>
            <telerik:GridBoundColumn DataField="CSPCMS_CodeList_Id" UniqueName="CSPCMS_CodeList_Id" HeaderText="ID" ReadOnly="true" ForceExtractValue="Always" />
            <telerik:GridBoundColumn DataField="CodeListName" HeaderText="Code List Name" />
            <telerik:GridDateTimeColumn DataField="DateCreated" HeaderText="Date Created" ReadOnly="True" />     
			<telerik:GridDateTimeColumn DataField="DateChanged" HeaderText="Date Changed" ReadOnly="True" /> 
			<telerik:GridEditCommandColumn ButtonType="ImageButton" />
            <telerik:GridButtonColumn ConfirmText='Delete this Code list?' ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" />                  
        </Columns>
		<DetailTables>
			<telerik:GridTableView EditMode="InPlace" AutoGenerateColumns="False" AllowPaging="False"  Name="CodeValue" runat="server" DataSourceID="lqsCodeValue" DataKeyNames="CSPCMS_CodeListValue_Id,CSPCMS_CodeList_Id" CommandItemDisplay="Top">
				<CommandItemSettings ShowAddNewRecordButton="True" ShowRefreshButton="False"/>
				<ParentTableRelation>
					<telerik:GridRelationFields DetailKeyField="CSPCMS_CodeList_Id" MasterKeyField="CSPCMS_CodeList_Id" />
				</ParentTableRelation>
				<Columns>
					<telerik:GridEditCommandColumn ButtonType="ImageButton" />
					<telerik:GridBoundColumn DataField="CSPCMS_CodeListValue_Id" UniqueName="CSPCMS_CodeListValue_Id" HeaderText="ID" ReadOnly="true" ForceExtractValue="Always">
						<HeaderStyle Width="250"></HeaderStyle>
					</telerik:GridBoundColumn>
					<telerik:GridBoundColumn HeaderText="Name" HeaderButtonType="TextButton" DataField="Name" UniqueName="Name" />
					<telerik:GridBoundColumn HeaderText="Value" HeaderButtonType="TextButton" DataField="Value" UniqueName="Value"/>
					<telerik:GridButtonColumn ConfirmText='Delete this Code list value?' ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" />
				</Columns>
				<EditFormSettings>
					<EditColumn ButtonType="ImageButton" />
					<PopUpSettings Modal="true" />
				</EditFormSettings>
			</telerik:GridTableView>
		</DetailTables>
		<EditFormSettings>
            <EditColumn ButtonType="ImageButton" />
            <PopUpSettings Modal="true" />
        </EditFormSettings>
		<PagerStyle Mode="Slider"></PagerStyle>
	</MasterTableView>
</telerik:RadGrid>
<!-- end Code list grid -->

<telerik:RadScriptBlock runat="server">
	<script type="text/javascript">
		$(function () {
			//
		})
	</script>
</telerik:RadScriptBlock>
<asp:LinqDataSource ID="lqsCodeList" runat="server"
	ContextTypeName="TIEKinetix.CspModules.Data.DnnDataContext" OnContextCreating="lqsCodeList_OnContextCreating"
	TableName="CSPCMS_CodeLists" EnableDelete="True" EnableInsert="True" EnableUpdate="True" OnUpdating="lqsCodeList_OnUpdating" OnInserting="lqsCodeList_OnInserting">
</asp:LinqDataSource>
<asp:LinqDataSource ID="lqsCodeValue" runat="server" 
	ContextTypeName="TIEKinetix.CspModules.Data.DnnDataContext" 
	TableName="CSPCMS_CodeListValues" EnableDelete="True" EnableInsert="True" OnContextCreating="lqsCodeValue_OnContextCreating" EnableUpdate="True" OnInserting="lqsCodeValue_OnInserting"
	Where="CSPCMS_CodeList_Id == Guid(@CSPCMS_CodeList_Id)">
	<WhereParameters>
        <asp:Parameter Name="CSPCMS_CodeList_Id" Type="Object" />
    </WhereParameters>
</asp:LinqDataSource>

