﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContentTypeRelation.ascx.cs" Inherits="TIEKinetix.CspModules.CSPContentManagementSystem.ContentTypeRelation" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Panel runat="server" ID="pContainer">
    <telerik:RadAjaxLoadingPanel ID="loadingPanel" IsSticky="True" Transparency="50" CssClass="ajax-loading-panel" runat="server"></telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxPanel ID="ajaxPanel" runat="server" LoadingPanelID="loadingPanel">
        <table width="100%" class="add-relation-table">
            <thead>
                <tr>
                    <td colspan="2">
                        <telerik:RadToolBar ID="toolBar" runat="server" OnButtonClick="ToolBarOnButtonClick" Width="100%"></telerik:RadToolBar>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Literal runat="server" ID="lContentTypeName"></asp:Literal>
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="relation-label">
                        <asp:Label runat="server" ID="lbParentKeyField"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlParentKeyField">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="relation-label">
                        <asp:Label runat="server" ID="lbChildContentType"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlChildContentType" OnSelectedIndexChanged="ChildContentTypeIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="relation-label">
                        <asp:Label runat="server" ID="lbChildKeyField"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlChildKeyField">
                        </asp:DropDownList>
                    </td>
                </tr>
            </tbody>
        </table>
    </telerik:RadAjaxPanel>
</asp:Panel>
<asp:Panel runat="server" ID="pErrorContainer">
</asp:Panel>
