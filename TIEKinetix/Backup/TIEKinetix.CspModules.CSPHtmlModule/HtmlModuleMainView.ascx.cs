﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Security;
using TIEKinetix.CspModules.CSPHtmlModule.Components;

namespace TIEKinetix.CspModules.CSPHtmlModule
{
    public partial class HtmlModuleMainView : DnnModuleBase, IActionable
    {
        #region Overrides of UserControl

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            string currentLanguage = Thread.CurrentThread.CurrentCulture.Name;

            string content = string.Empty;

            using(DnnContext db = new DnnContext(Config.GetConnectionString()))
            {
                // 1) portalid, tabmoduleid, culture code = current
                // 2) portalid, tabmoduleid, empty culture code
                // 3) portalid, -1, culture code
                // 4) -1

                CSPHtmlModule_Content[] contents = db.CSPHtmlModule_Contents.Where(a => a.PortalId == PortalId).ToArray();
                if (contents.Count(a=>a.PortalId == PortalId && a.TabModuleId == TabModuleId && a.CultureCode == currentLanguage) > 0)
                {
                    content = contents.First(a => a.PortalId == PortalId && a.TabModuleId == TabModuleId && a.CultureCode == currentLanguage).Content;
                }
                else if (contents.Count(a=>a.PortalId == PortalId && a.TabModuleId == TabModuleId && a.CultureCode == string.Empty) > 0)
                {
                    content = contents.First(a => a.PortalId == PortalId && a.TabModuleId == TabModuleId && a.CultureCode == string.Empty).Content;
                }
                else if (contents.Count(a=>a.PortalId == PortalId && a.TabModuleId == -1) > 0)
                {
                    content = contents.First(a => a.PortalId == PortalId && a.TabModuleId == -1).Content;
                }
                else
                {
                    content = GetLocalizedText(string.Format("Content_{0}_{1}.{2}", PortalId, TabModuleId, currentLanguage));
                }
            }

            Controls.Add(new Literal {Text = content});
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #region Implementation of IActionable

        public ModuleActionCollection ModuleActions
        {
            get
            {
                ModuleActionCollection Actions = new ModuleActionCollection();
                Actions.Add(
                    GetNextActionID(),
                    "Settings",
                    ModuleActionType.AddContent, "", ControlPath + "Images/edit.png", EditUrl("Configuration"), false, SecurityAccessLevel.Edit, true, false);
                return Actions;
            }
        }

        #endregion
    }
}