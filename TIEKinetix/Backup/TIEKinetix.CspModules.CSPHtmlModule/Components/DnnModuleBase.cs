﻿using System;
using System.Threading;
using CR.DnnModules.Common;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Localization;
using DotNetNuke.Services.Log.EventLog;

namespace TIEKinetix.CspModules.CSPHtmlModule.Components
{
    public class DnnModuleBase : PortalModuleBase
    {
        private string _localResourceFile;
        public string CurrentLanguage;
        protected EventLogController DnnEventLog;

        protected virtual void Page_Init(object sender, EventArgs e)
        {
            // get common resource file so all entries would be in 1 file instead of in many different files
            _localResourceFile = Utils.GetCommonResourceFile("CSPHtmlModule", LocalResourceFile);
            CurrentLanguage = Thread.CurrentThread.CurrentCulture.Name;
            DnnEventLog = new EventLogController();
            ModuleConfiguration.ModuleTitle = GetLocalizedText(Keys.ModuleTitleKey);
        }
        

        /// <summary>
        /// Get localized text for the current portal language
        /// </summary>
        /// <param name="key">Resource Key</param>
        /// <returns>Localized text</returns>
        public string GetLocalizedText(string key)
        {
            return Localization.GetString(key, _localResourceFile);
        }

        /// <summary>
        /// Write message to Dnn event view
        /// </summary>
        /// <param name="message">message to write</param>
        public void Log(string message)
        {
            DnnEventLog.AddLog("DEBUG", message, PortalSettings, UserId, EventLogController.EventLogType.ADMIN_ALERT);
        }
        
        /// <summary>
        /// Get portal external identifier used to identify portal channel.
        /// </summary>
        /// <returns></returns>
        protected string GetPortalExternalIdentifier()
        {
            string s = Utils.GetSetting(PortalId, "ExternalIdentifier");
            return !string.IsNullOrEmpty(s) ? s : Request.Url.Host;
        }
        
    }
}