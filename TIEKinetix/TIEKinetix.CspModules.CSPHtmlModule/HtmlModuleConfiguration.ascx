﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HtmlModuleConfiguration.ascx.cs" Inherits="TIEKinetix.CspModules.CSPHtmlModule.HtmlModuleConfiguration" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadToolBar ID="Toolbar" runat="server" OnButtonClick="Toolbar_OnClick" Width="99%">
    <Items>
        <telerik:RadToolBarButton ImageUrl="Images/disk_blue.png" Value="Save" Text="Save"></telerik:RadToolBarButton>
    </Items>
</telerik:RadToolBar>
<br />
<telerik:RadEditor ID="Editor" runat="server" Height="400" Width="99%">
</telerik:RadEditor>