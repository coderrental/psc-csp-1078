﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using Telerik.Web.UI;
using TIEKinetix.CspModules.CSPHtmlModule.Components;

namespace TIEKinetix.CspModules.CSPHtmlModule
{
    public partial class HtmlModuleConfiguration : DnnModuleBase
    {
        DnnContext _dnnContext;

        #region Overrides of UserControl

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            _dnnContext = new DnnContext(Config.GetConnectionString());

            Editor.Tools.Clear();
            EditorToolGroup group = new EditorToolGroup();
            Editor.Tools.Add(group);
            group.Tools.Add(new EditorTool { Name = "Cut", Text = GetLocalizedText("HtmlEditor.Cut") });
            group.Tools.Add(new EditorTool { Name = "Copy", Text = GetLocalizedText("HtmlEditor.Copy") });
            group.Tools.Add(new EditorTool { Name = "Paste", Text = GetLocalizedText("HtmlEditor.Paste") });

            group.Tools.Add(new EditorTool { Name = "Bold", Text = GetLocalizedText("HtmlEditor.Bold") });
            group.Tools.Add(new EditorTool { Name = "Italic", Text = GetLocalizedText("HtmlEditor.Italic") });
            group.Tools.Add(new EditorTool { Name = "Underline", Text = GetLocalizedText("HtmlEditor.Underline") });

            group.Tools.Add(new EditorTool {Name = "FontName"});

            CSPHtmlModule_Content content = GetHtmlModuleContent();
            if(content != null)
            {
                Editor.Content = content.Content;
            }
        }

        private CSPHtmlModule_Content GetHtmlModuleContent()
        {
            CSPHtmlModule_Content content = null;

            CSPHtmlModule_Content[] contents = _dnnContext.CSPHtmlModule_Contents.Where(a => a.PortalId == PortalId).ToArray();
            if (contents.Count(a => a.PortalId == PortalId && a.TabModuleId == TabModuleId && a.CultureCode == CurrentLanguage) > 0)
            {
                content = contents.First(a => a.PortalId == PortalId && a.TabModuleId == TabModuleId && a.CultureCode == CurrentLanguage);
            }
            else if (contents.Count(a => a.PortalId == PortalId && a.TabModuleId == TabModuleId && a.CultureCode == string.Empty) > 0)
            {
                content = contents.First(a => a.PortalId == PortalId && a.TabModuleId == TabModuleId && a.CultureCode == string.Empty);
            }
            else if (contents.Count(a => a.PortalId == PortalId && a.TabModuleId == -1) > 0)
            {
                content = contents.First(a => a.PortalId == PortalId && a.TabModuleId == -1);
            }

            return content;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Toolbar_OnClick(object sender, RadToolBarEventArgs e)
        {
            switch (e.Item.Value)
            {
                case "Save":
                    CSPHtmlModule_Content content = _dnnContext.CSPHtmlModule_Contents.SingleOrDefault(a => a.PortalId == PortalId && a.TabModuleId == TabModuleId && a.CultureCode == CurrentLanguage);
                    if (content == null)
                    {
                        _dnnContext.CSPHtmlModule_Contents.InsertOnSubmit(new CSPHtmlModule_Content
                                                                              {
                                                                                  Id=Guid.NewGuid(),
                                                                                  Content = Editor.Content,
                                                                                  CultureCode = CurrentLanguage,
                                                                                  CreatedByUserId = UserId,
                                                                                  PortalId = PortalId,
                                                                                  TabModuleId = TabModuleId,
                                                                                  Created = DateTime.Now,
                                                                                  Changed = DateTime.Now
                                                                              });
                    }
                    else
                    {
                        content.Content = Editor.Content;
                        content.Changed = DateTime.Now;
                    }
                    _dnnContext.SubmitChanges();
                    break;
                default:
                    break;
            }
        }
    }
}