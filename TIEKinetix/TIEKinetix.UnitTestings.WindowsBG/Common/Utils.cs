﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace TIEKinetix.UnitTestings.WindowsBG.Common
{
    public static class Utils
    {
        /// <summary>
        /// find an html element where its id = requested id
        /// </summary>
        /// <param name="id">id to search</param>
        /// <param name="w">timeout</param>
        /// <returns>null or found element</returns>
        public static IWebElement GetElementById(string id, WebDriverWait w)
        {
            IWebElement el = null;
            try
            {
                w.Until((a) => (el = a.FindElement(By.Id(id))) != null);
            }
            catch (Exception ex)
            {
                el = null;
            }
            return el;
        }

        /// <summary>
        /// find all elements that have tagName and are filtered by "filter(class name)"
        /// </summary>
        /// <param name="tagName"></param>
        /// <param name="filter"></param>
        /// <param name="w"></param>
        /// <returns></returns>
        public static IWebElement[] GetElementsByType(string tagName, string filter, WebDriverWait w)
        {
            IWebElement[] elements = null;
            try
            {
                w.Until((a) =>
                            {
                                var els = a.FindElements(By.TagName(tagName)).ToArray();
                                if (els.Length == 0)
                                {
                                    return false;
                                }
                                
                                if (!string.IsNullOrEmpty(filter))
                                {
                                    elements = els.Where(b => b.GetAttribute("id").IndexOf(filter) != -1).ToArray();
                                }
                                else if (els.Length > 0)
                                {
                                    elements = els;
                                }
                                return elements.Length > 0;
                            });
            }
            catch(Exception)
            {
                // time out
                elements = null;
            }
            return elements;
        }

        /// <summary>
        /// Find elements by class name
        /// </summary>
        /// <param name="className"></param>
        /// <param name="w"></param>
        /// <returns></returns>
        public static IWebElement[] GetElementsByClass(string className, WebDriverWait w)
        {
            IWebElement[] elements = null;
            try
            {
                w.Until((a) =>
                            {
                                elements = a.FindElements(By.ClassName(className)).ToArray();
                                return elements.Length > 0;
                            });
            }
            catch (Exception)
            {
                elements = null;
            }
            return elements;
        }

        /// <summary>
        /// in asp.net the id is different from client id. client id often contains the id at the end of the string.
        /// so search for elements by classname and then filter by the id
        /// </summary>
        /// <param name="className"></param>
        /// <param name="id"></param>
        /// <param name="w"></param>
        /// <returns></returns>
        public static IWebElement[] GetElementsByClassAndId(string className, string[] ids, WebDriverWait w)
        {
            IWebElement[] elements = null;
            try
            {
                w.Until((a) =>
                {
                    var els = a.FindElements(By.ClassName(className)).ToArray();
                    if (els.Length == 0)
                    {
                        return false;
                    }

                    List<IWebElement> temp = new List<IWebElement>();
                    foreach (string id in ids)
                    {
                        temp.AddRange(els.Where(e=>e.GetAttribute("id").EndsWith(id)).ToArray());
                    }

                    if (temp.Count > 0)
                        elements = temp.ToArray();
                    
                    return elements.Length > 0;
                });
            }
            catch (Exception)
            {
                // time out
                elements = null;
            }
            return elements;
        }

        public static IWebElement GetElementByCssSelector(string cssPath, WebDriverWait w)
        {
            IWebElement el = null;
            try
            {
                w.Until((a) => (el = a.FindElement(By.CssSelector(cssPath))) != null);
            }
            catch (Exception ex)
            {
                el = null;
            }
            return el;
        }
    }
}
