﻿using System;

namespace TIEKinetix.UnitTestings.WindowsBG
{
    public class RandomCharacter
    {
        private readonly Random _rand;
        public RandomCharacter()
        {
            _rand = new Random();
        }

        public string Generate(int length)
        {
            string s = string.Empty;
            int space = NextSpaceNum();
            for (int i = 0; i < length; i++)
            {
                s += Convert.ToChar(NextChar());
                space--;
                if (space == 0)
                {
                    space = NextSpaceNum();
                    s += " ";
                }
            }
            return s;
        }

        public string GenerateAlphaNumeric(int length, bool hasSpaces)
        {
            string s = string.Empty;
            int space = NextSpaceNum(), n = -1;
            for (int i = 0; i < length; i++)
            {
                n = NextChar();
                while (!((n>48&&n<58) || (n>64&&n<91) || (n>96&&n<123)))
                {
                    n = NextChar();
                }
                s += Convert.ToChar(n);

                space--;
                if (space != 0) continue;

                space = NextSpaceNum();
                if (hasSpaces)
                    s += " ";
            }
            return s;
        }

        public string GenerateAlphaNumeric(int length)
        {
            return GenerateAlphaNumeric(length, true);
        }

        public string GenerateNumeric(int length)
        {
            string s = string.Empty;
            int space = 4, n = -1;
            for (int i = 0; i < length; i++)
            {
                n = NextNum();
                s += Convert.ToChar(n);
            }
            return s;
        }

        public int Next(int min, int max)
        {
            return _rand.Next(min, max);
        }
        public int NextNum()
        {
            return Next(48, 57);
        }
        public int NextChar()
        {
            return Next(48, 126);
        }

        public int NextSpaceNum()
        {
            return Next(3, 7);
        }
    }
}