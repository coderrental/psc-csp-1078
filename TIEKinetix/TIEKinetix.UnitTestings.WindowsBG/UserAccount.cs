﻿namespace TIEKinetix.UnitTestings.WindowsBG
{
    public class UserAccount
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public UserAccount(string username, string password)
        {
            Username = username;
            Password = password;
        }

        public UserAccount() : this(string.Empty, string.Empty)
        {
            
        }
    }
}