﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using TIEKinetix.UnitTestings.WindowsBG.Common;


namespace TIEKinetix.UnitTestings.WindowsBG
{
    [TestFixture]
    [Category("Full Test")]
    
    public class SimpleUseCase
    {
        private static IWebDriver _browser = null;
        private static WebDriverWait _wait = null;
        //private string _url = "http://portals.syndicationtest.com/WinsBG/";
        private string _url = "http://ma-ltu-devw7/winsbg/";
        private UserAccount _userAccount;


        [TestFixtureSetUp]
        public void LoadBrowser()
        {
            Console.WriteLine("Load Browser");
            _browser = new OpenQA.Selenium.IE.InternetExplorerDriver();
            _browser.Manage().Window.Size = new Size(1000,800);
            _browser.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(20));
            
            //_browser = new FirefoxDriver();
            _wait = new WebDriverWait(_browser, TimeSpan.FromSeconds(15));

            // go to a url
            _browser.Navigate().GoToUrl(_url);

            _userAccount = new UserAccount
                               {
                                   Username = "testuser",
                                   Password = "open4me"
                               };

            //SignIn();
        }

        [TestFixtureTearDown]
        public void CleanUpBrowser()
        {
            if (_browser!=null)
                _browser.Close();
                    
        }

        [Test]
        public void _01_SignIn()
        {
            // check if the default page is the login page
            IWebElement[] inputs = Utils.GetElementsByType("input", "Login", _wait);

            if (inputs == null)
            {
                // look for dnn_LOGIN1_loginLink
                var link = Common.Utils.GetElementById("dnn_LOGIN1_loginLink", _wait);

                Assert.NotNull(link, "Can't find LOGIN link.");

                // go to login page
                link.Click();

                // search for the input textboxes again
                inputs = Utils.GetElementsByType("input", "Login", _wait);
            }

            Assert.IsNotNull(inputs, "Can't find UserName and Password Inputs");

            IWebElement loginButton = null;
            string id = string.Empty;
            foreach (var input in inputs)
            {
                id = input.GetAttribute("id");
                Console.WriteLine("Id: {0}", id);

                if (id.EndsWith("txtUsername"))
                {
                    input.SendKeys(_userAccount.Username);
                }
                else if (id.EndsWith("txtPassword"))
                {
                    input.SendKeys(_userAccount.Password);
                }
            }

            inputs = Utils.GetElementsByType("a", "cmdLogin", _wait);

            Assert.IsNotNull(inputs, "Unable to find login button");

            loginButton = inputs[0];
            loginButton.Click();
        }

        [Test]
        [Category("Company Settings")]
        [ExpectedException(typeof(NoAlertPresentException))]
        public void CompanyInformation()
        {
            RandomCharacter randomCharacter = new RandomCharacter();
            
            CompanyInformation_GoToScreen();

            var elements = Utils.GetElementsByClass("csp-form-field", _wait);
            Assert.IsNotNull(elements);

            string id = string.Empty;
            foreach (IWebElement el in elements)
            {
                id = el.GetAttribute("id");
                if (id.EndsWith("ddlCountry"))
                {
                }
                else if (id.EndsWith("tbUrl"))
                {
                    el.Clear();
                    el.SendKeys("http://" + randomCharacter.GenerateAlphaNumeric(6,false) + ".com");
                }
                else if (id.EndsWith("tbEmail"))
                {
                    el.Clear();
                    el.SendKeys(string.Format("{0}@{1}.com", randomCharacter.GenerateAlphaNumeric(10, false), randomCharacter.GenerateAlphaNumeric(5, false)));
                }
                else if (id.EndsWith("tbCompanyName"))
                {
                }   
                else if (id.EndsWith("tbPhoneNumber"))
                {
                    el.Clear();
                    el.SendKeys(randomCharacter.GenerateNumeric(10));
                    
                }
                else
                {
                    el.Clear();
                    el.SendKeys(string.Format("{0}", randomCharacter.GenerateAlphaNumeric(8)));
                }
            }

            elements = Utils.GetElementsByClass("csp-button", _wait);
            Assert.IsNotNull(elements);

            var nextButton = elements.FirstOrDefault(a => a.GetAttribute("id").EndsWith("StartNextButton"));
            Assert.IsNotNull(nextButton);

            nextButton.Click();

            var alert = _browser.SwitchTo().Alert();
            alert.Accept();
        }

        [Test]
        [Category("Company Settings")]
        public void CompanyInformation_BadData()
        {
            CompanyInformation_GoToScreen();

            RandomCharacter randomCharacter = new RandomCharacter();
            var elements = Utils.GetElementsByClass("csp-form-field", _wait);
            Assert.IsNotNull(elements);

            string id = string.Empty;
            foreach (IWebElement el in elements)
            {
                id = el.GetAttribute("id");
                if (id.EndsWith("tbUrl"))
                {
                    el.Clear();
                    el.SendKeys(randomCharacter.GenerateAlphaNumeric(20));
                }
                else if (id.EndsWith("tbEmail"))
                {
                    el.Clear();
                    el.SendKeys(randomCharacter.GenerateAlphaNumeric(20));
                }
            }

            elements = Utils.GetElementsByClass("csp-button", _wait);
            Assert.IsNotNull(elements);

            var nextButton = elements.FirstOrDefault(a => a.GetAttribute("id").EndsWith("StartNextButton"));
            Assert.IsNotNull(nextButton);

            nextButton.Click();

            var alert = _browser.SwitchTo().Alert();
            alert.Accept();
        }

        [Test]
        public void Registration()
        {
            
        }

        #region shared functions
        private void CompanyInformation_GoToScreen()
        {
            string url = _browser.Url;

            _browser.Navigate().GoToUrl(_url + "default.aspx?tabname=company settings");

            if (_browser.Url == url)
            {
                _browser.Navigate().GoToUrl(_url + "default.aspx?tabname=company settings");
                Console.WriteLine("Go to company settings screen again.");
            }
        }
        #endregion
    }
}
