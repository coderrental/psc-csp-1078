﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RollBack.ascx.cs" Inherits="TIEKinetix.CspModules.DnnTranslator.RollBack" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Panel runat="server" ID="pnRollBack">
    <div style="text-align: center">
        <h2 id="h2Title" runat="server"/>
        <hr />
    </div>
    <div style="text-align: center">
	<telerik:RadGrid Width="70%" runat="server" ID="RollBackList" OnItemCommand="RollBackList_OnItemCommand" AutoGenerateColumns="False" CellSpacing="0" GridLines="None" AllowPaging="True" PageSize="10">
		<MasterTableView>
			<Columns>
			    <telerik:GridBoundColumn DataField="FolderName" UniqueName="FolderName" Display="False">
				</telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="DateTime" UniqueName="DateTime" HeaderText="Backup Check point">
				</telerik:GridBoundColumn>
				<telerik:GridButtonColumn  Text="Rollback" ButtonType="ImageButton" ButtonCssClass="rbRefresh" CommandName="rollback" HeaderText="Action"> 
				</telerik:GridButtonColumn> 
			</Columns>
		</MasterTableView>
	</telerik:RadGrid>
    </div>
</asp:Panel>