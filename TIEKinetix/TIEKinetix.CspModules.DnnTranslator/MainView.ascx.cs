﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using DocumentFormat.OpenXml.Extensions;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DotNetNuke.Common;
using DotNetNuke.Data;
using TIEKinetix.CspModules.DnnTranslator.Common;
using TIEKinetix.CspModules.DnnTranslator.CustomSheets;
using TIEKinetix.CspModules.DnnTranslator.Entities;

namespace TIEKinetix.CspModules.DnnTranslator
{
    public partial class MainView : DnnModuleBase
    {
        private List<SheetEntity> _sheets;
        private List<FileInfo> _filesInfo;
        private List<string> _langsToTranslate;
        private List<DesktopModule> _desktopModules;
        private List<CustomSheet> _customSheets;
        private List<string> _listCountryCode; 
        /// <summary>
        /// Handles the Init event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 10/29/2012 - 10:08 AM
        protected override void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);

            _sheets = new List<SheetEntity>();
            _langsToTranslate = new List<string>();
            _desktopModules = new List<DesktopModule>();
            _customSheets = new List<CustomSheet>();

            var cinfo = CultureInfo.GetCultures(CultureTypes.AllCultures & ~CultureTypes.NeutralCultures);
             _listCountryCode = new List<String>();
            foreach (CultureInfo cultureInfo in cinfo)
            {
                var parent = CultureInfo.GetCultureInfo(cultureInfo.Name);
                var regionalLanguages = CultureInfo.GetCultures(CultureTypes.AllCultures).Where(x => x.Parent.Equals(parent)).ToList();
                if (regionalLanguages.Any())
                {
                    _listCountryCode.Add(regionalLanguages.First().Name);
                }

            }

            //Localized the controls
            LocalizeControls();

            InitLanguagesToTranslate();
        }

        /// <summary>
        /// Inits the languages to translate.
        /// </summary>
        /// Author: Vu Dinh
        /// 10/30/2012 - 9:31 AM
        private void InitLanguagesToTranslate()
        {
            string query = @"select b.CultureCode
from PortalLanguages a 
 join Languages b on a.LanguageID=b.LanguageID
where a.PortalID={0}";
            query = string.Format(query, PortalId);
            var reader = DataProvider.Instance().ExecuteSQL(query);
            if (reader != null)
            {
                while (reader.Read())
                {
                    _langsToTranslate.Add(reader["CultureCode"].ToString());
                }
            }
        }

        /// <summary>
        /// Localizes the controls.
        /// </summary>
        /// Author: Vu Dinh
        /// 10/29/2012 - 10:09 AM
        private void LocalizeControls()
        {
            btnExport.Text = GetLocalizedText("Label.Export");
            btnImport.Text = GetLocalizedText("Label.Import");
            btnRollback.Text = GetLocalizedText("Label.Rollback");
            h2Title.InnerText = GetLocalizedText("Label.ModuleTitle");
        }


        /// <summary>
        /// Scans the RESX files.
        /// </summary>
        /// <param name="moduleName">Name of the module.</param>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 10/30/2012 - 8:46 AM
        private SheetEntity ScanResxFiles(string moduleName, string path)
        {
            var sheet = new SheetEntity { SheetName = moduleName };
            //scan files
            _filesInfo = new List<FileInfo>();
            //Scan now
            Exception ex = ScanFiles("resx",path);
            if (ex != null)
                sheet.Error = ex;

            var listTranslateItems = new List<TranslateItem>();
            foreach (var file in _filesInfo)
            {
                var translateItems = ReadResourceFile(file);
                if (listTranslateItems.Any())
                {
                    foreach (var translateItem in translateItems)
                    {
                        bool match = false;
                        foreach (var item in listTranslateItems)
                        {
                            if (translateItem.Label == item.Label)
                            {
                                match = true;
                                foreach (DictionaryEntry entry in translateItem.LangValuePair)
                                {
                                    if (item.LangValuePair.ContainsKey(entry.Key))
                                        continue;
                                    item.LangValuePair.Add(entry.Key, entry.Value);
                                }
                            }
                        }
                        if (match == false)
                        {
                            listTranslateItems.Add(translateItem);
                        }
                    }
                }
                else
                {
                    listTranslateItems.AddRange(translateItems);
                }
                
            }
            listTranslateItems = listTranslateItems.OrderBy(x => x.Label).ToList();
            sheet.TranslateItems = listTranslateItems;
            return sheet;
        }

        /// <summary>
        /// Reads the resource file.
        /// </summary>
        /// <param name="resxFile">The RESX file.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 10/30/2012 - 8:48 AM
        private IEnumerable<TranslateItem> ReadResourceFile(FileInfo resxFile)
        {
            var langName = "en-US";
            var fileFriendlyName = GetResxFriendlyName(resxFile.Name);
            //Get all culture code
            var cinfo = CultureInfo.GetCultures(CultureTypes.AllCultures & ~CultureTypes.NeutralCultures).ToList();
            foreach (var lang in cinfo.Where(lang => resxFile.Name.Contains("." +lang.Name + "."))) //Resource with diffirent language
            {
                //Skip scan this file if not use in current portal
                //if (!resxFile.Name.Contains("Portal-" + PortalId))
                    //return new List<TranslateItem>();
                langName = lang.Name;
            }
            var translateItems = new List<TranslateItem>();

            var xmlDocument = new XmlDocument {PreserveWhitespace = true};
            xmlDocument.Load(@resxFile.FullName);
            var nodes = xmlDocument.GetElementsByTagName("data");
            for (int i = 0; i < nodes.Count; i++)
            {
                var node = nodes[i];
                var att = node.Attributes;
                var value = node["value"];
                if (att != null && value != null)
                {
                    var item = new TranslateItem {Label = fileFriendlyName + "|" + att["name"].Value};
                    item.LangValuePair[langName] = value.InnerText;
                    translateItems.Add(item);
                }
            }
            return translateItems;
        }

        /// <summary>
        /// Gets the name of the RESX file.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns>System.String.</returns>
        private string GetResxFriendlyName(string filename)
        {
            var splited = filename.Split(new []{"."}, StringSplitOptions.RemoveEmptyEntries);
            var breakIndex = 0;
            for (int i = splited.Length - 1; i >= 0; i--)
            {
                if (splited[i].Contains("Portal") || splited[i].Contains("ascx") || splited[i].Contains("resx") || _listCountryCode.Count(a => splited[i].Contains(a)) > 0)
                    continue;
                breakIndex = i;
                break;
            }
            var name = string.Empty;
            for (int i = 0; i <= breakIndex; i++)
            {
                name += splited[i] + ".";
            }
            return name.Remove(name.Length - 1);
        }

        /// <summary>
        /// Scans the files.
        /// </summary>
        /// <param name="fileExtention">The file extension to search.</param>
        /// <param name="pathToscan">The path toscan.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 10/29/2012 - 2:51 PM
        private Exception ScanFiles(string fileExtention, string pathToscan)
        {
            try
            {
                var portalPartern = "Portal-" + PortalId;
                var temp = new List<Hashtable>();
                var dir = new DirectoryInfo(pathToscan);
                //Get portal's resx
                foreach (var file in dir.GetFiles())
                {
                    if (file.Extension.Contains(fileExtention))
                    {
                        var resxName = GetResxFriendlyName(file.Name);
                        var item = new Hashtable();
                        item[resxName] = null;
                        
                        if (file.Name.Contains(portalPartern) && !_filesInfo.Contains(file))
                        {
                            item[resxName] = file;
                            _filesInfo.Add(file);
                        }
                        temp.Add(item);
                    }
                   
                       
                }

                //Get root resx file (ignore if has portal's resx above)
                foreach (var file in dir.GetFiles())
                {
                    if (file.Extension.Contains(fileExtention))
                    {
                        var resxName = GetResxFriendlyName(file.Name);
                        foreach (var hashItem in temp)
                        {
                            if (hashItem.ContainsKey(resxName) && hashItem[resxName] != null)
                                continue;
                            if (!_filesInfo.Contains(file) && !file.Name.Contains("Portal"))
                                _filesInfo.Add(file);
                        }
                    }
                }
                foreach (var directory in dir.GetDirectories())
                {
                    //recursive here to scan all files in sub diretories
                    ScanFiles(fileExtention, directory.FullName);
                }
            }
            catch(Exception exception)
            {
                return exception;
            }
            return null;
        }

        /// <summary>
        /// Exports the sheets.
        /// </summary>
        /// Author: Vu Dinh
        /// 10/30/2012 - 10:55 AM
        private void ExportSheets()
        {
            // Sort _sheets
            _sheets = _sheets.OrderBy(x => x.SheetName).ToList();
            // Create excel stream
            MemoryStream stream = SpreadsheetReader.Create();
            // Open the document for editing.
            using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(stream,true))
            {
                for (int i = 1; i <= _sheets.Count; i++)
                {
                    var item = _sheets[i - 1];
                    if (i <= 3)
                    {
                        var sheets = spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>();
                        Sheet sheet = sheets.Elements<Sheet>().Single(a => a.Name == "Sheet" + i);
                        sheet.Name = item.SheetName;
                        spreadSheet.WorkbookPart.Workbook.Save();
                    }
                    else
                    {
                        // Add a blank WorksheetPart.
                        var newWorksheetPart = spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();
                        newWorksheetPart.Worksheet = new Worksheet(new SheetData());

                        var sheets = spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>();

                        // Get a unique ID for the new worksheet.
                        uint sheetId = 1;
                        if (sheets.Elements<Sheet>().Any())
                        {
                            sheetId = sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                        }

                        // Append the new worksheet and associate it with the workbook.
                        Sheet sheet = new Sheet() { Id = spreadSheet.WorkbookPart.GetIdOfPart(newWorksheetPart), SheetId = sheetId, Name = item.SheetName };
                        sheets.Append(sheet);
                        spreadSheet.WorkbookPart.Workbook.Save();
                    }

                    var worksheetPart = SpreadsheetReader.GetWorksheetPartByName(spreadSheet, item.SheetName);
                    var writer = new WorksheetWriter(spreadSheet, worksheetPart);
                    //Write data to sheet. Col name at A1, data from A2
                    writer.PasteDataTable(GetDataTableFromSheetEntity(item), "A1");
                }
				
                //Add customsheet
                if (_customSheets.Any())
                {
                    foreach (var customSheet in _customSheets)
                    {
						if (customSheet == null)
							continue;
                        AddCustomSheet(customSheet.Sheetname, customSheet.Data, spreadSheet);
                    }
                }
            }

        

           // Write to response stream
            Response.Clear();
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", ExcelFilename + ".xlsx"));
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            stream.WriteTo(Response.OutputStream);
            Response.End();
        }

        /// <summary>
        /// Adds the custom sheet.
        /// </summary>
        /// <param name="sheetname">The sheetname.</param>
        /// <param name="dt">The dt.</param>
        /// <param name="spreadSheet">The spread sheet.</param>
        /// Author: Vu Dinh
        /// 10/31/2012 - 8:06 AM
        private void AddCustomSheet(string sheetname, DataTable dt, SpreadsheetDocument spreadSheet)
        {
            // Add a blank WorksheetPart.
            var newWorksheetPart = spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();
            newWorksheetPart.Worksheet = new Worksheet(new SheetData());

            var sheets = spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>();

            // Get a unique ID for the new worksheet.
            uint sheetId = 1;
            if (sheets.Elements<Sheet>().Any())
            {
                sheetId = sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
            }

            // Append the new worksheet and associate it with the workbook.
            var sheet = new Sheet() { Id = spreadSheet.WorkbookPart.GetIdOfPart(newWorksheetPart), SheetId = sheetId, Name = sheetname };
            sheets.Append(sheet);
            spreadSheet.WorkbookPart.Workbook.Save();

            var worksheetPart = SpreadsheetReader.GetWorksheetPartByName(spreadSheet, sheetname);
            var writer = new WorksheetWriter(spreadSheet, worksheetPart);
          
            //Write data to sheet. Col name at A1, data from A2
            writer.PasteDataTable(dt, "A1");
        }

        /// <summary>
        /// Gets the data table from sheet entity.
        /// </summary>
        /// <param name="sheetEntity">The sheet entity.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 10/30/2012 - 11:31 AM
        private DataTable GetDataTableFromSheetEntity(SheetEntity sheetEntity)
        {
            var dt = new DataTable();
            if (sheetEntity.TranslateItems.Any())
            {
                dt.Columns.Add(new DataColumn("Language"));
                //Setup column
                foreach (var item in sheetEntity.TranslateItems)
                {
                    dt.Columns.Add(new DataColumn(item.Label));
                }

                //Setup first row
                DataRow firstRow = dt.NewRow();
                firstRow[0] = "Language";
                for (int i = 0; i < sheetEntity.TranslateItems.Count; i++)
                {
                    firstRow[i + 1] = sheetEntity.TranslateItems[i].Label;
                }
                dt.Rows.Add(firstRow);

                foreach (string lang in _langsToTranslate)
                {
                    DataRow row = dt.NewRow();
                    row[0] = lang;
                    for (int i = 0; i < sheetEntity.TranslateItems.Count; i++)
                    {
                        var tranlateItem = sheetEntity.TranslateItems[i].LangValuePair[lang];
                        row[i + 1] = tranlateItem == null ? string.Empty : tranlateItem.ToString();

                    }
                    dt.Rows.Add(row);
                }
            }
            else if (sheetEntity.Error != null) //Display error to sheet
            {
                dt.Columns.Add(new DataColumn("Error"));
                DataRow mesgRow = dt.NewRow();
                mesgRow[0] = sheetEntity.Error.Message;
                dt.Rows.Add(mesgRow);

                DataRow traceRow = dt.NewRow();
                traceRow[0] = sheetEntity.Error.StackTrace;
                dt.Rows.Add(traceRow);
            }
            return dt;
        }

        /// <summary>
        /// Gets the list of used module.
        /// </summary>
        /// Author: Vu Dinh
        /// 10/30/2012 - 4:06 PM
        private void GetListOfUsedModule()
        {
            string query = @"select distinct a.DesktopModuleID, a.FriendlyName, a.FolderName from DesktopModules a
join ModuleDefinitions b on b.DesktopModuleID = a.DesktopModuleID
join Modules c on c.ModuleDefID = b.ModuleDefID
join TabModules d on d.ModuleID = c.ModuleID
where c.PortalID = {0}";
            query = string.Format(query, PortalId);
            var dataReader = DataProvider.Instance().ExecuteSQL(query);
            while (dataReader.Read())
            {
                _desktopModules.Add(new DesktopModule
                                        {
                                            FriendlyName = dataReader["FriendlyName"].ToString(),
                                            FolderName = dataReader["FolderName"].ToString()
                                        });
            }
        }

        #region [ Event Handlers ]
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 10/29/2012 - 10:08 AM
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Handles the OnClick event of the btnExport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 10/29/2012 - 10:08 AM
        /// <remarks>ltu 4/10/13: add a custom sheet for VR integration</remarks>
        /// 
        protected void btnExport_OnClick(object sender, EventArgs e)
        {
            //Scan module
            GetListOfUsedModule();
            if (_desktopModules.Any())
            {
                foreach (var desktopModule in _desktopModules)
                {
                    _sheets.Add(ScanResxFiles(desktopModule.FriendlyName, Server.MapPath(@"DesktopModules\" + desktopModule.FolderName)));

					// note: hardcode this module name
					if (desktopModule.FriendlyName == "CspVRIntegrationModule")
					{
						_customSheets.Add(CustomSheetFactory.CreateSheet("@DNN@CspVRIntegrationModule", PortalId, TabModuleId));
					}
                }  
            }
            
            //Export all Dnn special sheets
            var specialSheets = GetDnnSpecialSheets();
            foreach (var entry in specialSheets)
            {
                _sheets.Add(ScanResxFiles(entry.Key,Server.MapPath(entry.Value)));
            }

            
            if (_sheets.Any())
            {
				_customSheets.Add(CustomSheetFactory.CreateSheet("@DNN@Csp Html Module", PortalId, TabModuleId));
				_customSheets.Add(CustomSheetFactory.CreateSheet("@DNN@EaloTab", PortalId, TabModuleId));
				_customSheets.Add(CustomSheetFactory.CreateSheet("@DNN@CspSubscriptionModule", PortalId, TabModuleId));
				
                //Export data to excel file
                ExportSheets();    
            }
        }


    	#endregion

        /// <summary>
        /// Handles the OnClick event of the btnImport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 10/29/2012 - 10:26 AM
        protected void btnImport_OnClick(object sender, EventArgs e)
        {
            Response.Redirect(Globals.NavigateURL(TabId,"Import","mid",ModuleId.ToString()));
        }

        /// <summary>
        /// Handles the OnClick event of the btnRollback control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 11/2/2012 - 10:08 AM
        protected void btnRollback_OnClick(object sender, EventArgs e)
        {
            Response.Redirect(Globals.NavigateURL(TabId, "Rollback", "mid", ModuleId.ToString()));
        }
    }
}