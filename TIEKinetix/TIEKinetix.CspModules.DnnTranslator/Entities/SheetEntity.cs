﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TIEKinetix.CspModules.DnnTranslator.Entities
{
    public class SheetEntity
    {
        public SheetEntity()
        {
            TranslateItems = new List<TranslateItem>();
        }
        /// <summary>
        /// Gets or sets the name of the sheet.
        /// </summary>
        /// <value>
        /// The name of the sheet.
        /// </value>
        /// Author: Vu Dinh
        /// 10/29/2012 - 10:21 PM
        public string SheetName { get; set; }

        /// <summary>
        /// Gets or sets the translate items.
        /// </summary>
        /// <value>
        /// The translate items.
        /// </value>
        /// Author: Vu Dinh
        /// 10/29/2012 - 10:31 PM
        public List<TranslateItem> TranslateItems { get; set; }

        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        /// Author: Vu Dinh
        /// 10/31/2012 - 10:55 AM
        public Exception Error { get; set; }
    }
}