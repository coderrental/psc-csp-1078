﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TIEKinetix.CspModules.DnnTranslator.Entities
{
    public class BackupCheckpoint
    {
        /// <summary>
        /// Gets or sets the name of the folder.
        /// </summary>
        /// <value>
        /// The name of the folder.
        /// </value>
        /// Author: Vu Dinh
        /// 11/5/2012 - 6:24 PM
        public string FolderName { get; set; }

        /// <summary>
        /// Gets or sets the date time.
        /// </summary>
        /// <value>
        /// The date time.
        /// </value>
        /// Author: Vu Dinh
        /// 11/5/2012 - 6:24 PM
        public DateTime DateTime { get; set; }
    }
}