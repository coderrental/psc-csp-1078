﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Collections;


namespace TIEKinetix.CspModules.DnnTranslator.Entities
{
    public class ResourceFile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceFile"/> class.
        /// </summary>
        /// Author: Vu Dinh
        /// 11/5/2012 - 11:09 AM
        public ResourceFile()
        {
            LabelValuePair = new Hashtable();
        }
        /// <summary>
        /// Gets or sets the files info.
        /// </summary>
        /// <value>
        /// The files info.
        /// </value>
        /// Author: Vu Dinh
        /// 10/29/2012 - 10:37 PM
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the language.
        /// </summary>
        /// <value>
        /// The language.
        /// </value>
        /// Author: Vu Dinh
        /// 11/5/2012 - 10:22 AM
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the label value pair.
        /// </summary>
        /// <value>
        /// The label value pair.
        /// </value>
        /// Author: Vu Dinh
        /// 11/5/2012 - 10:22 AM
        public Hashtable LabelValuePair { get; set; }
    }
}