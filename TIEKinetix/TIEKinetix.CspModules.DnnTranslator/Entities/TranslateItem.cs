﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace TIEKinetix.CspModules.DnnTranslator.Entities
{
    public class TranslateItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TranslateItem"/> class.
        /// </summary>
        /// Author: Vu Dinh
        /// 10/30/2012 - 7:41 AM
        public TranslateItem()
        {
            LangValuePair = new Hashtable();
        }
        /// <summary>
        /// Gets or sets the label.
        /// </summary>
        /// <value>
        /// The label.
        /// </value>
        /// Author: Vu Dinh
        /// 10/29/2012 - 10:31 PM
        public string Label { get; set; }

        /// <summary>
        /// Gets or sets the lang value pair.
        /// </summary>
        /// <value>
        /// The lang value pair.
        /// </value>
        /// Author: Vu Dinh
        /// 10/30/2012 - 7:37 AM
        public Hashtable LangValuePair { get; set; }
    }
}