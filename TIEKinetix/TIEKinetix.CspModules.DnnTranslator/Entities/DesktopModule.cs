﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TIEKinetix.CspModules.DnnTranslator.Entities
{
    public class DesktopModule
    {
        /// <summary>
        /// Gets or sets the name of the friendly.
        /// </summary>
        /// <value>
        /// The name of the friendly.
        /// </value>
        /// Author: Vu Dinh
        /// 10/30/2012 - 4:03 PM
        public string FriendlyName { get; set; }

        /// <summary>
        /// Gets or sets the name of the folder.
        /// </summary>
        /// <value>
        /// The name of the folder.
        /// </value>
        /// Author: Vu Dinh
        /// 10/30/2012 - 4:03 PM
        public string FolderName { get; set; }
    }
}