﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Users;
using TIEKinetix.CspModules.DnnTranslator.Common;

namespace TIEKinetix.CspModules.DnnTranslator
{
    public partial class ModuleSettings : ModuleSettingsBase
    {
        /// <summary>
        /// Loads the settings.
        /// </summary>
        /// Author: Vu Dinh
        /// 10/29/2012 - 3:49 PM
        public override void LoadSettings()
        {
            if (TabModuleSettings[Cons.SETTING_EXCEL_FILENAME] != null)
            {
                tbxFilename.Text = TabModuleSettings[Cons.SETTING_EXCEL_FILENAME].ToString();
            }
        }

        /// <summary>
        /// Updates the settings.
        /// </summary>
        /// Author: Vu Dinh
        /// 10/29/2012 - 3:52 PM
        public override void UpdateSettings()
        {
            var moduleController = new ModuleController();
            moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_EXCEL_FILENAME, tbxFilename.Text);
            ModuleController.SynchronizeModule(ModuleId);
        }
    }
}