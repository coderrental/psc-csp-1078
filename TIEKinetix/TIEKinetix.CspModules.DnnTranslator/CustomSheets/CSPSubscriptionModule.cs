﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DocumentFormat.OpenXml.Spreadsheet;
using DotNetNuke.Data;
using TIEKinetix.CspModules.DnnTranslator.Common;

namespace TIEKinetix.CspModules.DnnTranslator.CustomSheets
{
	public class CspSubscriptionModule : CustomSheet
	{
		private string _bkFolder;
		private int _portalId;
		private int _tabModuleId;
		#region Overrides of CustomSheet

		public override ICustomSheet Export(int portalId, int tabModuleId)
		{
			_portalId = portalId;
			GetData();
			return this;
		}

		/// <summary>
		/// Imports the specified worksheet.
		/// </summary>
		/// <param name="worksheet">The worksheet.</param>
		/// <param name="sharedString">The shared string.</param>
		/// <param name="sheetTitle">The sheet title.</param>
		/// <param name="portalId">The portal id.</param>
		/// <param name="tabModuleId">The tab module id.</param>
		/// <param name="backupFolder">The backup folder.</param>
		/// <returns></returns>
		/// Author: ducuytran
		/// 11/4/2012 - 7:40 PM
		public override bool Import(Worksheet worksheet, SharedStringTable sharedString, string sheetTitle, int portalId, int tabModuleId, int userId, string backupFolder)
		{
			_portalId = portalId;
			_tabModuleId = tabModuleId;
			UserId = userId;
			Sheetname = sheetTitle;
			BackupFolder = backupFolder;
			//Backup
			if (!String.IsNullOrEmpty(BackupFolder))
				DoBackup();
			//Create datatable columns
			InitDataColumn();
			//Get data from sheet then insert into datatable
			InitData(worksheet, sharedString);
			//Update data to DB
			DoImport();
			return true;
		}

	    public override bool RollBack(DateTime time)
	    {
	    	return true;
	        throw new NotImplementedException();
	    }

		/// <summary>
		/// Inits the data column.
		/// </summary>
		/// Author: ducuytran
		/// 11/2/2012 - 6:34 PM
		public override void InitDataColumn()
		{
			Data = new DataTable();
			DataColumn CSpTransCol;

			//Qualifier col
			CSpTransCol = new DataColumn();
			CSpTransCol.DataType = System.Type.GetType("System.Guid");
			CSpTransCol.ColumnName = "Id";
			Data.Columns.Add(CSpTransCol);

			//StringName col
			CSpTransCol = new DataColumn();
			CSpTransCol.DataType = System.Type.GetType("System.String");
			CSpTransCol.ColumnName = "ShortDesc";
			Data.Columns.Add(CSpTransCol);

			//StringText col
			CSpTransCol = new DataColumn();
			CSpTransCol.DataType = System.Type.GetType("System.String");
			CSpTransCol.ColumnName = "LongDesc";
			Data.Columns.Add(CSpTransCol);

			//Locale col
			CSpTransCol = new DataColumn();
			CSpTransCol.DataType = System.Type.GetType("System.String");
			CSpTransCol.ColumnName = "CultureCode";
			Data.Columns.Add(CSpTransCol);
		}

		/// <summary>
		/// Inits the data.
		/// </summary>
		/// <param name="worksheet">The worksheet.</param>
		/// <param name="sharedString">The shared string.</param>
		/// Author: ducuytran
		/// 11/2/2012 - 6:34 PM
		private void InitData(Worksheet worksheet, SharedStringTable sharedString)
		{
			bool isFirstRow = true;
			int crrRow = 0;
			String[] textValuesArr;
			List<String> tabList = new List<string>();
			DataRow ealoRow;
            var listColumn = new List<string>();
            var x = 1;
			foreach (var row in worksheet.Descendants<Row>())
			{
                var textValues = new List<string>();
                if (x == 1) //First row
                {
                    foreach (var cell in row.Elements<Cell>())
                    {
                        listColumn.Add(cell.CellReference.InnerText.Replace(x.ToString(), string.Empty));
                        textValues.Add(GetCellValue(worksheet, sharedString, cell.CellReference));
                    }
                }
                else
                {
                    if (GetCellValue(worksheet, sharedString, listColumn[0] + x) == string.Empty)
                        break;
                    //textValues.AddRange(listColumn.Select(column => GetCellValue(worksheet, sharedString, column + x)));
					foreach (var cell in row.Elements<Cell>())
					{
						try
						{
							textValues.Add(GetCellValue(worksheet, sharedString, cell.CellReference));
						}
						catch (Exception)
						{
							textValues.Add(string.Empty);
						}
					}
                }
				//Check to verify the row contained data.
				if (textValues.Any())
				{
					textValuesArr = textValues.ToArray();
					int realCol = (textValuesArr.Length - 1)/2;
					if (x == 1)
					{
						for (int i = 1; i < textValuesArr.Count(); i++)
						{
							tabList.Add(textValuesArr[i].Split(new char[] { '|' })[0]);
						}
					}
					else
					{
						for (int i = 1; i <= realCol; i++)
						{
							if (String.IsNullOrEmpty(textValuesArr[i]) && String.IsNullOrEmpty(textValuesArr[i + realCol]))
								continue;
							ealoRow = Data.NewRow();
							ealoRow["Id"] = tabList[i - 1];
							ealoRow["ShortDesc"] = textValuesArr[i];
							ealoRow["LongDesc"] = textValuesArr[i + realCol];
							ealoRow["CultureCode"] = textValuesArr[0];
							Data.Rows.Add(ealoRow);
						}
					}
				}
				else
				{
					//If no cells, then you have reached the end of the table.
					break;
				}
			    x++;
			}
		}

		/// <summary>
		/// Does the import.
		/// </summary>
		/// Author: ducuytran
		/// 11/5/2012 - 10:38 AM
		private void DoImport()
		{
			if (Data.Rows.Count <= 0) return;

			var dataProvider = DataProvider.Instance();

			//Remove old data
			string queryRemove =
				String.Format(@"delete from CSPSubscriptionModule_Translation
where Id IN (select distinct FormId from CSPSubscriptionModule_Workflow where PortalId = {0}) OR Id IN (select distinct cf.FieldId from CSPSubscriptionModule_Field cf join CSPSubscriptionModule_Workflow cw on cf.FormId = cw.FormId where PortalId = {0})", _portalId);
			dataProvider.ExecuteSQL(queryRemove);

			SqlConnection conn = new SqlConnection(dataProvider.ConnectionString);
			conn.Open();
			SqlCommand sqlCommand =
				new SqlCommand(
					"INSERT INTO CSPSubscriptionModule_Translation (Id, ShortDesc, LongDesc, CultureCode) VALUES (@id, @sdesc, @ldesc, @ccode)", conn);
			sqlCommand.CommandType = CommandType.Text;

			foreach (DataRow row in Data.Rows)
			{
				sqlCommand.Parameters.Clear();
				sqlCommand.Parameters.AddWithValue("@id", row["Id"]);
				sqlCommand.Parameters.AddWithValue("@sdesc", row["ShortDesc"]);
				sqlCommand.Parameters.AddWithValue("@ldesc", row["LongDesc"]);
				sqlCommand.Parameters.AddWithValue("@ccode", row["CultureCode"]);
				//Save to Ealo TB
				try
				{
					sqlCommand.ExecuteNonQuery();
				}
				catch (Exception exc)//Skip the failed row
				{
					continue;
					throw;
				}
				
			}
		}

		#endregion

		/// <summary>
		/// Gets the data.
		/// </summary>
		/// Author: lam.tu
		public override void GetData()
		{
			
			int count = (int)DbHelper.ExecuteScalar("select COUNT(*) from INFORMATION_SCHEMA.TABLES a where a.TABLE_NAME='CSPSubscriptionModule_Translation' and a.TABLE_TYPE='BASE TABLE'");

			// return empty if there is no ealo table
			if (count == 0) return;

			string columnsToPivot = "";
			try
			{
				columnsToPivot = (string)DbHelper.ExecuteScalar(string.Format(@"
declare @s varchar(1000)
declare @temp table (id uniqueidentifier primary key)
insert into @temp(id)
select distinct ct.Id from CSPSubscriptionModule_Translation ct
where ct.Id IN (select distinct FormId from CSPSubscriptionModule_Workflow where PortalId = {0}) OR ct.Id IN (select distinct cf.FieldId from CSPSubscriptionModule_Field cf join CSPSubscriptionModule_Workflow cw on cf.FormId = cw.FormId where PortalId = {0})

select @s = coalesce(@s+',['+cast(a.id as varchar(38))+']', '['+cast(a.id as varchar(38))+']') 
from  @temp a
delete from @temp
select @s", _portalId));
			}
			catch (Exception)
			{
				return;
			}


			if (string.IsNullOrEmpty(columnsToPivot)) return;

			string pivotQuery = @"
select * from
(
select a.ShortDesc + '@CSPST@' + a.LongDesc as TheDesc, a.CultureCode as [Language], convert(varchar(38), a.Id)  as [ColumnName]
from CSPSubscriptionModule_Translation a where a.CultureCode in (select l.CultureCode from Languages l join PortalLanguages pl on pl.LanguageID = l.LanguageID where pl.PortalID = {0})
) as src

pivot
(
 max(src.TheDesc) for src.columnname in ({1})
) as p";

			var dataProvider = DataProvider.Instance();
			DataRow row;

			using (var dataReader = dataProvider.ExecuteSQL(string.Format(pivotQuery, _portalId, columnsToPivot)))
			{
				DataTable tableHeader = dataReader.GetSchemaTable();
				foreach (DataColumn dataColumn in tableHeader.Columns)
				{
					dataColumn.ReadOnly = false;
				}
				int columnCount = tableHeader.Rows.Count;

				for (int j = 1; j < columnCount; j++)
				{
					DataRow dtRow = tableHeader.NewRow();
					dtRow["ColumnName"] = tableHeader.Rows[j]["ColumnName"] + "|LongDesc";
					tableHeader.Rows.Add(dtRow);
					tableHeader.Rows[j]["ColumnName"] = tableHeader.Rows[j]["ColumnName"] + "|ShortDesc";
				}
				
				row = Data.NewRow();
				for (int i = 0; i < tableHeader.Rows.Count; i++)
				{
					Data.Columns.Add(tableHeader.Rows[i].ItemArray[tableHeader.Columns["ColumnName"].Ordinal].ToString());
					row.SetField(i, tableHeader.Rows[i].ItemArray[tableHeader.Columns["ColumnName"].Ordinal].ToString());
				}
				Data.Rows.Add(row);

				

				// fill in available languages
				while (dataReader.Read())
				{
					row = Data.NewRow();
					for (int i = 0; i < columnCount; i++)
					{
						if (i == 0)
							row.SetField(i, dataReader[i].ToString().Trim());
						else
						{
							string[] tmp = dataReader[i].ToString().Split(new string[] {"@CSPST@"},StringSplitOptions.RemoveEmptyEntries);
							
							if (tmp.Length == 2)
							{
								row.SetField(i, tmp[0]);
								row.SetField((i + columnCount - 1), tmp[1]);
							}
							else if (tmp.Length == 1)
							{
								if (dataReader[i].ToString().IndexOf("@CSPST@") == 0)
								{
									row.SetField(i, String.Empty);
									row.SetField((i + columnCount - 1), tmp[0]);
								}
								else
								{
									row.SetField(i, tmp[0]);
									row.SetField((i + columnCount - 1), String.Empty);
								}
							}
							else
							{
								row.SetField(i, string.Empty);
								row.SetField((i + columnCount - 1), string.Empty);
							}
						}
					}
					Data.Rows.Add(row);
				}
			}

			// fill in missing languages
			using (var dataReader = DataProvider.Instance().ExecuteSQL(string.Format(@"
select l.CultureCode
from Languages l
	join PortalLanguages pl on pl.LanguageID=l.LanguageID
where pl.PortalID = {0}	", _portalId)))
			{
				while (dataReader.Read())
				{
					if (Found(dataReader.GetString(0))) continue;

					row = Data.NewRow();
					row.SetField(0, dataReader[0]);
					Data.Rows.Add(row);
				}
			}
		}
	}
}