﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DocumentFormat.OpenXml.Spreadsheet;
using DotNetNuke.Data;
using TIEKinetix.CspModules.DnnTranslator.Common;

namespace TIEKinetix.CspModules.DnnTranslator.CustomSheets
{
	public class EaloTab : CustomSheet
	{
		private string _bkFolder;
		private int _portalId;
		private int _tabModuleId;
		#region Overrides of CustomSheet

		public override ICustomSheet Export(int portalId, int tabModuleId)
		{
			_portalId = portalId;
			GetData();
			return this;
		}

		/// <summary>
		/// Imports the specified worksheet.
		/// </summary>
		/// <param name="worksheet">The worksheet.</param>
		/// <param name="sharedString">The shared string.</param>
		/// <param name="sheetTitle">The sheet title.</param>
		/// <param name="portalId">The portal id.</param>
		/// <param name="tabModuleId">The tab module id.</param>
		/// <param name="backupFolder">The backup folder.</param>
		/// <returns></returns>
		/// Author: ducuytran
		/// 11/4/2012 - 7:40 PM
		public override bool Import(Worksheet worksheet, SharedStringTable sharedString, string sheetTitle, int portalId, int tabModuleId, int userId, string backupFolder)
		{
			_portalId = portalId;
			_tabModuleId = tabModuleId;
			UserId = userId;
			Sheetname = sheetTitle;
			BackupFolder = backupFolder;
			//Backup
			if (!String.IsNullOrEmpty(BackupFolder))
				DoBackup();
			//Create datatable columns
			InitDataColumn();
			//Get data from sheet then insert into datatable
			InitData(worksheet, sharedString);
			//Update data to DB
			DoImport();
			return true;
		}

	    public override bool RollBack(DateTime time)
	    {
	    	return true;
	        throw new NotImplementedException();
	    }

		/// <summary>
		/// Inits the data column.
		/// </summary>
		/// Author: ducuytran
		/// 11/2/2012 - 6:34 PM
		public override void InitDataColumn()
		{
			Data = new DataTable();
			DataColumn ealoCol;

			//Qualifier col
			ealoCol = new DataColumn();
			ealoCol.DataType = System.Type.GetType("System.String");
			ealoCol.ColumnName = "Qualifier";
			Data.Columns.Add(ealoCol);

			//StringName col
			ealoCol = new DataColumn();
			ealoCol.DataType = System.Type.GetType("System.String");
			ealoCol.ColumnName = "StringName";
			Data.Columns.Add(ealoCol);

			//StringText col
			ealoCol = new DataColumn();
			ealoCol.DataType = System.Type.GetType("System.String");
			ealoCol.ColumnName = "StringText";
			Data.Columns.Add(ealoCol);

			//Locale col
			ealoCol = new DataColumn();
			ealoCol.DataType = System.Type.GetType("System.String");
			ealoCol.ColumnName = "Locale";
			Data.Columns.Add(ealoCol);
		}

		/// <summary>
		/// Inits the data.
		/// </summary>
		/// <param name="worksheet">The worksheet.</param>
		/// <param name="sharedString">The shared string.</param>
		/// Author: ducuytran
		/// 11/2/2012 - 6:34 PM
		private void InitData(Worksheet worksheet, SharedStringTable sharedString)
		{
			bool isFirstRow = true;
			String[] textValuesArr;
			List<String> tabList = new List<string>();
			DataRow ealoRow;
            var listColumn = new List<string>();
            var x = 1;
			foreach (var row in worksheet.Descendants<Row>())
	        {
                var textValues = new List<string>();
                if (x == 1) //First row
                {
					foreach (var cell in row.Elements<Cell>())
                    {
                        listColumn.Add(cell.CellReference.InnerText.Replace(x.ToString(), string.Empty));
                        textValues.Add(GetCellValue(worksheet, sharedString, cell.CellReference));
                    }
                }
                else
                {
                    if (GetCellValue(worksheet, sharedString, listColumn[0] + x) == string.Empty)
                        break;
					foreach (var cell in row.Elements<Cell>())
					{
						try
						{
							textValues.Add(GetCellValue(worksheet, sharedString, cell.CellReference));
						}
						catch (Exception)
						{
							textValues.Add(string.Empty);
						}
					}
                    //textValues.Add(listColumn.Select(column => GetCellValue(worksheet, sharedString, column + x)));
                }
				//Check to verify the row contained data..
				if (textValues.Any())
				{
					textValuesArr = textValues.ToArray();
					if (isFirstRow)
					{
						for (int i = 1; i < textValuesArr.Count(); i++)
						{
							tabList.Add(textValuesArr[i].Split(new char[] { '|' })[1]);
						}
					}
					else
					{
						for (int i = 1; i < textValuesArr.Count(); i++)
						{
							if (String.IsNullOrEmpty(textValuesArr[i]))
								continue;
							ealoRow = Data.NewRow();
							ealoRow["Qualifier"] = "Tab";
							ealoRow["StringName"] = tabList[i - 1];
							ealoRow["StringText"] = textValuesArr[i];
							ealoRow["Locale"] = textValuesArr[0];
							Data.Rows.Add(ealoRow);
						}
					}
					isFirstRow = false;
				}
				else
				{
					//If no cells, then you have reached the end of the table.
					break;
				}
	            x++;
	        }
		}

		/// <summary>
		/// Does the import.
		/// </summary>
		/// Author: ducuytran
		/// 11/5/2012 - 10:38 AM
		private void DoImport()
		{
			if (Data.Rows.Count <= 0) return;

			var dataProvider = DataProvider.Instance();

			//Remove old data
			string queryRemove =
				String.Format(@"DELETE FROM Ealo
WHERE Id IN (
select Id
from Ealo a 
	join Tabs b on b.TabID=convert(int, a.StringName)
where PortalID={0} and IsDeleted = 0)", _portalId);
			dataProvider.ExecuteSQL(queryRemove);

			SqlConnection conn = new SqlConnection(dataProvider.ConnectionString);
			conn.Open();
			SqlCommand sqlCommand =
				new SqlCommand(
					"INSERT INTO Ealo (Qualifier, StringName, StringText, Locale) VALUES (@qual, @strname, @strtext, @locale)", conn);
			sqlCommand.CommandType = CommandType.Text;

			foreach (DataRow row in Data.Rows)
			{
				sqlCommand.Parameters.Clear();
				sqlCommand.Parameters.AddWithValue("@qual", row["Qualifier"]);
				sqlCommand.Parameters.AddWithValue("@strname", row["StringName"]);
				sqlCommand.Parameters.AddWithValue("@strtext", row["StringText"]);
				sqlCommand.Parameters.AddWithValue("@locale", row["Locale"]);
				//Save to Ealo TB
				try
				{
					sqlCommand.ExecuteNonQuery();
				}
				catch (Exception exc)//Skip the failed row
				{
					continue;
					throw;
				}
				
			}
		}

		#endregion

		/// <summary>
		/// Gets the data.
		/// </summary>
		/// Author: lam.tu
		public override void GetData()
		{
			int count = (int)DbHelper.ExecuteScalar("select COUNT(*) from INFORMATION_SCHEMA.TABLES a where a.TABLE_NAME='Ealo' and a.TABLE_TYPE='BASE TABLE'");

			// return empty if there is no ealo table
			if (count == 0) return;

			string columnsToPivot = "";
			try
			{
				columnsToPivot = (string)DbHelper.ExecuteScalar(string.Format(@"
declare @s varchar(1000) 
select @s = coalesce(@s + ',[' + t.ColumnName+']','['+ t.ColumnName+']')
from (
select distinct b.TabName + '|' + convert(varchar(5), b.TabID)  as [ColumnName]
from Ealo a 
	join Tabs b on b.TabID=convert(int, a.StringName)
where PortalID={0} and IsDeleted = 0
) as t
select @s", _portalId));
			}
			catch (Exception)
			{
				return;
			}
			

			if (string.IsNullOrEmpty(columnsToPivot)) return;

			string pivotQuery = @"
select * from
(
select convert(nvarchar(max), a.StringText) as StringText, a.Locale as [Language], b.TabName + '|' + convert(varchar(5), b.TabID)  as [ColumnName]
from Ealo a 
	join Tabs b on b.TabID=convert(int, a.StringName)
where PortalID={0} and IsDeleted = 0 and a.Qualifier = 'Tab'
) as src

pivot
(
 max(src.stringtext) for src.columnname in ({1})
) as p";

			var dataProvider = DataProvider.Instance();
			DataRow row;

			using (var dataReader = dataProvider.ExecuteSQL(string.Format(pivotQuery, _portalId, columnsToPivot)))
			{
				DataTable tableHeader = dataReader.GetSchemaTable();
				int columnCount = tableHeader.Rows.Count;

				row = Data.NewRow();
				for (int i = 0; i < columnCount; i++)
				{
					Data.Columns.Add(tableHeader.Rows[i].ItemArray[tableHeader.Columns["ColumnName"].Ordinal].ToString());
					row.SetField(i, tableHeader.Rows[i].ItemArray[tableHeader.Columns["ColumnName"].Ordinal].ToString());
				}
				Data.Rows.Add(row);

				// fill in available languages
				while (dataReader.Read())
				{
					row = Data.NewRow();
					for (int i = 0; i < columnCount; i++)
					{
						row.SetField(i, i == 0 ? dataReader[i].ToString().Trim() : dataReader[i]);
					}
					Data.Rows.Add(row);
				}
			}

			// fill in missing languages
			using (var dataReader = DataProvider.Instance().ExecuteSQL(string.Format(@"
select l.CultureCode
from Languages l
	join PortalLanguages pl on pl.LanguageID=l.LanguageID
where pl.PortalID = {0}	", _portalId)))
			{
				while (dataReader.Read())
				{
					if (Found(dataReader.GetString(0))) continue;

					row = Data.NewRow();
					row.SetField(0, dataReader[0]);
					Data.Rows.Add(row);
				}
			}
		}
	}
}