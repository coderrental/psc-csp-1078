﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DocumentFormat.OpenXml.Spreadsheet;
using DotNetNuke.Data;
using TIEKinetix.CspModules.DnnTranslator.Common;

namespace TIEKinetix.CspModules.DnnTranslator.CustomSheets
{
	public class CspHtmlModule : CustomSheet
	{
		private int _portalId;
		private int _tabModuleId;

		#region Overrides of CustomSheet

		/// <summary>
		/// 	Exports content the specified portal id to a data table
		/// </summary>
		/// <param name = "portalId">The portal id.</param>
		/// <param name = "tabModuleId">The tab module id.</param>
		/// <returns></returns>
		/// Author: ltu
		/// 10/31/2012 - 12:06 AM
		public override ICustomSheet Export(int portalId, int tabModuleId)
		{
			_portalId = portalId;
			_tabModuleId = tabModuleId;
			GetData();
			return this;
		}

		public override bool Import(Worksheet worksheet, SharedStringTable sharedString, string sheetTitle, int portalId, int tabModuleId, int userId, string backupFolder)
		{
			_portalId = portalId;
			_tabModuleId = tabModuleId;
			UserId = userId;
			Sheetname = sheetTitle;
			BackupFolder = backupFolder;
			//Backup
			if (!String.IsNullOrEmpty(BackupFolder))
				DoBackup();
			//Create datatable columns
			InitDataColumn();
			//Get data from sheet then insert into datatable
			InitData(worksheet, sharedString);
			//Update data to DB
			DoImport();
			return true;
		}

		#endregion

        public override bool RollBack(DateTime time)
        {
            throw new NotImplementedException();
        }

		/// <summary>
		/// Inits the data column.
		/// </summary>
		/// Author: ducuytran
		/// 11/4/2012 - 7:47 PM
		public override void InitDataColumn()
		{
			Data = new DataTable();
			DataColumn cspHtmlCol;

			//PortalId col
			cspHtmlCol = new DataColumn();
			cspHtmlCol.DataType = System.Type.GetType("System.Int32");
			cspHtmlCol.ColumnName = "PortalId";
			Data.Columns.Add(cspHtmlCol);

			//TabModuleId col
			cspHtmlCol = new DataColumn();
			cspHtmlCol.DataType = System.Type.GetType("System.Int32");
			cspHtmlCol.ColumnName = "TabModuleId";
			Data.Columns.Add(cspHtmlCol);

			//Content col
			cspHtmlCol = new DataColumn();
			cspHtmlCol.DataType = System.Type.GetType("System.String");
			cspHtmlCol.ColumnName = "Content";
			Data.Columns.Add(cspHtmlCol);

			//CultureCode col
			cspHtmlCol = new DataColumn();
			cspHtmlCol.DataType = System.Type.GetType("System.String");
			cspHtmlCol.ColumnName = "CultureCode";
			Data.Columns.Add(cspHtmlCol);

			//CreatedByUserId col
			cspHtmlCol = new DataColumn();
			cspHtmlCol.DataType = System.Type.GetType("System.Int32");
			cspHtmlCol.ColumnName = "CreatedByUserId";
			Data.Columns.Add(cspHtmlCol);

			//Created col
			cspHtmlCol = new DataColumn();
			cspHtmlCol.DataType = System.Type.GetType("System.DateTime");
			cspHtmlCol.ColumnName = "Created";
			Data.Columns.Add(cspHtmlCol);

			//Changed col
			cspHtmlCol = new DataColumn();
			cspHtmlCol.DataType = System.Type.GetType("System.DateTime");
			cspHtmlCol.ColumnName = "Changed";
			Data.Columns.Add(cspHtmlCol);
		}

		public override void GetData()
		{
			var dataProvider = DataProvider.Instance();
			DataRow row;

			int count = (int)DbHelper.ExecuteScalar("select COUNT(*) from INFORMATION_SCHEMA.TABLES a where a.TABLE_NAME='CSPHtmlModule_Content' and a.TABLE_TYPE='BASE TABLE'");

			// return empty if table doesnt exist.
			if (count == 0) return;
			string columnsToPivot = "";
			try
			{
				columnsToPivot = (string)DbHelper.ExecuteScalar(string.Format(@"
declare @s varchar(1000)
declare @temp table (id int primary key)
insert into @temp(id)
select distinct TabModuleId
from CSPHtmlModule_Content where PortalId={0}
select @s = coalesce(@s+',['+cast(a.id as varchar(5))+']', '['+cast(a.id as varchar(5))+']') 
from  @temp a
delete from @temp
select @s
", _portalId));
			}
			catch(Exception)
			{
				return;
			}

			// return empty if there isnt any column to pivot
			if (string.IsNullOrEmpty(columnsToPivot)) return;

			string pivotQuery = @"
select * 
from (
select a.CultureCode as [Language],a.Content,a.TabModuleId
from CSPHtmlModule_Content a
where a.PortalId={0}) as src
pivot(
	max(Content) for TabModuleId in ({1})
) as pivottable";

			//string tmp = string.Format(pivotQuery, _portalId, columnsToPivot);
			using (var dataReader = dataProvider.ExecuteSQL(string.Format(pivotQuery, _portalId, columnsToPivot)))
			{
				DataTable tableHeader = dataReader.GetSchemaTable();
				int columnCount = tableHeader.Rows.Count;

				row = Data.NewRow();
				for (int i = 0; i < columnCount; i++)
				{
					Data.Columns.Add(tableHeader.Rows[i].ItemArray[tableHeader.Columns["ColumnName"].Ordinal].ToString());
					row.SetField(i, tableHeader.Rows[i].ItemArray[tableHeader.Columns["ColumnName"].Ordinal].ToString());
				}
				Data.Rows.Add(row);

				// fill in available languages
				while (dataReader.Read())
				{
					row = Data.NewRow();
					for (int i = 0; i < columnCount; i++)
					{
						row.SetField(i, dataReader[i]);
					}
					Data.Rows.Add(row);
				}
			}

			// fill in missing languages
			using (var dataReader = DataProvider.Instance().ExecuteSQL(string.Format(@"
select l.CultureCode
from Languages l
	join PortalLanguages pl on pl.LanguageID=l.LanguageID
where pl.PortalID = {0}	", _portalId)))
			{
				while (dataReader.Read())
				{
					if (Found(dataReader.GetString(0))) continue;

					row = Data.NewRow();
					row.SetField(0, dataReader[0]);
					Data.Rows.Add(row);
				}
			}
		}

		/// <summary>
		/// Inits the data.
		/// </summary>
		/// <param name="worksheet">The worksheet.</param>
		/// <param name="sharedString">The shared string.</param>
		/// Author: ducuytran
		/// 11/4/2012 - 7:43 PM
		private void InitData(Worksheet worksheet, SharedStringTable sharedString)
		{
			bool isFirstRow = true;

			String[] textValuesArr;
			List<String> tabList = new List<string>();
			DataRow cspHtmlRow;
            var listColumn = new List<string>();
		    var x = 1;
			foreach (var row in worksheet.Descendants<Row>())
			{
                var textValues = new List<string>();
                if (x == 1) //First row
                {
                    foreach (var cell in row.Elements<Cell>())
                    {
                        listColumn.Add(cell.CellReference.InnerText.Replace(x.ToString(), string.Empty));
                        textValues.Add(GetCellValue(worksheet, sharedString, cell.CellReference));
                    }
                }
                else
                {
                    if (GetCellValue(worksheet, sharedString, listColumn[0] + x) == string.Empty)
                        break;
                    //textValues.AddRange(listColumn.Select(column => GetCellValue(worksheet, sharedString, column + x)));
					foreach (var cell in row.Elements<Cell>())
					{
						try
						{
							textValues.Add(GetCellValue(worksheet, sharedString, cell.CellReference));
						}
						catch (Exception)
						{
							textValues.Add(string.Empty);
						}
					}
                }
				//Check to verify the row contained data.
				if (textValues.Any())
				{
					textValuesArr = textValues.ToArray();
					if (isFirstRow)
					{
						for (int i = 1; i < textValuesArr.Count(); i++)
						{
							tabList.Add(textValuesArr[i]);
						}
					}
					else
					{
						for (int i = 1; i < textValuesArr.Count(); i++)
						{
							if (String.IsNullOrEmpty(textValuesArr[i]))
								continue;
							cspHtmlRow = Data.NewRow();
							cspHtmlRow["PortalId"] = _portalId;
							cspHtmlRow["TabModuleId"] = tabList[i - 1];
							cspHtmlRow["Content"] = textValuesArr[i];
							cspHtmlRow["CultureCode"] = textValuesArr[0];
							cspHtmlRow["CreatedByUserId"] = UserId;
							cspHtmlRow["Created"] = DateTime.Now.ToString();
							cspHtmlRow["Changed"] = DateTime.Now.ToString();
							Data.Rows.Add(cspHtmlRow);
						}
					}
					isFirstRow = false;
				}
				else
				{
					//If no cells, then you have reached the end of the table.
					break;
				}
			    x++;
			}
		}

		/// <summary>
		/// Does the import.
		/// </summary>
		/// Author: ducuytran
		/// 11/5/2012 - 4:34 PM
		private void DoImport()
		{
			if (Data.Rows.Count <= 0) return;

			var dataProvider = DataProvider.Instance();

			//Remove current data (overwrite)
			string queryRemove =
				String.Format(@"DELETE FROM CSPHtmlModule_Content
WHERE PortalID={0}", _portalId);
			dataProvider.ExecuteSQL(queryRemove);

			//Insert datas which got from excel file -> datatable
			SqlConnection conn = new SqlConnection(dataProvider.ConnectionString);
			conn.Open();
			SqlCommand sqlCommand =
				new SqlCommand(
					"INSERT INTO CSPHtmlModule_Content (PortalId, TabModuleId, Content, CultureCode, CreatedByUserId, Created, Changed) VALUES (@pid, @tmid, @content, @ccode, @uid, @cdate, @chdate)",conn);
			sqlCommand.CommandType = CommandType.Text;

			foreach (DataRow row in Data.Rows)
			{
				sqlCommand.Parameters.Clear();
				sqlCommand.Parameters.AddWithValue("@pid", row["PortalId"]);
				sqlCommand.Parameters.AddWithValue("@tmid", row["TabModuleId"]);
				sqlCommand.Parameters.AddWithValue("@content", row["Content"]);
				sqlCommand.Parameters.AddWithValue("@ccode", row["CultureCode"]);
				sqlCommand.Parameters.AddWithValue("@uid", row["CreatedByUserId"]);
				sqlCommand.Parameters.AddWithValue("@cdate", row["Created"]);
				sqlCommand.Parameters.AddWithValue("@chdate", row["Changed"]);
				sqlCommand.ExecuteNonQuery();
			}
		}
	}
}