﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;


namespace TIEKinetix.CspModules.DnnTranslator.CustomSheets
{
	public interface ICustomSheet
	{
		ICustomSheet Export(int portalId, int tabModuleId);
		bool Import(Worksheet worksheet, SharedStringTable sharedString, string sheetTitle, int portalId, int tabModuleId, int userId, string backupFolder);
	    bool RollBack(DateTime time);
	}

	public abstract class CustomSheet : ICustomSheet
	{
		/// <summary>
		/// Gets or sets the sheetname.
		/// </summary>
		/// <value>
		/// The sheetname.
		/// </value>
		/// Author: ltu
		/// 10/30/2012 - 9:17 PM
		public string Sheetname { get; set; }

		/// <summary>
		/// Gets or sets the data.
		/// </summary>
		/// <value>
		/// The data.
		/// </value>
		/// Author: ltu
		/// 10/30/2012 - 9:17 PM
		public DataTable Data { get; set; }

		/// <summary>
		/// The backup folder
		/// </summary>
		/// Author: ducuytran
		/// 11/4/2012 - 7:33 PM
		public string BackupFolder { get; set; }

		/// <summary>
		/// Gets or sets the user id.
		/// </summary>
		/// <value>
		/// The user id.
		/// </value>
		/// Author: ducuytran
		/// 11/5/2012 - 11:25 AM
		public int UserId { get; set; }

		#region Implementation of ICustomSheet

		protected virtual bool Found(string cultureCode)
		{
			return Data.Rows.Cast<DataRow>().Any(row => Equals(row.ItemArray[0], cultureCode));
		}
		public abstract ICustomSheet Export(int portalId, int tabModuleId);
		public abstract bool Import(Worksheet worksheet, SharedStringTable sharedString, string sheetTitle, int portalId, int tabModuleId, int userId, string backupFolder);
        public abstract bool RollBack(DateTime time);
		public abstract void InitDataColumn();
		public abstract void GetData();

		#endregion

        /// <summary>
        /// Gets the cell value.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="sharedString">The shared string.</param>
        /// <param name="reference">The reference.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 11/6/2012 - 5:00 PM
        public string GetCellValue(Worksheet worksheet, SharedStringTable sharedString, string reference)
        {
            Cell cell = worksheet.Descendants<Cell>().FirstOrDefault(c => reference.Equals(c.CellReference));
            if (cell == null)
                return string.Empty;
            if (cell.DataType!=null && cell.DataType == CellValues.SharedString)
                return sharedString.ChildElements[int.Parse(cell.CellValue.InnerText)].InnerText;
            return (cell.CellValue == null) ? cell.InnerText : cell.CellValue.InnerText;
        }

		/// <summary>
		/// Do the backup.
		/// </summary>
		/// Author: ducuytran
		/// 11/3/2012 - 4:18 PM
		protected void DoBackup()
		{
			Data = new DataTable();
			try
			{
				GetData();
			}
			catch (Exception)
			{
			}
			
			if (Data.Rows.Count <= 0)
				return;
			string excelFileName = Sheetname.Replace("@DNN@", "").Replace(" ", "");
			SharedStringTablePart sharedStringTablePart;
			using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Create(BackupFolder + "\\" + excelFileName + ".xlsx", SpreadsheetDocumentType.Workbook))
			{
				// Add a WorkbookPart to the document.
				WorkbookPart newWorkbookpart = spreadSheet.AddWorkbookPart();
				newWorkbookpart.Workbook = new Workbook();

				//spreadSheet.WorkbookPart.Workbook.Save();
				
				// Shared string table
				sharedStringTablePart = spreadSheet.WorkbookPart.AddNewPart<SharedStringTablePart>();
				sharedStringTablePart.SharedStringTable = new SharedStringTable();
				sharedStringTablePart.SharedStringTable.Save();

				// Add a WorksheetPart to the WorkbookPart.
				WorksheetPart newWorksheetPart = newWorkbookpart.AddNewPart<WorksheetPart>();
				newWorksheetPart.Worksheet = new Worksheet(new SheetData());

				// Add Sheets to the Workbook.
				Sheets newSheets = spreadSheet.WorkbookPart.Workbook.AppendChild<Sheets>(new Sheets());
				//workbookpart.Workbook.Save();

				// Append a new worksheet and associate it with the workbook.
				Sheet newSheet = new Sheet() { Id = spreadSheet.WorkbookPart.GetIdOfPart(newWorksheetPart), SheetId = 1, Name = Sheetname };
				newSheets.Append(newSheet);

				// Get the sheetData cell table.
				SheetData sheetData = newWorksheetPart.Worksheet.GetFirstChild<SheetData>();

				List<String> columns = new List<string>();
				foreach (DataColumn column in Data.Columns)
				{
					columns.Add(column.ColumnName);
				}

				int rIndex = 1;
				
				foreach (DataRow dsrow in Data.Rows)
				{
					Row newRow = new Row();
					int cIndex = 1;
					foreach (String col in columns)
					{
						Cell cell = createTextCell(cIndex, rIndex, dsrow[col]);
						newRow.AppendChild(cell);
						cIndex++;
					}
					
					sheetData.AppendChild(newRow);
					rIndex++;
				}

				// Close the document.
				spreadSheet.Close();
				spreadSheet.Dispose();
			}
		}

		/// <summary>
		/// Creates the text cell.
		/// </summary>
		/// <param name="columnIndex">Index of the column.</param>
		/// <param name="rowIndex">Index of the row.</param>
		/// <param name="cellValue">The cell value.</param>
		/// <returns></returns>
		/// Author: ducuytran
		/// 11/7/2012 - 12:58 AM
		private Cell createTextCell(int columnIndex, int rowIndex, object cellValue)
		{
			Cell cell = new Cell();

			cell.DataType = new EnumValue<CellValues>(CellValues.InlineString);
			cell.CellReference = getColumnName(columnIndex) + rowIndex;

			InlineString inlineString = new InlineString();
			Text t = new Text();

			t.Text = String.IsNullOrEmpty(cellValue.ToString()) ? "" : cellValue.ToString();
			inlineString.AppendChild(t);
			cell.AppendChild(inlineString);
			return cell;
		}

		/// <summary>
		/// Gets the name of the column.
		/// </summary>
		/// <param name="columnIndex">Index of the column.</param>
		/// <returns></returns>
		/// Author: ducuytran
		/// 11/7/2012 - 12:58 AM
		private string getColumnName(int columnIndex)
		{
			int dividend = columnIndex;
			string columnName = String.Empty;
			int modifier;

			while (dividend > 0)
			{
				modifier = (dividend - 1) % 26;
				columnName =
					Convert.ToChar(65 + modifier).ToString() + columnName;
				dividend = (int)((dividend - modifier) / 26);
			}

			return columnName;
		}
	}

	public static class CustomSheetFactory
	{
		public static CustomSheet CreateSheet(string sheetTitle, int portalId, int tabModuleId)
		{
			//@DNN@CSP Html Module
			string tmp = sheetTitle.Replace("@DNN@", "").Replace(" ", ""); // CspHtmlModule
			Type type = Type.GetType("TIEKinetix.CspModules.DnnTranslator.CustomSheets." + tmp);
			if (type == null)
				return null;
			CustomSheet sheet = Activator.CreateInstance(type) as CustomSheet;
			if (sheet != null)
			{
				sheet.Data = new DataTable(); // we need this because of Null Exception
				sheet.Sheetname = sheetTitle;
				return (CustomSheet) sheet.Export(portalId, tabModuleId);
			}
			throw new Exception("Couldn't create custom sheet");
		}

		public static bool ImportSheet(Worksheet worksheet, SharedStringTable sharedString, string sheetTitle, int portalId, int tabModuleId, int userId, string backupFolder)
		{
			string tmp = sheetTitle.Replace("@DNN@", "").Replace(" ", "");
			Type type = Type.GetType("TIEKinetix.CspModules.DnnTranslator.CustomSheets." + tmp);
			if (type == null)
				return false;
			CustomSheet sheet = Activator.CreateInstance(type) as CustomSheet;
			if (sheet != null)
			{
				sheet.Import(worksheet, sharedString, sheetTitle, portalId, tabModuleId, userId, backupFolder);
			}
			return true;
		}
	}
}