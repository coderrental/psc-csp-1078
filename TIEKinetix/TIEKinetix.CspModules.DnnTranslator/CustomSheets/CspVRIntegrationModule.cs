﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DocumentFormat.OpenXml.Spreadsheet;
using DotNetNuke.Data;
using DotNetNuke.Entities.Portals;
using DotNetNuke.Services.Log.EventLog;

namespace TIEKinetix.CspModules.DnnTranslator.CustomSheets
{
	class CspVRIntegrationTranslationRecord
	{
		private string _fieldId, _cultureCode, _shortDesc, _longDesc;

		public CspVRIntegrationTranslationRecord(string fieldId, string cultureCode, string shortDesc, string longDesc)
		{
			_fieldId = fieldId;
			_cultureCode = cultureCode;
			_shortDesc = shortDesc;
			_longDesc = longDesc;
		}

		public CspVRIntegrationTranslationRecord():this("","","","")
		{
			
		}

		public string FieldId
		{
			get { return _fieldId; }
			set { _fieldId = value; }
		}

		public string CultureCode
		{
			get { return _cultureCode; }
			set { _cultureCode = value; }
		}

		public string ShortDesc
		{
			get { return _shortDesc; }
			set { _shortDesc = value; }
		}

		public string LongDesc
		{
			get { return _longDesc; }
			set { _longDesc = value; }
		}
	}
	/// <summary>
	/// note: need to implement rollback
	/// </summary>
	public class CspVRIntegrationModule : CustomSheet
	{
		private string _vrFormIdSettingName = "VR_FORM_ID";
		private string _moduleFriendlyName = "CspVRIntegrationModule";
		private string _query = @"
select distinct
	f.FieldId,
	case when t.TranslationId is null then 'en-US' else t.CultureCode end as 'CultureCode',
	case when t.TranslationId is null or t.ShortDesc = '' then f.Name else t.ShortDesc end as 'ShortDesc',
	case when t.TranslationId is null or t.LongDesc = '' then f.Name else t.LongDesc end as 'LongDesc'
from DesktopModules a
	join ModuleDefinitions b on a.DesktopModuleID=b.DesktopModuleID
	join Modules c on c.ModuleDefID=b.ModuleDefID	
	join TabModules d on d.ModuleID = c.ModuleID
	join TabModuleSettings e on e.TabModuleID=d.TabModuleID
	join CSPSubscriptionModule_Field f on f.FormId=e.SettingValue
	left join CSPSubscriptionModule_Translation t on t.Id=f.FieldId
where c.PortalID={0} and c.IsDeleted = 0 and a.FriendlyName = N'{1}' and e.SettingName=N'{2}'
order by CultureCode
";
		private string _getFormTranslationQuery = @"
select distinct
	e.SettingValue,
	case when t.TranslationId is null then 'en-US' else t.CultureCode end as 'CultureCode',
	case when t.TranslationId is null or t.ShortDesc = '' then f.Name else t.ShortDesc end as 'ShortDesc',
	case when t.TranslationId is null or t.LongDesc = '' then f.Name else t.LongDesc end as 'LongDesc'
from DesktopModules a
	join ModuleDefinitions b on a.DesktopModuleID=b.DesktopModuleID
	join Modules c on c.ModuleDefID=b.ModuleDefID	
	join TabModules d on d.ModuleID = c.ModuleID
	join TabModuleSettings e on e.TabModuleID=d.TabModuleID
	join CSPSubscriptionModule_Form f on f.FormId=e.SettingValue
	left join CSPSubscriptionModule_Translation t on t.Id=f.FormId
where c.PortalID={0} and c.IsDeleted = 0 and a.FriendlyName = N'{1}' and e.SettingName=N'{2}'
";
		#region Overrides of CustomSheet

		public override ICustomSheet Export(int portalId, int tabModuleId)
		{
			var dataProvider = DataProvider.Instance();
			List<CspVRIntegrationTranslationRecord> data = new List<CspVRIntegrationTranslationRecord>();
			// fields
			using (var dataReader = dataProvider.ExecuteSQL(string.Format(_query, portalId, _moduleFriendlyName, _vrFormIdSettingName)))
			{
				while(dataReader.Read())
				{
					data.Add(new CspVRIntegrationTranslationRecord
					         	{
					         		FieldId = dataReader[0].ToString(),
					         		CultureCode = dataReader.GetString(1),
					         		ShortDesc = dataReader.GetString(2),
					         		LongDesc = dataReader.GetString(3)
					         	});
				}
			}
			//form
			using (var dataReader = dataProvider.ExecuteSQL(string.Format(_getFormTranslationQuery, portalId, _moduleFriendlyName, _vrFormIdSettingName)))
			{
				while (dataReader.Read())
				{
					data.Add(new CspVRIntegrationTranslationRecord
					{
						FieldId = dataReader[0].ToString(),
						CultureCode = dataReader.GetString(1),
						ShortDesc = dataReader.GetString(2),
						LongDesc = dataReader.GetString(3)
					});
				}
			}


			if (data.Count > 0)
			{
				DataRow dr = Data.NewRow();
				Data.Columns.Add("Language", typeof (string));
				dr.SetField(0, "Language");
				int i = 1;
				foreach (var record in data.Select(a=>a.FieldId).Distinct())
				{
					Data.Columns.Add(string.Format("{0}|ShortDesc",record), typeof (string));
					dr.SetField(i, string.Format("{0}|ShortDesc", record));
					i++;
					Data.Columns.Add(string.Format("{0}|LongDesc", record), typeof(string));
					dr.SetField(i, string.Format("{0}|LongDesc", record));
					i++;
				}
				Data.Rows.Add(dr);

				foreach (var lngCode in data.Select(a=>a.CultureCode).Distinct())
				{
					dr = Data.NewRow();
					foreach (DataColumn dc in Data.Columns)
					{
						if (dc.Ordinal == 0)
						{
							dr.SetField(0, lngCode);
						}
						else
						{
							var r = data.First(a => a.CultureCode == lngCode && a.FieldId == dc.ColumnName.Split('|')[0]);
							switch (dc.ColumnName.Split('|')[1])
							{
								case "ShortDesc":
									dr.SetField(dc, r.ShortDesc);
									break;
								case "LongDesc":
									dr.SetField(dc, r.LongDesc);
									break;
							}
						}
					}
					Data.Rows.Add(dr);
				}
			}

			return this;
		}

		public override bool Import(Worksheet worksheet, SharedStringTable sharedString, string sheetTitle, int portalId, int tabModuleId, int userId, string backupFolder)
		{
			var columnNames = new List<string>();
			List<CspVRIntegrationTranslationRecord> records = new List<CspVRIntegrationTranslationRecord>();
			int i = 1;
			foreach (var row in worksheet.Descendants<Row>())
			{
				var values = new List<string>();
				if (i == 1)
				{
					columnNames.AddRange(row.Elements<Cell>().Select(cell => GetCellValue(worksheet, sharedString, cell.CellReference)));
				}
				else
				{
					values.AddRange(row.Elements<Cell>().Select(cell => GetCellValue(worksheet, sharedString, cell.CellReference)));
					for (int j = 1; j < columnNames.Count; j++)
					{
						if (values.Count <= j)
							break;

						var fields = columnNames[j].Split('|');
						CspVRIntegrationTranslationRecord r;
						if (records.Count(a => a.FieldId == fields[0] && a.CultureCode == values[0]) == 0)
						{
							r = new CspVRIntegrationTranslationRecord
							    	{
							    		CultureCode = values[0],
							    		FieldId = fields[0]
							    	};
							records.Add(r);
						}
						else
						{
							r = records.Find(a => a.FieldId == fields[0] && a.CultureCode == values[0]);
						}

						if (fields[1] == "ShortDesc")
							r.ShortDesc = values[j];
						else
							r.LongDesc = values[j];

					}
				}
				i++;
			}

			if (records.Count > 0)
			{
				using (var conn = new SqlConnection(DataProvider.Instance().ConnectionString))
				{
					conn.Open();
					string query = @"
if not exists (select 1 from CSPSubscriptionModule_Translation a where a.Id=@id and a.CultureCode=@cultureCode)
begin
	insert into CSPSubscriptionModule_Translation(Id,CultureCode,ShortDesc,LongDesc) values (@id,@cultureCode,@shortDesc,@longDesc)
end
else
begin
	update CSPSubscriptionModule_Translation
	set ShortDesc=@shortDesc, LongDesc=@longDesc
	where Id=@id and CultureCode=@cultureCode
end
";
					using (var cmd = new SqlCommand(query, conn))
					{
						foreach (var record in records)
						{
							cmd.Parameters.Clear();
							cmd.Parameters.AddWithValue("@id", record.FieldId);
							cmd.Parameters.AddWithValue("@cultureCode", record.CultureCode);
							cmd.Parameters.AddWithValue("@shortDesc", record.ShortDesc);
							cmd.Parameters.AddWithValue("@longDesc", record.LongDesc);
							try
							{
								cmd.ExecuteNonQuery();
							}
							catch(Exception ex)
							{
								//ignore failed rows
								var log = new EventLogController();
								log.AddLog("Fail to update translation for "+_moduleFriendlyName, ex.Message+ex.StackTrace,PortalSettings.Current,-1,EventLogController.EventLogType.ADMIN_ALERT);
							}
						}
					}
				}
			}

			return true;
		}


		public override bool RollBack(DateTime time)
		{
			return true;
		}

		public override void InitDataColumn()
		{
			Data = new DataTable();
		}

		public override void GetData()
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}