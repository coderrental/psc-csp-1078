﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using TIEKinetix.CspModules.DnnTranslator.Common;
using TIEKinetix.CspModules.DnnTranslator.Entities;
using TIEKinetix.CspModules.DnnTranslator.CustomSheets;
using Telerik.Web.UI;

namespace TIEKinetix.CspModules.DnnTranslator
{
    public partial class RollBack : DnnModuleBase
    {
    	private List<BackupCheckpoint> _bkFolderList;
    	private string _backupFolder;

		/// <summary>
		/// Handles the Init event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
		/// Author: ducuytran
		/// 11/5/2012 - 4:58 PM
		protected override void Page_Init(object sender, EventArgs e)
		{
			base.Page_Init(sender, e);
            _bkFolderList = new List<BackupCheckpoint>();
            h2Title.InnerText = GetLocalizedText("Label.ModuleTitle");
			//Set backup root folder
			_backupFolder = Server.MapPath(PortalSettings.HomeDirectory + "\\" + Cons.DEFAULT_BACKUP_FOLDER_NAME);
			//Get backup folders list
			GetAllBackupList();
		}

    	protected void Page_Load(object sender, EventArgs e)
        {
			//Init RadGrid
    	    RollBackList.DataSource = _bkFolderList.OrderByDescending(a => a.DateTime);


			// Define the name and type of the client scripts on the page.
			String csname1 = "PopupScript";
			Type cstype = this.GetType();

			// Get a ClientScriptManager reference from the Page class.
			ClientScriptManager cs = Page.ClientScript;

			// Check to see if the startup script is already registered.
			/*if (!cs.IsStartupScriptRegistered(cstype, csname1))
			{
				StringBuilder cstext1 = new StringBuilder();
				cstext1.Append("<script type=text/javascript> alert('Hello World!') </");
				cstext1.Append("script>");

				cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
			}*/
        }

		/// <summary>
		/// Gets all backup list.
		/// </summary>
		/// Author: ducuytran
		/// 11/5/2012 - 5:02 PM
		private void GetAllBackupList()
		{
			//Create backup root folder if it not existed
			if (!Directory.Exists(_backupFolder))
			{
				Directory.CreateDirectory(_backupFolder);
			}

			foreach (var dir in new DirectoryInfo(_backupFolder).GetDirectories())
			{
			    var name = dir.Name;
			    var time = Convert.ToDateTime(name.Replace(".",":"));
				_bkFolderList.Add(new BackupCheckpoint
				                      {
				                          DateTime = time,
                                          FolderName =  name
				                      });
			}
		}

        /// <summary>
        /// Rollbacks the specified folder name.
        /// </summary>
        /// <param name="folderName">Name of the folder.</param>
        /// Author: Vu Dinh
        /// 11/5/2012 - 5:28 PM
        private void Rollback(string folderName)
        {
            //Rollback custom module
            RollbackCustomModule(folderName);
            //Rollback resx file
            RollbackResxFiles(folderName);
        }

		/// <summary>
		/// Rollbacks the custom module.
		/// </summary>
		/// <param name="foldername">The foldername.</param>
		/// Author: ducuytran
		/// 11/5/2012 - 10:25 PM
        private void RollbackCustomModule(string foldername)
        {
            List<string> customSheets = new List<string>();
			//Get all excel files in backup folder
			customSheets = GetCustomSheets(foldername);
			
			//My super power!!! Re-use import to rollback!
        	foreach (string customSheet in customSheets)
        	{
				SpreadsheetDocument document = SpreadsheetDocument.Open(customSheet, true);
				//References to the workbook and Shared String Table.
				var workBook = document.WorkbookPart.Workbook;
				var workSheets = workBook.Descendants<Sheet>().ToList();
        		var sharedStrings = document.WorkbookPart.SharedStringTablePart.SharedStringTable;
				foreach (var sheet in workSheets)
				{
					var worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id);
					if (sheet.Name.Value.Contains("@DNN@"))
					{
						CustomSheetFactory.ImportSheet(worksheetPart.Worksheet, sharedStrings, sheet.Name.Value, PortalId, TabModuleId,
													   UserId, String.Empty);
					}
				}
				document.Close();
        	}
        }

        /// <summary>
        /// Rollbacks the RESX file.
        /// </summary>
        /// <param name="foldername">The foldername.</param>
        /// Author: Vu Dinh
        /// 11/5/2012 - 5:29 PM
        private void RollbackResxFiles(string foldername)
        {
            var backupFolder = Server.MapPath(PortalSettings.HomeDirectory + "\\" + Cons.DEFAULT_BACKUP_FOLDER_NAME + "\\" + foldername);
            var moduleResxFolder = backupFolder + "\\DesktopModules";
            
            //Rollback resx files
            foreach (var dnnSpecialSheet in GetDnnSpecialSheets())
            {
                var bkFolder = backupFolder + "\\" + dnnSpecialSheet.Value;
                CopyFolderAndContents(bkFolder, Server.MapPath(dnnSpecialSheet.Value));
            }
            CopyFolderAndContents(moduleResxFolder, Server.MapPath("DesktopModules"));
        }

		/// <summary>
		/// Gets the custom sheets.
		/// </summary>
		/// <param name="folderName">Name of the folder.</param>
		/// <returns></returns>
		/// Author: ducuytran
		/// 11/5/2012 - 5:37 PM
		private List<string> GetCustomSheets(string folderName)
		{
			List<string> excelFiles = new List<string>();
			DirectoryInfo dirInfo = new DirectoryInfo(Server.MapPath(PortalSettings.HomeDirectory + "\\" + Cons.DEFAULT_BACKUP_FOLDER_NAME + "\\" + folderName));
			foreach (FileInfo file in dirInfo.GetFiles("*.xlsx"))
			{
				excelFiles.Add(file.FullName);
			}
			return excelFiles;
		}

        /// <summary>
        /// Handles the OnItemCommand event of the RollBackList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Telerik.Web.UI.GridCommandEventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 11/5/2012 - 5:40 PM
        protected void RollBackList_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "rollback")
            {
                var item = e.Item as GridDataItem;
                if (item != null)
                {
                    string folderName = item["folderName"].Text;
                    Rollback(folderName);
                }
            }
        }
    }
}