﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleSettings.ascx.cs" Inherits="TIEKinetix.CspModules.DnnTranslator.ModuleSettings" %>
<div>
    <table>
        <tbody>
            <tr>
                <td>Default excel filename - without extension (eg: Translatesheet)</td>
                <td><asp:TextBox runat="server" ID="tbxFilename"/></td>
            </tr>
        </tbody>
    </table>
</div>