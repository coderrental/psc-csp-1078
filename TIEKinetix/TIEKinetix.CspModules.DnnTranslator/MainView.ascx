﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainView.ascx.cs" Inherits="TIEKinetix.CspModules.DnnTranslator.MainView" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--
<telerik:RadAjaxLoadingPanel runat="server" IsSticky="True" ID="ajaxLoadingPanel" CssClass="ajax-loading-panel" Skin="Metro"/>
<telerik:RadWindowManager runat="server" ID="windowManager" EnableShadow="True"></telerik:RadWindowManager>
<telerik:RadAjaxManager runat="server" DefaultLoadingPanelID="ajaxLoadingPanel" ID="ajaxManager">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="pnExport">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnExport"/>
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>
 --%>
<asp:Panel runat="server" ID="pnExport">
    <div style="text-align: center">
        <h2 id="h2Title" runat="server"/>
        <hr />
        <p runat="server" id="errorMsg"></p>
    </div>
    <div style="text-align: center">
        <telerik:radbutton runat="server" ID="btnExport" AutoPostBack="True" OnClick="btnExport_OnClick">
            <Icon PrimaryIconCssClass="rbDownload" PrimaryIconLeft="4" PrimaryIconTop="4"/>
        </telerik:radbutton>
        <telerik:RadButton runat="server" ID="btnImport" AutoPostBack="True" OnClick="btnImport_OnClick">
            <Icon PrimaryIconCssClass="rbUpload" PrimaryIconTop="4" PrimaryIconLeft="4"/>
        </telerik:RadButton>
        <telerik:RadButton runat="server" ID="btnRollback" AutoPostBack="True" OnClick="btnRollback_OnClick">
            <Icon PrimaryIconCssClass="rbRefresh" PrimaryIconLeft="4" PrimaryIconTop="4"></Icon>
        </telerik:RadButton>
    </div>
</asp:Panel>
