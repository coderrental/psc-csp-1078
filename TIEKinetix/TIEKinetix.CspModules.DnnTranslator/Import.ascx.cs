﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using TIEKinetix.CspModules.DnnTranslator.Common;
using TIEKinetix.CspModules.DnnTranslator.CustomSheets;
using TIEKinetix.CspModules.DnnTranslator.Entities;
using DocumentFormat.OpenXml.Extensions;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DotNetNuke.Common;
using DotNetNuke.Data;

namespace TIEKinetix.CspModules.DnnTranslator
{
    public partial class Import : DnnModuleBase
    {
        private MemoryStream _excelFile;
        private List<SheetEntity> _sheetEntities;
        private string _backupFolder;
        private Hashtable _moduleFolderName;
        /// <summary>
        /// Handles the Init event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 11/2/2012 - 10:32 AM
        protected override void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
            _sheetEntities = new List<SheetEntity>();
            _moduleFolderName = new Hashtable();
            GetModuleFolderName();
            LocalizeControls();
        }

        /// <summary>
        /// Localizes the controls.
        /// </summary>
        /// Author: Vu Dinh
        /// 11/2/2012 - 10:37 AM
        private void LocalizeControls()
        {
            h2Title.InnerText = GetLocalizedText("Label.ModuleTitle");
            btnSubmit.Text = GetLocalizedText("Label.Submit");
        }

        /// <summary>
        /// Reads the excel file.
        /// </summary>
        /// Author: Vu Dinh
        /// 11/2/2012 - 2:56 PM
        private void ReadExcelFile()
        {
            try
            {
                SpreadsheetDocument document = SpreadsheetDocument.Open(_excelFile, true);

                //References to the workbook and Shared String Table.
                var workBook = document.WorkbookPart.Workbook;
                var workSheets = workBook.Descendants<Sheet>().ToList();
                var sharedStrings = document.WorkbookPart.SharedStringTablePart.SharedStringTable;
                if (workSheets.Any())
                {
                    CreateBackupFolder();
                }
                foreach (var sheet in workSheets)
                {
                    var worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id);

                    if (sheet.Name.Value.Contains("@DNN@"))
                    {
                        var entries = GetDnnSpecialSheets();
                        if (entries.ContainsKey(sheet.Name.Value))
                        {
                            _sheetEntities.Add(LoadModuleSheet(sheet.Name.Value, worksheetPart.Worksheet, sharedStrings));
                        }
                        else
                        {
                            LoadCustomSheet(sheet.Name.Value, worksheetPart.Worksheet, sharedStrings);
                        }
                            
                    }
                    else
                    {
                        _sheetEntities.Add(LoadModuleSheet(sheet.Name.Value, worksheetPart.Worksheet, sharedStrings));
                    }

                }
            }
            catch (Exception exception)
            {
                errorMsg.InnerHtml = exception.Message + "<br />" + exception.StackTrace;
            }
            finally
            {
                //Release memory stream
                _excelFile.Dispose();
            }
        }

        

        /// <summary>
        /// Loads the module sheet.
        /// </summary>
        /// <param name="sheetname">The sheetname.</param>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="sharedString">The shared string.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 11/2/2012 - 4:27 PM
        private SheetEntity LoadModuleSheet(string sheetname, Worksheet worksheet, SharedStringTable sharedString)
        {
            var sheetEntity = new SheetEntity();
            sheetEntity.SheetName = sheetname;
            var listColumn = new List<string>();
            var translateItems = new List<TranslateItem>();
            int x = 1;
            foreach (var row in worksheet.Descendants<Row>())
            {
                var textValues = new List<string>();
                if (x == 1) //First row
                {
                    foreach (var cell in row.Elements<Cell>().Where(c => c.DataType !=null && c.DataType.HasValue && c.CellValue != null))
                    {
                        listColumn.Add(cell.CellReference.InnerText.Replace(x.ToString(), string.Empty));
                        textValues.Add(GetCellValue(worksheet, sharedString, cell.CellReference));
                    }
                }
                else
                {
                    if (GetCellValue(worksheet, sharedString, listColumn[0] + x) == string.Empty)
                        break;
                    textValues.AddRange(listColumn.Select(column => GetCellValue(worksheet, sharedString, column +x)));
                }
                if (textValues.Any())
                {
                    for (int j = 1; j < textValues.Count; j++) //skip first cell
                    {
                        var cellValue = textValues[j];
                        if (x == 1) //First row
                        {
                            translateItems.Add(new TranslateItem
                            {
                                Label = cellValue
                            });
                        }
                        else
                        {
                            translateItems[j - 1].LangValuePair.Add(textValues[0], cellValue);
                        }
                    }

                }
                else
                {
                    //If no cells, then you have reached the end of the table.
                    break;
                }
               x++;
            }
            
            sheetEntity.TranslateItems = translateItems;
            return sheetEntity;
        }

        /// <summary>
        /// Loads the custom sheet.
        /// </summary>
        /// Author: Vu Dinh
        /// 11/2/2012 - 3:14 PM
        private void LoadCustomSheet(string sheetname, Worksheet worksheet, SharedStringTable sharedString)
        {
        	if (!CustomSheetFactory.ImportSheet(worksheet, sharedString, sheetname, PortalId, TabModuleId, UserId, GetBackupFolder()))
        	{
        		//Todo: read custom sheet form xlsx file
        	}
        }

        /// <summary>
        /// Backups the resxfiles.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 11/3/2012 - 2:52 PM
        private bool BackupResxfiles()
        {
            try
            {
                var foldersName  = _sheetEntities.Where(a => !a.SheetName.Contains("@DNN@")).Select(sheetEntity => GetModuleFolderName(sheetEntity.SheetName)).ToList();

                var backupFolder = GetBackupFolder();
                
                //backup Desktop Modules resx files
                foreach (var moduleFolder in foldersName)
                {
                    var folder = Server.MapPath("DesktopModules\\" + moduleFolder + "\\App_LocalResources");
                    if (Directory.Exists(folder))
                    {
                        var destFolder = backupFolder + "\\DesktopModules\\" + moduleFolder + "\\App_LocalResources";
                        CopyFolderAndContents(folder, destFolder);
                    }
                }

                //Backup Dnn resx files
                foreach (var dnnSpecialSheet in GetDnnSpecialSheets())
                {
                    var orgFolder = Server.MapPath(dnnSpecialSheet.Value);
                    var destFolder = backupFolder + "\\" + dnnSpecialSheet.Value;
                    CopyFolderAndContents(orgFolder, destFolder);
                }
            }
            catch (Exception exception)
            {
                errorMsg.InnerHtml = "<p>" + exception.Message + "</p>";
                return false;
            }
            return true;
        }

        /// <summary>
        /// Gets the name of the module folder.
        /// </summary>
        /// <param name="moduleName">Name of the module.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 11/5/2012 - 9:59 AM
        private string GetModuleFolderName(string moduleName)
        {
            if (_moduleFolderName.ContainsKey(moduleName))
                return _moduleFolderName[moduleName].ToString();
            return string.Empty;
        }

        /// <summary>
        /// Gets the name of the module folder.
        /// </summary>
        /// Author: Vu Dinh
        /// 11/5/2012 - 5:14 PM
        private void GetModuleFolderName ()
        {
            var query = "SELECT * FROM DesktopModules";
            var dataProvider = DataProvider.Instance();
            var dataReader = dataProvider.ExecuteSQL(query);
            while (dataReader.Read())
            {
                _moduleFolderName[dataReader["FriendlyName"].ToString()] = dataReader["FolderName"];
            }
        }

        /// <summary>
        /// Updates the resxfiles.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 11/4/2012 - 3:41 PM
        private bool UpdateResxfiles()
        {
            //try
            {
                foreach (var sheetEntity in _sheetEntities)
                { 
                    var listOfFile = new List<ResourceFile>();
                    foreach ( var item in sheetEntity.TranslateItems)
                    {
                        var fileName = item.Label.Split('|')[0];
                        var label = item.Label.Split('|')[1];
                        foreach (DictionaryEntry entry in item.LangValuePair)
                        {
                            if (entry.Value.ToString() == string.Empty)
                                continue;

                            var file = new ResourceFile();
                            if (entry.Key.ToString() == "en-US") //default language
                            {
                                file.FileName = string.Format("{0}.ascx.Portal-{1}.resx", fileName, PortalId);
                                file.Language = "en-US";
                            }
                            else
                            {
                                file.Language = entry.Key.ToString();
                                file.FileName = string.Format("{0}.ascx.{1}.Portal-{2}.resx", fileName, entry.Key.ToString(), PortalId);
                            }
                            if (listOfFile.Any(a => a.FileName == file.FileName))
                            {
                                file = listOfFile.Single(a => a.FileName == file.FileName);
                                file.LabelValuePair[label] = entry.Value;
                            }
                            else
                            {
                                file.LabelValuePair[label] = entry.Value;
                                listOfFile.Add(file);
                            }
                        }
                    }
                    foreach (var resourceFile in listOfFile)
                    {
                        string path;
                        var dnnSpecialResource = GetDnnSpecialSheets();
                        if (dnnSpecialResource.ContainsKey(sheetEntity.SheetName))
                        {
                            if (sheetEntity.SheetName.Contains("@DNN@Core"))
                            {
                                var filename = resourceFile.FileName.Replace(".ascx", string.Empty);
                                path = dnnSpecialResource.Single(a => a.Key == sheetEntity.SheetName).Value + "\\" + filename;
                            }
                            else
                            {
                                string filename = resourceFile.FileName;
                                if (filename.Contains("GlobalResources") || filename.Contains("SharedResources"))
                                {
                                    filename = filename.Replace(".ascx", string.Empty);
                                }
                                path = dnnSpecialResource.Single(a => a.Key == sheetEntity.SheetName).Value + "\\" + filename;
                            }
                        }
                        else
                        {
                            string filename = resourceFile.FileName;
                            if (filename.Contains("GlobalResources") || filename.Contains("SharedResources"))
                            {
                                filename = filename.Replace(".ascx", string.Empty);
                            }
                            var folder = GetModuleFolderName(sheetEntity.SheetName);
                            path = "DesktopModules\\" + folder + "\\App_LocalResources\\" + filename;
                            
                        }
                        CreateResxFile(path, resourceFile.LabelValuePair);   
                    }
                }
            }
            //catch (Exception exception)
            //{

            //    errorMsg.InnerHtml = "<p>" + exception.Message + "</p>";
            //    return false;
            //}
            return true;
        }

        /// <summary>
        /// Creates the RESX file.
        /// </summary>
        /// <param name="filepath">Full path of file.</param>
        /// <param name="labelValuePair">The label value pair.</param>
        /// Author: Vu Dinh
        /// 11/4/2012 - 4:13 PM
        private void CreateResxFile(string filepath, Hashtable labelValuePair )
        {
            var wSettings = new XmlWriterSettings {Indent = true};
            var ms = new MemoryStream();
            XmlWriter xw = XmlWriter.Create(ms, wSettings);// Write Declaration
            xw.WriteStartDocument();

            // Write the root node
            xw.WriteStartElement("root");

            #region [ Write resheader node ]
            //write resheader node
            xw.WriteStartElement("resheader");
            xw.WriteStartAttribute("name");
            xw.WriteString("resmimetype");
            xw.WriteEndAttribute();
            xw.WriteStartElement("value");
            xw.WriteString("text/microsoft-resx");
            xw.WriteEndElement();
            xw.WriteEndElement();

            xw.WriteStartElement("resheader");
            xw.WriteStartAttribute("name");
            xw.WriteString("version");
            xw.WriteEndAttribute();
            xw.WriteStartElement("value");
            xw.WriteString("2.0");
            xw.WriteEndElement();
            xw.WriteEndElement();

            xw.WriteStartElement("resheader");
            xw.WriteStartAttribute("name");
            xw.WriteString("reader");
            xw.WriteEndAttribute();
            xw.WriteStartElement("value");
            xw.WriteString("System.Resources.ResXResourceReader, System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
            xw.WriteEndElement();
            xw.WriteEndElement();

            xw.WriteStartElement("resheader");
            xw.WriteStartAttribute("name");
            xw.WriteString("writer");
            xw.WriteEndAttribute();
            xw.WriteStartElement("value");
            xw.WriteString("System.Resources.ResXResourceWriter, System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
            xw.WriteEndElement();
            xw.WriteEndElement();
            #endregion

            #region [ Write data nodes ]
            foreach (DictionaryEntry entry in labelValuePair)
            {
                //write note name
                xw.WriteStartElement("data");

                //write note attributes
                xw.WriteStartAttribute("name");
                xw.WriteString(entry.Key.ToString());
                xw.WriteEndAttribute();

                //write note content
                xw.WriteStartElement("value");
                xw.WriteString(entry.Value.ToString());
                xw.WriteEndElement();

                //write end node
                xw.WriteEndElement();
            }
            #endregion

            //write end root node
            xw.WriteEndElement();
            // write end document
            xw.WriteEndDocument();

            xw.Flush();
            if (File.Exists(Server.MapPath(filepath)))
            {
                File.Delete(Server.MapPath(filepath));
            }
            var file = new FileStream(Server.MapPath(filepath), FileMode.CreateNew, FileAccess.ReadWrite);
            ms.WriteTo(file);
            ms.Close();
            ms.Dispose();
            file.Close();
            file.Dispose();
            
        }

        /// <summary>
        /// Creates the backup folder.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 11/3/2012 - 5:49 PM
        private string CreateBackupFolder()
        {
            _backupFolder = Server.MapPath(PortalSettings.HomeDirectory + "\\" + Cons.DEFAULT_BACKUP_FOLDER_NAME + "\\" + DateTime.Now.ToString(Cons.BACKUP_FOLDER_TIME_FORMAT));
            //Create backup folder
            if (!Directory.Exists(_backupFolder))
            {
                Directory.CreateDirectory(_backupFolder);
            }
            return _backupFolder;
        }

        /// <summary>
        /// Gets the backup folder.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 11/3/2012 - 5:49 PM
        private string GetBackupFolder()
        {
            return _backupFolder;
        }

        #region [ Event Handlers ]
        /// <summary>
        /// Handles the OnClick event of the btnSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 11/2/2012 - 2:09 PM
        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(upExcel.FileName))
            {
                validateRequired.IsValid = false;
            }
            else if (!upExcel.FileName.Contains(".xlsx"))
            {
                validateExtension.IsValid = false;
            }

           if(Page.IsValid)
           {
               //Read xlsx file to memory stream
              _excelFile = new MemoryStream(upExcel.FileBytes);
              if (_excelFile.Length > 0)
              {
                   ReadExcelFile();
				   
                   if (_sheetEntities.Any())
                   {
                       //Backup file here
                       if (BackupResxfiles())
                       {
                           //Update resx here
                           UpdateResxfiles();
                       }
                   }
               }
           }
        }
        #endregion
    }
}