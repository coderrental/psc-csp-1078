﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System.Linq;
using CR.DnnModules.Common;
using DocumentFormat.OpenXml.Spreadsheet;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;
using DotNetNuke.Services.Log.EventLog;

namespace TIEKinetix.CspModules.DnnTranslator.Common
{
    public class DnnModuleBase : PortalModuleBase
    {
        protected EventLogController DnnEventLog;
        protected string ExcelFilename;
        private string _localResourceFile;
        /// <summary>
        /// Handles the Init event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected virtual void Page_Init(object sender, EventArgs e)
        {
            //Init Local Resource file
            _localResourceFile = Utils.GetCommonResourceFile("DnnTranslator", LocalResourceFile);
            //Init module title
            ModuleConfiguration.ModuleTitle = GetLocalizedText("Label.ModuleTitle");

            if (Settings[Cons.SETTING_EXCEL_FILENAME] != null && Settings[Cons.SETTING_EXCEL_FILENAME].ToString() != string.Empty)
                ExcelFilename = Settings[Cons.SETTING_EXCEL_FILENAME].ToString();
            else
                ExcelFilename = Cons.DEFAULT_EXCEL_FILENAME;
        }

        /// <summary>
        /// Get localized text for the current portal language
        /// </summary>
        /// <param name="key">Resource Key</param>
        /// <returns>Localized text</returns>
        public string GetLocalizedText(string key)
        {
            return Localization.GetString(key, _localResourceFile);
        }

        /// <summary>
        /// Write message to Dnn event view
        /// </summary>
        /// <param name="message">message to write</param>
        public void Log(string message)
        {
            DnnEventLog.AddLog("DEBUG", message, PortalSettings, UserId, EventLogController.EventLogType.ADMIN_ALERT);
        }

        /// <summary>
        /// Copies the folder and contents.
        /// </summary>
        /// <param name="sourcePath">The source path.</param>
        /// <param name="destinationPath">The destination path.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 11/3/2012 - 5:44 PM
        public Exception CopyFolderAndContents(string sourcePath, string destinationPath)
        {
            try
            {
                if (!Directory.Exists(destinationPath))
                    Directory.CreateDirectory(destinationPath);
                //Now Create all of the directories
                foreach (string dirPath in Directory.GetDirectories(sourcePath, "*",
                    SearchOption.AllDirectories))
                    Directory.CreateDirectory(dirPath.Replace(sourcePath, destinationPath));

                //Copy all the files
                foreach (string newPath in Directory.GetFiles(sourcePath, "*.*",
                    SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(sourcePath, destinationPath),true);
            }
            catch (Exception exception)
            {

                return exception;
            }
            return null;
        }

        /// <summary>
        /// Gets the cell value.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="sharedString">The shared string.</param>
        /// <param name="reference">The reference.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 11/6/2012 - 5:00 PM
        public string GetCellValue(Worksheet worksheet, SharedStringTable sharedString, string reference)
        {
            Cell cell = worksheet.Descendants<Cell>().FirstOrDefault(c => reference.Equals(c.CellReference));
            if (cell == null || cell.DataType == null)
                return string.Empty;
            if (cell.DataType == CellValues.SharedString)
                return sharedString.ChildElements[int.Parse(cell.CellValue.InnerText)].InnerText;
            return (cell.CellValue.InnerText);
        }

        /// <summary>
        /// Gets the DNN special sheet.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 11/7/2012 - 10:28 AM
        public Dictionary<string,string> GetDnnSpecialSheets()
        {
            var dic = new Dictionary<string, string>
                          {
                              {"@DNN@Core", "App_GlobalResources"},
                              {"@DNN@Admin-Skin", "admin\\Skins\\App_LocalResources"},
                              {"@DNN@Admin-Controlpanel", "admin\\ControlPanel\\App_LocalResources"},
                              {"@DNN@Admin-Modules", "admin\\Modules\\App_LocalResources"},
                              {"@DNN@Admin-Portal", "admin\\Portal\\App_LocalResources"},
                              {"@DNN@Admin-Security", "admin\\Security\\App_LocalResources"},
                              {"@DNN@Admin-Tab", "admin\\Tabs\\App_LocalResources"},
                              {"@DNN@Admin-Users", "admin\\Users\\App_LocalResources"},
                              {"@DNN@AuthenticationServices", "DesktopModules\\AuthenticationServices\\DNN\\App_LocalResources"}
                          };
            return dic;
        }
    }
}