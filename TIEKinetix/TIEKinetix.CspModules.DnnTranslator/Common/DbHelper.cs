﻿using System;
using DotNetNuke.Data;

namespace TIEKinetix.CspModules.DnnTranslator.Common
{
	public static class DbHelper
	{
		/// <summary>
		/// 	Executes the scalar.
		/// </summary>
		/// <param name = "query">The query.</param>
		/// <returns></returns>
		/// Author: ltu
		/// 10/31/2012 - 12:06 AM
		public static object ExecuteScalar(string query)
		{
			try
			{
				var dataReader = DataProvider.Instance().ExecuteSQL(query);
				return dataReader.Read() ? dataReader[0] : null;
			}
			catch (Exception)
			{
				return null;
			}
			
		}

	}
}