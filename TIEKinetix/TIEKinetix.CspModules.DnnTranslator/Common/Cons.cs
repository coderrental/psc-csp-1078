﻿namespace TIEKinetix.CspModules.DnnTranslator.Common
{
    public class Cons
    {
        public const string SETTING_EXCEL_FILENAME = "ExcelFilename";
        
        public const string DEFAULT_EXCEL_FILENAME = "TranslateSheets";

        public const string DEFAULT_BACKUP_FOLDER_NAME = "TranslateBackup";

        public const string BACKUP_FOLDER_TIME_FORMAT = "M-d-y HH.mm.ss";
    }
}