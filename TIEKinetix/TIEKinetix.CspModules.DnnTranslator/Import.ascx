﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Import.ascx.cs" Inherits="TIEKinetix.CspModules.DnnTranslator.Import" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Panel runat="server" ID="pnImport">
    <div style="text-align: center">
        <h2 id="h2Title" runat="server"/>
        <hr />
    </div>
    <div style="text-align: center">
        <asp:FileUpload runat="server" ID="upExcel"/>
        <asp:RequiredFieldValidator ID="validateRequired" runat="server" ControlToValidate="upExcel" EnableClientScript="False">
            <span style="FONT-SIZE: 11px; color: red;"><%=GetLocalizedText("Label.RequiredField")%></span>
        </asp:RequiredFieldValidator>
        <asp:CustomValidator ID="validateExtension" runat="server" Display="Dynamic" ClientValidationFunction="validateRadUpload" EnableClientScript="False">
                    <span style="FONT-SIZE: 11px; color: red;"><%=GetLocalizedText("Label.InvalidExtension")%></span>
        </asp:CustomValidator>
        <p></p>
        <telerik:RadButton runat="server" ID="btnSubmit" AutoPostBack="True" OnClick="btnSubmit_OnClick">
            <Icon PrimaryIconCssClass="rbUpload" PrimaryIconTop="4" PrimaryIconLeft="4"></Icon>
        </telerik:RadButton>
    </div>
    <pre id="errorMsg" runat="server"></pre>
</asp:Panel>
