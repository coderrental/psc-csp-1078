﻿using System;
using System.IO;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Profile;
using DotNetNuke.Entities.Users;

namespace TIEKinetix.CspModules.Common
{
    public static class Utils
    {
        public static string GetCommonResourceFile(string name, string localResourceFile)
        {
            return localResourceFile.Substring(0, localResourceFile.LastIndexOf("/") + 1) + name;
        }

        public static int GetIntegrationKey(UserInfo userInfo)
        {
            if (userInfo.Profile.ProfileProperties[Keys.CspIntegrationKey] == null) 
                return -1;
            int key = -1;
            return int.TryParse(userInfo.Profile.ProfileProperties[Keys.CspIntegrationKey].PropertyValue, out key) ? key : -1;
        }

        /// <summary>
        /// Get an app key value in web.config
        /// </summary>
        /// <param name="portalId"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetSetting(int portalId, string key)
        {
            return Config.GetSetting("Portal" + portalId + key);
        }

        /// <summary>
        /// Get csp connection string
        /// </summary>
        /// <param name="portalId"></param>
        /// <returns></returns>
        public static string GetConnectionString(int portalId)
        {
            string cs = "";

            try
            {
                cs = Config.GetConnectionString("CspPortal" + portalId);
            }
            catch
            {
                cs = "";
            }
            return cs;
        }

        /// <summary>
        /// Check an app setting key to see if the result is either true/false
        /// </summary>
        /// <param name="portalId">Portal id to check</param>
        /// <param name="key">Appsetting key to check</param>
        /// <returns></returns>
        public static bool Check(int portalId, string key)
        {
            string v = GetSetting(portalId, key);
            return !string.IsNullOrEmpty(v) && v.Equals("true", StringComparison.CurrentCultureIgnoreCase);
        }

        /// <summary>
        /// Resize an image
        /// </summary>
        /// <param name="path"></param>
        /// <param name="filename"></param>
        /// <param name="width"></param>
        /// <param name="maxHeight"></param>
        /// <returns></returns>
        public static string ResizeImage(string path, string filename, int width, int maxHeight)
        {
            string newFileName = filename;
            try
            {
                if (width <= 0 || maxHeight <= 0) return filename;

                System.Drawing.Image image = System.Drawing.Image.FromFile(path + "\\" + filename);
                if (image.Width <= width && image.Height <= maxHeight) return filename;

                image.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
                image.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

                int newHeight = image.Height * width / image.Width;
                int newWidth = 0;
                if (newHeight > maxHeight)
                {
                    newWidth = image.Width * maxHeight / image.Height;
                    newHeight = maxHeight;
                }
                if (newWidth == 0)
                    newWidth = width;

                System.Drawing.Image NewImage = image.GetThumbnailImage(newWidth, newHeight, null, IntPtr.Zero);

                // Clear handle to original file so that we can overwrite it if necessary
                image.Dispose();

                // Save resized picture

                newFileName = Path.GetFileNameWithoutExtension(filename) + "r" + Path.GetExtension(filename);
                NewImage.Save(path + "\\" + newFileName);
            }
            catch (Exception ex)
            {
                //Debug("Unable to resize image. Error: " + ex.Message);
                //DnnLog.Error(ex.Message, ex);
            }

            return newFileName;
        }
    }
}