﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TIEKinetix.CspModules.Common
{
    public class Keys
    {
        public static string CspIntegrationKey = "CspId";
        public static string ModuleTitleKey = "ModuleTitle";
        public static string ModuleError_IntegrationKey = "ModuleError.MissingIntegrationKey";
        public static string ModuleError_ChanelExternalId = "ModuleError.InvalidChannelExternalId";
        public static string ModuleError_PortalConnectionString = "ModuleError.MissingConnectionString";
        public static int CSP_Publish_Stage_Id = 50;
        public static int CSP_UnPublish_Stage_Id = 51;
    }
}
