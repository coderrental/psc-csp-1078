﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainView.ascx.cs" Inherits="TIEKinetix.CspModules.ContentSetup.MainView" %>
<%@ Import Namespace="DotNetNuke.Common" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadAjaxLoadingPanel runat="server" ID="ajaxLoadingPanel" IsSticky="True" CssClass="ajax-loading-panel"></telerik:RadAjaxLoadingPanel>
<p runat="server" Visible="False" ID="errorHolder"></p>
<asp:Panel runat="server" ID="panelMain">
    <asp:Panel runat="server" ID="panelIntro" CssClass="panelIntro">
        <%--Description Block--%>
		<div id="description_section">
			<div id="desc_text">
				<h1 id="desc_title"><%= GetLocalizedText("Description.Title") %></h1>
				<div id="desc_detail">
					<p>
						<%= GetLocalizedText("Description.Detail") %>
					</p>
				</div>
				<div id="desc_controls">
					<a id="btnPreviewShowcase" class="btn-long-link" href="javascript: void(0)">
						<span class="btn-long ss-btn">
							<span class="btn-long-right ss-btn">
								<span class="btn-long-mid ss-btn">
									<span class="btn-long-text ss-btn"><%= GetLocalizedText("Button.PreviewShowcase") %></span>
								</span>
							</span>
						</span>
					</a>
					<a id="btnPickupCode" class="btn-long-link" href="javascript: void(0)">
						<span class="btn-long ss-btn">
							<span class="btn-long-right ss-btn">
								<span class="btn-long-mid ss-btn">
									<span class="btn-long-text ss-btn"><%= GetLocalizedText("Button.PickUpCode") %></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>
			<div id="desc_figure">
				<img width="255px" src="<%= GetContentSetupThumbnail() %>"/>
			</div>
			<div class="clear"></div>
			
		</div>

		<%--Popup Windows--%>
		<div id="wdCodePickup" title="<%= GetLocalizedText("WindowTitle.PickupYourCode") %>">
			<div class="wdContent">
				<div id="PnlContainer" class="hide">
					<div id="pShowcase">
					</div>
					<table width="100%" align="center">
						<tbody>
						<tr>
							<td colspan="2" bgcolor="#EDE9EA" style="font: 18px/130% Arial, Helvetica, sans-serif;
								padding: 5px;">
								<b><%= GetLocalizedText("PickupYourCode.txtSetupSteps") %></b>
							</td>
						</tr>
						<tr>
							<td width="35" align="center" style="padding-top: 6px;">
								<img src="<% if (string.IsNullOrEmpty(GetLocalizedText("Image.StepOne")))
								             { %><%= ControlPath %>Images/stepOne.png  <%}
								             else
								             {
								                %>
								                 <%= GetLocalizedText("Image.StepOne") %>
                                                <%
								             }%>" width="24" height="25">
							</td>
							<td style="font: 18px/130% Arial, Helvetica, sans-serif; padding-top: 4px;">
								<%= GetLocalizedText("PickupYourCode.txtStep1") %>
							</td>
						</tr>
						<tr>
							<td align="center">
								&nbsp;
							</td>
							<td valign="top" style="font: 11px/130% Arial, Helvetica, sans-serif;">
								<%= GetLocalizedText("PickupYourCode.txtStep1Desc") %>
							</td>
						</tr>
						<tr>
							<td align="center" style="padding-top: 6px;">
								<img src="<% if (string.IsNullOrEmpty(GetLocalizedText("Image.StepTwo")))
								             { %><%= ControlPath %>Images/stepTwo.png  <%}
								             else
								             {
								                %>
								                 <%= GetLocalizedText("Image.StepTwo") %>
                                                <%
								             } %>" width="24" height="25">
							</td>
							<td style="font: 18px/130% Arial, Helvetica, sans-serif; padding-top: 4px;">
								<%= GetLocalizedText("PickupYourCode.txtStep2") %>
							</td>
						</tr>
						<tr>
							<td align="center">
								&nbsp;
							</td>
							<td valign="top" style="font: 11px/130% Arial, Helvetica, sans-serif;">
								<%= GetLocalizedText("PickupYourCode.txtStep2Desc") %>
							</td>
						</tr>
						<tr>
							<td align="center">
								&nbsp;
							</td>
							<td>
								<div class="title">
									<%= GetLocalizedText("PickupYourCode.txtShowcaseTitle") %></div>
								<div class="codeBlock">
									<p style="font: 12px/130% 'Courier New', Courier, monospace; color: #666666;">
										<%= GetLocalizedText("PickupYourCode.txtShowcaseCodeTitle") %></p>
									<p style="font: 12px/130% 'Courier New', Courier, monospace; color: #0000FF;">
										<textarea name="ShowcaseScript" rows="4" cols="20" readonly="readonly" id="ShowcaseScript" style="height:129px;width:99%;color: #990000; border: none;"><%= GetLocalizedText("PickupYourCode.TextareaContent").Replace("{scriptlink}", GetSyndicationScript())%></textarea>
									</p>
								</div>
							</td>
						</tr>
						<tr>
						</tr>
						<tr>
							<td align="center" style="padding-top: 6px;">
								<img src="<% if (string.IsNullOrEmpty(GetLocalizedText("Image.StepThree")))
								             { %><%= ControlPath %>Images/stepThree.png  <%}
								             else
								             {
                                                %>
								                 <%= GetLocalizedText("Image.StepThree") %>
                                                <%
								             } %>" width="24" height="25">
							</td>
							<td style="font: 18px/130% Arial, Helvetica, sans-serif; padding-top: 4px;">
								<%= GetLocalizedText("PickupYourCode.txtStep3") %>
							</td>
						</tr>
						<tr>
							<td align="center">
								&nbsp;
							</td>
							<td valign="top" style="font: 11px/130% Arial, Helvetica, sans-serif;">
								<%= GetLocalizedText("PickupYourCode.txtStep3Desc") %>
							</td>
						</tr>
						<tr>
							<td align="center">
							</td>
							<td style="font: 0px/0%;">
							</td>
						</tr>
						<tr>
							<td align="center">
							    <img src="<% if (string.IsNullOrEmpty(GetLocalizedText("Image.IconInfo")))
								             { %><%= ControlPath %>Images/icon_info.png  <%}
								             else
								             {
                                                %>
								                 <%= GetLocalizedText("Image.IconInfo") %>
                                                <%
								             } %>" width="16" height="16">
							</td>
							<td style="font: 11px/130% Arial, Helvetica, sans-serif; padding-top: 10px;">
								<%= GetLocalizedText("PickupYourCode.txtAttention") %>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div id="code-actions">
									<div id="generate_embed_code_email">
										<input type="text" id="email_embed_code" value="<%= UserInfo.Email %>"/>
                                        <br id="breakInput"/>
										<input type="submit" id="submit_email_embed_code" value="<%= GetLocalizedText("PickupYourCode.btnEmailScript")%>" label="<%= GetLocalizedText("PickupYourCode.msgInvalidEmailAddress") %>" />
										<input type="submit" id="copy-embed-code" href="javascript: void(0)" rel="<%= ControlPath %>Js/ZeroClipboard.swf" value="<%= GetLocalizedText("PickupYourCode.btnCopyCode")%>" />
										<span id="code-copied"><%= GetLocalizedText("PickupYourCode.CodeCopied") %></span>
									</div>
									<p id="email_embed_code_result"></p>
									<p class="hide" id="email_embed_code_succeed"><%= GetLocalizedText("PickupYourCode.msgEmailSentSucceed") %></p>
									<p class="hide" id="email_embed_code_failed"><%= GetLocalizedText("PickupYourCode.msgEmailSentFailed") %></p>
								</div>
							</td>
						</tr>
					</tbody></table>
				</div>
			</div>
		</div>
				
		<div id="wdPreviewShowCase" title="<%= GetLocalizedText("WindowTitle.PreviewShowcase") %>" class="<%= GetEntrypointValue() != 4 ? "wireframe" : "" %> hide">
			<div class="wdContent">
				<!--<script type="text/javascript" src="<%= GetSyndicationScript() %>"></script>-->
                <iframe id="csp_iframe_2" scrolling="no" frameborder="0" src='<%= GetShowcasePreviewIframeLink() %>' width="700" height="850"></iframe>
			</div>
		</div>
		<%--EOF Popup Windows--%>
    </asp:Panel>
	<div id="campaign_section_switch" class="<%= HideButtonShowcaseCustomization() ? "hide" : ""%>"><span id="switch_text"></span></div>
	<div id="customize_showcase_section" class="<%= !IsShowcaseCustomizationShowByDefault() ? "hide" : ""%>">
		<asp:Panel runat="server" ID="panelLanguage">
			<div id="categories-language">
				<div class="warning_container">
					<asp:Label runat="server" ID="lblLanguages"/>
				</div>          
				<telerik:RadComboBox runat="server" ID="cbxLanguages" CssClass="warning_combobox" AutoPostBack="True" OnSelectedIndexChanged="cbxLanguages_OnSelectedIndexChanged"/>
			</div>
		</asp:Panel>
		<asp:Panel runat="server" ID="panelCategoriesTree">
			<telerik:RadTabStrip runat="server" ID="tabMain" Orientation="HorizontalTop" MultiPageID="multiMainTab" CssClass="panel-category-tree" OnClientTabSelected="ClientTabSelecting"/>
			<telerik:RadMultiPage runat="server" ID="multiMainTab" CssClass="category-tree-content">
				<telerik:RadPageView runat="server" ID="pvCategory">
					<div class="pvHelpDescription">
						<%= GetLocalizedText("Category.Description") %>
					</div>
					<div class="csp_left_col">
					    <div class="device-show-categories"><span><%=GetLocalizedText("Label.ShowCategoriesTree") %></span></div>
                        <div class="device-hide-categories" title="<%= GetLocalizedText("Label.HideCategoriesTree") %>" ></div>
					    <div class="device-categories-wrap">
						    <asp:Panel runat="server" ID="treeContainer">
							    <%-- Tree categories render dynamic by themes  --%>
						    </asp:Panel>
						    <div id="btnSaveCategory"><telerik:RadButton runat="server" ID="btnCatSave" OnClick="btnCatSave_OnClick" CssClass="btnCatSave"></telerik:RadButton></div>
                        </div>
					</div>
					<div class="csp_right_col">
						<iframe id="csp_iframe_1" scrolling="no" frameborder="0" src='<%= GetShowcasePreviewIframeLink() %>' width="700" height="850"></iframe>
					</div>
					<div class="clear"></div>
				</telerik:RadPageView>
				<telerik:RadPageView runat="server" ID="pvEntrypoint">
					<div class="pvHelpDescription">
						<%= GetLocalizedText("EntryPoin.Description") %>
					</div>
					<div class="entrypoint_left_col">
						<div class="entrypoint-left-col-content">
							<div id="entryPointHeader">
								<asp:Label runat="server" ID="lblEntrypointHeader"></asp:Label>    
							</div>
							<div id="entryPointStep">
								<asp:Label runat="server" ID="lblEntrypointStep"></asp:Label>    
							</div>
							<div id="entryPointContainer">
								<div id="step1Container">
									<div class="content_setup_header">
										<asp:Label runat="server" ID="lblStep1Header" />
									</div>
									<div>
									   <ul id="cspEntryPoints">
											<li>
												<input type="radio" value="4" id="rEpIF" name="entrypoint" />
												<label for="rEpIF"><%= GetLocalizedText("Entrypoint.NoEntry")%></label>
											</li>
											<li>
												<input type="radio" value="1" id="rEpT" name="entrypoint" />
												<label for="rEpT"><%= GetLocalizedText("Entrypoint.TextLink")%></label>
												<div class="cspConfigurationBox hide" id="soTmbox">
													<input type="text" value='<%= GetTextlinkValue()%>' size="30" id="soTm" name="soTm" />
													<asp:CustomValidator runat="server" ID="textlinkValidator"></asp:CustomValidator>
												</div>
											</li>
											<li>
												<input type="radio" value="2" id="rEpImg" name="entrypoint">
												<label for="rEpImg"><%= GetLocalizedText("Entrypoint.Banner")%></label>
												<div class="cspConfigurationBox hide" id="epBannerbox">
													<ul id="epBanner">
														<li>
															<input type="radio" value="1" id="ldimg1" name="epBanner" />
															<label for="ldimg1"><%= GetLocalizedText("Entrypoint.BannerButton")%></label>
														</li>
														<li>
															<input type="radio" value="2" id="ldimg2" name="epBanner" />
															<label for="ldimg2"><%= GetLocalizedText("Entrypoint.BannerSquare")%></label>
														</li>
														<li>
															<input type="radio" value="3" id="ldimg3" name="epBanner" />
															<label for="ldimg3"><%= GetLocalizedText("Entrypoint.HalfBanner")%></label>
														</li>
														<li>
															<input type="radio" value="4" id="ldimg4" name="epBanner" />
															<label for="ldimg4"><%= GetLocalizedText("Entrypoint.FullBanner")%></label>
														</li>
														<li>
															<input type="radio" value="5" id="ldimg5" name="epBanner" />
															<label for="ldimg5"><%= GetLocalizedText("Entrypoint.UploadBanner")%></label>
														</li>
													</ul>
													<div id="banner_image_preview">
														<img src=""/>
														<br />
														<asp:FileUpload runat="server" ID="bannerUpload" CssClass="hide"/>
														&nbsp;&nbsp;&nbsp;<asp:CustomValidator runat="server" ID="bannerUploadValidator" ControlToValidate="bannerUpload"></asp:CustomValidator>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div id="step2Container" class="hide">
									<div class="content_setup_header">
										<asp:Label runat="server" ID="lblStep2Header"/>
									</div>
									<div>
									   <ul id="cspLaunchType">
											<li>
												<input type="radio" value="1" id="rLtLb" name="launchtype">
												<label for="rLtLb"><%= GetLocalizedText("Launchtype.Lightbox") %></label></li>
											<li>
												<input type="radio" value="2" id="rLtW" name="launchtype">
												<label for="rLtW"><%= GetLocalizedText("Launchtype.Iframe") %></label>
												<div class="cspConfigurationBox hide" id="rLtWTargetbox">
													<input type="text" value='<%= GetLaunchtypeIframeValue() %>' size="30" id="rLtWTarget" name="rLtWTarget">
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div id="btnSaveEntry"><telerik:RadButton runat="server" ID="btnSaveEntrypoint" CssClass="btnCatSave" OnClick="btnSaveEntrypoint_OnClick"/></div>
					</div>
					<div class="entrypoint_right_col">
						<script type="text/javascript" src="<%= GetSyndicationScript() %>"></script>
					</div>

					<div class="clear"></div>
					<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>
				</telerik:RadPageView>
			</telerik:RadMultiPage>
		</asp:Panel>
	</div>
	<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
		<script type="text/javascript">
		    var rightestSpanPx = 0;
		    var leftcolResizerFirstRun = false;
		    var iconShow = '<%=GetLocalizedText("Path.IconShow")%>';
            var iconHide = '<%=GetLocalizedText("Path.IconHide")%>';
		    function ClientTabSelecting(sender, eventArgs) {
		        //eventArgs.set_cancel(true);
		    }
		    //Init copy generated code to clipboard
		    var isZClipInit = 1;
		    function initEmbedCodeCopier() {
		        if (isZClipInit <= 2) {
		            var swfFilePath = $('#copy-embed-code').attr('rel');
								
		            $('#copy-embed-code').zclip({
		                path: swfFilePath,
		                copy: function () { return $('#ShowcaseScript').val(); },
		                beforeCopy: function () {
		                    $('#copy-embed-code').hide();
		                    $('#code-copied').show();
		                    setTimeout(function(){$('#copy-embed-code').show(); $('#code-copied').hide();},3000);
		                },
		                afterCopy: function () {
		                    /*$('#code-copied').show().delay(5000).fadeOut('fast', function () {
                    			$('#copy-embed-code').show();
                    		});*/
		                    //$('#copy-embed-code').show();
		                }
		            });
		            isZClipInit++;
		        }
		    }
						
		    //Init Submit Embed Code for sending email
		    function initEmbedCodeEmailSubmit() {
		        $('#submit_email_embed_code').click(function (e) {
		            e.preventDefault();
		            var thisEl = $(this);
		            var emailReciver = $('#email_embed_code').val();
		            if (!validateEmail(emailReciver)) {
		                $('#email_embed_code_result').html(thisEl.attr('label'));
		                return;
		            }
		            var loadingPanel = $find('<%= RadAjaxLoadingPanel1.ClientID %>');
                    var crrUpdateControl = "PnlContainer";
                    loadingPanel.show(crrUpdateControl);
                    $.ajax({
                        type: 'POST',
                        url: '<%= GetModuleURL() %>',
                    	data: { 'ajaxaction': 'emailembedcode', reciever: emailReciver },
                    	dataType: 'xml',
                    	success: function (responseData) {
                    	    var resultCode = $(responseData).find('resultcode').text();
                    	    resultCode = parseInt(resultCode);
                    	    if (resultCode == 1) {
                    	        $('#email_embed_code_result').html($('#email_embed_code_succeed').html());
                    	    } else if (resultCode == 3) {
                    	        $('#email_embed_code_result').html("Empty email content");
                    	    }
                    	    else {
                    	        $('#email_embed_code_result').html($('#email_embed_code_failed').html() + '<p style="color: #FF0000">' + $(responseData).find('resultmsg').text() + '</p>');
                    	    }
                    	    loadingPanel.hide(crrUpdateControl);
                    	},
                    	error: function (err) {
                    	}
                    });
                });
            }
						
            //Validate Email
            function validateEmail(email) { 
                var re = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                return re.test(email);
            } 
                    	
            function ClientNodeClicked(sender, eventArgs) {
                var isCheck = eventArgs.get_node().get_checked();
                if (isCheck) {
                    eventArgs.get_node().set_checked(false);
                } else {
                    eventArgs.get_node().set_checked(true);
                }
            }

            function initIconShow() {
                if (iconShow!='') {
                    $('#switch_text.icon-show').css("background", "url('"+iconShow+"') no-repeat scroll left center");
                    $("#switch_text.icon-hide").attr('style', '');
                }
            }

            function initIconHide() {
                if (iconHide!='') {
                    $('#switch_text.icon-hide').css("background", "url('"+iconHide+"') no-repeat scroll left center");
                    $("#switch_text.icon-show").attr('style', '');
                }
            }
            //Campaign hide/show switch text
            function initCampaignSwitchText(label4HideAll, label4ShowAll) {
                campaignSwitchOpenText = label4ShowAll;
                campaignSwitchCloseText = label4HideAll;
                $('#switch_text').html(campaignSwitchOpenText);
                $('#switch_text').removeClass('icon-hide').addClass('icon-show');
                initIconHide();
                initIconShow()
                if ($('#customize_showcase_section').is(':visible')) {
                    $('#switch_text').html(campaignSwitchCloseText);
                    $('#switch_text').removeClass('icon-show').addClass('icon-hide');
                    initIconShow();
                    initIconHide();
                    /*var iframe = document.getElementById('_opt1');
					iframe.src = iframe.src;*/
                    if (leftcolResizerFirstRun == false) {
                        LeftColResizer();
                        leftcolResizerFirstRun = true;
                    }
                }
            }

            jQuery(function() {
                var sblockSession = '<%= Session["SBlockState"] %>';
                if (sblockSession == '1')
                    $('#customize_showcase_section').removeClass('hide');
                else if (sblockSession == '-1')
                    $('#customize_showcase_section').addClass('hide');
                if (iconShow!='') {
                    $('#switch_text.icon-hide').css("background", "url('"+iconShow+"') no-repeat scroll left center");
                    $("#switch_text.icon-show").attr('style', '');
                }
                if (iconHide!='') {
                    $('#switch_text.icon-show').css("background", "url('"+iconHide+"') no-repeat scroll left center");
                    $("#switch_text.icon-hide").attr('style', '');
                }
                //Set default values
                var entryPointValue = <%= GetEntrypointValue() %>;
						jQuery('input[name=entrypoint]').each(function() {
						    if (jQuery(this).attr('value') == entryPointValue) {
						        jQuery(this).attr("checked", "checked");
						    }
						});

						var bannerValue = <%= GetBannerValue() %>;
                jQuery('input[name=epBanner]').each(function() {
                    if (jQuery(this).attr('value') == bannerValue) {
                        jQuery(this).attr("checked", "checked");
                    }
                });

                var launchtypeValue = <%= GetLaunchtypeValue() %>;
                jQuery('input[name=launchtype]').each(function() {
                    if (jQuery(this).attr('value') == launchtypeValue) {
                        jQuery(this).attr("checked", "checked");
                    }
                });

                if (jQuery('#rEpT').attr("checked") == "checked") {
                    jQuery('#soTmbox').show();
                    jQuery('#step2Container').show();
                    if (jQuery('#rLtW').attr("checked") == "checked") {
                        jQuery('#rLtWTargetbox').show();
                    }
                }

                if (jQuery('#rEpImg').attr("checked") == "checked") {
                    jQuery('#epBannerbox').show();
                    jQuery('#step2Container').show();
                    if (jQuery('#rLtW').attr("checked") == "checked") {
                        jQuery('#rLtWTargetbox').show();
                    }
                }

                if (jQuery('#rLtW').attr("checked") == "checked") {
                    jQuery('#rLtWTargetbox').show();

                }

                jQuery('input[name=entrypoint]').click(function() {
                    if (jQuery(this).attr("id") == "rEpT") {
                        jQuery('#soTmbox').slideDown();
                        jQuery('#epBannerbox').slideUp();
                        jQuery('#step2Container').slideDown();
                    } else if (jQuery(this).attr("id") == "rEpImg") {
                        jQuery('#soTmbox').slideUp();
                        jQuery('#epBannerbox').slideDown();
                        jQuery('#step2Container').slideDown();
                    } else {
                        jQuery('#soTmbox').slideUp();
                        jQuery('#epBannerbox').slideUp();
                        jQuery('#step2Container').slideUp();
                    }
                });

                jQuery('input[name=launchtype]').click(function() {
                    if (jQuery(this).attr("id") == "rLtW") {
                        jQuery('#rLtWTargetbox').slideDown();
                    } else {
                        jQuery('#rLtWTargetbox').slideUp();
                    }
                });

                var banner_buttonLink = '<%= GetBannerButtonLink() %>';
                var banner_buttonSquareLink = '<%= GetBannerSquareLink() %>';
                var banner_buttonHalfLink = '<%= GetBannerHalfLink() %>';
                var banner_buttonFullLink = '<%= GetBannerFullLink() %>';
                var banner_customLink = '<%= GetCustomBannerLink() %>';

                jQuery('input[name=epBanner]').each(function() {
                    if (jQuery(this).attr("checked") == "checked") {
                        if (jQuery(this).attr("value") == 1) {
                            jQuery('#banner_image_preview img').attr("src", banner_buttonLink);
                        } else if (jQuery(this).attr("value") == 2) {
                            jQuery('#banner_image_preview img').attr("src", banner_buttonSquareLink);
                        } else if (jQuery(this).attr("value") == 3) {
                            jQuery('#banner_image_preview img').attr("src", banner_buttonHalfLink);
                        } else if (jQuery(this).attr("value") == 4) {
                            jQuery('#banner_image_preview img').attr("src", banner_buttonFullLink);
                        } else if (jQuery(this).attr("value") == 5) {
                            jQuery('#banner_image_preview img').attr("src", banner_customLink);
                            if (banner_customLink.length == 0) {
                                jQuery('#banner_image_preview img').hide();
                            }
                            jQuery('#<%= bannerUpload.ClientID %>').show();
                        }
    }
                });

                jQuery('input[name=epBanner]').click(function() {
                    if (jQuery(this).attr("value") == 1) {
                        jQuery('#banner_image_preview img').attr("src", banner_buttonLink);
                        jQuery('#banner_image_preview img').show();
                        jQuery('#<%= bannerUpload.ClientID %>').hide();
                    } else if (jQuery(this).attr("value") == 2) {
                        jQuery('#banner_image_preview img').attr("src", banner_buttonSquareLink);
                        jQuery('#banner_image_preview img').show();
                        jQuery('#<%= bannerUpload.ClientID %>').hide();
                    } else if (jQuery(this).attr("value") == 3) {
                        jQuery('#banner_image_preview img').attr("src", banner_buttonHalfLink);
                        jQuery('#banner_image_preview img').show();
                        jQuery('#<%= bannerUpload.ClientID %>').hide();
                    } else if (jQuery(this).attr("value") == 4) {
                        jQuery('#banner_image_preview img').attr("src", banner_buttonFullLink);
                        jQuery('#banner_image_preview img').show();
                        jQuery('#<%= bannerUpload.ClientID %>').hide();
                    } else if (jQuery(this).attr("value") == 5) {
                        jQuery('#banner_image_preview img').attr("src", banner_customLink);
                        if (banner_customLink.length == 0) {
                            jQuery('#banner_image_preview img').hide();
                        } else {
                            jQuery('#banner_image_preview img').show();
                        }
                        jQuery('#<%= bannerUpload.ClientID %>').show();
                    }
                });

                
                //Code Pick Up Window
                var wdPickYourCodeInit = $('#wdCodePickup').kendoWindow({
                    actions: ["Close"],
                    draggable: true,
                    height: "550px",
                    width: "690px",
                    modal: true,
                    resizable: false,
                    visible: false,
                    title: $('#wdCodePickup').attr('title')
                }).data("kendoWindow");


                $('#btnPickupCode').click(function() {
                    var wdPickYourCode = $('#wdCodePickup').data("kendoWindow");
                    $('#PnlContainer').show();
                    wdPickYourCode.center();
                    wdPickYourCode.open();
                    initEmbedCodeCopier();
                    return false;
                });
                
                //Preview Showcase Window
                //wdPreviewShowcase
                //$('.entrypoint_right_col').clone(true).appendTo('#wdPreviewShowCase .wdContent');
                var wdPreviewShowCaseInit = $('#wdPreviewShowCase').kendoWindow({
                    actions: ["Close"],
                    draggable: true,
                    height: "<%= GetPreviewPopupHeight() %>",
                    width: "<%= GetPreviewPopupWidth() %>",
                    modal: true,
                    resizable: false,
                    visible: false,
                    title: $('#wdPreviewShowCase').attr('title')
                }).data("kendoWindow");

                $('#btnPreviewShowcase').click(function() {
                    var wdPreviewShowCase = $('#wdPreviewShowCase').data("kendoWindow");
                    $('#wdPreviewShowCase').show();
                    wdPreviewShowCase.center();
                    wdPreviewShowCase.open();
                    return false;
                });
                
                /*=============Detect browser size to make responsive layout===============*/
                if ($(window).width() <= 767) {
                    //resize pickyour code window
                    var pickyourcodeWd = $("#wdCodePickup").parent() ;
                    $(pickyourcodeWd).width($(window).width() - 10);
					
                    
                    //resize preview showcase window
                    var previewShowcaseWd = $("#wdPreviewShowCase").parent();
                    $(previewShowcaseWd).width($(window).width() - 10);
                    
                    //zoom the preview showcase iframe
                    $('#csp_iframe_2').width($(window).width() - 35);
                    /*
                    var zoomPercent =($(window).width() / $("#csp_iframe_2").width()) - 0.02;
                    if ($.browser.msie) {
                        zoomPercent = 100 * zoomPercent;
                        $('#csp_iframe_2').css({'zoom': zoomPercent + '%', '-ms-zoom': zoomPercent + '%'});
                    } else {
                        $('#csp_iframe_2').css({
                            '-moz-transform': 'scale(' + zoomPercent + ', ' + zoomPercent + ')',
                            '-webkit-transform': 'scale(' + zoomPercent + ', ' + zoomPercent + ')',
                            '-o-transform': 'scale(' + zoomPercent + ', ' + zoomPercent + ')',
                            '-ms-transform': 'scale(' + zoomPercent + ', ' + zoomPercent + ')',
                            'transform': 'scale(' + zoomPercent + ', ' + zoomPercent + ')'
                        });
                    }
                    */
                    
                    //show categories tree
                    $(".device-show-categories span").click(function() {
                        $(".device-categories-wrap").stop().slideDown();
                    });
                    $(window).scroll(function() {
                        var categoriesTreeTopOffset = $('.device-categories-wrap').offset().top;
                        var windowTopOffset = $(document).scrollTop();
                        var rangeOffset = categoriesTreeTopOffset + $('.device-categories-wrap').height();
                        if (windowTopOffset > categoriesTreeTopOffset && windowTopOffset < rangeOffset) {
                            $(".device-hide-categories").fadeIn();
                        } else {
                            $(".device-hide-categories").fadeOut();
                        }
                    });
                    //hide categories tree
                    $(".device-hide-categories").click(function() {
                        var categoriesTreeTopOffset = $('.device-categories-wrap').offset().top;
                        $(".device-categories-wrap").stop().slideUp();
                        $(".device-hide-categories").hide();
                        $(document).scrollTop(categoriesTreeTopOffset - 50);
                    });
                    
                    //zoom the categories iframe
                    var rightColWidth = parseInt($(window).width() * $('.csp_right_col').width() / 100); //convert from width percent
					
                    $('#csp_iframe_1').width($('.csp_right_col').width() - 35);
                    /*
                    var zoomPercentIfr1 = rightColWidth / $("#csp_iframe_1").width() - 0.02;
                    if ($.browser.msie) {
                        zoomPercentIfr1 = 100 * zoomPercentIfr1;
                        $('#csp_iframe_1').css({'zoom': zoomPercentIfr1 + '%', '-ms-zoom': zoomPercentIfr1 + '%'});
                    } else {
                        $('#csp_iframe_1').css({
                            '-moz-transform': 'scale(' + zoomPercentIfr1 + ', ' + zoomPercentIfr1 + ')',
                            '-webkit-transform': 'scale(' + zoomPercentIfr1 + ', ' + zoomPercentIfr1 + ')',
                            '-o-transform': 'scale(' + zoomPercentIfr1 + ', ' + zoomPercentIfr1 + ')',
                            '-ms-transform': 'scale(' + zoomPercentIfr1 + ', ' + zoomPercentIfr1 + ')',
                            'transform': 'scale(' + zoomPercentIfr1 + ', ' + zoomPercentIfr1 + ')'
                        });
                    }
                    */
                    
                    //zoom the entry point iframe ==> See : Resize entry point iframe for mobile section at the end of js script
                    
                    
                }
                /*=============End Detect browser size to make responsive layout===============*/
                

                initEmbedCodeCopier();

                initEmbedCodeEmailSubmit();

                initCampaignSwitchText('<%= GetLocalizedText("Label.HideCustomizeShowcase") %>', '<%= GetLocalizedText("Label.ShowCustomizeShowcase") %>');
            	
                $('#switch_text').click(function () {
                    $('#customize_showcase_section').toggle();
                    if ($('#customize_showcase_section').is(':hidden')) {
                        SetShowcaseBlockStateSession('-1');
                    } else {
                        SetShowcaseBlockStateSession('1');
                        var iframe = $('.frame1');
                        iframe.each(function () {
                            $(this).src = $(this).src;
                        });
                    }
                    initCampaignSwitchText('<%= GetLocalizedText("Label.HideCustomizeShowcase") %>', '<%= GetLocalizedText("Label.ShowCustomizeShowcase") %>');
				});
            });
			
            
            function SetShowcaseBlockStateSession(value) {
                $.ajax({
                    type: 'POST',
                    url: document.URL,
                    dataType: 'text',
                    data: { ajaxaction: 'setsblockstate', statevalue: value },
                    success: function (responseMsg) {
						
                    }
                });
            }
            
            function LeftColResizer() {
                $('.csp_left_col').find('span.rtIn').each(function() {
                    if ($(this).offset().left + $(this).width() >= rightestSpanPx)
                        rightestSpanPx = $(this).offset().left + $(this).width();
                });

                if (rightestSpanPx > parseInt($('.csp_left_col').offset().left + $('.csp_left_col').width())) {
                    //zoom out- zoomin iframe when hover categories tree
                    var newLeftColWidth = (parseInt($('.csp_left_col').width()) + parseInt(rightestSpanPx) - parseInt($('.csp_left_col').offset().left + $('.csp_left_col').width()) + 10);
                    var pixeltoExpand = rightestSpanPx - parseInt($('.csp_left_col').offset().left + $('.csp_left_col').width()) + 10;
                    var iframeZoomPercentageTo = pixeltoExpand / parseInt(jQuery('#csp_iframe_1').width());
                    var iframeZoomPercentage = 1 - iframeZoomPercentageTo.toFixed(2) - 0.02;
                    var originalRightColWidth = $('.csp_right_col').width();
                    var newRightColWidth = originalRightColWidth - pixeltoExpand;
                    if ($.browser.msie)
                        iframeZoomPercentage = parseInt(iframeZoomPercentage * 100);
                    jQuery('.csp_left_col').hover(function() {
                        if ($(window).width() > 767) {
                            if ($.browser.msie) {
                                jQuery('#csp_iframe_1').css({ 'zoom': iframeZoomPercentage + '%', '-ms-zoom': iframeZoomPercentage + '%' });
                            } else {
                                jQuery('#csp_iframe_1').addClass("iframe_zoom_out");
                                jQuery('#csp_iframe_1').css({
                                    '-moz-transform': 'scale(' + iframeZoomPercentage + ', ' + iframeZoomPercentage + ')',
                                    '-webkit-transform': 'scale(' + iframeZoomPercentage + ', ' + iframeZoomPercentage + ')',
                                    '-o-transform': 'scale(' + iframeZoomPercentage + ', ' + iframeZoomPercentage + ')',
                                    '-ms-transform': 'scale(' + iframeZoomPercentage + ', ' + iframeZoomPercentage + ')',
                                    'transform': 'scale(' + iframeZoomPercentage + ', ' + iframeZoomPercentage + ')'
                                });
                            }
                            jQuery(".csp_right_col").css("width", newRightColWidth + "px");
                            jQuery(".csp_left_col").stop().animate({
                                width: newLeftColWidth + 'px' //"450px"
                            }, 1000, function() {
                                // end animate
                            });
                        }
                    }, function() {
                        if ($(window).width() > 767) {
                            jQuery(".csp_left_col").stop().animate({
                                width: "240px"
                            }, 500, function() {
                                jQuery(".csp_right_col").css("width", originalRightColWidth + "px");
                                //jQuery('#_opt1').removeAttr('style');
                                if ($.browser.msie) {
                                    jQuery('#csp_iframe_1').removeClass("iframe_zoom_out_ie");
                                    jQuery('#csp_iframe_1').css({ 'zoom': '100%', '-ms-zoom': '100%' });
                                } else {
                                    var originalSize = 1;
                                    jQuery('#csp_iframe_1').removeClass("iframe_zoom_out");
                                    jQuery('#csp_iframe_1').css({
                                        '-moz-transform': 'scale(' + originalSize + ', ' + originalSize + ')',
                                        '-webkit-transform': 'scale(' + originalSize + ', ' + originalSize + ')',
                                        '-o-transform': 'scale(' + originalSize + ', ' + originalSize + ')',
                                        '-ms-transform': 'scale(' + originalSize + ', ' + originalSize + ')',
                                        'transform': 'scale(' + originalSize + ', ' + originalSize + ')'
                                    });
                                }
                            });
                        }
                    });
                }
            }

            jQuery(function(){
                var data;
                if (typeof window.addEventListener != 'undefined') {
                    window.addEventListener('message', function (e) {
                        if (e.data.search("_opt1") != -1)
                        {
                            data = e.data;
                            var params = data.split("&");
                            var height  = params[1];
                            height = height.replace("y=", "");
                            $('iframe#_opt1, #csp_iframe_1, #csp_iframe_2').height(height);
                        }
                    }, false);
                }
            });
            
            /* Resize entry point iframe for mobile */
            var int = null;
            function resizeEntryPointIframeForMobile()
            {
                
                if ($("iframe#_opt1").width() != null)
                {
                    var entrypointRightColWidth = parseInt($(window).width() * $('.entrypoint_right_col').width() / 100); //convert from width percent
                    $('iframe#_opt1').width($('.entrypoint_right_col').width() - 35);
                    //int=window.clearInterval(int);
                }

            }
		
            if ($(window).width() <= 767) {
                int=self.setInterval(function() { resizeEntryPointIframeForMobile(); },2000);
            }
		    /* End Resize entry point iframe for mobile */
		</script>
	</telerik:RadScriptBlock>
</asp:Panel>