﻿using System;

namespace TIEKinetix.CspModules.ContentSetup.Entities
{
    public class Category
    {
        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        /// Author: Vu Dinh
        /// 12/7/2012 - 4:59 PM
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        /// Author: Vu Dinh
        /// 12/7/2012 - 4:59 PM
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the parent id.
        /// </summary>
        /// <value>
        /// The parent id.
        /// </value>
        /// Author: Vu Dinh
        /// 12/7/2012 - 5:00 PM
        public int ParentId { get; set; }

        /// <summary>
        /// Gets or sets the theme id.
        /// </summary>
        /// <value>
        /// The theme id.
        /// </value>
        /// Author: Vu Dinh
        /// 12/10/2012 - 8:53 AM
        public int ThemeId { get; set; }

        /// <summary>
        /// Gets or sets the display order.
        /// </summary>
        /// <value>
        /// The display order.
        /// </value>
        /// Author: Vu Dinh
        /// 1/4/2013 - 11:32 AM
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Gets or sets the group.
        /// </summary>
        /// <value>The group.</value>
        public string Group { get; set; }

        /// <summary>
        /// Gets or sets the product content id.
        /// </summary>
        /// <value>The product content id.</value>
        public Guid ProductContentId { get; set; }


    	public override string ToString()
    	{
    		return string.Format("ParentId: {0}, Id: {1}, Text: {2}", ParentId, Id, Text);
    	}
    }
}