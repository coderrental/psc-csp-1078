﻿namespace TIEKinetix.CspModules.ContentSetup.Common
{
    public class Cons
    {
        public const string SETTING_USE_STATUS_SORT_ORDER = "UseStatusSortOrder";
	    public const string SETING_CATEGORY_CONTENT_FIELD_NAME = "CategoryContentTypeFieldName";
	    public const int SUPPLIER_DEFAULT_ID = 12;
        public const string SETTING_CATEGORY_CONTENT_TYPE_ID = "CategoryContenttypeID";
        public const string SETTING_ENABLE_LANGUAGE_DROPDOWN = "EnableLanguageDropdown";
        public const string SETTING_INSTANCE_NAME = "SettingInstanceName";
        public const string SETTING_DOMAIN_NAME = "DomainName";
        public const string SETTING_MODULE_THEME = "ModuleTheme";
        public const string SETTING_DEFAULT_SHOWCASE_TYPE = "DefaultShowcase";
        public const string SETTING_DEFAULT_SHOWCASE_BANNER = "DefaultShowcaseBanner";
        public const string SETTING_DEFAULT_SHOWCASE_EMBED = "DefaultShowcaseEmbed";
        public const string SETTING_REDIRECT_TAB_NAME = "RedirectTabName";
        public const string SETTING_SHOWCASE_CUSTOMIZATION_HIDE_SHOW = "ShowcaseCustomizationHideShow";
        public const string SETTING_HIDE_ENTRYPOINT_TAB = "HideEntryPointTab";
		public const string SETTING_HIDE_CATEGORY_TAB = "HideCategoryTab";
		public const string SETTING_HIDE_BUTTON_SHOWCASE_CUSTOMIZATION = "HideButtonShowcaseCustomization";
        public const string SETTING_SUPPIER_ID = "SupplierId";
		public const string SETTING_HIDE_CATEGORY_NO_CONTENT = "HideCategoryNoContent";
        public const string SETTING_SHOW_PRODUCT_LEVEL = "ShowProductLevel";
        public const string SETTING_SHOW_PRODUCT_LEVEL_CATEGORYCONTENT_T = "ShowProductLevelCategoryContentT";
        public const string SETTING_SHOW_PRODUCT_LEVEL_PRODUCT_T = "ShowProductLevelProductT";

        public const int CATEGORY_CONTENT_TYPE_DEFAULT_ID = 2;
        public const int ROOT_CATEGORY_ID = 1;

        public const string COMPANY_PARAMETER_ENTRYPOINT = "EntryPoint";
        public const string COMPANY_PARAMETER_LAUNCHTYPE = "LaunchType";
        public const string COMPANY_PARAMETER_LINK_TEXT = "SyndicationLinkText";
        public const string COMPANY_PARAMETER_IMAGE_OPTION = "SyndicationImageOption";
        public const string COMPANY_PARAMETER_LINK_IMAGE = "SyndicationLinkImage";
        public const string COMPANY_PARAMETER_IFRAME_TARGET = "SyndicationIFrameTarget";
        public const string CATEGORY_NODE_GROUP_CATEGORY = "GroupCategory";
        public const string CATEGORY_NODE_GROUP_CATEGORYHASPRODUCT = "GroupCategoryHasProduct";
        public const string CATEGORY_NODE_GROUP_PRODUCT = "GroupProduct";

		public const string SETTING_PREVIEW_POPUP_WIDTH = "PreviewPopupWidth";
		public const string SETTING_PREVIEW_POPUP_HEIGHT = "PreviewPopupHeight";

		public const int DEFAULT_PREVIEWPOPUP_WIDTH = 725;
		public const int DEFAULT_PREVIEWPOPUP_HEIGHT = 700;
    }
}