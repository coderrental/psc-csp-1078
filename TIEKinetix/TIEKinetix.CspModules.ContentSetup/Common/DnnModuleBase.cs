﻿using System;
using System.Collections.Generic;
using System.Linq;
using CR.DnnModules.Common;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Localization;
using DotNetNuke.Services.Log.EventLog;
using CR.ContentObjectLibrary.Data;

namespace TIEKinetix.CspModules.ContentSetup.Common
{
    public class DnnModuleBase : PortalModuleBase
    {
        protected EventLogController DnnEventLog;
        private string _localResourceFile;
        protected int CspId, SupplierId, CategoryContentTypeId,DefaultShowcaseType, ProductLevelCategoryContentT = -1, ProductLevelProductT = -1;
        protected string InstanceName, DomainName, ModuleTheme, RedirectTabName;
        protected CspDataContext CspDataContext;
        protected CspUtils CspUtils;
        protected bool EnableLanguageDropdown, ShowProductLevel = false;
        /// <summary>
        /// Handles the Init event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected virtual void Page_Init(object sender, EventArgs e)
        {
            //Init Local Resource file
            _localResourceFile = Utils.GetCommonResourceFile("ContentSetup", LocalResourceFile);
            //Init module title
            ModuleConfiguration.ModuleTitle = GetLocalizedText("Label.ModuleTitle");

            //Init csp datacontext
            CspDataContext = new CspDataContext(Utils.GetConnectionString(PortalId));

            CspUtils = new CspUtils(CspDataContext);

            //Init store procedure
            InitStoreProc();

            //Get CspId
            CspId = Utils.GetIntegrationKey(UserInfo);

            #region [ Get Setting Value ]
            //Get setting value
            if (Settings[Cons.SETTING_CATEGORY_CONTENT_TYPE_ID] != null && Settings[Cons.SETTING_CATEGORY_CONTENT_TYPE_ID].ToString() != string.Empty)
            {
                CategoryContentTypeId = int.Parse(Settings[Cons.SETTING_CATEGORY_CONTENT_TYPE_ID].ToString());
            }
            else
            {
                CategoryContentTypeId = Cons.CATEGORY_CONTENT_TYPE_DEFAULT_ID;
            }

            if (Settings[Cons.SETTING_REDIRECT_TAB_NAME] != null)
            {
                RedirectTabName = Settings[Cons.SETTING_REDIRECT_TAB_NAME].ToString();
            }

            if (Settings[Cons.SETTING_SUPPIER_ID] != null && !string.IsNullOrEmpty(Settings[Cons.SETTING_SUPPIER_ID].ToString()))
            {
                SupplierId = int.Parse(Settings[Cons.SETTING_SUPPIER_ID].ToString());
            }
            else
            {
                SupplierId = Cons.SUPPLIER_DEFAULT_ID;
            }
            

            if (Settings[Cons.SETTING_ENABLE_LANGUAGE_DROPDOWN] != null)
            {
                EnableLanguageDropdown = int.Parse(Settings[Cons.SETTING_ENABLE_LANGUAGE_DROPDOWN].ToString()) > 0;
            }
            else
            {
                EnableLanguageDropdown = false;
            }

            if (Settings[Cons.SETTING_INSTANCE_NAME] != null && Settings[Cons.SETTING_INSTANCE_NAME].ToString() != string.Empty)
            {
                InstanceName = Settings[Cons.SETTING_INSTANCE_NAME].ToString();
            }

            if (Settings[Cons.SETTING_MODULE_THEME] != null && Settings[Cons.SETTING_MODULE_THEME].ToString() != string.Empty)
            {
                ModuleTheme = Settings[Cons.SETTING_MODULE_THEME].ToString();
            }

            if (Settings[Cons.SETTING_DOMAIN_NAME] != null && Settings[Cons.SETTING_DOMAIN_NAME].ToString() != string.Empty)
            {
                DomainName = Settings[Cons.SETTING_DOMAIN_NAME].ToString();
            }

            if (Settings[Cons.SETTING_DEFAULT_SHOWCASE_TYPE] != null)
            {
                DefaultShowcaseType = int.Parse(Settings[Cons.SETTING_DEFAULT_SHOWCASE_TYPE].ToString());
            }

            if (Settings[Cons.SETTING_SHOW_PRODUCT_LEVEL] != null)
            {
                ShowProductLevel = bool.Parse(Settings[Cons.SETTING_SHOW_PRODUCT_LEVEL].ToString());
            }
            if (Settings[Cons.SETTING_SHOW_PRODUCT_LEVEL_CATEGORYCONTENT_T] != null && Settings[Cons.SETTING_SHOW_PRODUCT_LEVEL_CATEGORYCONTENT_T].ToString() != string.Empty)
            {
                ProductLevelCategoryContentT = int.Parse(Settings[Cons.SETTING_SHOW_PRODUCT_LEVEL_CATEGORYCONTENT_T].ToString());
            }
            if (Settings[Cons.SETTING_SHOW_PRODUCT_LEVEL_PRODUCT_T] != null && Settings[Cons.SETTING_SHOW_PRODUCT_LEVEL_PRODUCT_T].ToString() != string.Empty)
            {
                ProductLevelProductT = int.Parse(Settings[Cons.SETTING_SHOW_PRODUCT_LEVEL_PRODUCT_T].ToString());
            }
            #endregion
        }

        /// <summary>
        /// Get localized text for the current portal language
        /// </summary>
        /// <param name="key">Resource Key</param>
        /// <returns>Localized text</returns>
        public string GetLocalizedText(string key)
        {
            return Localization.GetString(key, _localResourceFile);
        }
        public string GetLocalizedText(string key, string language)
        {
            if (string.IsNullOrEmpty(language))
                return GetLocalizedText(key);
            string s = Localization.GetString(key, _localResourceFile, language);
            return string.IsNullOrEmpty(s) ? GetLocalizedText(key) : s;
        }

        private void InitStoreProc()
        {
            string query = "SELECT * FROM sys.objects WHERE type = 'p' AND name = 'GetContentFieldValue'";
            int procCount = CspDataContext.ExecuteQuery<object>(query).Count();
            //Create the stored proc
            if (procCount <= 0)
            {
                try
                {
                    CspDataContext.ExecuteCommand(@"
CREATE PROCEDURE dbo.GetContentFieldValue
	(
	@contentid uniqueidentifier,
	@field nvarchar(MAX)
	)
AS
SET NOCOUNT ON;
    select cf.value_text from content_fields cf
    join content_types_fields ctf on ctf.content_types_fields_Id = cf.content_types_fields_Id
    join content ct on ct.content_Id = cf.content_Id
    where ctf.fieldname = @field and ct.content_Id = @contentid
	RETURN;");
                }
                catch (Exception exc)
                {

                }
            }
        }


        /// <summary>
        /// Write message to Dnn event view
        /// </summary>
        /// <param name="message">message to write</param>
        public void Log(string message)
        {
            DnnEventLog.AddLog("DEBUG", message, PortalSettings, UserId, EventLogController.EventLogType.ADMIN_ALERT);
        }
    }
}