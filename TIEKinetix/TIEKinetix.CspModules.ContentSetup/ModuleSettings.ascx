﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleSettings.ascx.cs" Inherits="TIEKinetix.CspModules.ContentSetup.ModuleSettings" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div>
    <table>
        <tbody>
            <tr>
                <td>
                    Category Content type ID
                </td>
                <td>
                    <asp:TextBox runat="server" Width="400px" ID="tbxCategoryContentTypeId" />
                </td>
            </tr>
            <tr>
                <td>
                    Supplier ID
                </td>
                <td>
                    <asp:TextBox runat="server" Width="400px" ID="tbxSuppierId" />
                </td>
            </tr>
            <tr>
                <td>
                    Enable Language Dropdown
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chbxEnableLanguageDropdown"/>
                </td>
            </tr>
            <tr>
                <td>Hide Entrypoint tab?</td>
                <td>
                    <asp:CheckBox runat="server" ID="chbxHideEntrypointTab" />
                </td>
            </tr>
			 <tr>
                <td>Hide Category tab?</td>
                <td>
                    <asp:CheckBox runat="server" ID="chbxHideCategoryTab" />
                </td>
            </tr>
			<tr>
                <td>Hide category if there is no category content?</td>
                <td>
                    <asp:CheckBox runat="server" ID="chbxHideCateNoContent" />
                </td>
            </tr>
            <tr>
                <td>
                    Portal instance name
                </td>
                <td>
                    <asp:TextBox runat="server" ID="tbxInstanceName"/>
                </td>
            </tr>
            <tr>
                <td>
                    Category Content Field Name (default to Content_Title)
                </td>
                <td>
                    <asp:TextBox runat="server" ID="tbxCategoryContentFieldName"/>
                </td>
            </tr>
            <tr>
                <td>
                    Domain name (e.g http://yourdomain.com)
                </td>
                <td>
                    <asp:TextBox runat="server" ID="tbxDomainName"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Redirect Tab name if missing CspId key
                </td>
                <td>
                    <asp:TextBox runat="server" ID="tbxRedirectTabName"></asp:TextBox>     
                </td>
            </tr>
            <tr>
                <td>
                    Module Theme
                </td>
                <td>
                    <telerik:RadComboBox ID="RcbListTheme" runat="server" />
                </td>
            </tr>
			<tr>
                <td>Preview Popup Width (in pixel)</td>
                <td>
                    <telerik:RadTextBox runat="server" ID="tbPreviewPopupWidth"/>
                </td>
            </tr>
            <tr>
                <td>Preview Popup Height (in pixel)</td>
                <td>
                    <telerik:RadTextBox runat="server" ID="tbPreviewPopupHeight"/>
                </td>
            </tr>
            <tr>
                <td>Default display Showcase Customization</td>
                <td>
                    <telerik:RadComboBox runat="server" ID="cbShowcaseCustomizationShowHide"/>
                </td>
            </tr>
			<tr>
                <td>Hide Button Showcase Customization</td>
                <td>
                    <asp:CheckBox runat="server" ID="chbxHideButtonShowcaseCustomization" />
                </td>
            </tr>
            <tr>
                <td>Show Product Level?</td>
                <td>
                    <asp:CheckBox runat="server" ID="chbxShowProductLevel"/>
                </td>
            </tr>
            <tr class="hide showproductlevel">
                <td>CategoryContent Content type Id</td>
                <td>
                    <telerik:RadTextBox runat="server" ID="tbxProductCategoryContentTypeId"></telerik:RadTextBox>
                </td>
            </tr>
            <tr class="hide showproductlevel">
                <td>Product Content type Id</td>
                <td>
                    <telerik:RadTextBox runat="server" ID="tbxProductContentTypeId"></telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2"><h2>Default showcase for new registed user</h2></td>
            </tr>
            <tr>
                <td><asp:RadioButton runat="server" GroupName="entrypoint" ID="rdNoentry" Text="No Entry Point (Embed Within an iFrame)" /></td>
            </tr>
            <tr>
                <td><asp:RadioButton runat="server" GroupName="entrypoint" ID="rdTextlink" Text="Text Link" /></td>
            </tr>
            <tr>
                <td><asp:RadioButton runat="server" GroupName="entrypoint" ID="rdBanner" Text="Banner" /></td>
            </tr>
            <tr class="bannerHolder hide">
                <td>
                    <ul style="list-style: none;">
                        <li style="list-style: none;"><asp:RadioButton runat="server" GroupName="banner" ID="rdBannerButton" Text="Button (microbar_88x31.jpg)" /></li>
                        <li style="list-style: none;"><asp:RadioButton runat="server" GroupName="banner" ID="rdBannerSquare" Text="Square Banner (squareBanner_125x125.jpg)" /></li>
                        <li style="list-style: none;"><asp:RadioButton runat="server" GroupName="banner" ID="rdBannerHalf" Text="Half Banner (halfBanner_234x60.jpg)" /></li>
                        <li style="list-style: none;"><asp:RadioButton runat="server" GroupName="banner" ID="rdBannerFull" Text="Full Banner (fullbanner_468x60.jpg)" /></li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td><hr /></td>
            </tr>
            <tr>
                <td><asp:RadioButton runat="server" GroupName="embedtype" ID="rdLightbox" Text="Lightbox" /></td>
            </tr>
            <tr>
                <td><asp:RadioButton runat="server" GroupName="embedtype" ID="rdIframe" Text="Frameset (defaults to new window)" /></td>
            </tr>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    jQuery(function () {
        if (jQuery('#<%= rdBanner.ClientID %>').attr("checked") == "checked") {
            jQuery('.bannerHolder').show();
        }

        $('#<%= chbxShowProductLevel.ClientID%>').click(function() {
            if ($(this).attr("checked") == "checked")
                jQuery('.showproductlevel').show();
            else {
                jQuery('.showproductlevel').hide();
            }
        });
        
        if ($('#<%= chbxShowProductLevel.ClientID%>').attr("checked") == "checked") {
            jQuery('.showproductlevel').show();
        }

        jQuery('#<%= rdNoentry.ClientID %>').click(function () {
            jQuery('.bannerHolder').slideUp();
        });
        jQuery('#<%= rdTextlink.ClientID %>').click(function () {
            jQuery('.bannerHolder').slideUp();
        });
        jQuery('#<%= rdBanner.ClientID %>').click(function () {
            jQuery('.bannerHolder').slideDown();
        });
    });
    
</script>