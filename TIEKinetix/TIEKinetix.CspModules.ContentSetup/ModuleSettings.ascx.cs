﻿using System.IO;
using System.Linq;
using Telerik.Web.UI;
using DotNetNuke.Entities.Modules;
using TIEKinetix.CspModules.ContentSetup.Common;

namespace TIEKinetix.CspModules.ContentSetup
{
    public partial class ModuleSettings : ModuleSettingsBase
    {
        /// <summary>
        /// Loads the settings.
        /// </summary>
        /// Author: Vu Dinh
        /// 10/29/2012 - 3:49 PM
        public override void LoadSettings()
        {
            if (TabModuleSettings[Cons.SETTING_CATEGORY_CONTENT_TYPE_ID] != null)
            {
                tbxCategoryContentTypeId.Text = TabModuleSettings[Cons.SETTING_CATEGORY_CONTENT_TYPE_ID].ToString();
            }
	        if (TabModuleSettings[Cons.SETING_CATEGORY_CONTENT_FIELD_NAME] != null)
	        {
		        tbxCategoryContentFieldName.Text = TabModuleSettings[Cons.SETING_CATEGORY_CONTENT_FIELD_NAME].ToString();
	        }
	        else
	        {
		        tbxCategoryContentFieldName.Text = "Content_Title";
	        }
	        if (TabModuleSettings[Cons.SETTING_ENABLE_LANGUAGE_DROPDOWN] != null)
            {
                chbxEnableLanguageDropdown.Checked = int.Parse(TabModuleSettings[Cons.SETTING_ENABLE_LANGUAGE_DROPDOWN].ToString()) > 0;
            }
            if (TabModuleSettings[Cons.SETTING_HIDE_ENTRYPOINT_TAB] != null)
            {
                chbxHideEntrypointTab.Checked = int.Parse(TabModuleSettings[Cons.SETTING_HIDE_ENTRYPOINT_TAB].ToString()) > 0;
            }
			if (TabModuleSettings[Cons.SETTING_HIDE_CATEGORY_TAB] != null)
			{
				chbxHideCategoryTab.Checked = int.Parse(TabModuleSettings[Cons.SETTING_HIDE_CATEGORY_TAB].ToString()) > 0;
			}
			if (TabModuleSettings[Cons.SETTING_HIDE_BUTTON_SHOWCASE_CUSTOMIZATION] != null)
			{
				chbxHideButtonShowcaseCustomization.Checked = int.Parse(TabModuleSettings[Cons.SETTING_HIDE_BUTTON_SHOWCASE_CUSTOMIZATION].ToString()) > 0;
			}
			if (TabModuleSettings[Cons.SETTING_HIDE_CATEGORY_NO_CONTENT] != null)
			{
				chbxHideCateNoContent.Checked = int.Parse(TabModuleSettings[Cons.SETTING_HIDE_CATEGORY_NO_CONTENT].ToString()) > 0;
			}
            if (TabModuleSettings[Cons.SETTING_INSTANCE_NAME] != null)
            {
                tbxInstanceName.Text = TabModuleSettings[Cons.SETTING_INSTANCE_NAME].ToString();
            }
            if (TabModuleSettings[Cons.SETTING_DOMAIN_NAME] != null)
            {
                tbxDomainName.Text = TabModuleSettings[Cons.SETTING_DOMAIN_NAME].ToString();
            }
            if(TabModuleSettings[Cons.SETTING_REDIRECT_TAB_NAME] != null)
            {
                tbxRedirectTabName.Text = TabModuleSettings[Cons.SETTING_REDIRECT_TAB_NAME].ToString();
            }
            if (TabModuleSettings[Cons.SETTING_SUPPIER_ID] != null)
            {
                tbxSuppierId.Text = TabModuleSettings[Cons.SETTING_SUPPIER_ID].ToString();
            }

			tbPreviewPopupHeight.Text = Settings[Cons.SETTING_PREVIEW_POPUP_HEIGHT] != null ? Settings[Cons.SETTING_PREVIEW_POPUP_HEIGHT].ToString() : Cons.DEFAULT_PREVIEWPOPUP_HEIGHT.ToString();
			tbPreviewPopupWidth.Text = Settings[Cons.SETTING_PREVIEW_POPUP_WIDTH] != null ? Settings[Cons.SETTING_PREVIEW_POPUP_WIDTH].ToString() : Cons.DEFAULT_PREVIEWPOPUP_WIDTH.ToString();

            /**
             * load list module theme
             */
            var selectTheme = string.Empty;
            if (TabModuleSettings[Cons.SETTING_MODULE_THEME] != null)
            {
                selectTheme = TabModuleSettings[Cons.SETTING_MODULE_THEME].ToString();
            }
            foreach (string file in Directory.GetFiles(Server.MapPath(ControlPath)))
            {
                string[] files = file.Split('\\');
                if(files.Last().Split('.').Last().Equals("css") && !files.Last().Equals("Module.css"))
                    RcbListTheme.Items.Add(new RadComboBoxItem(files.Last(), files.Last()));
            }
            if (!string.IsNullOrEmpty(selectTheme))
                RcbListTheme.FindItemByValue(selectTheme).Selected = true;
            /**
             * end load list module theme
             */

            // Load showcase customization show - hide
            cbShowcaseCustomizationShowHide.Items.Add(new RadComboBoxItem("Show","1"));
            cbShowcaseCustomizationShowHide.Items.Add(new RadComboBoxItem("Hide", "2"));
            if (TabModuleSettings[Cons.SETTING_SHOWCASE_CUSTOMIZATION_HIDE_SHOW] != null)
                cbShowcaseCustomizationShowHide.Items.FindItemByValue(TabModuleSettings[Cons.SETTING_SHOWCASE_CUSTOMIZATION_HIDE_SHOW].ToString()).Selected = true;

            #region [ LOAD SETTINGS FOR DEFAULT SHOWCASE ]

            rdNoentry.Checked = true;
            rdBannerButton.Checked = true;
            rdLightbox.Checked = true;

            if (TabModuleSettings[Cons.SETTING_DEFAULT_SHOWCASE_TYPE] != null)
            {
                var showcaseType = int.Parse(TabModuleSettings[Cons.SETTING_DEFAULT_SHOWCASE_TYPE].ToString());
                switch (showcaseType)
                {
                    case  1:
                        rdNoentry.Checked = true;
                        break;
                    case 2:
                        rdTextlink.Checked = true;
                        break;
                    case 3:
                        rdBanner.Checked = true;
                        break;
                }

            }
            if (TabModuleSettings[Cons.SETTING_DEFAULT_SHOWCASE_BANNER] != null)
            {
                var bannerType = int.Parse(TabModuleSettings[Cons.SETTING_DEFAULT_SHOWCASE_BANNER].ToString());
                switch (bannerType)
                {
                    case 1:
                        rdBannerButton.Checked = true;
                        break;
                    case 2:
                        rdBannerSquare.Checked = true;
                        break;
                    case 3:
                        rdBannerHalf.Checked = true;
                        break;
                    case 4:
                        rdBannerFull.Checked = true;
                        break;
                }
            }

            if (TabModuleSettings[Cons.SETTING_DEFAULT_SHOWCASE_EMBED] != null)
            {
                var embedType = int.Parse(TabModuleSettings[Cons.SETTING_DEFAULT_SHOWCASE_EMBED].ToString());
                if (embedType == 1)
                {
                    rdLightbox.Checked = true;
                }
                if (embedType == 2)
                {
                    rdIframe.Checked = true;
                }
            }
            #endregion

            #region [ Show Product Level ]
            if (TabModuleSettings[Cons.SETTING_SHOW_PRODUCT_LEVEL] != null)
            {
                chbxShowProductLevel.Checked = bool.Parse(TabModuleSettings[Cons.SETTING_SHOW_PRODUCT_LEVEL].ToString());
            }
            if (TabModuleSettings[Cons.SETTING_SHOW_PRODUCT_LEVEL_CATEGORYCONTENT_T] != null && TabModuleSettings[Cons.SETTING_SHOW_PRODUCT_LEVEL_CATEGORYCONTENT_T].ToString() != string.Empty)
            {
                tbxProductCategoryContentTypeId.Text =TabModuleSettings[Cons.SETTING_SHOW_PRODUCT_LEVEL_CATEGORYCONTENT_T].ToString();
            }
            if (TabModuleSettings[Cons.SETTING_SHOW_PRODUCT_LEVEL_PRODUCT_T] != null && TabModuleSettings[Cons.SETTING_SHOW_PRODUCT_LEVEL_PRODUCT_T].ToString() != string.Empty)
            {
                tbxProductContentTypeId.Text = TabModuleSettings[Cons.SETTING_SHOW_PRODUCT_LEVEL_PRODUCT_T].ToString();
            }
            #endregion
        }

        /// <summary>
        /// Updates the settings.
        /// </summary>
        /// Author: Vu Dinh
        /// 10/29/2012 - 3:52 PM
        public override void UpdateSettings()
        {
            var moduleController = new ModuleController();
            moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_CATEGORY_CONTENT_TYPE_ID, tbxCategoryContentTypeId.Text);
            moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_ENABLE_LANGUAGE_DROPDOWN, chbxEnableLanguageDropdown.Checked ? "1" : "0");
            moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_HIDE_ENTRYPOINT_TAB, chbxHideEntrypointTab.Checked ? "1" : "0");
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_HIDE_CATEGORY_TAB, chbxHideCategoryTab.Checked ? "1" : "0");
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_HIDE_BUTTON_SHOWCASE_CUSTOMIZATION, chbxHideButtonShowcaseCustomization.Checked ? "1" : "0");
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_HIDE_CATEGORY_NO_CONTENT, chbxHideCateNoContent.Checked ? "1" : "0");
            moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_INSTANCE_NAME, tbxInstanceName.Text);
            moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_DOMAIN_NAME, tbxDomainName.Text);
            moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_MODULE_THEME, RcbListTheme.SelectedValue);
            moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_REDIRECT_TAB_NAME, tbxRedirectTabName.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_PREVIEW_POPUP_HEIGHT, tbPreviewPopupHeight.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_PREVIEW_POPUP_WIDTH, tbPreviewPopupWidth.Text);
            moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_SHOWCASE_CUSTOMIZATION_HIDE_SHOW, cbShowcaseCustomizationShowHide.SelectedValue);
            moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_SUPPIER_ID, tbxSuppierId.Text);
	        moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETING_CATEGORY_CONTENT_FIELD_NAME, tbxCategoryContentFieldName.Text);
            moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_SHOW_PRODUCT_LEVEL, chbxShowProductLevel.Checked.ToString());
            moduleController.UpdateTabModuleSetting(TabModuleId,Cons.SETTING_SHOW_PRODUCT_LEVEL_CATEGORYCONTENT_T, tbxProductCategoryContentTypeId.Text);
            moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_SHOW_PRODUCT_LEVEL_PRODUCT_T, tbxProductContentTypeId.Text);
            #region [ UPDATE SETTINGs FOR DEFAULT SHOWCASE]
            if (rdNoentry.Checked)
            {
                moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_DEFAULT_SHOWCASE_TYPE, "1");
            }

            if (rdLightbox.Checked)
            {
                moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_DEFAULT_SHOWCASE_EMBED,"1");
            }
            if (rdIframe.Checked)
            {
                moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_DEFAULT_SHOWCASE_EMBED, "2");
            }

            if (rdTextlink.Checked)
            {
                moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_DEFAULT_SHOWCASE_TYPE, "2");
            }
            if (rdBanner.Checked)
            {
                moduleController.UpdateTabModuleSetting(TabModuleId,Cons.SETTING_DEFAULT_SHOWCASE_TYPE, "3");
            }
            if (rdBannerButton.Checked)
            {
                moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_DEFAULT_SHOWCASE_BANNER, "1");
            }
            if (rdBannerSquare.Checked)
            {
                moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_DEFAULT_SHOWCASE_BANNER, "2");
            }
            if (rdBannerHalf.Checked)
            {
                moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_DEFAULT_SHOWCASE_BANNER, "3");
            }
            if (rdBannerFull.Checked)
            {
                moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_DEFAULT_SHOWCASE_BANNER, "4");
            }

            
            #endregion

            ModuleController.SynchronizeModule(ModuleId);
        }
    }
}