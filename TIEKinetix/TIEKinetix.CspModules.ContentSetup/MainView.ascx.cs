﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CR.DnnModules.Common;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Data;
using TIEKinetix.CspModules.ContentSetup.Common;
using TIEKinetix.CspModules.ContentSetup.Entities;
using CR.ContentObjectLibrary.Data;
using Telerik.Web.UI;
using DotNetNuke.Services.Mail;

namespace TIEKinetix.CspModules.ContentSetup
{
    public partial class MainView : DnnModuleBase
    {
        private companies_consumer _companiesConsumer;
        private int _langId;
	    private string _imageType, _categoryContentFieldName = "Content_Title";
        private List<int> _themeIds;
        private List<int> _childCatIds; 
        private bool _forceOut;
        private long _ticks;

        /// <summary>
        /// Handles the Init event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 12/7/2012 - 2:45 PM
        protected override void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
			//Ajax request
			if (!String.IsNullOrEmpty(Request.Params["ajaxaction"]))
			{
				if (Request.Params["ajaxaction"] == "emailembedcode")
					SendEmbedCodeByEmail();
				else if (Request.Params["ajaxaction"] == "setsblockstate")
				{
					Session["SBlockState"] = Request.Params["statevalue"];
				}
				return;
			}

            // Force IE8 browser render as IE 7 or IE 9 mode
            var keyword = new HtmlMeta { Content = "IE=7,IE=9", HttpEquiv = "X-UA-Compatible" };
            Page.Header.Controls.Add(keyword);

			var errorText = new StringBuilder();
            //Check for missing CSPID
			if (CspId == -1)
            {
				_forceOut = true;
				errorText.Append(string.Format("<p>{0}</p>", GetLocalizedText("Label.MissingIntegrationKey")));
				if (!HaveAdminPermission())
					GotoRedirectTabId(String.Empty);
            }
			else if (!HaveAdminPermission())
			{
                if (CspDataContext.companies.SingleOrDefault(a => a.companies_Id == CspId) == null)
                {
                    GotoRedirectTabId(String.Empty);
                }
                else
                {
                    DnnDataContext dnnContext = new DnnDataContext(Config.GetConnectionString());
                    Guid missingFormId = Utils.MissingMandatorySubscription(dnnContext, CspDataContext, UserInfo, PortalId);
                    if (missingFormId != Guid.Empty)
                    {
                        int missingStep = Utils.FormStepNo(dnnContext, CspDataContext, missingFormId, PortalId);
                        GotoRedirectTabId(String.Format("step={0}", missingStep));
                    }
                }
				
			}

            //Check for missing setting
            if (string.IsNullOrEmpty(InstanceName))
            {
                errorText.Append(string.Format("<p>{0}</p>", GetLocalizedText("Label.MissingPortalInstanceName")));
                _forceOut = true;
            }
            if (string.IsNullOrEmpty(ModuleTheme))
            {
                errorText.Append(string.Format("<p>{0}</p>", GetLocalizedText("Label.MissingModuleTheme")));
                _forceOut = true;
            }
            if (DefaultShowcaseType <=0)
            {
                errorText.Append(string.Format("<p>{0}</p>", GetLocalizedText("Label.MissingDefaultShowcaseType")));
                _forceOut = true;
            }
            if (ShowProductLevel)
            {
                if (ProductLevelCategoryContentT == -1 || ProductLevelProductT == -1)
                {
                    errorText.Append(string.Format("<p>{0}</p>", GetLocalizedText("Label.MissingProductLevelContentType")));
                    _forceOut = true;
                }
            }
       
            if (_forceOut)
            {
                errorHolder.Visible = true;
                errorHolder.InnerHtml = errorText.ToString();
                panelMain.Visible = false;
            }
            else
            {
                //Init Localize controls
                InitLocalize();

                //Init Language combobox
                InitLanguage();

                //Init tab strip
                InitTabStrip();

                //Init categories tree
                InitCategoriesTree();

                //Init default showcase
                InitDefaultShowcase();
            }
        }
        

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 12/7/2012 - 2:45 PM
        protected void Page_Load(object sender, EventArgs e)
        {
            if (_forceOut) return;

			if (!String.IsNullOrEmpty(Request.Params["ajaxaction"]))
				return;

			//zclip to create copy to clipboard function
            Page.ClientScript.RegisterClientScriptInclude("zclip", ControlPath + "Js/jquery.zclip.min.js");

            //Init Module Theme
            InitTheme();

            if (EnableLanguageDropdown)
            {
                _langId = int.Parse(cbxLanguages.SelectedValue);    
            }
            _imageType =  "image/bmp,image/jpeg,image/jpg,image/png,image/gif,image/pjpeg,image/x-png";

            //Translate categories node pair with selected language
            TranslateCategoriesTreeNode();
        }

        /// <summary>
        /// Inits the language.
        /// </summary>
        /// Author: Vu Dinh
        /// 12/8/2012 - 6:45 AM
        private void InitLanguage()
        {
             _companiesConsumer = CspDataContext.companies_consumers.FirstOrDefault(a => a.companies_Id == CspId);
            if (EnableLanguageDropdown)
            {
                var languages = CspDataContext.languages.Where(a => a.active == true).ToList();

                #region ltu 4/8/14 Handle an exception: a language is active, but shouldn't be shown in the portal
                // note: there is a flag in the language table called "ShowInBrowser", this flag is nullable. The value has to be true in order to hide this language
                List<int> tmp = new List<int>();

                try
                {
                    tmp = CspDataContext.ExecuteQuery<int>("select 	languages_Id from languages where showinportal is not null and showinportal = 0").ToList();
                    if (tmp.Any())
                        languages = languages.Where(a => tmp.All(b => b != a.languages_Id)).ToList();
                }
                catch
                {
                    // fail due to showinportal isnt a valid column in some databases
                }
                #endregion

                foreach (var language in languages)
                {
                    cbxLanguages.Items.Add(new RadComboBoxItem(language.description, language.languages_Id.ToString()));
                }

                //Get default language
                if (_companiesConsumer != null && !IsPostBack)
                {
                    RadComboBoxItem tmpComboBoxItem = cbxLanguages.FindItemByValue(_companiesConsumer.default_language_Id.ToString());
                    if (tmpComboBoxItem != null)
                        cbxLanguages.FindItemByValue(_companiesConsumer.default_language_Id.ToString()).Selected = true;
                    else
                    {
                        // try the fallback
                        tmpComboBoxItem = cbxLanguages.FindItemByValue(_companiesConsumer.fallback_language_Id.ToString());
                        if (tmpComboBoxItem != null) cbxLanguages.FindItemByValue(_companiesConsumer.default_language_Id.ToString()).Selected = true;
                        else
                            cbxLanguages.Items.First().Selected = true;
                    }
                }
                
            }
            else
            {
                panelLanguage.Visible = false;
                _langId = _companiesConsumer != null ?  (_companiesConsumer.default_language_Id.HasValue ? _companiesConsumer.default_language_Id.Value : 1) : 1 ;
            }
        }


        /// <summary>
        /// Inits the tab strip.
        /// </summary>
        /// Author: Vu Dinh
        /// 12/8/2012 - 3:46 PM
        private void InitTabStrip()
        {
			if (!HideCategoryTab())
			{
				tabMain.Tabs.Add(new RadTab {Text = GetLocalizedText("Tab.Category")});
				tabMain.SelectedIndex = 0;
				multiMainTab.SelectedIndex = 0;
			}
        	if (!HideEntrypointTab())
                tabMain.Tabs.Add(new RadTab{Text = GetLocalizedText("Tab.EntryPoint")});

			if(HideCategoryTab()&&!HideEntrypointTab())
			{
				tabMain.SelectedIndex = 1;
				multiMainTab.SelectedIndex = 1;
			}
        }

        /// <summary>
        /// Inits ModuleTheme.
        /// </summary>
        /// Author: Phat Ngo
        /// 12/29/2012 - 09:00 PM
        private void InitTheme()
        {
            HtmlLink link = new HtmlLink();
            if (Settings[Cons.SETTING_MODULE_THEME] != null && Settings[Cons.SETTING_MODULE_THEME].ToString() != string.Empty)
            {
                Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel='stylesheet' href='{0}/{1}' type='text/css' />", ControlPath, Settings[Cons.SETTING_MODULE_THEME].ToString())));
            } else
            {
                Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel='stylesheet' href='{0}/default.css' type='text/css' />", ControlPath)));
            }
            
        }

        /// <summary>
        /// Inits the default showcase.
        /// </summary>
        /// Author: Vu Dinh
        /// 12/31/2012 - 11:42 AM
        private void InitDefaultShowcase()
        {
            var entryPointValue = CspUtils.GetCompanyParameter(CspId,Cons.COMPANY_PARAMETER_ENTRYPOINT);
            if (entryPointValue != string.Empty)
                return; //Return if this user already has entry point value
            //Otherwise, add default showcase value to this user
            switch (DefaultShowcaseType)
            {
                case 1: //No Entry Point (Embed Within an iFrame)
                    {
                        var companyParameterEntrypoinType = GetOrCreateCompanyParamType(Cons.COMPANY_PARAMETER_ENTRYPOINT);
                        CreateOrUpdateCompanyParam(companyParameterEntrypoinType.companies_parameter_types_Id, "4");
                        break;
                    }
                case 2: //Text link
                    {
                        //Update entry value
                        var companyParameterEntrypoinType = GetOrCreateCompanyParamType(Cons.COMPANY_PARAMETER_ENTRYPOINT);
                        CreateOrUpdateCompanyParam(companyParameterEntrypoinType.companies_parameter_types_Id, "1");

                        //Update text link
                        var textLinkType = GetOrCreateCompanyParamType(Cons.COMPANY_PARAMETER_LINK_TEXT);
                        CreateOrUpdateCompanyParam(textLinkType.companies_parameter_types_Id, GetLocalizedText("Entrypoint.TextLinkInput"));

                        //Update embed type
                        var companyParamLaunchtype = GetOrCreateCompanyParamType(Cons.COMPANY_PARAMETER_LAUNCHTYPE);
                        CreateOrUpdateCompanyParam(companyParamLaunchtype.companies_parameter_types_Id, Settings[Cons.SETTING_DEFAULT_SHOWCASE_EMBED].ToString());
                        break;
                    }
                case 3: //Banner
                    {
                        //Update entry value
                        var companyParameterEntrypoinType = GetOrCreateCompanyParamType(Cons.COMPANY_PARAMETER_ENTRYPOINT);
                        CreateOrUpdateCompanyParam(companyParameterEntrypoinType.companies_parameter_types_Id, "2");

                        //Update banner value
                        var imageOptionType = GetOrCreateCompanyParamType(Cons.COMPANY_PARAMETER_IMAGE_OPTION);
                        CreateOrUpdateCompanyParam(imageOptionType.companies_parameter_types_Id, Settings[Cons.SETTING_DEFAULT_SHOWCASE_BANNER].ToString());

                        var imageCustomlinkType = GetOrCreateCompanyParamType(Cons.COMPANY_PARAMETER_LINK_IMAGE);
                        string path = string.Empty;
                        switch (int.Parse(Settings[Cons.SETTING_DEFAULT_SHOWCASE_BANNER].ToString()))
                        {
                            case 1:
                                path = GetBannerButtonLink();
                                break;
                            case 2:
                                path = GetBannerSquareLink();
                                break;
                            case 3:
                                path = GetBannerHalfLink();
                                break;
                            case 4:
                                path = GetBannerFullLink();
                                break;
                        }
                        if (path != string.Empty)
                            CreateOrUpdateCompanyParam(imageCustomlinkType.companies_parameter_types_Id, path);


                        //Update embed type
                        var companyParamLaunchtype = GetOrCreateCompanyParamType(Cons.COMPANY_PARAMETER_LAUNCHTYPE);
                        CreateOrUpdateCompanyParam(companyParamLaunchtype.companies_parameter_types_Id, Settings[Cons.SETTING_DEFAULT_SHOWCASE_EMBED].ToString());
                        break;
                    }
            }

        }

        /// <summary>
        /// Translates the categories tree node.
        /// </summary>
        /// Author: Vu Dinh
        /// 12/8/2012 - 7:16 AM
        /// <update>
        /// @ducuytran - 2013/05/10
		/// + If setting "Hide category if there is no category content?" is checked:
		/// _ Remove category tree node which has no category content
        /// </update>
        private void TranslateCategoriesTreeNode()
        {
            var treeControls = treeContainer.Controls;
            foreach (var treeControl in treeControls)
            {
                var tree = treeControl as RadTreeView;
                if (tree != null)
                {
                    foreach (var node in tree.GetAllNodes())
                    {
						if (node.ParentNode == null && tree.GetAllNodes().Count(a => a.ParentNode == null) == 1)
						{
							continue;
						}
                        if (node.Target == Cons.CATEGORY_NODE_GROUP_PRODUCT)
                        {
                            continue;
                        }

                        var catTran = CspUtils.GetContentsFromCategoryId(_langId, CategoryContentTypeId, SupplierId, int.Parse(node.Value)).FirstOrDefault(a => a.stage_Id == 50);
                        if (catTran != null)
                        {

                            var text = CspUtils.GetContentFieldValue(catTran, _categoryContentFieldName);
                            //According to setting, remove node without category content or not
                            if (String.IsNullOrEmpty(text) && Settings[Cons.SETTING_HIDE_CATEGORY_NO_CONTENT] != null && int.Parse(Settings[Cons.SETTING_HIDE_CATEGORY_NO_CONTENT].ToString()) == 1 && node.Target != Cons.CATEGORY_NODE_GROUP_CATEGORYHASPRODUCT)
                            {
                                node.Remove();
                                continue;
                            }
                            node.Text = text != string.Empty ? text : node.Text;
                            continue;
                        }
                        if (Settings[Cons.SETTING_HIDE_CATEGORY_NO_CONTENT] != null && int.Parse(Settings[Cons.SETTING_HIDE_CATEGORY_NO_CONTENT].ToString()) == 1 && node.Target != Cons.CATEGORY_NODE_GROUP_CATEGORYHASPRODUCT)
                        {
                            node.Remove();
                        }

                    }
                }
            }
        }

        /// <summary>
        /// Inits the localize.
        /// </summary>
        /// Author: Vu Dinh
        /// 12/7/2012 - 10:47 PM
        private void InitLocalize()
        {
            btnCatSave.Text = GetLocalizedText("Label.CategoriesSave");
            btnSaveEntrypoint.Text = GetLocalizedText("Label.CategoriesSave");
            lblLanguages.Text = GetLocalizedText("Label.SelectLanguage");
            lblEntrypointHeader.Text = GetLocalizedText("Label.EntrypointHeader");
            lblEntrypointStep.Text = GetLocalizedText("Label.EntrypointStep");
            lblStep1Header.Text = GetLocalizedText("Entrypoint.Step1Header");
            lblStep2Header.Text = GetLocalizedText("Entrypoint.Step2Header");

	        if (Settings[Cons.SETING_CATEGORY_CONTENT_FIELD_NAME] != null)
		        _categoryContentFieldName = Settings[Cons.SETING_CATEGORY_CONTENT_FIELD_NAME].ToString();
	        if (string.IsNullOrEmpty(_categoryContentFieldName))
		        _categoryContentFieldName = "Content_Title";

            //localize images, so user can change the images
           
        }

        /// <summary>
        /// Inits the categories tree.
        /// </summary>
        /// Author: Vu Dinh
        /// 12/7/2012 - 3:49 PM
        private void InitCategoriesTree()
        {
            _themeIds = GetThemeIds();
            var treeCount = 0;
            foreach (var themeId in _themeIds)
            {
                var categories = GetAllCategories(themeId);
                if (categories.Any())
                {
                    var treeView = new RadTreeView
                    {
                        ID = "treeview" + themeId,
                        CheckBoxes = true,
                        DataTextField = "Text",
                        DataFieldID = "Id",
                        DataFieldParentID = "ParentId",
                        DataValueField = "Id",
                        SingleExpandPath = false,
                        DataSource = categories,
                        CheckChildNodes = true,
                        TriStateCheckBoxes = true,
                        OnClientNodeClicked = "ClientNodeClicked",
                    };
                    treeView.DataBindings.Add(new RadTreeNodeBinding
                    {
                        Expanded = true,
                        ValueField = "Id",
                        TargetField = "Group",
                        ContentCssClassField = "ProductContentId"
                    });
                    treeView.DataBind();
                    treeContainer.Controls.Add(treeView);

                    //Set check and uncheck node
                    var categoryIdsToCheck = GetCheckCategoryIds(themeId);
                    foreach (var node in treeView.GetAllNodes())
                    {
                        node.Checked = (node.Value != string.Empty) && categoryIdsToCheck.Contains(int.Parse(node.Value));
                        node.Category = themeId.ToString();
                    }

                    if (ShowProductLevel)
                    {
                        var listProductToCheck = CspDataContext.ExecuteQuery<Guid>("SELECT ContentID FROM Sku_Company_ContentGUID_Include WHERE CompanyID = {0}", CspId).ToList();
                        foreach (var node in treeView.GetAllNodes().Where(a => a.Target == Cons.CATEGORY_NODE_GROUP_PRODUCT ))
                        {
                            var productId = Guid.Parse(node.ContentCssClass);
                            if (listProductToCheck.Contains(productId))
                            {
                                node.Checked = true;
                            }
                        }
                    }

                    treeCount++;
                }
            }
            if (treeCount == 1)
            {
                foreach (var treeControl in treeContainer.Controls)
                {
                    var tree = treeControl as RadTreeView;
                    if (tree != null)
                    {
                        var rootNode = tree.FindNodeByValue("1");
                        if (rootNode != null)
                            rootNode.Text = GetLocalizedText("Label.RootCategory");
                        break;
                    }
                }
            }
            
        }

        /// <summary>
        /// Gets all categories.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 12/7/2012 - 4:14 PM
        private IEnumerable<Category> GetAllCategories(int themeId)
        {
            var categories = new List<Category>();
            var query = @"select category_Id, tc.themes_Id from themes_categories tc
join categories c on c.categoryId = tc.category_Id
where c.active = 1 and c.IsSubscribable = 1 and tc.themes_Id ={0}";
            query = string.Format(query, themeId);
            using (var connection = new SqlConnection(Utils.GetConnectionString(PortalId)))
            {
                connection.Open();
                using (var command = new SqlCommand(query,connection))
                {
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        var category = CspDataContext.categories.SingleOrDefault(a => a.categoryId == int.Parse(reader["category_Id"].ToString()));
                        if (category != null)
                        {
                            categories.Add(new Category
                                                     {
                                                         Id = category.categoryId ,
                                                         ParentId = GetSkipParentCategoryId(category), 
                                                         Text = category.categoryText,
                                                         DisplayOrder = category.DisplayOrder.HasValue ? category.DisplayOrder.Value : 0,
                                                         Group = Cons.CATEGORY_NODE_GROUP_CATEGORY
                                                     });
                        }
                    }
                }
            }

            if (ShowProductLevel)
            {
                var temp = new List<Category>();
                foreach (var category in categories)
                {
                    var products = CspUtils.GetContentsFromCategoryId(_langId, ProductLevelProductT, SupplierId,category.Id).Where(a => a.stage_Id == 50).ToList();
                    if (products.Any())
                        category.Group = Cons.CATEGORY_NODE_GROUP_CATEGORYHASPRODUCT;
                    foreach (var product in products)
                    {
                        temp.Add(new Category
                            {
                                Id = (int)DateTime.Now.Ticks,
                                DisplayOrder = 0,
                                Text = CspUtils.GetContentFieldValue(product,"Content_Title"),
                                ParentId = category.Id,
                                Group = Cons.CATEGORY_NODE_GROUP_PRODUCT,
                                ProductContentId = product.content_Id
                            });
                    }
                }
                categories.AddRange(temp);
            }
            return categories.OrderBy(a => a.DisplayOrder).ThenByDescending(a => a.Id);
        }

        /// <summary>
        /// Gets the skip parent category.
        /// </summary>
        /// <param name="cat">The cat.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 1/3/2013 - 4:32 PM
        private int GetSkipParentCategoryId(category cat)
        {
            var temp = cat;
            while (temp !=null)
            {
                var parentCategory = CspDataContext.categories.SingleOrDefault(a => a.categoryId == temp.parentId);
                if (parentCategory != null && parentCategory.IsSubscribable == true)
                {
                    return parentCategory.categoryId;
                }
                temp = parentCategory;
            }
            return 0;
        }

        /// <summary>
        /// Gets the check category ids.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 12/7/2012 - 5:19 PM
        private List<int> GetCheckCategoryIds(int themeId)
        {
            var categoryIds = new List<int>();
            var query = @"select category_Id from consumers_themes ct
join categories cat on cat.categoryId = ct.category_Id
where ct.consumer_Id = {0} and ct.supplier_Id = {1} and ct.themes_Id = {2} and cat.active = 1 and cat.IsSubscribable = 1";
            query = string.Format(query, CspId, SupplierId, themeId);
            
            using (var con = new SqlConnection(Utils.GetConnectionString(PortalId)))
            {
                con.Open();
                using (var command = new SqlCommand(query, con))
                {
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        categoryIds.Add(int.Parse(reader["category_Id"].ToString()));
                    }
                }
            }
           
            return categoryIds;
        }

        /// <summary>
        /// Gets the type of the or create company param.
        /// </summary>
        /// <param name="paramtypeName">Name of the paramtype.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 12/10/2012 - 6:55 AM
        private companies_parameter_type GetOrCreateCompanyParamType(string paramtypeName)
        {
            var paramType = CspDataContext.companies_parameter_types.SingleOrDefault(a => a.parametername == paramtypeName);
            if (paramType == null)
            {
                paramType = new companies_parameter_type
                                {
                                    parametername = paramtypeName,
                                    sortorder = 0
                                };
                CspDataContext.companies_parameter_types.InsertOnSubmit(paramType);
                CspDataContext.SubmitChanges(ConflictMode.ContinueOnConflict);
            }
            return paramType;
        }

        /// <summary>
        /// Gets the parent categories id.
        /// </summary>
        /// <param name="categoryId">The category id.</param>
        /// <param name="list">The list.</param>
        /// <returns>List{System.Int32}.</returns>
        private List<int> GetParentCategoriesId(int categoryId, List<int> list )
        {
            var category = CspDataContext.categories.SingleOrDefault(a => a.categoryId == categoryId && a.active.Value);
            if (category != null)
            {
                list.Add(categoryId);
                if (category.parentId.HasValue)
                    return GetParentCategoriesId(category.parentId.Value, list);
            }
            return list;
        }


        /// <summary>
        /// Creates the or update company param.
        /// </summary>
        /// <param name="paramtypeId">The paramtype id.</param>
        /// <param name="valueText">The value text.</param>
        /// Author: Vu Dinh
        /// 12/21/2012 - 2:39 PM
        private void CreateOrUpdateCompanyParam(int paramtypeId,string valueText)
        {
            var param = CspDataContext.companies_parameters.SingleOrDefault(a => a.companies_Id == CspId && a.companies_parameter_types_Id == paramtypeId);
            if (param == null)
            {
                param = new companies_parameter
                            {
                                companies_Id = CspId,
                                value_text = valueText,
                                companies_parameter_types_Id = paramtypeId,
                                value_boolean = null,
                                value_date = null,
                                value_double = null,
                                value_integer = null
                            };
                CspDataContext.companies_parameters.InsertOnSubmit(param);
            }
            else
            {
                param.value_text = valueText;
            }
            CspDataContext.SubmitChanges(ConflictMode.ContinueOnConflict);
        }

        /// <summary>
        /// Gets the theme ids.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 12/10/2012 - 8:47 AM
        private List<int> GetThemeIds()
        {
            return CspDataContext.companies_themes.Where(a => a.companies_Id == CspId).Select(a => a.themes_Id.HasValue ? a.themes_Id.Value : 0).ToList();
        }

        /// <summary>
        /// Uploads the banner.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 12/10/2012 - 6:05 PM
        private string UploadBanner()
        {
            var fileName = bannerUpload.FileName;
            if (string.IsNullOrEmpty(fileName))
            {
                return string.Empty;
            }

            if (!_imageType.Contains(bannerUpload.PostedFile.ContentType))
            {
                bannerUploadValidator.Text = GetLocalizedText("Label.InvalidImageType");
                bannerUploadValidator.IsValid = false;
                return string.Empty;
            }
                    

            var pathToUpload = Server.MapPath(PortalSettings.HomeDirectory + "Images");
            fileName = string.Format("{0}_{1}{2}", UserInfo.UserID, DateTime.Now.Ticks, fileName);
            bannerUpload.SaveAs(string.Format("{0}\\{1}", pathToUpload, fileName));
                    
            //resize image
            var width = int.Parse(!string.IsNullOrEmpty(GetLocalizedText("Banner.ResizeWidth_" + PortalId)) ? GetLocalizedText("Banner.ResizeWidth_" + PortalId) : GetLocalizedText("Banner.ResizeWidth"));
            var height = int.Parse(!string.IsNullOrEmpty(GetLocalizedText("Banner.ResizeHeight_" + PortalId)) ? GetLocalizedText("Banner.ResizeHeight_" + PortalId) : GetLocalizedText("Banner.ResizeHeight"));
            fileName = Utils.ResizeImage(pathToUpload, fileName, width, height);

            if (string.IsNullOrEmpty(DomainName))
            {
                var strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                DomainName = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, string.Empty);
            }

            return DomainName + PortalSettings.HomeDirectory + "Images/" + fileName;
        }

        /// <summary>
        /// Gets the base domain.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 12/19/2012 - 10:56 AM
        protected string GetBaseDomain()
        {
        	if (_companiesConsumer==null&&CspId>=0)
        	{
        		_companiesConsumer = CspDataContext.companies_consumers.FirstOrDefault(c => c.companies_Id == CspId);
        	}
            return _companiesConsumer != null ? "http://" + _companiesConsumer.base_domain : string.Empty;
        }

		/// <summary>
		/// Sends the embed code by email.
		/// </summary>
		/// Author: ducuytran
		/// 12/19/2012 - 3:38 PM
		private void SendEmbedCodeByEmail()
		{
			Response.Clear();
			Response.ContentType = "text/xml";
			int resultCode = 0;
			string emailReciever = Request.Params["reciever"];
			string embedCode = GetLocalizedText("PickupYourCode.TextareaContent").Replace("{scriptlink}", GetSyndicationScript());
			string resultMsg = "";
			if (IsValidEmailAddress(emailReciever))
			{

				string hostEmail = PortalSettings.Email;
				if (string.IsNullOrEmpty(hostEmail))
					hostEmail = Config.GetSetting("Portal" + PortalId + "SupportEmail");
				if (string.IsNullOrEmpty(hostEmail))
					hostEmail = Config.GetSetting("DefaultPortalSupportEmail");
				if (string.IsNullOrEmpty(hostEmail))
					hostEmail = DotNetNuke.Common.Globals.HostSettings["HostEmail"].ToString();
				
				string emailBody = GetLocalizedText("PickupYourCode.EmailScriptContent").Replace("{script_code}", embedCode);
				resultMsg = Mail.SendMail(hostEmail, emailReciever, "", GetLocalizedText("PickupYourCode.EmailEmbedCodeTitle"), emailBody, "", "HTML", "", "", "", "");
				resultCode = 1;
				if (!String.IsNullOrEmpty(resultMsg))
					resultCode = 0;
			}
			string strData = "<xmlresult><resultcode>" + resultCode + "</resultcode><resultmsg>" + resultMsg + "</resultmsg></xmlresult>";
			Response.Write(strData);
			Response.End();
			HttpContext.Current.ApplicationInstance.CompleteRequest();
		}

		/// <summary>
		/// Determines whether [is valid email address] [the specified email address].
		/// </summary>
		/// <param name="emailAddress">The email address.</param>
		/// <returns>
		///   <c>true</c> if [is valid email address] [the specified email address]; otherwise, <c>false</c>.
		/// </returns>
		/// Author: ducuytran
		/// 12/19/2012 - 3:38 PM
		private bool IsValidEmailAddress(string emailAddress)
		{
			string MatchEmailPattern =
				@"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
				+ @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
				+ @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
				+ @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";
			if (!String.IsNullOrEmpty(emailAddress)) return Regex.IsMatch(emailAddress, MatchEmailPattern);
			else return false;
		}

		protected string GetModuleURL()
		{
			return Globals.NavigateURL(TabId, "", string.Format("mid={0}", ModuleId));
		}

        /// <summary>
        /// Gets the current language.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 12/20/2012 - 10:50 AM
        protected string GetCurrentLanguage()
        {
            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            if (EnableLanguageDropdown)
            {
                //get language by languages combobox
                var lang = CspDataContext.languages.SingleOrDefault(a => a.languages_Id == _langId);
                return lang == null ? currentCulture.TwoLetterISOLanguageName : lang.BrowserLanguageCode;
            }

            //Otherwise, Get current language of browser
            return currentCulture.TwoLetterISOLanguageName;  
        }

		/// <summary>
		/// Gotoes the redirect tab id.
		/// </summary>
		/// <param name="parameters">The parameters.</param>
		/// Author: ducuytran
		/// 1/9/2013 - 7:01 PM
		private void GotoRedirectTabId(string parameters)
		{
			var dataProvider = DataProvider.Instance();
			if (Settings[Cons.SETTING_REDIRECT_TAB_NAME] != null && Settings[Cons.SETTING_REDIRECT_TAB_NAME].ToString() != string.Empty)
			{
				var result = dataProvider.ExecuteSQL(string.Format("SELECT TabId FROM Tabs WHERE TabName = '{0}' and PortalID = '{1}'", Settings[Cons.SETTING_REDIRECT_TAB_NAME], PortalId));
				int tabId = 0;
				while (result.Read())
				{
					tabId = int.Parse(result["TabId"].ToString());
					break;
				}
				if (tabId != 0)
				{
					string theUrl = Globals.NavigateURL(tabId);
					if (!String.IsNullOrEmpty(parameters))
						theUrl = Globals.NavigateURL(tabId, "", parameters);
					Response.Redirect(theUrl);
				}
			}
		}

        /// <summary>
        /// Gets the list child cat id.
        /// </summary>
        /// Author: Vu Dinh
        /// 3/22/2013 - 10:01 AM
        private void GetListChildCatId(int catId )
        {
            var listChild = CspDataContext.categories.Where(a => a.parentId == catId).Select(a => a.categoryId).ToList();
            if (listChild.Any())
            {
                _childCatIds.AddRange(listChild);
                foreach (var childCatId in listChild)
                {
                    GetListChildCatId(childCatId);
                }
            }
           
            
        }

		/// <summary>
		/// Haves the admin permission.
		/// </summary>
		/// <returns></returns>
		public bool HaveAdminPermission()
		{
			if (UserInfo.IsSuperUser || UserInfo.IsInRole(PortalSettings.AdministratorRoleName))
				return true;
			return false;
		}

        /// <summary>
        /// Hides the entrypoint tab.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 1/30/2013 - 11:45 PM
        public bool HideEntrypointTab()
        {
            if (Settings[Cons.SETTING_HIDE_ENTRYPOINT_TAB] != null)
                return int.Parse(Settings[Cons.SETTING_HIDE_ENTRYPOINT_TAB].ToString()) > 0;
            return false;
        }
		public bool HideCategoryTab()
		{
			if (Settings[Cons.SETTING_HIDE_CATEGORY_TAB] != null)
				return int.Parse(Settings[Cons.SETTING_HIDE_CATEGORY_TAB].ToString()) > 0;
			return false;
		}
		public bool HideButtonShowcaseCustomization()
		{
			if (Settings[Cons.SETTING_HIDE_BUTTON_SHOWCASE_CUSTOMIZATION] != null)
				return int.Parse(Settings[Cons.SETTING_HIDE_BUTTON_SHOWCASE_CUSTOMIZATION].ToString()) > 0;
			return false;
		}

    	#region [ Get common value to front end ]
        /// <summary>
        /// Gets the width of the preview popup.
        /// </summary>
        /// <returns></returns>
        /// Author: ducuytran
        /// 1/7/2013 - 6:28 PM
        public string GetPreviewPopupWidth()
        {
            if (Settings[Cons.SETTING_PREVIEW_POPUP_WIDTH] != null && Settings[Cons.SETTING_PREVIEW_POPUP_WIDTH].ToString() != string.Empty)
                return Settings[Cons.SETTING_PREVIEW_POPUP_WIDTH] + "px";
            return Cons.DEFAULT_PREVIEWPOPUP_WIDTH + "px";
        }

        /// <summary>
        /// Gets the height of the preview popup.
        /// </summary>
        /// <returns></returns>
        /// Author: ducuytran
        /// 1/7/2013 - 6:28 PM
        public string GetPreviewPopupHeight()
        {
            if (Settings[Cons.SETTING_PREVIEW_POPUP_HEIGHT] != null && Settings[Cons.SETTING_PREVIEW_POPUP_HEIGHT].ToString() != string.Empty)
                return Settings[Cons.SETTING_PREVIEW_POPUP_HEIGHT] + "px";
            return Cons.DEFAULT_PREVIEWPOPUP_HEIGHT + "px";
        }

        /// <summary>
        /// Determines whether [is showcase customization default show].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if [is showcase customization default show]; otherwise, <c>false</c>.
        /// </returns>
        /// Author: Vu Dinh
        /// 1/10/2013 - 8:15 PM
        public bool IsShowcaseCustomizationShowByDefault()
        {
            if (Settings[Cons.SETTING_SHOWCASE_CUSTOMIZATION_HIDE_SHOW] != null)
            {
                return Settings[Cons.SETTING_SHOWCASE_CUSTOMIZATION_HIDE_SHOW].ToString() == "1"; //return true if 1, otherwise false if 2 (not 1)
            }
            return true; //Show by default
        }

        /// <summary>
        /// Gets the content setup thumbnail.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 1/10/2013 - 8:00 PM
        public string GetContentSetupThumbnail()
        {
            return GetLocalizedText("Thumbnail.ContentSetup");
        }

        /// <summary>
        /// Gets the syndication script.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 12/15/2012 - 5:36 PM
        public string GetSyndicationScript()
        {
            var baseDomain = GetBaseDomain();
			return string.Format("{0}/Csp/?mfrname={1}", baseDomain, InstanceName);
        }
		/// <summary>
		/// Gets the showcase preview iframe link.
		/// </summary>
		/// <returns></returns>
		/// Author: Vu Dinh
		/// 12/15/2012 - 2:31 PM
        public string GetShowcasePreviewIframeLink()
        {
            if (_ticks == 0)
                _ticks = DateTime.Now.Ticks;
            return _companiesConsumer != null ? string.Format("http://p{0}-{1}/d1.aspx?{2}&lng={3}&ticks={4}", InstanceName, _companiesConsumer.base_domain, _companiesConsumer.base_publication_parameters, GetCurrentLanguage(), _ticks) : string.Empty;
        }

        /// <summary>
        /// Gets the entrypoint value.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 12/10/2012 - 5:20 AM
        public int GetEntrypointValue()
        {
            var value = CspUtils.GetCompanyParameter(CspId,Cons.COMPANY_PARAMETER_ENTRYPOINT);
            return value != string.Empty ? int.Parse(value) : 4;
        }

        /// <summary>
        /// Gets the banner value.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 12/10/2012 - 5:44 AM
        public int GetBannerValue()
        {
            var value = CspUtils.GetCompanyParameter(CspId,Cons.COMPANY_PARAMETER_IMAGE_OPTION);
            return value != string.Empty ? int.Parse(value) : 1;
        }

        /// <summary>
        /// Gets the textlink value.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 12/10/2012 - 5:52 AM
        public string GetTextlinkValue()
        {
            var value = CspUtils.GetCompanyParameter(CspId,Cons.COMPANY_PARAMETER_LINK_TEXT);
            return value != string.Empty ? value : GetLocalizedText("Entrypoint.TextLinkInput");
        }
        /// <summary>
        /// Gets the launchtype value.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 12/10/2012 - 5:54 AM
        public int GetLaunchtypeValue()
        {
            var value = CspUtils.GetCompanyParameter(CspId,Cons.COMPANY_PARAMETER_LAUNCHTYPE);
            return value != string.Empty ? int.Parse(value) : 1;
        }

        /// <summary>
        /// Gets the launchtype iframe value.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 12/10/2012 - 5:55 AM
        public string GetLaunchtypeIframeValue()
        {
            return CspUtils.GetCompanyParameter(CspId,Cons.COMPANY_PARAMETER_IFRAME_TARGET);
        }

        /// <summary>
        /// Gets the banner button link.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 12/10/2012 - 4:50 PM
        public string GetBannerButtonLink()
        {
            return !string.IsNullOrEmpty(GetLocalizedText("Banner.ButtonLink_" + PortalId)) ? GetLocalizedText("Banner.ButtonLink_" + PortalId) : GetLocalizedText("Banner.ButtonLink");
        }

        /// <summary>
        /// Gets the banner square link.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 12/10/2012 - 4:51 PM
        public string GetBannerSquareLink()
        {
            return !string.IsNullOrEmpty(GetLocalizedText("Banner.SquareLink_" + PortalId)) ? GetLocalizedText("Banner.SquareLink_" + PortalId) : GetLocalizedText("Banner.SquareLink");
        }

        /// <summary>
        /// Gets the banner half link.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 12/10/2012 - 4:52 PM
        public string GetBannerHalfLink()
        {
            return !string.IsNullOrEmpty(GetLocalizedText("Banner.HalfLink_" + PortalId)) ? GetLocalizedText("Banner.HalfLink_" + PortalId) : GetLocalizedText("Banner.HalfLink");
        }

        /// <summary>
        /// Gets the banner full link.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 12/10/2012 - 4:52 PM
        public string GetBannerFullLink()
        {
            return !string.IsNullOrEmpty(GetLocalizedText("Banner.FullLink_" + PortalId)) ? GetLocalizedText("Banner.FullLink_" + PortalId) : GetLocalizedText("Banner.FullLink");
        }

        /// <summary>
        /// Gets the custom banner link.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 12/10/2012 - 4:54 PM
        public string GetCustomBannerLink()
        {
            return CspUtils.GetCompanyParameter(CspId,Cons.COMPANY_PARAMETER_LINK_IMAGE);
        }
        #endregion

        #region [ Event Handlers ]
        /// <summary>
        /// Handles the OnClick event of the btnCatSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 12/7/2012 - 11:04 PM
        protected void btnCatSave_OnClick(object sender, EventArgs e)
        {
            var treeControls = treeContainer.Controls;
        	var themeCategories = CspDataContext.themes_categories.ToList();

            foreach (var treeControl in treeControls)
            {
                var tree = treeControl as RadTreeView;
                if (tree != null)
                {
                    foreach (var node in tree.GetAllNodes().Where(a => a.Target != Cons.CATEGORY_NODE_GROUP_PRODUCT))
                    {
                        var nodeTheme = int.Parse(node.Category);
                        var consumerTheme = CspDataContext.consumers_themes.FirstOrDefault(a => a.category_Id == int.Parse(node.Value) && a.consumer_Id == CspId && a.supplier_Id == SupplierId && a.themes_Id == nodeTheme);

                        if (node.Checked)
                        {
                            if (consumerTheme == null) // Insert new consumer theme
                            {
                                CspDataContext.consumers_themes.InsertOnSubmit(new consumers_theme
                                {
                                    themes_Id = nodeTheme,
                                    consumer_Id = CspId,
                                    supplier_Id = SupplierId,
                                    category_Id = int.Parse(node.Value)
                                });
                                CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                            }

                            _childCatIds = new List<int>();
                            GetListChildCatId(int.Parse(node.Value));

                            if (_childCatIds.Count == 0)
                                continue;

                            // we only want to add the categories in the theme_categories table
                            _childCatIds = (from a in themeCategories.Where(b => b.themes_Id == nodeTheme)
                                            join c in _childCatIds on a.category_Id equals c
                                            select c).ToList();

                            foreach (var catId in _childCatIds)
                            {
                                if (!CspDataContext.consumers_themes.Any(a => a.category_Id == catId && a.consumer_Id == CspId && a.supplier_Id == SupplierId && a.themes_Id == nodeTheme))
                                {
                                    CspDataContext.consumers_themes.InsertOnSubmit(new consumers_theme
                                    {
                                        themes_Id = nodeTheme,
                                        consumer_Id = CspId,
                                        supplier_Id = SupplierId,
                                        category_Id = catId
                                    });
                                    CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                                }
                            }
                        }
                        else
                        {
                            if (consumerTheme != null) //Delete consumer theme
                            {
                                CspDataContext.consumers_themes.DeleteOnSubmit(consumerTheme);
                            }

                            _childCatIds = new List<int>();
                            GetListChildCatId(int.Parse(node.Value));
                            // optimize the code to avoid multiple commits

                            var consumerThemeTobedeleted = from a in CspDataContext.consumers_themes
                                                            where a.themes_Id == nodeTheme && a.supplier_Id == SupplierId && a.consumer_Id == CspId && _childCatIds.Contains(a.category_Id.Value)
                                                            select a;
                            CspDataContext.consumers_themes.DeleteAllOnSubmit(consumerThemeTobedeleted);
                            CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                        }
                    }

                    if (ShowProductLevel)
                    {
                        var listProductNode = tree.GetAllNodes().Where(a => a.Target == Cons.CATEGORY_NODE_GROUP_PRODUCT).ToList();
                        if (listProductNode.Any(a => a.Checked))
                        {
                            //clear tables before insert new record
                            CspDataContext.ExecuteCommand(@"DELETE FROM Sku_Company_Category_Include WHERE CompanyID = {0};
                                                          DELETE FROM Sku_Company_ContentGUID_Include WHERE CompanyID = {0}", CspId);
                            CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);

                            foreach (var node in listProductNode.Where(a => a.Checked))
                            {
                                var categoryId = int.Parse(node.ParentNode.Value);
                                var listParentCategoriesId = GetParentCategoriesId(categoryId, new List<int>());
                                var temp = new StringBuilder();
                                foreach (var catId in listParentCategoriesId)
                                {
                                    temp.Append(string.Format("({0}),", catId));
                                }


                                var query = "insert into @temp (categoryId) values " + temp.ToString(0, temp.Length - 1);
                                query = @"
declare @temp table (
categoryId int 
);" + query + "; ";
                                
                                query += string.Format(@"
INSERT INTO Sku_Company_Category_Include (CompanyID, CategoryID) SELECT {0},categoryId FROM @temp WHERE categoryId NOT IN (SELECT CategoryID FROM Sku_Company_Category_Include WHERE CompanyID = {0});
INSERT INTO Sku_Company_ContentGUID_Include (CompanyID, ContentID) VALUES ({0} , '{1}');
                                                                IF(NOT EXISTS(SELECT * FROM Sku_ImpactedCompanies WHERE CompanyID = {0}))
                                                                BEGIN
                                                                    INSERT INTO Sku_ImpactedCompanies (CompanyID) VALUES ({0})
                                                                END;
                                                                IF(NOT EXISTS(SELECT * FROM Sku_ImpactedContentTypes WHERE ContentTypeID = {2}))
                                                                BEGIN
                                                                    INSERT INTO Sku_ImpactedContentTypes (ContentTypeID) VALUES ({2})
                                                                END
                                                                ", CspId, Guid.Parse(node.ContentCssClass),ProductLevelProductT);
                                //insert table #Sku_Company_Category_Include,  #Sku_Company_ContentGUID_Include, #Sku_ImpactedCompanies, #Sku_ImpactedContentTypes
                                CspDataContext.ExecuteCommand(query);
                            }
                        }
                        else
                        {
                            // clear all record
                            CspDataContext.ExecuteCommand(@"DELETE FROM Sku_Company_Category_Include WHERE CompanyID = {0};
                                                          DELETE FROM Sku_Company_ContentGUID_Include WHERE CompanyID = {0};
                                                          DELETE FROM Sku_ImpactedCompanies WHERE CompanyID = {0};     
                                                            ", CspId, ProductLevelProductT);
                        }
                       
                    }
                }
            }
        }

        /// <summary>
        /// Handles the OnSelectedIndexChanged event of the cbxLanguages control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 12/8/2012 - 7:00 AM
        protected void cbxLanguages_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (_companiesConsumer != null)
            {
                //Change default language
                _companiesConsumer.default_language_Id = _langId;
                CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
            }
        }

        /// <summary>
        /// Handles the OnClick event of the btnSaveEntrypoint control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 12/10/2012 - 6:26 AM
        protected void btnSaveEntrypoint_OnClick(object sender, EventArgs e)
        {
            try
            {
                var entryPointvalue = Request.Params["entrypoint"];
                if (entryPointvalue != null)
                {
                    var companyParameterEntrypoinType = GetOrCreateCompanyParamType(Cons.COMPANY_PARAMETER_ENTRYPOINT);
                    CreateOrUpdateCompanyParam(companyParameterEntrypoinType.companies_parameter_types_Id, entryPointvalue);
                    switch (int.Parse(entryPointvalue))
                    {
                        case 1: //update text link
                            {
                                if (string.IsNullOrEmpty(Request.Params["soTm"]))
                                {
                                    textlinkValidator.Text = GetLocalizedText("Label.RequireField");
                                    textlinkValidator.IsValid = false;
                                }
                                else
                                {
                                    var textLinkType = GetOrCreateCompanyParamType(Cons.COMPANY_PARAMETER_LINK_TEXT);
                                    CreateOrUpdateCompanyParam(textLinkType.companies_parameter_types_Id, Request.Params["soTm"]);
                                }
                                break;
                            }
                        case 2: //Update banner
                            {
                                var imageOptionType = GetOrCreateCompanyParamType(Cons.COMPANY_PARAMETER_IMAGE_OPTION);
                                CreateOrUpdateCompanyParam(imageOptionType.companies_parameter_types_Id, Request.Params["epBanner"]);

                                var imageCustomlinkType = GetOrCreateCompanyParamType(Cons.COMPANY_PARAMETER_LINK_IMAGE);
                                var bannerType = int.Parse(Request.Params["epBanner"]);
                                string path = string.Empty;
                                switch (bannerType)
                                {
                                    case 5:
                                        path = UploadBanner();
                                        break;
                                    case 1:
                                        path = GetBannerButtonLink();
                                        break;
                                    case 2:
                                        path = GetBannerSquareLink();
                                        break;
                                    case 3:
                                        path = GetBannerHalfLink();
                                        break;
                                    case 4:
                                        path = GetBannerFullLink();
                                        break;
                                }
                                if (path != string.Empty)
                                    CreateOrUpdateCompanyParam(imageCustomlinkType.companies_parameter_types_Id, path);
                                break;
                            }
                    }

                    if (int.Parse(entryPointvalue) != 4) // save launch type value
                    {
                        var launchtypeValue = Request.Params["launchtype"];
                        if (launchtypeValue != null)
                        {
                            var companyParamLaunchtype = GetOrCreateCompanyParamType(Cons.COMPANY_PARAMETER_LAUNCHTYPE);
                            CreateOrUpdateCompanyParam(companyParamLaunchtype.companies_parameter_types_Id, launchtypeValue);
                            if (int.Parse(launchtypeValue) == 2) //update iframe text
                            {
                                var iframeTargetType = GetOrCreateCompanyParamType(Cons.COMPANY_PARAMETER_IFRAME_TARGET);
                                CreateOrUpdateCompanyParam(iframeTargetType.companies_parameter_types_Id,Request.Params["rLtWTarget"]);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                // Do nothing
            }
        }
        #endregion
    }
}