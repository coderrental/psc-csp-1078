﻿// ***********************************************************************
// Assembly         : TIEKinetix.CspModules.CSPCollateralDashboardModule
// Author           : VU DINH
// Created          : 06-07-2013
//
// Last Modified By : VU DINH
// Last Modified On : 06-07-2013
// ***********************************************************************
// <copyright file="LocalPartnerType.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace TIEKinetix.CspModules.CSPCollateralDashboardModule.Entities
{
    /// <summary>
    /// Class LocalPartnerType
    /// </summary>
    public class LocalPartnerType
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }


        /// <summary>
        /// Gets or sets the catgegory id.
        /// </summary>
        /// <value>The catgegory id.</value>
        public int CatgegoryId { get; set; }
    }
}