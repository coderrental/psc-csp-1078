﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TIEKinetix.CspModules.CSPCollateralDashboardModule.Entities
{
    public class CampaignBlock
    {
        public CampaignBlock()
        {
            Item = 0;
        }
        /// <summary>
        /// Gets or sets the layout.
        /// </summary>
        /// <value>
        /// The layout.
        /// </value>
        /// Author: Vu Dinh
        /// 11/29/2012 - 4:39 PM
        public string Layout { get; set; }

        /// <summary>
        /// Gets or sets the item.
        /// </summary>
        /// <value>
        /// The item.
        /// </value>
        /// Author: Vu Dinh
        /// 11/29/2012 - 4:51 PM
        public int Item { get; set; }
    }
}