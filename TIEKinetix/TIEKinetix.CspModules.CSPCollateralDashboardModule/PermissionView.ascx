﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PermissionView.ascx.cs" Inherits="TIEKinetix.CspModules.CSPCollateralDashboardModule.PermissionView" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadAjaxPanel runat="server">
<telerik:RadGrid ID="permissionGrid" runat="server" AllowPaging="true" AllowSorting="true" AllowFilteringByColumn="true" DataSourceID="dsPermissions"
    OnItemCommand="OnGridPermissionItemCommand" OnItemCreated="permissionGrid_ItemCreated"> 
    <MasterTableView  AutoGenerateColumns="false" DataKeyNames="PermissionId" EditMode="EditForms" CommandItemDisplay="Top">
    	<CommandItemSettings AddNewRecordText="Add new user"></CommandItemSettings>
        <Columns>
        	<telerik:GridBoundColumn DataField="Username" HeaderText="User Name"></telerik:GridBoundColumn>                
            <telerik:GridBoundColumn DataField="DisplayName" HeaderText="Display Name"></telerik:GridBoundColumn>            
            <telerik:GridBoundColumn DataField="DateCreated" HeaderText="Created Date" AllowFiltering="false"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="DateChanged" HeaderText="Changed Date" AllowFiltering="false"></telerik:GridBoundColumn>
            <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" HeaderStyle-Width="32"></telerik:GridButtonColumn>
        </Columns>
        <NestedViewTemplate>
            <asp:Panel ID="panel" runat="server">
                <asp:HiddenField ID="hfpid" runat="server" Value='<%#Eval("PermissionId")  %>' />
                <telerik:RadTabStrip ID="tabs" runat="server" MultiPageID="pages" SelectedIndex="0">
                    <Tabs>
                        <telerik:RadTab runat="server" PageViewID="pageLanguage"></telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
                <telerik:RadMultiPage ID="pages" runat="server" SelectedIndex="0">
                    <telerik:RadPageView runat="server" ID="pageLanguage">                        
                        <telerik:RadGrid runat="server" ID="gridLanguage" EnableViewState="true" OnItemCommand="OnLanguageGridItemCommand">
                            <MasterTableView AutoGenerateColumns="false" DataKeyNames="languages_Id" CommandItemDisplay="Top">
                                <Columns>
                                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" HeaderStyle-Width="32"></telerik:GridButtonColumn>
                                    <telerik:GridBoundColumn DataField="languagecode" HeaderText="Language Code" HeaderStyle-Width="100"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="description" HeaderText="Description"></telerik:GridBoundColumn>
                                </Columns>
                                <EditFormSettings EditFormType="WebUserControl">
                                        <EditColumn ButtonType="ImageButton"></EditColumn>                
                                </EditFormSettings>  
								<CommandItemSettings AddNewRecordText='Add new user'></CommandItemSettings>      
                            </MasterTableView>                            
                        </telerik:RadGrid>
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </asp:Panel>
            <asp:SqlDataSource runat="server" ID="dsLanguages" SelectCommand="
                select a.languages_Id, a.languagecode, a.description, a.active
                from languages a">
            </asp:SqlDataSource>
        </NestedViewTemplate>
        <EditFormSettings EditFormType="WebUserControl">
                <EditColumn ButtonType="ImageButton"></EditColumn>                
        </EditFormSettings>        
    </MasterTableView>
</telerik:RadGrid>
<asp:SqlDataSource ID="dsPermissions" runat="server">
</asp:SqlDataSource>
</telerik:RadAjaxPanel>
