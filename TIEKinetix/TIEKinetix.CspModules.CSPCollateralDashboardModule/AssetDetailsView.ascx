﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssetDetailsView.ascx.cs" Inherits="TIEKinetix.CspModules.CSPCollateralDashboardModule.AssetDetailsView" %>
<%@ Import Namespace="CR.ContentObjectLibrary.Data" %>
<%@ Import Namespace="CR.DnnModules.Common" %>
<%@ Import Namespace="TIEKinetix.CspModules.CSPCollateralDashboardModule.Components" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxLoadingPanel ID="LoadingPanel" runat="server" Skin="Windows7" IsSticky="true"></telerik:RadAjaxLoadingPanel>
<telerik:RadAjaxManager runat="server" ID="ajaxManager" DefaultLoadingPanelID="LoadingPanel">
    <AjaxSettings>
    </AjaxSettings>
</telerik:RadAjaxManager>
<div class="csp-asset-detail-container">
	<asp:Panel ID="AssetBreadcrumbPanel" runat="server" CssClass="csp-clear csp-asset-breadcrumb">		
	</asp:Panel>
    <asp:Panel ID="AssetDetailPanel" runat="server" CssClass="csp-asset-panel csp-asset-details csp-clear csp-asset-detailwrapper">
        <asp:Panel CssClass="csp-nav-container" ID="NavPanel" runat="server">
            <table>
                <tr>
                    <td runat="server" ID="PanelLanguage">
                        <div class="csp-asset-title"><asp:Literal ID="lbLanguage" runat="server"></asp:Literal></div>
                        <telerik:RadComboBox runat="server" ID="cbLanguage" AutoPostBack="true" OnSelectedIndexChanged="LanguageCombobox_LanguageChanged" Filter="Contains"></telerik:RadComboBox>
                    </td>
                    <td>
                        <div class="csp-asset-title"><asp:Literal ID="lbCategories" runat="server"></asp:Literal></div>
                        <telerik:RadComboBox runat="server" ID="cbCategories" ShowToggleImage="true" Width="350px" OnClientDropDownOpened="OnClientDropDownOpenedHandler" AutoPostBack="true">
                            <ItemTemplate>
                                <telerik:RadTreeView ID="categoryTree" runat="server" Skin="Windows7" OnClientNodeClicking="nodeClicking" OnNodeClick="CategoryTreeOnNodeClick">
                                    <DataBindings>
                                        <telerik:RadTreeNodeBinding Expanded="true" />
                                    </DataBindings>
                                </telerik:RadTreeView>                                        
                            </ItemTemplate>
                            <Items>
                                <telerik:RadComboBoxItem Text="" />
                            </Items>
                            <CollapseAnimation Type="None" />
                        </telerik:RadComboBox>
                    </td>
                    <td>
                        <asp:Panel runat="server" ID="AudiencePanel" Visible="False">
                            <div class="csp-asset-title"><asp:Literal ID="lbAudience" runat="server"></asp:Literal></div>
                            <telerik:RadComboBox runat="server" ID="cbAudience" AutoPostBack="True" OnSelectedIndexChanged="cbAudience_OnSelectedIndexChanged"></telerik:RadComboBox>
                        </asp:Panel>
                    </td>
                    <td>
                        <asp:Panel ID="StagePanel" runat="server" Visible="false" Enabled="false">
                        <div class="csp-asset-title"><asp:Literal ID="lbStages" runat="server"></asp:Literal></div>
                        <telerik:RadComboBox runat="server" ID="cbStages" AutoPostBack="true" Filter="StartsWith" OnSelectedIndexChanged="StageCombobox_StageChanged"></telerik:RadComboBox>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:Panel>   
        <p>&nbsp;</p>     
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <div class="csp-hide" cspobj="REPORT" csptype="asset" cspenglishvalue="<%=Utils.GetContentFieldValue(CspContext,CspContentPiece,"CSP_Reporting_Id") %>"
                cspstate="clicked">
            </div>
            <div class="asset-preview-image">
				<img alt="<%= Utils.GetContentFieldValue(CspContext,CspContentPiece,"Content_Image_Thumbnail") %>" src="<%= Utils.GetContentFieldValue(CspContext,CspContentPiece,"Content_Image_Thumbnail") %>" />
            </div>
        </telerik:RadCodeBlock>
            
        <div class="asset-content">
            <div class="asset-header">
                <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
                    <%--<h1><%= CspContentPiece.Content_Description_Short%></h1>--%>
                    <h1>
                        <%= Utils.GetContentFieldValue(CspContext,CspContentPiece, "Content_Title")%></h1>
                    <div>
                        <%= Utils.GetContentFieldValue(CspContext,CspContentPiece, "Content_Description_Long")%></div>
                </telerik:RadCodeBlock>
            </div>
            <div class="asset-buttons csp-clear">
                <ul>
                    <telerik:RadCodeBlock ID="RadCodeBlock3" runat="server">
                        <li>
							<a id="btnPreviewShowcase" class="btn-long-link" href="javascript: void(0)" onclick="ShowPreview('<%= Utils.GetContentFieldValue(CspContext,CspContentPiece,"Content_Image_Url") %>')" cspobj="REPORT" csptype="links" cspenglishvalue="Preview">
								<span class="btn-long ss-btn">
									<span class="btn-long-right ss-btn">
										<span class="btn-long-mid ss-btn">
											<span class="btn-long-text ss-btn"><%=GetLocalizedText("Button.Preview")%></span>
										</span>
									</span>
								</span>
							</a>
						</li>
                        <li>
							<%if (ModuleRole.CountryRegionalManager(UserInfo, _currentLanguageId))
							{%>
								<a id="bDownload" class="btn-long-link" href="javascript: void(0)" cspobj="REPORT" csptype="links" cspenglishvalue="Download" onclick="ShowDownload()">
									<span class="btn-long ss-btn">
										<span class="btn-long-right ss-btn">
											<span class="btn-long-mid ss-btn">
												<span class="btn-long-text ss-btn"><%=GetLocalizedText("Button.Download")%></span>
											</span>
										</span>
									</span>
								</a>
								<div id="DownloadContainer">
									<a class="btn-long-link" href="<%= GetAssetDownloadLink(CspContentPiece,"Content_File_Url") %>" target="_blank" cspobj="REPORT" csptype="links" cspenglishvalue="Download">
										<span class="btn-long ss-btn">
											<span class="btn-long-right ss-btn">
												<span class="btn-long-mid ss-btn">
													<span class="btn-long-text ss-btn"><%=GetLocalizedText("Button.Download")%></span>
												</span>
											</span>
										</span>
									</a> <br /><br />
									<a class="btn-long-link" href="<%= GetAssetDownloadLink(CspContentPiece,"Content_File_Url_Original") %>" target="_blank" cspobj="REPORT" csptype="links" cspenglishvalue="DownloadOriginal">
										<span class="btn-long ss-btn">
											<span class="btn-long-right ss-btn">
												<span class="btn-long-mid ss-btn">
													<span class="btn-long-text ss-btn"><%=GetLocalizedText("Button.DownloadOriginal") %></span>
												</span>
											</span>
										</span>
									</a><br /><br />
									<a class="btn-long-link" href="<%=GetAssetDownloadLink(CspContentPiece,"Content_Source_File_Url") %>" target="_blank" cspobj="REPORT" csptype="links" cspenglishvalue="DownloadSource">
										<span class="btn-long ss-btn">
											<span class="btn-long-right ss-btn">
												<span class="btn-long-mid ss-btn">
													<span class="btn-long-text ss-btn"><%=GetLocalizedText("Button.DownloadSource") %></span>
												</span>
											</span>
										</span>
									</a>
								</div>								
							<%}
							else // not a sub
							{%>
								<a class="btn-long-link" href="<%= EncodeAssetUrl(Utils.GetContentFieldValue(CspContext,CspContentPiece,"Content_File_Url"))  %>" target="_blank" cspobj="REPORT" csptype="links" cspenglishvalue="Download">
									<span class="btn-long ss-btn">
										<span class="btn-long-right ss-btn">
											<span class="btn-long-mid ss-btn">
												<span class="btn-long-text ss-btn"><%=GetLocalizedText("Button.Download")%></span>
											</span>
										</span>
									</span>
								</a>
							<%}
							%>
							
						</li>
                    </telerik:RadCodeBlock>
                    <li>
                        <telerik:RadButton runat="server" ID="bUpload" CssClass="btn-long-link" ButtonType="LinkButton"
							cspobj="REPORT" csptype="links" cspenglishvalue="Upload" AutoPostBack="false">
                            <ContentTemplate>
                                <span class="btn-long ss-btn">
									<span class="btn-long-right ss-btn">
										<span class="btn-long-mid ss-btn">
											<span class="btn-long-text ss-btn"><%=GetLocalizedText("Button.Upload") %></span>
										</span>
									</span>
								</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </li>
                    <li>
                        <telerik:RadButton runat="server" ID="bPublish" CssClass="btn-long-link" ButtonType="LinkButton"
                            OnClick="ModuleButtonOnClickEventHandler" cspobj="REPORT" csptype="links" cspenglishvalue="Publish">
                            <ContentTemplate>
								<span class="btn-long ss-btn">
									<span class="btn-long-right ss-btn">
										<span class="btn-long-mid ss-btn">
											<span class="btn-long-text ss-btn"><asp:Literal ID="lbText" runat="server"></asp:Literal></span>
										</span>
									</span>
								</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </li>
                </ul>
            </div>
            <div class="csp-clear">
            </div>
        </div> 
		<div class="clear"></div>                   
    </asp:Panel>
    <asp:Panel ID="AssetNav1Panel" runat="server" CssClass="csp-asset-panel nav-asset-container csp-clear black-bottom-nav">
        <div class="nav-asset-title"><asp:Literal ID="lbRelatedAssets" runat="server"></asp:Literal></div>
		<div>
			<img id="rightButton" src="<%=ControlPath+"Images/arrow_right.png" %>" alt="" />
			<img id="leftButton" src="<%=ControlPath+"Images/arrow_left.png" %>" alt="" />
			<telerik:RadRotator runat="server" ID="AssetRotator" RotatorType="ButtonsOver" Width="880"   
				Height="130" ItemHeight="120" ItemWidth="150" FrameDuration="1" OnItemClick="AssetRotatorOnItemClick" top>
				<ItemTemplate>
					<asp:Image ID="AssetImage" runat="server" ToolTip='<%# Utils.GetContentFieldValue(CspContext,(content)Container.DataItem,"Content_Title") %>' ImageUrl='<%#Utils.GetContentFieldValue(CspContext,(content)Container.DataItem,"Content_Image_Thumbnail")%>' Value='<%#Eval("content_Id")%>' Width="145" />
				</ItemTemplate>                
				<ControlButtons LeftButtonID="leftButton" RightButtonID="rightButton" />
			</telerik:RadRotator>
		</div>
    </asp:Panel>
    
    <asp:Panel ID="AssetNav2Panel" runat="server" CssClass="csp-asset-panel csp-clear black-bottom-nav">                
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
            	function nodeClicking(sender, args) {
            		var cb = $find("<%=cbCategories.ClientID %>");
            		var node = args.get_node();
            		cb.set_text(node.get_text());
            		cb.set_value(node.get_value());
            		cb.trackChanges();
            		cb.get_items().getItem(0).set_text(node.get_text());
            		cb.get_items().getItem(0).set_value(node.get_value());
            		cb.commitChanges();
            		cb.hideDropDown();
            		cb.attachDropDown();
            	}
            	function OnClientDropDownOpenedHandler(sender, eventArgs) {
            		var tree = sender.get_items().getItem(0).findControl("categoryTree");
            		var selectedNode = tree.get_selectedNode();
            		if (selectedNode) {
            			selectedNode.scrollIntoView();
            		}
            	}
            </script>
        </telerik:RadScriptBlock>
        <div class="csp-clear"></div>
    </asp:Panel>

    <telerik:RadWindow ID="AssetWindow" runat="server" Behaviors="Close,Move,Pin,Resize" 
        ShowContentDuringLoad="false" EnableShadow="true" Modal="true" VisibleStatusbar="false">		
    </telerik:RadWindow>
	<div id="kWin"></div>
     <telerik:RadScriptBlock ID="RadScriptBlock2" runat="server">
        <script type="text/javascript">
        	jQuery(function() {
				if ($.browser.mozilla == true) {
					$(".rrClipRegion").css("top", "0px").css("left", "0px");
				}
        		if ($.browser.msie && $.browser.version == 7) {
        			$(".rrClipRegion").css("top", "0px").css("left", "0px");
        		}
        	});
   
        	$("#DownloadContainer").kendoWindow({
        		actions: ["Refresh", "Close"],
        		draggable: false,
        		height: "170px",
        		modal: true,
        		resizable: true,
        		title: "<%=GetLocalizedText("Label.DownloadWindowTitle") %>",
        		width: "400px",
        		visible: false
        	}).data("kendoWindow");
        	$("#kWin").kendoWindow({actions: ["Refresh", "Close"],visible:false,modal:true,close:function(){$("#kWin").html("");}}).data("kendoWindow");

        	function ShowPreview(url) {
        		var win = $("#kWin").data("kendoWindow");
        		win.title('<%=GetLocalizedText("Button.Preview") %>');
        		win.center();
        		var ext = url.split('.').pop().toLowerCase();
        		if (ext != "swf" && ext != "pdf") {

        			var loadingPanel = $find('<%= LoadingPanel.ClientID %>');
        			var crrUpdateControl = "kWin";
        			loadingPanel.show(crrUpdateControl);

        			//Create DOM IMG to get size
        			var newImg = $('<img>');
        			newImg.hide();
        			newImg.bind('load', function() {
        				//Get IMG size
        				var newWDHeight = $(this).height() + 50;
        				var newWDWidth = $(this).width() + 30;

        				//Set new size to Kendo UI Window
        				$('#kWin').closest(".k-window").css({
        					width: newWDWidth,
        					height: newWDHeight
        				});

        				win.refresh({ url: url, type: "GET" });
        				win.center();
        				win.open();
        				loadingPanel.hide(crrUpdateControl);
        			});
        			$('body').append(newImg);
        			newImg.attr('src', url);
        		} else if (ext == "swf") {
        			win.refresh({ url: '<%=Request.Url.Scheme %>://<%=Request.Url.Host %><%=ControlPath %>preview.html?path=' + url, type: "GET" });
        			win.wrapper.height("365");
        			win.wrapper.width("370");
        			win.center();
        			win.open();
        		} else {
        			win.refresh({ url: url, type: "GET" });
        			win.wrapper.height("565");
        			win.wrapper.width("915");
        			win.center();
        			win.open();
        		}
        	}

        	function ShowDownload(){
        		var win=$("#DownloadContainer").data("kendoWindow");
        		win.center();
        		win.open();
        	}
        	function ShowUpload(sender, arg){		
        		var win=$("#kWin").data("kendoWindow");
        		win.wrapper.css({width:700,height:500});
        		win.refresh({url: arg._commandArgument,type: "GET"});
        		win.title('<%=GetLocalizedText("Button.Upload") %>');
        		win.center();
        		win.open();
        	}
        	var cspConsumerInfo = <%=ConsumerInfoJson %>;
        </script>
     </telerik:RadScriptBlock>
</div>