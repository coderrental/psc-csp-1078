﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using DotNetNuke.Common;
using DotNetNuke.Entities.Modules.Internal;
using TIEKinetix.CspModules.CSPCollateralDashboardModule.Components;

namespace TIEKinetix.CspModules.CSPCollateralDashboardModule
{
    public partial class Download : DnnModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Request.QueryString["f"]) || !Request.IsAuthenticated)
                Response.Redirect(Globals.NavigateURL());

            string file = Request.QueryString["f"];
            file = Decode(UrlPathDecode(file));
            Guid id = Guid.Parse(Request.QueryString["id"]);

            string rootPath = Settings[ModuleSetting.SecureAssetPath] != null ? Settings[ModuleSetting.SecureAssetPath].ToString() : @"E:\Backup\FTP\portal\\";
            string filePath = file.Replace("http://tiekinetix.com/rc_files/portal/", "").Replace("/", "\\");
            filePath = rootPath + filePath;
            if (!File.Exists(filePath))
            {
                Log(string.Format("Couldn't find {0} , redirecting to {1}", filePath, file));
                Response.Redirect(file, true);
                Response.End();
            }

            var cspSecureYN = CspContext.GetContentFieldValue(id, "CSP_Secure_Y_N").FirstOrDefault();

            if (Settings[ModuleSetting.SecureAssets].Equals(true.ToString())
                && (cspSecureYN != null && cspSecureYN.value_text.Equals("yes", StringComparison.CurrentCultureIgnoreCase))
                )
            {
                if (Request.IsAuthenticated)
                    DownloadFile(filePath);
                else
                {
                    Response.StatusCode = 404;
                    Response.End();
                }
            }
            else
                DownloadFile(filePath);
        }

        private void DownloadFile(string filePath)
        {
            try
            {
                using (var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    byte[] buffer = new byte[fs.Length];
                    fs.Read(buffer, 0, buffer.Length);
                    Response.BinaryWrite(buffer);
                }
                Response.StatusCode = 200;
            }
            catch (Exception ex)
            {
                Response.StatusCode = 404;
            }
            finally
            {
                Response.ContentType = "application/octet-stream";
                Response.Headers.Add("Content-Disposition", string.Format("attachment;filename=\"{0}\"", Path.GetFileName(filePath)));
                Response.Flush();
                Response.End();
            }
        }

        private string Decode(string s)
        {
            byte[] input = Convert.FromBase64String(s);
            using (var inputStream = new MemoryStream(input))
            {
                using (var gzip = new DeflateStream(inputStream, CompressionMode.Decompress))
                {
                    using (var reader = new StreamReader(gzip, Encoding.UTF8))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
        }

        private string UrlPathDecode(string file)
        {
            string temp = file;
            var r = new Regex("-l(?<v>\\d+)tu", RegexOptions.Compiled);
            while (r.IsMatch(temp))
            {
                Match m = r.Match(temp);
                temp = temp.Remove(m.Index, m.Length);
                temp = temp.Insert(m.Index, ((char)Convert.ToInt32(m.Groups["v"].Value)).ToString());
            }
            return temp;
        }
    }
}