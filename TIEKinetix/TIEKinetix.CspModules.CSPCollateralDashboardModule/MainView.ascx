﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainView.ascx.cs" Inherits="TIEKinetix.CspModules.CSPCollateralDashboardModule.MainView" %>
<%@ Import Namespace="CR.ContentObjectLibrary.Data" %>
<%@ Import Namespace="CR.DnnModules.Common" %>
<%@ Import Namespace="TIEKinetix.CspModules.CSPCollateralDashboardModule.Entities" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxLoadingPanel ID="LoadingPanel" runat="server" Skin="Windows7"></telerik:RadAjaxLoadingPanel>
<telerik:RadAjaxManager runat="server" ID="ajaxManager" DefaultLoadingPanelID="LoadingPanel">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="AssetsDataPager">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="AssetsPanel" />                
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>
<asp:Panel runat="server" ID="MsgPanel" Visible="False">
	<asp:Label runat="server" ID="lbMsg"/>
</asp:Panel>
<asp:Panel runat="server" ID="MainPanel" CssClass="main-pane-view">
	<asp:Panel ID="NavPanel" runat="server" CssClass="csp-nav-panel">
		<script type="text/javascript">
		    function nodeClicking(sender, args) {
		        var cb = $find("<%=cbCategories.ClientID %>");
		        var node = args.get_node();
		        cb.set_text(node.get_text());
		        cb.set_value(node.get_value());
		        cb.trackChanges();
		        cb.get_items().getItem(0).set_text(node.get_text());
		        cb.get_items().getItem(0).set_value(node.get_value());
		        cb.commitChanges();
		        cb.hideDropDown();
		        cb.attachDropDown();
		    }
		    function OnClientDropDownOpenedHandler(sender, eventArgs) {
		        var tree = sender.get_items().getItem(0).findControl("categoryTree");
		        var selectedNode = tree.get_selectedNode();
		        if (selectedNode) {
		            selectedNode.scrollIntoView();
		        }
		    }
		</script>
		<div class="csp-nav-container">
			<table>
				<tr>
					<td runat="server" ID="PanelLanguage">
						<div class="csp-asset-title"><asp:Literal ID="lbLanguage" runat="server"></asp:Literal></div>
						<telerik:RadComboBox runat="server" ID="cbLanguage" AutoPostBack="true" OnSelectedIndexChanged="cbLanguage_OnSelectedIndexChanged" Filter="StartsWith"></telerik:RadComboBox>
					</td>
					<td>
						<div class="csp-asset-title"><asp:Literal ID="lbCategories" runat="server"></asp:Literal></div>
						<telerik:RadComboBox runat="server" ID="cbCategories" ShowToggleImage="true" Width="400px" OnClientDropDownOpened="OnClientDropDownOpenedHandler" 
							OnSelectedIndexChanged="ComboBoxSelectedIndexChanged" AutoPostBack="true">
							<ItemTemplate>
								<telerik:RadTreeView ID="categoryTree" runat="server" Skin="Windows7" OnClientNodeClicking="nodeClicking" OnNodeClick="CategoryTreeOnNodeClick">
									<DataBindings>
										<telerik:RadTreeNodeBinding Expanded="true" />
									</DataBindings>
								</telerik:RadTreeView>                                        
							</ItemTemplate>
							<Items>
								<telerik:RadComboBoxItem Text="" />
							</Items>
							<CollapseAnimation Type="None" />
						</telerik:RadComboBox>
					</td>
                    <td>
                        <asp:Panel runat="server" ID="AudiencePanel" Visible="False">
                            <div class="csp-asset-title"><asp:Literal ID="lbAudience" runat="server"></asp:Literal></div>
                            <telerik:RadComboBox runat="server" ID="cbAudience" AutoPostBack="True" OnSelectedIndexChanged="ComboBoxSelectedIndexChanged"></telerik:RadComboBox>
                        </asp:Panel>
                    </td>
					<td>
						<asp:Panel ID="StagePanel" runat="server" Visible="false" Enabled="false">
						<div class="csp-asset-title"><asp:Literal ID="lbStages" runat="server"></asp:Literal></div>
						<telerik:RadComboBox runat="server" ID="cbStages" AutoPostBack="true" Filter="StartsWith" OnSelectedIndexChanged="ComboBoxSelectedIndexChanged"></telerik:RadComboBox>
						</asp:Panel>
					</td>
				</tr>
			</table>        
		</div>
		<div class="csp-clear"></div>
	</asp:Panel>
    <asp:Panel runat="server" CssClass="alert alert-info" ID="SearchPanel">
        <telerik:RadTextBox runat="server" ID="tbxSearch"></telerik:RadTextBox>
        <telerik:RadButton runat="server" ID="btnSearch">
             <Icon PrimaryIconCssClass="rbSearch" PrimaryIconTop="4" PrimaryIconLeft="4"></Icon>
        </telerik:RadButton>
    </asp:Panel>
	<asp:Panel ID="AssetsPanelContainer" runat="server">
		<telerik:RadListView runat="server" ID="listView" AllowPaging="False" ItemPlaceholderID="AssetsPlaceHolder" OnPreRender="ListViewPreRender">
			<LayoutTemplate>
				<asp:Panel runat="server" ID="AssetsPanel">
					<div class="csp-assets csp-assets-holder">
					    <ul>
						<asp:PlaceHolder ID="AssetsPlaceHolder" runat="server"></asp:PlaceHolder>
                        </ul>
					</div>
					<!--<div class="csp-clear" id="carousel-pagination">
					</div>-->
				</asp:Panel>            
			</LayoutTemplate>
			<ItemTemplate>
                <li>
                    <%# GetCampaignItemLayout((CampaignBlock)Container.DataItem) %>
                </li>
			</ItemTemplate>
			<EmptyDataTemplate>            
				<div class="csp-no-asset"><%=GetLocalizedText("Label.NoAvailableAsset")%></div>
			</EmptyDataTemplate>
		</telerik:RadListView>

	</asp:Panel>

	<telerik:RadScriptBlock runat="server">
	<script type="text/javascript">
	    jQuery(function() {
	    	if ($('.csp-assets-holder li').length > 1)
				$('.csp-assets-holder').carousel({ dispItems: 1, pagination: true });
	    	else 
	    		$('.csp-assets-holder').carousel({ dispItems: 1, pagination: false });
	    });

	    var cspConsumerInfo = <%=ConsumerInfoJson %>;
	    function Pager_OnPageIndexChanging(sender, eventArgs) {
	        if (typeof(_tag)!="undefined" && typeof(CspReportLib)!="undefined"&&sender.get_currentPageIndex()>0) {
	            // only fire this event on second page because the first page has been reported by default
	            CspReportLib.trackAjaxHtml($("#<%=AssetsPanelContainer.ClientID %>"));            
	        }         
	    }
	</script>
	</telerik:RadScriptBlock>
</asp:Panel>
