﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using CR.ContentObjectLibrary;
using CR.ContentObjectLibrary.Data;
using CR.ContentObjectLibrary.Factory;
using CR.DnnModules.Common;
using CR.DnnModules.Common.CategoryTree;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.UI.Modules;
using Telerik.Web.UI;
using TIEKinetix.CspModules.CSPCollateralDashboardModule.Components;
using TIEKinetix.CspModules.CSPCollateralDashboardModule.Components.Data.Lib.CAF;
using TIEKinetix.CspModules.CSPCollateralDashboardModule.Components.Data.Lib.Content;
using IContentAccessFilter = CR.ContentObjectLibrary.Interface.IContentAccessFilter;

namespace TIEKinetix.CspModules.CSPCollateralDashboardModule
{
	public partial class AssetDetailsView : DnnModuleBase
	{
		protected content CspContentPiece;
		protected List<content> CspContents;
		protected CspContents CspCategoryContentData;

		private Guid _contentId;
		public int _currentCategoryId, _currentLanguageId;
		private string _currentStageId;
		private CspDataContext _dataContext;
		private RadTreeView tree;
        private bool _isFilterCategory, _isDisableNavPanel;
        private int _cspId, _audienceId;
	    private bool _isSyncLanguage;
	    private List<category> _listChildCategories; 
        

		#region Overrides of UserControl

		/// <summary>
		/// Page Init
		/// </summary>
		/// <param name="e"></param>
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			//js & css
			Page.ClientScript.RegisterClientScriptInclude("kendo", ControlPath + "Js/kendo.web.min.js");
			Page.Header.Controls.Add(new Literal { Text = "<link href='" + ControlPath + "Css/kendo.common.min.css' type='text/css' rel='stylesheet'>" });
			Page.Header.Controls.Add(new Literal { Text = "<link href='" + ControlPath + "Css/kendo.metro.min.css' type='text/css' rel='stylesheet'>" });
			Page.Header.Controls.Add(new Literal { Text = "<link href='" + ControlPath + "Css/assetdetailview.css' type='text/css' rel='stylesheet'>" });
			//Page.Header.Controls.Add(new Literal { Text = "<link href='" + ControlPath + "Css/kendo.blueopal.min.css' type='text/css' rel='stylesheet'>" });

			_cspId = Utils.GetIntegrationKey(UserInfo);

			if (Settings[ModuleSetting.FilterCategoryTree] != null && Settings[ModuleSetting.FilterCategoryTree].ToString() == "True")
			{
				_isFilterCategory = true;
			}

			// requested content id
			_contentId = new Guid(Request.QueryString.Get("id"));

			// request language id
			if (!int.TryParse(Request.QueryString.Get("lid"), out _currentLanguageId))
				_currentLanguageId = -1;

			// requested category id
			if (!int.TryParse(Request.QueryString.Get("cid"), out _currentCategoryId))
				_currentCategoryId = -1;

            //requested audience id
            if (!int.TryParse(Request.QueryString.Get("aid"), out _audienceId))
                _audienceId = -1;

            if (Settings[ModuleSetting.SyncLanguage] != null)
            {
                _isSyncLanguage = Settings[ModuleSetting.SyncLanguage].ToString() == "1";
            }
            if (Settings[ModuleSetting.DisableNavigatePanel] != null)
            {
                _isDisableNavPanel = Settings[ModuleSetting.DisableNavigatePanel].ToString() == "1";
                if (_isDisableNavPanel)
                {
                    NavPanel.CssClass = "csp-nav-container hide";
                }
            }

			// requested stage id, this variable can be empty
			_currentStageId = !string.IsNullOrEmpty(Request.QueryString.Get("sid")) ? Request.QueryString.Get("sid") : string.Empty;

			_dataContext = new CspDataContext(Utils.GetConnectionString(PortalId));

			// language drop down list
            if (_isSyncLanguage)
            {
                var currentLang = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                var lang = CspContext.languages.FirstOrDefault(a => a.BrowserLanguageCode == currentLang);
                if (lang != null)
                {
                    if (_currentLanguageId != lang.languages_Id)
                        Response.Redirect(Globals.NavigateURL(TabId,"","mid",ModuleId.ToString()),false);
                }
                else
                {
                    if (_currentLanguageId != 1)
                        Response.Redirect(Globals.NavigateURL(TabId, "", "mid", ModuleId.ToString()), false);
                }
                PanelLanguage.Visible = false;
            }
            else
            {
                bool renderLanguages = false;
                if (_cspId == -1)
                {
                    var listLanguages = CspContext.languages.Where(
                        a => a.active.HasValue && a.active.Value).OrderBy(a => a.description).ToList();
                    foreach (language lng in listLanguages)
                    {
                        cbLanguage.Items.Add(new RadComboBoxItem
                        {
                            Text = lng.description,
                            Value = lng.languages_Id.ToString()
                        });
                        renderLanguages = true;
                    }
                }
                else
                {
                    var company = CspContext.companies.SingleOrDefault(a => a.companies_Id == _cspId);
                    if (company != null)
                    {
                        var country = DnnDataContext.CSPSubscriptionModule_Countries.FirstOrDefault(a => a.Name == company.country);
                        if (country != null)
                        {
                            var clusterCountry = DnnDataContext.CSPSubscriptionModule_Cluster_Countries
                                            .Where(a => a.CountryId == country.Id).ToList();
                            if (clusterCountry.Any())
                            {
                                var languagesIds = GetListLanguageByListClusterCountry(clusterCountry);
                                var listLanguages = new List<language>();
                                if (languagesIds != null && languagesIds.Count > 0)
                                {
                                    listLanguages =
                                        languagesIds.SelectMany(
                                            languagesId =>
                                            CspContext.languages.Where(a => a.active.HasValue && a.active.Value).Where(
                                                lng => languagesId == lng.languages_Id)).OrderBy(a => a.description).ToList();
                                    foreach (language lang in listLanguages)
                                    {
                                        if (lang != null)
                                        {
                                            cbLanguage.Items.Add(new RadComboBoxItem
                                            {
                                                Text = lang.description,
                                                Value = lang.languages_Id.ToString()
                                            });
                                        }
                                    }
                                    renderLanguages = true;
                                }
                            }
                        }
                    }
                }
                if (!renderLanguages)
                {
                    var listLanguages = CspContext.languages.Where(a => a.active.HasValue && a.active.Value).OrderBy(a => a.description).ToList();
                    foreach (language lng in listLanguages)
                    {
                        cbLanguage.Items.Add(new RadComboBoxItem
                        {
                            Text = lng.description,
                            Value = lng.languages_Id.ToString()
                        });
                    }
                }
                if (_currentLanguageId != -1)
                {
                    cbLanguage.Items.FindItemByValue(_currentLanguageId.ToString()).Selected = true;
                }
            }
            

			CspContentPiece = _dataContext.contents.SingleOrDefault(a => a.content_Id == _contentId);

			bPublish.Text = GetLocalizedText(CspContentPiece.stage_Id != Keys.CspPublishStageId ? "Button.Publish" : "Button.UnPublish");

			// role handler
			if (!ModuleRole.CountryRegionalManager(UserInfo, _currentLanguageId) && !HaveAdminPermission())
			{
				bPublish.Enabled = bUpload.Enabled = bPublish.Visible = bUpload.Visible = false;
			}
			else
			{
				bUpload.CommandArgument = GetUploadUrl();
				bUpload.OnClientClicked = "ShowUpload";
			}


			// localize
			lbLanguage.Text = GetLocalizedText("Label.Language");
			lbCategories.Text = GetLocalizedText("Label.Categories");
			lbRelatedAssets.Text = GetLocalizedText("Label.RelatedAssets");
			bUpload.Text = GetLocalizedText("Button.Upload");
		    lbAudience.Text = GetLocalizedText("Label.Audience");
			lbStages.Text = GetLocalizedText("Label.Stages");
			AssetWindow.Localization.PinOn = GetLocalizedText("Label.WindowPinOn");
			AssetWindow.Localization.PinOff = GetLocalizedText("Label.WindowPinOff");

			//localize un/publish button
			((Literal)bPublish.FindControl("lbText")).Text = GetLocalizedText(CspContentPiece.stage_Id != Keys.CspPublishStageId ? "Button.Publish" : "Button.UnPublish");

			// stage drop down list
			cbStages.Items.Add(new RadComboBoxItem(GetLocalizedText("Stage.All"), ""));
			cbStages.Items.Add(new RadComboBoxItem(GetLocalizedText("Stage.Publish"), Keys.CspPublishStageId.ToString()));
			cbStages.Items.Add(new RadComboBoxItem(GetLocalizedText("Stage.UnPublish"), Keys.CspUnPublishStageId.ToString()));

			if (!string.IsNullOrEmpty(_currentStageId))
			{
				var item = cbStages.FindItemByValue(_currentStageId);
				if (item != null)
					item.Selected = true;
			}

			SimpleSearch searchContent = (SimpleSearch)SearchExpressionFactory.CreateSearch();
		    var catId = CspContentPiece.content_main.content_categories.First().category_Id;
			searchContent.Left = SearchExpressionFactory.CreateSearch(CspContext.content_types.FirstOrDefault(a => a.content_types_Id == AssetContentTypeId));
			searchContent.Operator = SearchOperator.And;
			searchContent.Right = SearchExpressionFactory.CreateSearch();
			searchContent.Right.Left = SearchExpressionFactory.CreateSearch(CspContext.companies.FirstOrDefault(a => a.companies_Id == SupplierId));
			searchContent.Right.Operator = SearchOperator.And;
			searchContent.Right.Right = SearchExpressionFactory.CreateSearch(CspContext.languages.FirstOrDefault(a => a.languages_Id == _currentLanguageId));
		    IContentAccessFilter cAll = ContentAccessFilterFactory.CreateContentAccessFilter(searchContent, CspContext, catId); 
			CspContents = cAll.Search();

            //local audience drop down list
            if (_isFilterCategory)
            {
                AudiencePanel.Visible = true;
                foreach (var audience in ListLocalAudiences)
                {
                    cbAudience.Items.Add(new RadComboBoxItem(GetLocalizedText("LocalAudience.") + audience.AudienceName, audience.Id.ToString()));
                }
                if (_audienceId != -1)
                {
                    cbAudience.Items.FindItemByValue(_audienceId.ToString()).Selected = true;
                }
            }
		}

		#endregion

		/// <summary>
		/// Page Load
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Page_Load(object sender, EventArgs e)
		{

			tree = (RadTreeView)cbCategories.Items[0].FindControl("categoryTree");
            var companyParameter = CspContext.companies_parameters.FirstOrDefault(a => a.companies_Id == _cspId && a.companies_parameter_type.parametername == Cons.LOCAL_PARTNER_TYPE_TEXT);
			// render category if it has no nodes otherwise this is a post back
			if (tree != null && tree.Nodes.Count == 0)
			{
				List<category> listCategories = CspContext.categories.Where(a => a.active == true).ToList();


				var customCategories = new List<CustomCategory>();

                _listChildCategories = new List<category>();
                if (companyParameter != null && !string.IsNullOrEmpty(companyParameter.value_text) && _isFilterCategory && !HaveAdminPermission())
                {
                    var currentLocalPartnerType = ListLocalPartnerType.SingleOrDefault(a => a.Name == companyParameter.value_text);
                    if (currentLocalPartnerType != null)
                    {
                        var catx = listCategories.SingleOrDefault(a => a.categoryId == currentLocalPartnerType.CatgegoryId);
                        if (catx != null)
                        {
                            GetListChildCategories(catx.categoryId, true);
                        }
                    }

                }
               
				foreach (category c in listCategories)
				{
                    if (companyParameter != null && !string.IsNullOrEmpty(companyParameter.value_text) && _isFilterCategory && !HaveAdminPermission())
                    {
                        if (c.parentId == 0 & c.parentId != 1) // category not in branch
                            continue;
                        if (_listChildCategories.All(a => a.categoryId != c.categoryId) && _listChildCategories.First().depth < c.depth)
                        {
                            continue;
                        }
                    }
					var customCategory = new CustomCategory(c.categoryText, c.categoryId, c.parentId.HasValue ? c.parentId.Value : 0, c.depth.HasValue ? c.depth.Value : 0, c.lineage);
                    var contentsFromCat = GetContentsFromCategoryId(_currentLanguageId, AssetContentTypeId, SupplierId, c.categoryId);
                    if (_isFilterCategory) //Filter audience
                    {
                        var curentAudience = ListLocalAudiences.Single(a => a.Id == _audienceId);
                        contentsFromCat = contentsFromCat.Where(a => Utils.GetContentFieldValue(CspContext, a, Cons.LOCAL_AUDIENCE_FIELD_NAME) == curentAudience.AudienceName).ToList();

                    }

                    if (StageIdToNotDisplay != 0)
                    {
                        contentsFromCat = contentsFromCat.Where(a => a.stage_Id != StageIdToNotDisplay).ToList();
                    }

					customCategory.Count = contentsFromCat.Count;
					// update parent category asset counts
					if (customCategory.Count > 0)
					{
						CustomCategory temp = customCategories.FirstOrDefault(a => a.Id == customCategory.ParentId);
						while (temp != null && temp.Id != 0)
						{
							temp.Count += customCategory.Count;
							temp = customCategories.FirstOrDefault(a => a.Id == temp.ParentId);
						}
					}
					customCategories.Add(customCategory);
				}
					
				

				CategoryTreeView.RenderCategoryTree(tree, customCategories);
				//Filter tree view
				if (_isFilterCategory && !HaveAdminPermission())
				{
                    //filter display node with local partner type
                    if (companyParameter != null && !string.IsNullOrEmpty(companyParameter.value_text))
                    {
                        foreach (var localPartnerType in ListLocalPartnerType)
                        {
                            if (companyParameter.value_text == localPartnerType.Name)
                            {
                                RadTreeNode node = tree.FindNodeByValue(localPartnerType.CatgegoryId.ToString());
                                if (node != null)
                                {
                                    //remove all node at same level
                                    var listNodeToRemove = tree.GetAllNodes().Where(item => item.Value != node.Value && item.Level == node.Level).ToList();
                                    foreach (var item in listNodeToRemove)
                                    {
                                        item.Remove();
                                    }
                                }
                            }
                        }
                    }
				}
                //Mapping category - category content
                var mapCatName = new Dictionary<int, string>();
                foreach (var node in tree.GetAllNodes())
                {
                    string nodeTranslateText = string.Empty;
                    if (int.Parse(node.Value) > 0)
                    {
                        var catId = int.Parse(node.Value);

                        var categoryContent = GetContentsFromCategoryId(_currentLanguageId, CategoryContentTypeId, SupplierId, catId).SingleOrDefault(a => a.stage_Id == Keys.CspPublishStageId);
                        if (categoryContent != null)
                        {
                            nodeTranslateText = Utils.GetContentFieldValue(CspContext, categoryContent, "Content_Title");
                            if (!mapCatName.ContainsKey(catId))
                            {
                                mapCatName[catId] = nodeTranslateText;
                            }
                        }
                        else
                        {
                            nodeTranslateText = node.Text;
                        }
                    }
                    if (nodeTranslateText != string.Empty)
                        node.Text = nodeTranslateText;
                }
				RadTreeNode selectedNode = tree.FindNodeByValue(_currentCategoryId.ToString());
				if (selectedNode != null)
				{
					selectedNode.Selected = true;
					cbCategories.SelectedValue = selectedNode.Value;
				}

                if (_currentCategoryId != -1 && selectedNode != null)
                {
                    if (mapCatName.ContainsKey(_currentCategoryId))
                    {
                        if (cbCategories.SelectedItem != null)
                        {
                            cbCategories.SelectedItem.Text = mapCatName[_currentCategoryId];
                        }
                        else
                        {
                            cbCategories.Text = mapCatName[_currentCategoryId];
                        }
                    }
                    else
                    {
                        cbCategories.Text = selectedNode.Text;
                    }

                }

			}

			// breadcrumb
			RadTreeNode currentNode = null;
			currentNode = tree.FindNodeByValue(CspContentPiece.content_main.content_categories.First().category_Id.ToString());
			string url = "";
			while (currentNode != null)
			{
				url = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Assets",
										  "mid", ModuleId.ToString(),
										  "lid", _currentLanguageId.ToString(),
										  "cid", currentNode.Value);
				AssetBreadcrumbPanel.Controls.AddAt(0, new Literal { Text = string.Format("<a href='{0}'>{1}</a> &raquo; ", url, currentNode.Text) });
				currentNode = currentNode.ParentNode;
			}




			// if users are in country manager role, they can see non/publish
			if (ModuleRole.CountryRegionalManager(UserInfo, _currentLanguageId) || HaveAdminPermission())
			{
				StagePanel.Enabled = StagePanel.Visible = true;

				if (cbStages.SelectedValue == string.Empty)
                    CspContents = CspContents.GroupBy(a => a.content_Id).Select(a => a.First()).ToList(); 
				else if (cbStages.SelectedValue == Keys.CspPublishStageId.ToString())
                    CspContents = CspContents.GroupBy(a => a.content_Id).Select(a => a.First()).Where(a => a.stage_Id == Keys.CspPublishStageId).ToList(); 
				else
                    CspContents = CspContents.GroupBy(a => a.content_Id).Select(a => a.First()).Where(a => a.stage_Id != Keys.CspPublishStageId).ToList();
			}
			else
			{
                CspContents = CspContents.GroupBy(a => a.content_Id).Select(a => a.First()).Where(a => a.stage_Id == Keys.CspPublishStageId).ToList(); 
			}

            if (StageIdToNotDisplay != 0)
            {
                CspContents = CspContents.Where(a => a.stage_Id != StageIdToNotDisplay).ToList();
            }


		    AssetRotator.DataSource = CspContents;
			AssetRotator.DataBind();
			if (AssetRotator.Items.Count < 6)
			{
				AssetRotator.WrapFrames = false;
			}

			cbCategories.EmptyMessage = GetLocalizedText("CategoryNav.ShowAll");
		}

        /// <summary>
        /// Gets the list child categories.
        /// </summary>
        /// <param name="parentCategoryId">The parent category id.</param>
        /// <param name="first">if set to <c>true</c> [first].</param>
        private void GetListChildCategories(int parentCategoryId, bool first)
        {
            if (first)
                _listChildCategories.Add(CspContext.categories.Single(a => a.categoryId == parentCategoryId));
            var list = CspContext.categories.Where(a => a.parentId == parentCategoryId && a.active == true);
            _listChildCategories.AddRange(list);
            foreach (var category in list)
            {
                GetListChildCategories(category.categoryId, false);
            }
        }

        /**
         * Get & distinct List LanguageIds to display in dropdownlist
         * Author: Ngo Quang Phat
         */
        public List<int> GetListLanguageByListClusterCountry(List<CSPSubscriptionModule_Cluster_Country> clusterCountries)
        {
            List<int> list = new List<int>();
            foreach (CSPSubscriptionModule_Cluster_Country cspSubscriptionModuleCluster in clusterCountries)
            {
                List<CSPSubscriptionModule_Cluster_Language> moduleClusterLanguages = DnnDataContext.CSPSubscriptionModule_Cluster_Languages.Where(
                    a => a.ClusterId == cspSubscriptionModuleCluster.ClusterId).ToList();
                list.AddRange(moduleClusterLanguages.Select(cspSubscriptionModuleClusterLanguage => cspSubscriptionModuleClusterLanguage.LanguageId));
            }
            return list.Distinct().ToList();
        } 

		#region Control Event Handlers


		/// <summary>
		/// language drop down changed - users select a language
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void LanguageCombobox_LanguageChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
		{
			Response.Redirect(Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Assets",
												  "mid", ModuleId.ToString(),
												  "lid", e.Value,
												  "cid", _currentCategoryId.ToString()), false);
		}

		/// <summary>
		/// stage changed - users select another filter for stage
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void StageCombobox_StageChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
		{
			string url = string.Empty;

			if (StagePanel.Enabled && !string.IsNullOrEmpty(cbStages.SelectedValue))
			{
                if (AudiencePanel.Visible)
                {
                    url = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Assets",
                                          "mid", ModuleId.ToString(),
                                          "lid", _currentLanguageId.ToString(),
                                          "cid", _currentCategoryId.ToString(),
                                          "sid", e.Value,
                                          "aid", _audienceId.ToString());
                }
                else
                {
                    url = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Assets",
                                          "mid", ModuleId.ToString(),
                                          "lid", _currentLanguageId.ToString(),
                                          "cid", _currentCategoryId.ToString(),
                                          "sid", e.Value);
                }
				
			}
			else
			{
                if (AudiencePanel.Visible)
                {
                    url = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Assets",
                                          "mid", ModuleId.ToString(),
                                          "lid", _currentLanguageId.ToString(),
                                          "cid", _currentCategoryId.ToString(),
                                          "aid", _audienceId.ToString());
                }
                else
                {
                    url = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Assets",
                                          "mid", ModuleId.ToString(),
                                          "lid", _currentLanguageId.ToString(),
                                          "cid", _currentCategoryId.ToString());
                }
				
			}

			Response.Redirect(url, false);
		}

        /// <summary>
        /// Handles the OnSelectedIndexChanged event of the cbAudience control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 11/13/2012 - 6:36 PM
        protected void cbAudience_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            string url = string.Empty;

            if (StagePanel.Enabled && !string.IsNullOrEmpty(cbStages.SelectedValue))
            {
                if (AudiencePanel.Visible)
                {
                    url = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Assets",
                                          "mid", ModuleId.ToString(),
                                          "lid", _currentLanguageId.ToString(),
                                          "cid", _currentCategoryId.ToString(),
                                          "sid", _currentStageId,
                                          "aid", e.Value);
                }
                else
                {
                    url = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Assets",
                                          "mid", ModuleId.ToString(),
                                          "lid", _currentLanguageId.ToString(),
                                          "cid", _currentCategoryId.ToString(),
                                          "sid", _currentStageId);
                }

            }
            else
            {
                if (AudiencePanel.Visible)
                {
                    url = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Assets",
                                          "mid", ModuleId.ToString(),
                                          "lid", _currentLanguageId.ToString(),
                                          "cid", _currentCategoryId.ToString(),
                                          "aid", e.Value);
                }
                else
                {
                    url = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Assets",
                                          "mid", ModuleId.ToString(),
                                          "lid", _currentLanguageId.ToString(),
                                          "cid", _currentCategoryId.ToString());
                }

            }

            Response.Redirect(url, false);
        }

		/// <summary>
		/// category changed - users select a category
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void CategoryTreeOnNodeClick(object sender, RadTreeNodeEventArgs e)
		{
            if (AudiencePanel.Visible)
            {
                Response.Redirect(Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Assets",
                                                  "mid", ModuleId.ToString(),
                                                  "lid", _currentLanguageId.ToString(),
                                                  "cid", e.Node.Value,
                                                  "aid", _audienceId.ToString()), false);
            }
            else
            {
                Response.Redirect(Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Assets",
                                                  "mid", ModuleId.ToString(),
                                                  "lid", _currentLanguageId.ToString(),
                                                  "cid", e.Node.Value), false);
            }
			
		}

		/// <summary>
		/// users click on a banner in the asset rotator
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void AssetRotatorOnItemClick(object sender, RadRotatorEventArgs e)
		{
			string url = string.Empty;

			if (StagePanel.Enabled && !string.IsNullOrEmpty(cbStages.SelectedValue))
			{
                if (AudiencePanel.Visible)
                {
                    url = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Details",
                                          "mid", ModuleId.ToString(),
                                          "id", ((Image)e.Item.FindControl("AssetImage")).Attributes["Value"],
                                          "lid", _currentLanguageId.ToString(),
                                          "cid", _currentCategoryId.ToString(),
                                          "sid", cbStages.SelectedValue,
                                          "aid", cbAudience.SelectedValue);
                }
                else
                {
                    url = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Details",
                                          "mid", ModuleId.ToString(),
                                          "id", ((Image)e.Item.FindControl("AssetImage")).Attributes["Value"],
                                          "lid", _currentLanguageId.ToString(),
                                          "cid", _currentCategoryId.ToString(),
                                          "sid", cbStages.SelectedValue);
                }
				
			}
			else
			{
                if (AudiencePanel.Visible)
                {
                    url = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Details",
                                          "mid", ModuleId.ToString(),
                                          "id", ((Image)e.Item.FindControl("AssetImage")).Attributes["Value"],
                                          "lid", _currentLanguageId.ToString(),
                                          "cid", _currentCategoryId.ToString(),
                                          "aid", cbAudience.SelectedValue);
                }
                else
                {
                    url = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Details",
                                          "mid", ModuleId.ToString(),
                                          "id", ((Image)e.Item.FindControl("AssetImage")).Attributes["Value"],
                                          "lid", _currentLanguageId.ToString(),
                                          "cid", _currentCategoryId.ToString());
                }
				
			}

			Response.Redirect(url, true);

		}

		/// <summary>
		/// button event handlers - users click on Preview, Download, Upload and Publish buttons
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void ModuleButtonOnClickEventHandler(object sender, EventArgs e)
		{
			RadButton button = (RadButton)sender;
			switch (button.ID)
			{
				case "bUpload":
					var url = Globals.NavigateURL(TabId, "Upload",
												  "mid", ModuleId.ToString(),
												  "id", _contentId.ToString(),
												  "lid", _currentLanguageId.ToString(),
												  "cid", _currentCategoryId.ToString());
					AssetWindow.NavigateUrl = url + "?popUp=true";
					AssetWindow.VisibleOnPageLoad = true;
					//AssetWindow.Width = new Unit(750);
					//AssetWindow.Height = new Unit(400);
					AssetWindow.AutoSize = true;
					break;
				case "bPublish":
					AssetWindow.VisibleOnPageLoad = false;
					content cspContent = _dataContext.contents.FirstOrDefault(a => a.content_Id == _contentId);
					if (cspContent != null && cspContent.stage_Id.HasValue)
					{
						cspContent.stage_Id = (cspContent.stage_Id.Value == Keys.CspPublishStageId ? Keys.CspUnPublishStageId : Keys.CspPublishStageId);
						_dataContext.SubmitChanges();
						((Literal)bPublish.FindControl("lbText")).Text = GetLocalizedText(cspContent.stage_Id != Keys.CspPublishStageId ? "Button.Publish" : "Button.UnPublish");
					}
					break;
				default:
					break;
			}
		}

		/// <summary>
		/// Gets the asset download link.
		/// </summary>
		/// <param name="dataItem">The data item.</param>
		/// <param name="fieldUrl">The field URL.</param>
		/// <returns></returns>
		public string GetAssetDownloadLink(object dataItem, string fieldUrl)
		{
			var content = (content)dataItem;
			var url = Utils.GetContentFieldValue(CspContext,content, fieldUrl);
			if (url != string.Empty == url.Contains(Cons.CONSUMER_DOMAIN_REPLACE_TEXT))
			{
				if (_cspId != -1) //Subs
				{
					var consumer = CspContext.companies_consumers.FirstOrDefault(a => a.companies_Id == _cspId);
					if (consumer != null)
					{
						var baseDomain = consumer.base_domain;
						url = url.Replace(Cons.CONSUMER_DOMAIN_REPLACE_TEXT, baseDomain);
					}
				}
				else //Normal user - Admin, then use default url from module setting
				{
					if (Settings[ModuleSetting.BaseDomain] != null && Settings[ModuleSetting.BaseDomain].ToString() != string.Empty)
					{
						url = url.Replace(Cons.CONSUMER_DOMAIN_REPLACE_TEXT, Settings[ModuleSetting.BaseDomain].ToString());
					}
				}
			}
			return EncodeAssetUrl(url);
		}

	    public string EncodeAssetUrl(string url)
	    {
            if (Settings[ModuleSetting.SecureAssets]!=null&&Settings[ModuleSetting.SecureAssets].Equals(true.ToString()))
            {
                url = Globals.NavigateURL(TabId, "download", string.Format("mid={0}&f={1}&id={2}", ModuleId, UrlPathEncode(url), _contentId));
            }
	        return url;
	    }

		protected string GetUploadUrl()
		{
			return Globals.NavigateURL(TabId, "Upload",
									   "mid", ModuleId.ToString(),
									   "id", _contentId.ToString(),
									   "lid", _currentLanguageId.ToString(),
									   "cid", _currentCategoryId.ToString()) + "?popUp=true";
		}

		#endregion

        private string UrlPathEncode(string s)
        {
            string temp = HttpUtility.UrlPathEncode(Encode(s));
            temp = temp.Replace("/", "-l"+Convert.ToInt32('/')+"tu".ToString());
            temp = temp.Replace("+", "-l"+Convert.ToInt32('+')+"tu".ToString());
            temp = temp.Replace("=", "-l" + Convert.ToInt32('=') + "tu".ToString());
            return temp;
        }
	}

}