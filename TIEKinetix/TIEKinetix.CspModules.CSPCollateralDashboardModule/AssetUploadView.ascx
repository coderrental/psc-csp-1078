﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssetUploadView.ascx.cs" Inherits="TIEKinetix.CspModules.CSPCollateralDashboardModule.AssetUploadView" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>
<telerik:RadAjaxManager ID="ajaxManager" runat="server"></telerik:RadAjaxManager>
<telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server"></telerik:RadAjaxLoadingPanel>
<div class="asset-upload-container csp-clear" id="asset-upload-container">
    <table>
        <tbody>
            <tr>
                <td valign="top" align="right" class="upload-field-label">                    
                    <dnn:label runat="server" id="lbAssetName"></dnn:label>                    
                </td>
                <td valign="top">
                    <asp:TextBox runat="server" ID="tbAssetName" CssClass="upload-field" Width="450px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="tbAssetName" ValidationGroup="AssetUpload" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td valign="top" align="right" class="upload-field-label">
                    <dnn:label runat="server" id="lbAssetDesc"></dnn:label>
                </td>
                <td valign="top">
                    <telerik:RadEditor runat="server" ID="tbAssetDesc" CssClass="upload-field" Height="250" Width="450px"></telerik:RadEditor>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tbAssetDesc" ValidationGroup="AssetUpload" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td valign="top" align="right" class="upload-field-label">
                    <dnn:label runat="server" id="lbAssetFile"></dnn:label>                    
                </td>
                <td valign="top">
                    <asp:FileUpload ID="uploadAssetFile" runat="server" CssClass="upload-field" Resize="false"  />
                    <asp:CustomValidator runat="server" ID="validUploadFile" ControlToValidate="uploadAssetFile"></asp:CustomValidator>
                </td>
            </tr>
			<asp:Panel runat="server" ID="pnImgUpload">
            <tr id="PreviewImageUploadBox">
                <td valign="top" align="right" class="upload-field-label">
                    <dnn:label runat="server" id="lbPreviewImage"></dnn:label>                    
                </td>
                <td valign="top">
                    <asp:FileUpload runat="server" CssClass="upload-field" ID="uploadPreviewImage" Resize="true" Resize-Width="300" Resize-Height="500" />
                    <asp:CustomValidator runat="server" ID="validUploadPreviewImage" ControlToValidate="uploadPreviewImage"></asp:CustomValidator>
                </td>
            </tr>
            <tr id="TileImageUploadBox">
                <td valign="top" align="right" class="upload-field-label">
                    <dnn:label runat="server" id="lbTileImage"></dnn:label>                    
                </td>
                <td valign="top">
                    <asp:FileUpload runat="server" CssClass="upload-field" ID="uploadTileImage" Resize="true" Resize-Width="300" Resize-Height="250" />
                    <asp:CustomValidator runat="server" ID="validTileImage" ControlToValidate="uploadTileImage"></asp:CustomValidator>
                </td>
            </tr>
			</asp:Panel>
        </tbody>        
        <tfoot>
            <tr>
                <td colspan="2" class="upload-buttons">
                    <ul>
                        <li>
                            <telerik:RadButton ID="bSubmit" runat="server" Text="RadButton" CssClass="csp-button" OnClick="ButtonOnClickEventHandler" ValidationGroup="AssetUpload" OnClientClicked="OnClientClicked" cspobj="REPORT" csptype="links" cspenglishvalue="UploadNewAsset">
                                <ContentTemplate>
                                    <span class="span_submit">
                                        <center><%=GetLocalizedText("Button.Submit")%></center>
                                    </span>
                                </ContentTemplate>
                            </telerik:RadButton>
                        </li>
                        <li style="padding-top:3px;">
                            <asp:Label ID="lbNotification" CssClass="csp-error" runat="server"></asp:Label>
                        </li>
                    </ul>
                </td>
            </tr>
        </tfoot>
    </table>
	<telerik:RadScriptBlock runat="server">
    <script type="text/javascript">
    	function OnClientClicked() {
    		if (typeof (Page_ClientValidate) == 'function') {
    			Page_ClientValidate();
    		}
    		if (Page_IsValid) {
    			var currentLoadingPanel = $find("<%= loadingPanel.ClientID %>");
    			currentLoadingPanel.hide();
    			currentLoadingPanel.show("Body");
    		}
    		else {
    			$("#<%=lbNotification.ClientID %>").text('<%=GetLocalizedText("AssetUploadView.NeedMandatoryFields") %>');
    		}
    	}
    	function CloseWindow() {
    		//Close kendo window
    		top.location.href = '<%= GetDetailPageUrl() %>';
    		window.parent.$("#kWin").data("kendoWindow").close();
    	}

    	if ($.browser.msie && $.browser.version <= 7) {
    		$("span.span_submit").each(function () { $(this).css("line-height", "20px"); });
    	}

    </script>
	</telerik:RadScriptBlock>
    <asp:Literal ID="lScript" runat="server"></asp:Literal>
</div>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
<script type="text/javascript" src="<%=PortalSettings.ActiveTab.SkinSrc.Substring(0, PortalSettings.ActiveTab.SkinSrc.LastIndexOf("/")) %>/js/webtrends.js"></script>
<script type="text/javascript">
	var cspConsumerInfo = <%=ConsumerInfoJson %>;
</script>

<script type="text/javascript">
	var _tag = new WebTrends();
	_tag.DCSext.csp_stype = _tag.DCSext.csp_sname = "Windows BG - Portal";
	_tag.DCSext.csp_pageTitle = document.title;
	if (typeof (cspConsumerInfo) != "undefined") {
		_tag.DCSext.language = cspConsumerInfo.lngDesc;
	}
	else {
		var cspConsumerInfo = {};
	}
    // tag all clickable links
	$(".dnnPrimaryAction,.dnnSecondaryAction").each(function () {
		var el = $(this);
		if (!el.attr("cspobj")) {
			el.attr("cspobj", "REPORT").attr("csptype", "links").attr("cspenglishvalue", el.text());
		}

	});
</script>
<noscript><img alt="dcsimg" id="dcsimg" width="1" height="1" src="//statse.webtrendslive.com/dcsfdyh2000000k3gm9zjzc6r_9h1c/njs.gif?dcsuri=/nojavascript&amp;WT.js=No&amp;WT.tv=10.2.10&amp;dcssip=www.syndicationtest.com"/></noscript>
<script type="text/javascript" src="<%=PortalSettings.ActiveTab.SkinSrc.Substring(0, PortalSettings.ActiveTab.SkinSrc.LastIndexOf("/")) %>/js/CspReportLib.js"></script>
</telerik:RadScriptBlock>

