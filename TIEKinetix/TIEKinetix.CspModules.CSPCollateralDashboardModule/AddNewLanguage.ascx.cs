﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TIEKinetix.CspModules.CSPCollateralDashboardModule
{
    public partial class AddNewLanguage : System.Web.UI.UserControl
    {
        private object _dataItem = null;

        public object DataItem
        {
            get
            {
                return this._dataItem;
            }
            set
            {
                this._dataItem = value;
            }
        }

        #region Overrides of UserControl

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            cbLanguages.DataTextField = "description";
            cbLanguages.DataValueField = "languages_Id";

            DataBinding += new EventHandler(AddNewLanguageDataBinding);
        }

        void AddNewLanguageDataBinding(object sender, EventArgs e)
        {
            var props = DataItem;
            if (props != null)
            {
                cbLanguages.DataSource = props;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}