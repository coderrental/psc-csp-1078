﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleSettings.ascx.cs" Inherits="TIEKinetix.CspModules.CSPCollateralDashboardModule.ModuleSettings" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div>
    <table>
        <tr>
            <td>Supplier Id</td>
            <td>
                <telerik:RadTextBox ID="tbSupplier" runat="server"></telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td>Asset Content Type Id</td>
            <td>
                <telerik:RadTextBox ID="tbContentTypeId" runat="server"></telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td>Category Content Type Id</td>
            <td>
                <telerik:RadTextBox ID="tbCategoryContentTypeId" runat="server"></telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td>Number of days to show new asset indicator</td>
            <td>
                <telerik:RadTextBox ID="tbNewAssetNumOfDays" runat="server"></telerik:RadTextBox>
            </td>
        </tr>
	    <tr>
		    <td>Filter category tree</td>
		    <td>
			    <asp:CheckBox runat="server" ID="chbxFilterCategory"/>
		    </td>
	    </tr>
	    <tr>
		    <td>Defaul Base domain (Do not include http, eg. basedomain.com)</td>
		    <td>
			    <telerik:RadTextBox runat="server" ID="tbDefaultBaseDomain"/>
		    </td>
	    </tr>
        <tr>
            <td>Tab Name To Redirect</td>
            <td>
                <telerik:RadTextBox runat="server" ID="tbTabId"></telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td>Stage Id to not display contents</td>
            <td>
                <telerik:RadTextBox runat="server" ID="tbStageId"></telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td>Sync Language?</td>
            <td>
                <asp:CheckBox runat="server" ID="cbxSyncLang"/>
            </td>
        </tr>
        <tr>
            <td>Secure Asset</td>
            <td>
                <asp:CheckBox runat="server" ID="cbxSecureAssets"/>
            </td>
        </tr>
        <tr>
            <td>Secure Asset Path</td>
            <td>
                <asp:TextBox runat="server" ID="tbSecureAssetPath"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Disable Navigation Panel?</td>
            <td>
                <asp:CheckBox runat="server" ID="cbxDisableNavPanel"/>
            </td>
        </tr>
	    <tr>
			    <td colspan="2">
				    <hr />
			    </td>
		    </tr>
		    <tr>
			    <td style="width: 300px;">Choose User(s) with Admin right permission</td>
			    <td>
				    <p style="text-align: center;"><telerik:RadTextBox EmptyMessage="Enter Username to filter..." runat="server" ID="tbUserFilter" onkeyup="filterList();"/></p>
				    <telerik:RadListBox Width="180px" runat="server" SelectionMode="Multiple" OnClientDropped="OnClientDroppedHandler" ID="listUsers" OnClientLoad="listBoxLoad" CheckBoxes="True"></telerik:RadListBox>			
			    </td>
		    </tr>
    </table>
    <style type="text/css">
	    ul li { list-style: none;}
	    .rlbGroup, .rlbList { overflow: auto !important; }
    </style>
    <script type="text/javascript">
	    var listbox;
	    var filterTextBox;

	    function pageLoad() {
		    listbox = $find("<%= listUsers.ClientID %>");
		    filterTextBox = document.getElementById("<%= tbUserFilter.ClientID %>");
		    // set on anything but text box
		    listbox._getGroupElement().focus();
	    }
	    function listBoxLoad(sender, args) {
		    var elementHeight = $telerik.getSize(sender.get_items().getItem(0).get_element()).height;
		    if (sender.get_items().get_count() > 10)
			    sender._groupElement.style.height = (elementHeight * 10) + "px";
	    }

	    function filterList() {
		    clearListEmphasis();
		    createMatchingList();
	    }

	    // clear emphasis from matching characters for entire list
	    function clearListEmphasis() {
		    var re = new RegExp("</{0,1}em>", "gi");
		    var items = listbox.get_items();
		    var itemText;

		    items.forEach(
			    function (item) {
				    itemText = item.get_text();
				    item.set_text(clearTextEmphasis(itemText));
			    }
		    );
	    }

	    // hide listboxitems without matching characters
	    function createMatchingList() {
		    var items = listbox.get_items();
		    var filterText = filterTextBox.value;
		    var re = new RegExp(filterText, "i");

		    items.forEach(
			    function (item) {
				    var itemText = item.get_text();

				    if (itemText.toLowerCase().indexOf(filterText.toLowerCase()) != -1) {
					    item.set_text(itemText.replace(re, "<em>" + itemText.match(re) + "</em>"));
					    item.set_visible(true);
				    } else {
					    item.set_visible(false);
				    }
			    }
		    );
	    }

	    // clear emphasis from matching characters for given text
	    function clearTextEmphasis(text) {
		    var re = new RegExp("</{0,1}em>", "gi");
		    return text.replace(re, "");
	    }

	    function OnClientDroppedHandler(sender, eventArgs) {
		    // clear emphasis from matching characters
		    eventArgs.get_sourceItem().set_text(clearTextEmphasis(eventArgs.get_sourceItem().get_text()));
	    }
    </script>
</div>
<div>
    <h2>Customize Campaign Local Audience</h2>
    <asp:HiddenField runat="server" ID="hfListAudience" />
    <table id="tbListAudience" class="setting_list_tb">
        <tr class="heading">
            <th style="width: 200px;">Local Audience</th>
            <th>Action</th>
        </tr>
        <%= GetListOfLocalAudienceLayout() %>
    </table>
    <table>
        <tr>
            <td>
                <asp:TextBox Width="198" runat="server" ID="tbxAudience"></asp:TextBox>
            </td>
            <td>
                <telerik:RadButton runat="server" Text="Add" ID="btnAddAudience" AutoPostBack="False">
                    <Icon PrimaryIconCssClass="rbAdd" PrimaryIconLeft="4" PrimaryIconTop="4"></Icon>
                </telerik:RadButton>
            </td>
        </tr>
    </table>
</div>
<div>
    <h2>Customize Local Partner Type</h2>
    <asp:HiddenField runat="server" ID="hfListPartnerType"/>
    <table id="tbListPartnerType" class="setting_list_tb">
        <tr class="heading">
            <th style="width: 140px;">Local Parner Type</th>
            <th style="width: 90px;">Category Id</th>
            <th>Action</th>
        </tr>
        <%= GetListOfLocalPartnerTypeLayout() %>
    </table>
    <table>
        <tr>
            <td>
                <asp:TextBox Width="138" runat="server" ID="tbxPartnerType"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox  Width="88" runat="server" ID="tbxPartnerTypeCatId"></asp:TextBox>
            </td>
            <td>
                <telerik:RadButton runat="server" Text="Add" ID="btnAddPartnerType" AutoPostBack="False">
                    <Icon PrimaryIconCssClass="rbAdd" PrimaryIconLeft="4" PrimaryIconTop="4"></Icon>
                </telerik:RadButton>
            </td>
        </tr>
    </table>
</div>
<telerik:RadScriptBlock runat="server">
    <script type="text/javascript">
        jQuery(function () {
            Init();
        });

        function Init() {

            //Add new partner type
            jQuery("#<%= btnAddPartnerType.ClientID %>").click(function (event) {
                var name = jQuery("#<%= tbxPartnerType.ClientID %>").val();
                var catid = jQuery("#<%= tbxPartnerTypeCatId.ClientID %>").val();
                if (name == "" || catid == "")
                    return false;
                var list = jQuery("#<%= hfListPartnerType.ClientID %>").val();
                list = list + "," + name + "-" + catid;
                jQuery("#<%= hfListPartnerType.ClientID %>").val(list);
                jQuery("#<%= tbxPartnerType.ClientID %>").val("");
                jQuery("#<%= tbxPartnerTypeCatId.ClientID %>").val("");
                jQuery("#tbListPartnerType").append("<tr class='row'><td class='name'>" + name + "</td><td class='id'>" + catid + "</td><td><a href='#' class='delete_partnertype'>Delete</a></td></tr>");
                Init();
                event.preventDefault();
                return false;
            });

            // Remove a partner type
            jQuery(".delete_partnertype").click(function () {
                var name = jQuery(this).parents(".row").children(".name").html();
                var catid = jQuery(this).parents(".row").children(".id").html();
                var list = jQuery("#<%= hfListPartnerType.ClientID %>").val();
                jQuery("#<%= hfListPartnerType.ClientID %>").val(list.split(name + "-" + catid).join(""));
                jQuery(this).parents(".row").fadeOut();
                return false;
            });

            //Remove an audience
            jQuery(".delete_audience").click(function () {
                var audienceName = jQuery(this).parents(".row").children(".name").html();
                var list = jQuery("#<%= hfListAudience.ClientID %>").val();
                jQuery("#<%= hfListAudience.ClientID %>").val(list.split(audienceName).join(""));
                jQuery(this).parents(".row").fadeOut();
                return false;
            });

            //Add new audience
            jQuery("#<%= btnAddAudience.ClientID %>").click(function (event) {
                var value = jQuery("#<%= tbxAudience.ClientID %>").val();
                if (value == "")
                    return false;

                var list = jQuery("#<%= hfListAudience.ClientID %>").val();
                list = list + "," + value;
                jQuery("#<%= hfListAudience.ClientID %>").val(list);
                jQuery("#<%= tbxAudience.ClientID %>").val("");

                jQuery("#tbListAudience").append("<tr class='row'><td class='name'>" + value + "</td><td><a href='#' class='delete_audience'>Delete</a></td></tr>");
                Init();
                event.preventDefault();
                return false;
            });
        }
    </script>
</telerik:RadScriptBlock>