﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TIEKinetix.CspModules.CSPCollateralDashboardModule.Components.Data;
using CR.ContentObjectLibrary.Data;

namespace TIEKinetix.CspModules.CSPCollateralDashboardModule
{
    public partial class AddNewPermission : System.Web.UI.UserControl
    {
        #region Overrides of UserControl

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            cbUsers.DataTextField = "Username";
            cbUsers.DataValueField = "UserID";
            DataBinding += new EventHandler(AddNewPermissionDataBinding);
        }

        void AddNewPermissionDataBinding(object sender, EventArgs e)
        {
            var props = DataItem;
            if (props != null)
            {
                cbUsers.DataSource = props;
            }
        }

        #endregion
        private object _dataItem = null;
        public object DataItem
        {
            get
            {
                return _dataItem;
            }
            set
            {
                _dataItem = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}