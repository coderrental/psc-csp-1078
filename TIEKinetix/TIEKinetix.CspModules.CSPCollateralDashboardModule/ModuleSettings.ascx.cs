﻿// ***********************************************************************
// Assembly         : TIEKinetix.CspModules.CSPCollateralDashboardModule
// Author           : VU DINH
// Created          : 11-22-2012
//
// Last Modified By : VU DINH
// Last Modified On : 01-20-2013
// ***********************************************************************
// <copyright file="ModuleSettings.ascx.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using DotNetNuke.Entities.Modules;
using TIEKinetix.CspModules.CSPCollateralDashboardModule.Components;
using DotNetNuke.Entities.Users;
using TIEKinetix.CspModules.CSPCollateralDashboardModule.Entities;
using Telerik.Web.UI;

namespace TIEKinetix.CspModules.CSPCollateralDashboardModule
{
    /// <summary>
    /// Class ModuleSettings
    /// </summary>
    public partial class ModuleSettings : ModuleSettingsBase
    {
        private List<LocalAudience> _listLocalAudience;
        private List<LocalPartnerType> _listLocalPartnerType; 

        /// <summary>
        /// Loads the settings.
        /// </summary>
        public override void LoadSettings()
        {

            #region [ Load Local Audience list ]
            _listLocalAudience = new List<LocalAudience>();
            if (Settings[Cons.SETTING_LIST_LOCAL_AUDIENCES] != null)
            {
                var listAudiencearr = Settings[Cons.SETTING_LIST_LOCAL_AUDIENCES].ToString().Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                if (listAudiencearr.Any())
                {
                    foreach (var item in listAudiencearr)
                    {
                        hfListAudience.Value += item + ",";
                        _listLocalAudience.Add(new LocalAudience
                        {
                            AudienceName = item,
                            Id = listAudiencearr.IndexOf(item)
                        });
                    }
                }
            }
            #endregion

            #region [Load Local Partner type list ]
            _listLocalPartnerType = new List<LocalPartnerType>();
            if (Settings[Cons.SETTING_LIST_LOCAL_PARTNER_TYPE] != null)
            {
                var listPartnerTypeArr = Settings[Cons.SETTING_LIST_LOCAL_PARTNER_TYPE].ToString().Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                if (listPartnerTypeArr.Any())
                {
                    foreach (var item in listPartnerTypeArr)
                    {
                        var arr = item.Split(new[] { "-" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        hfListPartnerType.Value += item + ",";
                        _listLocalPartnerType.Add(new LocalPartnerType
                        {
                            Name = arr[0],
                            CatgegoryId = int.Parse(arr[1])
                        });
                    }
                }
            }
            #endregion

            if (TabModuleSettings[ModuleSetting.SupplierId] != null)
                tbSupplier.Text = TabModuleSettings[ModuleSetting.SupplierId].ToString();
            if (TabModuleSettings[ModuleSetting.AssetContentTypeId] != null)
                tbContentTypeId.Text = TabModuleSettings[ModuleSetting.AssetContentTypeId].ToString();
            if (TabModuleSettings[ModuleSetting.CategoryContentTypeId] != null)
                tbCategoryContentTypeId.Text = TabModuleSettings[ModuleSetting.CategoryContentTypeId].ToString();
            if (TabModuleSettings[ModuleSetting.NewAssetNumOfDays] != null)
                tbNewAssetNumOfDays.Text = TabModuleSettings[ModuleSetting.NewAssetNumOfDays].ToString();
			if (TabModuleSettings[ModuleSetting.FilterCategoryTree] != null)
				chbxFilterCategory.Checked = TabModuleSettings[ModuleSetting.FilterCategoryTree].ToString() == "True";
			if (TabModuleSettings[ModuleSetting.BaseDomain] != null)
				tbDefaultBaseDomain.Text = TabModuleSettings[ModuleSetting.BaseDomain].ToString();
            if (TabModuleSettings[ModuleSetting.CompanySettingTabName] != null)
                tbTabId.Text = TabModuleSettings[ModuleSetting.CompanySettingTabName].ToString();
            if (TabModuleSettings[ModuleSetting.SyncLanguage] != null)
                cbxSyncLang.Checked = TabModuleSettings[ModuleSetting.SyncLanguage].ToString() == "1";
            if (TabModuleSettings[ModuleSetting.DisableNavigatePanel] != null)
                cbxDisableNavPanel.Checked = TabModuleSettings[ModuleSetting.DisableNavigatePanel].ToString() == "1";
            if (TabModuleSettings[ModuleSetting.StageIdToNotDisplay] != null)
                tbStageId.Text = TabModuleSettings[ModuleSetting.StageIdToNotDisplay].ToString();
            
            // ltu: 12/11/2013 added secure assets flag
            if (TabModuleSettings[ModuleSetting.SecureAssets] != null)
                cbxSecureAssets.Checked = Convert.ToBoolean(TabModuleSettings[ModuleSetting.SecureAssets]);
            if (TabModuleSettings[ModuleSetting.SecureAssetPath] != null)
                tbSecureAssetPath.Text = TabModuleSettings[ModuleSetting.SecureAssetPath].ToString();

            if (TabModuleSettings[ModuleSetting.FilterCategoryTree] != null)
                chbxFilterCategory.Checked = TabModuleSettings[ModuleSetting.FilterCategoryTree].ToString() == "True";


			//Setup list user for admin rights permission
			var objRoleController = new DotNetNuke.Security.Roles.RoleController();
			var arrayList = objRoleController.GetUsersByRoleName(PortalId, PortalSettings.RegisteredRoleName);
        	var listUser = arrayList.Cast<UserInfo>().OrderBy(a => a.Username).ToList();
        	var usersSeletect = string.Empty;
			if (TabModuleSettings[ModuleSetting.UserWithAdminRole] != null)
				usersSeletect = TabModuleSettings[ModuleSetting.UserWithAdminRole].ToString();
        	var listUsersSeleted = new List<string>();
			if (usersSeletect != string.Empty)
			{
				listUsersSeleted = usersSeletect.Split(',').ToList();
			}
        	foreach (var userInfo in listUser)
        	{
				if (userInfo.IsSuperUser || userInfo.IsInRole(PortalSettings.AdministratorRoleName))
					continue;
        		listUsers.Items.Add(new RadListBoxItem(userInfo.Username,userInfo.UserID.ToString()));
        	}

			foreach (var userSeleted in listUsersSeleted)
			{
				listUsers.FindItemByValue(userSeleted).Checked = true;
			}
			
        }

        /// <summary>
        /// Updates the settings.
        /// </summary>
        public override void UpdateSettings()
        {
            ModuleController objModules = new ModuleController();
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.SupplierId, tbSupplier.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.AssetContentTypeId, tbContentTypeId.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.CategoryContentTypeId, tbCategoryContentTypeId.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.NewAssetNumOfDays, tbNewAssetNumOfDays.Text);
			objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.FilterCategoryTree, chbxFilterCategory.Checked.ToString());
			objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.BaseDomain, tbDefaultBaseDomain.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.CompanySettingTabName, tbTabId.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.SyncLanguage, cbxSyncLang.Checked ? "1" : "0");
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.DisableNavigatePanel, cbxDisableNavPanel.Checked ? "1" : "0");
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.StageIdToNotDisplay, tbStageId.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.SecureAssets, cbxSecureAssets.Checked.ToString());
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.SecureAssetPath, tbSecureAssetPath.Text);

			//Update List users with admin role
        	string userWithAdminRole = listUsers.CheckedItems.Aggregate(string.Empty, (current, radListBoxItem) => current + (radListBoxItem.Value + ","));
			if (userWithAdminRole != string.Empty)
				userWithAdminRole = userWithAdminRole.Substring(0, userWithAdminRole.Length - 1); //Remove last commas
        	objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.UserWithAdminRole, userWithAdminRole);

            #region [ Save Local Audience list ]
            objModules.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_LIST_LOCAL_AUDIENCES, hfListAudience.Value);
            //update resx file
            AddAudienceTranslate(hfListAudience.Value);
            #endregion

            #region [ Save Local Audience list ]
            objModules.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_LIST_LOCAL_PARTNER_TYPE, hfListPartnerType.Value);
            #endregion

        	//refresh cache
            ModuleController.SynchronizeModule(ModuleId);        

        }

        /// <summary>
        /// Adds the audience translate.
        /// </summary>
        private void AddAudienceTranslate(string data)
        {
            var listAudience = data.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
            if (listAudience.Any())
            {
                var filename = Server.MapPath(ControlPath + "App_LocalResources\\CSPCollateralDashboardModule.ascx.resx");
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(filename);

                //add new translation item
                foreach (var audience in listAudience)
                {

                    var name = "LocalAudience." + audience;
                    //remove exist nodes
                    XmlNodeList xnList = xmlDoc.SelectNodes("/root/data[@name='" + name + "']");
                    if (xnList != null)
                    {
                        foreach (XmlNode node in xnList)
                        {
                            if (node.ParentNode != null)
                                node.ParentNode.RemoveChild(node);
                        }
                    }

                    XmlElement newElem = xmlDoc.CreateElement("data");
                    newElem.SetAttribute("name", name);
                    newElem.SetAttribute("xml:space", "preserve");
                    newElem.InnerXml = "<value></value>";
                    var xmlElement = newElem["value"];
                    if (xmlElement != null)
                        xmlElement.InnerText = audience;
                    if (xmlDoc.DocumentElement != null)
                        xmlDoc.DocumentElement.AppendChild(newElem);
                }
                xmlDoc.Save(filename);
            }


        }

        #region [ Functions to render layout content]
        /// <summary>
        /// Gets the list of local audience.
        /// </summary>
        /// <returns>System.String.</returns>
        public string GetListOfLocalAudienceLayout()
        {
            if (_listLocalAudience == null || !_listLocalAudience.Any())
                return string.Empty;

            var html = new StringBuilder();
            foreach (var localAudience in _listLocalAudience.OrderBy(a => a.Id))
            {
                var deleteLink = @"<a href='#' class='delete_audience'>Delete</a>";
                html.Append(string.Format(@"
<tr class='row'>
    <td class='name'>{0}</td>
    <td>{1}</td>
</tr>"
                    , localAudience.AudienceName, deleteLink));
            }
            return html.ToString();
        }

        /// <summary>
        /// Gets the list of local partner type layout.
        /// </summary>
        /// <returns>System.String.</returns>
        public string GetListOfLocalPartnerTypeLayout()
        {
            if (_listLocalPartnerType == null || !_listLocalPartnerType.Any())
                return string.Empty;
            var html = new StringBuilder();
            foreach (var localPartnerType in _listLocalPartnerType)
            {
                var deleteLink = @"<a href='#' class='delete_partnertype'>Delete</a>";
                html.Append(string.Format(@"
<tr class='row'>
    <td class='name'>{0}</td>
    <td class='id'>{1}</td>
    <td>{2}</td>
</tr>"
                    , localPartnerType.Name, localPartnerType.CatgegoryId, deleteLink));
            }
            return html.ToString();
        }

        #endregion
    }
}