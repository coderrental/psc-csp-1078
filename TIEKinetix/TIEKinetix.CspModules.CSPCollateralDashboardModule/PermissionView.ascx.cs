﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using DotNetNuke.Common.Utilities;
using Telerik.Web.UI;
using TIEKinetix.CspModules.CSPCollateralDashboardModule.Components;
using TIEKinetix.CspModules.CSPCollateralDashboardModule.Components.Data;

namespace TIEKinetix.CspModules.CSPCollateralDashboardModule
{
    public partial class PermissionView : DnnModuleBase
    {
        private CspDataContext _dataContext;
        private DnnContext _dnnContext;

        #region Overrides of UserControl

		protected override void Page_Init(object sender, EventArgs e)
        {
			base.Page_Init(sender, e);
			_dataContext = new CspDataContext(Utils.GetConnectionString(PortalId));
			_dnnContext = new DnnContext(Config.GetConnectionString());

            dsPermissions.ConnectionString = Config.GetConnectionString();
			dsPermissions.SelectCommand = @"select a.PermissionId,c.DisplayName, c.UserName, a.DateCreated, a.DateChanged 
	from CSPCollateralDashboardModule_Permission a
	join UserPortals b on b.PortalId=a.PortalId and b.UserId=a.UserId
	join Users c on c.UserID=a.UserId
    ";

			permissionGrid.MasterTableView.CommandItemSettings.AddNewRecordText = GetLocalizedText("Permission.AddNewUser");
        }

		protected void permissionGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridNestedViewItem)
            {
                RadGrid grid = (sender as RadGrid);
                GridNestedViewItem item = e.Item as GridNestedViewItem;
                if (item.ParentItem.GetDataKeyValue("PermissionId") == null) return;

                Guid id = (Guid)item.ParentItem.GetDataKeyValue("PermissionId");

                List<CSPCollateralDashboardModule_Language> languages = _dnnContext.CSPCollateralDashboardModule_Languages.Where(a => a.PermissionId == id).ToList();

                RadTabStrip tabs = (RadTabStrip)item.FindControl("tabs");
                if (tabs != null)
                {
                    tabs.Tabs[0].Text = GetLocalizedText("Permissions.LanguageTab");
                }

                RadGrid gridLanguage = (RadGrid)item.FindControl("gridLanguage");
				gridLanguage.MasterTableView.CommandItemSettings.AddNewRecordText = GetLocalizedText("Permission.AddNewLanguage");
                gridLanguage.DataSource = _dataContext.languages.Where(a => a.active.HasValue && a.active.Value).ToList().Where(a => languages.Count(b => b.LanguageId == a.languages_Id) > 0);
            }
        }


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void OnGridPermissionItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                e.Canceled = true;

                ((RadGrid)e.Item.OwnerTableView.NamingContainer).EditIndexes.Clear();
                e.Item.OwnerTableView.EditFormSettings.UserControlName = ControlPath + "AddNewPermission.ascx";

				var items = _dnnContext.ExecuteQuery<DnnUser>(@"
                        select *
from Users u
	join UserPortals up on u.UserID = up.UserId
where up.PortalId = {0} and (u.UserID not in (
	select urs.UserID
	from Users urs
		join UserRoles urrls on urs.UserID = urrls.UserID
		join Roles rls on urrls.RoleID = rls.RoleID
	where rls.RoleName = 'Administrators')) and (not exists (
	select 1
	from CSPCollateralDashboardModule_Permission ip
	where ip.PortalId = up.PortalId and ip.UserId = up.UserId)) and up.IsDeleted = 0", PortalId).ToList();
				e.Item.OwnerTableView.InsertItem(items);
            }
            else if (e.CommandName == RadGrid.PerformInsertCommandName)
            {
                GridEditableItem insertedItem = e.Item.OwnerTableView.GetInsertItem();
                UserControl userControl = insertedItem.FindControl(GridEditFormItem.EditFormUserControlID) as UserControl;
                if (userControl == null) return;

                string userId = ((RadComboBox)userControl.FindControl("cbUsers")).SelectedValue;
                int id = -1;
                if (int.TryParse(userId, out id))
                {
                    _dnnContext.CSPCollateralDashboardModule_Permissions.InsertOnSubmit(new CSPCollateralDashboardModule_Permission
                                                                                            {
                                                                                                PermissionId = Guid.NewGuid(),
                                                                                                Active = true,
                                                                                                DateCreated = DateTime.Now,
                                                                                                DateChanged = DateTime.Now,
                                                                                                PortalId = PortalId,
                                                                                                UserId = id
                                                                                            });
                    _dnnContext.SubmitChanges();

					//Refresh to clear POST request
					Response.Redirect(Request.RawUrl); 
                }
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
				Guid id = new Guid(((GridEditableItem)e.Item).GetDataKeyValue("PermissionId").ToString());

				_dnnContext.CSPCollateralDashboardModule_Languages.DeleteAllOnSubmit(_dnnContext.CSPCollateralDashboardModule_Languages.Where(a => a.PermissionId == id));
				_dnnContext.CSPCollateralDashboardModule_Permissions.DeleteOnSubmit(_dnnContext.CSPCollateralDashboardModule_Permissions.First(a => a.PermissionId == id));
				_dnnContext.SubmitChanges();

				if (e.Item.OwnerTableView.EditFormSettings.UserControlName == ControlPath + "AddNewPermission.ascx")
				{
					((RadGrid)e.Item.OwnerTableView.NamingContainer).EditIndexes.Clear();
					var items =
						_dnnContext.ExecuteQuery<DnnUser>(
							@"
                        select *
from Users u
	join UserPortals up on u.UserID = up.UserId
where up.PortalId = {0} and (u.UserID not in (
	select urs.UserID
	from Users urs
		join UserRoles urrls on urs.UserID = urrls.UserID
		join Roles rls on urrls.RoleID = rls.RoleID
	where rls.RoleName = 'Administrators')) and (not exists (
	select 1
	from CSPCollateralDashboardModule_Permission ip
	where ip.PortalId = up.PortalId and ip.UserId = up.UserId)) and up.IsDeleted = 0",
							PortalId).ToList();
					e.Item.OwnerTableView.InsertItem(items);
				}

				permissionGrid.DataBind();
            }
        }

        protected void OnLanguageGridItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                e.Canceled = true;

                ((RadGrid)e.Item.OwnerTableView.NamingContainer).EditIndexes.Clear();
                e.Item.OwnerTableView.EditFormSettings.UserControlName = ControlPath + "AddNewLanguage.ascx";
                string permissionId = ((HiddenField)e.Item.OwnerTableView.OwnerGrid.Parent.FindControl("hfpid")).Value;
                Guid id = Guid.Empty;

                if (Guid.TryParse(permissionId, out id))
                {
                    var lngs = (from l in _dnnContext.CSPCollateralDashboardModule_Languages.Where(a => a.PermissionId == id)
                                select l.LanguageId).ToArray();

                    var items = _dataContext.languages.Where(a => a.active.HasValue && a.active.Value).ToArray().Where(a => lngs.Count(b => b == a.languages_Id) == 0);
                    e.Item.OwnerTableView.InsertItem(items);
                }
            }
            else if (e.CommandName == RadGrid.PerformInsertCommandName)
            {
				GridEditableItem insertedItem = e.Item.OwnerTableView.GetInsertItem();
				UserControl userControl = insertedItem.FindControl(GridEditFormItem.EditFormUserControlID) as UserControl;
				if (userControl == null) return;

				Guid permissionId = new Guid(((HiddenField)e.Item.OwnerTableView.OwnerGrid.Parent.FindControl("hfpid")).Value);

				string languageId = ((RadComboBox)userControl.FindControl("cbLanguages")).SelectedValue;
				int lngId = -1;
				if (int.TryParse(languageId, out lngId))
				{
					_dnnContext.CSPCollateralDashboardModule_Languages.InsertOnSubmit(new CSPCollateralDashboardModule_Language
					{
						Id = Guid.NewGuid(),
						LanguageId = lngId,
						PermissionId = permissionId
					});
					_dnnContext.SubmitChanges();
					UpdateLanguageGrid(permissionId, e);
				}
            }
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                GridEditableItem item = (GridEditableItem)e.Item;
                int languageId = -1;
                string permissionId = ((HiddenField)e.Item.OwnerTableView.OwnerGrid.Parent.FindControl("hfpid")).Value;
                Guid id = Guid.Empty;

                if (Guid.TryParse(permissionId, out id) && int.TryParse(item.GetDataKeyValue("languages_Id").ToString(), out languageId))
                {
                    _dnnContext.CSPCollateralDashboardModule_Languages.DeleteAllOnSubmit(_dnnContext.CSPCollateralDashboardModule_Languages.Where(a => a.LanguageId == languageId && a.PermissionId == id));
                    _dnnContext.SubmitChanges();
					UpdateLanguageGrid(id, e);
                    //permissionGrid.DataBind();
                }
            }
        }

		private void UpdateLanguageGrid(Guid permissionId, GridCommandEventArgs e)
		{
			var lngs = (from l in _dnnContext.CSPCollateralDashboardModule_Languages.Where(a => a.PermissionId == permissionId)
						select l.LanguageId).ToArray();

			e.Item.OwnerTableView.DataSource = _dataContext.languages.Where(a => a.active.HasValue && a.active.Value).ToArray().Where(a => lngs.Count(b => b == a.languages_Id) != 0);
		}
    }
}