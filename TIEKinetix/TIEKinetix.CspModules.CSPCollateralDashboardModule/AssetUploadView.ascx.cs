﻿using System;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using Telerik.Web.UI;
using TIEKinetix.CspModules.CSPCollateralDashboardModule.Components;

namespace TIEKinetix.CspModules.CSPCollateralDashboardModule
{
    public partial class AssetUploadView : DnnModuleBase
    {
        Guid _contentId;

        #region Overrides of UserControl

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            _contentId = new Guid(Request.QueryString.Get("id"));
            // customize radeditor toolbar
            tbAssetDesc.Tools.Clear();
            EditorToolGroup group = new EditorToolGroup();
            tbAssetDesc.Tools.Add(group);
        	var etBold = new EditorTool {Name = "Bold", Text = GetLocalizedText("HtmlEditor.Bold")};
        	var etItalic = new EditorTool {Name = "Italic", Text = GetLocalizedText("HtmlEditor.Italic")};
			var etUnderline = new EditorTool { Name = "Underline", Text = GetLocalizedText("HtmlEditor.Underline") };
			var etStrikeThrough = new EditorTool { Name = "StrikeThrough", Text = GetLocalizedText("HtmlEditor.StrikeThrough") };
			var etJustifyLeft = new EditorTool { Name = "JustifyLeft", Text = GetLocalizedText("HtmlEditor.JustifyLeft") };
			var etJustifyCenter = new EditorTool { Name = "JustifyCenter", Text = GetLocalizedText("HtmlEditor.JustifyCenter") };
			var etJustifyRight = new EditorTool { Name = "JustifyRight", Text = GetLocalizedText("HtmlEditor.JustifyRight") };
			var etJustifyFull = new EditorTool { Name = "JustifyFull", Text = GetLocalizedText("HtmlEditor.JustifyFull") };
			var etJustifyNone = new EditorTool { Name = "JustifyNone", Text = GetLocalizedText("HtmlEditor.JustifyNone") };
            var etCut = new EditorTool { Name = "Cut", Text = GetLocalizedText("HtmlEditor.Cut") };
            var etCopy = new EditorTool { Name = "Copy", Text = GetLocalizedText("HtmlEditor.Copy") };
			var etPaste = new EditorTool { Name = "Paste", Text = GetLocalizedText("HtmlEditor.Paste") };
			var etIndent = new EditorTool { Name = "Indent", Text = GetLocalizedText("HtmlEditor.Indent") };
			var etOutdent = new EditorTool { Name = "Outdent", Text = GetLocalizedText("HtmlEditor.Outdent") };
			var etInsertOrderedList = new EditorTool { Name = "InsertOrderedList", Text = GetLocalizedText("HtmlEditor.InsertOrderedList") };
			var etInsertUnorderedList = new EditorTool { Name = "InsertUnorderedList", Text = GetLocalizedText("HtmlEditor.InsertUnorderedList") };
			group.Tools.Add(etBold);
			group.Tools.Add(etItalic);
			group.Tools.Add(etUnderline);
			group.Tools.Add(etStrikeThrough);
			group.Tools.Add(new EditorSeparator());
			group.Tools.Add(etJustifyLeft);
			group.Tools.Add(etJustifyCenter);
			group.Tools.Add(etJustifyRight);
			group.Tools.Add(etJustifyFull);
			group.Tools.Add(etJustifyNone);
			group.Tools.Add(new EditorSeparator());
            group.Tools.Add(etCut);
            group.Tools.Add(etCopy);
            group.Tools.Add(etPaste);
			group.Tools.Add(new EditorSeparator());
			group.Tools.Add(etIndent);
			group.Tools.Add(etOutdent);
			group.Tools.Add(etInsertOrderedList);
			group.Tools.Add(etInsertUnorderedList);

            // localize
            ((DotNetNuke.UI.UserControls.LabelControl) lbAssetName).Text = GetLocalizedText("AssetUpload.AssetName");
            ((DotNetNuke.UI.UserControls.LabelControl)lbAssetName).HelpText = GetLocalizedText("AssetUpload.AssetNameHelp");

            ((DotNetNuke.UI.UserControls.LabelControl)lbAssetDesc).Text = GetLocalizedText("AssetUpload.AssetDesc");
            ((DotNetNuke.UI.UserControls.LabelControl)lbAssetDesc).HelpText = GetLocalizedText("AssetUpload.AssetDescHelp");
            ((DotNetNuke.UI.UserControls.LabelControl)lbAssetFile).Text = GetLocalizedText("AssetUpload.AssetFile");
            ((DotNetNuke.UI.UserControls.LabelControl)lbAssetFile).HelpText = GetLocalizedText("AssetUpload.AssetFileHelp");
            ((DotNetNuke.UI.UserControls.LabelControl)lbPreviewImage).Text = GetLocalizedText("AssetUpload.PreviewImage");
            ((DotNetNuke.UI.UserControls.LabelControl)lbPreviewImage).HelpText = GetLocalizedText("AssetUpload.PreviewImageHelp");
            ((DotNetNuke.UI.UserControls.LabelControl)lbTileImage).Text = GetLocalizedText("AssetUpload.TileImage");
            ((DotNetNuke.UI.UserControls.LabelControl)lbTileImage).HelpText = GetLocalizedText("AssetUpload.TileImageHelp");

            // populate data
            using (var dataContext = new CspDataContext(Utils.GetConnectionString(PortalId)))
            {
                content cspContent = dataContext.contents.FirstOrDefault(a => a.content_Id == _contentId);
                if (cspContent != null)
                {
                    tbAssetName.Text = LoadContentFieldData(dataContext, cspContent, "Content_Title");
                    tbAssetDesc.Content = LoadContentFieldData(dataContext, cspContent, "Content_Description_Long");
                }
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
			if (ModuleRole.CountryRegionalManager(UserInfo, int.Parse(Request.Params["lid"])) && !HaveAdminPermission())
			{
				pnImgUpload.Visible = false;
			}
        }

        protected void ButtonOnClickEventHandler(object sender, EventArgs e)
        {
            Page.Validate("AssetUpload");
            if (Page.IsValid)
            {
                bool isValid = true;
                using (var dataContext = new CspDataContext(Utils.GetConnectionString(PortalId)))
                {
                    content cspContent = dataContext.contents.FirstOrDefault(a => a.content_Id == _contentId);
                    if (cspContent!=null)
                    {
                        // asset name (content title)
                        isValid = SaveContentField(dataContext, cspContent, "Content_Title", tbAssetName.Text);

                        // asset description (description long)
                        isValid = SaveContentField(dataContext, cspContent, "Content_Description_Long", tbAssetDesc.Content) && isValid;

						// preview image
                        if (!SaveContentUploadField(dataContext, cspContent, "Content_Image_Url", uploadPreviewImage))
                        {
                            validUploadPreviewImage.Text = " " + GetLocalizedText("ModuleError.FailValidation");
                            validUploadPreviewImage.IsValid = false;
                            validUploadPreviewImage.Visible = true;
                            isValid = false;
                        }

						// tile image
                        if (!SaveContentUploadField(dataContext, cspContent, "Content_Image_Thumbnail", uploadTileImage))
                        {
                            validTileImage.Text = " " + GetLocalizedText("ModuleError.FailValidation");
                            validTileImage.IsValid = false;
                            validTileImage.Visible = true;
                            isValid = false;
                        }

                    	// actual file
                        if (!SaveContentUploadField(dataContext, cspContent, "Content_File_Url", uploadAssetFile))
                        {
                            validUploadFile.Text = " " + GetLocalizedText("ModuleError.FailValidation");
                            validUploadFile.IsValid = false;
                            validUploadFile.Visible = true;
                            isValid = false;
                        }

                        
                        if (isValid)
                        {
                            dataContext.SubmitChanges();    
                        }
                        
                    }
                }
                if (isValid)
                {
                    // close rad window
                    ajaxManager.ResponseScripts.Add("CloseWindow();");    
                }
                
            }
            else
            {
                // notify users
                lbNotification.Text = GetLocalizedText("AssetUploadView.NeedMandatoryFields");
            }
        }

        private bool SaveContentUploadField(CspDataContext dataContext, content cspContent, string fieldName, FileUpload control)
        {
            if (string.IsNullOrEmpty(((FileUpload)control).FileName))
                return true;

            if (fieldName != "Content_File_Url")
            {
                if (!string.IsNullOrEmpty(control.FileName))
                {
                    string imageTypes = "image/bmp,image/jpeg,image/jpg,image/png,image/gif,image/pjpeg,image/x-png";
                    if (!imageTypes.Contains(control.PostedFile.ContentType))
                    {
                        return false;
                    }
                }
                    
            }

            // consideration: resize, custom url path, custom scheme (http/s)
            string customScheme = Utils.GetSetting(PortalId, "HttpTypeForImageUpload");
            string customHost = Utils.GetSetting(PortalId, "CustomHostForImageUpload");
            string serverPath = PortalSettings.HomeDirectoryMapPath + "Images\\" + UserInfo.UserID;
            string filename = Path.GetExtension(control.FileName), filePath = "", fileExt = Path.GetExtension(control.FileName);

            if (string.IsNullOrEmpty(customScheme))
                customScheme = Request.Url.Scheme;
            if (string.IsNullOrEmpty(customHost))
                customHost = Request.Url.Host;

            if (!Directory.Exists(serverPath))
                Directory.CreateDirectory(serverPath);

            filename = string.Format("{0}_{1}{2}", UserInfo.UserID, DateTime.Now.Ticks, fileExt);
            filePath = string.Format("{0}\\{1}", serverPath, filename);
            Random r = new Random(100);
            while (File.Exists(filePath))
            {
                filename = string.Format("{0}_{1}{2}{3}", UserInfo.UserID, DateTime.Now.Ticks,r.Next(99),fileExt);
                filePath = string.Format("{0}\\{1}", serverPath, filename);
            }
            control.SaveAs(filePath);

            if (!string.IsNullOrEmpty(control.Attributes["Resize"]) && control.Attributes["Resize"] == "true")
            {
                int resizeWidth = 200, resizeHeight = 60;
                int.TryParse(control.Attributes["Resize-Width"], out resizeWidth);
                int.TryParse(control.Attributes["Resize-Height"], out resizeHeight);
                filename = Utils.ResizeImage(serverPath, filename, resizeWidth, resizeHeight);
            }
            return SaveContentField(dataContext, cspContent, fieldName, customScheme + "://" + customHost + PortalSettings.HomeDirectory + "Images/" + UserInfo.UserID + "/" + filename);
        }

        private static bool SaveContentField(CspDataContext dataContext, content cspContent, string fieldName, string fieldValue)
        {
            content_field field = cspContent.content_fields.FirstOrDefault(a => a.content_types_field.fieldname == fieldName);
            if (field == null) return false;
            field.value_text = fieldValue;
            return true;
        }

        private string LoadContentFieldData(CspDataContext dataContext, content cspContent, string fieldName)
        {
            //int contentTypeId = cspContent.content_main.content_types_Id;
            //content_types_field fieldDef = dataContext.content_types_fields.FirstOrDefault(a => a.content_types_Id == contentTypeId && a.fieldname == fieldName);
            //if (fieldDef==null)
            //    return string.Empty;
            //content_field field = cspContent.content_fields.FirstOrDefault(a => a.content_types_fields_Id == fieldDef.content_types_fields_Id);
            //return (field != null ? field.value_text : string.Empty);
            return Utils.GetContentFieldValue(dataContext, cspContent, fieldName);
        }
		/// <summary>
		/// Gets the detail page URL.
		/// </summary>
		/// <returns></returns>
		public string GetDetailPageUrl()
		{
			return DotNetNuke.Common.Globals.NavigateURL(TabId, "Details", 
															"mid", ModuleId.ToString(), 
															"id", Request.Params["id"],
															"cid", Request.Params["cid"], 
															"lid", Request.Params["lid"]);
		}
    }
}