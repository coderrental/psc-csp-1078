﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddNewLanguage.ascx.cs" Inherits="TIEKinetix.CspModules.CSPCollateralDashboardModule.AddNewLanguage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div>    
    <span>Select a language: </span><telerik:RadComboBox runat="server" ID="cbLanguages" Filter="Contains"></telerik:RadComboBox>    
    &nbsp;
    <asp:ImageButton ID="btnInsert" Text="Insert" runat="server" CommandName="PerformInsert" ImageUrl="Images/add.png"></asp:ImageButton>
    &nbsp;
    <asp:ImageButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False" CommandName="Cancel" ImageUrl="Images/delete.png"></asp:ImageButton>    
</div>
