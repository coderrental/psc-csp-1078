﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddNewPermission.ascx.cs" Inherits="TIEKinetix.CspModules.CSPCollateralDashboardModule.AddNewPermission" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div>    
    <span>Select a user</span><telerik:RadComboBox runat="server" ID="cbUsers" Filter="Contains"></telerik:RadComboBox>    
    &nbsp;
    <asp:ImageButton ID="btnInsert" Text="Insert" runat="server" CommandName="PerformInsert" ImageUrl="Images/add.png"></asp:ImageButton>
    &nbsp;
    <asp:ImageButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False" CommandName="Cancel" ImageUrl="Images/delete.png"></asp:ImageButton>
</div>