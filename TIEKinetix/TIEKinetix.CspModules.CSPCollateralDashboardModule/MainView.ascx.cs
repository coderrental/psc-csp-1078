﻿using System;
using System.Collections.Generic;
using System.Linq;
using CR.ContentObjectLibrary;
using CR.ContentObjectLibrary.Data;
using CR.ContentObjectLibrary.Factory;
using CR.DnnModules.Common;
using CR.DnnModules.Common.CategoryTree;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Data;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Security;
using TIEKinetix.CspModules.CSPCollateralDashboardModule.Entities;
using Telerik.Web.UI;
using TIEKinetix.CspModules.CSPCollateralDashboardModule.Components;
using TIEKinetix.CspModules.CSPCollateralDashboardModule.Components.Data.Lib.Content;
using IContentAccessFilter = CR.ContentObjectLibrary.Interface.IContentAccessFilter;

namespace TIEKinetix.CspModules.CSPCollateralDashboardModule
{
    public partial class MainView : DnnModuleBase, IActionable
    {
        protected CspContents CspData;
        private int _categoryId, _languageId;
        private string _stageId;
    	private bool _isFilterCategory;
        private bool _isSyncLanguage, _isDisableNavPanel;
    	private int  _cspId, _audienceId;
        private List<content> _contents;
        private List<category> _listChildCategories; 
        #region Overrides of UserControl

        /// <summary>
        /// Page Init
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            Page.ClientScript.RegisterClientScriptInclude("carousel", ControlPath + "Js/carousel.js");
        	_cspId = Utils.GetIntegrationKey(UserInfo);
            if (_cspId == -1 && !HaveAdminPermission())
            {
				GotoRedirectTabId(String.Empty);
            }

			if (!HaveAdminPermission())
			{
                if (CspContext.companies.SingleOrDefault(a => a.companies_Id == _cspId) == null) {
                    GotoRedirectTabId(String.Empty);
                }
				DnnDataContext dnnContext = new DnnDataContext(Config.GetConnectionString());
				Guid missingFormId = Utils.MissingMandatorySubscription(dnnContext, CspContext, UserInfo, PortalId);
				if (missingFormId != Guid.Empty)
				{
					int missingStep = Utils.FormStepNo(dnnContext, CspContext, missingFormId, PortalId);
					GotoRedirectTabId(String.Format("step={0}", missingStep));
				}
			}

			if (Settings[ModuleSetting.FilterCategoryTree] != null && Settings[ModuleSetting.FilterCategoryTree].ToString() == "True") 
			{
			    _isFilterCategory = true;
			}

            if (Settings[ModuleSetting.SyncLanguage] != null)
            {
                _isSyncLanguage = Settings[ModuleSetting.SyncLanguage].ToString() == "1";
            }
            if (Settings[ModuleSetting.DisableNavigatePanel] != null)
            {
                _isDisableNavPanel = Settings[ModuleSetting.DisableNavigatePanel].ToString() == "1";
                if (_isDisableNavPanel)
                {
                    NavPanel.CssClass = "csp-nav-panel hide";
                }
            }

            _categoryId = _languageId = -1;

            //requested audience id
            if (!int.TryParse(Request.QueryString.Get("aid"), out _audienceId))
                _audienceId = -1;

            // requested language id
            if (!int.TryParse(Request.QueryString.Get("lid"), out _languageId))
                _languageId = -1;

            // requested category id
            if (!int.TryParse(Request.QueryString.Get("cid"), out _categoryId))
                _categoryId = -1;

            // requested stage id, this variable can be empty
            _stageId = !string.IsNullOrEmpty(Request.QueryString.Get("sid")) ? Request.QueryString.Get("sid") : string.Empty;

            // localize
            lbLanguage.Text = GetLocalizedText("Label.Language");
            lbCategories.Text = GetLocalizedText("Label.Categories");
            lbStages.Text = GetLocalizedText("Label.Stages");
            lbAudience.Text = GetLocalizedText("Label.Audience");
            btnSearch.Text = GetLocalizedText("Label.Search");

            // language drop down list
            if (_isSyncLanguage)
            {
                PanelLanguage.Visible = false;
            }
            else
            {
                bool renderLanguages = false;
                if (_cspId == -1)
                {
                    var listLanguages = CspContext.languages.Where(a => a.active.HasValue && a.active.Value).OrderBy(a => a.description).ToList();
                    foreach (language lng in listLanguages)
                    {
                        cbLanguage.Items.Add(new RadComboBoxItem
                        {
                            Text = lng.description,
                            Value = lng.languages_Id.ToString()
                        });
                    }
                    renderLanguages = true;

                }
                else
                {
                    var company = CspContext.companies.SingleOrDefault(a => a.companies_Id == _cspId);
                    if (company != null)
                    {
                        var country = DnnDataContext.CSPSubscriptionModule_Countries.FirstOrDefault(a => a.Name == company.country);
                        if (country != null)
                        {
                            var clusterCountry = DnnDataContext.CSPSubscriptionModule_Cluster_Countries.Where(a => a.CountryId == country.Id).ToList();
                            if (clusterCountry.Any())
                            {
                                List<int> languagesIds = GetListLanguageByListClusterCountry(clusterCountry);
                                if (languagesIds.Count > 0)
                                {
                                    var listLanguages =
                                        languagesIds.SelectMany(
                                            languagesId =>
                                            CspContext.languages.Where(a => a.active.HasValue && a.active.Value).Where(
                                                lng => languagesId == lng.languages_Id)).OrderBy(a => a.description).ToList();
                                    foreach (language lng in listLanguages)
                                    {
                                        cbLanguage.Items.Add(new RadComboBoxItem
                                        {
                                            Text = lng.description,
                                            Value = lng.languages_Id.ToString()
                                        });
                                    }
                                    renderLanguages = true;
                                }
                            }
                        }
                    }
                }

                if (!renderLanguages)
                {
                    var listLanguages = CspContext.languages.Where(a => a.active.HasValue && a.active.Value).OrderBy(a => a.description).ToList();
                    foreach (language lng in listLanguages)
                    {
                        cbLanguage.Items.Add(new RadComboBoxItem
                        {
                            Text = lng.description,
                            Value = lng.languages_Id.ToString()
                        });
                    }
                }
                
                if (_languageId != -1)
                {
                    cbLanguage.Items.FindItemByValue(_languageId.ToString()).Selected = true;
                }
                else
                {
                    // if there is no specified language, then use consumer profile default language
                    if (_cspId != -1)
                    {
                        companies_consumer consumerProfile = CspContext.companies_consumers.FirstOrDefault(a => a.companies_Id == Utils.GetIntegrationKey(UserInfo));
                        if (consumerProfile != null && consumerProfile.default_language_Id.HasValue)
                        {
                            var item = cbLanguage.Items.FindItemByValue(consumerProfile.default_language_Id.Value.ToString());
                            if (item != null)
                                item.Selected = true;
                        }
                    }
                    else // default to browser language when there is no default language to use
                    {
                        var currentLang = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                        var lang = CspContext.languages.FirstOrDefault(a => a.BrowserLanguageCode == currentLang && a.active.HasValue && a.active.Value);
                        var item = cbLanguage.FindItemByValue(lang.languages_Id.ToString());
                        if (item != null) item.Selected = true;
                    }
                }
            }
            

            // stage drop down list
            cbStages.Items.Add(new RadComboBoxItem(GetLocalizedText("Stage.All"), ""));
            cbStages.Items.Add(new RadComboBoxItem(GetLocalizedText("Stage.Publish"), Keys.CspPublishStageId.ToString()));
            cbStages.Items.Add(new RadComboBoxItem(GetLocalizedText("Stage.UnPublish"), Keys.CspUnPublishStageId.ToString()));

            if (!string.IsNullOrEmpty(_stageId))
            {
                var item = cbStages.FindItemByValue(_stageId);
                if (item != null)
                    item.Selected = true;
            }

            //local audience drop down list
            if (_isFilterCategory)
            {
                AudiencePanel.Visible = true;
                foreach (var audience in ListLocalAudiences)
                {
                    cbAudience.Items.Add(new RadComboBoxItem(GetLocalizedText("LocalAudience.") + audience.AudienceName, audience.Id.ToString()));
                }
                
                if (_audienceId != -1)
                {
                    cbAudience.Items.FindItemByValue(_audienceId.ToString()).Selected = true;
                }

                if (_cspId != -1)
                {
                    companies_parameter companyParameter = CspContext.companies_parameters.FirstOrDefault(a => a.companies_Id == _cspId && a.companies_parameter_type.parametername == Cons.COMPANY_PARAMETER_AUDIENCE_NAME);
                    if (companyParameter != null && !string.IsNullOrEmpty(companyParameter.value_text))
                    {
                        var audience = ListLocalAudiences.SingleOrDefault(a => a.AudienceName == companyParameter.value_text);
                        if (audience != null)
                        {
                            var item = cbAudience.FindItemByValue(audience.Id.ToString());
                            if (item != null)
                                item.Selected = true;
                        }
                        
                    }
                }
            }
        }

        /**
         * Get & distinct List LanguageIds to display in dropdownlist
         * Author: Ngo Quang Phat
         */
        public List<int> GetListLanguageByListClusterCountry(List<CSPSubscriptionModule_Cluster_Country> clusterCountries)
        {
            List<int> list = new List<int>();
            foreach (CSPSubscriptionModule_Cluster_Country cspSubscriptionModuleCluster in clusterCountries)
            {
                List<CSPSubscriptionModule_Cluster_Language> moduleClusterLanguages = DnnDataContext.CSPSubscriptionModule_Cluster_Languages.Where(
                    a => a.ClusterId == cspSubscriptionModuleCluster.ClusterId).ToList();
                list.AddRange(moduleClusterLanguages.Select(cspSubscriptionModuleClusterLanguage => cspSubscriptionModuleClusterLanguage.LanguageId));
            }
            return list.Distinct().ToList();
        } 

        #endregion

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (_isSyncLanguage)
            {
                var currentLang = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                var lang = CspContext.languages.FirstOrDefault(a => a.BrowserLanguageCode == currentLang && a.active.HasValue && a.active.Value);
                _languageId = lang != null ? lang.languages_Id : 1;
            }
            else
            {
                if (!int.TryParse(cbLanguage.SelectedValue,out _languageId))
                {
                    throw new Exception("Invalid Language value");
                }
               
            }
            var tree = (RadTreeView) cbCategories.Items[0].FindControl("categoryTree");

            int selectedCategory;
            if (!int.TryParse(tree.SelectedValue, out selectedCategory))
            {
                if (_categoryId > 0)
                    selectedCategory = _categoryId;
                else
                {
                    selectedCategory = -1;
                }
            }

            if(AudiencePanel.Visible)
            {
                if (!int.TryParse(cbAudience.SelectedValue,out _audienceId))
                {
                    throw new Exception("Invalid Audience value");
                }
            }

			var searchContent = (SimpleSearch)SearchExpressionFactory.CreateSearch();

			searchContent.Left = SearchExpressionFactory.CreateSearch(CspContext.content_types.FirstOrDefault(a => a.content_types_Id == AssetContentTypeId));
			searchContent.Operator = SearchOperator.And;
			searchContent.Right = SearchExpressionFactory.CreateSearch();
			searchContent.Right.Left = SearchExpressionFactory.CreateSearch(CspContext.companies.FirstOrDefault(a => a.companies_Id == SupplierId));
			searchContent.Right.Operator = SearchOperator.And;
            searchContent.Right.Right = SearchExpressionFactory.CreateSearch(CspContext.languages.FirstOrDefault(a => a.languages_Id == _languageId));    
        	IContentAccessFilter caf = null;
            var companyParameter = CspContext.companies_parameters.FirstOrDefault(a => a.companies_Id == _cspId && a.companies_parameter_type.parametername == Cons.LOCAL_PARTNER_TYPE_TEXT);

            if (selectedCategory != -1)
            {
                caf = ContentAccessFilterFactory.CreateContentAccessFilter(searchContent, CspContext, selectedCategory);
            }
            else if (_isFilterCategory && !HaveAdminPermission())
            {

                if (companyParameter != null && !string.IsNullOrEmpty(companyParameter.value_text))
                {
                    var currentPartnerType = ListLocalPartnerType.SingleOrDefault(a => a.Name == companyParameter.value_text);
                    if (currentPartnerType != null)
                    {
                        caf = ContentAccessFilterFactory.CreateContentAccessFilter(searchContent, CspContext, currentPartnerType.CatgegoryId, false);
                    }
                }
                else
                    caf = ContentAccessFilterFactory.CreateContentAccessFilter(searchContent, CspContext);
            }
			else
			{
				caf = ContentAccessFilterFactory.CreateContentAccessFilter(searchContent, CspContext);	
			}
			
			_contents = new List<content>();
			
            // if users are in country manager role, then see all, if not then users can see published content only
            if (ModuleRole.CountryRegionalManager(UserInfo, _languageId) || HaveAdminPermission())
            {
                // show stage filter drop down list
                StagePanel.Visible = StagePanel.Enabled = true;
				
				if (cbStages.SelectedValue == string.Empty)
                    _contents = caf.Search(); 
				else if (cbStages.SelectedValue == Keys.CspPublishStageId.ToString())
                    _contents = caf.Search().Where(a => a.stage_Id == Keys.CspPublishStageId).ToList();
				else
                    _contents = caf.Search().Where(a => a.stage_Id != Keys.CspPublishStageId).ToList();
            }
            else
            {
                _contents = caf.Search().Where(a => a.stage_Id == Keys.CspPublishStageId).ToList(); 

                // hide stage filter drop down list
                StagePanel.Visible = StagePanel.Enabled = false;
            }

            
            if (_isFilterCategory) //Filter audience
            {
                var curentAudience = ListLocalAudiences.Single(a => a.Id == _audienceId);
                _contents = _contents.Where(a => Utils.GetContentFieldValue(CspContext, a, Cons.LOCAL_AUDIENCE_FIELD_NAME) == curentAudience.AudienceName).ToList();
            }
           

            //Only display content once time
            var temps = _contents;
            for (int i = 0; i < temps.Count; i++)
            {
                var content = temps[i];
                if (_contents.Count(a => a.content_Id == content.content_Id) > 1)
                {
                    _contents.Remove(_contents.First(a => a.content_Id == content.content_Id));
                    i--;
                }
                    
            }

            //If have stageid do not display, then hide the content with this stage id
            if (StageIdToNotDisplay != 0)
            {
                _contents = _contents.Where(a => a.stage_Id != StageIdToNotDisplay).ToList();
            }

            if (_isSyncLanguage) //Check if no content then redirect page
            {
                if (!_contents.Any() && _categoryId >= 0)
                {
                    Response.Redirect(
                        _isFilterCategory
                            ? Globals.NavigateURL(TabId, "",string.Format("mid={0}&lid={1}&cid={2}&aid={3}", ModuleId, _languageId,-1, cbAudience.SelectedValue))
                            : Globals.NavigateURL(TabId, "",string.Format("mid={0}&lid={1}&cid={2}", ModuleId, _languageId, -1)),
                        false);
                }
                    
            }

            //Vu.dinh 04-25-2014: UR-416 update search feature. Filter campaigns by followings fields:
            //Content_Title
            //Content_Description_Long
            //Content_Description_Short
            //Local_Keywords
            DoSearchHandler();

            //Get campaigns block
            var campaignBlock = GetCampaignBlocklayout();
            listView.DataSource = campaignBlock;

        	var listCategories = CspContext.categories.Where(a => a.active == true).ToList();
           
            var customCategories = new List<CustomCategory>();

            cbCategories.EmptyMessage = GetLocalizedText("CategoryNav.ShowAll");

            _listChildCategories = new List<category>();
            if (companyParameter != null && !string.IsNullOrEmpty(companyParameter.value_text) && _isFilterCategory && !HaveAdminPermission())
            {
                var currentLocalPartnerType = ListLocalPartnerType.SingleOrDefault(a => a.Name == companyParameter.value_text);
                if (currentLocalPartnerType != null)
                {
                    var catx = listCategories.SingleOrDefault(a => a.categoryId == currentLocalPartnerType.CatgegoryId);
                    if (catx != null)
                    {
                        GetListChildCategories(catx.categoryId, true);
                    }
                }
            
            }
			foreach (category c in listCategories)
			{
			    if (companyParameter != null && !string.IsNullOrEmpty(companyParameter.value_text) && _isFilterCategory && !HaveAdminPermission())
			    {
			        if (c.parentId == 0 & c.parentId != 1) // category not in branch
			            continue;
                    if (_listChildCategories.All(a => a.categoryId != c.categoryId) && _listChildCategories.First().depth < c.depth)
                    {
                        continue;
                    }
			    }
			    var customCategory = new CustomCategory(c.categoryText, c.categoryId, c.parentId.HasValue ? c.parentId.Value : 0, c.depth.HasValue ? c.depth.Value : 0, c.lineage);
				var contentsFromCat = GetContentsFromCategoryId(_languageId, AssetContentTypeId, SupplierId,c.categoryId);
                if (_isFilterCategory) //Filter audience
                {
                     contentsFromCat = contentsFromCat.Where(a => Utils.GetContentFieldValue(CspContext, a, Cons.LOCAL_AUDIENCE_FIELD_NAME) == ListLocalAudiences.Single(b => b.Id == _audienceId).AudienceName).ToList();
                }

                //If have stage id to not display contens, then remove countent count so the category not this play too
                if (StageIdToNotDisplay != 0)
                {
                    contentsFromCat = contentsFromCat.Where(a => a.stage_Id != StageIdToNotDisplay).ToList();
                }

				if (ModuleRole.CountryRegionalManager(UserInfo, _languageId))
				{
					if (cbStages.SelectedValue != "")
					{
                        if (cbStages.SelectedValue == Keys.CspPublishStageId.ToString())
                            customCategory.Count = contentsFromCat.Count(a => a.stage_Id == Keys.CspPublishStageId);
                        else
                        {
                            customCategory.Count = contentsFromCat.Count(a => a.stage_Id != Keys.CspPublishStageId);
                        }
					}
					else
					{
                        customCategory.Count = contentsFromCat.Count;
					}

				}
				else
				{
                    customCategory.Count = contentsFromCat.Count(a => a.stage_Id == Keys.CspPublishStageId);
				}



				// update parent category asset counts
				if (customCategory.Count > 0)
				{
					CustomCategory temp = customCategories.FirstOrDefault(a => a.Id == customCategory.ParentId);
					while (temp != null && temp.Id != 0)
					{
						temp.Count += customCategory.Count;
						temp = customCategories.FirstOrDefault(a => a.Id == temp.ParentId);
					}
				}

				customCategories.Add(customCategory);
			}
				
        	

            tree.Nodes.Clear();
            CategoryTreeView.RenderCategoryTree(tree, customCategories);
			//Filter tree view
			if (_isFilterCategory && !HaveAdminPermission())
			{
                //filter display node with local partner type
				if (companyParameter != null && !string.IsNullOrEmpty(companyParameter.value_text))
				{
                    foreach (var localPartnerType in ListLocalPartnerType)
                    {
                        if (companyParameter.value_text == localPartnerType.Name)
                        {
                            RadTreeNode node = tree.FindNodeByValue(localPartnerType.CatgegoryId.ToString());
                            if (node != null)
                            {
                                //remove all node at same level
                                var listNodeToRemove = tree.GetAllNodes().Where(item => item.Value != node.Value && item.Level == node.Level).ToList();
                                foreach (var item in listNodeToRemove)
                                {
                                    item.Remove();
                                }
                            }
                        }
                    }
				}
			}

            //Mapping category - category content
            var mapCatName = new Dictionary<int, string>();
            foreach (var node in tree.GetAllNodes())
            {
                string nodeTranslateText = string.Empty;
                var catId = 0;
                if (!int.TryParse(node.Value, out catId))
                {
                    throw  new Exception("Invalid node value");
                }
                if (catId > 0)
                {
                    var categoryContent = GetContentsFromCategoryId(_languageId, CategoryContentTypeId, SupplierId,catId).SingleOrDefault(a => a.stage_Id == Keys.CspPublishStageId);
                    if (categoryContent != null)
                    {
                        nodeTranslateText = Utils.GetContentFieldValue(CspContext, categoryContent, "Content_Title");
                        if (!mapCatName.ContainsKey(catId))
                        {
                            mapCatName[catId] = nodeTranslateText;
                        }
                    }
                    else
                    {
                        nodeTranslateText = node.Text;
                    }
                }
                if (nodeTranslateText != string.Empty)
                    node.Text = nodeTranslateText;
            }

            RadTreeNode selectedNode = tree.FindNodeByValue(selectedCategory.ToString());
            if (selectedNode != null)
            {
                selectedNode.Selected = true;
                cbCategories.SelectedValue = selectedNode.Value;
            }
            if (cbCategories.SelectedValue != "-1" && selectedNode!= null)
            {
                if (mapCatName.ContainsKey(_categoryId))
                {
                    if (cbCategories.SelectedItem != null)
                    {
                        cbCategories.SelectedItem.Text = mapCatName[_categoryId];
                    }
                    else
                    {
                        cbCategories.Text = mapCatName[_categoryId];
                    }
                }
                else
                {
                    cbCategories.Text = selectedNode.Text;
                }

            }
        }


        /// <summary>
        /// Does the search handler.
        /// Vu.Dinh UR-416. Need filter campaigns by the followings field:
        ///Content_Title
        ///Content_Description_Long
        ///Content_Description_Short
        ///Local_Keywords
        /// </summary>
        /// <author>Vu Dinh</author>
        /// <modified>04/25/2014</modified>
        private void DoSearchHandler()
        {
            if (!string.IsNullOrEmpty(tbxSearch.Text))
            {
                var searchTerm = tbxSearch.Text;
                var listSearchTerm = new[] {"Content_Title","Content_Description_Long","Content_Description_Short","Local_Keywords"};

                for (int i = 0; i < _contents.Count; i ++)
                {
                    var match = false;
                    var ct = _contents[i];
                    foreach (var searchTermField in listSearchTerm)
                    {
                        var fieldValue = Utils.GetContentFieldValue(CspContext, ct, searchTermField);
                        if (!string.IsNullOrEmpty(fieldValue))
                        {
                            if (fieldValue.ToLower().Contains(searchTerm.ToLower()))
                            {
                                match = true;
                                break;
                            }
                        }
                    }
                    if (!match) //if not match search term, remove from list
                    {
                        _contents.RemoveAt(i);
                        i--;
                    }
                    
                }
                    
                    
            }
        }
        /// <summary>
        /// Gets the list child categories.
        /// </summary>
        /// <param name="parentCategoryId">The parent category id.</param>
        /// <param name="first">if set to <c>true</c> [first].</param>
        private void GetListChildCategories(int parentCategoryId, bool first)
        {
            if (first)
                _listChildCategories.Add(CspContext.categories.Single(a => a.categoryId == parentCategoryId));
            var list = CspContext.categories.Where(a => a.parentId == parentCategoryId && a.active == true);
            _listChildCategories.AddRange(list);
            foreach (var category in list)
            {
                GetListChildCategories(category.categoryId, false);
            }
        }

        /// <summary>
        /// Gets the campaign blocklayout.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 11/29/2012 - 4:18 PM
        private List<CampaignBlock> GetCampaignBlocklayout()
        {
            if (_contents.Any())
            {
                var campaignBlockLayout = new List<CampaignBlock>();
                for (int i = 0; i < _contents.Count; i++)
                {
                    var campaignBlock = campaignBlockLayout.FirstOrDefault(a => a.Item < 8);
                    if (campaignBlock != null)
                    {
                        campaignBlock.Layout += GetCampaignItemLayout(_contents[i]);
                        campaignBlock.Item++;
                    }
                    else
                    {
                        campaignBlockLayout.Add(new CampaignBlock
                                                    {
                                                        Layout = GetCampaignItemLayout(_contents[i]),
                                                        Item = 1
                                                    });
                    }
                }
                return campaignBlockLayout;
            }
            return new List<Entities.CampaignBlock>();
        }

        /// <summary>
        /// Gets the campaign item layout.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 11/29/2012 - 4:18 PM
        private string GetCampaignItemLayout(content content)
        {
            var contentTitle = Utils.GetContentFieldValue(CspContext, content, "Content_Title");
            var assetStatusImage = GetAssetStatusImage(content);
            var newAssetImage = GetNewAssetImage(content);
            string layout;
            if (string.IsNullOrEmpty(assetStatusImage) && string.IsNullOrEmpty(newAssetImage))
            {
                layout = @"<div class='csp-asset'>
					    <a href='{0}' cspobj='REPORT' csptype='asset' cspenglishvalue='{1}'>
						    <img width='175' height='150' src='{2}' alt='{3}' title='{4}' />       
					    </a>
					    <div class='csp-asset-title'>
						    <a href='{5}' target='_blank' title='{6}'>
                            {7}
						    </a>
					    </div>
				    </div>";
                layout = string.Format(layout,
                                   GetAssetUrl(content.content_Id),
                                   Utils.GetContentFieldValue(CspContext, content, "CSP_Reporting_Id"),
                                   Utils.GetContentFieldValue(CspContext, content, "Content_Image_Thumbnail"),
                                   contentTitle,
                                   contentTitle,
                                   GetAssetDownloadLink(content, "Content_File_Url"),
                                   GetLocalizedText("DownloadHoverText.Text"),
                                   contentTitle);
            }
            else
            {
                layout = @"<div class='csp-asset'>
					    <a href='{0}' cspobj='REPORT' csptype='asset' cspenglishvalue='{1}'>
						    <img width='175' height='150' src='{2}' alt='{3}' title='{4}' />
						    <span class='csp-asset-statusbar'></span>
						    <span class='csp-asset-indicators'>        
                            {5}
                            {6}                                         
						    </span>              
					    </a>
					    <div class='csp-asset-title'>
						    <a href='{7}' target='_blank' title='{8}'>
                            {9}
						    </a>
					    </div>
				    </div>";
                layout = string.Format(layout,
                                   GetAssetUrl(content.content_Id),
                                   Utils.GetContentFieldValue(CspContext, content, "CSP_Reporting_Id"),
                                   Utils.GetContentFieldValue(CspContext, content, "Content_Image_Thumbnail"),
                                   contentTitle,
                                   contentTitle,
                                   assetStatusImage,
                                   newAssetImage,
                                   GetAssetDownloadLink(content, "Content_File_Url"),
                                   GetLocalizedText("DownloadHoverText.Text"),
                                   contentTitle);
            }
            
            return layout;
        }

		/// <summary>
		/// Gotoes the redirect tab id.
		/// </summary>
		/// <param name="parameters">The parameters.</param>
		/// Author: ducuytran
		/// 1/9/2013 - 7:01 PM
		private void GotoRedirectTabId(string parameters)
		{
			var dataProvider = DataProvider.Instance();
            var result = dataProvider.ExecuteSQL(string.Format("SELECT TabID FROM Tabs WHERE TabName = '{0}' and PortalID = '{1}'", Settings[ModuleSetting.CompanySettingTabName], PortalId));
			int tabId = 0;
			while (result.Read())
			{
				tabId = int.Parse(result["TabId"].ToString());
				break;
			}
			if (tabId != 0)
			{
				string theUrl = Globals.NavigateURL(tabId);
				if (!String.IsNullOrEmpty(parameters))
					theUrl = Globals.NavigateURL(tabId, "", parameters);
				Response.Redirect(theUrl);
			}
		}

        public string GetCampaignItemLayout (CampaignBlock campaignBlock)
        {
            return campaignBlock.Layout;
        }

        #region Control Event Handlers
        /// <summary>
        /// Comboes the box selected index changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RadComboBoxSelectedIndexChangedEventArgs"/> instance containing the event data.</param>
        /// <author>Vu Dinh</author>
        /// <modified>04/25/2014 16:27:43</modified>
        protected void ComboBoxSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            // place holder
            RadDataPager pager = listView.FindControl("AssetsDataPager") as RadDataPager;
            if (pager != null)
            {
                pager.FireCommand(RadDataPager.PageCommandName, "0");
            }
            
        }


        /// <summary>
        /// Handles the OnSelectedIndexChanged event of the cbLanguage control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 11/28/2012 - 10:21 AM
        protected void cbLanguage_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (!_contents.Any() && _categoryId >= 0) //If there are no content, reset category tree
            {
                Response.Redirect(
                    _isFilterCategory
                        ? Globals.NavigateURL(TabId, "",string.Format("mid={0}&lid={1}&cid={2}&aid={3}", ModuleId, _languageId, -1,cbAudience.SelectedValue))
                        : Globals.NavigateURL(TabId, "",string.Format("mid={0}&lid={1}&cid={2}", ModuleId, _languageId, -1)),
                    false);
            }
            else //Reset pager
            {
                // place holder
                RadDataPager pager = listView.FindControl("AssetsDataPager") as RadDataPager;
                if (pager != null)
                {
                    pager.FireCommand(RadDataPager.PageCommandName, "0");
                }
            }
        }

        protected void ListViewPreRender(object sender, EventArgs e)
        {
            // place holder
        }

        protected void CategoryTreeOnNodeClick(object sender, RadTreeNodeEventArgs e)
        {
            // place holder to trigger post back even
            cbCategories.SelectedValue = e.Node.Value;
            cbCategories.SelectedItem.Text = e.Node.Text;
        }

        /// <summary>
        /// return a dnn url for a given content asset guid
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected string GetAssetUrl(Guid id)
        {
            string url = string.Empty; 
            if (StagePanel.Enabled && !string.IsNullOrEmpty(cbStages.SelectedValue))
            {
                if (AudiencePanel.Visible && !string.IsNullOrEmpty(cbAudience.SelectedValue))
                {
                    url = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Details",
                                          "mid", ModuleId.ToString(),
                                          "id", id.ToString(),
                                          "lid", _languageId.ToString(),
                                          "cid", !string.IsNullOrEmpty(cbCategories.SelectedValue) ? cbCategories.SelectedValue : "0",
                                          "sid", cbStages.SelectedValue,
                                          "aid",cbAudience.SelectedValue);
                }
                else
                {
                    url = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Details",
                                          "mid", ModuleId.ToString(),
                                          "id", id.ToString(),
                                          "lid", _languageId.ToString(),
                                          "cid", !string.IsNullOrEmpty(cbCategories.SelectedValue) ? cbCategories.SelectedValue : "0",
                                          "sid", cbStages.SelectedValue);
                }
                
            }
            else
            {
                if (AudiencePanel.Visible && !string.IsNullOrEmpty(cbAudience.SelectedValue))
                {
                    url = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Details",
                                             "mid", ModuleId.ToString(),
                                             "id", id.ToString(),
                                             "lid",_languageId.ToString(),
                                             "cid", !string.IsNullOrEmpty(cbCategories.SelectedValue) ? cbCategories.SelectedValue : "0",
                                             "aid", cbAudience.SelectedValue);
                }
                else
                {
                    url = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Details",
                                             "mid", ModuleId.ToString(),
                                             "id", id.ToString(),
                                             "lid", _languageId.ToString(),
                                             "cid", !string.IsNullOrEmpty(cbCategories.SelectedValue) ? cbCategories.SelectedValue : "0");    
                }
                
            }
            return url;
        }

        /// <summary>
        /// show an image if an asset is within n number of days
        /// </summary>
        /// <param name="dataItem"></param>
        /// <returns></returns>
        public string GetNewAssetImage(object dataItem)
        {
            var obj = dataItem as content;
            DateTime dt;
            string s = string.Empty;
            int n = 10;

            if (Settings[ModuleSetting.NewAssetNumOfDays] == null || !int.TryParse(Settings[ModuleSetting.NewAssetNumOfDays].ToString(), out n))
            {
                n = 10;
            }

            if (DateTime.TryParse(Utils.GetContentFieldValue(CspContext,obj, "Content_Asset_Date"), out dt) && DateTime.Now.Subtract(dt).TotalDays <= n)
            {
            	s = string.Format("<img src='{0}' alt='' title='{1}' />", ControlPath + "Images/icon_new.png", GetLocalizedText("Label.NewAssetHoverText"));
            }

            return s;
        }

        /// <summary>
        /// show status image of an asset (publish or unpublish) only if users are in country manager role
        /// </summary>
        /// <param name="dataItem"></param>
        /// <returns></returns>
        public string GetAssetStatusImage(object dataItem)
        {
            var obj = (content)dataItem;
            string html = "";
            if (ModuleRole.CountryRegionalManager(UserInfo, _languageId) || HaveAdminPermission())
            {
				if (obj.stage_Id != Keys.CspPublishStageId)
				{
					html = "<img src='" + ControlPath + "Images/icon_not_published.png' alt='' title='" + GetLocalizedText("Label.NotPublishHoverText") + "' />";
				}
				else
				{
					html = "<img src='" + ControlPath + "Images/icon_published.png' alt='' title='" + GetLocalizedText("Label.PublishHoverText") + "' />";
				}
                
            }
            return html;
        }


		/// <summary>
		/// Gets the asset download link.
		/// </summary>
		/// <param name="dataItem">The data item.</param>
		/// <param name="fieldUrl">The field URL.</param>
		/// <returns></returns>
		public string GetAssetDownloadLink(object dataItem, string fieldUrl)
		{
			var content = (content)dataItem;
            var url = Utils.GetContentFieldValue(CspContext,content, fieldUrl);
			if (url != string.Empty == url.Contains(Cons.CONSUMER_DOMAIN_REPLACE_TEXT))
			{
				if (_cspId != -1) //Subs
				{
					var consumer = CspContext.companies_consumers.FirstOrDefault(a => a.companies_Id == _cspId);
					if (consumer != null)
					{
						var baseDomain = consumer.base_domain;
						url = url.Replace(Cons.CONSUMER_DOMAIN_REPLACE_TEXT, baseDomain);
					}
				}
				else //Normal user - Admin, then use default url from module setting
				{
					if (Settings[ModuleSetting.BaseDomain] != null && Settings[ModuleSetting.BaseDomain].ToString() != string.Empty)
					{
						url = url.Replace(Cons.CONSUMER_DOMAIN_REPLACE_TEXT, Settings[ModuleSetting.BaseDomain].ToString());
					}
				}
			}
			return url;
		}
        #endregion
        
        #region Implementation of IActionable

        public ModuleActionCollection ModuleActions
        {
            get
            {
                ModuleActionCollection Actions = new ModuleActionCollection();
                Actions.Add(
                    GetNextActionID(),
                    "Settings",
                    ModuleActionType.AddContent, "", ControlPath + "Images/edit.png", EditUrl("Permissions"), false, SecurityAccessLevel.Edit, true, false);
                return Actions;
            }
        }

        #endregion

    }
}