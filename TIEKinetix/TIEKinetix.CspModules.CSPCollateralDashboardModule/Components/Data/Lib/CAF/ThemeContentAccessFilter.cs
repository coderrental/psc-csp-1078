﻿using System;
using System.Collections.Generic;
using TIEKinetix.CspModules.CSPCollateralDashboardModule.Components.Data.Lib.Content;
using CR.ContentObjectLibrary.Data;

namespace TIEKinetix.CspModules.CSPCollateralDashboardModule.Components.Data.Lib.CAF
{
    public class ThemeContentAccessFilter : ContentAccessFilter
    {
        public List<int> ThemeIds { get; set; }
        public ThemeContentAccessFilter()
        {
            ThemeIds = new List<int>();
        }

        #region Overrides of ContentAccessFilter

        public override CspContents Query(CspDataContext csp)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}