﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Transactions;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using DotNetNuke.Instrumentation;
using TIEKinetix.CspModules.CSPCollateralDashboardModule.Components.Data.Lib.Content;

namespace TIEKinetix.CspModules.CSPCollateralDashboardModule.Components.Data.Lib.CAF
{
    public class SupplierContentAccessFilter : ContentAccessFilter
    {
        public List<int> SupplierIds { get; set; }

        public SupplierContentAccessFilter()
        {
            SupplierIds = new List<int>();
        }


        #region Overrides of ContentAccessFilter

        public override CspContents Query(CspDataContext csp)
        {
            ModuleDebug debug = new ModuleDebug();

            debug.Start("SupplierContentAccessFilter.Query Function");
            
            CspContents data = new CspContents();

            using (new TransactionScope(TransactionScopeOption.Required,
                                        new TransactionOptions
                                            {
                                                IsolationLevel = IsolationLevel.ReadUncommitted,
                                                Timeout = TimeSpan.FromSeconds(10)
                                            }))
            {
                List<object> queryParameters= new List<object>();
                SimpleSqlQueryBuilder queryBuilder = new SimpleSqlQueryBuilder();

                queryBuilder.Append(@"
declare @suppliers table (
	sid int primary key
)

declare @contentTypeIds table (
	ctid int primary key
)

declare @categoryIds table (
    id int primary key
)

declare @lng int = 1
");

                
                // apply language
                queryBuilder.Append("select @lng = [value]", RequestedLanguage);

                // apply suppliers
                foreach (int supplierId in SupplierIds)
                {
                    queryBuilder.Append("insert into @suppliers(sid) values ([value])", supplierId);

                }
                
                // apply content types
                foreach (int contentTypeId in ContentTypeIds)
                {
                    queryBuilder.Append("insert into @contentTypeIds(ctid) values ([value])", contentTypeId);
                }

                //apply categories
                if (CategoryId != -1)
                {
                    // get a list of categories starting from category id
                    queryBuilder.Append(@"
;with cte(id,pid,l) as
(
select c.categoryId,c.parentId,0
from categories c
where c.categoryId=[value]

union all

select c1.categoryId, c1.parentId, cte.l+1
from cte
	join categories c1 on c1.parentId=cte.id
)
insert into @categoryIds(id)
select a.id
from cte a	
order by a.l", CategoryId);

                    queryBuilder.Append(@"
select ctf.fieldname, cf.value_text,c.content_Id,c.content_main_Id,c.content_types_languages_Id,c.stage_Id,cm.content_types_Id,cm.supplier_Id, cc.category_Id
from content_main cm 
	join content c on cm.content_main_Id = c.content_main_Id	
	join content_fields cf on cf.content_Id=c.content_Id
	join content_types_fields ctf on ctf.content_types_fields_Id=cf.content_types_fields_Id	
    join content_categories cc on cc.content_main_Id=cm.content_main_Id
    join @categoryIds cid on cid.id=cc.category_Id
where c.content_types_languages_Id = @lng and exists (
	select 1 from @suppliers s 
		cross join @contentTypeIds ctid		
	where s.sid = cm.supplier_Id and ctid.ctid = cm.content_types_Id
) and c.active = 1
order by c.content_Id, c.content_main_Id, cc.category_Id");
                }
                else
                {
                    queryBuilder.Append(@"
select ctf.fieldname, cf.value_text,c.content_Id,c.content_main_Id,c.content_types_languages_Id,c.stage_Id,cm.content_types_Id,cm.supplier_Id, cc.category_Id
from content_main cm 
	join content c on cm.content_main_Id = c.content_main_Id	
	join content_fields cf on cf.content_Id=c.content_Id
	join content_types_fields ctf on ctf.content_types_fields_Id=cf.content_types_fields_Id	
    left join content_categories cc on cc.content_main_Id=cm.content_main_Id
where c.content_types_languages_Id = @lng and exists (
	select 1 from @suppliers s 
		cross join @contentTypeIds ctid
	where s.sid = cm.supplier_Id and ctid.ctid = cm.content_types_Id
) and c.active = 1
order by c.content_Id, c.content_main_Id, cc.category_Id");
                }

                debug.Start("Query Content");
                IEnumerable result = csp.ExecuteQuery<CspResultRecord>(queryBuilder.Query, queryBuilder.Parameters);
                debug.Stop();

                // parse
                debug.Start("Parse Content");
                Guid currentContentId = Guid.Empty, currentContentMainId = Guid.Empty;
                int? currentCagetoryId = -1;
                dynamic currentContent = null;
                bool bIgnore = false; // ignore record that has the same content id, content main id. if language is different, then content id should be different.

                foreach (CspResultRecord record in result)
                {
                    // start parsing
                    if (currentContentMainId == Guid.Empty)
                    {
                        currentContentMainId = record.content_main_Id;
                        currentContentId = record.content_Id;
                        currentCagetoryId = record.category_Id;
                
                        currentContent = new CspContent(record.content_types_languages_Id, record.content_types_Id, currentContentId, currentContentMainId, record.stage_Id);
                        if (currentCagetoryId.HasValue)
                            currentContent.Categories.Add(currentCagetoryId.Value);
                    }

                    // new content piece
                    if (currentContentId != record.content_Id || currentCagetoryId != record.category_Id)
                    {
                        if (!bIgnore)
                        {
                            data.Contents.Add(currentContent);
                        }
                        else
                        {
                            // reset the ignore flag to prevent skipping a content piece
                            bIgnore = false;
                        }

                        currentCagetoryId = record.category_Id;
                        
                        // if the next current is the same as the previous record with different category id, then we should skip it and add only the category id in
                        if (currentContentId == record.content_Id && currentContentMainId == record.content_main_Id)
                        {
                            currentContent.Categories.Add(currentCagetoryId.Value);
                            bIgnore = true;
                        }
                        else
                        {
                            currentContentId = record.content_Id;
                            currentContentMainId = record.content_main_Id;
                            currentContent = new CspContent(record.content_types_languages_Id, record.content_types_Id, currentContentId, currentContentMainId, record.stage_Id);
                            if (currentCagetoryId.HasValue)
                                currentContent.Categories.Add(currentCagetoryId.Value);
                        }

                        if (!data.ContentMainIds.Contains(currentContent.ContentMainId))
                            data.ContentMainIds.Add(currentContent.ContentMainId);
                    }

                    if (!bIgnore)
                        currentContent.CreateOrGetProperty(record.fieldname, record.value_text);
                    
                }

                // last record
                if (currentContent != null)
                {
                    // if content belongs to multiple categories, then the content will duplicate.
                    if (data.Contents.Count(a => a.ContentId == currentContent.ContentId) > 0)
                    {
                        if (currentCagetoryId.HasValue)
                            currentContent.Categories.Add(currentCagetoryId.Value);
                    }
                    // else add content to queue
                    else
                    {
                        data.Contents.Add(currentContent);
                    }
                }

                debug.Stop();
            }

            // get category list
            debug.Start("Query Categories");
            data.Categories = csp.categories.Where(a => a.active.HasValue && a.active.Value).ToList();
            debug.Stop();

            int key = Utils.GetIntegrationKey(UserInfo);
            
            debug.Start("Query Content Categories");
            SimpleSqlQueryBuilder query = new SimpleSqlQueryBuilder();
            query.Append("declare @ContentMainIds table ( id uniqueidentifier primary key)");
            foreach (Guid id in data.ContentMainIds)
            {
                query.Append("insert into @ContentMainIds(id) values ('" + id.ToString() + "')");
            }

            // get all category content because there is no consumer profile
            if (key == -1)
            {
                query.Append(@"
select cc.*
from content_categories cc
	join @ContentMainIds cm on cm.id=cc.content_main_Id
order by cc.category_Id, cc.content_main_Id	
");
                data.ContentCategories = csp.ExecuteQuery<content_category>(query.Query, query.Parameters).ToList();
            }
            else
            {
                //get categories using consumer profile if a company exists
                query.Append(@"
select cc.*
from content_categories cc
	join @ContentMainIds cm on cm.id=cc.content_main_Id
	join consumers_themes ct on ct.category_Id=cc.category_Id 	
where ct.consumer_Id = [value]
order by cc.category_Id, cc.content_main_Id	
", key);
            }

            data.ContentCategories = csp.ExecuteQuery<content_category>(query.Query, query.Parameters).ToList();
            debug.Stop();

            debug.Stop(); // end of function
            return data;
        }

        private List<int> GetCategories(CspDataContext dataContext, Guid contentMainId)
        {
            int key = Utils.GetIntegrationKey(UserInfo);

            // if no key, then return all
            if (key == -1)
                return (from cat in dataContext.content_categories.Where(a => a.content_main_Id == contentMainId)
                        select cat.category_Id).Distinct().ToList(); 

            return (from cc in dataContext.content_categories.Where(a=>a.content_main_Id == contentMainId)
                    join ct in dataContext.consumers_themes on cc.category_Id equals ct.category_Id
                    where ct.consumer_Id == key && SupplierIds.Contains(ct.supplier_Id.Value)
                    select cc.category_Id
                   ).Distinct().ToList();
        }

        #endregion
    }
}