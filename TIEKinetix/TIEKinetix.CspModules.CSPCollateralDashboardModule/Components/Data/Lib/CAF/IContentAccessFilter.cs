﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using CR.ContentObjectLibrary.Data;
using DotNetNuke.Entities.Users;
using TIEKinetix.CspModules.CSPCollateralDashboardModule.Components.Data.Lib.Content;


namespace TIEKinetix.CspModules.CSPCollateralDashboardModule.Components.Data.Lib.CAF
{
    public interface IContentAccessFilter
    {
        CspContents Query(CspDataContext csp);
    }

    public abstract class ContentAccessFilter : IContentAccessFilter
    {
        public int RequestedLanguage { get; set; }
        public int CategoryId { get; set; }
        public List<int> ContentTypeIds { get; set; }
        public UserInfo UserInfo { get; set; }

        protected ContentAccessFilter()
        {
            CategoryId = -1;
        }
        

        #region Implementation of IContentAccessFilter

        public abstract CspContents Query(CspDataContext csp);

        #endregion
    }

    public class SimpleSqlQueryBuilder
    {
        private readonly StringBuilder _query;
        private readonly List<object> _parameters;
        private int _counter;
        private const string CounterKey = "[value]";

        public SimpleSqlQueryBuilder()
        {
            _counter = 0;
            _query = new StringBuilder();
            _parameters = new List<object>();
        }

        public void Append(string s, object value)
        {
            if (s.IndexOf(CounterKey)>=0)
            {
                s = s.Replace(CounterKey, "{" + _counter + "}");
                _counter++;
                _parameters.Add(value);
            }
            _query.AppendLine(s);
        }
        public void Append(string s)
        {
            _query.AppendLine(s);
        }

        public string Query
        {
            get { return _query.ToString(); }
        }

        public object[] Parameters
        {
            get { return _parameters.ToArray(); }
        }
    }

    public class CspResultRecord
    {
        public string fieldname;
        public Guid content_Id;
        public string value_text;
        public Guid content_main_Id;
        public int content_types_languages_Id;
        public int stage_Id;
        public int content_types_Id;
        public int supplier_Id;
        public int? category_Id;

        public CspResultRecord()
        {
        }
    }

    public static class PredicateBuilder
    {
        public static Expression<Func<T, bool>> True<T>() { return f => true; }
        public static Expression<Func<T, bool>> False<T>() { return f => false; }

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> expr1,
                                                            Expression<Func<T, bool>> expr2)
        {
            var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
            return Expression.Lambda<Func<T, bool>>
                  (Expression.Or(expr1.Body, invokedExpr), expr1.Parameters);
        }

        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> expr1,
                                                             Expression<Func<T, bool>> expr2)
        {
            var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
            return Expression.Lambda<Func<T, bool>>
                  (Expression.And(expr1.Body, invokedExpr), expr1.Parameters);
        }
    }

    public class QueueDictionary<TKey, TValue> : Dictionary<TKey, TValue>
    {
        List<TKey> _q = new List<TKey>();
        readonly object  _padLock = new object();

        public QueueDictionary() : base() {}
        new public void Add(TKey key, TValue value)
        {
            lock (_padLock)
            {
                base.Add(key,value);
                _q.Add(key);
            }
        }
        public KeyValuePair<TKey, TValue>? Dequeue()
        {
            lock (_padLock)
            {
                if (_q.Count == 0)
                    return null;

                TKey key = _q[_q.Count - 1];
                _q.Remove(key);

                if (base.ContainsKey(key))
                {
                    TValue val = base[key];
                    base.Remove(key);
                    return new KeyValuePair<TKey, TValue>(key, val);
                }
                return null;
            }
        }
    }

}