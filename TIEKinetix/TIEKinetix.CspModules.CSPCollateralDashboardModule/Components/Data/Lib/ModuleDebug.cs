﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using TIEKinetix.CspModules.CSPCollateralDashboardModule.Components.Data.Lib.CAF;

namespace TIEKinetix.CspModules.CSPCollateralDashboardModule.Components.Data.Lib
{
    public class ModuleDebug
    {
        private DateTime _timer;
        private string _message;
        private QueueDictionary<string, DateTime> _dict;
        public ModuleDebug()
        {
            _timer = DateTime.Now;
            _dict = new QueueDictionary<string, DateTime>();
        }

        public void Start(string message)
        {
            _timer = DateTime.Now;
            _message = message;
            
            if (_dict.ContainsKey(message))
            {
                // reset timer if key exists
                _dict[message] = DateTime.Now;
            }
            else
            {
                _dict.Add(message, DateTime.Now);
            }
        }

        public void Start()
        {
            Start(Guid.NewGuid().ToString());
        }

        public void Stop()
        {
            KeyValuePair<string, DateTime>? timer = _dict.Dequeue();
            if (timer.HasValue)
            {
                TimeSpan ts = DateTime.Now.Subtract(timer.Value.Value);
                for (int i = 0; i < _dict.Count; i++)
                {
                    Debug.Write("*");
                }
                Debug.WriteLine("[" + timer.Value.Key + "]".PadRight(75-timer.Value.Key.Length)  + string.Format("{0}m {1}s {2}ms ({3})", ts.Minutes, ts.Seconds, ts.Milliseconds, ts.TotalMilliseconds));
            }
            else
            {
                Debug.WriteLine("Can't Dequeue Debug.Queue");
            }
        }
    }
}