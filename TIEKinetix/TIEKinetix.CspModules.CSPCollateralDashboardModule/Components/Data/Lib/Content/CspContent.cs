﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using CR.ContentObjectLibrary.Data;

namespace TIEKinetix.CspModules.CSPCollateralDashboardModule.Components.Data.Lib.Content
{
    public abstract class DynamicContentObject : DynamicObject, INotifyPropertyChanged
    {
        private readonly Dictionary<string, object> _dictionary;

        protected DynamicContentObject()
        {
            _dictionary = new Dictionary<string, object>();
        }

        #region Overrides of DynamicObject

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            if (HasProperty(binder.Name))
            {
                result = _dictionary[binder.Name];
            }
            else
            {
                result = string.Empty;
                AddProperty(binder.Name, string.Empty);
            }
            return true;
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            string memberName = binder.Name;

            if (_dictionary.ContainsKey(memberName))
            {
                _dictionary[memberName] = value;
            }
            else
            {
                _dictionary.Add(memberName, value);
            }

            OnPropertyChanged(memberName);
            return true;
        }

        #endregion

        #region Implementation of INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string prop)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        #endregion

        #region Methods

        public object GetProperty(string name)
        {
            return _dictionary[name];
        }

        protected void AddProperty(string name, string value)
        {
            _dictionary.Add(name, value);
        }

        protected bool HasProperty(string name)
        {
            return _dictionary.ContainsKey(name);
        }

        public object CreateOrGetProperty(string name, string value)
        {
            if (!HasProperty(name))
            {
                AddProperty(name, value);
            }
            return GetProperty(name);
        }

        #endregion
    }
    public class CspContents
    {
        public CompanyInfo Consumer { get; set; }

        public List<content_category> ContentCategories { get; set; }

        public List<category> Categories { get; set; }

        public List<Guid> ContentMainIds { get; set; }

        public List<dynamic> Contents;

        public CspContents()
        {
            Contents = new List<dynamic>();
            ContentCategories = new List<content_category>();
            ContentMainIds = new List<Guid>();
        }
    }
    public class CompanyInfo : DynamicContentObject
    {
    }

    public class CspContent : DynamicContentObject
    {
        public List<int> Categories { get; set; }
        public CompanyInfo Supplier { get; set; }
        public int LanguageId { get; set; }
        public int ContentTypeId { get; set; }
        public Guid ContentId { get; set; }
        public Guid ContentMainId { get; set; }
        public int StageId { get; set; }

        public CspContent()
        {
            LanguageId = ContentTypeId = StageId = -1;
            ContentId = ContentMainId = Guid.Empty;
        }

        public CspContent(int lngId, int contentTypesId, Guid currentContentId, Guid currentContentMainId, int stageId)
        {
            LanguageId = lngId;
            ContentTypeId = contentTypesId;
            ContentId = currentContentId;
            ContentMainId = currentContentMainId;
            StageId = stageId;
            Categories = new List<int>();
        }

        #region Overrides of Object

        public override string ToString()
        {
            string s = Categories.Aggregate("", (current, category) => current + category + ",");
            return string.Format("Content: {0}, Content Main: {1}, Categories: {2}", ContentId, ContentMainId, s);
        }

        #endregion
    }
}