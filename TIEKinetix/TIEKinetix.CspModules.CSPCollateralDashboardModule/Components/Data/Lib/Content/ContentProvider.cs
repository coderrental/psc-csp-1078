﻿using System;
using System.Linq;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Users;
using TIEKinetix.CspModules.CSPCollateralDashboardModule.Components.Data.Lib.CAF;

namespace TIEKinetix.CspModules.CSPCollateralDashboardModule.Components.Data.Lib.Content
{
    public class ContentProvider
    {
        public static CspContents GetCspContents(CspQueryParameters parameters, CspDataContext dataContext)
        {
            int integrationKey = Utils.GetIntegrationKey(parameters.ContentAccessFilter.UserInfo);

            string cacheKey = string.Format("CspContentKey_{0}_{1}_{2}_{3}",
                parameters.ContentAccessFilter.UserInfo.UserID, 
                parameters.ContentAccessFilter.UserInfo.PortalID, 
                parameters.ContentAccessFilter.RequestedLanguage,
                parameters.ContentAccessFilter.CategoryId);

            CspContents contents = GetCache(cacheKey) as CspContents;
        	//CspContents contents = null;
            if (contents == null || Utils.Check(parameters.ContentAccessFilter.UserInfo.PortalID, "SkipCache"))
            {
                CspQuery query = new CspQuery(parameters, dataContext);

                //
                // todo: extend CspQuery.Execute to receive an query string using a parser
                //
                contents = query.Execute();

                //
                // todo : make cache time a variable
                //
                DataCache.SetCache(cacheKey, contents, TimeSpan.FromMinutes(5));
            }
            return contents;
        }

        public static void ClearCache(CspQueryParameters parameters)
        {
            string cacheKey = string.Format("CspContentKey_{0}_{1}_{2}",
                parameters.ContentAccessFilter.UserInfo.UserID, parameters.ContentAccessFilter.UserInfo.PortalID, parameters.ContentAccessFilter.RequestedLanguage);
            DataCache.ClearCache(cacheKey);
        }

        private static object GetCache(string cacheKey)
        {
            return DataCache.GetCache(cacheKey);
        }

        public static dynamic GetCspContent(Guid contentId, string languageId)
        {
            return new CspContent();
        }
    }

    public class CspQueryParameters
    {
        private readonly string _query;
        public  ContentAccessFilter ContentAccessFilter;

        public CspQueryParameters(string query)
        {
            _query = query;
        }
    }

    /// <summary>
    /// query that deals with the content access filter and result a result set
    /// </summary>
    public class CspQuery
    {
        private readonly CspQueryParameters _parameters;
        private readonly CspDataContext _dataContext;

        public CspQuery(CspQueryParameters parameters, CspDataContext dataContext)
        {
            _parameters = parameters;
            _dataContext = dataContext;
        }

        public CspContents Execute()
        {
            CspContents contents = _parameters.ContentAccessFilter.Query(_dataContext);
            return contents;
        }
    }
}