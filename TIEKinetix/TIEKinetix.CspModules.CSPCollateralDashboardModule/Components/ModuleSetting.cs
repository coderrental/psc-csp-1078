﻿namespace TIEKinetix.CspModules.CSPCollateralDashboardModule.Components
{
    public class ModuleSetting
    {
        public static string AssetContentTypeId = "AssetContentTypeId";
        public static string CategoryContentTypeId = "CategoryContentTypeId";
        public static string NewAssetNumOfDays = "NewAssetNumOfDays";
        public static string SupplierId = "SupplierId";

    	public static string FilterCategoryTree = "FilterCategoryTree";
    	public static string BaseDomain = "BaseDomain";
    	public static string UserWithAdminRole = "UserWithAdminRole";
        public static string CompanySettingTabName = "CompanySettingTabName";
        public static string SyncLanguage = "SyncLanguage";
        public static string DisableNavigatePanel = "DisableNavigatePanel";
        public static string StageIdToNotDisplay = "StageIdToNotDisplay";
        public static string SecureAssets = "SecureAssets";
        public static string SecureAssetPath = "SecureAssetPath";
    }
}