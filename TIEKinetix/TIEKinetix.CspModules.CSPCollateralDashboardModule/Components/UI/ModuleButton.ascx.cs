﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TIEKinetix.CspModules.CSPCollateralDashboardModule.Components.UI
{
    public partial class ModuleButton : LinkButton
    {
        #region Overrides of LinkButton

        protected override void RenderContents(HtmlTextWriter writer)
        {
            writer.WriteBeginTag("center");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteEncodedText(Text);
            writer.WriteEndTag("center");
        }

        #endregion
    }
}