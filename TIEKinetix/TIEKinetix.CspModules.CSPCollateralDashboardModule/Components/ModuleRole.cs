﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CR.DnnModules.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Users;
using TIEKinetix.CspModules.CSPCollateralDashboardModule.Components.Data;

namespace TIEKinetix.CspModules.CSPCollateralDashboardModule.Components
{
    public class ModuleRole
    {
        public static bool IsInRole(UserInfo userInfo, params string[] roles)
        {
            bool flag = true;
            foreach (string role in roles)
            {
                flag &= userInfo.IsInRole(role);
                if (!flag)
                    break;
            }
            return flag;
        }

        /// <summary>
        /// Check if user is in role
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="lngId"></param>
        /// <returns></returns>
        public static bool CountryRegionalManager(UserInfo userInfo, int lngId)
        {
            string cacheKey = string.Format("CountryRegionalManager_{0}_{1}_{2}", userInfo.PortalID, userInfo.UserID, lngId);
            if (DataCache.GetCache(cacheKey) != null)
                return (bool) DataCache.GetCache(cacheKey);

            bool f = false;

            if (IsInRole(userInfo, "Administrators"))
            {
                f = true;
            }

            if (!f && Utils.GetIntegrationKey(userInfo) != -1)
            {
                using (DnnContext context = new DnnContext(Config.GetConnectionString()))
                {
                    CSPCollateralDashboardModule_Permission permission = context.CSPCollateralDashboardModule_Permissions.FirstOrDefault(a => a.UserId == userInfo.UserID && a.PortalId == userInfo.PortalID);
                    if (permission != null && permission.CSPCollateralDashboardModule_Languages.Count(a => a.LanguageId == lngId) > 0)
                    {
                        f = true;
                    }
                }
            }
            DataCache.SetCache(cacheKey, f, TimeSpan.FromSeconds(5));
            return f;
        }
    }
}