﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TIEKinetix.CspModules.CSPCollateralDashboardModule.Components
{
	public class Cons
	{
		public const string LOCAL_PARTNER_TYPE_TEXT = "Local_Partner_Type";
		public const string LOCAL_PARTNER_RESELLER_TEXT = "reseller";
		public const string LOCAL_PARTNER_DISTRIBUTOR_TEXT = "distributor";
		public const string CONSUMER_DOMAIN_REPLACE_TEXT = "{consumer:FBaseDomain}";

	    
	    public const string LOCAL_AUDIENCE_CONSUMER_DEFAULT_VALUE = "1";
        public const string LOCAL_AUDIENCE_BUSINESS_DEFAULT_VALUE = "2";
        public const string COMPANY_PARAMETER_AUDIENCE_NAME = "Local_Audience";
        public const string AUDIENCE_BUSINESS_TEXT = "Business";
        public const string AUDIENCE_CONSUMER_TEXT = "Consumer";
	    public const string LOCAL_AUDIENCE_FIELD_NAME = "Asset_Local_Audience";
        public const string SETTING_LIST_LOCAL_AUDIENCES = "ListLocalAudiences";
        public const string SETTING_LIST_LOCAL_PARTNER_TYPE = "ListLocalPartnerType";

        public const int RESELLER_CATEGORY_DEFAULTID = 42;
        public const int DISTRIBUTOR_CATEGORY_DEFAULTID = 2;
	}
}