﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Localization;
using DotNetNuke.Services.Log.EventLog;
using TIEKinetix.CspModules.CSPCollateralDashboardModule.Entities;

namespace TIEKinetix.CspModules.CSPCollateralDashboardModule.Components
{
    public class DnnModuleBase : PortalModuleBase
    {
        private string _localResourceFile;
        public string CurrentLanguage;
        protected EventLogController DnnEventLog;
        protected internal int AssetContentTypeId;
        protected internal int CategoryContentTypeId;
        protected internal int SupplierId;
        protected internal int StageIdToNotDisplay;
        protected CspDataContext CspContext;
        protected DnnDataContext DnnDataContext;
        protected internal List<LocalAudience> ListLocalAudiences;
        protected internal List<LocalPartnerType> ListLocalPartnerType;

        /// <summary>
        /// Handles the Init event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void Page_Init(object sender, EventArgs e)
        {

            // get asset content type id, category content type id and supplier id from settings
            if (Settings[ModuleSetting.AssetContentTypeId] == null || !int.TryParse(Settings[ModuleSetting.AssetContentTypeId].ToString(), out AssetContentTypeId))
                AssetContentTypeId = 1;
            if (Settings[ModuleSetting.CategoryContentTypeId] == null || !int.TryParse(Settings[ModuleSetting.CategoryContentTypeId].ToString(), out CategoryContentTypeId))
                CategoryContentTypeId = 2;
            if (Settings[ModuleSetting.SupplierId] == null || !int.TryParse(Settings[ModuleSetting.SupplierId].ToString(), out SupplierId))
                SupplierId = 12;
            if (Settings[ModuleSetting.StageIdToNotDisplay] != null)
            {
                StageIdToNotDisplay = 0;
                int.TryParse(Settings[ModuleSetting.StageIdToNotDisplay].ToString(), out StageIdToNotDisplay);
            }


            // get common resource file so all entries would be in 1 file instead of in many different files
            _localResourceFile = Utils.GetCommonResourceFile("CSPCollateralDashboardModule", LocalResourceFile);
            CurrentLanguage = Thread.CurrentThread.CurrentCulture.Name;
            DnnEventLog = new EventLogController();
            ModuleConfiguration.ModuleTitle = GetLocalizedText(Keys.ModuleTitleKey);

            // get csp context
            CspContext = new CspDataContext(Utils.GetConnectionString(PortalId));
            // get dnn context
            DnnDataContext = new DnnDataContext(Config.GetConnectionString());

            GetListLocalAudiences();
            GetListLocalPartnerType();

            InitStoreProc();
        }

        /// <summary>
        /// Gets the type of the list local partner.
        /// </summary>
        private void GetListLocalPartnerType()
        {
            ListLocalPartnerType = new List<LocalPartnerType>();
            if (Settings[Cons.SETTING_LIST_LOCAL_PARTNER_TYPE] != null)
            {
                var listPartnerTypeArr = Settings[Cons.SETTING_LIST_LOCAL_PARTNER_TYPE].ToString().Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                if (listPartnerTypeArr.Any())
                {
                    foreach (var item in listPartnerTypeArr)
                    {
                        var arr = item.Split(new[] { "-" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        ListLocalPartnerType.Add(new LocalPartnerType
                        {
                            Name = arr[0],
                            CatgegoryId = int.Parse(arr[1])
                        });
                    }
                }
            }
        }

        /// <summary>
        /// Gets the list local audiences.
        /// </summary>
        private void GetListLocalAudiences()
        {
            ListLocalAudiences = new List<LocalAudience>();
            if (Settings[Cons.SETTING_LIST_LOCAL_AUDIENCES] != null)
            {
                var listarr = Settings[Cons.SETTING_LIST_LOCAL_AUDIENCES].ToString().Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                if (listarr.Any())
                {
                    foreach (var item in listarr)
                    {
                        ListLocalAudiences.Add(new LocalAudience
                        {
                            AudienceName = item,
                            Id = listarr.IndexOf(item)
                        });
                    }
                }
            }
            if (!ListLocalAudiences.Any())
            {
                ListLocalAudiences.Add(new LocalAudience
                {
                    AudienceName = GetLocalizedText("Label.AudienceConsumer"),
                    Id = int.Parse(Cons.LOCAL_AUDIENCE_CONSUMER_DEFAULT_VALUE)
                });
                ListLocalAudiences.Add(new LocalAudience
                {
                    AudienceName = GetLocalizedText("Label.AudienceBusiness"),
                    Id = int.Parse(Cons.LOCAL_AUDIENCE_BUSINESS_DEFAULT_VALUE)
                });
            }
        }
        

        /// <summary>
        /// Get localized text for the current portal language
        /// </summary>
        /// <param name="key">Resource Key</param>
        /// <returns>Localized text</returns>
        public string GetLocalizedText(string key)
        {
            return Localization.GetString(key, _localResourceFile);
        }

        private void InitStoreProc()
        {
            string query = "SELECT * FROM sys.objects WHERE type = 'p' AND name = 'GetContentFieldValue'";
            int procCount = CspContext.ExecuteQuery<object>(query).Count();
            //Create the stored proc
            if (procCount <= 0)
            {
                try
                {
                    CspContext.ExecuteCommand(@"
CREATE PROCEDURE dbo.GetContentFieldValue
	(
	@contentid uniqueidentifier,
	@field nvarchar(MAX)
	)
AS
SET NOCOUNT ON;
    select cf.value_text from content_fields cf
    join content_types_fields ctf on ctf.content_types_fields_Id = cf.content_types_fields_Id
    join content ct on ct.content_Id = cf.content_Id
    where ctf.fieldname = @field and ct.content_Id = @contentid
	RETURN;");
                }
                catch (Exception exc)
                {

                }
            }
        }

        /// <summary>
        /// Gets the contents.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 11/15/2012 - 7:14 PM
        public List<content> GetContentsFromCategoryId(int langId, int contentTypeId, int suppId, int catId)
        {
            var query =
                @"
  select * from content c
  join content_main cm on cm.content_main_Id = c.content_main_Id
  join content_types ct on ct.content_types_Id = cm.content_types_Id
  join content_categories cc on cc.content_main_Id = cm.content_main_Id
  join categories cat on cat.categoryId = cc.category_Id
  where c.content_types_languages_Id = {0} and ct.content_types_Id = {1} and cat.categoryId = {2} and cm.supplier_Id = {3}";
            var result = CspContext.ExecuteQuery<content>(query, langId, contentTypeId, catId, suppId);
            return result.ToList();
        }

		/// <summary>
		/// Haves the admin permission.
		/// </summary>
		/// <returns></returns>
		public bool HaveAdminPermission()
		{
			if (UserInfo.IsSuperUser || UserInfo.IsInRole(PortalSettings.AdministratorRoleName))
				return true;
			if (Settings[ModuleSetting.UserWithAdminRole] == null)
				return false;
			var listUserInRole = Settings[ModuleSetting.UserWithAdminRole].ToString().Split(',').ToList();
			if (listUserInRole.Contains(UserInfo.UserID.ToString()))
				return true;
			return false;
		}

        /// <summary>
        /// Write message to Dnn event view
        /// </summary>
        /// <param name="message">message to write</param>
        public void Log(string message)
        {
            DnnEventLog.AddLog("DEBUG", message, PortalSettings, UserId, EventLogController.EventLogType.ADMIN_ALERT);
        }
        
        /// <summary>
        /// Get portal external identifier used to identify portal channel.
        /// </summary>
        /// <returns></returns>
        protected string GetPortalExternalIdentifier()
        {
            string s = Utils.GetSetting(PortalId, "ExternalIdentifier");
            return !string.IsNullOrEmpty(s) ? s : Request.Url.Host;
        }

        /// <summary>
        /// return a json object with consumer information. this object is being used in the skin
        /// </summary>
        protected string ConsumerInfoJson
        {
            get
            {
                string value = "{}";
                if (Utils.GetIntegrationKey(UserInfo) != -1)
                {
                    company consumer = CspContext.companies.First(a => a.companies_Id == Utils.GetIntegrationKey(UserInfo));
                    if (consumer != null)
                    {
                        company parentCompany = CspContext.companies.FirstOrDefault(a => a.companies_Id == (consumer.parent_companies_Id.HasValue ? consumer.parent_companies_Id.Value : -1));
                        value = (new
                        {
                            companyname = consumer.companyname,
                            companies_Id = consumer.companies_Id,
                            country = consumer.country,
                            sId = (parentCompany != null) ? parentCompany.companies_Id : -1,
                            sName = (parentCompany != null) ? parentCompany.companyname : "",
                            lngDesc = Thread.CurrentThread.CurrentCulture.EnglishName
                        }).ToJson();
                    }
                }
                return value;
            }
        }

        public string Encode(string s)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (DeflateStream gzip = new DeflateStream(memoryStream, CompressionMode.Compress))
                {
                    using (StreamWriter streamWriter = new StreamWriter(gzip, Encoding.UTF8))
                    {
                        streamWriter.Write(s);
                    }
                }
                return Convert.ToBase64String(memoryStream.ToArray());
            }
        }
    }
}