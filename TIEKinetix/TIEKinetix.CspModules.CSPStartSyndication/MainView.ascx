﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainView.ascx.cs" Inherits="TIEKinetix.CspModules.CSPStartSyndication.MainView" %>
<%@ Import Namespace="CR.ContentObjectLibrary.Data" %>
<%@ Import Namespace="CR.DnnModules.Common" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<telerik:radajaxloadingpanel runat="server" ID="ajaxLoadingPanel" OnClientShowing="ShowingContent" OnClientHiding="HidingContent"/>
<telerik:RadAjaxManager runat="server" ID="ajaxManager" DefaultLoadingPanelID="ajaxLoadingPanel">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="AssetsDataPager">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="csp_categories_panel" />                
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>--%>
	<asp:Panel runat="server" ID="mainPanel" CssClass="module_main_wrap">
	    <asp:Panel ID="TopicBreadcrumbPanel" runat="server" CssClass="csp-clear csp-asset-breadcrumb-mainview" Visible="False"></asp:Panel>
		<asp:Panel CssClass="csp_asset_filter" ID="NavPanel" runat="server">
			<table class="csp_asset_filter_container">
				<tr>
					<td runat="server" ID="LanguagePanel">
						<div class="csp-asset-title"><asp:Label runat="server" ID="lbLanguage"></asp:Label></div>
						<telerik:RadComboBox AutoPostBack="True" Filter="StartsWith" runat="server" ID="cbLanguage" Width="180" OnSelectedIndexChanged="cbLanguage_OnSelectedIndexChanged" />
					</td>
					<td>
						<div class="csp-asset-title"><asp:Label runat="server" ID="lbCategories"/></div>
						<telerik:RadComboBox runat="server" ID="cbCategories" ShowToggleImage="true" Width="400px" OnClientDropDownOpened="OnClientDropDownOpenedHandler" AutoPostBack="true">
							<ItemTemplate>
								<telerik:RadTreeView ID="categoryTree" runat="server" OnClientNodeClicking="nodeClicking" OnNodeClick="categoryTree_OnNodeClick">
									<DataBindings>
										<telerik:RadTreeNodeBinding Expanded="true" />
									</DataBindings>
								</telerik:RadTreeView>                                        
							</ItemTemplate>
							<Items>
								<telerik:RadComboBoxItem Text="adasd" />
							</Items>
							<CollapseAnimation Type="None" />
						</telerik:RadComboBox>
						
					</td>
					<td>
						<div class="csp-asset-title"><asp:Label runat="server" ID="lbAudience" /></div>
						<telerik:RadComboBox runat="server" AutoPostBack="True" OnSelectedIndexChanged="cbAudience_OnSelectedIndexChanged" ID="cbAudience" Width="180"></telerik:RadComboBox>
					</td>
				</tr>
			</table>
		</asp:Panel>
		<div id="csp_asset_headline">
			<asp:Label runat="server" ID="lbCategoryHeadline"></asp:Label>
		</div>
		<asp:Panel runat="server" ID="csp_categories_panel">
			<div id="csp_list_categories">
				<telerik:RadListView runat="server" ID="listView" ItemPlaceholderID="CategoriesPlaceHolder">
				<LayoutTemplate>
					<asp:Panel runat="server" ID="AssetsPanel">
						<div class="csp-assets">
							<ul>
							<asp:PlaceHolder ID="CategoriesPlaceHolder" runat="server"></asp:PlaceHolder>
							</ul>
						</div>
						<div class="csp-assets-paging">
						</div>
					</asp:Panel>            
				</LayoutTemplate>
				<ItemTemplate>
					<li>
					<table class="csp_category_block">
						<tr>
							<td class="csp_asset_title"><%# Eval("CategoryName")%></td>
						</tr>
						<tr>
							<td class="csp_category_block_mainimage">
								<%# GetCategoryBlockMainImage(Container.DataItem)%>
							</td>
						</tr>
						<tr>
							<td class="csp_category_block_subimage">
								<%# GetCategoryBlockSubImage(Container.DataItem) %>
							</td>
						</tr>
						<tr>
							<td class="csp_category_block_link">
								<%# GetSubCategoriesListLayout(Container.DataItem)%>	
							</td>
						</tr>
					</table>
					</li>
				</ItemTemplate>
				<EmptyDataTemplate>            
					<div class="csp-no-asset"><%=GetLocalizedText("Label.NoAvailableCategory")%></div>
				</EmptyDataTemplate>
			</telerik:RadListView>
			</div>
		</asp:Panel>
	</asp:Panel>
	<asp:Panel runat="server" ID="bottomPanel" CssClass="module_bottom_wrap">
		
	</asp:Panel>
	<telerik:RadScriptBlock runat="server">
		<script type="text/javascript">
			$(function () {
				if ($('.csp_category_block').length > 2)
					$('.csp-assets').carousel({ dispItems: 2, pagination: true });
				else
					$('.csp-assets').carousel({ dispItems: 2, pagination: false });
				$('.csp-assets').append("<div class='clear'></div>");
				$('.carousel-pagination').after($('<div>').addClass('clear'));
			});
			function nodeClicking(sender, args) {
				var cb = $find("<%=cbCategories.ClientID %>");
				var node = args.get_node();
				cb.set_text(node.get_text());
				cb.set_value(node.get_value());
				cb.trackChanges();
				cb.get_items().getItem(0).set_text(node.get_text());
				cb.get_items().getItem(0).set_value(node.get_value());
				cb.commitChanges();
				cb.hideDropDown();
				cb.attachDropDown();
			}
			function OnClientDropDownOpenedHandler(sender, eventArgs) {
				var tree = sender.get_items().getItem(0).findControl("categoryTree");
				var selectedNode = tree.get_selectedNode();
				if (selectedNode) {
					selectedNode.scrollIntoView();
				}
			}
			function ShowingContent(sender, eventArgs) {
				/*eventArgs.set_cancelNativeDisplay(true);
				$telerik.$(eventArgs.get_loadingElement()).fadeIn();*/
			}
			function HidingContent(sender, eventArgs) {
				/*eventArgs.set_cancelNativeDisplay(true);
				$telerik.$(eventArgs.get_loadingElement()).fadeOut();*/
			}
		</script>
	</telerik:RadScriptBlock>