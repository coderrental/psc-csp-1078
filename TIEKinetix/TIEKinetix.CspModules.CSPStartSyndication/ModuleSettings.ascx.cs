﻿// ***********************************************************************
// Assembly         : TIEKinetix.CspModules.CSPStartSyndication
// Author           : VU DINH
// Created          : 01-03-2013
//
// Last Modified By : VU DINH
// Last Modified On : 06-06-2013
// ***********************************************************************
// <copyright file="ModuleSettings.ascx.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using CR.DnnModules.Common.CategoryTree;
using DotNetNuke.Entities.Modules;
using TIEKinetix.CspModules.CSPStartSyndication.Common;
using TIEKinetix.CspModules.CSPStartSyndication.Entities;
using System.Linq;
using Telerik.Web.UI;

namespace TIEKinetix.CspModules.CSPStartSyndication
{
    /// <summary>
    /// Class ModuleSettings
    /// </summary>
	public partial class ModuleSettings : ModuleSettingsBase
    {

        private List<LocalAudience> _listLocalAudience;
        private List<LocalPartnerType> _listLocalPartnerType;
        private CspDataContext _cspDataContext;
        /// <summary>
        /// Loads the settings.
        /// </summary>
        /// <update>
        /// 12/12/18 by @ducuytran:
        /// _ Add PPPGeneral Content Types Id setting for Category View content text.
        ///   </update>
		public override void LoadSettings()
        {
            LoadLocalAudienceList();

            LoadLocalPartnertypeList();

            RenderCategoriesTree();

            if (Settings[Cons.SETTING_CAMPAIGN_CONTENT_TYPE_ID] != null)
				tbCampaignContentTypeId.Text = Settings[Cons.SETTING_CAMPAIGN_CONTENT_TYPE_ID].ToString();
			if (Settings[Cons.SETTING_CATEGORY_CONTENT_TYPE_ID] != null)
				tbCategoryCampaignTypeId.Text = Settings[Cons.SETTING_CATEGORY_CONTENT_TYPE_ID].ToString();
			if (Settings[Cons.SETTING_SUPPLIER_ID] != null)
				tbSupplierId.Text = Settings[Cons.SETTING_SUPPLIER_ID].ToString();
			if (Settings[Cons.SETTING_ASSET_CONTENT_TYPE_ID] != null)
				tbAssetContentTypeId.Text = Settings[Cons.SETTING_ASSET_CONTENT_TYPE_ID].ToString();
			if (Settings[Cons.SETTING_PPPGENERAL_CONTENT_TYPE_ID] != null)
				tbPPPGeneralContentTypeId.Text = Settings[Cons.SETTING_PPPGENERAL_CONTENT_TYPE_ID].ToString();

			if (Settings[Cons.SETTING_GENERATE_EMBED_CODE_SRC] != null)
				tbEmbedCodeSrc.Text = Settings[Cons.SETTING_GENERATE_EMBED_CODE_SRC].ToString();
			if (Settings[Cons.SETTING_CAMPAIGN_PREVIEW_URL] != null)
				tbCampaignPreviewURL.Text = Settings[Cons.SETTING_CAMPAIGN_PREVIEW_URL].ToString();
			if (Settings[Cons.SETTING_CAMPAIGN_DESCRIPTION_LENGTH] != null)
				tbCampaignDescLength.Text = Settings[Cons.SETTING_CAMPAIGN_DESCRIPTION_LENGTH].ToString();
            if (Settings[Cons.COMPANY_SETTINGS_TAB_NAME] != null)
                tbTabName.Text = Settings[Cons.COMPANY_SETTINGS_TAB_NAME].ToString();
			if (Settings[Cons.SETTING_PORTAL_INSTANCE_NAME] != null)
				tbPortalInstance.Text = Settings[Cons.SETTING_PORTAL_INSTANCE_NAME].ToString();
            if (Settings[Cons.SETTING_SYNC_LANGUAGE] != null)
                cbxSyncLang.Checked = int.Parse(Settings[Cons.SETTING_SYNC_LANGUAGE].ToString()) == 1;
            if (Settings[Cons.SETTING_DISABLE_NAVPANEL] != null)
                cbxDisableNavPanel.Checked = int.Parse(Settings[Cons.SETTING_DISABLE_NAVPANEL].ToString()) == 1;
            
            tbPreviewPopupHeight.Text = Settings[Cons.SETTING_PREVIEW_POPUP_HEIGHT] != null ? Settings[Cons.SETTING_PREVIEW_POPUP_HEIGHT].ToString() : Cons.DEFAULT_PREVIEWPOPUP_HEIGHT.ToString();
		    tbPreviewPopupWidth.Text = Settings[Cons.SETTING_PREVIEW_POPUP_WIDTH] != null ? Settings[Cons.SETTING_PREVIEW_POPUP_WIDTH].ToString() : Cons.DEFAULT_PREVIEWPOPUP_WIDTH.ToString();
		}

        /// <summary>
        /// Loads the local audience list.
        /// </summary>
        private void LoadLocalAudienceList()
        {
            _listLocalAudience = new List<LocalAudience>();
            if (Settings[Cons.SETTING_LIST_LOCAL_AUDIENCES] != null)
            {
                var listAudiencearr = Settings[Cons.SETTING_LIST_LOCAL_AUDIENCES].ToString().Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                if (listAudiencearr.Any())
                {
                    foreach (var item in listAudiencearr)
                    {
                        hfListAudience.Value += item + ",";
                        _listLocalAudience.Add(new LocalAudience
                        {
                            AudienceName = item,
                            Id = listAudiencearr.IndexOf(item)
                        });
                    }
                }
            }
        }

        /// <summary>
        /// Loads the local partnertype list.
        /// </summary>
        private void LoadLocalPartnertypeList()
        {
            _listLocalPartnerType = new List<LocalPartnerType>();
            if (Settings[Cons.SETTING_LIST_LOCAL_PARTNER_TYPE] != null)
            {
                var listPartnerTypeArr = Settings[Cons.SETTING_LIST_LOCAL_PARTNER_TYPE].ToString().Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                if (listPartnerTypeArr.Any())
                {
                    foreach (var item in listPartnerTypeArr)
                    {
                        var arr = item.Split(new[] { "-" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        hfListPartnerType.Value += item + ",";
                        _listLocalPartnerType.Add(new LocalPartnerType
                        {
                            Name = arr[0],
                            CatgegoryId = int.Parse(arr[1])
                        });
                    }
                }
            }
        }

        /// <summary>
        /// Renders the categories tree.
        /// </summary>
        private void RenderCategoriesTree()
        {
            _cspDataContext = new CspDataContext(Utils.GetConnectionString(PortalId));
            var listCustomCategories = new List<CustomCategory>();
            foreach (var category in _cspDataContext.categories.Where(a => a.active == true) )
            {
              listCustomCategories.Add(new CustomCategory
                  {
                      Id = category.categoryId,
                      Count = 1,
                      Depth = category.depth.HasValue ? category.depth.Value : 0,
                      ParentId = category.parentId.HasValue ? category.parentId.Value : 0,
                      Text = category.categoryText
                  });
            }

            var tree = (RadTreeView)cbCategories.Items[0].FindControl("categoryTree");
            //Render tree
            tree.Nodes.Clear();
            CategoryTreeView.RenderCategoryTree(tree, listCustomCategories);

            if (Settings[Cons.SETTING_CATEGORIES_SKIP_LEVEL] != null && Settings[Cons.SETTING_CATEGORIES_SKIP_LEVEL].ToString() != string.Empty)
            {
                var listCatsToSkip = Settings[Cons.SETTING_CATEGORIES_SKIP_LEVEL].ToString().Split(new[] {","},StringSplitOptions.RemoveEmptyEntries).ToList();
                foreach (var item in listCatsToSkip)
                {
                    var node = tree.FindNodeByValue(item);
                    if (node != null)
                        node.Checked = true;
                }
            }
            
        }

        /// <summary>
        /// Updates the settings.
        /// </summary>
		public override void UpdateSettings()
		{
			var objectModule = new ModuleController();
			objectModule.UpdateTabModuleSetting(TabModuleId,Cons.SETTING_CAMPAIGN_CONTENT_TYPE_ID, tbCampaignContentTypeId.Text);
			objectModule.UpdateTabModuleSetting(TabModuleId,Cons.SETTING_CATEGORY_CONTENT_TYPE_ID, tbCategoryCampaignTypeId.Text);
			objectModule.UpdateTabModuleSetting(TabModuleId,Cons.SETTING_ASSET_CONTENT_TYPE_ID, tbAssetContentTypeId.Text);
			objectModule.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_PPPGENERAL_CONTENT_TYPE_ID, tbPPPGeneralContentTypeId.Text);
			objectModule.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_SUPPLIER_ID, tbSupplierId.Text);
			objectModule.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_GENERATE_EMBED_CODE_SRC, tbEmbedCodeSrc.Text);
			objectModule.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_CAMPAIGN_DESCRIPTION_LENGTH, tbCampaignDescLength.Text);
			objectModule.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_CAMPAIGN_PREVIEW_URL, tbCampaignPreviewURL.Text);
            objectModule.UpdateTabModuleSetting(TabModuleId, Cons.COMPANY_SETTINGS_TAB_NAME, tbTabName.Text);
			objectModule.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_PORTAL_INSTANCE_NAME, tbPortalInstance.Text);
            objectModule.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_SYNC_LANGUAGE, cbxSyncLang.Checked ? "1" : "0");
            objectModule.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_DISABLE_NAVPANEL, cbxDisableNavPanel.Checked ? "1" : "0");
            objectModule.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_PREVIEW_POPUP_HEIGHT, tbPreviewPopupHeight.Text);
            objectModule.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_PREVIEW_POPUP_WIDTH, tbPreviewPopupWidth.Text);

            #region [ Save Local Audience list ]
            objectModule.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_LIST_LOCAL_AUDIENCES, hfListAudience.Value);
            //update resx file
            AddAudienceTranslate(hfListAudience.Value);
            #endregion

            #region [ Save Local Audience list ]
            objectModule.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_LIST_LOCAL_PARTNER_TYPE, hfListPartnerType.Value);
            #endregion

            #region [Save Categories skip level]
            var tree = (RadTreeView)cbCategories.Items[0].FindControl("categoryTree");
            if (tree.CheckedNodes.Any())
            {
                var listCatsToSkip = tree.CheckedNodes.Aggregate(string.Empty, (current, selectedNode) => current + (selectedNode.Value + ","));
                objectModule.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_CATEGORIES_SKIP_LEVEL, listCatsToSkip);
            }
            else
            {
                objectModule.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_CATEGORIES_SKIP_LEVEL, string.Empty);
            }
            #endregion

            ModuleController.SynchronizeModule(ModuleId);
        }

        /// <summary>
        /// Adds the audience translate.
        /// </summary>
        private void AddAudienceTranslate(string data)
        {
            var listAudience = data.Split(new[] {","}, StringSplitOptions.RemoveEmptyEntries).ToList();
            if (listAudience.Any())
            {
                var filename = Server.MapPath(ControlPath + "App_LocalResources\\CSPStartSyndication.ascx.resx");
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(filename);

                //add new translation item
                foreach (var audience in listAudience)
                {
                    var name = "LocalAudience." + audience;
                    //remove exist nodes
                    XmlNodeList xnList = xmlDoc.SelectNodes("/root/data[@name='" + name + "']");
                    if (xnList != null)
                    {
                        foreach (XmlNode node in xnList)
                        {
                            if (node.ParentNode != null)
                                node.ParentNode.RemoveChild(node);
                        }
                    }

                    //add new node
                    XmlElement newElem = xmlDoc.CreateElement("data");
                    newElem.SetAttribute("name", name);
                    newElem.SetAttribute("xml:space", "preserve");
                    newElem.InnerXml = "<value></value>";
                    var xmlElement = newElem["value"];
                    if (xmlElement != null)
                        xmlElement.InnerText = audience;
                    if (xmlDoc.DocumentElement != null)
                        xmlDoc.DocumentElement.AppendChild(newElem);
                }
                xmlDoc.Save(filename);
            }

            
        }

        #region [ Functions to render layout content]
        /// <summary>
        /// Gets the list of local audience.
        /// </summary>
        /// <returns>System.String.</returns>
        public string GetListOfLocalAudienceLayout()
        {
            if (_listLocalAudience == null || !_listLocalAudience.Any())
                return string.Empty;

            var html = new StringBuilder ();
            foreach (var localAudience in _listLocalAudience.OrderBy(a => a.Id))
            {
                var deleteLink = @"<a href='#' class='delete_audience'>Delete</a>";
                html.Append(string.Format(@"
<tr class='row'>
    <td class='name'>{0}</td>
    <td>{1}</td>
</tr>"
                    , localAudience.AudienceName, deleteLink));
            }
            return html.ToString();
        }

        /// <summary>
        /// Gets the list of local partner type layout.
        /// </summary>
        /// <returns>System.String.</returns>
        public string GetListOfLocalPartnerTypeLayout()
        {
            if (_listLocalPartnerType == null || !_listLocalPartnerType.Any())
                return string.Empty;
            var html = new StringBuilder();
            foreach (var localPartnerType in _listLocalPartnerType)
            {
                var deleteLink = @"<a href='#' class='delete_partnertype'>Delete</a>";
                html.Append(string.Format(@"
<tr class='row'>
    <td class='name'>{0}</td>
    <td class='id'>{1}</td>
    <td>{2}</td>
</tr>"
                    , localPartnerType.Name, localPartnerType.CatgegoryId, deleteLink));
            }
            return html.ToString();
        }

        #endregion
    }
}