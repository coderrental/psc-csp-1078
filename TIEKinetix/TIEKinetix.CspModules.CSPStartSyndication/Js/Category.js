﻿var campaignSwitchOpenText = '';
var campaignSwitchCloseText = '';
var wdBuyNowUrl;
var isZClipInit = 0;

function initApp() {
	initCampaignSwitch();
	initPickYourBanner();
	initGenerateEmbedCode();
	initEmbedCodeCopier();
	initBuyNowUrl();
	initDownload();
	initCampaignDescription();
	initCampaignPreview();
	initSwitchProduct();
	
	$('.campaign-item:first').addClass('first-campaign-item');
}

//Campaign hide/show switch text
function initCampaignSwitchText(label4HideAll, label4ShowAll, campaignCount) {
	if (campaignCount == 0) {
		$('#switch_text').hide();
		$('#campaign_section').hide();
		return;
	}
	if (campaignSwitchOpenText == '' && campaignSwitchCloseText == '') {
		campaignSwitchOpenText = label4ShowAll;
		campaignSwitchCloseText = label4HideAll;
	}
	
	$('#switch_text').html(campaignSwitchOpenText);
	$('#switch_text').removeClass('icon-hide').addClass('icon-show');
	if ($('#campaign_section').is(':visible')) {
		$('#switch_text').html(campaignSwitchCloseText);
		$('#switch_text').removeClass('icon-show').addClass('icon-hide');
	}
}

//Campaign hide/show switch action
function initCampaignSwitch() {
	$('#switch_text').click(function () {
		$('#campaign_section').toggle();
		initCampaignSwitchText(campaignSwitchCloseText, campaignSwitchOpenText);
	});
}


function initBuyNowUrl() {
	//Open buy now URL window
	$('.btn-buynowurl').unbind('click').click(function () {
		var buyNowUrl = $(this).attr('rel');
		//var textBoxId = $(this).attr('rel');
		var objIds = $(this).attr('objid');
		$('#txtBuyNowURL').val(buyNowUrl);
		$('#objids').val(objIds);
		var thisOffset = $(this).offset();
		var wdBuyNowUrl = $('#wdBuyNowUrl').data('kendoWindow');
		$('#wdBuyNowUrl').closest(".k-window").css({
			top: thisOffset.top - 66,
			left: thisOffset.left - 115
		});
		wdBuyNowUrl.open();
	});

	$('#buynowurl-help-switch').unbind('click').click(function () {
		var wdBuyNowUrl = $('#wdBuyNowUrl').data('kendoWindow');
		if ($('#buynowurl-help-text').is(':visible')) {
			$('#buynowurl-help-text').hide();
			wdBuyNowUrl.wrapper.css({
				height: 100
			});
			return;
		}
		$('#buynowurl-help-text').show();
		wdBuyNowUrl.wrapper.css({
			height: 145
		});
	});

	//Submit new buy now URL
	$('#btn_submit_buynowurl').unbind('click').click(function () {
		if ($('#objids').val() == '') {
			alert('Missing content_types_fields: Buy_Now_URL');
			return;
		}

		var mixedId = $('#objids').val().split(';');
		if (mixedId.length != 2) {
			alert('Missing id');
			return;
		}

		var thisEl = $(this);
		var thisText = thisEl.find('.btn-long-text').html();
		var pleaseWaitText = thisEl.attr('rel');
		var contentId = mixedId[0];
		var contentFieldId = mixedId[1];
		var textBoxId = 'txtBuyNowURL';
		var buyNowURLValue = $('#' + textBoxId).val();
		if (!validateURL(buyNowURLValue)) {
			alert($('#' + textBoxId).attr('label'));
			return;
		}
		thisEl.unbind('click');
		thisEl.find('.btn-long-text').html(pleaseWaitText);
		$.ajax({
			type: 'POST',
			data: { 'ajaxaction': 'buynowurl', contentid: contentId, contentfieldid: contentFieldId, buynowurl: buyNowURLValue },
			dataType: 'xml',
			success: function (responseData) {
				$('.btn-buynowurl[objid="' + $('#objids').val() + '"]').attr('rel', buyNowURLValue);
				thisEl.find('.btn-long-text').html(thisText);
				initBuyNowUrl();
				var wdBuyNowUrl = $('#wdBuyNowUrl').data('kendoWindow');
				wdBuyNowUrl.close();
			},
			error: function (err) {
				initBuyNowUrl();
			}
		});
	});
}

function initDownload() {
	$('.btn-marketcollat-download').click(function () {
		var windowId = $(this).attr('rel');
		var wdDownload = $('#' + windowId).data("kendoWindow");
		wdDownload.center();
		wdDownload.open();
	});

	$('.wdDownload').each(function () {
		var objId = $(this).attr('id');
		var thisUl = $(this).find('ul');
		var showItems = 4;
		if (thisUl.find('li').length == 0) {
			$('.btn-marketcollat-download[rel="' + objId + '"]').css('visibility', 'hidden');
		}
		else if (thisUl.find('li').length > showItems) {
			thisUl.find('li').css('float', 'left !important');
			$(this).find('.asset-list').carousel({ dispItems: showItems, pagination: false });
			$(this).find('.asset-list').append('<div class="clear"></div>');
		}
	});
}

function initPickYourBanner() {
	//Init jQuery-Carousel rotator
	if ($('.banner_160_600').length > 1) {
		$('.PYB-item').carousel({ dispItems: 1, pagination: false, autoSlide: true, loop: true, autoSlideInterval: 6000, effect: 'fade' });
	}
	else {
		$('.PYB-item').carousel({ dispItems: 1, pagination: false, autoSlide: false, loop: true, autoSlideInterval: 6000, effect: 'fade' });
	}
	$('.PYB-item').append("<div class='clear'></div>");
	//Show Pick Your Banner window
	$('#btnPickBanner').click(function () {
		var wdPickYourBanner = $('#wdPickYourBanner').data("kendoWindow");
		wdPickYourBanner.center();
		wdPickYourBanner.open();
	});

	//Banner selected
	$('.banner_size').click(function () {
		var bannerFieldName = $(this).val();
		setDefaultBannerSize(bannerFieldName);
	});

	//Checkbox label clicked
	$('.banner-size-label').click(function () {
		var bannerFieldName = $(this).attr('title');
		setDefaultBannerSize(bannerFieldName);
	});

	//Banner image clicked (selected)
	$('.banner-item-wrapper img').click(function () {
		var divWrapper = $(this).closest('div');
		divWrapper.find('input[type="radio"]').click();
	});

	//button banner select clicked
	$('#btnBannerSelected').click(function () {
		var wdPickYourBanner = $('#wdPickYourBanner').data("kendoWindow");
		wdPickYourBanner.close();
	});
}

function initGenerateEmbedCode() {
	$('#btnEmbedCode').click(function () {
		var wdGenerateEmbedCode = $('#wdGenerateEmbedCode').data("kendoWindow");
		wdGenerateEmbedCode.center();
		wdGenerateEmbedCode.open();
		initEmbedCodeCopier();
	});
}

//Show campaign description popup
function initCampaignDescription() {
	$('.campaign-item-desc').hover(function() {
		$(this).find('.campaign-item-desc-full').show();
	}, function () {
		$(this).find('.campaign-item-desc-full').hide();
	});
}

function initCampaignPreview() {
	$('.btn-campaign-preview').click(function () {
		var cPreviewURL = campaignPreviewURL;
		var previewId = $(this).attr('objid');
		cPreviewURL = cPreviewURL.replace('[microsite_link_url]', previewId);
		var wdCampaignPreview = $('#wdCampaignPreview').data("kendoWindow");
		var previewIframe = $('<iframe id="preview-iframe">');
		previewIframe.attr({src: cPreviewURL, border: 0, frameborder: 0, style: 'border:0'});
		$('#wdCampaignPreview').append(previewIframe);
		//wdCampaignPreview.iframe(true);
		/*wdCampaignPreview.refresh({
		url: cPreviewURL,
		iframe: true
		});*/
		wdCampaignPreview.center();
		wdCampaignPreview.open();
	});
}

function resetPreviewIframe() {
	$('#wdCampaignPreview').find('#preview-iframe').attr('src', 'about:blank');
}

function initSwitchProduct() {
	$('.csp-cats-tile').carousel({ dispItems: 4, pagination: false });
	$('.csp-cats-tile').append("<div class='clear'></div>");
}

function initEmbedCodeCopier() {
	if (isZClipInit <= 2) {
		var swfFilePath = $('#copy-embed-code').attr('rel');
		var theOffset = $('#submit_email_embed_code'); //.parent('div');
		theOffset.after($('#copy-embed-code'));
		$('#copy-embed-code').after($('#code-copied'));
		//$('a#copy-embed-code').before(theOffset);

		$('#copy-embed-code').zclip({
			path: swfFilePath,
			copy: function () { return $('#csp-generated-code').val(); },
			beforeCopy: function () {
				$('#copy-embed-code').hide();
			},
			afterCopy: function () {
				$('#code-copied').show().delay(5000).fadeOut('fast', function () {
					$('#copy-embed-code').show();
				});
			}
		});
		isZClipInit++;
	}
}

//Set default selected banner size
function setDefaultBannerSize(bannerSizeFieldName) {
	if (bannerSizeFieldName == '')
		return;
	$('.banner_size').attr('checked', false);
	$('.banner_size[value="' + bannerSizeFieldName + '"]').attr('checked', true);

	var embedCodeSrc = generateEmbedCodeSrc;
	embedCodeSrc = $.trim(embedCodeSrc);
	embedCodeSrc = embedCodeSrc.replace('[banner_type]', bannerSizeFieldName);
	$('#csp-generated-code').val('<script type="text/javascript" src="' + embedCodeSrc + '"></script>');
	//$('#embed-code-hidden').val(embedCodeSrc);
}

//Validate URL
function validateURL(textval) {
	var urlregex = new RegExp(
        "^(http:\/\/|https:\/\/|ftp:\/\/){1}([0-9A-Za-z]+\.)");
	return urlregex.test(textval);
}

//Validate Email
function validateEmail(email) { 
    //var re = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    //return re.test(email);
    return true;
} 

function Ie7CssFixer() {
	$('#Form').css('overflow', 'hidden');
	$('#inner_banner_300_250').attr('style', 'margin-bottom: 92px !important');
	$('#inner_banner_468_60').attr('style', 'margin-bottom: 92px !important');
	$('#inner_banner_134x60').attr('style', 'margin-top: 92px !important');
	$('#inner_banner_120x60').attr('style', 'margin-top: 92px !important');
	$('#wdGenerateEmbedCode h2').css({ 'margin-top': '20px' });
}

function detectEmptySwitchProd() {
	if ($('#switch_product_list li').length == 0)
		$('#switch_product_section').hide();
}