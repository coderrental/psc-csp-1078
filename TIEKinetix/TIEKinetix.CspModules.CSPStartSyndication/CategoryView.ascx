﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CategoryView.ascx.cs" Inherits="TIEKinetix.CspModules.CSPStartSyndication.CategoryView" %>
<%@ Import Namespace="CR.ContentObjectLibrary.Data" %>
<%@ Import Namespace="CR.DnnModules.Common" %>
<%@ Import Namespace="TIEKinetix.CspModules.CSPStartSyndication.Entities" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div id="ss_category_wrapper">
     <asp:Panel ID="TopicBreadcrumbPanel" runat="server" CssClass="csp-clear csp-asset-breadcrumb"></asp:Panel>
	<%--Description Block--%>
	<div id="description_section">
		<div id="desc_text">
			<h1 id="desc_title"><%= GetCategoryTitle() %></h1>
			<div id="desc_detail">
				<p>
					<%= GetCategoryDescription() %>
				</p>
			</div>
			<div id="desc_controls">
				<a id="btnPickBanner" class="btn-long-link" href="javascript: void(0)">
					<span class="btn-long ss-btn">
						<span class="btn-long-right ss-btn">
							<span class="btn-long-mid ss-btn">
								<span class="btn-long-text ss-btn"><%= GetContentText("Content_Step_2_Text") %></span>
							</span>
						</span>
					</span>
				</a>
				<a id="btnEmbedCode" class="btn-long-link" href="javascript: void(0)">
					<span class="btn-long ss-btn">
						<span class="btn-long-right ss-btn">
							<span class="btn-long-mid ss-btn">
								<span class="btn-long-text ss-btn"><%= GetContentText("Content_Step_3_Text") %></span>
							</span>
						</span>
					</span>
				</a>
			</div>
		</div>
		<div id="desc_figure">
			<img width="255px" src="<%= GetCategoryContentImage() %>"/>
		</div>
		<div class="clear"></div>
	</div>
	
	<%--Campaign List--%>
	<div id="campaign_section_switch"><span id="switch_text"></span></div>
	<div id="campaign_section">
		<%= GetContentText("Content_Step_4_Text") %>
		<div id="campaign_list">
			<telerik:RadListView runat="server" ID="lvCategoryCampaign">
				<ItemTemplate>
					<div class="campaign-item">
						<div class="campaign-item-desc">
							<div class="campaign-item-picture">
								<table class="tb-vertical-center">
									<tr>
										<td>
											<%#GetCampaignThumbnail(Container.DataItem) %>
										</td>
									</tr>
								</table>
							</div>
							<div class="campaign-item-desc-text">
								<table class="tb-vertical-center">
									<tr>
										<td>
											<a href="javascript: void(0)" class="campaign-item-title-link" title="<%#GetCampaignTitle(Container.DataItem) %>"><%#GetCampaignTitle(Container.DataItem) %></a>
											<div class="campaign-icons">
												<img src="<%= ControlPath %>Images/buy-now-btn.png" class="<%# (String.IsNullOrEmpty(GetContentFieldValue(Container.DataItem, "Buy_Now_URL_Y_N")) || GetContentFieldValue(Container.DataItem, "Buy_Now_URL_Y_N").ToLower() != "yes") ? "invisible" : string.Empty %>" />
											</div>
											<p class="campaign-item-desc-full">
												<%#String.IsNullOrEmpty(GetContentFieldValue(Container.DataItem, "PPP_Description_Short")) ? GetLocalizedText("CategoryView.NoCampaignDescription") : GetContentFieldValue(Container.DataItem, "PPP_Description_Short")%>
											</p>
										</td>
									</tr>
								</table>
							</div>
							<div class="clear"></div>
						</div>
						<div class="campaign-item-controls">
							<table class="tb-vertical-center">
								<tr>
									<td>
										<a class="btn-black-link btn-campaign-preview" href="javascript: void(0)" title="<%#GetContentFieldValue(Container.DataItem, "Microsite_Title") %>" objid="<%#GetContentFieldValue(Container.DataItem, "Microsite_Link_Url") %>">
											<span class="btn-black ss-btn">
												<span class="btn-black-right ss-btn">
													<span class="btn-black-mid ss-btn">
														<span class="btn-black-text ss-btn"><%#GetContentText("Content_Step_1_Button_Text") %></span>
													</span>
												</span>
											</span>
										</a>
										<a class="btn-black-link btn-buynowurl <%# (String.IsNullOrEmpty(GetContentFieldValue(Container.DataItem, "Buy_Now_URL_Y_N")) || GetContentFieldValue(Container.DataItem, "Buy_Now_URL_Y_N").ToLower() != "yes") ? "invisible" : string.Empty %>" href="javascript: void(0)" title="<%#GetLocalizedText("CategoryView.btnPersonalizeHover") %>" rel="<%#GetContentFieldValue(Container.DataItem, "Buy_Now_URL") %>" objid="<%#GetCampaignIdInfo(Container.DataItem) %>">
											<span class="btn-black ss-btn">
												<span class="btn-black-right ss-btn">
													<span class="btn-black-mid ss-btn">
														<span class="btn-black-text ss-btn"><%#GetLocalizedText("CategoryView.btnPersonalize") %></span>
													</span>
												</span>
											</span>
										</a>
										<a class="btn-black-link btn-marketcollat-download" href="javascript: void(0)" title="<%#GetLocalizedText("CategoryView.btnDownload").Trim() + " " + GetLocalizedText("CategoryView.btnMarketingCollateral").Trim() %>" rel="wdof-<%#((content)Container.DataItem).content_Id %>">
											<span class="btn-black ss-btn">
												<span class="btn-black-right ss-btn">
													<span class="btn-black-mid ss-btn">
														<span class="btn-black-text ss-btn">
															<div class="btn-black-inner-text">
																<%#GetLocalizedText("CategoryView.btnDownload").Trim() %><br />
																<small><%#GetLocalizedText("CategoryView.btnMarketingCollateral").Trim() %></small>
															</div>
														</span>
													</span>
												</span>
											</span>
										</a>
									</td>
								</tr>
							</table>
						</div>
						<div class="clear"></div>
					</div>
					<%--Download Window--%>
					<div class="wdDownload" id="wdof-<%#((content)Container.DataItem).content_Id %>" title="<%#GetLocalizedText("CategoryView.WindowDownloadTitle") %>">
						<div class="asset-list">
							<%#GetDownloadList(((content)Container.DataItem).content_Id) %>
						</div>
					</div>
				</ItemTemplate>
			</telerik:RadListView>
		</div>
	</div>
	
	<%--Switch Product--%>
	<div runat="server" ID="switch_product_section_wrapper">
		<div id="switch_product_section">
			<div id="switch_product_title">
				SWITCH PRODUCT
			</div>
			<div id="switch_product_list">
				<telerik:RadListView runat="server" ID="listSubCatsTile" ItemPlaceholderID="CategoriesPlaceHolder">
					<LayoutTemplate>
						<asp:Panel runat="server" ID="pnSubCatsTile">
							<div class="csp-cats-tile">
								<ul>
								<asp:PlaceHolder ID="CategoriesPlaceHolder" runat="server"></asp:PlaceHolder>
								</ul>
							</div>
						</asp:Panel>            
					</LayoutTemplate>
					<ItemTemplate>
						<li>
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td class="csp_cats_tile_img"><a href='<%# Eval("CategoryUrl") %>'><img width="100" height="100" src='<%# Eval("TopicImage") %>'/></a></td>
								</tr>
								<tr>
									<td class="csp_cats_tile_title"><a href='<%# Eval("CategoryUrl") %>'><%# Eval("CategoryName")%></a></td>
								</tr>
							</table>
						</li>
					</ItemTemplate>
					<EmptyDataTemplate>            
						<div class="csp-no-sub-tile"><%=GetLocalizedText("Label.NoAvilableSubCatsTile")%></div>
					</EmptyDataTemplate>
				</telerik:RadListView>
			</div>
		</div>
	</div>
</div>
<%--ALL POPUP WINDOWS--%>

<%--Buy Now URL Window--%>
<div id="wdBuyNowUrl" title="<%= GetLocalizedText("CategoryView.WindowTitleBuyNowUrl") %>">
	<div id="buynowurl-text" class="center-align">
		<input type="text" ID="txtBuyNowURL" label="<%= GetLocalizedText("CategoryView.InvalidURL") %>">
		<input type="hidden" id="objids"/>
	</div>
	<div id="buynowurl-help-switch"><%= GetLocalizedText("CategoryView.BuyNowURLHelp") %></div>
	<div id="buynowurl-help-text"><%= GetLocalizedText("CategoryView.BuyNowURLDescription") %></div>
	<div class="center-align">
		<a id="btn_submit_buynowurl" class="btn-long-link" rel="<%= GetLocalizedText("CategoryView.PleaseWait") %>" href="javascript: void(0)" label="<%= GetLocalizedText("CategoryView.SaveBuyNowURLSucceed") %>">
			<span class="btn-long ss-btn">
				<span class="btn-long-right ss-btn">
					<span class="btn-long-mid ss-btn">
						<span class="btn-long-text ss-btn"><%= GetLocalizedText("CategoryView.btnSubmit") %></span>
					</span>
				</span>
			</span>
		</a>
	</div>
</div>

<%--Campaign Description Window--%>
<div id="wdCampaignDescription">
	<div id="wdCampaignDescription-text">
		
	</div>
</div>

<%--Pick Your Banner Window--%>
<div id="wdPickYourBanner">
	<div id="PYB_list">
		<telerik:RadListView runat="server" ID="BannerRotater" ItemPlaceholderID="BannerPlaceHolder">
			<LayoutTemplate>
				<asp:Panel runat="server" ID="AssetsPanel">
					<div class="PYB-item">
						<ul>
						<asp:PlaceHolder ID="BannerPlaceHolder" runat="server"></asp:PlaceHolder>
						</ul>
					</div>
				</asp:Panel>            
			</LayoutTemplate>
			<ItemTemplate>
				<li>
					<div class="banner_160_600 banner-item-wrapper <%#String.IsNullOrEmpty(((BannerEntity)Container.DataItem).Wide_Skyscraper_160x600_Image) ? "invisible" : String.Empty %>">
						<img src="<%#((BannerEntity)Container.DataItem).Wide_Skyscraper_160x600_Image %>"/>
						<div class="left-align relative-pos">
							<input type="radio" class="banner_size bsize160_600" value="<%#BannerEntity.Wide_Skyscraper_160x600_Image_fieldname %>"/><label class="banner-size-label absolute-pos" title="<%#BannerEntity.Wide_Skyscraper_160x600_Image_fieldname %>">160x600</label>
						</div>
					</div>
					<div class="banner_468_60">
						<div class="inner_banner_300_250 banner-item-wrapper <%#String.IsNullOrEmpty(((BannerEntity)Container.DataItem).Medium_Rectangle_300x250_Image) ? "invisible" : String.Empty %>">
							<img src="<%#((BannerEntity)Container.DataItem).Medium_Rectangle_300x250_Image %>"/>
							<div class="left-align relative-pos">
								<input type="radio" class="banner_size bsize300_250" value="<%#BannerEntity.Medium_Rectangle_300x250_Image_fieldname %>"/><label class="banner-size-label absolute-pos" title="<%#BannerEntity.Medium_Rectangle_300x250_Image_fieldname %>">300x250</label>
							</div>
						</div>
						<div class="inner_banner_468_60 banner-item-wrapper <%#String.IsNullOrEmpty(((BannerEntity)Container.DataItem).Full_Banner_468x60_Image) ? "invisible" : String.Empty %>">
							<img src="<%#((BannerEntity)Container.DataItem).Full_Banner_468x60_Image %>"/>
							<div class="left-align relative-pos">
								<input type="radio" class="banner_size bsize468_60" value="<%#BannerEntity.Full_Banner_468x60_Image_fieldname %>"/><label class="banner-size-label absolute-pos" title="<%#BannerEntity.Full_Banner_468x60_Image_fieldname %>">468x60</label>
							</div>
						</div>
						<div class="inner_banner_234_60 banner-item-wrapper <%#String.IsNullOrEmpty(((BannerEntity)Container.DataItem).Half_Banner_234x60_Image) ? "invisible" : String.Empty %>">
							<img src="<%#((BannerEntity)Container.DataItem).Half_Banner_234x60_Image %>"/>
							<div class="left-align relative-pos">
								<input type="radio" class="banner_size bsize234_60" value="<%#BannerEntity.Half_Banner_234x60_Image_fieldname %>"/><label class="banner-size-label absolute-pos" title="<%#BannerEntity.Half_Banner_234x60_Image_fieldname %>">234x60</label>
							</div>
						</div>
					</div>
					<div class="banner_180_90">
						<div class="inner_banner_180_90 banner-item-wrapper <%#String.IsNullOrEmpty(((BannerEntity)Container.DataItem).Button_180x90_Image) ? "invisible" : String.Empty %>">
							<img src="<%#((BannerEntity)Container.DataItem).Button_180x90_Image %>"/>
							<div class="left-align relative-pos">
								<input type="radio" class="banner_size bsize180_90" value="<%#BannerEntity.Button_180x90_Image_fieldname %>"/><label class="banner-size-label absolute-pos" title="<%#BannerEntity.Button_180x90_Image_fieldname %>">180x90</label>
							</div>
						</div>
						<div class="inner_banner_134x60 banner-item-wrapper <%#String.IsNullOrEmpty(((BannerEntity)Container.DataItem).Banner_Thumbnail_Image) ? "invisible" : String.Empty %>">
							<img src="<%#((BannerEntity)Container.DataItem).Banner_Thumbnail_Image %>"/>
							<div class="left-align relative-pos">
								<input type="radio" class="banner_size bsize143_60" value="<%#BannerEntity.Banner_Thumbnail_Image_fieldname %>"/><label class="banner-size-label absolute-pos" title="<%#BannerEntity.Banner_Thumbnail_Image_fieldname %>">143x60</label>
							</div>
						</div>
						<div class="inner_banner_120x60 banner-item-wrapper <%#String.IsNullOrEmpty(((BannerEntity)Container.DataItem).Button_120x60_Image) ? "invisible" : String.Empty %>">
							<img width="120px" height="60px" src="<%#((BannerEntity)Container.DataItem).Button_120x60_Image %>"/>
							<div class="left-align relative-pos">
								<input type="radio" class="banner_size bsize120_60" value="<%#BannerEntity.Button_120x60_Image_fieldname %>"/><label class="banner-size-label absolute-pos" title="<%#BannerEntity.Button_120x60_Image_fieldname %>">120x60</label>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</li>
			</ItemTemplate>
		</telerik:RadListView>
	</div>
	<div class="center-align" style="padding-top: 8px; border-top: 1px solid #DADADA;">
		<a id="btnBannerSelected" class="btn-long-link" href="javascript: void(0)">
			<span class="btn-long ss-btn">
				<span class="btn-long-right ss-btn">
					<span class="btn-long-mid ss-btn">
						<span class="btn-long-text ss-btn"><%= GetLocalizedText("CategoryView.btnSelect") %></span>
					</span>
				</span>
			</span>
		</a>
		<div id="generate_embed_src" class="hide">
			<%= GetGenerateEmbedSource() %>
		</div>
	</div>
</div>

<%--Generate Embed Code Window--%>
<div id="wdGenerateEmbedCode" title="<%= GetLocalizedText("CategoryView.WindowGenerateEmbedCodeTitle") %>">
	<div id="wdGenerateEmbedCode-text">
		<%= GetContentText("CSP_Embed_Campaign_Text") %>
		<input type="submit" id="copy-embed-code" href="javascript: void(0)" rel="<%= ControlPath %>Js/ZeroClipboard.swf" value="<%= GetLocalizedText("CategoryView.btnCopyCode")%>" />
		<span id="code-copied"><%= GetLocalizedText("CategoryView.CodeCopied") %></span>
		<div id="generate_embed_code_email">
			<input type="text" id="email_embed_code" value="<%= UserInfo.Email %>"/><input type="submit" id="submit_email_embed_code" value="<%= GetContentText("CSP_SendScript_Email_Button_Text") %>" label="<%= GetLocalizedText("CategoryView.InvalidEmailAddress") %>" rel="<%= GetLocalizedText("CategoryView.EmailSent") %>"/>
		</div>
		<p id="email_embed_code_result"></p>
		<p class="hide" id="email_embed_code_succeed"><%= GetContentText("CSP_SendSuccess_Email_Text") %></p>
		<p class="hide" id="email_embed_code_failed"><%= GetContentText("CSP_SendFail_Email_Text") %></p>
	</div>
</div>

<%--Campaign Preview Window--%>
<div id="wdCampaignPreview" title="<%= GetLocalizedText("CategoryView.WindowCampaignPreviewTitle") %>">
</div>

<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
<script type="text/javascript">
	var generateEmbedCodeSrc = '<%= GetGenerateEmbedSource() %>';
	var campaignPreviewURL = '<%= GetCampaignPreviewURL() %>';
	
	$(function () {
		//Init all JS effect
		initApp();

        //Init Popup windows
	    initPopupWindow();
	    
		//Show/hide campaign button
		initCampaignSwitchText('<%= GetLocalizedText("CategoryView.HideAllCampaigns") %>', '<%= GetLocalizedText("CategoryView.ShowAllCampaigns") %>', <%= _campaignContent.Count %>);
		
		//Set default selected banner
		setDefaultBannerSize('<%= BannerEntity.Medium_Rectangle_300x250_Image_fieldname %>');

		initEmbedCodeEmailSubmit();

		if ($.browser.msie  && parseInt($.browser.version, 10) === 7)
			Ie7CssFixer();
	});
	
    //Init all popup windows
    function initPopupWindow() {
	    //Buy Now URL window
	    var wdBuyNowUrlTitle = $('#wdBuyNowUrl').attr('title');
	    wdBuyNowUrl = $('#wdBuyNowUrl').kendoWindow({
		    actions: ["Close"],
		    draggable: true,
		    height: "100px",
		    width: "330px",
		    modal: true,
		    resizable: false,
		    visible: false,
		    title: wdBuyNowUrlTitle
	    }).data("kendoWindow");

	    //Pick your banner window
	    var wdPickYourBannerTitle = $('#btnPickBanner').find('.btn-long-text').html();
	    var wdPickYourBanner = $('#wdPickYourBanner').kendoWindow({
		    actions: ["Close"],
		    draggable: true,
		    height: "750px",
		    width: "920px",
		    modal: true,
		    resizable: false,
		    visible: false,
		    title: wdPickYourBannerTitle
	    }).data("kendoWindow");

	    //Campaign description window
	    var wdCampaignDescription = $('#wdCampaignDescription').kendoWindow({
		    actions: ["Close"],
		    draggable: false,
		    height: "70px",
		    width: "340px",
		    modal: true,
		    resizable: false,
		    visible: false,
		    title: ''
	    }).data("kendoWindow");

	    //Generate Embed Code window
	    var titleH1 = $('#wdGenerateEmbedCode-text').find('h1');
	    titleH1.hide();
	    titleH1.find('img').remove();
	    var wdGenerateEmbedCodeTitle = titleH1.html();
	    var wdGenerateEmbedCode = $('#wdGenerateEmbedCode').kendoWindow({
		    actions: ["Close"],
		    draggable: true,
		    height: "570px",
		    width: "670px",
		    modal: true,
		    resizable: false,
		    visible: false,
		    title: wdGenerateEmbedCodeTitle
	    }).data("kendoWindow");

	    //Campaign Preview Widow
	    var wdCampaignPreviewTitle = $('#wdCampaignPreview').attr('title');
	    var wdCampaignPreview = $('#wdCampaignPreview').kendoWindow({
		    actions: ["Close"],
		    animation: { open: { effects: false }, close: { effects: false} },
		    draggable: true,
		    height: "<%= GetPreviewPopupHeight() %>",
		    width: "<%= GetPreviewPopupWidth() %>",
		    modal: true,
		    resizable: false,
		    visible: false,
		    title: wdCampaignPreviewTitle,
		    close: function () { $('#wdCampaignPreview').find('#preview-iframe').remove(); }
	    }).data("kendoWindow");

	    //Download Windows
	    $('.wdDownload').each(function () {
		    var thisWdTitle = $(this).attr('title');
		    var wdDownload = $(this).kendoWindow({
			    actions: ["Close"],
			    draggable: true,
			    height: "230px",
			    width: "750px",
			    modal: true,
			    resizable: false,
			    visible: false,
			    title: thisWdTitle
		    }).data("kendoWindow");
	    });
    }
	
	//Init Submit Embed Code for sending email
	function initEmbedCodeEmailSubmit() {
		$('#submit_email_embed_code').click(function (e) {
			e.preventDefault();
			var thisEl = $(this);
			var emailReciver = $('#email_embed_code').val();
			if (!validateEmail(emailReciver)) {
				$('#email_embed_code_result').html($('#email_embed_code_failed').html());
				return;
			}
			var loadingPanel = $find('<%= RadAjaxLoadingPanel1.ClientID %>');
			var crrUpdateControl = "wdGenerateEmbedCode";
			loadingPanel.show(crrUpdateControl);
			var embedCode = $('#csp-generated-code').val();
			$.ajax({
				type: 'POST',
				data: { 'ajaxaction': 'emailembedcode', reciever: emailReciver, embedcode: embedCode },
				dataType: 'xml',
				success: function (responseData) {
					var resultCode = $(responseData).find('resultcode').text();
					resultCode = parseInt(resultCode);
					if (resultCode == 1) {
						$('#email_embed_code_result').html($('#email_embed_code_succeed').html());
					} else {
						$('#email_embed_code_result').html($('#email_embed_code_failed').html() + '<p style="color: #FF0000">' + $(responseData).find('resultmsg').text() + '</p>');
					}
					loadingPanel.hide(crrUpdateControl);
				},
				error: function (err) {
				}
			});
		});
	}
</script>
</telerik:RadScriptBlock>