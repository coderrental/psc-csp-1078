﻿// ***********************************************************************
// Assembly         : TIEKinetix.CspModules.CSPStartSyndication
// Author           : VU DINH
// Created          : 10-09-2012
//
// Last Modified By : VU DINH
// Last Modified On : 06-06-2013
// ***********************************************************************
// <copyright file="DnnModuleBase.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using CR.DnnModules.Common;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;
using DotNetNuke.Services.Log.EventLog;
using CR.ContentObjectLibrary.Data;
using System.Linq;
using TIEKinetix.CspModules.CSPStartSyndication.Entities;

namespace TIEKinetix.CspModules.CSPStartSyndication.Common
{
    /// <summary>
    /// Class DnnModuleBase
    /// </summary>
	public class DnnModuleBase : PortalModuleBase
	{
        /// <summary>
        /// The DNN event log
        /// </summary>
		protected EventLogController DnnEventLog;
        /// <summary>
        /// My actions
        /// </summary>
		protected ModuleActionCollection MyActions;
        /// <summary>
        /// The _local resource file
        /// </summary>
		private string _localResourceFile;
        /// <summary>
        /// The CSP data context
        /// </summary>
		protected CspDataContext CspDataContext;

        /// <summary>
        /// The campaign content type id
        /// </summary>
		protected internal int CampaignContentTypeId;
        /// <summary>
        /// The category content type id
        /// </summary>
		protected internal int CategoryContentTypeId;
        /// <summary>
        /// The asset content type id
        /// </summary>
		protected internal int AssetContentTypeId;
        /// <summary>
        /// The supplier id
        /// </summary>
		protected internal int SupplierId;
        /// <summary>
        /// The reseller category id
        /// </summary>
		protected internal int ResellerCategoryId;
        /// <summary>
        /// The distributor category id
        /// </summary>
		protected internal int DistributorCategoryId;
        /// <summary>
        /// The list local audiences
        /// </summary>
	    protected internal List<LocalAudience> ListLocalAudiences;

        /// <summary>
        /// The list local partner type
        /// </summary>
        protected internal List<LocalPartnerType> ListLocalPartnerType;

        protected internal List<int> ListCategoriesIdToSkipLevel; 

        /// <summary>
        /// Handles the Init event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
		protected virtual void Page_Init(object sender, EventArgs e)
		{
			//Init Local Resource file
			_localResourceFile = Utils.GetCommonResourceFile("CSPStartSyndication", LocalResourceFile);
			//Init module title
			ModuleConfiguration.ModuleTitle = GetLocalizedText("ModuleTitle");

			CspDataContext = new CspDataContext(Utils.GetConnectionString(PortalId));

			// get campaign content type id, category content type id and supplier id from settings, if setting have not set yet, then use default
			if (Settings[Cons.SETTING_CAMPAIGN_CONTENT_TYPE_ID] == null || !int.TryParse(Settings[Cons.SETTING_CAMPAIGN_CONTENT_TYPE_ID].ToString(), out CampaignContentTypeId))
				CampaignContentTypeId = Cons.CONTENT_TYPE_CAMPAIGN_DEFAULTID;
			if (Settings[Cons.SETTING_CATEGORY_CONTENT_TYPE_ID] == null || !int.TryParse(Settings[Cons.SETTING_CATEGORY_CONTENT_TYPE_ID].ToString(), out CategoryContentTypeId))
				CategoryContentTypeId = Cons.CONTENT_TYPE_CATEGORY_DEFAULTID;
			if (Settings[Cons.SETTING_ASSET_CONTENT_TYPE_ID] == null || !int.TryParse(Settings[Cons.SETTING_ASSET_CONTENT_TYPE_ID].ToString(), out AssetContentTypeId))
				AssetContentTypeId = Cons.CONTENT_TYPE_ASSET_DEFAULTID;
			if (Settings[Cons.SETTING_SUPPLIER_ID] == null || !int.TryParse(Settings[Cons.SETTING_SUPPLIER_ID].ToString(), out SupplierId))
				SupplierId = Cons.SUPPLIER_DEFAULTID;


            GetListLocalAudiences();
            GetListLocalPartnerType();
            GetListCategoriesIdSkipLevel();

		    InitStoreProc();
		}

        /// <summary>
        /// Gets the list categories id skip level.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        private void GetListCategoriesIdSkipLevel()
        {
            ListCategoriesIdToSkipLevel = new List<int>();
            if (Settings[Cons.SETTING_CATEGORIES_SKIP_LEVEL] != null &&Settings[Cons.SETTING_CATEGORIES_SKIP_LEVEL].ToString() != string.Empty)
            {
                ListCategoriesIdToSkipLevel.AddRange(Settings[Cons.SETTING_CATEGORIES_SKIP_LEVEL].ToString().Split(new []{","},StringSplitOptions.RemoveEmptyEntries).ToList().Select(int.Parse));
            }
        }

        /// <summary>
        /// Gets the type of the list local partner.
        /// </summary>
        private void GetListLocalPartnerType()
        {
            ListLocalPartnerType = new List<LocalPartnerType>();
            if (Settings[Cons.SETTING_LIST_LOCAL_PARTNER_TYPE] != null)
            {
                var listPartnerTypeArr = Settings[Cons.SETTING_LIST_LOCAL_PARTNER_TYPE].ToString().Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                if (listPartnerTypeArr.Any())
                {
                    foreach (var item in listPartnerTypeArr)
                    {
                        var arr = item.Split(new[] { "-" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        ListLocalPartnerType.Add(new LocalPartnerType
                        {
                            Name = arr[0],
                            CatgegoryId = int.Parse(arr[1])
                        });
                    }
                }
            }
        }

        /// <summary>
        /// Gets the list local audiences.
        /// </summary>
        private void GetListLocalAudiences()
        {
            ListLocalAudiences = new List<LocalAudience>();
            if (Settings[Cons.SETTING_LIST_LOCAL_AUDIENCES] != null)
            {
                var listarr = Settings[Cons.SETTING_LIST_LOCAL_AUDIENCES].ToString().Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                if (listarr.Any())
                {
                    foreach (var item in listarr)
                    {
                        ListLocalAudiences.Add(new LocalAudience
                        {
                            AudienceName = item,
                            Id = listarr.IndexOf(item)+1
                        });
                    }
                }
            }
            if (!ListLocalAudiences.Any())
            {
                ListLocalAudiences.Add(new LocalAudience
                    {
                        AudienceName = GetLocalizedText("Label.AudienceConsumer"),
                        Id = Cons.AUDIENCE_CONSUMER_ID
                    });
                ListLocalAudiences.Add(new LocalAudience
                {
                    AudienceName = GetLocalizedText("Label.AudienceBusiness"),
                    Id = Cons.AUDIENCE_BUSINESS_ID
                });
            }
        }

        /// <summary>
        /// Inits the store proc.
        /// </summary>
        /// Author: Vu Dinh
        /// 11/15/2012 - 2:47 PM
        private void InitStoreProc()
        {
            string query = "SELECT * FROM sys.objects WHERE type = 'p' AND name = 'GetContentFieldValue'";
            int procCount = CspDataContext.ExecuteQuery<object>(query).Count();
           //Create the stored proc
           if (procCount <= 0)
           {
            try
            {
                CspDataContext.ExecuteCommand(@"
CREATE PROCEDURE dbo.GetContentFieldValue
	(
	@contentid uniqueidentifier,
	@field nvarchar(MAX)
	)
AS
SET NOCOUNT ON;
    select cf.value_text from content_fields cf
    join content_types_fields ctf on ctf.content_types_fields_Id = cf.content_types_fields_Id
    join content ct on ct.content_Id = cf.content_Id
    where ctf.fieldname = @field and ct.content_Id = @contentid
	RETURN;");
            }
            catch (Exception exc)
            {
     
            }
           }
        }

        /// <summary>
        /// Gets the contents.
        /// </summary>
        /// <param name="langId">The lang id.</param>
        /// <param name="contentTypeId">The content type id.</param>
        /// <param name="suppId">The supp id.</param>
        /// <param name="catId">The cat id.</param>
        /// <returns>List{content}.</returns>
        /// Author: Vu Dinh
        /// 11/15/2012 - 7:14 PM
        public List<content> GetContentsFromCategoryId(int langId, int contentTypeId, int suppId, int catId)
        {
            var query =
                @"
  select * from content c
  join content_main cm on cm.content_main_Id = c.content_main_Id
  join content_types ct on ct.content_types_Id = cm.content_types_Id
  join content_categories cc on cc.content_main_Id = cm.content_main_Id
  join categories cat on cat.categoryId = cc.category_Id
  where c.content_types_languages_Id = {0} and ct.content_types_Id = {1} and cat.categoryId = {2} and cm.supplier_Id = {3}";
            var result = CspDataContext.ExecuteQuery<content>(query, langId, contentTypeId, catId, suppId);
            return result.ToList();
        }

        /// <summary>
        /// Get localized text for the current portal language
        /// </summary>
        /// <param name="key">Resource Key</param>
        /// <returns>Localized text</returns>
		public string GetLocalizedText(string key)
		{
			return Localization.GetString(key, _localResourceFile);
		}

        /// <summary>
        /// Write message to Dnn event view
        /// </summary>
        /// <param name="message">message to write</param>
		public void Log(string message)
        {
	        var dnnEventLog = new DotNetNuke.Services.Log.EventLog.EventLogController();
			dnnEventLog.AddLog("DEBUG", message, PortalSettings, UserId, EventLogController.EventLogType.ADMIN_ALERT);
		}
	}
}