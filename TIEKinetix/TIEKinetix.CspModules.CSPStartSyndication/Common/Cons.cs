﻿namespace TIEKinetix.CspModules.CSPStartSyndication.Common
{
	public class Cons
	{
		public const int CONTENT_TYPE_CAMPAIGN_DEFAULTID = 1;
		public const int CONTENT_TYPE_CATEGORY_DEFAULTID = 2;
		public const int CONTENT_TYPE_ASSET_DEFAULTID = 8;
		public const int SUPPLIER_DEFAULTID = 12;
		public const int CAMPAIGN_DESC_LENGTH_DEFAULT = 50;
		public const int PUBLISH_STAGE_ID = 50;
		public const int PPPGENERAL_CONTENT_TYPE_ID = 5;

		public const int AUDIENCE_CONSUMER_ID = 1;
		public const int AUDIENCE_BUSINESS_ID = 2;

	    public const int DEFAULT_PREVIEWPOPUP_WIDTH = 550;
	    public const int DEFAULT_PREVIEWPOPUP_HEIGHT = 850;

		public const string SETTING_CAMPAIGN_CONTENT_TYPE_ID = "CampaignContentTypeId";
		public const string SETTING_CATEGORY_CONTENT_TYPE_ID = "CategoryContentTypeId";
		public const string SETTING_SUPPLIER_ID = "SupplierId";
		public const string SETTING_GENERATE_EMBED_CODE_SRC = "GenerateEmbedCodeSrc";
		public const string SETTING_CAMPAIGN_PREVIEW_URL = "CampaignPreviewURL";
		public const string SETTING_CAMPAIGN_DESCRIPTION_LENGTH = "CampaignDescriptionLength";
		public const string SETTING_ASSET_CONTENT_TYPE_ID = "AssetContentTypeId";
		public const string SETTING_PPPGENERAL_CONTENT_TYPE_ID = "PPPGeneralContentTypeId";
	    public const string SETTING_SYNC_LANGUAGE = "SyncLanguage";
		public const string SETTING_PORTAL_INSTANCE_NAME = "PortalInstance";
	    public const string SETTING_DISABLE_NAVPANEL = "DisableNavPanel";
	    public const string SETTING_PREVIEW_POPUP_WIDTH = "PreviewPopupWidth";
	    public const string SETTING_PREVIEW_POPUP_HEIGHT = "PreviewPopupHeight";
	    public const string SETTING_LIST_LOCAL_AUDIENCES = "ListLocalAudiences";
	    public const string SETTING_LIST_LOCAL_PARTNER_TYPE = "ListLocalPartnerType";
	    public const string SETTING_CATEGORIES_SKIP_LEVEL = "CategoriesSkipLevel";

		public const string COMPANY_PARAMETER_AUDIENCE_NAME = "Local_Audience";
		public const string CAMPAIGNS_LOCAL_AUDIENCE_TEXT = "Campaigns_Local_Audience";
		public const string AUDIENCE_BUSINESS_TEXT = "Business";
		public const string AUDIENCE_CONSUMER_TEXT = "Consumer";
	    public const string COMPANY_SETTINGS_TAB_NAME = "Company_Settings_Tab_Name";

		public const string LOCAL_PARTNER_TYPE_TEXT = "Local_Partner_Type";
		public const string LOCAL_PARTNER_RESELLER_TEXT = "reseller";
		public const string LOCAL_PARTNER_DISTRIBUTOR_TEXT = "distributor";
		public const string CAMPAIGN_PREVIEW_URL_DEFAULT = "http://12.urgent.contentcastsyndication.com";
		public const string GENERATE_EMBED_CODE_SRC_DEFAULT = "http://12.urgent.contentcastsyndication.com";
	}
}