﻿// ***********************************************************************
// Assembly         : TIEKinetix.CspModules.CSPStartSyndication
// Author           : VU DINH
// Created          : 06-06-2013
//
// Last Modified By : VU DINH
// Last Modified On : 06-06-2013
// ***********************************************************************
// <copyright file="MainView.ascx.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using CR.ContentObjectLibrary;
using CR.ContentObjectLibrary.Data;
using CR.ContentObjectLibrary.Factory;
using CR.ContentObjectLibrary.Interface;
using CR.DnnModules.Common;
using CR.DnnModules.Common.CategoryTree;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Data;
using TIEKinetix.CspModules.CSPStartSyndication.Common;
using TIEKinetix.CspModules.CSPStartSyndication.Entities;
using Telerik.Web.UI;

namespace TIEKinetix.CspModules.CSPStartSyndication
{
    /// <summary>
    /// Class MainView
    /// </summary>
	public partial class MainView : DnnModuleBase
	{
        /// <summary>
        /// The _audience id
        /// </summary>
		private int _audienceId;
        /// <summary>
        /// The _category blocks
        /// </summary>
		private List<CategoryBlock> _categoryBlocks;
        /// <summary>
        /// The _category id
        /// </summary>
		private int _categoryId;
        /// <summary>
        /// The _CSP id
        /// </summary>
		private int _cspId;
        /// <summary>
        /// The _custom categories
        /// </summary>
		private List<CustomCategory> _customCategories;
        /// <summary>
        /// The _language id
        /// </summary>
		private int _languageId;
        /// <summary>
        /// The _category content mapping
        /// </summary>
		private Dictionary<int, string> _categoryContentMapping;
        /// <summary>
        /// The _companies parameter
        /// </summary>
	    private companies_parameter _companiesParameter;
        /// <summary>
        /// The _is sync language
        /// </summary>
	    private bool _isSyncLanguage, _isDisableNavPanel;

        private List<category> _listChildCategories; 

        /// <summary>
        /// Handles the Init event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
		protected override void Page_Init(object sender, EventArgs e)
		{
            base.Page_Init(sender, e);

			_cspId = Utils.GetIntegrationKey(UserInfo);
			if (_cspId == -1 && !HaveAdminPermission())
			{
				GotoRedirectTabId(String.Empty);
			}

			if (!HaveAdminPermission())
			{
                if (CspDataContext.companies.SingleOrDefault(a => a.companies_Id == _cspId) == null) {
                    GotoRedirectTabId(String.Empty);
                }
				var dnnContext = new DnnDataContext(Config.GetConnectionString());
				var missingFormId = Utils.MissingMandatorySubscription(dnnContext, CspDataContext, UserInfo, PortalId);
				if (missingFormId != Guid.Empty)
				{
					int missingStep = Utils.FormStepNo(dnnContext, CspDataContext, missingFormId, PortalId);
					GotoRedirectTabId(String.Format("step={0}", missingStep));
				}
			}

			// requested language id
			if (!int.TryParse(Request.QueryString.Get("lid"), out _languageId))
				_languageId = -1;
            
			// requested category id
			if (!int.TryParse(Request.QueryString.Get("cid"), out _categoryId))
				_categoryId = -1;

			// requested audience id
			if (!int.TryParse(Request.QueryString.Get("aid"), out _audienceId))
				_audienceId = -1;

			Page.ClientScript.RegisterClientScriptInclude("carousel", ControlPath + "Js/carousel.js");

            if (Settings[Cons.SETTING_SYNC_LANGUAGE] != null)
            {
                int sync;
                int.TryParse(Settings[Cons.SETTING_SYNC_LANGUAGE].ToString(), out sync);
                _isSyncLanguage = sync == 1;
            }
            if (Settings[Cons.SETTING_DISABLE_NAVPANEL] != null)
            {
                _isDisableNavPanel = Settings[Cons.SETTING_DISABLE_NAVPANEL].ToString() == "1";
                if (_isDisableNavPanel)
                {
                    NavPanel.CssClass = "csp_asset_filter hide";
                }
            }
			InitLanguage();
		    InitAudience();
		}

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
            if (_isSyncLanguage)
            {
                var currentLang = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                var lang = CspDataContext.languages.FirstOrDefault(a => a.BrowserLanguageCode == currentLang);
                _languageId = lang != null ? lang.languages_Id : 1;
            }
            else
            {
                _languageId = int.Parse(cbLanguage.SelectedValue);    
            }
			_audienceId = int.Parse(cbAudience.SelectedValue);
			//Localize text
			LocalizeControl();    
            
			//Init list of category block
			_categoryBlocks = new List<CategoryBlock>();
			//Init category - content mapping
			_categoryContentMapping = new Dictionary<int, string>();
			//Populate data for controls
			InitcategoriesTree();  
			InitCategoryBlocks();
            InitBreadcrumb();
		}

        /// <summary>
        /// Inits the breadcrumb.
        /// </summary>
        /// Author: Vu Dinh
        /// 11/29/2012 - 8:18 PM
        private void InitBreadcrumb()
        {
            if (_cspId != -1 && !UserInfo.IsInRole(PortalSettings.AdministratorRoleName) && (_categoryId != -1 || cbCategories.SelectedValue != "-1"))
            {
                TopicBreadcrumbPanel.Visible = true;
                var catName = _categoryContentMapping[int.Parse(cbCategories.SelectedValue)];
                TopicBreadcrumbPanel.Controls.AddAt(0, new Literal { Text = catName });   
                 var url = Globals.NavigateURL(TabId, "", string.Format("mid={0}&lid={1}&cid={2}&aid={3}", ModuleId, _languageId, -1, _audienceId));
                 TopicBreadcrumbPanel.Controls.AddAt(0, new Literal { Text = string.Format("<a href='{0}'>{1}</a> &raquo; ", url, GetLocalizedText("Category.All")) });    
                
            }
        }

        /// <summary>
        /// Inits the category block.
        /// </summary>
		private void InitCategoryBlocks()
		{
			var tree = (RadTreeView) cbCategories.Items[0].FindControl("categoryTree");
			var listCategories = new List<category>();
			if (cbCategories.SelectedValue == "-1")
			{
				//Get list categories, except root, local partner type categories
				listCategories = tree.GetAllNodes().Where(n =>n.Value != "-1" && n.Value != "1" && ListLocalPartnerType.All(a => a.CatgegoryId.ToString() != n.Value)).Select(n => CspDataContext.categories.SingleOrDefault(a => a.categoryId == int.Parse(n.Value))).Where(category => category != null).ToList();
			}
			else if (cbCategories.SelectedValue == "1")
			{
			    var temp = ListLocalPartnerType.Select(localPartnerType => tree.FindNodeByValue(localPartnerType.CatgegoryId.ToString())).ToList();
			    listCategories = tree.GetAllNodes().Where(n => temp.Contains(n.ParentNode)).Select(n => CspDataContext.categories.SingleOrDefault(a => a.categoryId == int.Parse(n.Value))).Where(cat => cat != null).ToList();
			}
			else if (ListLocalPartnerType.Any(a => a.CatgegoryId.ToString() == cbCategories.SelectedValue))
			{
				listCategories = tree.GetAllNodes().Where(n => n.ParentNode == tree.FindNodeByValue(cbCategories.SelectedValue)).Select(n => CspDataContext.categories.SingleOrDefault(a => a.categoryId == int.Parse(n.Value))).Where(cat => cat != null).ToList();
			}
			else
			{
				listCategories.Add(CspDataContext.categories.SingleOrDefault(a => a.categoryId == int.Parse(cbCategories.SelectedValue)));
			}

			foreach (category category in listCategories)
			{
				var categoryBlock = GetCategoryBlock(category, true);
				if (categoryBlock == null)
					continue;

				_categoryBlocks.Add(categoryBlock);
			}

			if (_categoryBlocks.Count == 1 && !_categoryBlocks[0].SubCategories.Any())
				//Redirect to category detail view when select an category with no sub
			{
				Response.Redirect(_categoryBlocks[0].CategoryUrl, true);
				return;
			}
            if (_isSyncLanguage) //Check if is sync language, and category block has no content when change language
            {
                if ( !_categoryBlocks.Any() && _categoryId >= 0)
                {
                    Response.Redirect(Globals.NavigateURL(TabId, "", string.Format("mid={0}&lid={1}&aid={2}&cid={3}", ModuleId, _languageId, _audienceId, "-1")), false);
                }
            }
            //Sort category block asc
		    _categoryBlocks = _categoryBlocks.OrderBy(a => a.SortOrder).ToList();
			listView.DataSource = _categoryBlocks;
		}


        /// <summary>
        /// Gets the category block.
        /// </summary>
        /// <param name="category">The category.</param>
        /// <param name="isParent">if set to <c>true</c> [is parent].</param>
        /// <returns>CategoryBlock.</returns>
        /// <remarks>ltu 3/11/13: added a flag field to determine if the module should show this category (default) or not (Hide_Category_Y_N=yes)</remarks>
		private CategoryBlock GetCategoryBlock(category category, bool isParent)
		{
			var categoryBlock = new CategoryBlock();
			var cc = GetContentsFromCategoryId(_languageId, CategoryContentTypeId, SupplierId, category.categoryId);
            content categoryContent = cc.FirstOrDefault(a => a.stage_Id == Cons.PUBLISH_STAGE_ID); //_categoryContents.SingleOrDefault(a => a.content_main.content_categories.Count(b => b.category_Id == category.categoryId) > 0 && a.content_types_languages_Id == _languageId);
            if (categoryContent != null)
            {
				if ("Yes".Equals(Utils.GetContentFieldValue(CspDataContext, categoryContent, "Hide_Category_Y_N"), StringComparison.CurrentCultureIgnoreCase))
					return null;
                string sortOrder = Utils.GetContentFieldValue(CspDataContext,categoryContent, "Status_Sort_Order"); 
                categoryBlock.SortOrder = sortOrder != string.Empty ? int.Parse(sortOrder) : 0;
                categoryBlock.MouseOverText = Utils.GetContentFieldValue(CspDataContext,categoryContent, "Content_Mouseover_Text"); 
            }
            else
			{
				categoryBlock.CategoryName = category.categoryText;
				categoryBlock.SortOrder = category.DisplayOrder.HasValue ? category.DisplayOrder.Value : 0;
			}

			//ltu 11/22/2013 check for the status dates
	        if (GetCampaignCount(category) == 0)
		        return null;

            var catName = _categoryContentMapping.ContainsKey(category.categoryId) ? _categoryContentMapping[category.categoryId] : string.Empty;
            if (catName == string.Empty)
            {
                if (categoryContent != null)
                {
                    catName = Utils.GetContentFieldValue(CspDataContext, categoryContent, "Content_Title");
                }
                else
                {
                    catName = category.categoryText;
                }
                _categoryContentMapping.Add(category.categoryId, catName);
            }
            
			categoryBlock.CategoryName = catName;
			categoryBlock.CategoryUrl = GetCategoryViewUrl(category.categoryId);
			categoryBlock.CategoryId = category.categoryId;
			categoryBlock.TopicImage = GetCategoryImage(category);
			if (isParent)
			{
				foreach (category subcat in CspDataContext.categories.Where(a => a.parentId == category.categoryId).ToList())
				{
					if (_customCategories.SingleOrDefault(a => a.Id == subcat.categoryId && a.Count > 0) != null)
					{
						//Recursive call here
						categoryBlock.SubCategories.Add(GetCategoryBlock(subcat, false));
					}
				}
			}
			return categoryBlock;
		}

	    private int GetCampaignCount(category category)
	    {
		    int r = 0;
		    using (var cmd = new SqlCommand("", (SqlConnection) CspDataContext.Connection))
		    {
			    cmd.CommandText = @"
select cm.content_identifier, c.content_types_languages_Id, cf1.value_text as startDate, cf2.value_text as endDate
from content_main cm
	join content_categories cc on cc.content_main_Id=cm.content_main_Id	
	join content c on c.content_main_Id=cm.content_main_Id
	join content_fields cf1 on cf1.content_Id=c.content_Id and cf1.content_types_fields_Id =(
		select top 1 ctf1.content_types_fields_Id from content_types_fields ctf1 where ctf1.content_types_Id=@ctId and ctf1.fieldname='Status_Launch_Date'
	)
	join content_fields cf2 on cf2.content_Id=c.content_Id and cf2.content_types_fields_Id =(
		select top 1 ctf2.content_types_fields_Id from content_types_fields ctf2 where ctf2.content_types_Id=@ctId and ctf2.fieldname='Status_End_Date'
	)
where cm.content_types_Id=@ctId and cm.supplier_Id=@sid and c.content_types_languages_Id=@lid and c.stage_Id=50 and cc.category_Id=@cid";
				cmd.Parameters.Clear();
			    cmd.Parameters.AddWithValue("@ctId", CampaignContentTypeId);
				cmd.Parameters.AddWithValue("@sid", SupplierId);
				cmd.Parameters.AddWithValue("@lid", _languageId);
			    cmd.Parameters.AddWithValue("@cid", category.categoryId);
			    try
			    {
					if (cmd.Connection.State != ConnectionState.Open)
						cmd.Connection.Open();
				    var reader = cmd.ExecuteReader();
				    while (reader.Read())
				    {
						string logMessage = string.Format("Start Syndicating: startDate: {0} endDate: {1} for category: {2} Ctid: {3}, Sid: {4}, Lid: {5}, Cid: {6} "
							, reader[2], reader[3], category.categoryId
							, CampaignContentTypeId, SupplierId, _languageId, category.categoryId);
					    DateTime launchDate = getDate(reader.GetString(2)), endDate = getDate(reader.GetString(3));
					    if (endDate.Subtract(launchDate).TotalDays>0 && 
							DateTime.Now.Subtract(launchDate).TotalDays>=0 &&
							endDate.Subtract(DateTime.Now).TotalDays>0)
					    {
							logMessage += " good:" + launchDate.ToString("MM/dd/yyyy") + " vs " + DateTime.Now.ToString("MM/dd/yyyy");
						    r++;
					    }
					    else
					    {
						    logMessage += " bad:" + launchDate.ToString("MM/dd/yyyy") + " vs " + DateTime.Now.ToString("MM/dd/yyyy");
					    }
					    Log(logMessage);
					}
					reader.Close();
					cmd.Connection.Close();
			    }
			    catch (Exception ex)
			    {
				    Log("Start Syndicating: Error " + ex.Message);
				    r = 1; // return 1 so it can continue the old routine
			    }
		    }
		    Log("GetCampaignCount for category: " + category.categoryId + " Count: " + r);
		    return r;
	    }

	    private DateTime getDate(string s)
	    {
		    DateTime value;
		    if (DateTime.TryParseExact(s, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces, out value)) 
				return value;

			if (!DateTime.TryParseExact(s, "MM-dd-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces, out value))
			    value = DateTime.MinValue;
		    return value;
	    }

	    /// <summary>
        /// Gets the category image.
        /// </summary>
        /// <param name="cat">The cat.</param>
        /// <returns>System.String.</returns>
		private string GetCategoryImage(category cat)
		{
			var searchFactory = (SimpleSearch)SearchExpressionFactory.CreateSearch();
			searchFactory.Left = SearchExpressionFactory.CreateSearch(CspDataContext.content_types.FirstOrDefault(a => a.content_types_Id == CampaignContentTypeId));
			searchFactory.Operator = SearchOperator.And;
			searchFactory.Right = SearchExpressionFactory.CreateSearch();
			searchFactory.Right.Left = SearchExpressionFactory.CreateSearch(CspDataContext.companies.FirstOrDefault(a => a.companies_Id == SupplierId));
			searchFactory.Right.Operator = SearchOperator.And;
			searchFactory.Right.Right = SearchExpressionFactory.CreateSearch(CspDataContext.languages.FirstOrDefault(a => a.languages_Id == _languageId));
			IContentAccessFilter caf = ContentAccessFilterFactory.CreateContentAccessFilter(searchFactory, CspDataContext,cat.categoryId);
            var campaigns = caf.Search().Where(a => a.stage_Id == Keys.CspPublishStageId).OrderBy(a => Utils.GetContentFieldValue(CspDataContext, a, "Status_Featured_Y_N") == "yes").OrderByDescending(a => Utils.GetContentFieldValue(CspDataContext, a, "Status_Sort_Order")).Distinct().ToList();

            //filter campaigns by audience
            campaigns = campaigns.Where(a => Utils.GetContentFieldValue(CspDataContext, a, Cons.CAMPAIGNS_LOCAL_AUDIENCE_TEXT) == ListLocalAudiences.Single(b => b.Id == _audienceId).AudienceName).ToList();

			string img = string.Empty;
            if (campaigns.Any())
            {
                foreach (var campaign in campaigns)
                {
                    img = Utils.GetContentFieldValue(CspDataContext, campaign, "Medium_Rectangle_300x250_Image");
                    if (img != string.Empty)
                        break;
                }
            }
			return img != string.Empty ? img : GetNoImageUrl();
		}

        /// <summary>
        /// Gets the category view URL.
        /// </summary>
        /// <param name="categoryId">The category id.</param>
        /// <returns>System.String.</returns>
		private string GetCategoryViewUrl(int categoryId)
		{
			return Globals.NavigateURL(TabId, "category", "mid", ModuleId.ToString(), "langid", _languageId.ToString(), "audid",_audienceId.ToString(), "cid", categoryId.ToString());
		}

        /// <summary>
        /// Gets the no image URL.
        /// </summary>
        /// <returns>System.String.</returns>
		private string GetNoImageUrl()
		{
			return ControlPath + "Images/noimage.png";
		}

        /// <summary>
        /// Localizes the control.
        /// </summary>
		private void LocalizeControl()
		{
			lbAudience.Text = GetLocalizedText("Label.Audience");
			lbCategories.Text = GetLocalizedText("Label.CategoryCampaignAsset");
			lbLanguage.Text = GetLocalizedText("Label.Language");
			lbCategoryHeadline.Text = GetLocalizedText("Label.ChooseCategoryToSyndicating");
		}

        /// <summary>
        /// Setups the language.
        /// </summary>
		private void InitLanguage()
		{
            if (_isSyncLanguage)
            {
                LanguagePanel.Visible = false;
            }
            else
            {
                //Load language
                var listLanguages = CspDataContext.languages.Where(a => a.active == true).ToList();
                var languages = from lang in listLanguages orderby lang.description select lang;
                foreach (language language in languages)
                {
                    cbLanguage.Items.Add(new RadComboBoxItem(language.description, language.languages_Id.ToString()));
                }
                if (_languageId != -1) //Set current language if have query string
                {
                    cbLanguage.Items.FindItemByValue(_languageId.ToString()).Selected = true;
                }

                else if (_cspId != -1) //otherwise set current language from consumer profile
                {
                    companies_consumer consumerProfile = CspDataContext.companies_consumers.FirstOrDefault(a => a.companies_Id == _cspId);
                    if (consumerProfile != null && consumerProfile.default_language_Id.HasValue)
                        cbLanguage.Items.FindItemByValue(consumerProfile.default_language_Id.ToString()).Selected = true;
                }
            }
			
		}

        /// <summary>
        /// Setups the audience.
        /// </summary>
		private void InitAudience()
		{
			//Load Audience
            foreach (var audience in ListLocalAudiences)
            {
                var audienceTranslation = GetLocalizedText("LocalAudience." + audience.AudienceName);
                cbAudience.Items.Add(new RadComboBoxItem(audienceTranslation, audience.Id.ToString()));
            }
			
			if (_audienceId != -1) // Set current audience if have query string
			{
				cbAudience.Items.FindItemByValue(_audienceId.ToString()).Selected = true;
			}
			else if (_cspId != -1)
			{
				companies_parameter companyParameter = CspDataContext.companies_parameters.FirstOrDefault(a => a.companies_Id == _cspId && a.companies_parameter_type.parametername == Cons.COMPANY_PARAMETER_AUDIENCE_NAME);
				if (companyParameter != null && !string.IsNullOrEmpty(companyParameter.value_text))
				{
                    var audience = ListLocalAudiences.SingleOrDefault(a => a.AudienceName == companyParameter.value_text);
                    if (audience != null)
                    {
                        var item = cbAudience.FindItemByValue(audience.Id.ToString());
                        if (item != null)
                            item.Selected = true;
                    }
				}
			}
		}


        /// <summary>
        /// Setupcategorieses the tree.
        /// </summary>
		private void InitcategoriesTree()
		{

            var searchFactory = (SimpleSearch)SearchExpressionFactory.CreateSearch();
            searchFactory.Left = SearchExpressionFactory.CreateSearch(CspDataContext.content_types.FirstOrDefault(a => a.content_types_Id == CampaignContentTypeId));
            searchFactory.Operator = SearchOperator.And;
            searchFactory.Right = SearchExpressionFactory.CreateSearch();
            searchFactory.Right.Left = SearchExpressionFactory.CreateSearch(CspDataContext.companies.FirstOrDefault(a => a.companies_Id == SupplierId));
            searchFactory.Right.Operator = SearchOperator.And;
            searchFactory.Right.Right = SearchExpressionFactory.CreateSearch(CspDataContext.languages.FirstOrDefault(a => a.languages_Id == _languageId));
            IContentAccessFilter caf = ContentAccessFilterFactory.CreateContentAccessFilter(searchFactory, CspDataContext);
		    var all = caf.Search();
            _companiesParameter = CspDataContext.companies_parameters.FirstOrDefault(a => a.companies_Id == _cspId && a.companies_parameter_type.parametername == Cons.LOCAL_PARTNER_TYPE_TEXT);
			_customCategories = new List<CustomCategory>();
			//Load category
			cbCategories.EmptyMessage = GetLocalizedText("Category.All");

			List<category> listCategories = CspDataContext.categories.Where(a => a.active == true).ToList();

            _listChildCategories = new List<category>();
            if (_companiesParameter != null && !string.IsNullOrEmpty(_companiesParameter.value_text))
            {
                var currentLocalPartnerType = ListLocalPartnerType.SingleOrDefault(a => a.Name == _companiesParameter.value_text);
                if (currentLocalPartnerType != null)
                {
                    var catx = listCategories.SingleOrDefault(a => a.categoryId == currentLocalPartnerType.CatgegoryId);
                    if (catx != null)
                    {
                        GetListChildCategories(catx.categoryId, true);
                    }
                }

            }

            foreach (category c in listCategories)
            {
                //skip level categories - get from module settings
               if (ListCategoriesIdToSkipLevel.Any(a => a == c.categoryId))
                   continue;

                if (_listChildCategories.Count>0 && _listChildCategories.All(a => a.categoryId != c.categoryId) && _listChildCategories.First().depth < c.depth)
                {
                    continue;
                }
                var customCategory = new CustomCategory(c.categoryText, c.categoryId, c.parentId.HasValue ? c.parentId.Value : 0, c.depth.HasValue ? c.depth.Value : 0, c.lineage);

                List<content> campaign;
                var curentAudience = ListLocalAudiences.SingleOrDefault(a => a.Id == _audienceId);
                if (curentAudience != null)
                {
                    campaign = all.Where(a => a.content_main.content_categories.Count(b => b.category_Id == c.categoryId) > 0 && Utils.GetContentFieldValue(CspDataContext, a, Cons.CAMPAIGNS_LOCAL_AUDIENCE_TEXT) == curentAudience.AudienceName && a.stage_Id == Keys.CspPublishStageId).ToList();
                }
                else
                {
                    campaign = all.Where(a => a.content_main.content_categories.Count(b => b.category_Id == c.categoryId) > 0 && a.stage_Id == Keys.CspPublishStageId).ToList();
                }
  
                customCategory.Count = campaign.Count;
                // update parent category campaign counts
                if (customCategory.Count > 0)
                {
                    CustomCategory temp = _customCategories.FirstOrDefault(a => a.Id == customCategory.ParentId);
                    while (temp != null && temp.Id != 0)
                    {
                        temp.Count += customCategory.Count;
                        temp = _customCategories.FirstOrDefault(a => a.Id == temp.ParentId);
                    }
                }

                _customCategories.Add(customCategory);
            }

			//Render tree categories
			RenderTree();
		}

        /// <summary>
        /// Gets the list child categories.
        /// </summary>
        /// <param name="parentCategoryId">The parent category id.</param>
        /// <param name="first">if set to <c>true</c> [first].</param>
        private void GetListChildCategories(int parentCategoryId, bool first)
        {
            if (first)
                _listChildCategories.Add(CspDataContext.categories.Single(a => a.categoryId == parentCategoryId));
            var list = CspDataContext.categories.Where(a => a.parentId == parentCategoryId && a.active == true);
            _listChildCategories.AddRange(list);
            foreach (var category in list)
            {
                GetListChildCategories(category.categoryId, false);
            }
        }

        /// <summary>
        /// Renders the tree.
        /// </summary>
	    private void RenderTree()
		{
			var tree = (RadTreeView) cbCategories.Items[0].FindControl("categoryTree");

			int selectedCategory;
			if (!int.TryParse(tree.SelectedValue, out selectedCategory))
			{
				if (_categoryId > 0)
					selectedCategory = _categoryId;
				else
					selectedCategory = -1;
			}
			//Render tree
			tree.Nodes.Clear();
			CategoryTreeView.RenderCategoryTree(tree, _customCategories);

            //filter display node with local partner type
            if (_companiesParameter != null && !string.IsNullOrEmpty(_companiesParameter.value_text))
            {
                foreach (var localPartnerType in ListLocalPartnerType)
                {
                    if (_companiesParameter.value_text == localPartnerType.Name)
                    {
                        RadTreeNode node = tree.FindNodeByValue(localPartnerType.CatgegoryId.ToString());
                        if (node != null)
                        {
                            //remove all node at same level
                            var listNodeToRemove = tree.GetAllNodes().Where(item => item.Value != node.Value && item.Level == node.Level).ToList();
                            foreach (var item in listNodeToRemove)
                            {
                                item.Remove();
                            }
                        }
                    }
                }
            }

			
			//Do not display sub category
			foreach (CustomCategory customCategory in _customCategories)
			{
				if (customCategory.Depth > 2)
				{
					RadTreeNode node = tree.FindNodeByValue(customCategory.Id.ToString());
					if (node != null)
						node.Remove();
				}
			}

			//Mapping category - category content
			foreach (var node in tree.GetAllNodes())
			{
                
				if (int.Parse(node.Value) > 0)
				{
					var catId = int.Parse(node.Value);
                    var categoryContentName = _categoryContentMapping.ContainsKey(catId) ? _categoryContentMapping[catId] : string.Empty;
                    if (categoryContentName == string.Empty)
                    {
                        var categoryContent = GetContentsFromCategoryId(_languageId, CategoryContentTypeId, SupplierId, catId).FirstOrDefault(a=> a.stage_Id == Cons.PUBLISH_STAGE_ID); 
                        if (categoryContent != null)
                        {
                            categoryContentName = Utils.GetContentFieldValue(CspDataContext, categoryContent, "Content_Title");
                        }
                        else
                        {
                            categoryContentName = node.Text;
                        }
                        _categoryContentMapping.Add(catId, categoryContentName);
                    }
					node.Text = categoryContentName;
				}
			}

            RadTreeNode selectedNode = tree.FindNodeByValue(selectedCategory.ToString());
            if (selectedNode != null)
            {
                selectedNode.Selected = true;
                cbCategories.SelectedValue = selectedNode.Value;
            }
            if (cbCategories.SelectedValue != "-1" && selectedNode!= null)
            {
                if (_categoryContentMapping.ContainsKey(int.Parse(selectedNode.Value)))
                {
                    if (cbCategories.SelectedItem != null)
                        cbCategories.SelectedItem.Text = _categoryContentMapping[int.Parse(selectedNode.Value)];
                    else
                    {
                        cbCategories.Text = _categoryContentMapping[int.Parse(selectedNode.Value)];
                    }
                }
                else
                {
                    cbCategories.Text = selectedNode.Text;
                }
                    
            }
		    
		}

        /// <summary>
        /// Sorts the category block.
        /// </summary>
        /// <param name="categoryBlocksToSort">The category blocks to sort by order.</param>
        /// <returns>List{CategoryBlock}.</returns>
		private List<CategoryBlock> SortCategoryBlock(IEnumerable<CategoryBlock> categoryBlocksToSort)
		{
			List<CategoryBlock> categoryBlock = (from element in categoryBlocksToSort
			                                     orderby element.SortOrder
			                                     select element).ToList();
			return categoryBlock;
		}

        /// <summary>
        /// Haves the admin permission.
        /// </summary>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
		public bool HaveAdminPermission()
		{
			if (UserInfo.IsSuperUser || UserInfo.IsInRole(PortalSettings.AdministratorRoleName))
				return true;
			return false;
		}

        /// <summary>
        /// Gotoes the redirect tab id.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// Author: ducuytran
        /// 1/9/2013 - 7:01 PM
		private void GotoRedirectTabId(string parameters)
		{
			if (Settings[Cons.COMPANY_SETTINGS_TAB_NAME] != null && Settings[Cons.COMPANY_SETTINGS_TAB_NAME].ToString() != string.Empty)
			{
				var dataProvider = DataProvider.Instance();
                var result = dataProvider.ExecuteSQL(string.Format("SELECT TabID FROM Tabs WHERE TabName = '{0}' and PortalID = '{1}'", Settings[Cons.COMPANY_SETTINGS_TAB_NAME], PortalId));
				int tabId = 0;
				while (result.Read())
				{
					tabId = int.Parse(result["TabId"].ToString());
					break;
				}
				if (tabId != 0)
				{
					string theUrl = Globals.NavigateURL(tabId);
					if (!String.IsNullOrEmpty(parameters))
						theUrl = Globals.NavigateURL(tabId, "", parameters);
					Response.Redirect(theUrl);
				}
			}
		}

		#region [ Block layouts ]

        /// <summary>
        /// Gets the sub categories list layout.
        /// </summary>
        /// <param name="block">The block.</param>
        /// <returns>System.String.</returns>
		public string GetSubCategoriesListLayout(object block)
		{
			var catBlock = (CategoryBlock) block;
			string html = string.Empty;
			if (catBlock.SubCategories.Any(sb => sb != null))
			{
				html = catBlock.SubCategories.Where(sb => sb != null).Aggregate(html, (current, subCat) => current + string.Format("<a href={0}>{1}</a>, ", GetCategoryViewUrl(subCat.CategoryId), subCat.CategoryName));
			}
			return html.Substring(0, html.Length > 0 ? html.Length - 2 : 0); //Remove last commas
		}

        /// <summary>
        /// Gets the category block main image.
        /// </summary>
        /// <param name="block">The block.</param>
        /// <returns>System.String.</returns>
		public string GetCategoryBlockMainImage(object block)
		{
			var catBlock = (CategoryBlock) block;
			string html = string.Empty;
			if (catBlock.SubCategories.Any())
			{
				List<CategoryBlock> subCats = SortCategoryBlock(catBlock.SubCategories.Where(sc => sc != null));
				if (subCats.Count(categoryBlock => categoryBlock.SortOrder == 0) > 0)
				{
					foreach (CategoryBlock cat in subCats.Where(a => a.SortOrder == 0))
					{
						html += string.Format("<a href='{0}'><img src='{1}' title='{2}' alt='{2}' width='142px' height='118px' /></a>",GetCategoryViewUrl(cat.CategoryId), cat.TopicImage, cat.MouseOverText);
					}
				}
				else
				{
                    html = string.Format("<a href='{0}'><img src='{1}' title='{2}' alt='{2}' width='300px' height='250px' /></a>", GetCategoryViewUrl(subCats[0].CategoryId), subCats[0].TopicImage, subCats[0].MouseOverText);
				}
			}
			else
			{
                html = string.Format("<a href='{0}'><img src='{1}' title='{2}' alt='{2}' width='300px' height='250px' /></a>", GetCategoryViewUrl(catBlock.CategoryId), catBlock.TopicImage, catBlock.MouseOverText);
			}
			return html;
		}

        /// <summary>
        /// Gets the category block sub image.
        /// </summary>
        /// <param name="block">The block.</param>
        /// <returns>System.String.</returns>
		public string GetCategoryBlockSubImage(object block)
		{
			var catBlock = (CategoryBlock) block;
			string html = string.Empty;
			if (catBlock.SubCategories.Any(sb=>sb!=null))
			{
				List<CategoryBlock> subCats = SortCategoryBlock(catBlock.SubCategories.Where(sb => sb != null));
				if (subCats.Count(a => a.SortOrder == 0) > 0)
				{
					foreach (CategoryBlock cat in subCats.Where(a => a.SortOrder != 0).ToList())
					{
                        html += string.Format("<a href='{0}'><img src='{1}' title='{2}' alt='{2}' width='94px' height='78px' /></a>", GetCategoryViewUrl(cat.CategoryId), cat.TopicImage, cat.MouseOverText);
					}
				}
				else
				{
					List<CategoryBlock> cats = subCats.Where(a => a.SortOrder != 0).ToList();
					for (int i = 1; i < cats.Count; i++)
					{
						CategoryBlock cat = cats[i];
                        html += string.Format("<a href='{0}'><img src='{1}' title='{2}' alt='{2}' width='94px' height='78px' /></a>", GetCategoryViewUrl(cat.CategoryId), cat.TopicImage, cat.MouseOverText);
					}
				}
			}
			return html;
		}

		#endregion

		#region [ Event Handlers ]

        /// <summary>
        /// Handles the OnNodeClick event of the categoryTree control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Telerik.Web.UI.RadTreeNodeEventArgs" /> instance containing the event data.</param>
		protected void categoryTree_OnNodeClick(object sender, RadTreeNodeEventArgs e)
		{
			cbCategories.SelectedValue = e.Node.Value;
		}

        /// <summary>
        /// Handles the OnSelectedIndexChanged event of the cbAudience control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs" /> instance containing the event data.</param>
		protected void cbAudience_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
		{
			Response.Redirect(Globals.NavigateURL(TabId, "",string.Format("mid={0}&lid={1}&aid={2}&cid{3}", ModuleId,_languageId, _audienceId, "-1")),false);
		}

        /// <summary>
        /// Handles the OnSelectedIndexChanged event of the cbLanguage control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RadComboBoxSelectedIndexChangedEventArgs"/> instance containing the event data.</param>
        protected void cbLanguage_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (!_categoryBlocks.Any()) // if there are no categories block, then reset categories tree when change language
            {
                Response.Redirect(Globals.NavigateURL(TabId, "", string.Format("mid={0}&lid={1}&aid={2}&cid={3}",ModuleId,_languageId,_audienceId,"-1")),false);
            }
        }

		#endregion

	    
	}
}