﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleSettings.ascx.cs" Inherits="TIEKinetix.CspModules.CSPStartSyndication.ModuleSettings" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div>
    <h2>Basic Settings</h2>
	<table>
		<tbody>
			<tr>
				<td>Campaign Content type Id</td>
				<td>
					<telerik:radtextbox ID="tbCampaignContentTypeId" runat="server"/>
				</td>
			</tr>
			<tr>
				<td>Category Content type Id</td>
				<td>
					<telerik:RadTextBox runat="server" ID="tbCategoryCampaignTypeId" />
				</td>
			</tr>
			<tr>
				<td>Asset Content type Id</td>
				<td>
					<telerik:RadTextBox runat="server" ID="tbAssetContentTypeId"/>
				</td>
			</tr>
			<tr>
				<td>PPPGeneral Content type Id</td>
				<td>
					<telerik:RadTextBox runat="server" ID="tbPPPGeneralContentTypeId"/>
				</td>
			</tr>
			<tr>
				<td>Supplier Id</td>
				<td>
					<telerik:RadTextBox runat="server" ID="tbSupplierId"/>
				</td>
			</tr>
			<tr>
				<td>Banner Embed Code Source</td>
				<td>
					<telerik:RadTextBox runat="server" ID="tbEmbedCodeSrc" />
				</td>
			</tr>
			<tr>
				<td>Campaign Preview URL</td>
				<td>
					<telerik:RadTextBox runat="server" ID="tbCampaignPreviewURL" />
				</td>
			</tr>
			<tr>
				<td>Campaign Description Length</td>
				<td>
					<telerik:RadTextBox runat="server" ID="tbCampaignDescLength" />
				</td>
			</tr>
            <tr>
				<td>Company Setting Tab Name</td>
				<td>
					<telerik:RadTextBox runat="server" ID="tbTabName" />
				</td>
			</tr>
			<tr>
				<td>Portal Instance Name</td>
				<td>
					<telerik:RadTextBox runat="server" ID="tbPortalInstance"/>
				</td>
			</tr>
            <tr>
                <td>Preview Popup Width (in pixel)</td>
                <td>
                    <telerik:RadTextBox runat="server" ID="tbPreviewPopupWidth"/>
                </td>
            </tr>
            <tr>
                <td>Preview Popup Height (in pixel)</td>
                <td>
                    <telerik:RadTextBox runat="server" ID="tbPreviewPopupHeight"/>
                </td>
            </tr>
            <tr>
                <td>Active sync language</td>
                <td>
                    <asp:CheckBox runat="server" ID="cbxSyncLang"/>
                </td>
            </tr>
            <tr>
                <td>Disable Navigation Panel?</td>
                <td>
                    <asp:CheckBox runat="server" ID="cbxDisableNavPanel"/>
                </td>
            </tr>
		</tbody>
	</table>
</div>
<div>
    <h2>Customize Campaign Local Audience</h2>
    <asp:HiddenField runat="server" ID="hfListAudience" />
    <table id="tbListAudience" class="setting_list_tb">
        <tr class="heading">
            <th style="width: 200px;">Local Audience</th>
            <th>Action</th>
        </tr>
        <%= GetListOfLocalAudienceLayout() %>
    </table>
    <table>
        <tr>
            <td>
                <asp:TextBox Width="198" runat="server" ID="tbxAudience"></asp:TextBox>
            </td>
            <td>
                <telerik:RadButton runat="server" Text="Add" ID="btnAddAudience" AutoPostBack="False">
                    <Icon PrimaryIconCssClass="rbAdd" PrimaryIconLeft="4" PrimaryIconTop="4"></Icon>
                </telerik:RadButton>
            </td>
        </tr>
    </table>
</div>
<div>
    <h2>Customize Local Partner Type</h2>
    <asp:HiddenField runat="server" ID="hfListPartnerType"/>
    <table id="tbListPartnerType" class="setting_list_tb">
        <tr class="heading">
            <th style="width: 140px;">Local Parner Type</th>
            <th style="width: 90px;">Category Id</th>
            <th>Action</th>
        </tr>
        <%= GetListOfLocalPartnerTypeLayout() %>
    </table>
    <table>
        <tr>
            <td>
                <asp:TextBox Width="138" runat="server" ID="tbxPartnerType"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox  Width="88" runat="server" ID="tbxPartnerTypeCatId"></asp:TextBox>
            </td>
            <td>
                <telerik:RadButton runat="server" Text="Add" ID="btnAddPartnerType" AutoPostBack="False">
                    <Icon PrimaryIconCssClass="rbAdd" PrimaryIconLeft="4" PrimaryIconTop="4"></Icon>
                </telerik:RadButton>
            </td>
        </tr>
    </table>
</div>
<div style="border-top:  1px solid #ddd; margin-top: 10px; padding-top: 10px;">
    <table width="100%">
        <tr>
            <td>
               <telerik:RadComboBox runat="server" ID="cbCategories" AutoPostBack="False" Width="400px" Height="300px" EmptyMessage="Select categories to skip level at Categories Tree">
                   <ItemTemplate>
                       <telerik:RadTreeView ID="categoryTree" runat="server" CheckBoxes="True">
							<DataBindings>
								<telerik:RadTreeNodeBinding Expanded="true" />
							</DataBindings>
						</telerik:RadTreeView>           
                   </ItemTemplate>
                   <Items>
						<telerik:RadComboBoxItem Text="Select categories to skip level at Categories Tree" />
					</Items>
					<CollapseAnimation Type="None" />
               </telerik:RadComboBox>
            </td>
        </tr>
    </table>
</div>
<div class="clear"></div>
<telerik:RadScriptBlock runat="server">
    <script type="text/javascript">
        jQuery(function () {
            Init();
        });
        
        function Init() {
            
            //Add new partner type
            jQuery("#<%= btnAddPartnerType.ClientID %>").click(function (event) {
                var name = jQuery("#<%= tbxPartnerType.ClientID %>").val();
                var catid = jQuery("#<%= tbxPartnerTypeCatId.ClientID %>").val();
                if (name == "" || catid == "")
                    return false;
                var list = jQuery("#<%= hfListPartnerType.ClientID %>").val();
                list = list + "," + name + "-" + catid;
                jQuery("#<%= hfListPartnerType.ClientID %>").val(list);
                jQuery("#<%= tbxPartnerType.ClientID %>").val("");
                jQuery("#<%= tbxPartnerTypeCatId.ClientID %>").val("");
                jQuery("#tbListPartnerType").append("<tr class='row'><td class='name'>" + name + "</td><td class='id'>" + catid + "</td><td><a href='#' class='delete_partnertype'>Delete</a></td></tr>");
                Init();
                event.preventDefault();
                return false;
            });
            
            // Remove a partner type
            jQuery(".delete_partnertype").click(function () {
                var name = jQuery(this).parents(".row").children(".name").html();
                var catid = jQuery(this).parents(".row").children(".id").html();
                var list = jQuery("#<%= hfListPartnerType.ClientID %>").val();
                jQuery("#<%= hfListPartnerType.ClientID %>").val(list.split(name + "-" + catid).join(""));
                jQuery(this).parents(".row").fadeOut();
                return false;
            });

            //Remove an audience
            jQuery(".delete_audience").click(function () {
                var audienceName = jQuery(this).parents(".row").children(".name").html();
                var list = jQuery("#<%= hfListAudience.ClientID %>").val();
                jQuery("#<%= hfListAudience.ClientID %>").val(list.split(audienceName).join(""));
                jQuery(this).parents(".row").fadeOut();
                return false;
            });
            
            //Add new audience
            jQuery("#<%= btnAddAudience.ClientID %>").click(function (event) {
                var value = jQuery("#<%= tbxAudience.ClientID %>").val();
                if (value == "")
                    return false;

                var list = jQuery("#<%= hfListAudience.ClientID %>").val();
                list = list + "," + value;
                jQuery("#<%= hfListAudience.ClientID %>").val(list);
                jQuery("#<%= tbxAudience.ClientID %>").val("");

                jQuery("#tbListAudience").append("<tr class='row'><td class='name'>" + value + "</td><td><a href='#' class='delete_audience'>Delete</a></td></tr>");
                Init();
                event.preventDefault();
                return false;
            });
        }
    </script>
</telerik:RadScriptBlock>