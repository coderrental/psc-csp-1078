﻿using System.Collections.Generic;

namespace TIEKinetix.CspModules.CSPStartSyndication.Entities
{
	public class CategoryBlock
	{

		/// <summary>
		/// Initializes a new instance of the <see cref="CategoryBlock"/> class.
		/// </summary>
		public CategoryBlock()
		{
			this.SubCategories = new List<CategoryBlock>();
		    MouseOverText = string.Empty;
		}

		/// <summary>
		/// Gets or sets the category id.
		/// </summary>
		/// <value>
		/// The category id.
		/// </value>
		public int CategoryId { get; set; }

		/// <summary>
		/// Gets or sets the topic image.
		/// </summary>
		/// <value>
		/// The topic image.
		/// </value>
		public string TopicImage { get; set; }

		/// <summary>
		/// Gets or sets the name of the category.
		/// </summary>
		/// <value>
		/// The name of the category.
		/// </value>
		public string CategoryName { get; set; }

		/// <summary>
		/// Gets or sets the category URL.
		/// </summary>
		/// <value>
		/// The category URL.
		/// </value>
		public string CategoryUrl { get; set; }

		/// <summary>
		/// Gets or sets the sort order.
		/// </summary>
		/// <value>
		/// The sort order.
		/// </value>
		public int SortOrder { get; set; }

        /// <summary>
        /// Gets or sets the mouse over text.
        /// </summary>
        /// <value>
        /// The mouse over text.
        /// </value>
        /// Author: Vu Dinh
        /// 11/14/2012 - 6:31 PM
        public string MouseOverText { get; set; }

		/// <summary>
		/// Gets or sets the sub categories.
		/// </summary>
		/// <value>
		/// The sub categories.
		/// </value>
		public List<CategoryBlock> SubCategories { get; set; } 
	}
}