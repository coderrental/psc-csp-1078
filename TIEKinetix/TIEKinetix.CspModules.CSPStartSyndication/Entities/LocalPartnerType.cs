﻿namespace TIEKinetix.CspModules.CSPStartSyndication.Entities
{
    public class LocalPartnerType
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }


        /// <summary>
        /// Gets or sets the catgegory id.
        /// </summary>
        /// <value>The catgegory id.</value>
        public int CatgegoryId { get; set; }
    }
}