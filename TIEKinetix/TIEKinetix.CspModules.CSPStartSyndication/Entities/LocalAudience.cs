﻿// ***********************************************************************
// Assembly         : TIEKinetix.CspModules.CSPStartSyndication
// Author           : VU DINH
// Created          : 06-06-2013
//
// Last Modified By : VU DINH
// Last Modified On : 06-06-2013
// ***********************************************************************
// <copyright file="LocalAudience.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace TIEKinetix.CspModules.CSPStartSyndication.Entities
{
    /// <summary>
    /// Class LocalAudience
    /// </summary>
    public class LocalAudience
    {
        /// <summary>
        /// Gets or sets the name of the audience.
        /// </summary>
        /// <value>The name of the audience.</value>
        public string AudienceName { get; set; }


        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        public int Id { get; set; }
    }
}