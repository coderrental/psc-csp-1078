﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TIEKinetix.CspModules.CSPStartSyndication.Entities
{
	public class BannerEntity
	{
		public string Wide_Skyscraper_160x600_Image { get; set; }

		public static string Wide_Skyscraper_160x600_Image_fieldname = "Wide_Skyscraper_160x600_Image";

		public string Medium_Rectangle_300x250_Image { get; set; }

		public static string Medium_Rectangle_300x250_Image_fieldname = "Medium_Rectangle_300x250_Image";

		public string Full_Banner_468x60_Image { get; set; }

		public static string Full_Banner_468x60_Image_fieldname = "Full_Banner_468x60_Image";

		public string Half_Banner_234x60_Image { get; set; }

		public static string Half_Banner_234x60_Image_fieldname = "Half_Banner_234x60_Image";

		public string Banner_Thumbnail_Image { get; set; }

		public static string Banner_Thumbnail_Image_fieldname = "Banner_Thumbnail_Image";

		public string Button_120x60_Image { get; set; }

		public static string Button_120x60_Image_fieldname = "Button_120x60_Image";

		public string Button_180x90_Image { get; set; }

		public static string Button_180x90_Image_fieldname = "Button_180x90_Image";
	}
}