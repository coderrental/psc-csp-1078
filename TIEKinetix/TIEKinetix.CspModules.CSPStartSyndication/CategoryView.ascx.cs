﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.ContentObjectLibrary;
using CR.ContentObjectLibrary.Data;
using CR.ContentObjectLibrary.Factory;
using CR.ContentObjectLibrary.Interface;
using DotNetNuke.Common.Utilities;
using DotNetNuke.UI.WebControls;
using TIEKinetix.CspModules.CSPStartSyndication.Common;
using CR.DnnModules.Common;
using DotNetNuke.Common;
using DotNetNuke.Services.Mail;
using TIEKinetix.CspModules.CSPStartSyndication.Entities;

namespace TIEKinetix.CspModules.CSPStartSyndication
{
	public partial class CategoryView : DnnModuleBase
	{
		private content _content;
		private content _contentText;
		private category _category;
		private int _categoryId;
		private int _langId;
		private int _audienceId = 0;
		private int _pppgeneralContentTypeId = Cons.PPPGENERAL_CONTENT_TYPE_ID;
		private string _mainViewUrl;
		private string _audience;
		private string _langText;
		private int _cspId;
		private Dictionary<int, string> _categoryContentMapping; 
		protected List<content> _campaignContent;
		protected string consumerBaseURL;

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			//Get csp_id form userinfo
			_cspId = Utils.GetIntegrationKey(UserInfo);
			_categoryContentMapping = new Dictionary<int, string>();
			_langText =
				CspDataContext.languages.FirstOrDefault(a => a.languages_Id == int.Parse(Request.QueryString["langid"])).languagecode;
			
			//Ajax request
			if (!String.IsNullOrEmpty(Request.Params["ajaxaction"]))
			{
				if (Request.Params["ajaxaction"] == "buynowurl")
					UpdateBuyNowURL();
				else if (Request.Params["ajaxaction"] == "emailembedcode")
					SendEmbedCodeByEmail();
				return;
			}
			//Get audience info from request query string
			GetAudienceInfo();

			//Get URL for generate embed code & preview
			GetConsumerBaseDomain();
			//Get category content
			GetCategoryContent();
			
			//Get category campaign content
			GetCampaignContent();

		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!String.IsNullOrEmpty(Request.Params["ajaxaction"]))
				return;
			//Init css, javascript files
			InitResources();
			//Init campaign list (show list)
			InitCampaignList();
			//Init cats tile (show list)
			InitCatsTile();
            //Init breadcrumb
		    InitBreadcrumb();
		}

        /// <summary>
        /// Inits the breadcrumb.
        /// </summary>
        /// Author: Vu Dinh
        /// 11/13/2012 - 7:10 PM
        private void InitBreadcrumb()
        {
            string catName;
            var category = CspDataContext.categories.SingleOrDefault(a => a.categoryId == _categoryId);
            if (category != null)
            {
                var temp = category;
                string url;
                while ( temp != null && temp.parentId != 0)
                {
                    if (_categoryContentMapping.ContainsKey(_categoryId))
                    {
                        catName = _categoryContentMapping[temp.categoryId];
                    }
                    else
                    {
                        var catContent = CspDataContext.contents.FirstOrDefault(
                            a =>
                            a.content_main.content_categories.Count(b => b.category_Id == temp.categoryId) > 0 &&
                            a.content_types_languages_Id == _langId &&
                            a.content_main.content_types_Id == CategoryContentTypeId);
                        catName = catContent == null ? temp.categoryText : Utils.GetContentFieldValue(CspDataContext, catContent, "Content_Title");
                    }

                    if (temp == category)
                    {
                        TopicBreadcrumbPanel.Controls.AddAt(0, new Literal { Text = catName });    
                    }
                    else
                    {
                        //skip local partner type
                        if (ListLocalPartnerType.Count > 0 && ListLocalPartnerType.Any(a => a.CatgegoryId == temp.categoryId))
                        {
                            break;
                        }
                        //skip categories level - get from module settings
                        if (ListCategoriesIdToSkipLevel.Count > 0 &&ListCategoriesIdToSkipLevel.Any(a => a == temp.categoryId))
                        {
                            break;
                        }

                        url = Globals.NavigateURL(TabId, "", string.Format("mid={0}&lid={1}&cid={2}&aid={3}", ModuleId, _langId, temp.categoryId, _audienceId));
                        TopicBreadcrumbPanel.Controls.AddAt(0, new Literal { Text = string.Format("<a href='{0}'>{1}</a> &raquo; ", url, catName) });    
                      
                        
                    }
                    
                    temp = CspDataContext.categories.SingleOrDefault(a => a.categoryId == temp.parentId);
                }
                url = Globals.NavigateURL(TabId, "", string.Format("mid={0}&lid={1}&cid={2}&aid={3}", ModuleId, _langId, "-1", _audienceId));
                TopicBreadcrumbPanel.Controls.AddAt(0, new Literal { Text = string.Format("<a href='{0}'>{1}</a> &raquo; ", url, GetLocalizedText("Category.All")) });
               
            }
           
        }

		/// <summary>
		/// @ducuytran - 12/10/16
		/// Init all resources (e.g css, js, v.v..)
		/// </summary>
		private void InitResources()
		{
			Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel='stylesheet' href='{0}Css/Category.css' type='text/css' />", ControlPath)));
			Page.ClientScript.RegisterClientScriptInclude("carousel", ControlPath + "Js/carousel.js");
			Page.ClientScript.RegisterClientScriptInclude("category", ControlPath + "Js/Category.js");
			Page.ClientScript.RegisterClientScriptInclude("zclip", ControlPath + "Js/jquery.zclip.min.js");

			RadAjaxLoadingPanel1.EnableSkinTransparency = true;
		}

		/// <summary>
		/// @ducuytran - 12/10/19
		/// Show campaign contents on list view
		/// </summary>
		private void InitCampaignList()
		{
			lvCategoryCampaign.DataSource = _campaignContent;
		}

		/// <summary>
		/// @ducuytran - 12/10/19
		/// Get categoryId & category content
		/// </summary>
		private void GetCategoryContent()
		{
			_mainViewUrl = Globals.NavigateURL(TabId, "", "mid", ModuleId.ToString());

			//Invalid categoryid or langid (invalid int)
			if (!int.TryParse(Request.Params["cid"], out _categoryId) || !int.TryParse(Request.Params["langid"], out _langId))
				Response.Redirect(_mainViewUrl, false);
            if (Settings[Cons.SETTING_SYNC_LANGUAGE] != null)
            {
                if (Settings[Cons.SETTING_SYNC_LANGUAGE].ToString() == "1")
                {
                    var lang = CspDataContext.languages.FirstOrDefault(a => a.BrowserLanguageCode == System.Threading.Thread.CurrentThread.CurrentCulture.Name);
                    if (lang != null)
                    {
                        if (lang.languages_Id != _langId)
                            Response.Redirect(_mainViewUrl, false);
                    }
                    else
                    {
                        if (_langId != 1)
                            Response.Redirect(_mainViewUrl, false);
                    }    
                }
            }

			_category = CspDataContext.categories.FirstOrDefault(a => a.categoryId == _categoryId);

			_content =
				CspDataContext.contents.SingleOrDefault(
					a =>
					a.content_types_languages_Id == _langId && a.content_main.supplier_Id == SupplierId &&
					a.content_main.content_categories.Count(b => b.category_Id == _categoryId) > 0 && a.content_main.content_types_Id == CategoryContentTypeId);
			if (Settings[Cons.SETTING_PPPGENERAL_CONTENT_TYPE_ID] != null && Settings[Cons.SETTING_PPPGENERAL_CONTENT_TYPE_ID].ToString() != string.Empty)
				int.TryParse(Settings[Cons.SETTING_PPPGENERAL_CONTENT_TYPE_ID].ToString(), out _pppgeneralContentTypeId);
			List<content> allContetText = CspDataContext.contents.Where(a => a.content_types_languages_Id == _langId && a.content_main.supplier_Id == SupplierId && a.stage_Id == Keys.CspPublishStageId && a.content_main.content_types_Id == _pppgeneralContentTypeId).ToList();
			_contentText = allContetText.FirstOrDefault(a => a["Local_Audience"].value_text.ToLower() == _audience.ToLower());
		}

		/// <summary>
		/// Get content text (use content_types_Id = 5)
		/// </summary>
		/// <param name="fieldName">Field name</param>
		/// <returns>Field value</returns>
		protected string GetContentText(string fieldName)
		{
			string contentText = "";
			if (_contentText == null)
				return contentText;
			contentText = GetContentFieldValue(_contentText, fieldName);
			return contentText;
		}

		/// <summary>
		/// @ducuytran - 12/10/19
		/// Get audience info from request query string
		/// </summary>
		private void GetAudienceInfo()
		{
			if (!int.TryParse(Request.QueryString["audid"], out _audienceId))
				Response.Redirect(_mainViewUrl, false);
			//_audienceId = audienceId;
			/*
			if (_audienceId == Cons.AUDIENCE_BUSINESS_ID)
				_audience = Cons.AUDIENCE_BUSINESS_TEXT;
			else if (_audienceId == Cons.AUDIENCE_CONSUMER_ID)
				_audience = Cons.AUDIENCE_CONSUMER_TEXT;
			else
				Response.Redirect(_mainViewUrl, false);
			*/

			_audience = ListLocalAudiences.First(a => a.Id == _audienceId).AudienceName;
		}

		/// <summary>
		/// @ducuytran - 12/10/19
		/// Get category campaign contents
		/// </summary>
		/// <update>
		/// @ducuytran - 12/10/24
		/// Use GetListCampaign() to get by consumer & publish stage
		/// </update>
		private void GetCampaignContent()
		{
			_campaignContent = GetListCampaign(_category);
			GetBannerList();
		}

		/// <summary>
		/// @ducuytran - 12/10/21
		/// Get banner list
		/// </summary>
		private void GetBannerList()
		{
			List<BannerEntity> bannerList = new List<BannerEntity>();
			foreach (content cContent in _campaignContent)
			{
				bannerList.Add(new BannerEntity
				               	{
				               		Wide_Skyscraper_160x600_Image = GetContentFieldValue(cContent, BannerEntity.Wide_Skyscraper_160x600_Image_fieldname),
									Medium_Rectangle_300x250_Image = GetContentFieldValue(cContent, BannerEntity.Medium_Rectangle_300x250_Image_fieldname),
									Full_Banner_468x60_Image = GetContentFieldValue(cContent, BannerEntity.Full_Banner_468x60_Image_fieldname),
									Half_Banner_234x60_Image = GetContentFieldValue(cContent, BannerEntity.Half_Banner_234x60_Image_fieldname),
									Button_120x60_Image = GetContentFieldValue(cContent, BannerEntity.Button_120x60_Image_fieldname),
									Button_180x90_Image = GetContentFieldValue(cContent, BannerEntity.Button_180x90_Image_fieldname),
									Banner_Thumbnail_Image = GetContentFieldValue(cContent, BannerEntity.Banner_Thumbnail_Image_fieldname)
				               	});
			}
			BannerRotater.DataSource = bannerList;
		}

		/// <summary>
		/// @ducuytran - 12/10/19
		/// Get category content title
		/// </summary>
		/// <returns>Category content title</returns>
		protected string GetCategoryTitle()
		{
			if (_content == null)
				return _category.categoryText;
            var catName = Utils.GetContentFieldValue(CspDataContext, _content, "Content_Title");
            _categoryContentMapping.Add(_categoryId,catName);
		    return catName;
		}

		/// <summary>
		/// @ducuytran - 12/10/19
		/// Get category content description (from .resx)
		/// </summary>
		/// <returns>
		/// Category content description
		/// </returns>
		/// <update>
		/// @ducuytran - 12/10/25
		/// Get from content where content_type_Id = 5
		/// </update>
		protected string GetCategoryDescription()
		{
			string catDesc = GetContentText("Content_Campaign_Intro");
			string charsToReplace = "{campaignNum}";
			if (String.IsNullOrEmpty(catDesc))
			{
				catDesc = GetLocalizedText("CategoryView.CategoryContentDescription");
				charsToReplace = "[campaign_count]";
			}
			catDesc = catDesc.Replace(charsToReplace, _campaignContent.Count().ToString());
			return catDesc;
		}

		protected string GetCategoryContentImage()
		{
			return GetCategoryImage(_category);
		}

		/// <summary>
		/// @ducuytran - 12/10/19
		/// Get campaign content title
		/// </summary>
		/// <param name="campaignItem">content</param>
		/// <returns>
		/// Content title
		/// </returns>
		protected string GetCampaignTitle(object campaignItem)
		{
            string campaignTitle = Utils.GetContentFieldValue(CspDataContext, campaignItem as content, "Microsite_Title");
			if (String.IsNullOrEmpty(campaignTitle))
				campaignTitle = GetLocalizedText("CategoryView.EmptyCampaignTitle");
			return campaignTitle;
		}

		/// <summary>
		/// @ducuytran - 12/10/19
		/// Get campaign thumbnail
		/// </summary>
		/// <param name="campaignItem">campaign content</param>
		/// <returns>HTML img</returns>
		protected string GetCampaignThumbnail(object campaignItem)
		{
            string thumbUrl = Utils.GetContentFieldValue(CspDataContext, campaignItem as content, "Medium_Rectangle_300x250_Image");
			if (String.IsNullOrEmpty(thumbUrl))
				return "<img height='100px' width='100px' src='" + GetNoImageUrl() + "' />";
			return String.Format("<img height='100px' width='100px' src='{0}' />", thumbUrl);
		}

		/// <summary>
		/// @ducuytran - 12/10/22
		/// Get shorten campaign content description
		/// </summary>
		/// <param name="campaignItem">campaign item object</param>
		/// <returns>Shorten description</returns>
		protected string GetCampaignShortDesc(object campaignItem)
		{
			string cDesc = GetContentFieldValue(campaignItem, "PPP_Description_Short");
			int cDescLength = 0;
			
			//Get char count from setting
			if (Settings[Cons.SETTING_CAMPAIGN_DESCRIPTION_LENGTH] != null && !String.IsNullOrEmpty(Settings[Cons.SETTING_CAMPAIGN_DESCRIPTION_LENGTH].ToString()))
				int.TryParse(Settings[Cons.SETTING_CAMPAIGN_DESCRIPTION_LENGTH].ToString(), out cDescLength);
			
			if (cDescLength <= 0)
				cDescLength = Cons.CAMPAIGN_DESC_LENGTH_DEFAULT;
			
			//Shortening the description
			string newDesc = (cDesc.Length > cDescLength ? cDesc.Substring(0, cDescLength) : cDesc);
			return newDesc == string.Empty ? string.Empty : newDesc.Substring(0, newDesc.LastIndexOf(' ')) + "...";
		}

		/// <summary>
		/// @ducuytran - 12/10/22
		/// Get campaign preview URL
		/// </summary>
		/// <returns>Campaign preview URL</returns>
		protected string GetCampaignPreviewURL()
		{
			string previewURL = consumerBaseURL;
			string portalInstanceName = "null";
			
			if (String.IsNullOrEmpty(previewURL) && Settings[Cons.SETTING_CAMPAIGN_PREVIEW_URL] != null && !String.IsNullOrEmpty(Settings[Cons.SETTING_CAMPAIGN_PREVIEW_URL].ToString()))
				previewURL = Settings[Cons.SETTING_CAMPAIGN_PREVIEW_URL].ToString();
			
			if (String.IsNullOrEmpty(previewURL))
				previewURL = Cons.CAMPAIGN_PREVIEW_URL_DEFAULT;
			
			if (!previewURL.EndsWith("/"))
				previewURL += "/";
			if (!previewURL.StartsWith("http://") && !previewURL.StartsWith("https://"))
				previewURL = "http://" + previewURL;

			if (Settings[Cons.SETTING_PORTAL_INSTANCE_NAME] != null && !String.IsNullOrEmpty(Settings[Cons.SETTING_PORTAL_INSTANCE_NAME].ToString()))
				portalInstanceName = Settings[Cons.SETTING_PORTAL_INSTANCE_NAME].ToString();

			previewURL += "ftp/campaign/" + portalInstanceName + "/[microsite_link_url]/" + _langText;
			return previewURL;
		}

		/// <summary>
		/// Get campaign buy now url
		/// </summary>
		/// <param name="campaignItem"></param>
		/// <returns>Campaign buy now url</returns>
		protected string GetCampaignBuyNowURL(object campaignItem)
		{
			string buyNowURL = "";
			content campaignContent = campaignItem as content;
			content_field contentFieldBuyNowURL =
				CspDataContext.content_fields.FirstOrDefault(
					a => a.content_Id == campaignContent.content_Id && a.content_types_field.fieldname == "Buy_Now_URL");
			if (contentFieldBuyNowURL == null)
				return buyNowURL;

			custom_content_field contentBuyNowURL =
				CspDataContext.custom_content_fields.FirstOrDefault(
					a => a.consumer_Id == _cspId && a.content_Id == campaignContent.content_Id && a.content_fields_Id == contentFieldBuyNowURL.content_fields_Id);
			if (contentBuyNowURL == null)
				return buyNowURL;

			buyNowURL = contentBuyNowURL.value_text;
			return buyNowURL;
		}

		/// <summary>
		/// Get campaign's content id & buy now url content field id
		/// </summary>
		/// <param name="campaignItem"></param>
		/// <returns>content id & buy now url content field id</returns>
		protected string GetCampaignIdInfo(object campaignItem)
		{
			string idInfo = "";
			content campaignContent = campaignItem as content;
			content_field contentFieldBuyNowURL =
				CspDataContext.content_fields.FirstOrDefault(
					a => a.content_Id == campaignContent.content_Id && a.content_types_field.fieldname == "Buy_Now_URL");

			if (contentFieldBuyNowURL == null)
				return idInfo;

			idInfo = campaignContent.content_Id.ToString() + ";" + contentFieldBuyNowURL.content_fields_Id.ToString();
			return idInfo;
		}

		/// <summary>
		/// @ducuytran - 12/10/22
		/// Get generate embed code src
		/// </summary>
		/// <returns>generate embed code src</returns>
		protected string GetGenerateEmbedSource()
		{
			string embedSrc = consumerBaseURL;

			if (String.IsNullOrEmpty(embedSrc) && Settings[Cons.SETTING_GENERATE_EMBED_CODE_SRC] != null && !String.IsNullOrEmpty(Settings[Cons.SETTING_GENERATE_EMBED_CODE_SRC].ToString()))
				embedSrc = Settings[Cons.SETTING_GENERATE_EMBED_CODE_SRC].ToString();

			if (String.IsNullOrEmpty(embedSrc))
				embedSrc = Cons.GENERATE_EMBED_CODE_SRC_DEFAULT;

			if (!embedSrc.EndsWith("/"))
				embedSrc += "/";
			if (!embedSrc.StartsWith("http://") && !embedSrc.StartsWith("https://"))
				embedSrc = "http://" + embedSrc;

			embedSrc += "Csp/?t=campaign&category=[category_id]&audience=[audience_text]&banner=[banner_type]&lng=[lang_text]";

			embedSrc = embedSrc.Replace("[category_id]", _categoryId.ToString());
			embedSrc = embedSrc.Replace("[audience_text]", _audience);
			embedSrc = embedSrc.Replace("[lang_text]", _langText);
			return embedSrc;
		}

		protected string GetConsumerBaseDomain()
		{
            consumerBaseURL = "";
		    var portalInstanceName = string.Empty;
            if (Settings[Cons.SETTING_PORTAL_INSTANCE_NAME] != null && !String.IsNullOrEmpty(Settings[Cons.SETTING_PORTAL_INSTANCE_NAME].ToString()))
                portalInstanceName = Settings[Cons.SETTING_PORTAL_INSTANCE_NAME].ToString();
			
			companies_consumer coConsumer = CspDataContext.companies_consumers.FirstOrDefault(a => a.companies_Id == _cspId);
			if (coConsumer == null)
				return String.Empty;
			consumerBaseURL = string.IsNullOrEmpty(portalInstanceName) ? coConsumer.base_domain : "p" + portalInstanceName + "-" + coConsumer.base_domain;
			return coConsumer.base_domain;
		}

		/// <summary>
		/// @ducuytran - 12/10/19
		/// Get content field value
		/// </summary>
		/// <param name="contentItem">content</param>
		/// <param name="fieldName">fieldname</param>
		/// <returns>String</returns>
		protected string GetContentFieldValue(object contentItem, string fieldName)
		{
            return Utils.GetContentFieldValue(CspDataContext, contentItem as content, fieldName);
		}

		protected string GetContentFieldValue(content contentItem, string fieldName)
		{
            return Utils.GetContentFieldValue(CspDataContext, contentItem, fieldName);
		}

		/// <summary>
		/// Inits the cats tile.
		/// Author: DINH LONG VU
		/// </summary>
		protected void InitCatsTile()
		{
			var categoryBlocks = new List<CategoryBlock>();
			if (_category.depth == 3)
			{
				categoryBlocks.AddRange(from subcats in CspDataContext.categories.Where(a => a.parentId == _category.parentId && a.categoryId != _category.categoryId).ToList() where GetListCampaign(subcats).Any() select GetCategoryBlock(subcats));
			}
			listSubCatsTile.DataSource = categoryBlocks;
			if (categoryBlocks.Count == 0)
				switch_product_section_wrapper.Visible = false;
		}

		/// <summary>
		/// Gets the list campaign.
		/// </summary>
		/// <returns></returns>
		private List<content> GetListCampaign(category cat)
		{
			var resultList = new List<content>();
			var searchFactory = (SimpleSearch)SearchExpressionFactory.CreateSearch();

			searchFactory.Left = SearchExpressionFactory.CreateSearch(CspDataContext.content_types.FirstOrDefault(a => a.content_types_Id == CampaignContentTypeId));
			searchFactory.Operator = SearchOperator.And;
			searchFactory.Right = SearchExpressionFactory.CreateSearch();
			searchFactory.Right.Left = SearchExpressionFactory.CreateSearch(CspDataContext.companies.FirstOrDefault(a => a.companies_Id == SupplierId));
			searchFactory.Right.Operator = SearchOperator.And;
			searchFactory.Right.Right = SearchExpressionFactory.CreateSearch(CspDataContext.languages.FirstOrDefault(a => a.languages_Id == _langId));

			IContentAccessFilter caf = ContentAccessFilterFactory.CreateContentAccessFilter(searchFactory, CspDataContext, cat.categoryId);

			resultList = caf.Search().Where(a => a.stage_Id == Keys.CspPublishStageId)
				.OrderBy(a => Utils.GetContentFieldValue(CspDataContext, a, "Status_Featured_Y_N") == "yes")
				.OrderByDescending(a => Utils.GetContentFieldValue(CspDataContext, a, "Status_Sort_Order")).ToList();
			if (_audienceId == Cons.AUDIENCE_BUSINESS_ID || _audienceId == Cons.AUDIENCE_CONSUMER_ID)
			{
				return resultList.Where(a => Utils.GetContentFieldValue(CspDataContext, a, Cons.CAMPAIGNS_LOCAL_AUDIENCE_TEXT) == _audience).Distinct().ToList();
			}
			
			return resultList;
		}

		/// <summary>
		/// Gets the category block.
		/// </summary>
		/// <param name="cat">The cat.</param>
		/// <returns></returns>
		protected CategoryBlock GetCategoryBlock(category cat)
		{
			var categoryBlock = new CategoryBlock();
			var categoryContent =
				CspDataContext.contents.SingleOrDefault(
					a =>
					a.content_main.content_categories.Count(b => b.category_Id == cat.categoryId) > 0 &&
					a.content_types_languages_Id == _langId &&
					a.content_main.content_types_Id == CategoryContentTypeId &&
					a.content_main.supplier_Id == SupplierId);
			if (categoryContent != null)
			{
				var catName = GetContentFieldValue(categoryContent, "Content_Title");
				categoryBlock.CategoryName = catName != string.Empty ? catName : cat.categoryText;
			}
			else
			{
				categoryBlock.CategoryName = cat.categoryText;
			}
			categoryBlock.TopicImage = GetCategoryImage(cat);
			categoryBlock.CategoryId = cat.categoryId;
			categoryBlock.CategoryUrl = GetCategoryViewUrl(cat.categoryId);
			return categoryBlock;
		}

		private string GetCategoryImage(category cat)
		{
			var searchFactory = (SimpleSearch)SearchExpressionFactory.CreateSearch();
			searchFactory.Left = SearchExpressionFactory.CreateSearch(CspDataContext.content_types.FirstOrDefault(a => a.content_types_Id == CampaignContentTypeId));
			searchFactory.Operator = SearchOperator.And;
			searchFactory.Right = SearchExpressionFactory.CreateSearch();
			searchFactory.Right.Left = SearchExpressionFactory.CreateSearch(CspDataContext.companies.FirstOrDefault(a => a.companies_Id == SupplierId));
			searchFactory.Right.Operator = SearchOperator.And;
			searchFactory.Right.Right = SearchExpressionFactory.CreateSearch(CspDataContext.languages.FirstOrDefault(a => a.languages_Id == _langId));
			IContentAccessFilter caf = ContentAccessFilterFactory.CreateContentAccessFilter(searchFactory, CspDataContext, cat.categoryId);
            var campaigns = caf.Search().Where(a => a.stage_Id == Keys.CspPublishStageId).OrderBy(a => Utils.GetContentFieldValue(CspDataContext, a, "Status_Featured_Y_N") == "yes").OrderByDescending(a => Utils.GetContentFieldValue(CspDataContext, a, "Status_Sort_Order")).ToList();
            if (_audienceId == Cons.AUDIENCE_BUSINESS_ID || _audienceId == Cons.AUDIENCE_CONSUMER_ID)
            {
                campaigns = campaigns.Where(a => Utils.GetContentFieldValue(CspDataContext, a, Cons.CAMPAIGNS_LOCAL_AUDIENCE_TEXT) == _audience).ToList();
            }
			string img = string.Empty;
            if (campaigns.Any())
            {
                foreach (var campaign in campaigns)
                {
                    img = Utils.GetContentFieldValue(CspDataContext, campaign, "Medium_Rectangle_300x250_Image");
                    if (img != string.Empty)
                        break;
                }
            }
			return img != string.Empty ? img : GetNoImageUrl();
		}

		/// <summary>
		/// Gets the no image URL.
		/// </summary>
		/// <returns></returns>
		private string GetNoImageUrl()
		{
			return ControlPath + "Images/noimage.png";
		}

		/// <summary>
		/// Gets the category view URL.
		/// </summary>
		/// <param name="categoryId">The category id.</param>
		/// <returns></returns>
		private string GetCategoryViewUrl(int categoryId)
		{
			return Globals.NavigateURL(TabId, "category", "mid", ModuleId.ToString(), "langid", Request.Params["langid"], "audid", Request.Params["audid"], "cid", categoryId.ToString());
		}

		/// <summary>
		/// Create/Update Buy Now URL
		/// </summary>
		private void UpdateBuyNowURL()
		{
			Response.Clear();
			Response.ContentType = "text/xml";
			int resultCode = 0;
			Guid contentId = new Guid(Request.Params["contentid"]);
			int contentFieldId = 0;
			try
			{
				contentFieldId = int.Parse(Request.Params["contentfieldid"]);
			}
			catch (Exception)
			{
				contentFieldId = 0;
			}
			
			string buyNowURL = Request.Params["buynowurl"];
			if (contentId == Guid.Empty || contentFieldId <= 0 || String.IsNullOrEmpty(buyNowURL))
				resultCode = 0;
			else
			{
				//Check if existed
				custom_content_field crrBuyNowURL =
					CspDataContext.custom_content_fields.FirstOrDefault(
						a => a.consumer_Id == _cspId && a.content_Id == contentId && a.content_fields_Id == contentFieldId);
				if (crrBuyNowURL != null)
				{
					crrBuyNowURL.value_text = buyNowURL;
				}
				//Not existed, create new
				else
				{
					custom_content_field newBuyNowURL = new custom_content_field
					{
						content_Id = contentId,
						content_fields_Id = contentFieldId,
						consumer_Id = _cspId,
						value_text = buyNowURL
					};
					CspDataContext.custom_content_fields.InsertOnSubmit(newBuyNowURL);
				}
				try
				{
					CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
					resultCode = 1;
				}
				catch (Exception)
				{
					resultCode = -1;
				}
			}
			string strData = "<xmlresult><resultcode>" + resultCode + "</resultcode></xmlresult>";
			Response.Write(strData);
			Response.End();
			HttpContext.Current.ApplicationInstance.CompleteRequest();
		}

		/// <summary>
		/// Gets the download list.
		/// </summary>
		/// <param name="campaignId">The campaign id.</param>
		/// <returns></returns>
		protected string GetDownloadList(Guid campaignId)
		{
			var campaign = CspDataContext.contents.SingleOrDefault(a => a.content_Id == campaignId);
			if (campaign == null)
				return string.Empty;
			var htmlUlTags = "<ul>";
			var htmlLiTags = "";
			var listAsset = GetListAssetByCampaign(campaign);
			if (listAsset.Any())
			{
				foreach (content content in listAsset)
				{
					string contentFileUrl = GetContentFieldValue(content, "Content_File_Url");
					if (String.IsNullOrEmpty(contentFileUrl))
						contentFileUrl = "javascript:void(0); return false;";
					htmlLiTags += "<li style='width: 150px; height: 200px;'>";
					htmlLiTags += "<div class='asset-img center-align'><img height='100px' src='" + GetContentFieldValue(content, "Content_Image_Thumbnail") + "' /></div>";
					htmlLiTags += "<div class='asset-title center-align'>" + GetContentFieldValue(content, "Content_Title") + "</div>";
					htmlLiTags += @"<div class='center-align'><a class='btn-long-link' href='" + contentFileUrl + @"' target='_blank' title='" + GetContentFieldValue(content, "Content_Title") + @"'>
										<span class='btn-long ss-btn'>
											<span class='btn-long-right ss-btn'>
												<span class='btn-long-mid ss-btn'>
													<span class='btn-long-text ss-btn'>" + GetLocalizedText("CategoryView.btnDownload") + @"</span>
												</span>
											</span>
										</span>
									</a></div>";
					htmlLiTags += "</li>";
				}
			}
			htmlUlTags += htmlLiTags + "</ul>";
			return htmlUlTags;
		}

		/// <summary>
		/// Gets the list asset by campaign.
		/// </summary>
		/// <param name="campaign">The campaign.</param>
		/// <returns></returns>
		private IEnumerable<content> GetListAssetByCampaign(content campaign)
		{
			var listAsset = new List<content>();
			var searchFactory = (SimpleSearch)SearchExpressionFactory.CreateSearch();

			searchFactory.Left = SearchExpressionFactory.CreateSearch(CspDataContext.content_types.FirstOrDefault(a => a.content_types_Id == AssetContentTypeId));
			searchFactory.Operator = SearchOperator.And;
			searchFactory.Right = SearchExpressionFactory.CreateSearch();
			searchFactory.Right.Left = SearchExpressionFactory.CreateSearch(CspDataContext.companies.FirstOrDefault(a => a.companies_Id == SupplierId));
			searchFactory.Right.Operator = SearchOperator.And;
			searchFactory.Right.Right = SearchExpressionFactory.CreateSearch(CspDataContext.languages.FirstOrDefault(a => a.languages_Id == int.Parse(Request.QueryString["langid"])));


			IContentAccessFilter caf = ContentAccessFilterFactory.CreateContentAccessFilter(searchFactory, CspDataContext, int.Parse(Request.QueryString["cid"]));
			foreach (var asset in caf.Search())
			{
                if (Utils.GetContentFieldValue(CspDataContext, asset, "Campaign_Unique_Key") == Utils.GetContentFieldValue(CspDataContext, campaign, "CSP_Unique_Key"))
				{
					listAsset.Add(asset);
				}
			}
			return listAsset;
		}

		/// <summary>
		/// Get translated text of category (use content type 5)
		/// </summary>
		/// <param name="fieldName">Field name</param>
		/// <returns>Translated text</returns>
		protected string GetContentTranslatedText(string fieldName)
		{
			string returnText = "";
			if (_content != null)
			{
				content_field contentText =
					CspDataContext.content_fields.FirstOrDefault(
						a => a.content_Id == _content.content_Id && a.content_types_field.fieldname == fieldName);
				if (contentText != null)
					returnText = contentText.value_text;
			}
			return returnText;
		}

		/// <summary>
		/// Send embed code by email
		/// </summary>
		private void SendEmbedCodeByEmail()
		{
			Response.Clear();
			Response.ContentType = "text/xml";
			int resultCode = 0;
			string emailReciever = Request.Params["reciever"];
			string embedCode = Request.Params["embedcode"];
			string resultMsg = "";
            
            // ltu 3-26-2014 added splitter to email addresses
            string[] emails = emailReciever.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
            #region ltu 3-26-2014: updated code to use portal admin email
            string hostEmail = PortalSettings.Email;
            if (string.IsNullOrEmpty(hostEmail))
                hostEmail = Config.GetSetting("Portal" + PortalId + "SupportEmail");
            if (string.IsNullOrEmpty(hostEmail))
                hostEmail = Config.GetSetting("DefaultPortalSupportEmail");
            if (string.IsNullOrEmpty(hostEmail))
                hostEmail = DotNetNuke.Common.Globals.HostSettings["HostEmail"].ToString();
            #endregion

		    foreach (string email in emails)
		    {
                emailReciever = email.Trim();
		        if (IsValidEmailAddress(emailReciever))
		        {
		            GetAudienceInfo();
		            GetCategoryContent();
		            embedCode = Server.HtmlEncode(embedCode);

		            string emailBody = GetContentText("CSP_SendScript_Email_Text").Replace("{encode_post:s}", embedCode);
		            resultMsg = Mail.SendMail(hostEmail, emailReciever, "", GetLocalizedText("CategoryView.EmailEmbedCodeTitle"), emailBody, "", "HTML", "", "", "", "");
		            resultCode = 1;
		            if (!String.IsNullOrEmpty(resultMsg))
		                resultCode = 0;
		        }
		    }
		    string strData = "<xmlresult><resultcode>" + resultCode + "</resultcode><resultmsg>" + resultMsg + "</resultmsg></xmlresult>";
			Response.Write(strData);
			Response.End();
			HttpContext.Current.ApplicationInstance.CompleteRequest();
		}

		/// <summary>
		/// Determines whether [is valid email address] [the specified email address].
		/// </summary>
		/// <param name="emailAddress">The email address.</param>
		/// <returns>
		///   <c>true</c> if [is valid email address] [the specified email address]; otherwise, <c>false</c>.
		/// </returns>
		/// Author: ducuytran
		/// 11/28/2012 - 10:15 AM
		private bool IsValidEmailAddress(string emailAddress)
		{
			string MatchEmailPattern =
				@"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
				+ @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
				+ @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
				+ @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";
			if (!String.IsNullOrEmpty(emailAddress)) return Regex.IsMatch(emailAddress, MatchEmailPattern);
			else return false;
		}

        /// <summary>
        /// Gets the width of the preview popup.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 1/5/2013 - 5:42 PM
        public string GetPreviewPopupWidth()
        {
            if (Settings[Cons.SETTING_PREVIEW_POPUP_WIDTH] != null && Settings[Cons.SETTING_PREVIEW_POPUP_WIDTH].ToString() != string.Empty)
                return Settings[Cons.SETTING_PREVIEW_POPUP_WIDTH] + "px";
            return Cons.DEFAULT_PREVIEWPOPUP_WIDTH + "px";
        }

        /// <summary>
        /// Gets the height of the preview popup.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 1/5/2013 - 5:45 PM
        public string GetPreviewPopupHeight()
        {
            if (Settings[Cons.SETTING_PREVIEW_POPUP_HEIGHT] != null && Settings[Cons.SETTING_PREVIEW_POPUP_HEIGHT].ToString() != string.Empty)
                return Settings[Cons.SETTING_PREVIEW_POPUP_HEIGHT] + "px";
            return Cons.DEFAULT_PREVIEWPOPUP_HEIGHT + "px";
        }
	}
}