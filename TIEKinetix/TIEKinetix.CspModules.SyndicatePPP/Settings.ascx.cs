﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Entities.Modules;

namespace TIEKinetix.CspModules.SyndicatePPP
{
	public partial class Settings : ModuleSettingsBase
	{
		#region Overrides of ModuleSettingsBase

		public override void LoadSettings()
		{
			tbTabName.Text = TabModuleSettings[Cons.SETTING_REDIRECT_TAB_NAME] != null ? TabModuleSettings[Cons.SETTING_REDIRECT_TAB_NAME].ToString() : "";
			tbInstanceName.Text = TabModuleSettings[Cons.SETTING_INSTANCE_NAME] != null ? TabModuleSettings[Cons.SETTING_INSTANCE_NAME].ToString() : "";
			tbWidth.Text = TabModuleSettings[Cons.WIDTH] != null ? TabModuleSettings[Cons.WIDTH].ToString() : "";
			tbHeight.Text = TabModuleSettings[Cons.HEIGHT] != null ? TabModuleSettings[Cons.HEIGHT].ToString() : "";
			tbPPPPreviewParameter.Text = TabModuleSettings[Cons.PPP_PREVIEW_PARAMETERS] != null ? TabModuleSettings[Cons.PPP_PREVIEW_PARAMETERS].ToString() : "";
            tbCobrandHtmlUrl.Text = TabModuleSettings[Cons.SETTING_COBRANDHTMLURL] != null ? TabModuleSettings[Cons.SETTING_COBRANDHTMLURL].ToString() : Cons.SETTING_DEFAULT_COBRANDHTMLURL
                ;
		}

		public override void UpdateSettings()
		{
			var objModules = new ModuleController();
			objModules.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_REDIRECT_TAB_NAME, tbTabName.Text);
			objModules.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_INSTANCE_NAME, tbInstanceName.Text);
			objModules.UpdateTabModuleSetting(TabModuleId, Cons.WIDTH, tbWidth.Text);
			objModules.UpdateTabModuleSetting(TabModuleId, Cons.HEIGHT, tbHeight.Text);
			objModules.UpdateTabModuleSetting(TabModuleId, Cons.PPP_PREVIEW_PARAMETERS, tbPPPPreviewParameter.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_COBRANDHTMLURL, tbCobrandHtmlUrl.Text);
			ModuleController.SynchronizeModule(ModuleId);
		}

		#endregion
	}
}