﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainView.ascx.cs" Inherits="TIEKinetix.CspModules.SyndicatePPP.MainView" %>
<asp:Panel runat="server" ID="panelIntro" CssClass="panelIntro">
    <%--Description Block--%>
	<div id="description_section">
		<div id="desc_text">
			<h1 id="desc_title"><%= GetLocalizedText("Description.Title") %></h1>
			<div id="desc_detail">
				<p>
					<%= GetLocalizedText("Description.Detail") %>
				</p>
			</div>
			<div id="desc_controls">
				<a id="btnPreviewShowcase" class="btn-long-link" href="javascript: void(0)">
					<span class="btn-long ss-btn">
						<span class="btn-long-right ss-btn">
							<span class="btn-long-mid ss-btn">
								<span class="btn-long-text ss-btn"><%= GetLocalizedText("Button.Preview") %></span>
							</span>
						</span>
					</span>
				</a>
				<a id="btnPickupCode" class="btn-long-link" href="javascript: void(0)">
					<span class="btn-long ss-btn">
						<span class="btn-long-right ss-btn">
							<span class="btn-long-mid ss-btn">
								<span class="btn-long-text ss-btn"><%= GetLocalizedText("Button.PickUpCode") %></span>
							</span>
						</span>
					</span>
				</a>
			</div>
		</div>
		<div id="desc_figure">
			<img width="255px" src="<%= GetLocalizedText("PPP.Image") %>" alt=""/>
		</div>
		<div class="clear"></div>
			
	</div>

	<%--Popup Windows--%>
	<div id="wdCodePickup" title="<%= GetLocalizedText("WindowTitle.PickupYourCode") %>">
		<div class="wdContent">
			<div id="PnlContainer" class="hide">
				<div id="pShowcase">
				</div>
				<table width="640" align="center">
					<tbody>
					<tr>
						<td colspan="2" bgcolor="#EDE9EA" style="font: 18px/130% Arial, Helvetica, sans-serif;
							padding: 5px;">
							<b><%= GetLocalizedText("PickupYourCode.txtSetupSteps") %></b>
						</td>
					</tr>
					<tr>
						<td width="35" align="center" style="padding-top: 6px;">
							<img src="<%= ControlPath %>Images/stepOne.png" width="24" height="25" alt="" />
						</td>
						<td style="font: 18px/130% Arial, Helvetica, sans-serif; padding-top: 4px;">
							<%= GetLocalizedText("PickupYourCode.txtStep1") %>
						</td>
					</tr>
					<tr>
						<td align="center">
							&nbsp;
						</td>
						<td valign="top" style="font: 11px/130% Arial, Helvetica, sans-serif;">
							<%= GetLocalizedText("PickupYourCode.txtStep1Desc") %>
						</td>
					</tr>
					<tr>
						<td align="center" style="padding-top: 6px;">
							<img src="<%= ControlPath %>Images/stepTwo.png" width="24" height="25" alt="" />
						</td>
						<td style="font: 18px/130% Arial, Helvetica, sans-serif; padding-top: 4px;">
							<%= GetLocalizedText("PickupYourCode.txtStep2") %>
						</td>
					</tr>
					<tr>
						<td align="center">
							&nbsp;
						</td>
						<td valign="top" style="font: 11px/130% Arial, Helvetica, sans-serif;">
							<%= GetLocalizedText("PickupYourCode.txtStep2Desc") %>
						</td>
					</tr>
					<tr>
						<td align="center">
							&nbsp;
						</td>
						<td>
							<div class="title">
								<%= GetLocalizedText("PickupYourCode.txtShowcaseTitle") %></div>
							<div class="codeBlock">
								<p style="font: 12px/130% 'Courier New', Courier, monospace; color: #666666;">
									<%= GetLocalizedText("PickupYourCode.txtShowcaseCodeTitle") %></p>
								<p style="font: 12px/130% 'Courier New', Courier, monospace; color: #0000FF;">
									<textarea name="ShowcaseScript" rows="4" cols="20" readonly="readonly" id="ShowcaseScript" style="height:129px;width:99%;color: #990000; border: none;"><%= GetLocalizedText("PickupYourCode.TextareaContent").Replace("{scriptlink}", GetSyndicationScript())%></textarea>
								</p>
							</div>
						</td>
					</tr>
					<tr>
					</tr>
					<tr>
						<td align="center" style="padding-top: 6px;">
							<img src="<%= ControlPath %>Images/stepThree.png" width="24" height="25"  alt=""/>
						</td>
						<td style="font: 18px/130% Arial, Helvetica, sans-serif; padding-top: 4px;">
							<%= GetLocalizedText("PickupYourCode.txtStep3") %>
						</td>
					</tr>
					<tr>
						<td align="center">
							&nbsp;
						</td>
						<td valign="top" style="font: 11px/130% Arial, Helvetica, sans-serif;">
							<%= GetLocalizedText("PickupYourCode.txtStep3Desc") %>
						</td>
					</tr>
					<tr>
						<td align="center">
						</td>
						<td style="font: 0px/0%;">
						</td>
					</tr>
					<tr>
						<td align="center">
							<img src="<%= ControlPath %>Images/icon_info.png" width="16" height="16"  alt="" />
						</td>
						<td style="font: 11px/130% Arial, Helvetica, sans-serif; padding-top: 10px;">
							<%= GetLocalizedText("PickupYourCode.txtAttention") %>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<div id="code-actions">
								<div id="generate_embed_code_email">
									<input type="text" id="email_embed_code" value="<%= UserInfo.Email %>"/>
									<input type="submit" id="submit_email_embed_code" value="<%= GetLocalizedText("PickupYourCode.btnEmailScript")%>" label="<%= GetLocalizedText("PickupYourCode.msgInvalidEmailAddress") %>" />
									<input type="submit" id="copy-embed-code" href="javascript: void(0)" rel="<%= ControlPath %>Js/ZeroClipboard.swf" value="<%= GetLocalizedText("PickupYourCode.btnCopyCode")%>" />
									<span id="code-copied"><%= GetLocalizedText("PickupYourCode.CodeCopied") %></span>
								</div>
								<p id="email_embed_code_result"></p>
								<p class="hide" id="email_embed_code_succeed"><%= GetLocalizedText("PickupYourCode.msgEmailSentSucceed") %></p>
								<p class="hide" id="email_embed_code_failed"><%= GetLocalizedText("PickupYourCode.msgEmailSentFailed") %></p>
							</div>
						</td>
					</tr>
				</tbody></table>
			</div>
		</div>
	</div>
				
	<div id="wdPreviewShowCase" title="<%= GetLocalizedText("WindowTitle.PreviewShowcase") %>"  >
		<div class="wdContent">
			<!--<script type="text/javascript" src="<%= GetSyndicationScript() %>"></script>-->
            <iframe id="_opt1" scrolling="no" frameborder="0" src='<%= GetPppPreviewIframeLink() %>' width="980" height="600"></iframe>
		</div>
	</div>
	<%--EOF Popup Windows--%>
</asp:Panel>
<asp:Panel ID="panelError" runat="server" Visible="false" Enabled="false">
<p>Invalid configuration or host/admin account is currently being used.</p>
</asp:Panel>
<script type="text/javascript">
	//Init copy generated code to clipboard
	var isZClipInit = 1;
	function initEmbedCodeCopier() {
		if (isZClipInit <= 2) {
			var swfFilePath = $('#copy-embed-code').attr('rel');

			$('#copy-embed-code').zclip({
				path: swfFilePath,
				copy: function () { return $('#ShowcaseScript').val(); },
				beforeCopy: function () {
					$('#copy-embed-code').hide();
					$('#code-copied').show();
					setTimeout(function () { $('#copy-embed-code').show(); $('#code-copied').hide(); }, 3000);
				},
				afterCopy: function () {
					/*$('#code-copied').show().delay(5000).fadeOut('fast', function () {
					$('#copy-embed-code').show();
					});*/
					//$('#copy-embed-code').show();
				}
			});
			isZClipInit++;
		}
	}
	//Init Submit Embed Code for sending email
	function initEmbedCodeEmailSubmit() {
		$('#submit_email_embed_code').click(function (e) {
			e.preventDefault();
			var thisEl = $(this);
			var emailReciver = $('#email_embed_code').val();
			if (!validateEmail(emailReciver)) {
				$('#email_embed_code_result').html(thisEl.attr('label'));
				return;
			}
			
			var crrUpdateControl = "PnlContainer";
			
			$.ajax({
				type: 'POST',
				url: '<%= GetModuleURL() %>',
				data: { 'ajaxaction': 'emailembedcode', reciever: emailReciver },
				dataType: 'xml',
				success: function (responseData) {
					var resultCode = $(responseData).find('resultcode').text();
					resultCode = parseInt(resultCode);
					if (resultCode == 1) {
						$('#email_embed_code_result').html($('#email_embed_code_succeed').html());
					} else if (resultCode == 3) {
						$('#email_embed_code_result').html("Empty email content");
					}
					else {
						$('#email_embed_code_result').html($('#email_embed_code_failed').html() + '<p style="color: #FF0000">' + $(responseData).find('resultmsg').text() + '</p>');
					}					
				},
				error: function (err) {
				}
			});
		});
	}

	//Validate Email
	function validateEmail(email) {
		var re = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		return re.test(email);
	}



	jQuery(function ($) {
		//Code Pick Up Window
		var wdPickYourCodeInit = $('#wdCodePickup').kendoWindow({
			actions: ["Close"],
			draggable: true,
			height: "550px",
			width: "690px",
			modal: true,
			resizable: false,
			visible: false,
			title: $('#wdCodePickup').attr('title')
		}).data("kendoWindow");

		$('#btnPickupCode').click(function () {
			var wdPickYourCode = $('#wdCodePickup').data("kendoWindow");
			$('#PnlContainer').show();
			wdPickYourCode.center();
			wdPickYourCode.open();
			initEmbedCodeCopier();
			return false;
		});

		//Preview Showcase Window
		//wdPreviewShowcase
		var wdPreviewShowCaseInit = $('#wdPreviewShowCase').kendoWindow({
			actions: ["Close"],
			draggable: true,
			height: "<%= GetPreviewPopupHeight() %>",
			width: "<%= GetPreviewPopupWidth() %>",
			modal: true,
			resizable: false,
			visible: false,
			title: $('#wdPreviewShowCase').attr('title')
		}).data("kendoWindow");

		$('#btnPreviewShowcase').click(function () {
			var wdPreviewShowCase = $('#wdPreviewShowCase').data("kendoWindow");
			$('#wdPreviewShowCase').show();
			wdPreviewShowCase.center();
			wdPreviewShowCase.open();
			return false;
		});

		initEmbedCodeCopier();
		initEmbedCodeEmailSubmit();
	});
</script>