﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Settings.ascx.cs" Inherits="TIEKinetix.CspModules.SyndicatePPP.Settings" %>
<table>
	<tr>
		<td>Tab Name to redirect to:</td>
		<td><asp:TextBox ID="tbTabName" runat="server"></asp:TextBox></td>
	</tr>
	<tr>
		<td>Instance Name:</td>
		<td><asp:TextBox ID="tbInstanceName" runat="server"></asp:TextBox></td>
	</tr>
	<tr>
		<td>Preview Width:</td>
		<td><asp:TextBox ID="tbWidth" runat="server"></asp:TextBox></td>
	</tr>
	<tr>
		<td>Preview Height:</td>
		<td><asp:TextBox ID="tbHeight" runat="server"></asp:TextBox></td>
	</tr>
	<tr>
		<td>PPP Preview Parameter:</td>
		<td><asp:TextBox ID="tbPPPPreviewParameter" runat="server"></asp:TextBox></td>
	</tr>
	<tr>
		<td>CobrandHtml Url:</td>
		<td><asp:TextBox ID="tbCobrandHtmlUrl" runat="server"></asp:TextBox></td>
	</tr>
</table>
