﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DesktopModules.CSPModules.CspCommons;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Data;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Localization;
using DotNetNuke.Services.Log.EventLog;
using DotNetNuke.Services.Mail;

namespace TIEKinetix.CspModules.SyndicatePPP
{
	public class Cons
	{
		public static string SETTING_INSTANCE_NAME = "SETTING_INSTANCE_NAME";
		public static string WIDTH = "IFRAME_WIDTH";
		public static string HEIGHT = "IFRAME_HEIGHT";
	    public static string SETTING_COBRANDHTMLURL = "SETTING_COBRANDHTMLURL";
	    public static string SETTING_DEFAULT_COBRANDHTMLURL = "http://209.134.51.131:9995/CobrandHTML2/";
	    public const string PPP_PREVIEW_PARAMETERS = "PPP_PREVIEW_PARAMETERS";
		public const string SETTING_REDIRECT_TAB_NAME = "SETTING_REDIRECT_TAB_NAME";
	}

	public partial class MainView : PortalModuleBase
	{
		private CspDataLayerDataContext _db;
		private int _integrationKey;
		private CompaniesConsumer _companiesConsumer;
		private string _instanceName = "", _previewParameter = "";
		private Company _consumer;

		protected void Page_Init(object sender, EventArgs e)
		{
			if (!String.IsNullOrEmpty(Request.Params["ajaxaction"]))
			{
				if (Request.Params["ajaxaction"] == "emailembedcode")
				{
					_integrationKey = -1;
					if (!int.TryParse(UserInfo.Profile.ProfileProperties[Globals.IntegrationKey].PropertyValue, out _integrationKey))
					{
						return;
					}
					if (Settings[Cons.SETTING_INSTANCE_NAME] != null && Settings[Cons.SETTING_INSTANCE_NAME].ToString() != string.Empty)
					{
						_instanceName = Settings[Cons.SETTING_INSTANCE_NAME].ToString();
					}
					_db = new CspDataLayerDataContext(Config.GetConnectionString("CspPortal" + PortalId));
					_consumer = _db.Companies.FirstOrDefault(a => a.companies_Id == _integrationKey);
					if (_consumer != null)
					{
                        //ltu 10-11-2013: add a better way to get consumer record
					    _companiesConsumer = GetCompanyConsumerUsingChannel(_consumer);
						SendEmbedCodeByEmail();
					}
				}
				return;
			}

			if (UserInfo.Profile.ProfileProperties[Globals.IntegrationKey] == null)
			{
				panelIntro.Enabled = panelIntro.Visible =  false;
				//Controls.Add(new Literal
				//                {
				//                    Text = "Invalid configuration or host/admin account is currently being used."
				//                });
				panelError.Visible = panelError.Enabled = true;
				return;
				
			}

			// Force IE8 browser render as IE 7 or IE 9 mode
			var keyword = new HtmlMeta { Content = "IE=7,IE=9", HttpEquiv = "X-UA-Compatible" };
			Page.Header.Controls.Add(keyword);

			ModuleConfiguration.ModuleTitle = GetLocalizedText("Label.ModuleTitle");

			//Init csp datacontext
			_db = new CspDataLayerDataContext(Config.GetConnectionString("CspPortal" + PortalId));

			_integrationKey = -1;
			if (!int.TryParse(UserInfo.Profile.ProfileProperties[Globals.IntegrationKey].PropertyValue, out _integrationKey))
			{
				GotoRedirectTabId(string.Empty);
				return;
			}

			if (Settings[Cons.SETTING_INSTANCE_NAME] != null && Settings[Cons.SETTING_INSTANCE_NAME].ToString() != string.Empty)
			{
				_instanceName = Settings[Cons.SETTING_INSTANCE_NAME].ToString();
			}

			if (Settings[Cons.PPP_PREVIEW_PARAMETERS] != null && Settings[Cons.PPP_PREVIEW_PARAMETERS].ToString() != string.Empty)
			{
				_previewParameter = Settings[Cons.PPP_PREVIEW_PARAMETERS].ToString();
			}

			_consumer = _db.Companies.Single(a => a.companies_Id == _integrationKey);
            _companiesConsumer = GetCompanyConsumerUsingChannel(_consumer);
		}

        /// <summary>
        /// Using channel table to get consumers
        /// - if there are multiple channel, use the one that matches the portal
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
	    private CompaniesConsumer GetCompanyConsumerUsingChannel(Company company)
        {
            var logController = new DotNetNuke.Services.Log.EventLog.EventLogController();
            logController.AddLog("GetCompanyConsumerUsingChannel", string.Format("There are  {0} consumer records for {1} ", company.CompaniesConsumers.Count, company.companyname),
                PortalSettings, UserId, EventLogController.EventLogType.ADMIN_ALERT);
            if (!company.CompaniesConsumers.Any())
                return null;
	        if (company.CompaniesConsumers.Count == 1)
	            return company.CompaniesConsumers.First();
            string externalIdentifier = GetPortalExternalIdentifier();
            Channel channel = _db.Channels.FirstOrDefault(a => a.ExternalIdentifier == externalIdentifier);
            logController.AddLog("GetCompanyConsumerUsingChannel", string.Format("External Id: {0}. {1}",externalIdentifier, channel==null?"Can't find channel":"Found channel=" + channel.BaseDomain),
                PortalSettings, UserId, EventLogController.EventLogType.ADMIN_ALERT);

            if (channel == null)
                return company.CompaniesConsumers.First();
            return company.CompaniesConsumers.Any(cc => cc.base_domain.Contains(channel.BaseDomain)) ? 
                company.CompaniesConsumers.First(cc => cc.base_domain.Contains(channel.BaseDomain)) : 
                company.CompaniesConsumers.First();
        }

        /// <summary>
        /// Get portal external identifier used to identify portal channel.
        /// </summary>
        /// <returns></returns>
        protected string GetPortalExternalIdentifier()
        {
            string s = Config.GetSetting("Portal" + PortalId + "ExternalIdentifier");
            return !string.IsNullOrEmpty(s) ? s : Request.Url.Host;
        }


	    protected void Page_Load(object sender, EventArgs e)
		{
			Page.ClientScript.RegisterClientScriptInclude("zclip", ControlPath + "Js/jquery.zclip.min.js");
		}

		/// <summary>
		/// Get localized text for the current portal language
		/// </summary>
		/// <param name="key">Resource Key</param>
		/// <returns>Localized text</returns>
		protected string GetLocalizedText(string key)
		{
			return Localization.GetString(key, LocalResourceFile);
		}

        /// <summary>
        /// Get localized text for the current portal language
        /// </summary>
        /// <param name="key">Resource Key</param>
        /// <returns>Localized text</returns>
        protected string GetLocalizedTextWithReplaceCode(string key)
        {
            var s = Localization.GetString(key, LocalResourceFile);
            if (_companiesConsumer == null) return s;
            return s.Replace("{consumer:fbasedomain}", _companiesConsumer.base_domain).Replace("{consumer:FConsumerID}", _consumer.companies_Id.ToString(CultureInfo.InvariantCulture));
        }

		/// <summary>
		/// Gets the syndication script.
		/// </summary>
		/// <returns></returns>
		public string GetSyndicationScript()
		{
			var baseDomain = GetBaseDomain();
			return string.Format("http://{0}/Csp/?mfrname={1}&t=ppp&p=15", baseDomain, _instanceName);
		}

		/// <summary>
		/// Gets the base domain.
		/// </summary>
		/// <returns></returns>
		protected string GetBaseDomain()
		{
			//var companyConsumer = _db.CompaniesConsumers.FirstOrDefault(a => a.companies_Id == _integrationKey && a.active == true);
			//return companyConsumer != null ? "" + companyConsumer.base_domain : string.Empty;
			return _companiesConsumer != null ? _companiesConsumer.base_domain : string.Empty;
		}
		/// <summary>
		/// Gotoes the redirect tab id.
		/// </summary>
		/// <param name="parameters">The parameters.</param>
		private void GotoRedirectTabId(string parameters)
		{
			var dataProvider = DataProvider.Instance();
			if (Settings[Cons.SETTING_REDIRECT_TAB_NAME] != null && Settings[Cons.SETTING_REDIRECT_TAB_NAME].ToString() != string.Empty)
			{
				var result = dataProvider.ExecuteSQL(string.Format("SELECT TabId FROM Tabs WHERE TabName = '{0}' and PortalID = '{1}'", Settings[Cons.SETTING_REDIRECT_TAB_NAME], PortalId));
				int tabId = 0;
				while (result.Read())
				{
					tabId = int.Parse(result["TabId"].ToString());
					break;
				}
				if (tabId != 0)
				{
					string theUrl = DotNetNuke.Common.Globals.NavigateURL(tabId);
					if (!String.IsNullOrEmpty(parameters))
						theUrl = DotNetNuke.Common.Globals.NavigateURL(tabId, "", parameters);
					Response.Redirect(theUrl);
				}
			}
		}

		protected string GetPppPreviewIframeLink()
		{
			return string.Format("http://{0}{1}/{2}",
			                     string.IsNullOrEmpty(_instanceName) ? "" : "p" + _instanceName + "-",
			                     GetBaseDomain(),
			                     string.IsNullOrEmpty(_previewParameter) ? "" : _previewParameter.Replace("[ticks]", DateTime.Now.Ticks.ToString()));
		}

		protected string GetModuleURL()
		{
			return DotNetNuke.Common.Globals.NavigateURL(TabId, "", string.Format("mid={0}", ModuleId));
		}

		protected string GetPreviewPopupHeight()
		{
			string temp = "600px";
			if (Settings[Cons.HEIGHT] != null && Settings[Cons.HEIGHT].ToString() != string.Empty)
			{
				temp= Settings[Cons.HEIGHT].ToString();
			}
			return temp;
		}

		protected string GetPreviewPopupWidth()
		{
			string temp = "600px";
			if (Settings[Cons.WIDTH] != null && Settings[Cons.WIDTH].ToString() != string.Empty)
			{
				temp = Settings[Cons.WIDTH].ToString();
			}
			return temp;
		}

		private void SendEmbedCodeByEmail()
		{
			Response.Clear();
			Response.ContentType = "text/xml";
			int resultCode = 0;
			string emailReciever = Request.Params["reciever"];
			string embedCode = GetLocalizedText("PickupYourCode.TextareaContent").Replace("{scriptlink}", GetSyndicationScript());
			string resultMsg = "";
			if (IsValidEmailAddress(emailReciever))
			{

				string hostEmail = PortalSettings.Email;
				if (string.IsNullOrEmpty(hostEmail))
					hostEmail = Config.GetSetting("Portal" + PortalId + "SupportEmail");
				if (string.IsNullOrEmpty(hostEmail))
					hostEmail = Config.GetSetting("DefaultPortalSupportEmail");
				if (string.IsNullOrEmpty(hostEmail))
					hostEmail = DotNetNuke.Common.Globals.HostSettings["HostEmail"].ToString();

				string emailBody = GetLocalizedText("PickupYourCode.EmailScriptContent").Replace("{script_code}", embedCode);
				resultMsg = Mail.SendMail(hostEmail, emailReciever, "", GetLocalizedText("PickupYourCode.EmailEmbedCodeTitle"), emailBody, "", "HTML", "", "", "", "");
				resultCode = 1;
				if (!String.IsNullOrEmpty(resultMsg))
					resultCode = 0;
			}
			string strData = "<xmlresult><resultcode>" + resultCode + "</resultcode><resultmsg>" + resultMsg + "</resultmsg></xmlresult>";
			Response.Write(strData);
			Response.End();
			HttpContext.Current.ApplicationInstance.CompleteRequest();
		}

		private static bool IsValidEmailAddress(string emailAddress)
		{
			string MatchEmailPattern =
				@"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
				+ @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
				+ @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
				+ @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";
			if (!String.IsNullOrEmpty(emailAddress)) return Regex.IsMatch(emailAddress, MatchEmailPattern);
			else return false;
		}

	    protected string GetDownloadUrl()
	    {
	        var url = GetLocalizedText("Button.DownloadUrl");
	        if (!string.IsNullOrEmpty(url))
	            url = HttpUtility.UrlEncode(url);
	        string prefix = string.Empty;
	        if (Settings.ContainsKey(Cons.SETTING_COBRANDHTMLURL))
	            prefix = Settings[Cons.SETTING_COBRANDHTMLURL].ToString();
	        if (string.IsNullOrEmpty(prefix))
	            prefix = Cons.SETTING_DEFAULT_COBRANDHTMLURL;

	        if (prefix.EndsWith("/"))
	            prefix = prefix.Substring(0, prefix.Length - 1);

	        prefix += "/" + _instanceName + "/" + _integrationKey + "/" + url;
	        return prefix;
	    }
	}
}