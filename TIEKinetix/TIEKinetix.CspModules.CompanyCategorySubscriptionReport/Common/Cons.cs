﻿// ***********************************************************************
// Assembly         : TIEKinetix.CspModules.CompanyCategorySubscriptionReport
// Author           : VU DINH
// Created          : 06-11-2013
//
// Last Modified By : VU DINH
// Last Modified On : 06-11-2013
// ***********************************************************************
// <copyright file="Cons.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace TIEKinetix.CspModules.CompanyCategorySubscriptionReport.Common
{
    /// <summary>
    /// Class Cons
    /// </summary>
    public class Cons
    {
        public const string CONSUMERTYPE_PARAMETER_NAME = "ConsumerType";
    }
}