﻿// ***********************************************************************
// Assembly         : TIEKinetix.CspModules.CompanyCategorySubscriptionReport
// Author           : VU DINH
// Created          : 06-11-2013
//
// Last Modified By : VU DINH
// Last Modified On : 06-11-2013
// ***********************************************************************
// <copyright file="DnnModuleBase.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Web.UI;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Localization;

namespace TIEKinetix.CspModules.CompanyCategorySubscriptionReport.Common
{
    /// <summary>
    /// Class DnnModuleBase
    /// </summary>
    public class DnnModuleBase : PortalModuleBase
    {
        private string _localResourceFile;
        internal CspDataContext CspDataContext;
        internal CspUtils CspUtils;
        /// <summary>
        /// Handles the Init event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
         protected virtual void Page_Init(object sender, EventArgs e)
         {
             //Init Local Resource file
             _localResourceFile = Utils.GetCommonResourceFile("CompanyCategorySubscriptionReport", LocalResourceFile);
             //Init module title
             ModuleConfiguration.ModuleTitle = GetLocalizedText("Label.ModuleTitle");
             //Add google font for fancy layout
             Page.Header.Controls.Add(new LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"http://fonts.googleapis.com/css?family=Exo\" />"));

             //Init Data
             CspDataContext = new CspDataContext(Utils.GetConnectionString(PortalId));
             CspUtils = new CspUtils(CspDataContext);
         }

         /// <summary>
         /// Gets the portal external identifier.
         /// </summary>
         /// <returns>System.String.</returns>
         protected string GetPortalExternalIdentifier()
         {
             string s = Utils.GetSetting(PortalId, "ExternalIdentifier");
             return !string.IsNullOrEmpty(s) ? s : Request.Url.Host;
         }

         /// <summary>
         /// Gets the localized text.
         /// </summary>
         /// <param name="key">The key.</param>
         /// <returns>System.String.</returns>
         public string GetLocalizedText(string key)
         {
             return Localization.GetString(key, _localResourceFile);
         }
    }
}