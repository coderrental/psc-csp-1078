﻿// ***********************************************************************
// Assembly         : TIEKinetix.CspModules.CompanyCategorySubscriptionReport
// Author           : VU DINH
// Created          : 06-11-2013
//
// Last Modified By : VU DINH
// Last Modified On : 06-11-2013
// ***********************************************************************
// <copyright file="MainView.ascx.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using CR.ContentObjectLibrary.Data;
using TIEKinetix.CspModules.CompanyCategorySubscriptionReport.Common;
using TIEKinetix.CspModules.CompanyCategorySubscriptionReport.Entity;
using DocumentFormat.OpenXml.Extensions;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace TIEKinetix.CspModules.CompanyCategorySubscriptionReport
{
    /// <summary>
    /// Class MainView
    /// </summary>
    public partial class MainView : DnnModuleBase
    {
        private List<company> _listCompanies;
        private List<category> _listCategories; 
        private List<RowLayout> _listRowLayout;
        private Channel _channel;
        private List<CompanyCategoryPair> _subscriptionCollection;
        /// <summary>
        /// Handles the Init event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected override void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
            _channel = CspDataContext.Channels.FirstOrDefault(a => a.ExternalIdentifier == GetPortalExternalIdentifier());
            btnExport.ImageUrl = ControlPath + "images/download.png";
            btnExport.ToolTip = GetLocalizedText("Label.Export");
            _subscriptionCollection = new List<CompanyCategoryPair>();
            InitDataResource();
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Inits the data resource.
        /// </summary>
        private void InitDataResource()
        {
            //Init list companies with parameter "ConsumerType" is P or null (not T)
            InitListCompanies();

            //Init list of subscriable categories
            InitListCategories();

           _listRowLayout = new List<RowLayout>();
           /*
                       //init first row - or header columns
                       var heading = @"
               <tr>
                   <td></td>
                   {0}
               </tr>
           ";
                       heading = string.Format(heading, GetListCategoriesHeader());
            * 
                       _listRowLayout.Add(new RowLayout
                           {
                    
                               Layout = heading
                           });
                       */
            //render each row
           foreach (var company in _listCompanies)
           {
               var strBuilder = new StringBuilder();
               strBuilder.Append(
               @"
    <tr class='{0}'>
        <td class='first-column' title='{1}'>{1}</td>
");
               foreach (var category in _listCategories)
               {
                   var isSubscription = CheckCompanySubscription(company, category);
                   strBuilder.Append(isSubscription
                                         ? "<td class='value-column check'>X</td>"
                                         : "<td class='value-column'></td>");
               }
               strBuilder.Append("</tr>");
               var html = strBuilder.ToString();
               html = string.Format(html, _listCompanies.IndexOf(company) % 2 == 0 ? "row_odd" : "row_even", company.companyname, company.companyname.Length > 20 ? company.companyname.Substring(0, 20) + "..." : company.companyname);
               _listRowLayout.Add(new RowLayout
                   {
                       Layout = html
                   });
           }


            rlvListData.DataSource = _listRowLayout;
            rlvListData.DataBind();

        }

        /// <summary>
        /// Checks the company subscription.
        /// </summary>
        /// <param name="company">The company.</param>
        /// <param name="category">The category.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
        private bool CheckCompanySubscription(company company, category category)
        {
            //get current channel
            
            if (_channel != null)
            {
                var theme = CspDataContext.companies_themes.FirstOrDefault(a => a.companies_Id == company.companies_Id);
                if (theme != null)
                {
                    var companyConsumer = CspDataContext.consumers_themes.FirstOrDefault(a => a.consumer_Id == company.companies_Id && a.themes_Id == theme.themes_Id && a.supplier_Id == _channel.CompanyId);
                    if (companyConsumer != null)
                    {
                        _subscriptionCollection.Add(new CompanyCategoryPair
                            {
                                CategoryId = category.categoryId,
                                CompanyId = company.companies_Id,
                                IsSubscription = true
                            });
                        return true;
                    }
                        
                }
                    
            }
            _subscriptionCollection.Add(new CompanyCategoryPair
            {
                CategoryId = category.categoryId,
                CompanyId = company.companies_Id,
                IsSubscription = false
            });
            return false;
        }

        /// <summary>
        /// Inits the list companies.
        /// </summary>
        private void InitListCompanies()
        {
            _listCompanies = new List<company>();
            foreach (var company in CspDataContext.companies.ToList() )
            {
                var consumerType = CspUtils.GetCompanyParameter(company.companies_Id, Cons.CONSUMERTYPE_PARAMETER_NAME);
                if (string.IsNullOrEmpty(consumerType) || consumerType == "P")
                {
                    _listCompanies.Add(company);    
                }
            }    
        }

        /// <summary>
        /// Inits the list categories.
        /// </summary>
        private void InitListCategories()
        {
            if (_listCategories == null)
            {
                _listCategories = new List<category>();
                _listCategories = CspDataContext.categories.OrderBy(a => a.categoryText).Where(a => a.active.HasValue && a.active.Value && a.IsSubscribable.HasValue && a.IsSubscribable.Value).ToList();     
            }
        }

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <returns>DataTable.</returns>
        private DataTable GetReportData()
        {
            var table = new DataTable();
            //Setup first row
            var firstRow = table.NewRow();
            var columnNames = new List<string> {"Company Id", "Company Name"};
            columnNames.AddRange(_listCategories.Select(category => category.categoryText + " (" + category.categoryId + ")"));

            foreach (string columnName in columnNames)
            {
                table.Columns.Add(columnName);
                firstRow[columnName] = columnName;
            }

            //Add first row
            table.Rows.Add(firstRow);

            foreach (var company in _listCompanies)
            {
                var row = table.NewRow();
                row["Company Id"] = company.companies_Id;
                row["Company Name"] = company.companyname;
                foreach (var category in _listCategories)
                {
                    if (_subscriptionCollection.Any(a => a.CategoryId == category.categoryId && a.CompanyId == company.companies_Id && a.IsSubscription))
                        row[category.categoryText + " (" + category.categoryId + ")"] = "x";
                    else
                        row[category.categoryText + " (" + category.categoryId + ")"] = string.Empty;
                }
                table.Rows.Add(row);
            }

            return table;
        }

        #region [ Get layout ]
        /// <summary>
        /// Gets the list categories header.
        /// </summary>
        /// <returns>System.String.</returns>
        public string GetListCategoriesHeader()
        {
            if (_listCategories == null)
            {
                InitListCategories();
            }
            var strBuilder = new StringBuilder();
            if (_listCategories != null)
                foreach (var category in _listCategories)
                {
                    strBuilder.Append(string.Format("<td class='head-category' title='{0}'>{1}</td>", category.categoryText, category.categoryText.Length > 30 ? category.categoryText.Substring(0, 30) + "(" + category.categoryId + ")" + "..." : category.categoryText + "(" + category.categoryId + ")"));
                }
            return strBuilder.ToString();
        }
        #endregion

        #region [ Event handlers ]
        /// <summary>
        /// Handles the OnClick event of the btnExport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ImageClickEventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected void btnExport_OnClick(object sender, ImageClickEventArgs e)
        {
            //Create excel stream
            MemoryStream stream = SpreadsheetReader.Create();
            // Open the document for editing.
            using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(stream, true))
            {
                var sheets = spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>();
                Sheet sheet = sheets.Elements<Sheet>().Single(a => a.Name == "Sheet" + 1);
                sheet.Name = "CompanySubscription";
                spreadSheet.WorkbookPart.Workbook.Save();


                var worksheetPart = SpreadsheetReader.GetWorksheetPartByName(spreadSheet, "CompanySubscription");
                var writer = new WorksheetWriter(spreadSheet, worksheetPart);
                //Write data to sheet. Col name at A1, data from A2
                writer.PasteDataTable(GetReportData(), "A1");
            }


            // Write to response stream
            Response.Clear();
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", "CompanySubscriptionExport.xlsx"));
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            stream.WriteTo(Response.OutputStream);
            Response.End();
        }
        #endregion
    }
}