﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainView.ascx.cs" Inherits="TIEKinetix.CspModules.CompanyCategorySubscriptionReport.MainView" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadAjaxLoadingPanel runat="server" CssClass="loading-panel" ID="AjaxLoading"></telerik:RadAjaxLoadingPanel>
<%--<telerik:RadAjaxPanel runat="server" ID="ajaxPanel" LoadingPanelID="AjaxLoading">--%>
    <asp:Panel runat="server" ID="pnWrap" CssClass="module_wrap">
        <div class="module_heading"><%= GetLocalizedText("Label.ModuleTitle") %></div>  
        <div class="module_export"><asp:ImageButton runat="server" ID="btnExport" OnClick="btnExport_OnClick"></asp:ImageButton></div>
        <div class="clear"></div>
            <div class="table_wrap">
                 <table class="module_table_main">
                     <tr>
                         <td></td>
                         <%= GetListCategoriesHeader() %>
                     </tr>  
                    <telerik:RadListView ID="rlvListData" runat="server" ItemPlaceholderID="DatarowHolder" AllowPaging="True" PageSize="20">
                        <LayoutTemplate>
                             <asp:PlaceHolder runat="server" ID="DatarowHolder"></asp:PlaceHolder>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <%# Eval("Layout") %>
                            </tr>
                        </ItemTemplate>
                    </telerik:RadListView>
                </table>
             </div>
            <div id="navigation_pager">
			        <telerik:RadDataPager ID="RadDataPager1" PagedControlID="rlvListData" runat="server" PageSize="15" Skin="Transparent">
				        <Fields>
					        <telerik:RadDataPagerSliderField  SliderDragText=""  SliderOrientation ="Horizontal"/>
				        </Fields> 
			        </telerik:RadDataPager>
            </div>
    </asp:Panel>
<%--</telerik:RadAjaxPanel>--%>

