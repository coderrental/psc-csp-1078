﻿// ***********************************************************************
// Assembly         : TIEKinetix.CspModules.CompanyCategorySubscriptionReport
// Author           : VU DINH
// Created          : 06-11-2013
//
// Last Modified By : VU DINH
// Last Modified On : 06-11-2013
// ***********************************************************************
// <copyright file="CompanyCategoryPair.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace TIEKinetix.CspModules.CompanyCategorySubscriptionReport.Entity
{
    /// <summary>
    /// Class CompanyCategoryPair
    /// </summary>
    public class CompanyCategoryPair
    {
        /// <summary>
        /// Gets or sets the company id.
        /// </summary>
        /// <value>The company id.</value>
        public int CompanyId { get; set; }

        /// <summary>
        /// Gets or sets the category id.
        /// </summary>
        /// <value>The category id.</value>
        public int CategoryId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is subscription.
        /// </summary>
        /// <value><c>true</c> if this instance is subscription; otherwise, <c>false</c>.</value>
        public bool IsSubscription { get; set; }
    }
}