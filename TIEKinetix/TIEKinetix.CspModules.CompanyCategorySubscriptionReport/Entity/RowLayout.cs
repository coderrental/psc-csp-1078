﻿// ***********************************************************************
// Assembly         : TIEKinetix.CspModules.CompanyCategorySubscriptionReport
// Author           : VU DINH
// Created          : 06-11-2013
//
// Last Modified By : VU DINH
// Last Modified On : 06-11-2013
// ***********************************************************************
// <copyright file="RowLayout.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace TIEKinetix.CspModules.CompanyCategorySubscriptionReport.Entity
{
    /// <summary>
    /// Class RowLayout
    /// </summary>
    public class RowLayout
    {
        /// <summary>
        /// Gets or sets the layout.
        /// </summary>
        /// <value>The layout.</value>
        public string Layout { get; set; }
    }
}