﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CspSubscriptionManageCustomDomainParams.ascx.cs" Inherits="TIEKinetix.CspModules.CSPSubscriptionModule.CspSubscriptionManageCustomDomainParams" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<telerik:Radgrid runat="server" ID="rgCustomDomainParams" AutoGenerateColumns="False" OnNeedDataSource="rgCustomDomainParams_OnNeedDataSource" OnItemDataBound="rgCustomDomainParams_OnItemDataBound" OnItemCommand="rgCustomDomainParams_OnItemCommand" OnItemCreated="rgCustomDomainParams_OnItemCreated">
	<MasterTableView DataKeyNames="Id,LanguageId" CommandItemDisplay="Top" Name="CustomDomainParams" EditMode="InPlace">
		<Columns>
            <telerik:GridBoundColumn DataField="Id" UniqueName="Id" HeaderText="ID" ReadOnly="true" Visible="False" ForceExtractValue="Always">
            	<HeaderStyle Width="250"></HeaderStyle>
            </telerik:GridBoundColumn>
            <telerik:GridDropDownColumn DataField="LanguageId" UniqueName="Language" HeaderText="Language" DropDownControlType="RadComboBox"/>
            <telerik:GridBoundColumn DataField="Params" UniqueName="DomainParams" HeaderText="Domain Params" />
            <telerik:GridCheckBoxColumn DataField="VisibleToAllCountries" HeaderText="Visible to all Countries" UniqueName="VisibleToAllCountries"/>
			<telerik:GridEditCommandColumn ButtonType="ImageButton" >
				<HeaderStyle Width="100"></HeaderStyle>
			</telerik:GridEditCommandColumn>
            <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete">
            	<HeaderStyle Width="100"></HeaderStyle>
			</telerik:GridButtonColumn>                  
        </Columns>
       <NestedViewTemplate>
            <asp:HiddenField runat="server" ID="LanguageId" Value='<%# Eval("LanguageId") %>'/>
            <telerik:RadGrid ID="rgCountries" runat="server" OnItemCommand="rgCountries_OnItemCommand" OnItemDataBound="rgCountries_OnItemDataBound">
                <MasterTableView AutoGenerateColumns="False" CommandItemDisplay="Top" EditMode="InPlace" DataKeyNames="Id">
                    <Columns>
                        <telerik:GridTemplateColumn runat="server" UniqueName="country" DataField="Id" HeaderText="Country">
                            <ItemTemplate>
                                <%#Eval("Name")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <telerik:RadComboBox  ID="rlbCountry" runat="server" CheckBoxes="True" EnableCheckAllItemsCheckBox="True" Width="220px" Height="300px"/>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridEditCommandColumn  ButtonType="ImageButton" UniqueName="editcolumn">
							<HeaderStyle Width="100"></HeaderStyle>
						</telerik:GridEditCommandColumn>
                        <telerik:GridButtonColumn HeaderStyle-Width="100" ConfirmText='Delete this country?' ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete"/>
                    </Columns>
                    <EditFormSettings>
				        <EditColumn ButtonType="ImageButton"></EditColumn>
			        </EditFormSettings>
			        <CommandItemSettings AddNewRecordText="Add new Country"></CommandItemSettings>
                </MasterTableView>
            </telerik:RadGrid>
        </NestedViewTemplate>
		<CommandItemSettings AddNewRecordText="Add new Custom Domain Params"></CommandItemSettings>
		<EditFormSettings>
			<EditColumn ButtonType="ImageButton"></EditColumn>
		</EditFormSettings>
	</MasterTableView>
</telerik:Radgrid>