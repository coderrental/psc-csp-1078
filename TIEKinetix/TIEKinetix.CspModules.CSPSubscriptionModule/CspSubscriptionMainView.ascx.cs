﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.DnnModules.Common;
using CR.SocialLib;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Security;
using DotNetNuke.Services.Localization;
using TIEKinetix.CspModules.CSPSubscriptionModule.Components;
using CR.ContentObjectLibrary.Data;
using Twitterizer;

namespace TIEKinetix.CspModules.CSPSubscriptionModule
{
    public partial class CspSubscriptionMainView : DnnModuleBase, IActionable
    {
        private bool _bValidated = true;
        private string _externalIdentifier;

        #region Overrides of DnnModuleBase

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            #region check for dnn mandatory properties

            string errorMessage = "";
            if (UserInfo.Profile.ProfileProperties[Keys.CspIntegrationKey] == null || UserInfo.IsInRole(PortalSettings.AdministratorRoleName))
            {
                _bValidated = false;
                errorMessage = string.Format("<p>{0}</p>", GetString(Keys.ModuleErrorIntegrationKey));
            }

            // check connection string
            string portalCs =Utils.GetConnectionString(PortalId);
            if (string.IsNullOrEmpty(portalCs))
            {
                _bValidated = false;
                errorMessage += string.Format("<p>{0}</p>", GetString(Keys.ModuleErrorPortalConnectionString));
            }
            else
            {
                // check channel
                _externalIdentifier = Utils.GetSetting(PortalId, "ExternalIdentifier");
                if (string.IsNullOrEmpty(_externalIdentifier))
                    _externalIdentifier = Request.Url.Host;

                using (CspDataContext csp = new CspDataContext(portalCs))
                {
                    if (csp.Channels.Count(a => a.ExternalIdentifier == _externalIdentifier) == 0)
                    {
                        _bValidated = false;
	                    errorMessage += string.Format("<p>{0}: {1}</p>", GetString(Keys.ModuleErrorChanelExternalId), _externalIdentifier);
                    }
                }
            }
            #endregion

            if (_bValidated)
            {
                int key = Utils.GetIntegrationKey(UserInfo);
            	string theUrl = DotNetNuke.Common.Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "WizardMode",
            	                                                      "mid=" + ModuleId);
				if (!String.IsNullOrEmpty(Request.Params["step"]))
					theUrl = DotNetNuke.Common.Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "WizardMode", String.Format("mid={0}&step={1}", ModuleId, Request.Params["step"]));
				Response.Redirect(theUrl, false);
/*
                // if integration key exists, then go to view mode
                if (key != -1)
                    //Response.Redirect(DotNetNuke.Common.Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "ViewMode", "mid=" + ModuleId), false);
                    Response.Redirect(DotNetNuke.Common.Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "WizardMode", "mid=" + ModuleId), false);
                else
                    Response.Redirect(DotNetNuke.Common.Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "WizardMode", "mid=" + ModuleId), false);*/
            }
            else
            {
                pError.Visible = pError.Enabled = true;
                pError.Controls.Add(new Literal { Text = errorMessage });
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region Implementation of IActionable

        public ModuleActionCollection ModuleActions
        {
            get
            {
                ModuleActionCollection Actions = new ModuleActionCollection();
                Actions.Add(
                    GetNextActionID(),
                    "Settings",
                    ModuleActionType.AddContent, "", ControlPath + "Images/edit.png", EditUrl("Configuration"), false, SecurityAccessLevel.Edit, true, false);
                return Actions;
            }
        }

        #endregion
    }
}