﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CspSubscriptionConfigurationView.ascx.cs" Inherits="TIEKinetix.CspModules.CSPSubscriptionModule.CspSubscriptionConfigurationView" %>
<%@ Register Assembly="Telerik.Web.UI, Version=2012.1.411.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4"
	Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadAjaxLoadingPanel runat="server" ID="ajaxLoadingPanel" IsSticky="True" CssClass="ajax-loading-panel"></telerik:RadAjaxLoadingPanel>
<telerik:RadAjaxManager runat="server" ID="ajaxManager">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="tabstrip">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="tabstrip" />
				<telerik:AjaxUpdatedControl ControlID="multipage" LoadingPanelID="ajaxLoadingPanel" />
			</UpdatedControls>
		</telerik:AjaxSetting>
		<telerik:AjaxSetting AjaxControlID="multipage">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="multipage" LoadingPanelID="ajaxLoadingPanel" />
			</UpdatedControls>
		</telerik:AjaxSetting>
	</AjaxSettings>
</telerik:RadAjaxManager>
<telerik:radtabstrip runat="server" ID="tabstrip" SelectedIndex="0" MultiPageID="multipage" Orientation="HorizontalTop" OnTabClick="tabstrip_OnTabClick"/>

<telerik:radmultipage runat="server" ID="multipage" SelectedIndex="0" OnPageViewCreated="multipage_OnPageViewCreated">
	<telerik:RadPageView runat="server" id="pvMainPage">
		<telerik:RadGrid ID="GridForms" runat="server" OnItemCommand="GridForms_OnItemCommand" OnItemCreated="GridForms_OnItemCreated">
			<MasterTableView DataKeyNames="FormId" AutoGenerateColumns="false" CommandItemDisplay="Top">
				<Columns>
					<telerik:GridTemplateColumn HeaderStyle-Width="30px">
						<ItemTemplate>
							<asp:ImageButton runat="server" ID="bTranslate" ImageUrl="Images/text.png" CommandName="Translate" ToolTip="Translate" />
						</ItemTemplate>
					</telerik:GridTemplateColumn>
					<telerik:GridBoundColumn DataField="Name" HeaderText="Name"></telerik:GridBoundColumn>
					<telerik:GridCheckBoxColumn DataField="Active" HeaderText="Active"></telerik:GridCheckBoxColumn>            
					<telerik:GridTemplateColumn HeaderStyle-Width="60px">
						<ItemTemplate>
							<%--<asp:ImageButton runat="server" ID="bEdit" ImageUrl="Images/edit.png" CommandName="EditFields" ToolTip="Edit" />--%>
							<asp:ImageButton runat="server" ID="bDelete" ImageUrl="Images/delete.png" CommandName="Delete" ToolTip="Delete" />
						</ItemTemplate>
					</telerik:GridTemplateColumn>
				</Columns>
				<NestedViewTemplate>
					<asp:Panel ID="FieldContainer" runat="server">
						<asp:HiddenField runat="server" ID="hffId" Value='<%#Eval("FormId") %>' />
						<telerik:RadGrid ID="GridFields" runat="server" 
							OnItemCommand="GridFields_OnItemCommand" 
							OnItemDataBound="GridFields_OnItemDataBound"
							OnItemCreated="GridFields_OnItemCreated">
							<MasterTableView AutoGenerateColumns="false" DataKeyNames="FormId,FieldId,CodeListId" CommandItemDisplay="Top">
								<Columns>
									<telerik:GridEditCommandColumn ButtonType="ImageButton"></telerik:GridEditCommandColumn>
									<telerik:GridBoundColumn DataField="Name" HeaderText="Name"></telerik:GridBoundColumn>
									<telerik:GridBoundColumn DataField="Type" HeaderText="Type"></telerik:GridBoundColumn>
									<telerik:GridCheckBoxColumn DataField="Mandatory" HeaderText="Mandatory"></telerik:GridCheckBoxColumn>
									<telerik:GridnumericColumn DataField="DisplayOrder" HeaderText="Display Order"></telerik:GridnumericColumn>
									<telerik:GridDropDownColumn DataField="CodeListId" HeaderText="Code List" DropDownControlType="DropDownList" UniqueName="CodeList" 
										EnableEmptyListItem="true" EmptyListItemText="No Selected Codelist" EmptyListItemValue="0">
									</telerik:GridDropDownColumn>
									<telerik:GridBoundColumn DataField="RegexValidation" HeaderText="Regex Validation" EmptyDataText="No validation" UniqueName="RegexValidation"/>
									<telerik:GridTemplateColumn>
										<ItemTemplate>
											<asp:ImageButton ImageUrl="Images/delete.png" CommandName="Delete" runat="server" ID="bDeleteField" />
										</ItemTemplate>
									</telerik:GridTemplateColumn>
								</Columns>
								<CommandItemSettings AddNewRecordText="Add new field" />
								<EditFormSettings>
									<EditColumn ButtonType="ImageButton"></EditColumn>
								</EditFormSettings>        
							</MasterTableView>
						</telerik:RadGrid>                
					</asp:Panel>
				</NestedViewTemplate>
				<CommandItemSettings AddNewRecordText="Add new form" />
				<EditFormSettings>
					<EditColumn ButtonType="ImageButton"></EditColumn>
				</EditFormSettings>        
			</MasterTableView>
		</telerik:RadGrid>
	</telerik:RadPageView>
</telerik:radmultipage>