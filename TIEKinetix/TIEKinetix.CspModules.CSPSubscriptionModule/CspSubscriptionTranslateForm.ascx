﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CspSubscriptionTranslateForm.ascx.cs" Inherits="TIEKinetix.CspModules.CSPSubscriptionModule.CspSubscriptionTranslateForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadToolBar ID="Toolbar" runat="server" Width="100%">
    <Items>
        <telerik:RadToolBarButton Text="Back" ImageUrl="Images/arrow_left_green.png" Value="Back" ToolTip="Back">            
        </telerik:RadToolBarButton>
        <telerik:RadToolBarButton Text="Save" ImageUrl="Images/disk_blue.png" Value="Save" ToolTip="Back">            
        </telerik:RadToolBarButton>
    </Items>
</telerik:RadToolBar>

<h1><asp:Literal ID="lName" runat="server"></asp:Literal></h1>

<asp:Panel ID="ContainerPanel" runat="server">
    <telerik:RadTabStrip runat="server" ID="Tabs" SelectedIndex="0" MultiPageID="Pages">
        <Tabs>
            <telerik:RadTab runat="server" Text="Translate Form" PageViewID="formPage">                                  
            </telerik:RadTab>
            <telerik:RadTab runat="server" Text="Translate Fields" PageViewID="formFields"></telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage runat="server" ID="Pages" SelectedIndex="0">
        <telerik:RadPageView ID="formPage" runat="server">            
            <asp:TextBox ID="tbFormName" runat="server" Width="350" CssClass="csp-form-field"></asp:TextBox>
            <br />
            <asp:TextBox ID="tbFormDesc" runat="server" TextMode="MultiLine" Rows="5" Width="350" CssClass="csp-form-field"></asp:TextBox>
        </telerik:RadPageView>
        <telerik:RadPageView ID="formFields" runat="server"></telerik:RadPageView>
    </telerik:RadMultiPage>
</asp:Panel>