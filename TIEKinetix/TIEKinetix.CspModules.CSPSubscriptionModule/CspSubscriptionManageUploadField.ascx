﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CspSubscriptionManageUploadField.ascx.cs" Inherits="TIEKinetix.CspModules.CSPSubscriptionModule.CspSubscriptionManageUploadField" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:radgrid runat="server" ID="gridListUploadFields" OnNeedDataSource="gridListUploadFields_OnNeedDataSource" OnItemCommand="gridListUploadFields_OnItemCommand">
    <MasterTableView DataKeyNames="FieldId,FormId" AutoGenerateColumns="False" CommandItemDisplay="None">
        <Columns>
             <telerik:GridBoundColumn HeaderText="Form Name" UniqueName="FormName" DataField="FormName" ReadOnly="True"/>
            <telerik:GridBoundColumn HeaderText="Field Name" UniqueName="FieldName" DataField="FieldName" ReadOnly="True"/>
            <telerik:GridCheckBoxColumn HeaderText="Resize when Upload?" UniqueName="IsResize" DataField="IsResize"/>
            <telerik:GridBoundColumn HeaderText="Resize Width" UniqueName="ResizeWidth" DataField="ResizeWidth"/>
            <telerik:GridBoundColumn HeaderText="Resize Height" UniqueName="ResizeHight" DataField="ResizeHeight"/>
            <telerik:GridEditCommandColumn ButtonType="ImageButton"/>
        </Columns>
        <EditFormSettings>
			<EditColumn ButtonType="ImageButton"></EditColumn>
		</EditFormSettings>   
    </MasterTableView>
</telerik:radgrid>
