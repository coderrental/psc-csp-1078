﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CspSubscriptionClusterList.ascx.cs" 
            Inherits="TIEKinetix.CspModules.CSPSubscriptionModule.CspSubscriptionClusterList" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<telerik:radgrid runat="server" ID="rgClusterList" AutoGenerateColumns="False" OnItemCreated="rgClusterList_OnItemCreated"
        OnItemCommand="rgClusterList_OnItemCommand" OnNeedDataSource="rgClusterList_OnNeedDataSource">
    <MasterTableView CommandItemDisplay="Top" EditMode="InPlace" DataKeyNames="ClusterId" Name="Name">
        <CommandItemSettings ShowAddNewRecordButton="True" ShowRefreshButton="True" />
        <Columns>
            <telerik:GridBoundColumn runat="server" HeaderText="Cluster Name" DataField="ClusterName" UniqueName="Name"/>
            <telerik:GridBoundColumn runat="server" HeaderText="Description" DataField="Description" UniqueName="Name"/>
            <telerik:GridEditCommandColumn ButtonType="ImageButton">
                <HeaderStyle Width="100"></HeaderStyle>
            </telerik:GridEditCommandColumn>
            <telerik:GridButtonColumn ConfirmText='Delete this Cluster?' ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete">
                <HeaderStyle Width="100"></HeaderStyle>
            </telerik:GridButtonColumn>
        </Columns>
        <NestedViewTemplate>
            <asp:HiddenField runat="server" ID="clusterId" Value='<%# Eval("ClusterId") %>'/>
            <telerik:RadGrid ID="rgCluster_Country" runat="server" OnItemCommand="rgCluster_Country_OnItemCommand" OnItemDataBound="rgCluster_Country_OnItemDataBound">
                <MasterTableView AutoGenerateColumns="False" CommandItemDisplay="Top" EditMode="InPlace" DataKeyNames="Id">
                    <Columns>
                        <telerik:GridTemplateColumn runat="server" UniqueName="country" DataField="Id" HeaderText="Country">
                            <ItemTemplate>
                                <%#Eval("Name")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <telerik:RadComboBox  ID="rlbCountry" runat="server" CheckBoxes="True" EnableCheckAllItemsCheckBox="True" Width="220px" Height="300px"/>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridEditCommandColumn  ButtonType="ImageButton" UniqueName="editcolumn">
							<HeaderStyle Width="100"></HeaderStyle>
						</telerik:GridEditCommandColumn>
                        <telerik:GridButtonColumn HeaderStyle-Width="100" ConfirmText='Delete this country in cluster?' ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete"/>
                    </Columns>
                    <EditFormSettings>
				        <EditColumn ButtonType="ImageButton"></EditColumn>
			        </EditFormSettings>
			        <CommandItemSettings AddNewRecordText="Add new Country to cluster"></CommandItemSettings>
                </MasterTableView>
            </telerik:RadGrid>
            <telerik:RadGrid ID="rgCluster_Language" runat="server" OnItemCommand="rgCluster_Language_OnItemCommand" OnItemDataBound="rgCluster_Language_OnItemDataBound">
                <MasterTableView AutoGenerateColumns="False" CommandItemDisplay="Top" EditMode="InPlace" DataKeyNames="languages_Id">
                    <Columns>
                        <telerik:GridTemplateColumn runat="server" UniqueName="language" DataField="Id" HeaderText="Language">
                            <EditItemTemplate>
                                <telerik:RadComboBox EnableCheckAllItemsCheckBox="True" runat="server" ID="rlbLanguage" CheckBoxes="true" Width="220px" Height="300px" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <%# Eval("languagecode")%>
                            </ItemTemplate>
                            <ItemTemplate>
                                <%# Eval("description")%>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton" HeaderStyle-Width="100" UniqueName="editcolumn"/>
                        <telerik:GridButtonColumn HeaderStyle-Width="100" ConfirmText='Delete this language in cluster?' ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete"/>
                    </Columns>
                    <EditFormSettings>
				        <EditColumn ButtonType="ImageButton"></EditColumn>
			        </EditFormSettings>
			        <CommandItemSettings AddNewRecordText="Add new Language to cluster"></CommandItemSettings>
                </MasterTableView>
            </telerik:RadGrid>
        </NestedViewTemplate>
        <EditFormSettings>
            <EditColumn ButtonType="ImageButton"></EditColumn>
        </EditFormSettings>
        <CommandItemSettings AddNewRecordText="Add new Cluster "></CommandItemSettings>
    </MasterTableView>
</telerik:radgrid>