﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CspSubscriptionManageWorkflow.ascx.cs" Inherits="TIEKinetix.CspModules.CSPSubscriptionModule.CspSubscriptionManageWorkflow" %>
<%@ Register Assembly="Telerik.Web.UI, Version=2012.1.411.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4"
	Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<telerik:radwindowmanager runat="server" ID="windowManager"></telerik:radwindowmanager>
<telerik:radgrid runat="server" ID="rgWorkFlow" AutoGenerateColumns="False" OnNeedDataSource="rgWorkFlow_OnNeedDataSource" OnItemDataBound="rgWorkFlow_OnItemDataBound" OnItemCreated="rgWorkFlow_OnItemCreated" OnItemCommand="rgWorkFlow_OnItemCommand">
	<MasterTableView Name="WorkFlow" DataKeyNames="Id,ParentFormId,FormId" EditMode="InPlace" CommandItemDisplay="Top">
		<CommandItemSettings ShowAddNewRecordButton="True" ShowRefreshButton="False"/>
		<Columns>
			<telerik:GridDropDownColumn DataField="ParentFormName" UniqueName="ParentFormName" HeaderText="Parent Form" DropDownControlType="RadComboBox" EmptyListItemText="Company Information" EnableEmptyListItem="True" EmptyListItemValue="0"/>
			<telerik:GridDropDownColumn DataField="FormName" UniqueName="FormName" HeaderText="Form" DropDownControlType="RadComboBox"/>
			<telerik:GridEditCommandColumn ButtonType="ImageButton" HeaderStyle-Width="100"/>
			<telerik:GridButtonColumn ConfirmText="Delete this workflow? This will remove all workflow conditions." ConfirmDialogType="RadWindow" ConfirmTitle="Detete" ButtonType="ImageButton" CommandName="Delete" HeaderStyle-Width="100"/>
		</Columns>
		<NestedViewTemplate>
			<asp:HiddenField runat="server" ID="workflowid" Value='<%# Eval("Id") %>'/>
			<telerik:RadGrid runat="server" ID="rgWorkflowCondition" OnItemCreated="rgWorkflowCondition_OnItemCreated" OnItemCommand="rgWorkflowCondition_OnItemCommand" OnItemDataBound="rgWorkflowCondition_OnItemDataBound">
				<MasterTableView Name="WorkFlowCondition" DataKeyNames="WorkflowConditionId,FieldId,TargetFieldIds" CommandItemDisplay="Top" AutoGenerateColumns="False">
					<Columns>
						<telerik:GridDropDownColumn DataField="FieldId" UniqueName="FieldId" HeaderText="Field Name" DropDownControlType="RadComboBox" EnableEmptyListItem="False" EmptyListItemValue="0"/>
						<telerik:GridBoundColumn DataField="Operator" UniqueName="Operator" HeaderText="Operator"/>
						<telerik:GridBoundColumn DataField="Value" UniqueName="Value" HeaderText="Value"/>
						<%--<telerik:GridBoundColumn DataField="TargetFieldIds" UniqueName="TargetFieldIds" HeaderText="Target Field Ids"/>--%>
						<telerik:GridTemplateColumn UniqueName="TargetFieldIds" HeaderText="Target Field">
							<ItemTemplate>
							  <asp:Label runat="server" ID="tbxTargetFieldIds"></asp:Label>
							</ItemTemplate>
							<EditItemTemplate>
								<telerik:RadListBox Width="300" ID="lbxTargetFieldIds" SelectionMode="Multiple" CheckBoxes="True" runat="server"/>
							</EditItemTemplate>
						</telerik:GridTemplateColumn>
						<telerik:GridEditCommandColumn  ButtonType="ImageButton">
							<HeaderStyle Width="100"></HeaderStyle>
						</telerik:GridEditCommandColumn>
						<telerik:GridButtonColumn ConfirmText='Delete this condition?' ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete">
							<HeaderStyle Width="100"></HeaderStyle>
						</telerik:GridButtonColumn>
					</Columns>
					<CommandItemSettings AddNewRecordText="Add new condition"></CommandItemSettings>
					<EditFormSettings>
						<EditColumn ButtonType="ImageButton"/>
					</EditFormSettings>
				</MasterTableView>
			</telerik:RadGrid>
		</NestedViewTemplate>
	</MasterTableView>
</telerik:radgrid>
