﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CspSubscriptionFormWizard.ascx.cs" Inherits="TIEKinetix.CspModules.CSPSubscriptionModule.CspSubscriptionFormWizard" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import Namespace="DotNetNuke.Common" %>
<telerik:radajaxloadingpanel runat="server" ID="ajaxLoading" IsSticky="True" CssClass="ajax-loading-panel"></telerik:radajaxloadingpanel>
<telerik:radwindowmanager ID="windowManager" runat="server"></telerik:radwindowmanager>
<div id="wrapper">
<div id="wizard_container_pane" runat="server">
	<telerik:RadScriptBlock runat="server">
	<script type="text/javascript">
		$(function () {
			$('.csp-nav-item-a').each(function () {
				var height = $(this).height();
				var wrapHeight = $(this).parent().height();
				var heightfix = (wrapHeight - height) / 2;
				$(this).css("top", heightfix + "px");
			});
		});
	</script>
</telerik:RadScriptBlock>
	<asp:Wizard ID="formWizard" runat="server" CssClass="csp-form-wizard"
					OnFinishButtonClick="formWizard_FinishButtonClick" OnNextButtonClick="formWizard_NextButtonClick" OnActiveStepChanged="formWizard_ActiveStepChanged"
					ActiveStepIndex="0" 
					CellPadding="5" CellSpacing="5"
					FinishCompleteButtonType="Link"
					FinishPreviousButtonType="Link" 
					StartNextButtonType="Link" 
					StepNextButtonType="Link"
					StepPreviousButtonType="Link" 
					DisplaySideBar="true">
		<SideBarTemplate>
			<asp:ListView ID="sideBarList" runat="server" OnItemDataBound="SideBarList_ItemDataBound">
			<LayoutTemplate>
				<div id="ItemPlaceHolder" runat="server"/>
			</LayoutTemplate>
			<SelectedItemTemplate>
				<span class="csp-nav-item">
					<span class="csp-nav-item_wrap csp-nav-item_wrap_seleted">
						<asp:LinkButton ID="sideBarButton" runat="server" Text="Button" CssClass="csp-nav-item-a"  Enabled="false" />
					</span>
				</span>
			</SelectedItemTemplate>
			<ItemTemplate>
				<span class="csp-nav-item">
					<span class="csp-nav-item_wrap">
						<asp:LinkButton ID="sideBarButton" runat="server" Text="Button" Enabled="false" CssClass="csp-nav-item-a" />
					</span>
				</span>
			</ItemTemplate>
			</asp:ListView>
		</SideBarTemplate>
		<LayoutTemplate>
			<div class="csp-form-wizard">
				<div class="csp-clear">
				  <asp:PlaceHolder ID="headerPlaceHolder" runat="server" />
				</div>
				<div class="csp-form-wizard-nav">
				  <asp:PlaceHolder ID="sideBarPlaceHolder" runat="server" />
				</div>
				<div class="csp-clear">
					<div class="csp-mandatory-indicator">
						<div class="csp-form-container">
							<div class="csp-form-block">
								<div class="csp-form-fields">
									<table>
										<tr>
											<td>
												<span><asp:Literal ID="lMandatoryIndicatorText" runat="server"></asp:Literal></span>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
					<asp:PlaceHolder ID="WizardStepPlaceHolder" runat="server" />
				</div>
				<div class="csp-clear">
				  <asp:PlaceHolder ID="navigationPlaceHolder" runat="server" />
				</div>
			</div>
		</LayoutTemplate>

		<StartNavigationTemplate>
			<ul class="csp-buttons-ul csp-clear">
    			<li><asp:LinkButton id="StartNextButton" runat="server" CssClass="csp-button" CommandName="MoveNext" ValidationGroup="CompanyInformation" OnClientClick="ClientValidation()" cspobj="REPORT" csptype="links" cspenglishvalue="Wiz_Next"/></li>            
			</ul>
			<div class="csp-clear"></div>
		</StartNavigationTemplate>
		<StepNavigationTemplate>
			<ul class="csp-buttons-ul csp-clear">            
				<li><asp:LinkButton id="StepNextButton" runat="server" CssClass="csp-button" CommandName="MoveNext" cspobj="REPORT" csptype="links" cspenglishvalue="Wiz_Next" /></li>
				<li><asp:LinkButton id="StepPreviousButton" runat="server" CssClass="csp-button" CommandName="MovePrevious" CausesValidation="false" cspobj="REPORT" csptype="links" cspenglishvalue="Wiz_Back" /></li>
				<li><asp:Label ID="StepErrorMessage" CssClass="csp-form-error" runat="server"></asp:Label></li>    	    
			</ul>
			<div class="csp-clear"></div>
		</StepNavigationTemplate>
		<FinishNavigationTemplate>
			<ul class="csp-buttons-ul csp-clear">
				<li><asp:LinkButton id="FinishButton" runat="server" CssClass="csp-button" CommandName="MoveComplete" cspobj="REPORT" csptype="links" cspenglishvalue="Wiz_Finish"/></li>
				<li><asp:LinkButton id="FinishPreviousButton" runat="server" CssClass="csp-button" CommandName="MovePrevious" CausesValidation="false" cspobj="REPORT" csptype="links" cspenglishvalue="Wiz_Back" /></li>
				<li><asp:Label ID="FinishErrorMessage" CssClass="csp-form-error" runat="server"></asp:Label></li>
			</ul>
			<div class="csp-clear"></div>
		</FinishNavigationTemplate>    
		<HeaderTemplate>
			<asp:ValidationSummary ID="validationSummary" runat="server" DisplayMode="List" />        
		</HeaderTemplate>
    
		<WizardSteps>
			<asp:WizardStep ID="MandatoryForm" runat="server" Title="CompanyInfo">
				<div class="csp-form-container">
					<div class="csp-form-block">
						<h2><%=GetString("CompanyInfo.Header") %></h2>
						<div class="csp-form-fields">
							<table>
								<tbody>
									<tr>
										<td><span class="csp-form-field-label"><%=GetString("CompanyInfo.CompanyName") %></span></td>
										<td><span class="csp-form-field-label"><%=GetString("CompanyInfo.Address1") %></span></td>
										<td><span class="csp-form-field-label"><%=GetString("CompanyInfo.City") %></span></td>
									</tr>
									<tr>
										<td class="company-name-mobile bold">
											<asp:TextBox ID="tbCompanyName" runat="server" CssClass="csp-form-field csp-form-mandatory"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vCompanyName" runat="server" ControlToValidate="tbCompanyName" ValidationGroup="CompanyInformation"></asp:RequiredFieldValidator>
										</td>
										<td class="address-mobile bold">
											<asp:TextBox ID="tbAddress1" runat="server" CssClass="csp-form-field csp-form-mandatory"  Width="400px"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vAddress1" runat="server" ControlToValidate="tbAddress1" ValidationGroup="CompanyInformation"></asp:RequiredFieldValidator>
										</td>
										<td class="city-mobile bold">
											<asp:TextBox ID="tbCity" runat="server" CssClass="csp-form-field csp-form-mandatory" Width="225px"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vCity" runat="server" ControlToValidate="tbCity" ValidationGroup="CompanyInformation"></asp:RequiredFieldValidator>
										</td>
									</tr>
									<tr>
										<td><span class="csp-form-field-label"><%=GetString("CompanyInfo.State") %></span></td>
										<td><span class="csp-form-field-label"><%=GetString("CompanyInfo.Zip") %></span></td>
										<td><span class="csp-form-field-label"><%=GetString("CompanyInfo.Country") %></span></td>                                    
									</tr>
									<tr>
										<td class="state-mobile bold">
											<asp:TextBox ID="tbState" runat="server" CssClass="csp-form-field csp-form-mandatory"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vState" runat="server" ControlToValidate="tbState" ValidationGroup="CompanyInformation"></asp:RequiredFieldValidator>
										</td>
										<td class="zipcode-mobile bold">
											<asp:TextBox ID="tbZip" runat="server" CssClass="csp-form-field csp-form-mandatory"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vZip" runat="server" ControlToValidate="tbZip" ValidationGroup="CompanyInformation"></asp:RequiredFieldValidator>
										</td>
										<td class="country-mobile bold">
											<asp:DropDownList ID="ddlCountry" runat="server" CssClass="csp-form-field csp-form-mandatory"></asp:DropDownList>
											<asp:RequiredFieldValidator ID="vCountry" runat="server" ControlToValidate="ddlCountry" ValidationGroup="CompanyInformation"></asp:RequiredFieldValidator>
										</td>
									</tr>
									<tr>
										<td colspan="3"><span class="csp-form-field-label"><%=GetString("CompanyInfo.Url") %></span></td>
									</tr>
									<tr>
										<td colspan="3" class="url-mobile bold">
											<asp:TextBox ID="tbUrl" runat="server" CssClass="csp-form-field csp-form-mandatory" Width="353px"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vUrl1" runat="server" ControlToValidate="tbUrl" ValidationGroup="CompanyInformation"></asp:RequiredFieldValidator>
											<asp:RegularExpressionValidator ID="vUrl" runat="server" CssClass="company-websiteurl-error" ControlToValidate="tbUrl" ValidationGroup="CompanyInformation" ValidationExpression="^(http|https)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,4}(/\S*)?$"></asp:RegularExpressionValidator>
										</td>
									</tr>
								</tbody>
							</table>                        
						</div>
					</div>
					<div class="csp-form-block">
						<h2><%=GetString("ContactPersonInfo.Header") %></h2>
						<div class="csp-form-fields">
							<table>
								<tbody>
									<tr>
										<td><span class="csp-form-field-label"><%=GetString("ContactPersonInfo.FirstName") %></span></td>
										<td><span class="csp-form-field-label"><%=GetString("ContactPersonInfo.LastName") %></span></td>
										<td><span class="csp-form-field-label"><%=GetString("ContactPersonInfo.JobTitle") %></span></td>
										<td><span class="csp-form-field-label"><%=GetString("ContactPersonInfo.PhoneNumber") %></span></td>
									</tr>
									<tr>
										<td class="firstname-mobile bold">
											<asp:TextBox ID="tbFirstName" runat="server" CssClass="csp-form-field csp-form-mandatory"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vFirstName" runat="server" ControlToValidate="tbFirstName" ValidationGroup="CompanyInformation"></asp:RequiredFieldValidator>
										</td>
										<td class="lastname-mobile bold">
											<asp:TextBox ID="tbLastName" runat="server" CssClass="csp-form-field csp-form-mandatory"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vLastName" runat="server" ControlToValidate="tbLastName" ValidationGroup="CompanyInformation"></asp:RequiredFieldValidator>
										</td>
										<td class="jobtitle-mobile bold">
											<asp:TextBox ID="tbJobTitle" runat="server" CssClass="csp-form-field csp-form-mandatory"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vJobTitle" runat="server" ControlToValidate="tbJobTitle" ValidationGroup="CompanyInformation"></asp:RequiredFieldValidator>
										</td>
										<td class="phonenumber-mobile bold">
											<asp:TextBox ID="tbPhoneNumber" runat="server" CssClass="csp-form-field csp-form-mandatory"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vPhoneNumber" runat="server" ControlToValidate="tbPhoneNumber" ValidationGroup="CompanyInformation"></asp:RequiredFieldValidator>
										</td>
									</tr>
									<tr>
										<td colspan="4"><span class="csp-form-field-label"><%=GetString("ContactPersonInfo.Email")%></span></td>
									</tr>
									<tr>
										<td colspan="4" class="emailaddress-mobile bold">
											<asp:TextBox ID="tbEmail" runat="server" CssClass="csp-form-field csp-form-mandatory" Width="374px"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vEmail1" runat="server" ControlToValidate="tbEmail" ValidationGroup="CompanyInformation"></asp:RequiredFieldValidator>
											<asp:RegularExpressionValidator ID="vEmail" CssClass="company-websiteurl-error" runat="server" ControlToValidate="tbEmail" ValidationGroup="CompanyInformation" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
										</td>
									</tr>
									<tr>
										<td colspan="4">
											<asp:CheckBox ID="cbAgreeToMailings" runat="server" CssClass="csp-form-field-label csp-form-field-checkbox" />
										</td>
									</tr>
								</tbody>
							</table>                        
						</div>
					</div>
				</div>
			</asp:WizardStep>
		</WizardSteps>
	</asp:Wizard>
	<input type="hidden" runat="server" ID="missingStep" value="0"/>
	<input type="hidden" runat="server" ID="isFilled" value="0"/>
    <input type="hidden" runat="server" ID="hidePopup" value="0"/>
	<div id="mandatoryFieldsRequest" class="hide">
		<div id="mandatoryFieldsRequest-text">
			<%=GetString("Message.MandatoryFieldsRequired")%>
		</div>
		<div id="mandatoryFieldsRequest-button-wrapper">
			<a id="mandatoryFieldsRequest-ok" class="btn-long-link" href="javascript: void(0)">
				<span class="btn-long ss-btn">
					<span class="btn-long-right ss-btn">
						<span class="btn-long-mid ss-btn">
							<span class="btn-long-text ss-btn"><%= GetString("Message.MandatoryFieldsRequiredOk")%></span>
						</span>
					</span>
				</span>
			</a>
		</div>
	</div>
	<div id="mandatoryFieldsRequest-overlay"></div>
	<telerik:RadScriptBlock runat="server">
		<script type="text/javascript">
			function ClientValidation() {
				if (!Page_ClientValidate("CompanyInformation")) {
					//alert("<%=HttpUtility.JavaScriptStringEncode(GetString("CompanyInfo.ErrorMessage")) %>");
					return false;
				}
			}
			var cspConsumerInfo = <%=ConsumerInfoJson %>;

			if ($.browser.msie && $.browser.version <= 7)
			{
				$(".csp-form-wizard .csp-form-wizard-nav .csp-nav-item a").each(function(){$(this).css("display","block");$(this).css("width","165px");});
				$(".company-websiteurl-error").css({ 'position': 'relative', 'top': '-5px' });
			}

			$(function() {
				//initMandatoryFieldsRequiredMessage();
				var missingStep = $('#<%= missingStep.ClientID %>').val();
				var formCount = $('.csp-form-wizard-nav').find('.csp-nav-item').length;
				missingStep = parseInt(missingStep);
				if ($('#<%= isFilled.ClientID %>').val() != '0')
					missingStep = 0;
				if (missingStep > 1 && missingStep <= formCount) {
					if ($('.csp-form-wizard-nav').find('.csp-nav-item_wrap:eq(' + parseInt(missingStep - 1) + ')').hasClass('csp-nav-item_wrap_seleted')
						|| $('.csp-form-wizard-nav').find('.csp-nav-item_wrap:gt(' + parseInt(missingStep - 1) + ')').hasClass('csp-nav-item_wrap_seleted'))
						return;
					$('#mandatoryFieldsRequest-overlay').show(); $('#main-nav').css({'z-index': '1'});
					eval($('.csp-form-wizard-nav').find('.csp-nav-item-a:eq(' + parseInt(missingStep - 1) + ')').attr('href'));
				}
			});
			
			try {
				$('#mandatoryFieldsRequest-ok').click(function() {
					try {
						var dgMandatoryFieldsRequest = $('#mandatoryFieldsRequest').data("kendoWindow");
						dgMandatoryFieldsRequest.close();
					}
					catch(e) {}
					$('#mandatoryFieldsRequest-overlay').hide();
					$('#main-nav').css({'z-index': '2'});
				});
                var dialogMandatoryFieldsRequest = $('#mandatoryFieldsRequest').kendoWindow({
                    actions: ["Close"],
                    draggable: true,
                    height: "110px",
                    width: "250px",
                    modal: true,
                    resizable: false,
                    visible: false,
                    open: function () { $('#mandatoryFieldsRequest-overlay').show(); $('#main-nav').css({'z-index': '1'}); },
                    close: function () { $('#mandatoryFieldsRequest-overlay').hide(); $('#main-nav').css({'z-index': '2'}); },
                    title: '<%=GetString("Message.MandatoryFieldsRequiredTitle")%>'
                }).data("kendoWindow");
				if ($('#<%= missingStep.ClientID %>').val() != 0 && $('.csp-form-wizard-nav').find('.csp-nav-item_wrap:eq(' + parseInt($('#<%= missingStep.ClientID %>').val() - 1) + ')').hasClass('csp-nav-item_wrap_seleted')) {
					if ($('#<%= isFilled.ClientID %>').val() == '0' && $('#<%= hidePopup.ClientID %>').val() == '0') {
						var dgMandatoryFieldsRequest = $('#mandatoryFieldsRequest').data("kendoWindow");
						$('#mandatoryFieldsRequest').show();
						dgMandatoryFieldsRequest.center();
						dgMandatoryFieldsRequest.open();
					}
				}
			}
			catch(e){}			
		</script>
	</telerik:RadScriptBlock>
	
</div>