﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TIEKinetix.CspModules.CSPSubscriptionModule.Components.Entities
{
	public class Workflow
	{
		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		/// <value>
		/// The id.
		/// </value>
		public Guid Id { get; set; }

		/// <summary>
		/// Gets or sets the parent form id.
		/// </summary>
		/// <value>
		/// The parent id.
		/// </value>
		public Guid ParentFormId { get; set; }

		/// <summary>
		/// Gets or sets the form id.
		/// </summary>
		/// <value>
		/// The form id.
		/// </value>
		public Guid FormId { get; set; }

		/// <summary>
		/// Gets or sets the name of the parent form.
		/// </summary>
		/// <value>
		/// The name of the parent form.
		/// </value>
		public string ParentFormName { get; set; }

		/// <summary>
		/// Gets or sets the name of the form.
		/// </summary>
		/// <value>
		/// The name of the form.
		/// </value>
		public string FormName { get; set; }
	}
}