﻿using TIEKinetix.CspModules.CSPSubscriptionModule.Components.Data;

namespace TIEKinetix.CspModules.CSPSubscriptionModule.Components
{
    public class TranslationText
    {
        public TranslationText(string s)
        {
            ShortDescription = LongDescription = s;
        }
        public TranslationText(CSPSubscriptionModule_Translation t)
        {
            ShortDescription = t.ShortDesc;
            LongDescription = t.LongDesc;
        }

        public string LongDescription { get; set; }
        public string ShortDescription { get; set; }
    }
}