﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Linq;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.ContentObjectLibrary.Data;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Instrumentation;
using DotNetNuke.Services.Localization;
using DotNetNuke.Services.Log.EventLog;
using CR.ContentObjectLibrary;
using CR.DnnModules.Common;
using CR.SocialLib;
using CR.SocialLib.OAuth;
using TIEKinetix.CspModules.CSPSubscriptionModule.Components.Data;
using CSPSubscriptionModule_Workflow = TIEKinetix.CspModules.CSPSubscriptionModule.Components.Data.CSPSubscriptionModule_Workflow;


namespace TIEKinetix.CspModules.CSPSubscriptionModule.Components
{
    public class DnnModuleBase : PortalModuleBase
    {
        
        private string _localResourceFile;
        protected CspDataContext CspContext;
        protected DnnCspSubscriptionModuleContext DnnContext;
        protected int CspId;
        public string CurrentLanguage;
        protected EventLogController DnnEventLog;
        protected string RedirectTabName, ApproveTweetParameterName, TwitterConsumerKey, TwitterConsumerSecret, LinkedInConsumerKey, LinkedInConsumerSecret, FacebookConsumerKey, FacebookConsumerSecret;
        protected int SupplierId;
        public int TabmoduleId;

        protected virtual void Page_Init(object sender, EventArgs e)
        {
            // get common resource file so all entries would be in 1 file instead of in many different files
            _localResourceFile = Utils.GetCommonResourceFile("CspSubscriptionModule", LocalResourceFile);
            CurrentLanguage = Thread.CurrentThread.CurrentCulture.Name;
            DnnEventLog = new EventLogController();
			if (ModuleConfiguration != null)
				ModuleConfiguration.ModuleTitle = GetString(Keys.ModuleTitleKey);

            CspContext = new CspDataContext(Utils.GetConnectionString(PortalId));
            DnnContext = new DnnCspSubscriptionModuleContext(Config.GetConnectionString());
            CspId = Utils.GetIntegrationKey(UserInfo);
            CheckMissingCompany();


            //Get info from module setting
            if (Settings[ModuleSetting.TabName] != null && Settings[ModuleSetting.TabName].ToString() != string.Empty)
            {
                RedirectTabName = Settings[ModuleSetting.TabName].ToString();
            }
            if (Settings[ModuleSetting.SupplierId] != null && Settings[ModuleSetting.SupplierId].ToString() != string.Empty)
            {
                SupplierId = int.Parse(Settings[ModuleSetting.SupplierId].ToString());
            }
            else
            {
                SupplierId = 12;
            }

            #region [ Get social consumer key, consumer secret from Module Settings ]
            if (Settings[ModuleSetting.TwitterConsumerKey] != null && Settings[ModuleSetting.TwitterConsumerKey].ToString() != string.Empty)
            {
                TwitterConsumerKey = Settings[ModuleSetting.TwitterConsumerKey].ToString();
            }
            if (Settings[ModuleSetting.TwitterConsumerSecret] != null && Settings[ModuleSetting.TwitterConsumerSecret].ToString() != string.Empty)
            {
                TwitterConsumerSecret = Settings[ModuleSetting.TwitterConsumerSecret].ToString();
            }
            if (Settings[ModuleSetting.LinkedInConsumerKey] != null && Settings[ModuleSetting.LinkedInConsumerKey].ToString() != string.Empty)
            {
                LinkedInConsumerKey = Settings[ModuleSetting.LinkedInConsumerKey].ToString();
            }
            if (Settings[ModuleSetting.LinkedInConsumerSecret] != null && Settings[ModuleSetting.LinkedInConsumerSecret].ToString() != string.Empty)
            {
                LinkedInConsumerSecret = Settings[ModuleSetting.LinkedInConsumerSecret].ToString();
            }
            if (Settings[ModuleSetting.FacebookConsumerKey] != null && Settings[ModuleSetting.FacebookConsumerKey].ToString() != string.Empty)
            {
                FacebookConsumerKey = Settings[ModuleSetting.FacebookConsumerKey].ToString();
            }
            if (Settings[ModuleSetting.FacebookConsumerSecret] != null && Settings[ModuleSetting.FacebookConsumerSecret].ToString() != string.Empty)
            {
                FacebookConsumerSecret = Settings[ModuleSetting.FacebookConsumerSecret].ToString();
            }
            #endregion

            if (Settings[ModuleSetting.ApproveTweetParameterName] != null && Settings[ModuleSetting.ApproveTweetParameterName].ToString() != string.Empty)
            {
                ApproveTweetParameterName = Settings[ModuleSetting.ApproveTweetParameterName].ToString();
            }
        }

        /// <summary>
        /// Check CspId && company
        /// </summary>
        /// Author: Phat Ngo
        private void CheckMissingCompany() {
            if (CspId != -1) {
                company cspCompany = CspContext.companies.SingleOrDefault(a => a.companies_Id == CspId);
                if (cspCompany == null)
                    CspId = -1;
            }
        }

        /// <summary>
        /// Get localized text for the current portal language
        /// </summary>
        /// <param name="key">Resource Key</param>
        /// <returns>Localized text</returns>
        public string GetString(string key)
        {
            return Localization.GetString(key, _localResourceFile);
        }

        /// <summary>
        /// Write message to Dnn event view
        /// </summary>
        /// <param name="message">message to write</param>
        public void Debug(string message)
        {
            DnnEventLog.AddLog("DEBUG", message, PortalSettings, UserId, EventLogController.EventLogType.ADMIN_ALERT);
        }
        
        /// <summary>
        /// Get portal external identifier used to identify portal channel.
        /// </summary>
        /// <returns></returns>
        protected string GetPortalExternalIdentifier()
        {
            string s = Utils.GetSetting(PortalId, "ExternalIdentifier");
            return !string.IsNullOrEmpty(s) ? s : Request.Url.Host;
        }

        /// <summary>
        /// return a json object with consumer information. this object is being used in the skin
        /// </summary>
        protected string ConsumerInfoJson
        {
            get
            {
                string value = "{}";
                if (CspId != -1)
                {
                    company consumer = CspContext.companies.First(a => a.companies_Id == CspId);
                    if (consumer != null)
                    {
                        company parentCompany = CspContext.companies.FirstOrDefault(a => a.companies_Id == (consumer.parent_companies_Id.HasValue ? consumer.parent_companies_Id.Value : -1));
                        value = (new
                                     {
                                         companyname = consumer.companyname,
                                         companies_Id = consumer.companies_Id,
                                         country = consumer.country,
                                         sId = (parentCompany != null) ? parentCompany.companies_Id : -1,
                                         sName = (parentCompany != null) ? parentCompany.companyname : "",
                                         lngDesc = Thread.CurrentThread.CurrentCulture.EnglishName
                                     }).ToJson();
                    }
                }
                return value;
            }
        }

        private void LogToEventLog(params object[] parameters)
        {
            string title = "LogToEventLog: ", message;
            if (parameters == null)
            {
                message = "";
            }
            else
            {
                title = parameters.Length > 1 ? parameters[0].ToString() : title;
                message = parameters.Aggregate(string.Empty, (current, parameter) => current == string.Empty ? parameter.ToString() : current + " | " + parameter);
            }

            var eventLog = new EventLogController();
            eventLog.AddLog(title, message, PortalSettings, -1, EventLogController.EventLogType.ADMIN_ALERT);
        }


        #region [Social Repository]
        /// <summary>
        /// Starts the listening the httprequest to authorize the social feature.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <author>Vu Dinh</author>
        /// <modified>02/12/2014 16:03:58</modified>
        protected void StartListening(HttpRequest request)
        {
            if (request["oauth_token"] != null || request["code"] != null)
            {
                var company = CspContext.companies.SingleOrDefault(a => a.companies_Id == CspId);
               
                if (company != null)
                {
                    var callbackUrl = string.Empty;
                    var socialType = (Common.Socialtype) 0;
                    string consumerKey = null, consumerSecret = null;
                    if (request["socialtype"] == Common.Socialtype.Twitter.ToString())
                    {
                        socialType = Common.Socialtype.Twitter;
                        consumerKey = TwitterConsumerKey;
                        consumerSecret = TwitterConsumerSecret;
                    }
                    else if (request["socialtype"] == Common.Socialtype.LinkedIn.ToString())
                    {
                        socialType = Common.Socialtype.LinkedIn;
                        consumerKey = LinkedInConsumerKey;
                        consumerSecret = LinkedInConsumerSecret;
                        callbackUrl = HttpContext.Current.Cache.Get("callbackurl" + PortalId + socialType).ToString();
                    }
                    else if (request["socialtype"] == Common.Socialtype.Facebook.ToString())
                    {
                        socialType = Common.Socialtype.Facebook;
                        consumerKey = FacebookConsumerKey;
                        consumerSecret = FacebookConsumerSecret;
                        callbackUrl = HttpContext.Current.Cache.Get("callbackurl" + PortalId + socialType).ToString();
                    }
                    var socialHelper = new SocialHelper(socialType, consumerKey, consumerSecret, callbackUrl);
                    var token = socialHelper.EndAuthenticationAndRegisterTokens();
                    if (token.TokenSecret==null && token.Token==null)
                    {
                        return;
                    }
                    //get followers of this social account
                    var numFollowers = Common.NumOfFollowers(socialType, token.ProfileResponse);

                    
                    var record = DnnContext.CSPTSMM_Partners.SingleOrDefault(a => a.CspId == company.companies_Id && a.PortalId == PortalId && a.SocialId == (int)socialType);

                    
                    if (record == null)
                    {
                        //insert new partner
                        DnnContext.CSPTSMM_Partners.InsertOnSubmit(new CSPTSMM_Partner
                            {
                                CspId = company.companies_Id,
                                Followers = numFollowers,
                                Token = token.Token,
                                TokenSecret = token.TokenSecret ?? string.Empty,
                                SubscribedAt = DateTime.Now,
                                IsSubscribed = true,
                                PortalId = PortalId,
                                SocialId = (int) socialType
                            });
                        //update partner token 
                        DnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                    }
                    if (record != null && record.IsSubscribed == false)
                    {
                        record.IsSubscribed = true;
                        record.Followers = numFollowers;
                        record.Token = token.Token;
                        record.TokenSecret = token.TokenSecret ?? string.Empty;
                        DnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict); 
                    }

                    //update token subscription info
                    var tokenSup =DnnContext.CSPTSMM_PartnersSubscriptionInfos.SingleOrDefault(a =>a.PartnerId == company.companies_Id && a.PortalId == PortalId &&a.SocialId == (int) socialType);
                    if (tokenSup == null)
                    {
                        //Insert new partner subscription info
                        DnnContext.CSPTSMM_PartnersSubscriptionInfos.InsertOnSubmit(new CSPTSMM_PartnersSubscriptionInfo
                        {
                            Id = Guid.NewGuid(),
                            LastRenewDate = DateTime.Now,
                            PartnerId = company.companies_Id,
                            PortalId = PortalId,
                            SocialId = (int)socialType,
                            NextRenewDate = (token.TokenExpireIn > 0) ? DateTime.Now.AddSeconds(token.TokenExpireIn) : (DateTime?) null
                        });
                        DnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                    }
                    else
                    {
                        tokenSup.LastRenewDate = DateTime.Now;
                        tokenSup.NextRenewDate = (token.TokenExpireIn > 0) ? DateTime.Now.AddSeconds(token.TokenExpireIn) : (DateTime?) null;
                        DnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                    }

                }
            }
            //vudinh 6/26/14 prevent dotnetnuke display exception bar return from socials subscribe with error in url
            if (request.Params["socialtype"] != null && request.Params["error"] != null)
            {
                Response.Redirect(Globals.NavigateURL(TabId, "WizardMode",string.Format("mid={0}&step={1}&hidepopup=1&socialtype={2}",request.Params["mid"],request.Params["step"],request.Params["socialtype"])));
            }

            //vudinh 7/14/14 add unsubscription social button
            //unsubscription social when click unsubscription button
            if (request["action"] != null && request["action"] == "unsubscription" && request["cspid"] != null)
            {
                var socialType = (int)((Common.Socialtype) Enum.Parse(typeof(Common.Socialtype), request["socialtype"]));
                var partner = DnnContext.CSPTSMM_Partners.FirstOrDefault(a => a.CspId == int.Parse(request["cspid"]) && a.PortalId == PortalId && a.SocialId == socialType);
                if (partner != null && partner.IsSubscribed)
                {
                    partner.IsSubscribed = false;
                    DnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                }
            }
        }

        /// <summary>
        /// Renders the social authentication button.
        /// </summary>
        /// <param name="socialtype">The socialtype.</param>
        /// <param name="company">The company.</param>
        /// <param name="wf">The wf.</param>
        /// <param name="controlId">The control id.</param>
        /// <returns>Control.</returns>
        protected Control RenderSocialAuthenticationButton(Common.Socialtype socialtype,company company, CSPSubscriptionModule_Workflow wf,string controlId)
        {
            var isSubscribed = false;
            if (company != null)
            {
                var record = DnnContext.CSPTSMM_Partners.SingleOrDefault(a => a.CspId == company.companies_Id && a.PortalId == PortalId && a.SocialId == (int)socialtype);
                if (record != null && record.IsSubscribed)
                {
                    isSubscribed = true;
                }
            }

            //vudinh 7/14/14 add unsubscription social button
            Button input = null;
            if (isSubscribed) //Render unsubscription button
            {
                var unsubscriptionLink = Globals.NavigateURL(TabId, "WizardMode",String.Format("mid={0}&step={1}&hidepopup=1&socialtype={2}&cspid={3}&action=unsubscription",ModuleId, GetCurrentFormStep(wf), socialtype, company.companies_Id));
                input = new Button
                {
                    Text = GetString(string.Format("Button.Unsubscribe{0}", socialtype)),
                    ID = controlId,
                    CssClass = string.Format("csp-button-social csp-button-{0} unsubscribe-button", socialtype),
                    OnClientClick = "window.location = '" + unsubscriptionLink + "'; return false;",
                };
            }
            else //render subscription button
            {
                var postBackUrl = Globals.NavigateURL(TabId, "WizardMode", String.Format("mid={0}&step={1}&hidepopup=1&socialtype={2}", ModuleId, GetCurrentFormStep(wf), socialtype));
                SocialHelper socialHelper = null;
                switch (socialtype)
                {

                    case Common.Socialtype.Twitter:
                        socialHelper = new CR.SocialLib.SocialHelper(socialtype, TwitterConsumerKey, TwitterConsumerSecret, postBackUrl);
                        socialHelper.BeginAuthentication();
                        break;
                    case Common.Socialtype.LinkedIn:
                        socialHelper = new CR.SocialLib.SocialHelper(socialtype, LinkedInConsumerKey, LinkedInConsumerSecret, postBackUrl);
                        HttpContext.Current.Cache.Add("callbackurl" + PortalId + socialtype, postBackUrl, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(24, 0, 0), System.Web.Caching.CacheItemPriority.Default, null);
                        break;
                    case Common.Socialtype.Facebook:
                        socialHelper = new SocialHelper(socialtype, FacebookConsumerKey, FacebookConsumerSecret, postBackUrl);
                        HttpContext.Current.Cache.Add("callbackurl" + PortalId + socialtype, postBackUrl, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(24, 0, 0), System.Web.Caching.CacheItemPriority.Default, null);
                        break;
                }

                input = new Button
                {
                    Text = GetString(string.Format("Button.Follow{0}", socialtype)),
                    ID = controlId,
                    CssClass = string.Format("csp-button-social csp-button-{0}", socialtype),
                    OnClientClick = "window.location = '" + socialHelper.GetAuthenticationLink() + "'; return false;",
                };
            }
           
            return input;
        }

        /// <summary>
        /// Gets the current form step.
        /// </summary>
        /// <param name="wf">The wf.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 3/1/2013 - 8:32 AM
        private int GetCurrentFormStep(CSPSubscriptionModule_Workflow wf)
        {
            var wfs = DnnContext.CSPSubscriptionModule_Workflows.Where(a => a.PortalId == PortalId &&a.TabModuleId == TabModuleId).OrderBy(a=>a.ParentFormId).ToList();
            var list = new List<Guid>();
            var rootwf = DnnContext.CSPSubscriptionModule_Workflows.FirstOrDefault(a => a.ParentFormId == null && a.PortalId == PortalId && a.TabModuleId == TabModuleId);
            if (rootwf != null)
            {
                list.Add(rootwf.CurrentForm.FormId);
            }
            for (int i = 0; i < wfs.Count; i++)
            {  
                var wft = DnnContext.CSPSubscriptionModule_Workflows.FirstOrDefault(a => a.ParentFormId == list[i] && a.PortalId == PortalId &&
                               a.TabModuleId == TabModuleId);
                if (wft ==null)
                    continue;
                list.Add(wft.CurrentForm.FormId);
            }
            foreach (var guid in list)
            {
                if (wf.FormId == guid) return list.IndexOf(guid) + 2;
            }
            return 1;
            /* for (int i = 0; i < wfs.Count; i++)
            {
                if (wfs[i].CurrentForm == wf.CurrentForm)
                    return i + 2;
            }
            return 1;*/
        }
        #endregion
    }
}