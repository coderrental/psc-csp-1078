﻿namespace TIEKinetix.CspModules.CSPSubscriptionModule.Components
{
	public class ModuleKeys
	{
		public const string MANAGE_FORM_KEY_NAME = "manageform";
		public const string MANAGE_CODELIST_KEY_NAME = "managecodelist";
		public const string MANAGE_WORKFLOW_NAME = "manageworkflow";
	    public const string MANAGE_CLUSTER = "managecluster";
        public const string MANAGE_UPLOAD_FIELD = "manageuploadfield";
	    public const string MANAGE_CUSTOM_DOMAIN_PARAMS = "managecustomdomainparams";
        public const string DEFAULT_LANGUAGE_ID = "1";
	    public const string TWITTER_USER_ROLE = "Csp_Tweet_User_Role";
	}
}