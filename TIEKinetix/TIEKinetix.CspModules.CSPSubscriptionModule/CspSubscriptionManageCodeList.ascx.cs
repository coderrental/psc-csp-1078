﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using TIEKinetix.CspModules.CSPSubscriptionModule.Components;
using TIEKinetix.CspModules.CSPSubscriptionModule.Components.Data;
using Telerik.Web.UI;

namespace TIEKinetix.CspModules.CSPSubscriptionModule
{
	public partial class CspSubscriptionManageCodeList : DnnModuleBase 
	{
		private DnnCspSubscriptionModuleContext _dnnContext;

        

		/// <summary>
		/// Handles the Init event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected override void Page_Init(object sender, EventArgs e)
		{
			base.Page_Init(sender, e);
			_dnnContext = new DnnCspSubscriptionModuleContext(Config.GetConnectionString());
		}


		/// <summary>
		/// Handles the OnItemCreated event of the rgCodelist control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Telerik.Web.UI.GridItemEventArgs"/> instance containing the event data.</param>
		protected void rgCodelist_OnItemCreated(object sender, GridItemEventArgs e)
		{
			if (e.Item is GridNestedViewItem)
			{
				var item = (GridNestedViewItem) e.Item;
				var gridCodeListValue = (RadGrid) item.FindControl("rgCodeListValue");
				var id = (Guid) item.ParentItem.GetDataKeyValue("CodeListId");
				gridCodeListValue.DataSource = _dnnContext.CSPSubscriptionModule_CodeListValues.Where(a => a.CodeListId == id).ToList();
			}
		}

		protected void rgCodelist_OnItemCommand(object sender, GridCommandEventArgs e)
		{
			var id = Guid.Empty;
			var value = new List<object>();
			switch (e.CommandName)
			{
				case RadGrid.PerformInsertCommandName: //Insert new code list
					value = GetAutoGeneratedColumns(e.Item);
					_dnnContext.CSPSubscriptionModule_CodeLists.InsertOnSubmit(new CSPSubscriptionModule_CodeList
					                                                           	{
																					CodeListId = Guid.NewGuid(),
					                                                           		Name = value[0].ToString()
					                                                           	});

					_dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
					rgCodelist.Rebind();
					break;

				case RadGrid.DeleteCommandName: //Delete code list
					id = (Guid) (e.Item as GridEditableItem).GetDataKeyValue("CodeListId");
					//Delete code list value
					_dnnContext.CSPSubscriptionModule_CodeListValues.DeleteAllOnSubmit(_dnnContext.CSPSubscriptionModule_CodeListValues.Where(a => a.CodeListId == id));
					//Delete code list
					_dnnContext.CSPSubscriptionModule_CodeLists.DeleteAllOnSubmit(_dnnContext.CSPSubscriptionModule_CodeLists.Where(a => a.CodeListId == id));
					_dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
					rgCodelist.Rebind();
					break;

				case RadGrid.UpdateCommandName: //Edit code list
					id = (Guid) (e.Item as GridEditableItem).GetDataKeyValue("CodeListId");
					value = GetAutoGeneratedColumns(e.Item);
					var item = _dnnContext.CSPSubscriptionModule_CodeLists.SingleOrDefault(a => a.CodeListId == id);
					if (item != null)
					{
						item.Name = value[0].ToString();
						_dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
					}
					break;
			}
		}

		/// <summary>
		/// Handles the OnItemCommand event of the rgCodeListValue control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Telerik.Web.UI.GridCommandEventArgs"/> instance containing the event data.</param>
		protected void rgCodeListValue_OnItemCommand(object sender, GridCommandEventArgs e)
		{
			var id = Guid.Empty;
			var value = new List<object>();
			switch (e.CommandName)
			{
				case RadGrid.DeleteCommandName: //Delete code list value
					id = (Guid) (e.Item as GridEditableItem).GetDataKeyValue("CodeListValueId");
					_dnnContext.CSPSubscriptionModule_CodeListValues.DeleteAllOnSubmit(_dnnContext.CSPSubscriptionModule_CodeListValues.Where(a => a.CodeListValueId == id));
					_dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
					rgCodelist.Rebind();
					break;
				case RadGrid.PerformInsertCommandName: //Insert new code list value
					var codeListId = new Guid(((HiddenField)e.Item.OwnerTableView.OwnerGrid.Parent.FindControl("codelistId")).Value);
					value = GetAutoGeneratedColumns(e.Item);
					_dnnContext.CSPSubscriptionModule_CodeListValues.InsertOnSubmit(new CSPSubscriptionModule_CodeListValue
					                                                                	{
																							CodeListValueId = Guid.NewGuid(),
																							CodeListId = codeListId,
					                                                                		Name = value[0].ToString(),
																							Value = value[1].ToString()
					                                                                	});
					_dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
					rgCodelist.Rebind();
					break;
				case  RadGrid.UpdateCommandName : // Edit code list value
					id = (Guid) (e.Item as GridEditableItem).GetDataKeyValue("CodeListValueId");
					value = GetAutoGeneratedColumns(e.Item);
					var item = _dnnContext.CSPSubscriptionModule_CodeListValues.SingleOrDefault(a => a.CodeListValueId == id);
					if (item != null)
					{
						item.Name = value[0].ToString();
						item.Value = value[1].ToString();
						_dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
					}
					break;
			}
		}

		/// <summary>
		/// get all input for auto-generated fields from Grid.
		/// </summary>
		/// <param name="item">Grid Item</param>
		/// <returns></returns>
		private List<object> GetAutoGeneratedColumns(GridItem item)
		{
			var gridEditableItem = (GridEditableItem)item;
			var gridEditManager = gridEditableItem.EditManager;
			var values = new List<object>();
			foreach (GridColumn column in item.OwnerTableView.RenderColumns)
			{
				// only insert what can edit
				if (column is IGridEditableColumn)
				{
					var editableColumn = (IGridEditableColumn)column;
					if (editableColumn.IsEditable)
					{
						IGridColumnEditor columnEditor = gridEditManager.GetColumnEditor(editableColumn);
						object value = null;

						if (columnEditor is GridTextColumnEditor)
						{
							value = (columnEditor as GridTextColumnEditor).Text;
						}
						else if (columnEditor is GridBoolColumnEditor)
						{
							value = (columnEditor as GridBoolColumnEditor).Value;
						}
						else if (columnEditor is GridDropDownColumnEditor)
						{
							value = (columnEditor as GridDropDownColumnEditor).SelectedValue;
						}
						if (value != null)
						{
							values.Add(value);
						}
					}
				}
			}
			return values;
		}

		/// <summary>
		/// Handles the OnNeedDataSource event of the rgCodelist control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Telerik.Web.UI.GridNeedDataSourceEventArgs"/> instance containing the event data.</param>
		protected void rgCodelist_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
		{
			rgCodelist.DataSource = _dnnContext.CSPSubscriptionModule_CodeLists.ToList();
		}
    }
}