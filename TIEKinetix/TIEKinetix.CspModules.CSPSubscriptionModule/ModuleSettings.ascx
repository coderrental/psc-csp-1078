﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleSettings.ascx.cs" Inherits="TIEKinetix.CspModules.CSPSubscriptionModule.ModuleSettings" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>
<div class="dnnFormItem">
    <dnn:Label ID="lblRedirectAfterFinish" runat="server" ControlName="tbTabName"/>
    <asp:TextBox runat="server" ID="tbTabName"/>
</div>
<div class="dnnFormItem">
    <dnn:Label ID="lblSupplierId" runat="server" ControlName="tbSupplierId" />
    <asp:TextBox runat="server" ID="tbSupplierId"/>
</div>
<div class="dnnFormItem">
    <dnn:Label ID="lblTwitterConsumerKey" runat="server" ControlName="tbTwitterConsumerKey" />
    <asp:TextBox runat="server" ID="tbTwitterConsumerKey"/>
</div>
<div class="dnnFormItem">
    <dnn:Label ID="lblTwitterConsumerSecret" runat="server" ControlName="tbTwitterConsumerSecret" />
    <asp:TextBox runat="server" ID="tbTwitterConsumerSecret"/>
</div>
<div class="dnnFormItem">
    <dnn:Label ID="lblLinkedConsumerKey" runat="server" ControlName="tbLinkedInConsumerKey" />
    <asp:TextBox runat="server" ID="tbLinkedInConsumerKey"/>
</div>
<div class="dnnFormItem">
    <dnn:Label ID="lblLinkedConsumerSecret" runat="server" ControlName="tbLinkedInConsumerSecret" />
    <asp:TextBox runat="server" ID="tbLinkedInConsumerSecret"/>
</div>
<div class="dnnFormItem">
    <dnn:Label ID="lblFacebookConsumerKey" runat="server" ControlName="tbFacebookConsumerKey" />
    <asp:TextBox runat="server" ID="tbFacebookConsumerKey"/>
</div>
<div class="dnnFormItem">
    <dnn:Label ID="lblFacebookConsumerSecret" runat="server" ControlName="tbFacebookConsumerSecret" />
    <asp:TextBox runat="server" ID="tbFacebookConsumerSecret"/>
</div>
<div class="dnnFormItem">
    <dnn:Label ID="lblApproveTweetParameterName" runat="server" ControlName="tbApproveTweetParameterName" />
    <asp:TextBox runat="server" ID="tbApproveTweetParameterName"></asp:TextBox>
</div>
<div class="dnnFormItem">
    <dnn:Label ID="lblHideMailingCheckbox" runat="server" ControlName="cbHideMailingCheckbox" />
    <asp:CheckBox runat="server" ID="cbHideMailingCheckbox"></asp:CheckBox>
</div>
<div class="dnnFormItem">
    <dnn:Label ID="lbManualOptIn" runat="server" ControlName="cbManualOptIn" />
    <asp:CheckBox runat="server" ID="cbManualOptIn"></asp:CheckBox>
</div>
<div class="dnnFormItem">
    <dnn:Label ID="lbCustomBaseParamsForThemes" runat="server" ControlName="tbCustomBaseParamsForThemes" />
    <asp:TextBox runat="server" ID="tbCustomBaseParamsForThemes"></asp:TextBox>
</div>
<div class="dnnFormItem">
    <dnn:Label ID="lbThemeRoleAssociation" runat="server" ControlName="tbThemeRoleAssociation" />
    <asp:TextBox runat="server" ID="tbThemeRoleAssociation"></asp:TextBox>
</div>
<div class="dnnFormItem">
    <dnn:Label ID="lbDefaultCountry" runat="server" ControlName="tbDefaultCountry" />
    <asp:TextBox runat="server" ID="tbDefaultCountry"></asp:TextBox>
</div>
<div class="dnnFormItem">
    <dnn:Label ID="lbProjectName" runat="server" ControlName="tbProjectName" />
    <asp:TextBox runat="server" ID="tbProjectName"></asp:TextBox>
</div>
<div class="dnnFormItem">
    <dnn:Label ID="lbProjectMode" runat="server" ControlName="tbProjectMode" />
    <asp:TextBox runat="server" ID="tbProjectMode"></asp:TextBox>
</div>
<div class="dnnFormItem">
    <dnn:Label ID="lbGenerateDeeplink" runat="server" ControlName="cbGenerateDeeplink" />
    <asp:CheckBox runat="server" ID="cbGenerateDeeplink"></asp:CheckBox>
</div>