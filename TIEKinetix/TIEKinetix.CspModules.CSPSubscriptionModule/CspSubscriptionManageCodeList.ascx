﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CspSubscriptionManageCodeList.ascx.cs" Inherits="TIEKinetix.CspModules.CSPSubscriptionModule.CspSubscriptionManageCodeList" %>
<%@ Register Assembly="Telerik.Web.UI, Version=2012.1.411.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4"
	Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<telerik:Radwindowmanager runat="server" ID="windowManager"></telerik:Radwindowmanager>
<telerik:Radgrid runat="server" ID="rgCodelist" AutoGenerateColumns="False" OnItemCreated="rgCodelist_OnItemCreated" OnNeedDataSource="rgCodelist_OnNeedDataSource" OnItemCommand="rgCodelist_OnItemCommand">
	<MasterTableView DataKeyNames="CodeListId" CommandItemDisplay="Top" Name="CodeList" EditMode="InPlace">
		<Columns>
            <telerik:GridBoundColumn DataField="CodeListId" UniqueName="CodeListId" HeaderText="ID" ReadOnly="true" Visible="False" ForceExtractValue="Always">
            	<HeaderStyle Width="250"></HeaderStyle>
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Name" HeaderText="Code List Name" />
			<telerik:GridEditCommandColumn ButtonType="ImageButton" >
				<HeaderStyle Width="100"></HeaderStyle>
			</telerik:GridEditCommandColumn>
            <telerik:GridButtonColumn ConfirmText='Delete this Code list? This will also remove Code list values.' ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete">
            	<HeaderStyle Width="100"></HeaderStyle>
			</telerik:GridButtonColumn>                  
        </Columns>
		<NestedViewTemplate>
			<asp:Panel ID="FieldContainer" runat="server">
				<asp:HiddenField runat="server" ID="codelistId" Value='<%#Eval("CodeListId") %>' />
				<telerik:RadGrid ID="rgCodeListValue" runat="server" OnItemCommand="rgCodeListValue_OnItemCommand">
					<MasterTableView AutoGenerateColumns="false" DataKeyNames="CodeListValueId,CodeListId" CommandItemDisplay="Top" EditMode="InPlace">
						<Columns>
							<telerik:GridBoundColumn DataField="CodeListValueId" UniqueName="CodeListValueId" Visible="False" HeaderText="ID" ReadOnly="true" ForceExtractValue="Always">
								<HeaderStyle Width="250"></HeaderStyle>
							</telerik:GridBoundColumn>
							<telerik:GridBoundColumn HeaderText="Name" HeaderButtonType="TextButton" DataField="Name" UniqueName="Name" />
							<telerik:GridBoundColumn HeaderText="Value" HeaderButtonType="TextButton" DataField="Value" UniqueName="Value"/>
							<telerik:GridEditCommandColumn ButtonType="ImageButton">
								<HeaderStyle Width="100"></HeaderStyle>
							</telerik:GridEditCommandColumn>
							<telerik:GridButtonColumn ConfirmText='Delete this Code list value?' ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete">
								<HeaderStyle Width="100"></HeaderStyle>
							</telerik:GridButtonColumn>
						</Columns>
						<EditFormSettings>
							<EditColumn ButtonType="ImageButton"></EditColumn>
						</EditFormSettings>        
						<CommandItemSettings AddNewRecordText="Add new Code list value"></CommandItemSettings>
					</MasterTableView>
				</telerik:RadGrid>                
			</asp:Panel>
		</NestedViewTemplate>
		<CommandItemSettings AddNewRecordText="Add new Code list"></CommandItemSettings>
		<EditFormSettings>
			<EditColumn ButtonType="ImageButton"></EditColumn>
		</EditFormSettings>
	</MasterTableView>
</telerik:Radgrid>