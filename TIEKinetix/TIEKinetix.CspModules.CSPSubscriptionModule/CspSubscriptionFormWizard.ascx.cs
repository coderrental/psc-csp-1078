﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using CR.SocialLib;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Data;
using DotNetNuke.Entities.Controllers;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Entities.Tabs;
using DotNetNuke.Entities.Users;
using DotNetNuke.Instrumentation;
using DotNetNuke.Security;
using DotNetNuke.Security.Roles;
using DotNetNuke.Services.Localization;
using DotNetNuke.Services.Mail;
using DotNetNuke.UI.UserControls;
using CR.SocialLib.OAuth;
using TIEKinetix.CspModules.CSPSubscriptionModule.Components;
using TIEKinetix.CspModules.CSPSubscriptionModule.Components.Data;
using Telerik.Web.UI;
using CSPSubscriptionModule_Field = TIEKinetix.CspModules.CSPSubscriptionModule.Components.Data.CSPSubscriptionModule_Field;
using CSPSubscriptionModule_Form = CR.ContentObjectLibrary.Data.CSPSubscriptionModule_Form;
using CSPSubscriptionModule_Workflow = TIEKinetix.CspModules.CSPSubscriptionModule.Components.Data.CSPSubscriptionModule_Workflow;
using CSPTSMM_Partner = TIEKinetix.CspModules.CSPSubscriptionModule.Components.Data.CSPTSMM_Partner;

namespace TIEKinetix.CspModules.CSPSubscriptionModule
{
    public partial class CspSubscriptionFormWizard : DnnModuleBase, IActionable
    {
        private Dictionary<string, string> Countries;
        private Dictionary<string, string> _customParamsForThemes = new Dictionary<string, string>();
        private string _errorMessage = "";

        private string _externalIdentifier = string.Empty;
        private Channel _channel;


        #region Overrides of DnnModuleBase
        protected override void Page_Init(object sender,EventArgs e)
        {
            base.Page_Init(sender,e);

            if (!IsPostBack)
            {
                StartListening(Request); //for social authentication
            }

            AddWizardForm();

            _externalIdentifier = GetPortalExternalIdentifier();
            _channel = CspContext.Channels.FirstOrDefault(a => a.ExternalIdentifier == _externalIdentifier);

            #region load country list from Dnn cache

            Countries = DataCache.GetCache("CspCountries") as Dictionary<string, string>;
            if (Countries == null || Countries.Count == 0)
            {
                Countries = new Dictionary<string, string>();
                try
                {
                    var reader =
                        (SqlDataReader)
                        DataProvider.Instance().ExecuteSQL(
                            "select Name, Code from [CSPSubscriptionModule_Country] order by DisplayOrder asc");
                    while (reader.Read())
                    {
                        Countries.Add(reader["Name"].ToString(), reader["Code"].ToString());
                    }

                    DataCache.SetCache("CspCountries", Countries);
                }
                catch
                {
                    //ignore
                    DnnLog.Warn("Unable to load country list in DNN");
                }
            }

            if (Countries.Count > 0)
            {
                foreach (ListItem li in Countries.Select(country => new ListItem(country.Key, country.Value)))
                {
                    // ltu 5/8/14 added a default country
                    if (Settings.ContainsKey(ModuleSetting.DefaultCountry) && !string.IsNullOrEmpty(Settings[ModuleSetting.DefaultCountry].ToString()))
                    {
                        if (li.Text.IndexOf(Settings[ModuleSetting.DefaultCountry].ToString(), StringComparison.CurrentCultureIgnoreCase) == 0)
                            li.Selected = true;
                    }
                    ddlCountry.Items.Add(li);
                }
            }

            #endregion

            #region load custom base params for themes into a dictionary
            // ltu 4/15/14 first we will use the CustomParameters col in the ChannelThemeAssocation to determine the parameters
            // if it doesnt exists, then we will use the module settings
            try
            {
                var result = CspContext.ExecuteQuery<NameValuePair>("select ThemeId as 'Key', CustomParameters as 'Value' from ChannelThemeAssocation where ChannelId = {0}", _channel.Id).ToList();
                _customParamsForThemes = result.Where(a=>!string.IsNullOrEmpty(a.Value)).Distinct().ToDictionary(a => a.Key.ToString(), a => a.Value);
            }
            catch(Exception ex)
            {
                // ignore exception due to the missing column name
                DnnLog.Warn(ex.Message, ex);
            }

            if (_customParamsForThemes.Count == 0 && Settings[ModuleSetting.CustomBaseParamsForThemes] != null)
            {
                // default delimiter is semi colon (;)
                try
                {
                    var list = Settings[ModuleSetting.CustomBaseParamsForThemes]
                        .ToString()
                        .Split(new char[] {';'}, StringSplitOptions.RemoveEmptyEntries);
                    if (list.Length%2 == 0 && list.Length > 0)
                    {
                        for (int i = 0; i < list.Length; i = i + 2)
                        {
                            _customParamsForThemes.Add(list[i], list[i + 1]);
                        }
                    }

                }
                catch
                {
                    // ignore parsing error
                }
            }
            #endregion

            // translation
            formWizard.WizardSteps[0].Title = GetString("FormWizard.CompanyInfo_Title");
            LocalizePage();
        }

        /// <summary>
        /// Adds the wizard form.
        /// </summary>
        private void AddWizardForm()
        {
            int tmp = TabModuleId;
            List<CSPSubscriptionModule_Form> subscriptionForms = new List<CSPSubscriptionModule_Form>();
            using (DnnDataContext dnnDataContext = new DnnDataContext(Config.GetConnectionString()))
            {
                //Get form ordered list
                CR.DnnModules.Common.Utils.GetFormTree(dnnDataContext, Guid.Empty, subscriptionForms, PortalId,
                                                       TabModuleId);
            }

            if (subscriptionForms.Any())
            {
                for (int i = -1; i < subscriptionForms.Count; i++)
                {
                    CSPSubscriptionModule_Workflow wf = new CSPSubscriptionModule_Workflow();
                    //Add first form
                    if (i == -1)
                        wf =
                            DnnContext.CSPSubscriptionModule_Workflows.FirstOrDefault(
                                a => a.ParentFormId == null && a.PortalId == PortalId && a.TabModuleId == TabModuleId);
                    else
                        wf =
                            DnnContext.CSPSubscriptionModule_Workflows.FirstOrDefault(
                                a =>
                                a.ParentFormId == subscriptionForms[i].FormId && a.PortalId == PortalId &&
                                a.TabModuleId == TabModuleId);
                    if (wf == null)
                        continue;
                    var wizardStep = new WizardStep();
                    wizardStep.ID = wf.WorkflowId.ToString();
                    wizardStep.Title =
                        GetTranslation(wf.FormId.HasValue ? wf.FormId.Value : wf.WorkflowId, wf.CurrentForm.Name).
                            ShortDescription;
                    wizardStep.Controls.Add(CreateWizardControl(wf));
                    formWizard.WizardSteps.Add(wizardStep);
                }
            }
        }

        /// <summary>
        /// wrap try/catch around SubmitChanges to log errors
        /// </summary>
        private void SubmitCspChanges()
        {
            try
            {
                CspContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
            }
            catch (Exception ex) // log error to debug
            {
                DnnLog.Error(ex.Message, ex);
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        /// <summary>
        /// save or create csp company record
        ///     - default consumer parameter
        ///     - company consumer table
        ///     - company contact
        ///     - company theme
        ///     - consumer theme
        /// </summary>
        /// <returns></returns>
        private bool CreateOrUpdateCompany()
        {
            _errorMessage = string.Empty;

            // server side validation
            Page.Validate("CompanyInformation");
            if (!Page.IsValid)
                return false;

            bool bSuccess = true;
            if (_channel == null)
            {
                Debug("Channel cannot be null. External Id = [" + _externalIdentifier + "]");
                return false;
            }

            // contact record
            string phone = string.Empty;
            if (!string.IsNullOrEmpty(UserInfo.Profile.GetPropertyValue("Telephone")))
            {
                phone = UserInfo.Profile.GetPropertyValue("Telephone");
                if (!string.IsNullOrEmpty(phone))
                {
                    if (phone.Length > 12) phone = phone.Substring(0, 12);
                }
            }

            var comId = 0;

            if (CspId == -1) // new user
            {
                // company record
                string tempName = tbCompanyName.Text.Replace("\"", "'");
                if (tempName.Length > 75)
                    tempName = tempName.Substring(0, 75);
                var cspCompany = new company
                    {
                        companyname = tempName,
                        displayname = tempName,
                        address1 = tbAddress1.Text,
                        address2 = "",
                        city = tbCity.Text,
                        state = tbState.Text.Length > 20 ? tbState.Text.Substring(0, 20) : tbState.Text,
                        zipcode = tbZip.Text,
                        country = ddlCountry.SelectedValue,
                        created = DateTime.Now,
                        date_changed = DateTime.Now,
                        website_url = tbUrl.Text,
                        parent_companies_Id = _channel.CompanyId,
                        is_consumer = true,
                        is_supplier = false
                    };

                cspCompany.companies_contacts.Add(new companies_contact
                    {
                        company = cspCompany,
                        emailaddress = UserInfo.Email,
                        firstname = UserInfo.FirstName,
                        lastname = UserInfo.LastName,
                        user_Id = Guid.Empty,
                        telephone = phone
                    });

                // consumer profile
                language lng = CspContext.languages.FirstOrDefault(a => a.BrowserLanguageCode == CurrentLanguage && a.active.HasValue && a.active.Value) ??
                               CspContext.languages.First(a => a.active.HasValue && a.active.Value);
                var baseDomain = DateTime.Now.Ticks + "." + _channel.BaseDomain;
                cspCompany.companies_consumers.Add(new companies_consumer
                    {
                        company = cspCompany,
                        active = true,
                        base_domain = baseDomain,
                        base_publication_Id = _channel.BasePublicationId,
                        base_publication_parameters = _channel.DefaultParameters,
                        default_language_Id = lng.languages_Id,
                        fallback_language_Id = lng.languages_Id
                    });

                // default parameters
                companies_parameter_type paramDefinition = null;
                foreach (
                    CSPSubscriptionModule_DefaultField defaultField in DnnContext.CSPSubscriptionModule_DefaultFields)
                {
                    paramDefinition =
                        CspContext.companies_parameter_types.FirstOrDefault(a => a.parametername == defaultField.Name);
                    if (paramDefinition != null)
                    {
                        cspCompany.companies_parameters.Add(new companies_parameter
                            {
                                company = cspCompany,
                                companies_parameter_types_Id =
                                    paramDefinition.companies_parameter_types_Id,
                                value_text =
                                    !string.IsNullOrEmpty(defaultField.Value)
                                        ? defaultField.Value
                                        : Utils.GetSetting(PortalId,
                                                           defaultField.Value)
                            });
                    }
                }
                // company theme
                string themeIds = string.Empty;
                foreach (
                    ChannelThemeAssocation themeAssocation in
                        CspContext.ChannelThemeAssocations.Where(a => a.ChannelId == _channel.Id))
                {
                    cspCompany.companies_themes.Add(new companies_theme
                        {
                            company = cspCompany,
                            themes_Id = themeAssocation.ThemeId
                        });
                    themeIds += themeAssocation.ThemeId + ",";
                }

                // todo: consumer theme
				// consumer is not needed since the company theme table has a trigger that handles consumer theme automatically

                CspContext.companies.InsertOnSubmit(cspCompany);
                SubmitCspChanges();


                #region @ltu 03/11/2014 check the manual opt-in flag, if it is true, then we need to remove the optional categories from the consumer themes

                if (!string.IsNullOrEmpty(themeIds) && Settings[ModuleSetting.ManualOptIn] != null && !string.IsNullOrEmpty(Settings[ModuleSetting.ManualOptIn] as string))
                {
                    bool fManualOptIn = bool.Parse(Settings[ModuleSetting.ManualOptIn].ToString());
                    if (fManualOptIn)
                    {
                        if (themeIds.EndsWith(","))
                            themeIds = themeIds.Substring(0, themeIds.Length - 1);
                        CspContext.ExecuteCommand(@"
                            delete ct
                            from categories c
	                            join consumers_themes ct on ct.category_Id=c.categoryId
                            where (c.IsSubscribable is null or c.IsSubscribable = 1) and (ct.themes_Id in (" + themeIds + @")) and (ct.consumer_Id={0}) and (ct.supplier_Id={1})
                        ", cspCompany.companies_Id, _channel.CompanyId);
                    }
                }

                #endregion


                // save contact person information
                bSuccess = bSuccess &&
                           SaveConsumerParameter(cspCompany, "Personalize_Contact_Firstname", tbFirstName.Text);
                bSuccess = bSuccess &&
                           SaveConsumerParameter(cspCompany, "Personalize_Contact_Lastname", tbLastName.Text);
                bSuccess = bSuccess &&
                           SaveConsumerParameter(cspCompany, "Personalize_Contact_Jobtitle", tbJobTitle.Text);
                bSuccess = bSuccess &&
                           SaveConsumerParameter(cspCompany, "Personalize_Contact_Emailaddress", tbEmail.Text);

                #region  Sync with VR

                // legacy systems require ContactUsEmail field
                // ltu 10/13/2013 Write value to ContactUsEmail parameter for VR
                SaveConsumerParameter(cspCompany, "ContactUsEmail", tbEmail.Text);
                SaveConsumerParameter(cspCompany, "Mailing_Companyname", tbCompanyName.Text);
                SaveConsumerParameter(cspCompany, "Mailing_Website", tbUrl.Text);
                SaveConsumerParameter(cspCompany, "Mailing_Phone", tbPhoneNumber.Text);
                bool bGenerateDeeplink = true;

                if (Settings.ContainsKey(ModuleSetting.GenerateDeeplink) && !string.IsNullOrEmpty(Settings[ModuleSetting.GenerateDeeplink].ToString()))
                    bGenerateDeeplink = bool.Parse(Settings[ModuleSetting.GenerateDeeplink].ToString());

                if (bGenerateDeeplink  && Settings.ContainsKey(ModuleSetting.ProjectName) && !string.IsNullOrEmpty(Settings[ModuleSetting.ProjectName].ToString()))
                {
                    SaveConsumerParameter(cspCompany, "Deeplink_Landingpage_Url", "http://p" + Settings[ModuleSetting.ProjectName].ToString() + "-c" + baseDomain + "/");
                }
                else if (bGenerateDeeplink)
                {
                    SaveConsumerParameter(cspCompany, "Deeplink_Landingpage_Url", "http://" + baseDomain + "/");
                }
                

                #endregion

                #region ltu 07/01/2014 Write value to DNN_Portal_Id parameter for Secured Intranet Asset

                // legacy systems require ContactUsEmail field
                SaveConsumerParameter(cspCompany, "DNN_Portal_Id", PortalId.ToString());

                #endregion

                bSuccess = bSuccess &&
                           SaveConsumerParameter(cspCompany, "Personalize_Contact_Phonenumber", tbPhoneNumber.Text);

                // save agree to receive mailings
                bSuccess = bSuccess &&
                           SaveConsumerParameter(cspCompany, "Agree_Receiving_Mailings",
                                                 cbAgreeToMailings.Checked.ToString());

                // also save first name, last name and telephone number if these are empty
                if (string.IsNullOrEmpty(UserInfo.Profile.FirstName))
                {
                    UserInfo.Profile.FirstName = tbFirstName.Text;
                }
                if (string.IsNullOrEmpty(UserInfo.Profile.LastName))
                {
                    UserInfo.Profile.LastName = tbLastName.Text;
                }
                if (string.IsNullOrEmpty(UserInfo.Profile.Telephone))
                {
                    UserInfo.Profile.Telephone = tbPhoneNumber.Text;
                }

                // save the integration key
                UserInfo.Profile.SetProfileProperty(Keys.CspIntegrationKey, cspCompany.companies_Id.ToString());
                UserController.UpdateUser(PortalId, UserInfo);
                comId = cspCompany.companies_Id;
                // send notification
                SendEmailNotification(cspCompany);

                #region Sync with eCommerce system
                try
                {
                    string tieCoreAdminCs = Config.GetConnectionString("TIECoreAdmin");
                    if (!string.IsNullOrEmpty(tieCoreAdminCs))
                    {
                        string tieCoreAdminSystemId = Utils.GetSetting(PortalId, "TieCoreAdminSystemId");
                        string tieCoreAdminProjectId = Utils.GetSetting(PortalId, "TieCoreAdminProjectId");

                        if (!string.IsNullOrEmpty(tieCoreAdminProjectId) && !string.IsNullOrEmpty(tieCoreAdminSystemId))
                        {
                            try
                            {
                                using (SqlConnection connection = new SqlConnection(tieCoreAdminCs))
                                {
                                    connection.Open();
                                    using (SqlCommand cmd = new SqlCommand("", connection))
                                    {
                                        cmd.CommandText = @"
declare @id uniqueidentifier = newid()
insert into on_demand_process(on_demand_process_id,command_name,project_id,subscriber_id,status_code,source,date_created)
values(@id,'NewConsumer',@projectId,@subscriberId,0,'DNN Subscriber Form',getdate())
insert into on_demand_process_parameter(on_demand_process_parameter_id,on_demand_process_id,parameter_name,parameter_value,date_created)
values(NEWID(), @id, 'companyId',@companyId,GETDATE())
insert into on_demand_process_parameter(on_demand_process_parameter_id,on_demand_process_id,parameter_name,parameter_value,date_created)
values(NEWID(), @id, 'cs',@cs,GETDATE())
insert into on_demand_process_parameter(on_demand_process_parameter_id,on_demand_process_id,parameter_name,parameter_value,date_created)
values(NEWID(), @id, 'systemId',@systemId,GETDATE())
insert into on_demand_process_parameter(on_demand_process_parameter_id,on_demand_process_id,parameter_name,parameter_value,date_created)
values(NEWID(), @id, 'projectId',@projectId,GETDATE())
";
                                        cmd.Parameters.AddWithValue("@projectId", tieCoreAdminProjectId);
                                        cmd.Parameters.AddWithValue("@subscriberId", Guid.Empty);
                                        cmd.Parameters.AddWithValue("@companyId", cspCompany.companies_Id);
                                        cmd.Parameters.AddWithValue("@cs",
                                                                    Config.GetConnectionString("CspPortal" + PortalId));
                                        cmd.Parameters.AddWithValue("@systemId", tieCoreAdminSystemId);

                                        cmd.ExecuteNonQuery();
                                    }
                                    connection.Close();
                                }
                            }
                            catch (Exception syncException)
                            {
                                // write exception to DNN log and continue to prevent taking the portal down
                                Debug(syncException.Message);
                                Debug(syncException.StackTrace);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug(ex.Message);
                    Debug(ex.StackTrace);
                }
                #endregion
			}
            else // existing user
            {
                comId = CspId;
                company cspCompany = CspContext.companies.FirstOrDefault(a => a.companies_Id == CspId);
                if (cspCompany == null)
                {
                    bSuccess = false;
                    _errorMessage = GetString("ModuleError.InvalidIntegrationKey");
                }
                else
                {
                    // save company info
                    cspCompany.displayname = tbCompanyName.Text;
                    cspCompany.address1 = tbAddress1.Text;
                    if (string.IsNullOrEmpty(cspCompany.address2))
                        cspCompany.address2 = "";
                    cspCompany.companyname = tbCompanyName.Text.Replace("\"", "'");
                    cspCompany.city = tbCity.Text;
                    cspCompany.state = tbState.Text.Length > 20 ? tbState.Text.Substring(0, 20) : tbState.Text;
                    cspCompany.zipcode = tbZip.Text;
                    cspCompany.country = ddlCountry.SelectedValue;
                    //cspCompany.created = DateTime.Now;
                    cspCompany.date_changed = DateTime.Now;
                    cspCompany.website_url = tbUrl.Text;
                    SubmitCspChanges();

                    // save contact person information
                    bSuccess = bSuccess &&
                               SaveConsumerParameter(cspCompany, "Personalize_Contact_Firstname", tbFirstName.Text);
                    bSuccess = bSuccess &&
                               SaveConsumerParameter(cspCompany, "Personalize_Contact_Lastname", tbLastName.Text);
                    bSuccess = bSuccess &&
                               SaveConsumerParameter(cspCompany, "Personalize_Contact_Jobtitle", tbJobTitle.Text);
                    bSuccess = bSuccess &&
                               SaveConsumerParameter(cspCompany, "Personalize_Contact_Emailaddress", tbEmail.Text);

                    #region ltu 10/13/2013 Write value to ContactUsEmail parameter for VR

                    // legacy systems require ContactUsEmail field
                    SaveConsumerParameter(cspCompany, "ContactUsEmail", tbEmail.Text);

                    #endregion

                    bSuccess = bSuccess &&
                               SaveConsumerParameter(cspCompany, "Personalize_Contact_Phonenumber", tbPhoneNumber.Text);

                    // save agree to receive mailings
                    bSuccess = bSuccess &&
                               SaveConsumerParameter(cspCompany, "Agree_Receiving_Mailings",
                                                     cbAgreeToMailings.Checked.ToString());

                    // also save first name, last name and telephone number if these are empty
                    if (string.IsNullOrEmpty(UserInfo.Profile.FirstName))
                    {
                        UserInfo.Profile.FirstName = tbFirstName.Text;
                    }
                    if (string.IsNullOrEmpty(UserInfo.Profile.LastName))
                    {
                        UserInfo.Profile.LastName = tbLastName.Text;
                    }
                    if (string.IsNullOrEmpty(UserInfo.Profile.Telephone))
                    {
                        UserInfo.Profile.Telephone = tbPhoneNumber.Text;
                    }
                    UserController.UpdateUser(PortalId, UserInfo);
                }
            }


            //re-render language dropdown list
            var com = CspContext.companies.FirstOrDefault(a => a.companies_Id == comId);

            // ltu 7/8/14 change to use company consumers from com
            var consumer = com.companies_consumers.FirstOrDefault();
            
            var country = DnnContext.CSPSubscriptionModule_Countries.FirstOrDefault(a => a.Name == com.country);
            if (country != null)
            {
                var languageFields = DnnContext.CSPSubscriptionModule_Fields.Where(a => a.Type == "csp-language-select").ToList();
                foreach (var languageField in languageFields)
                {
                    var mId = languageField.FormId + "_" + languageField.FieldId;
                    var mControl = (DropDownList)FindControlRecursive(Control, mId);
                    if (mControl != null)
                    {
                        var clusterCountry = DnnContext.CSPSubscriptionModule_Cluster_Countries
                                                        .Where(a => a.CountryId == country.Id).ToList();
                        var languagesIds = GetListLanguageByListClusterCountry(clusterCountry);
                        var listLanguages = new List<language>();
                        if (languagesIds.Any())
                        {
                            mControl.Items.Clear();
                            listLanguages = languagesIds.SelectMany(languagesId => CspContext.languages.Where(a => a.active.HasValue && a.active.Value).Where(lng => languagesId == lng.languages_Id)).OrderBy(a => a.description).ToList();
                            foreach (language lang in listLanguages)
                            {
                                if (lang != null)
                                {
                                    mControl.Items.Add(new ListItem
                                    {
                                        Text = lang.description,
                                        Value = lang.languages_Id.ToString(),
                                    });
                                }
                            }
                        }
                        else
                        {
                            //render language with custom domain
                            var listCustomDomain = DnnContext.CSPSubscriptionModule_CustomDomainParams.Where(a => a.PortalId == PortalId);
                            var listLangs = new List<language>();
                            var languagesToRemove = new List<int>();

                            foreach (var record in listCustomDomain)
                            {
                                if (string.IsNullOrEmpty(record.CountryIds))
                                {
                                    // this custom domain is avaialble to all languages
                                    continue;
                                }

                                var customCountryList = record.CountryIds.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                                    
                                // ltu 10/30/2013 
                                // if the current country is NOT in the custom country list, then we don't want to show the language
                                // otherwise show only the languages that are specified in the custom param table

                                if (customCountryList.Contains(country.Id.ToString()))
                                {
                                    //vu.dinh 8/7/14 CSP-769
                                    //If company's language is disabled, keep showing this language
                                    var lang = CspContext.languages.FirstOrDefault(a => a.languages_Id == record.LanguageId && a.active.HasValue && a.active.Value);
                                    if (record.LanguageId == consumer.default_language_Id)
                                    {
                                        lang = CspContext.languages.FirstOrDefault(a => a.languages_Id == record.LanguageId);
                                    }
                                    
                                    if (lang != null && !listLangs.Contains(lang))
                                    {
                                        listLangs.Add(lang);
                                    }
                                }
                                else
                                {
                                    if (!record.VisibleToAllCountries)
                                    languagesToRemove.Add(record.LanguageId);
                                }
                            }
                            if (listLangs.Any())
                            {
                                mControl.Items.Clear();
                                foreach (var language in listLangs)
                                {
                                    mControl.Items.Add(new ListItem
                                    {
                                        Text = language.description,
                                        Value = language.languages_Id.ToString(),
                                    });
                                }
                            }
                            else
                            {
                                mControl.Items.Clear();
                                //vu.dinh 8/7/14 CSP-769
                                //If company's language is disabled, keep showing this language
                                foreach (language lng in CspContext.languages.OrderBy(a => a.description))
                                {
                                    if (consumer.default_language_Id != lng.languages_Id && (!lng.active.HasValue || !lng.active.Value))
                                        continue;
                                    // do not want to show the languages in the languagesToRemove list
                                    if (languagesToRemove.Contains(lng.languages_Id))
                                        continue;

                                    mControl.Items.Add(new ListItem
                                    {
                                        Text = lng.description,
                                        Value = lng.languages_Id.ToString(),
                                        //Selected = (lng.languages_Id.ToString() == userValue),
                                    });
                                }
                            }
                        }

                        var companyLanguage = com.companies_consumers.First().default_language_Id;
                        if (companyLanguage.HasValue)
                        {
                            var item = mControl.Items.FindByValue(companyLanguage.Value.ToString());
                            if (item != null)
                                item.Selected = true;
                        }
                    }
                }
            }

            #region ltu 8/11/2014 add a table to create a process and sync the fields upon changing the company

            try
            {
                DnnContext.ExecuteCommand(@"
                IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CSPSubscriptionModule_SyncVR]') AND type in (N'U'))
                BEGIN
                    CREATE TABLE [dbo].[CSPSubscriptionModule_SyncVR](
	                    [id] [uniqueidentifier] NOT NULL,
	                    [command] [varchar](100) NOT NULL,
	                    [cspId] [int] NOT NULL,
	                    [vrUserName] [varchar](100) NOT NULL,
                        [projectName][varchar](100) NOT NULL,
                        [mode][varchar](100) NOT NULL,
	                    [status] [int] NOT NULL,
	                    [logMessage] [nvarchar](max) NULL,
                     CONSTRAINT [PK_CSPSubscriptionModule_SyncVR] PRIMARY KEY CLUSTERED 
                    (
	                    [id] ASC
                    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
                    ) ON [PRIMARY]

                    ALTER TABLE [dbo].[CSPSubscriptionModule_SyncVR] ADD  CONSTRAINT [DF_CSPSubscriptionModule_SyncVR_id]  DEFAULT (newid()) FOR [id]
                END
                ");

                // check to see if they are actual VR Users
                string vrUser = (from cp in CspContext.companies_parameters
                    where cp.companies_parameter_type.parametername == "Mailing_VR_User" && cp.companies_Id == CspId
                    select cp.value_text).FirstOrDefault();
                if (!string.IsNullOrEmpty(vrUser))
                {
                    if (Settings.ContainsKey(ModuleSetting.ProjectName) && !string.IsNullOrEmpty(Settings[ModuleSetting.ProjectName].ToString()))
                    {
                        string mode = "prod";
                        if (Settings.ContainsKey(ModuleSetting.ProjectMode) && !string.IsNullOrEmpty(Settings[ModuleSetting.ProjectMode].ToString()))
                            mode = Settings[ModuleSetting.ProjectMode].ToString();

                        DnnContext.ExecuteCommand(@"
if not exists (select 1 from CSPSubscriptionModule_SyncVR where cspId= {1} and vrUserName = {2} and status=-1)
insert into CSPSubscriptionModule_SyncVR(command,cspId,vrUserName, projectName, mode,status) values({0}, {1}, {2}, {3}, {4}, -1)"
                            , "sync", CspId, vrUser, Settings[ModuleSetting.ProjectName].ToString(), mode);
                    }
                }
                else // ltu 09/18/14 if the vr account hasnt been created, we should keep updating the VR fields
                {
                    DnnLog.Info("Update Mailing_Website = " + tbUrl.Text);
                    SaveConsumerParameter(com, "ContactUsEmail", tbEmail.Text);
                    SaveConsumerParameter(com, "Mailing_Companyname", tbCompanyName.Text);
                    SaveConsumerParameter(com, "Mailing_Website", tbUrl.Text);
                    SaveConsumerParameter(com, "Mailing_Phone", tbPhoneNumber.Text);
                }

            }
            catch (Exception ex)
            {
                DnnLog.Warn(ex.Message, ex);
            }

            #endregion

            return bSuccess;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentStepIndex">current wizard step</param>
        /// <returns>true if save successfully</returns>
        private bool SaveData(int currentStepIndex)
        {
        	missingStep.Value = "0";
        	isFilled.Value = "1";

            // reset error message
            _errorMessage = string.Empty;
            // ignore types
            var ignoreTypes = new[] {"csp-twitter", "csp-linkedin","csp-facebook","text"};

            bool validated = true;
            var wizardStep = (WizardStep)formWizard.WizardSteps[currentStepIndex];
            var workflowId = new Guid(wizardStep.ID);
            Control control = null;
            string value = ""; // value of a control
            string id = ""; // dynamic control id
            string imageTypes = "image/bmp,image/jpeg,image/jpg,image/png,image/gif,image/pjpeg,image/x-png";
            // supported image type



            // get csp company record
            company cspCompany =
                CspContext.companies.FirstOrDefault(a => a.companies_Id == CspId);
            if (cspCompany == null) // redundant check 
            {
                Debug("Unable to save data: csp company record shouldn't be null: " + CspId);
                return false;
            }

            // get workflow record
            CSPSubscriptionModule_Workflow wf =
                DnnContext.CSPSubscriptionModule_Workflows.FirstOrDefault(a => a.WorkflowId == workflowId);
            if (wf == null)
            {
                Debug("Unable to find workflow record: " + workflowId);
                return false;
            }

            // server validation
            foreach (CSPSubscriptionModule_Field field in wf.CurrentForm.CSPSubscriptionModule_Fields)
            {
                id = field.FormId + "_" + field.FieldId;
                control = wizardStep.FindControl(id);
                if (control == null)
                    continue;

                switch (field.Type)
                {
                    case "select":
                        value = ((DropDownList)control).SelectedValue;
                        break;
                    case "csp-language-select":
                        value = ((DropDownList)control).SelectedValue;
                        break;
                    case "csp-theme-select":
                        value = ((DropDownList) control).SelectedValue;
                        break;
                    case "upload":
                        value = ((FileUpload)control).FileName;
                        if (string.IsNullOrEmpty(value))
                            value = GetCompanyParameter(cspCompany, field.Name);
                        break;
                    case "checkbox":
                        value = ((CheckBox) control).Checked.ToString();
                        break;
                    case "csp-multi-select":
                        value = (((RadListBox) control).GetCheckedIndices().Count() > 0)? ((RadListBox) control).CheckedItems[0].Value: string.Empty;
                        break;
                    case "csp-twitter":
                        break;
                    case "csp-linkedin":
                        break;
                    case "csp-facebook":
                        break;
                    case "text":
                        break;
                    default:
                        value = ((TextBox)control).Text;
                        break;
                }
 
               
                if (field.Type == "upload")
                {
                    string fileName = ((FileUpload)control).FileName;

                    // user chooses not to upload a new file, filename should be empty.
                    if (string.IsNullOrEmpty(fileName))
                        continue;

                    // check for file extension
                    if (!imageTypes.Contains(((FileUpload)control).PostedFile.ContentType))
                    {
                        var validateControl = (CustomValidator)wizardStep.FindControl(id + "_validate");
                        if (validateControl != null)
                        {
                            validateControl.Text = "  " + GetString("ModuleError.UploadFileNotSupported") + " [" +
                                                   fileName + "]";
                            validateControl.IsValid = false;
                            validateControl.Visible = true;
                            validated = false;
                        }
                        
                        
                    }
                }
                else if (field.Type == "csp-twitter")
                {
                    if ((DnnContext.CSPTSMM_Partners.SingleOrDefault(a => a.CspId == cspCompany.companies_Id && a.PortalId == PortalId && a.SocialId == (int)Common.Socialtype.Twitter) == null) &&
                        field.Mandatory)
                    {
                        var validateControl = (CustomValidator)wizardStep.FindControl(id + "_validate");
                        if (validateControl != null)
                        {
                            validateControl.Text = GetString("ModuleError.NeedToFollowUsOnTwitter");
                            validateControl.IsValid = false;
                            validateControl.Visible = true;
                            validated = false;
                        }
                    }
                }
                else if (field.Type == "csp-linkedin")
                {
                    if ((DnnContext.CSPTSMM_Partners.SingleOrDefault(a => a.CspId == cspCompany.companies_Id && a.PortalId == PortalId && a.SocialId == (int)Common.Socialtype.LinkedIn) == null) && field.Mandatory)

                    {
                        var validateControl = (CustomValidator)wizardStep.FindControl(id + "_validate");
                        if (validateControl != null)
                        {
                            validateControl.Text = GetString("ModuleError.NeedToFollowUsOnLinkedIn");
                            validateControl.IsValid = false;
                            validateControl.Visible = true;
                            validated = false;
                        }
                    }
                }
                else if (field.Type == "csp-facebook")
                {
                    if ((DnnContext.CSPTSMM_Partners.SingleOrDefault(a => a.CspId == cspCompany.companies_Id && a.PortalId == PortalId && a.SocialId == (int)Common.Socialtype.Facebook) == null) && field.Mandatory)
                    {
                        var validateControl = (CustomValidator)wizardStep.FindControl(id + "_validate");
                        if (validateControl != null)
                        {
                            validateControl.Text = GetString("ModuleError.NeedToFollowUsOnFacebook");
                            validateControl.IsValid = false;
                            validateControl.Visible = true;
                            validated = false;
                        }
                    }
                }
                else
                {
                    if (field.Mandatory)
                        validated = validated && !string.IsNullOrEmpty(value);
                }

                if (!validated)
                    break; // break out of the foreach
            }

            //Check field with regex validation
            foreach (
                CSPSubscriptionModule_Field field in
                    wf.CurrentForm.CSPSubscriptionModule_Fields.Where(a => a.RegexValidation != string.Empty))
            {
                if (ignoreTypes.Contains(field.Type))
                    continue;
                id = field.FormId + "_" + field.FieldId;
                control = wizardStep.FindControl(id);
                var listOfIgnoreFieldType = new List<string> { "select","title", "csp-language-select", "upload", "csp-theme-select", "csp-twitter", "csp-linkedin", "csp-facebook","csp-multi-select" };
                if (!listOfIgnoreFieldType.Contains(field.Type))
                {
                    string fieldValue = ((TextBox)control).Text;
                    if (string.IsNullOrEmpty(fieldValue)) // Ignore field with empty data
                        continue;
                    Match match = Regex.Match(fieldValue, string.Format(@"{0}", field.RegexValidation),
                                                RegexOptions.IgnoreCase);
                    if (!match.Success) // Not match the validation
                    {
                        validated = false;
                        break;
                    }
                }
            }
            
          
            if (!validated)
            {
                if (string.IsNullOrEmpty(_errorMessage))
                    _errorMessage = GetString("ModuleError.FailValidation");
                return false;
            }

           
            //save
            // ltu 4/15/14 there is a possibility that the theme field comes before the language, that means the base parameter will be overriden by the legacy code
            // to fix this, i will move the code to save themes out of the for loop to make sure it gets saved last
            bool bCustomParamForTheme = false;
            int selectedThemeId = -1;

            foreach (CSPSubscriptionModule_Field field in wf.CurrentForm.CSPSubscriptionModule_Fields)
            {
                if (ignoreTypes.Contains(field.Type))
                    continue;
                id = field.FormId + "_" + field.FieldId;
                control = wizardStep.FindControl(id);
                if (control == null)
                    continue;

                switch (field.Type)
                {
                    case "select":
                        value = ((DropDownList) control).SelectedValue;
                        break;
                    case "csp-multi-select":
                        {
                            value = string.Empty;
                            if (field.CSPSubscriptionModule_CodeList != null)
                            {
                                var codeListValue = field.CSPSubscriptionModule_CodeList.CSPSubscriptionModule_CodeListValues.ToList();
                                var indices = ((RadListBox)control).GetCheckedIndices();

                                foreach (var index in indices)
                                {
                                    value += codeListValue[index].Value + ",";
                                }
                                value = value.TrimEnd(',');
                            }
                        }
                        break;
                    case "csp-language-select":
                        value = ((DropDownList)control).SelectedValue;
                        break;
                    case "csp-theme-select":
                        value = ((DropDownList) control).SelectedValue;
                        break;
                    case "upload":
                        value = ((FileUpload)control).FileName;
                        break;
                    case "checkbox":
                        value = ((CheckBox) control).Checked.ToString();
                        break;
                    default:
                        value = ((TextBox)control).Text;
                        break;
                }
                // exception: this needs to save to companies consumer table
                if (field.Type == "csp-language-select")
                {
                    foreach (companies_consumer companiesConsumer in cspCompany.companies_consumers)
                    {
                        companiesConsumer.default_language_Id = int.Parse(value);
                        var customDomainParams =
                            DnnContext.CSPSubscriptionModule_CustomDomainParams.FirstOrDefault(
                                a => a.LanguageId == int.Parse(value) && a.PortalId == PortalId);
                        if (customDomainParams != null && customDomainParams.Params != string.Empty)
                            //use custom domain params
                            companiesConsumer.base_publication_parameters = customDomainParams.Params;
                        else //use defaul params from channel
                        {
                            var channel =
                                CspContext.Channels.FirstOrDefault(
                                    a => a.ExternalIdentifier == GetPortalExternalIdentifier());
                            if (channel != null)
                                companiesConsumer.base_publication_parameters = channel.DefaultParameters;
                        }
                    }
                    SubmitCspChanges();
                }
                else if (field.Type == "upload")
                {
                    // user chooses not to upload
                    if (string.IsNullOrEmpty(((FileUpload)control).FileName))
                        continue;

                    // consideration: resize, custom url path, custom scheme (http/s)
                    string customScheme = Utils.GetSetting(PortalId, "HttpTypeForImageUpload");
                    string customHost = Utils.GetSetting(PortalId, "CustomHostForImageUpload");
                    string serverPath = PortalSettings.HomeDirectoryMapPath + "Images\\" + UserInfo.UserID;
                    string filename = Path.GetExtension(((FileUpload)control).FileName);

                    if (string.IsNullOrEmpty(customScheme))
                        customScheme = Request.Url.Scheme;
                    if (string.IsNullOrEmpty(customHost))
                        customHost = Request.Url.Host;

                    if (!Directory.Exists(serverPath))
                        Directory.CreateDirectory(serverPath);

                    filename = string.Format("{0}_{1}{2}", UserInfo.UserID, DateTime.Now.Ticks, filename);
                    ((FileUpload)control).SaveAs(string.Format("{0}\\{1}", serverPath, filename));
                    filename = ResizeImage(filename,serverPath, field);
                    value = customScheme + "://" + customHost + PortalSettings.HomeDirectory + "Images/" +
                            UserInfo.UserID + "/" + filename;

                    //save parameter value
                    validated = validated && SaveConsumerParameter(cspCompany, field.Name, value);

                    // ltu 8/11/2014 Sync Mailing_Logo with Personalize_PDF_Logo but only if Mailing_Logo doesn't exist
                    if (field.Name.Equals("Personalize_PDF_Logo", StringComparison.CurrentCultureIgnoreCase))
                    {
                        SaveConsumerParameter(cspCompany, "Mailing_Logo", value, true);
                    }
                }
                else if(field.Type == "csp-theme-select")
                {
                    bCustomParamForTheme = true;
                    selectedThemeId = int.Parse(value);
                    //SaveTheme(cspCompany, int.Parse(value));
                }
                else
                    validated = validated && SaveConsumerParameter(cspCompany, field.Name, value);
                if (!validated)
                {
                    Debug(string.Format("Unable to save [{0}]: [{1}", field.Name, value));
                    break;
                }
            }

            // ltu 4/15/14 ensure to excute save theme last
            if (bCustomParamForTheme)
            {
                SaveTheme(cspCompany, selectedThemeId);
            }
            return true;
        }


        /// <summary>
        /// Resizes the image.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="serverpath">The serverpath.</param>
        /// <param name="field">The field.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 2/25/2013 - 11:27 AM
        private string ResizeImage(string filename, string serverpath, CSPSubscriptionModule_Field field)
        {
            var uploadFieldSetting =
                DnnContext.CSPSubscriptionModule_UploadFieldSettings.SingleOrDefault(
                    a => a.FieldId == field.FieldId && a.FormId == field.FormId);
            if (uploadFieldSetting != null && uploadFieldSetting.IsResize)
            {
                filename = Utils.ResizeImage(serverpath, filename, uploadFieldSetting.ResizeWidth,
                                             uploadFieldSetting.ResizeHeight.HasValue
                                                 ? uploadFieldSetting.ResizeHeight.Value
                                                 : 0);
            }
            return filename;
        }

        /// <summary>
        /// Saves the theme.
        /// </summary>
        /// <param name="company">The company.</param>
        /// <param name="themeId">The theme id.</param>
        /// Author: Vu Dinh
        /// 1/9/2013 - 3:41 PM
        /// <remarks>
        /// ltu 4/15/2014: check the custom param dictionary and update the base parameter if applicable
        /// </remarks>
        private void SaveTheme(company company, int themeId)
        {
            var companyThemes = CspContext.companies_themes.Where(a => a.companies_Id == company.companies_Id).ToList();

            if (companyThemes.Count == 1 && companyThemes.First().themes_Id == themeId)
                return;

            CspContext.companies_themes.DeleteAllOnSubmit(companyThemes);

            CspContext.SubmitChanges(ConflictMode.FailOnFirstConflict);

            // update custom parameter for the theme
            if (_customParamsForThemes.Count > 0 && _customParamsForThemes.ContainsKey(themeId.ToString(CultureInfo.InvariantCulture)))
            {
                foreach (var cc in company.companies_consumers)
                {
                    cc.base_publication_parameters = _customParamsForThemes[themeId.ToString(CultureInfo.InvariantCulture)];
                }

                // save
                //CspContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
            }


            CspContext.companies_themes.InsertOnSubmit(new companies_theme
            {
                companies_Id = company.companies_Id,
                themes_Id = themeId
            });
            CspContext.SubmitChanges(ConflictMode.FailOnFirstConflict);

            //vu.dinh 21-4-2014: handler theme and role association
            HandlerThemeRoleAssociation(company, themeId);
        }

        /// <summary>
        /// Handlers the theme role association. Assign current user to a role when select a theme
        /// </summary>
        /// <param name="company">The company.</param>
        /// <param name="themeId">The theme id.</param>
        /// <author>Vu Dinh</author>
        /// <modified>04/21/2014 14:49:42</modified>
        private void HandlerThemeRoleAssociation(company company, int themeId)
        {
            var themeRoleAssociation = new Dictionary<int, List<string>>();
            if (Settings[ModuleSetting.ThemeRoleAssociation] != null && Settings[ModuleSetting.ThemeRoleAssociation].ToString() != string.Empty)
            {
                var themeroles = Settings[ModuleSetting.ThemeRoleAssociation].ToString().Split(new string[]{";"},StringSplitOptions.RemoveEmptyEntries);
                foreach (var themerole in themeroles)
                {
                    var arr = themerole.Split(new string[] {":"}, StringSplitOptions.RemoveEmptyEntries);
                    if (arr.Length >= 2)
                    {
                        var listRole = new List<string>();
                        for (int i = 1; i < arr.Length; i++)
                        {
                            listRole.Add(arr[i]);
                               
                        }

                        themeRoleAssociation.Add(int.Parse(arr[0]),listRole);
                    }
                }
            }

            if (themeRoleAssociation.Any())
            {
                var currentUser = UserController.GetCurrentUserInfo();
                //remove old role from another theme
                foreach (var record in themeRoleAssociation.Where(a => a.Key != themeId))
                {
                    foreach (string roleName in record.Value)
                    {
                        var rc = new DotNetNuke.Security.Roles.RoleController();
                        if (currentUser.IsInRole(roleName))
                        {
                            var role = rc.GetRoleByName(PortalId, roleName);
                            if (role != null)
                            {
                                RoleController.DeleteUserRole(currentUser.UserID, role, PortalSettings, false);
                            }
                        }
                    }
                }

                foreach (var record in themeRoleAssociation.Where(a => a.Key == themeId))
                {
                     foreach (string roleName in record.Value)
                     {
                         var rc = new RoleController();
                         var role = rc.GetRoleByName(PortalId, roleName);
                         if (role != null)
                         {
                             rc.AddUserRole(PortalId, currentUser.UserID, role.RoleID, DateTime.Now.AddDays(-1), Null.NullDate);
                         }
                        
                     }
                }

                //delete tabs cache 
                DataCache.ClearTabsCache(PortalId);
            }
        }


        /// <summary>
        /// Create Wizard Step
        /// </summary>
        /// <param name="wf"></param>
        /// <returns></returns>
        private Control CreateWizardControl(CSPSubscriptionModule_Workflow wf)
        {
            var tbl = new Table();
            tbl.CssClass = "csp-form-table";
            company cspCompany = CspId != -1 ? CspContext.companies.FirstOrDefault(a => a.companies_Id == CspId) : null;
            string id = "", cssClass;
            Control input;

            //Handler for workflow function
            List<CSPSubscriptionModule_WorkflowCondition> wfConditions =
                wf.CSPSubscriptionModule_WorkflowConditions.ToList();
            var avaibleFieldList = new List<Guid>();
            if (cspCompany != null)
            {
                if (wfConditions.Any())
                {
                    List<CSPSubscriptionModule_Field> parentFormFields =
                        wf.ParentForm.CSPSubscriptionModule_Fields.ToList();
                    foreach (CSPSubscriptionModule_WorkflowCondition wfCondition in wfConditions)
                    {
                        CSPSubscriptionModule_Field field =
                            DnnContext.CSPSubscriptionModule_Fields.SingleOrDefault(
                                a => a.FieldId == wfCondition.FieldId);
                        if (field != null)
                        {
                            string fieldValue = LoadCompanyParameter(cspCompany, field.Name);
                            string wfConfitionOperator = wfCondition.Operator;
                            if (wfConfitionOperator == "=") //Need varible configurable here
                            {
                                if (fieldValue == wfCondition.Value)
                                {
                                    //Get target field ids
                                    List<string> targetFieldIds = wfCondition.TargetFieldIds.Split(',').ToList();
                                    avaibleFieldList.AddRange(
                                        targetFieldIds.Select(targetFieldId => Guid.Parse(targetFieldId)));
                                }
                            }
                        }
                    }
                }
            }

            foreach (
                CSPSubscriptionModule_Field field in
                    wf.CurrentForm.CSPSubscriptionModule_Fields.OrderBy(a => a.DisplayOrder))
            {
                if (avaibleFieldList.Any() && !avaibleFieldList.Contains(field.FieldId))
                    continue;
                TranslationText translationText = GetTranslation(field);

                var tr = new TableRow();
                tbl.Rows.Add(tr);
                TableCell tc;
              
                // control id
                id = field.FormId + "_" + field.FieldId;
                // css class
                cssClass = "csp-form-field" + (field.Mandatory ? " csp-form-mandatory" : "") + " " + field.Type;

                // load user value
                string userValue = "";

                // exception case: is when type = csp-language-select, then load the userValue from the consumer profile instead.
                if (cspCompany != null)
                {
                    if (field.Type == "csp-language-select")
                    {
                        companies_consumer consumerProfile = cspCompany.companies_consumers.FirstOrDefault();
                        if (consumerProfile != null)
                            userValue = consumerProfile.default_language_Id.ToString();
                    }
                    else
                    {
                        userValue = GetCompanyParameter(cspCompany, field.Name);
                    }
                }
                
                if (field.Type != "text")
                {
                    // label
                    tc = new TableCell
                    {
                        CssClass = "csp-table-cell-label",
                        VerticalAlign = VerticalAlign.Top,
                        HorizontalAlign = HorizontalAlign.Right
                    };

                    tr.Cells.Add(tc);

                    // title
                    if (field.Type.Equals("title", StringComparison.OrdinalIgnoreCase))
                    {
                        // field label name
                        var lbl = new Label
                        {
                            Text = translationText.ShortDescription
                        };


                        tc.ColumnSpan = 2;
                        lbl.CssClass = "csp-form-title";
                        tc.Controls.Add(lbl);
                        continue;
                    }

                    // label
                    Control userControl = Page.LoadControl("~/controls/LabelControl.ascx");
                    ((LabelControl)userControl).Text = translationText.ShortDescription;
                    ((LabelControl)userControl).HelpText = string.IsNullOrEmpty(translationText.LongDescription)
                                                                ? translationText.ShortDescription
                                                                : translationText.LongDescription;
                    tc.Controls.Add(userControl);

                    // input
                    tc = new TableCell
                    {
                        CssClass = "csp-table-cell-input",
                        VerticalAlign = VerticalAlign.Top
                    };
                }
                else
                {
                    // input
                    tc = new TableCell
                    {
                        CssClass = "csp-table-cell-full",
                        VerticalAlign = VerticalAlign.Top,
                        HorizontalAlign = HorizontalAlign.Left,
                        ColumnSpan = 2
                    };
                }
                
                
               
                switch (field.Type.ToLower())
                {
                    case "select":
                        input = new DropDownList
                                    {
                                        CssClass = cssClass,
                                        ID = id
                                    };
                        if (field.CSPSubscriptionModule_CodeList != null)
                        {
                            foreach (
                                CSPSubscriptionModule_CodeListValue codeListValue in
                                    field.CSPSubscriptionModule_CodeList.CSPSubscriptionModule_CodeListValues)
                            {
                                ((DropDownList) input).Items.Add(new ListItem
                                                                     {
                                                                         Text =
                                                                             GetTranslation(
                                                                                 codeListValue.CodeListValueId,
                                                                                 codeListValue.Name).ShortDescription,
                                                                         Value = codeListValue.Value,
                                                                         Selected = (codeListValue.Value == userValue)
                                                                     });
                            }
                        }
                        break;
                    case "csp-multi-select":
                        input = new RadListBox
                            {
                                CssClass = cssClass,
                                ID = id,
                                CheckBoxes = true
                            };
                        if (field.CSPSubscriptionModule_CodeList != null)
                        {
                            var listvalue = userValue.Split(new string[] {","}, StringSplitOptions.RemoveEmptyEntries);
                            foreach (var codelistValue in field.CSPSubscriptionModule_CodeList.CSPSubscriptionModule_CodeListValues)
                            {
                                ((RadListBox)input).Items.Add(new RadListBoxItem
                                    {
                                        Text = GetTranslation(codelistValue.CodeListValueId,codelistValue.Name).ShortDescription,
                                        Value = codelistValue.Value,
                                        Checked = listvalue.Contains(codelistValue.Value)
                                    });
                            }
                        }
                        break;
                    case "upload":
                        input = new FileUpload
                                    {
                                        ID = id,
                                        CssClass = cssClass
                                    };
                        break;
                    case "longtext":
                        input = new TextBox
                                    {
                                        TextMode = TextBoxMode.MultiLine,
                                        Rows = 5,
                                        CssClass = cssClass,
                                        Text = userValue,
                                        ID = id
                                    };
                        break;
                    case "csp-language-select":
                        // load languages from csp
                        input = new DropDownList
                            {
                                CssClass = cssClass,
                                ID = id
                            };
                            RenderLanguages(input, userValue);
                        break;
                    case "csp-theme-select":
                        {
                            input = new DropDownList
                            {
                                CssClass = cssClass,
                                ID = id
                            };
                            //Get all available theme
                            string externalIdentifier = GetPortalExternalIdentifier();
                            Channel channel =
                                CspContext.Channels.FirstOrDefault(a => a.ExternalIdentifier == externalIdentifier);
                            var listThemeAvalable =
                                CspContext.ChannelThemeAssocations.Where(a => a.ChannelId == channel.Id);
                            foreach (var themeAssocation in listThemeAvalable)
                            {
                                var theme =
                                    CspContext.themes.SingleOrDefault(
                                        a => a.themes_Id == themeAssocation.ThemeId && a.active);
                                if (theme != null)
                                {
                                    ((DropDownList) input).Items.Add(new ListItem
                                        {
                                            Text = theme.description,
                                            Value = theme.themes_Id.ToString()
                                        });
                                }
                            }

                            if (CspId != -1)
                            {
                                // ltu 4/21/14 there are cases where there are multiple themes assigned to the same consumer
                                var companyTheme =
                                    CspContext.companies_themes.FirstOrDefault(a => a.companies_Id == CspId);
                                if (companyTheme != null)
                                {
                                    var themOption =
                                        ((DropDownList) input).Items.FindByValue(companyTheme.themes_Id.ToString());
                                    if (themOption != null)
                                        themOption.Selected = true;
                                }
                            	
                            }
                            break;
                        }
                    case "csp-twitter":
                        {
                            if (cspCompany == null)
                                continue;
                            else
                            {
                                input = RenderSocialAuthenticationButton(Common.Socialtype.Twitter, cspCompany, wf, id);
                            }
                            
                            break;
                        }
                    case "csp-linkedin":
                        {
                            if (cspCompany == null)
                                continue;
                            else
                            {
                                input = RenderSocialAuthenticationButton(Common.Socialtype.LinkedIn, cspCompany, wf, id);
                            }
                            break;
                        }
                    case "csp-facebook":
                        {
                            if (cspCompany == null)
                                continue;
                            else
                            {
                                input = RenderSocialAuthenticationButton(Common.Socialtype.Facebook, cspCompany, wf, id);
                            }
                            break;
                        }
                    case "checkbox":
                        {
                            input = new CheckBox
                                        {
                                            ID = id,
                                            Checked = !string.IsNullOrEmpty(userValue) && bool.Parse(userValue)
                                        };
                            break;
                        }
                    case "text":
                        {
                            input = new LiteralControl(!string.IsNullOrEmpty(translationText.LongDescription) ? translationText.LongDescription : string.Empty);
                            break;
                        }
                    default:
                        input = new TextBox
                                    {
                                        CssClass = cssClass,
                                        Text = userValue,
                                        ID = id
                                    };
                        break;
                }

                //add control
                tc.Controls.Add(input);
                // add validate holder for upload type
                if (field.Type == "upload") 
                {
                    tc.Controls.Add(new CustomValidator { ID = id + "_validate", ControlToValidate = id , CssClass = "validator"});
                }
                // show preview image if field is upload
                if (field.Type == "upload" && !string.IsNullOrEmpty(userValue))
                {
                    
                    tc.Controls.Add(new Literal {Text = "<br/>"});
                    tc.Controls.Add(new Image
                                        {
                                            ImageUrl = userValue,
                                            AlternateText = userValue,
                                            CssClass = "csp-form-preview-image"
                                        });
                }

                // validation
                if (field.Mandatory)
                {
                    if (field.Type == "csp-twitter" || field.Type == "csp-linkedin" || field.Type == "csp-facebook")
                    {
                        tc.Controls.Add(new CustomValidator { ID = id + "_validate", CssClass = "validator" });
                    }
                    else
                    {
                        string validationErrorMessage = GetString("Validation." + field.Name.Replace(" ", "_"));
                        var fieldValidator = new RequiredFieldValidator
                        {
                            Text =
                                !string.IsNullOrEmpty(validationErrorMessage)
                                    ? validationErrorMessage
                                    : "*",
                            ControlToValidate = id,
                            CssClass = "validator"
                        };
                        // assign initial value to avoid false positive validation
                        if (field.Type == "upload")
                        {
                            fieldValidator.InitialValue = userValue;
                        }
                        tc.Controls.Add(fieldValidator);
                    }
                    
                }
                tr.Cells.Add(tc);
            }
            return tbl;
        }

        /// <summary>
        /// Get Languages
        /// </summary>
        /// 

        private void RenderLanguages(System.Web.UI.Control input, String userValue)
        {
            ((DropDownList) input).Items.Clear();

            //var listCustomDomain = DnnContext.CSPSubscriptionModule_CustomDomainParams.Where(a => a.PortalId == PortalId).Select(a => a.CountryIds.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries));
            var customDomains = DnnContext.CSPSubscriptionModule_CustomDomainParams.Where(a => a.PortalId == PortalId);
            var languagesToShow = new List<language>();
            var languagesToRemove = new List<int>();

            if (CspId > 0)
            {
                var consumer = CspContext.companies.FirstOrDefault(c=>c.companies_Id==CspId);
                if (consumer != null)
                {
                    var country = DnnContext.CSPSubscriptionModule_Countries.FirstOrDefault(c=>c.Name == consumer.country);
                    if (country != null)
                    {
                        foreach (var customDomain in customDomains)
                        {
                            if (string.IsNullOrEmpty(customDomain.CountryIds))
                            {
                                // this custom domain is avaialble to all languages
                                continue;
                            }

                            if (customDomain.CountryIds.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Any(c=>c==country.Id.ToString()))
                            {
                                //vu.dinh 8/7/14 CSP-769
                                //If company's language is disabled, keep showing this language
                                var lng = CspContext.languages.FirstOrDefault(l => l.languages_Id == customDomain.LanguageId && l.active.HasValue && l.active.Value);
                                if (consumer.companies_consumers.First().default_language_Id == customDomain.LanguageId)
                                    lng = CspContext.languages.FirstOrDefault(l => l.languages_Id == customDomain.LanguageId);
                                if (lng!=null&&!languagesToShow.Contains(lng))
                                    languagesToShow.Add(lng);
                            }
                            else
                            {
                                if (!customDomain.VisibleToAllCountries)
                                    languagesToRemove.Add(customDomain.LanguageId);
                            }

                        }
                    }
                }
            }

            #region ltu 4/8/14 Handle an exception: a language is active, but shouldn't be shown in the portal
            // note: there is a flag in the language table called "ShowInBrowser", this flag is nullable. The value has to be true in order to hide this language
            try
            {
                var tmp = CspContext.ExecuteQuery<int>("select 	languages_Id from languages where showinportal is not null and showinportal = 0").ToList();
	
                if (languagesToShow.Any())
                {
                    // check for the flag
                    languagesToShow = languagesToShow.Where(a => tmp.All(b => b != a.languages_Id)).ToList();
                }
                languagesToRemove.AddRange(tmp);
                languagesToRemove = languagesToRemove.Distinct().ToList(); // remove duplicates
            }
            catch{
                // there is no column ShowInPortal
            }

            #endregion

            if (languagesToShow.Any())
            {
                foreach (var lng in languagesToShow)
                {
                    ((DropDownList)input).Items.Add(new ListItem
                    {
                        Text = lng.description,
                        Value = lng.languages_Id.ToString(CultureInfo.InvariantCulture),
                        Selected = (lng.languages_Id.ToString() == (!string.IsNullOrEmpty(userValue) ? userValue : ModuleKeys.DEFAULT_LANGUAGE_ID))
                    });
                }
            }
            else
            {
                company consumer = null;
                if (CspId > 0)
                {
                    consumer = CspContext.companies.FirstOrDefault(c => c.companies_Id == CspId);
                }
                
                foreach (language lng in CspContext.languages.Where(a => !languagesToRemove.Contains(a.languages_Id)).OrderBy(a => a.description))
                {
                    //vu.dinh 8/7/14 CSP-769
                    //If company's language is disabled, keep showing this language
                    if (consumer!= null && consumer.companies_consumers.First().default_language_Id != lng.languages_Id && (!lng.active.HasValue || !lng.active.Value) )
                        continue;
                    ((DropDownList) input).Items.Add(new ListItem
                    {
                        Text = lng.description,
                        Value = lng.languages_Id.ToString(),
                        Selected =
                            (lng.languages_Id.ToString() == (!string.IsNullOrEmpty(userValue) ? userValue : ModuleKeys.DEFAULT_LANGUAGE_ID))
                    });
                }
            }
        }

        /**
         * Get & distinct List LanguageIds to display in dropdownlist
         * Author: Ngo Quang Phat
         */
        public List<int> GetListLanguageByListClusterCountry(List<TIEKinetix.CspModules.CSPSubscriptionModule.Components.Data.CSPSubscriptionModule_Cluster_Country> cluster)
        {
            List<int> list = new List<int>();
            foreach (var cspSubscriptionModuleCluster in cluster)
            {
                List<TIEKinetix.CspModules.CSPSubscriptionModule.Components.Data.CSPSubscriptionModule_Cluster_Language> moduleClusterLanguages = DnnContext.CSPSubscriptionModule_Cluster_Languages.Where(a => a.ClusterId == cspSubscriptionModuleCluster.ClusterId).ToList();
                list.AddRange(moduleClusterLanguages.Select(cspSubscriptionModuleClusterLanguage => cspSubscriptionModuleClusterLanguage.LanguageId));
            }
            return list.Distinct().ToList();
        } 

        /// <summary>
        /// Return request parameter value from parameterName
        /// </summary>
        /// <param name="cspCompany">Csp company</param>
        /// <param name="parameterName">Requested parameter name</param>
        /// <returns>found value or string.Empty</returns>
        private string GetCompanyParameter(company cspCompany, string parameterName)
        {
            companies_parameter_type parameter =
                CspContext.companies_parameter_types.FirstOrDefault(a => a.parametername == parameterName);
            if (parameter == null) return string.Empty;

            companies_parameter cp =
                cspCompany.companies_parameters.FirstOrDefault(
                    a => a.companies_parameter_types_Id == parameter.companies_parameter_types_Id);
            return cp != null ? cp.value_text : string.Empty;
        }

        private TranslationText GetTranslation(CSPSubscriptionModule_Field field)
        {
            CSPSubscriptionModule_Translation translation =
                DnnContext.CSPSubscriptionModule_Translations.FirstOrDefault(
                    a => a.CultureCode == CurrentLanguage && a.Id == field.FieldId);
            return translation == null ? new TranslationText(GetString(field.Name)) : new TranslationText(translation);
        }

        private TranslationText GetTranslation(Guid id, string defaultValue)
        {
            CSPSubscriptionModule_Translation t =
                DnnContext.CSPSubscriptionModule_Translations.FirstOrDefault(
                    a => a.Id == id && a.CultureCode == CurrentLanguage);
            return t == null
                       ? new TranslationText(!string.IsNullOrEmpty(defaultValue)
                                                 ? defaultValue
                                                 : "Need Translation For [" + id + "]")
                       : new TranslationText(t);
        }

        #region Form Wizard Events

        protected void formWizard_ActiveStepChanged(object sender, EventArgs e)
        {
        }

        protected void formWizard_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {
            switch (e.CurrentStepIndex)
            {
                case 0:
                    e.Cancel = !CreateOrUpdateCompany();
                    break;
                default:
                    // other steps
                    if (SaveData(e.CurrentStepIndex))
                    {
                        CheckAutoPostUser();
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                    break;
            }
        }

        protected void formWizard_FinishButtonClick(object sender, WizardNavigationEventArgs e)
        {
            // if there are more steps than the mandatory company information step
            if (formWizard.WizardSteps.Count > 1)
            {
                if (SaveData(e.CurrentStepIndex))
                {
                    CheckAutoPostUser();
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                e.Cancel = !CreateOrUpdateCompany();
            }

            if (!e.Cancel)
            {
                // successfully saved, redirect if tab name is defined
                if (!string.IsNullOrEmpty(RedirectTabName))
                {
                    var dataProvider = DataProvider.Instance();
                    var result  = dataProvider.ExecuteSQL(string.Format("SELECT TabID FROM Tabs WHERE TabName = '{0}'",RedirectTabName));
                    int tabId = 0;
                    while (result.Read())
                    {
                        tabId = int.Parse(result["TabId"].ToString());
                        break;
                    }
                    if (tabId != 0)
                    {
                        Response.Redirect(Globals.NavigateURL(tabId), true);
                    }
                }
                Response.Redirect(Globals.NavigateURL(TabId, "", "mid", ModuleId.ToString()), true);
            }
            
        }

        private void CheckAutoPostUser()
        {
            //create user role
            var oDnnRoleController = new RoleController();
            var oRole = oDnnRoleController.GetRoleByName(PortalId, ModuleKeys.TWITTER_USER_ROLE);
            if (oRole == null)
            {
                oRole = new RoleInfo
                {
                    PortalID = PortalId,
                    RoleName = ModuleKeys.TWITTER_USER_ROLE,
                    IsPublic = false,
                    AutoAssignment = false,
                    RoleGroupID = Null.NullInteger,
                    Status = RoleStatus.Approved
                };
                oDnnRoleController.AddRole(oRole);
            }

            if (!string.IsNullOrEmpty(ApproveTweetParameterName))
            {
                var partner = DnnContext.CSPTSMM_Partners.FirstOrDefault(a => a.CspId == Utils.GetIntegrationKey(UserInfo) && a.IsSubscribed);
                bool isDeleteRole = false;
                //Get the role information
                if (partner != null && partner.IsSubscribed)
                {
                    oDnnRoleController.AddUserRole(PortalId, UserInfo.UserID, oRole.RoleID, DateTime.Now.AddDays(Null.NullInteger), Null.NullDate);
                }
                else
                {
                    isDeleteRole = true;
                    
                }

                if (isDeleteRole)
                {
                    if (PortalSecurity.IsInRoles(ModuleKeys.TWITTER_USER_ROLE))
                        oDnnRoleController.DeleteUserRole(PortalId, UserInfo.UserID, oRole.RoleID);
                }
            }
        }

        #endregion

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
        	if (!String.IsNullOrEmpty(Request.Params["step"]))
			{
				missingStep.Value = Request.Params["step"];
                if (!string.IsNullOrEmpty(Request.Params["hidepopup"]))
                {
                    hidePopup.Value = "1";
                }
            }
        }

        private Control FindControlRecursive(Control control, string id)
        {
            if (control == null) return null;
            //try to find the control at the current level
            Control ctrl = control.FindControl(id);

            if (ctrl == null)
            {
                //search the children
                foreach (Control child in control.Controls)
                {
                    ctrl = FindControlRecursive(child, id);

                    if (ctrl != null) break;
                }
            }
            return ctrl;
        }

        /// <summary>
        /// localize all of the text values
        /// </summary>
        private void LocalizePage()
        {
            // load content if available
            if (CspId != -1)
            {
                company cspCompany = CspContext.companies.FirstOrDefault(a => a.companies_Id == CspId);
                if (cspCompany != null) // double check to ensure the company is there
                {
                    tbCompanyName.Text = cspCompany.companyname;
                    tbAddress1.Text = cspCompany.address1;
                    tbCity.Text = cspCompany.city;
                    tbState.Text = cspCompany.state;
                    tbZip.Text = cspCompany.zipcode;
                    tbUrl.Text = cspCompany.website_url;
                    ListItem li = ddlCountry.Items.FindByValue(cspCompany.country);
                    if (li != null)
                    {
                        ddlCountry.ClearSelection();
                        li.Selected = true;
                }
                }

                // load contact person info
                tbFirstName.Text = LoadCompanyParameter(cspCompany, "Personalize_Contact_Firstname");
                tbLastName.Text = LoadCompanyParameter(cspCompany, "Personalize_Contact_Lastname");
                tbJobTitle.Text = LoadCompanyParameter(cspCompany, "Personalize_Contact_Jobtitle");
                tbEmail.Text = LoadCompanyParameter(cspCompany, "Personalize_Contact_Emailaddress");
                tbPhoneNumber.Text = LoadCompanyParameter(cspCompany, "Personalize_Contact_Phonenumber");

                // load agreement to receive mailings
                bool fAgreeToMailings = false;
                if (bool.TryParse(LoadCompanyParameter(cspCompany, "Agree_Receiving_Mailings"), out fAgreeToMailings) &&
                    fAgreeToMailings)
                    cbAgreeToMailings.Checked = true;
            }
            else
            {
                tbFirstName.Text = UserInfo.FirstName;
                tbLastName.Text = UserInfo.LastName;
                tbPhoneNumber.Text = UserInfo.Profile.Telephone;
                tbEmail.Text = UserInfo.Email;
                tbUrl.Text = GetString("Label.TextBoxUrlDefaultText");
            }

            // localize checkbox
            cbAgreeToMailings.Text = GetString("CompanyInfo.AgreeToReceiveMailings");
			if (Settings[ModuleSetting.HideMailingCheckbox]!=null)
			{
				bool flag = false;
				if (bool.TryParse(Settings[ModuleSetting.HideMailingCheckbox].ToString(), out flag) && flag)
					cbAgreeToMailings.Visible = false;
			}

            // localize company information form
            vCompanyName.ErrorMessage = GetString("CompanyInfo.ErrorMessage_CompanyName");
            vAddress1.ErrorMessage = GetString("CompanyInfo.ErrorMessage_Address1");
            vCity.ErrorMessage = GetString("CompanyInfo.ErrorMessage_City");
            vState.ErrorMessage = GetString("CompanyInfo.ErrorMessage_State");
            vZip.ErrorMessage = GetString("CompanyInfo.ErrorMessage_Zip");
            vCountry.ErrorMessage = GetString("CompanyInfo.ErrorMessage_Country");
            vUrl.ErrorMessage = GetString("CompanyInfo.ErrorMessage_Url");

            vUrl1.ErrorMessage = GetString("FormWizard.ValidationErrorText");
            vEmail1.ErrorMessage = GetString("FormWizard.ValidationErrorText");

            //localize contact information
            vFirstName.ErrorMessage = GetString("CompanyInfo.ErrorMessage_FirstName");
            vLastName.ErrorMessage = GetString("CompanyInfo.ErrorMessage_LastName");
            vJobTitle.ErrorMessage = GetString("CompanyInfo.ErrorMessage_JobTitle");
            vPhoneNumber.ErrorMessage = GetString("CompanyInfo.ErrorMessage_PhoneNumber");
            vEmail.ErrorMessage = GetString("CompanyInfo.ErrorMessage_Email");

            // steps
            LinkButton nextButton = GetWizardButton("StepNavigationTemplateContainerID", "StepNextButton");
            nextButton.Text = GetString("WizardForm.Next");

            nextButton = GetWizardButton("StartNavigationTemplateContainerID", "StartNextButton");
            nextButton.Text = GetString("WizardForm.Next");

            LinkButton previousButton = GetWizardButton("StepNavigationTemplateContainerID", "StepPreviousButton");
            previousButton.Text = GetString("WizardForm.Previous");

            // finish
            previousButton = GetWizardButton("FinishNavigationTemplateContainerID", "FinishPreviousButton");
            previousButton.Text = GetString("WizardForm.Previous");

            nextButton = GetWizardButton("FinishNavigationTemplateContainerID", "FinishButton");
            nextButton.Text = GetString("WizardForm.Finish");

            // mandatory field indicator
            var literal = formWizard.FindControl("lMandatoryIndicatorText") as Literal;
            if (literal != null)
                literal.Text = GetString("CompanyInfo.MandatoryIndicatorText");
        }

        /// <summary>
        /// Load company parameter
        /// </summary>
        /// <param name="cspCompany">company record to load from</param>
        /// <param name="parameterName">parameter name</param>
        /// <returns></returns>
        private string LoadCompanyParameter(company cspCompany, string parameterName)
        {
            string value = "";
            companies_parameter_type paramDef =
                CspContext.companies_parameter_types.FirstOrDefault(a => a.parametername == parameterName);
            if (paramDef != null)
            {
                companies_parameter cp =
                    cspCompany.companies_parameters.FirstOrDefault(
                        a => a.companies_parameter_types_Id == paramDef.companies_parameter_types_Id);
                if (cp != null)
                    value = cp.value_text;
            }
            return value;
        }

        /// <summary>
        /// Save company parameter
        /// </summary>
        /// <param name="cspCompany">company record</param>
        /// <param name="parameterName">parameter name</param>
        /// <param name="parameterValue">parameter value</param>
        /// <param name="createNewOnly"></param>
        /// <returns></returns>
        private bool SaveConsumerParameter(company cspCompany, string parameterName, string parameterValue, bool createNewOnly = false)
        {
            companies_parameter_type paramDefinition =
                CspContext.companies_parameter_types.FirstOrDefault(a => a.parametername == parameterName);
            if (paramDefinition == null)
            {
                paramDefinition = new companies_parameter_type
                                      {
                                          parametername = parameterName,
                                          parametertype = 1,
                                          sortorder = 0,
                                          IsExport = null
                                      };
                CspContext.companies_parameter_types.InsertOnSubmit(paramDefinition);
                CspContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
            }
            companies_parameter p =
                cspCompany.companies_parameters.FirstOrDefault(
                    a => a.companies_parameter_types_Id == paramDefinition.companies_parameter_types_Id);
            if (p != null)
            {
                if (createNewOnly) return true; // return because it already exists

                p.value_text = parameterValue;
            }
            else
            {
                CspContext.companies_parameters.InsertOnSubmit(new companies_parameter
                                                                    {
                                                                        company = cspCompany,
                                                                        companies_parameter_types_Id =
                                                                            paramDefinition.companies_parameter_types_Id,
                                                                        value_text = parameterValue
                                                                    });
            }

            // save
            SubmitCspChanges();
            return true;
        }

        private LinkButton GetWizardButton(string containerId, string buttonId)
        {
            Control navContainer = formWizard.FindControl(containerId);
            LinkButton button = null;
            if (navContainer != null)
            {
                button = navContainer.FindControl(buttonId) as LinkButton;
            }
            return button;
        }

        protected void SideBarList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var button = e.Item.FindControl("sideBarButton") as LinkButton;
            if (button != null && CspId >= 0)
            {
                button.Enabled = true;
            }
        }

        /// <summary>
        /// send email notification with company information
        /// </summary>
        /// <param name="cspCompany">csp company record</param>
        private void SendEmailNotification(company comp)
        {
            try
            {
                var sb = new StringBuilder();
                string portalUrl = string.Empty;
                portalUrl = Localization.GetString("PortalUrl", Localization.SharedResourceFile);
                if (string.IsNullOrEmpty(portalUrl)) portalUrl = PortalAlias.HTTPAlias;

                sb.AppendFormat("<p>Portal Website Address: {0}</p>", portalUrl);
                sb.AppendFormat("<p>Company: {0}</p>", comp.companyname);
                sb.AppendFormat("<p>First Name: {0}</p>", UserInfo.FirstName);
                sb.AppendFormat("<p>Last Name: {0}</p>", UserInfo.LastName);
                sb.AppendFormat("<p>Street: {0}</p>", comp.address1);
                sb.AppendFormat("<p>Street: {0}</p>", comp.address2);
                sb.AppendFormat("<p>City: {0}</p>", comp.city);
                sb.AppendFormat("<p>State: {0}</p>", comp.state);
                sb.AppendFormat("<p>Zipcode: {0}</p>", comp.zipcode);
                sb.AppendFormat("<p>Country: {0}</p>", comp.country);
                sb.AppendFormat("<p>Web URL: {0}</p>", comp.website_url);
                sb.AppendFormat("<p>Telephone: {0}</p>",
                                !string.IsNullOrEmpty(UserInfo.Profile.Telephone)
                                    ? UserInfo.Profile.Telephone
                                    : string.Empty);
                sb.AppendFormat("<p>Email: {0}</p>", UserInfo.Email);

                string body = sb.ToString();


                string emailFromAddress = string.Empty, bccEmailAddress = string.Empty;
                emailFromAddress = Config.GetSetting("Portal" + PortalId + "EmailFromAddressForRegistration");
                if (string.IsNullOrEmpty(emailFromAddress))
                    emailFromAddress = Config.GetSetting("AllPortalDefaultEmailFromAddressForRegistration");
                if (string.IsNullOrEmpty(emailFromAddress))
					emailFromAddress = "lam.tu@tiekinetix.com";

                bccEmailAddress = Utils.GetSetting(PortalId, "BccEmailAddresses");
                if (string.IsNullOrEmpty(bccEmailAddress))
                {
                    bccEmailAddress =
                        "CSP-Support@TIEKinetix.com";
                }

                string secretEmailSubject = string.Empty;
                secretEmailSubject = Localization.GetString("SecretEmailSubject", LocalResourceFile);
                if (string.IsNullOrEmpty(secretEmailSubject))
                    secretEmailSubject = "Content Syndication Platform New User Registration";


                Dictionary<string, string> dict = HostController.Instance.GetSettingsDictionary();

                Mail.SendMail(emailFromAddress, //mail from
                              emailFromAddress, // mail to
                              "", //cc
                              bccEmailAddress,
                              //bcc
                              MailPriority.Normal,
                              secretEmailSubject,
                              MailFormat.Html,
                              Encoding.UTF8,
                              body,
                              "",
                              dict.ContainsKey("SMTPServer") && !string.IsNullOrEmpty(dict["SMTPServer"])
                                  ? dict["SMTPServer"]
                                  : "127.0.0.1",
                              dict.ContainsKey("SMTPAuthentication") &&
                              !string.IsNullOrEmpty(dict["SMTPAuthentication"])
                                  ? dict["SMTPAuthentication"]
                                  : "",
                              dict.ContainsKey("SMTPUsername") && !string.IsNullOrEmpty(dict["SMTPUsername"])
                                  ? dict["SMTPUsername"]
                                  : "",
                              dict.ContainsKey("SMTPPassword") && !string.IsNullOrEmpty(dict["SMTPPassword"])
                                  ? dict["SMTPPassword"]
                                  : "");
            }
            catch
            {
            }
        }

        #region Implementation of IActionable

        public ModuleActionCollection ModuleActions
        {
            get
            {
                var Actions = new ModuleActionCollection();
                Actions.Add(
                    GetNextActionID(),
                    "Settings",
                    ModuleActionType.AddContent, "", ControlPath + "Images/edit.png", EditUrl("Configuration"), false,
                    SecurityAccessLevel.Edit, true, false);
                return Actions;
            }
        }

        #endregion
    }

    internal class NameValuePair
    {
        public int Key { get; set; }
        public string Value { get; set; }

        public NameValuePair(int key, string value)
        {
            Key = key;
            Value = value;
        }

        public NameValuePair() : this(-1, string.Empty)
        {
        }
    }

}