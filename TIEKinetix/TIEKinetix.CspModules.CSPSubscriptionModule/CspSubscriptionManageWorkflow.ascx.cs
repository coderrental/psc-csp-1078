﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Data;
using DotNetNuke.Entities.Modules;
using TIEKinetix.CspModules.CSPSubscriptionModule.Components;
using TIEKinetix.CspModules.CSPSubscriptionModule.Components.Data;
using TIEKinetix.CspModules.CSPSubscriptionModule.Components.Entities;
using Telerik.Web.UI;

namespace TIEKinetix.CspModules.CSPSubscriptionModule
{
	public partial class CspSubscriptionManageWorkflow : DnnModuleBase
	{
		private DnnCspSubscriptionModuleContext _dnnContext;
		private List<Workflow> _listWorkflow;
		private int _tabModuleId;

        protected override void Page_Init(object sender, EventArgs e)
		{
			base.Page_Init(sender, e);

			_dnnContext = new DnnCspSubscriptionModuleContext(Config.GetConnectionString());
			_listWorkflow = new List<Workflow>();
			rgWorkFlow.MasterTableView.CommandItemSettings.AddNewRecordText = GetString("Grid.AddNewWorkflow");
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			_tabModuleId = base.TabmoduleId;
		}

		/// <summary>
		/// Handles the OnNeedDataSource event of the rgWorkFlow control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Telerik.Web.UI.GridNeedDataSourceEventArgs"/> instance containing the event data.</param>
		protected void rgWorkFlow_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
		{
			//Get Datasource
			int tmp = TabmoduleId;
			_listWorkflow = ConvertWorkflowToEntities(_dnnContext.CSPSubscriptionModule_Workflows.Where(a => a.PortalId == PortalId && a.TabModuleId == _tabModuleId));
			foreach (var workflow in _listWorkflow)
			{
				workflow.FormName = _dnnContext.CSPSubscriptionModule_Forms.Single(a => a.FormId == workflow.FormId).Name;
				if (workflow.ParentFormId == Guid.Empty)
					workflow.ParentFormName = GetString("CompanyInfo.Header");
				else
					workflow.ParentFormName = _dnnContext.CSPSubscriptionModule_Forms.Single(a => a.FormId == workflow.ParentFormId).Name;
			}

			rgWorkFlow.DataSource = _listWorkflow;
		}

		#region [ Convert linq table data into entities ]
		/// <summary>
		/// Converts the workflow to entity.
		/// </summary>
		/// <param name="workflow">The workflow.</param>
		/// <returns></returns>
		private Workflow ConvertWorkflowToEntity(CSPSubscriptionModule_Workflow workflow)
		{
			var workFlowEntity = new Workflow();
			workFlowEntity.Id = workflow.WorkflowId;

			workFlowEntity.FormId = workflow.FormId.HasValue ?workflow.FormId.Value : Guid.Empty;
			workFlowEntity.ParentFormId = workflow.ParentFormId.HasValue ?  workflow.ParentFormId.Value : Guid.Empty;

			return workFlowEntity;
		}

		/// <summary>
		/// Converts the workflow to entities.
		/// </summary>
		/// <param name="workflows">The workflows.</param>
		/// <returns></returns>
		private List<Workflow> ConvertWorkflowToEntities (IEnumerable<CSPSubscriptionModule_Workflow> workflows )
		{
			return (from cspSubscriptionModuleWorkflow in workflows select ConvertWorkflowToEntity(cspSubscriptionModuleWorkflow)).ToList();
		}
		#endregion

		/// <summary>
		/// Handles the OnItemCreated event of the rgWorkFlow control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Telerik.Web.UI.GridItemEventArgs"/> instance containing the event data.</param>
		protected void rgWorkFlow_OnItemCreated(object sender, GridItemEventArgs e)
		{
			if (e.Item is GridNestedViewItem) //Fetch data for workflow condition grid
			{
				var item = (GridNestedViewItem) e.Item;
				var workflowGrid = (RadGrid) item.FindControl("rgWorkflowCondition");
				var id = (Guid) item.ParentItem.GetDataKeyValue("Id");
				var workflow = _dnnContext.CSPSubscriptionModule_Workflows.SingleOrDefault(a => a.WorkflowId == id);
				if (workflow != null && workflow.ParentFormId != null)
					workflowGrid.DataSource =_dnnContext.CSPSubscriptionModule_WorkflowConditions.Where(a => a.WorkflowId == id).ToList();
				else
				{
					workflowGrid.Visible = false;
				}
			}
			if (e.Item is GridEditableItem && e.Item.IsInEditMode)
			{
				var editableItem = (GridEditableItem) e.Item;
				if (editableItem.RowIndex > 0)
					return;
				GridEditManager editManager = editableItem.EditManager;

				//Display dropdow Form Name
				var parentFormEditor = (GridDropDownColumnEditor) editManager.GetColumnEditor("ParentFormName");
				var formEditor = (GridDropDownColumnEditor) editManager.GetColumnEditor("FormName");
				parentFormEditor.DataSource = _dnnContext.CSPSubscriptionModule_Forms.Where(a => a.Active).ToList();
				parentFormEditor.DataTextField = "Name";
				parentFormEditor.DataValueField = "FormId";

				formEditor.DataSource = _dnnContext.CSPSubscriptionModule_Forms.Where(a => a.Active).ToList();
				formEditor.DataTextField = "Name";
				formEditor.DataValueField = "FormId";
			}
		}

		/// <summary>
		/// Handles the OnItemCreated event of the rgWorkflowCondition control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Telerik.Web.UI.GridItemEventArgs"/> instance containing the event data.</param>
		protected void rgWorkflowCondition_OnItemCreated(object sender, GridItemEventArgs e)
		{
			if (e.Item is GridEditableItem && e.Item.IsInEditMode)
			{	
				var editableItem = (GridEditableItem)e.Item;
				GridEditManager editManager = editableItem.EditManager;

				var nestedViewItem = (GridNestedViewItem)editableItem.OwnerTableView.OwnerGrid.NamingContainer;
				var parentFormId = (Guid)nestedViewItem.ParentItem.GetDataKeyValue("ParentFormId");

				//Display dropdow Field Name
				var editor = (GridDropDownColumnEditor)editManager.GetColumnEditor("FieldId");
				editor.DataSource = _dnnContext.CSPSubscriptionModule_Fields.Where(a => a.FormId == parentFormId).ToList();
				editor.DataTextField = "Name";
				editor.DataValueField = "FieldId";

				//Display listbox Target Field Name
				var editFormItem = e.Item as GridEditFormItem;
				var dropdowlist = (RadListBox)editFormItem["TargetFieldIds"].FindControl("lbxTargetFieldIds");
				var formId = (Guid) nestedViewItem.ParentItem.GetDataKeyValue("FormId");
				var dropdowlistSource = _dnnContext.CSPSubscriptionModule_Fields.Where(a => a.FormId == formId);

				var targetFieldIds = string.Empty;
				var fields = new List<string>();
				try
				{
					targetFieldIds = editableItem.GetDataKeyValue("TargetFieldIds").ToString();
				}
				catch (Exception)
				{

				}
				if (!string.IsNullOrEmpty(targetFieldIds))
				{
					fields = targetFieldIds.Trim().Split(',').ToList();
				}
				
				foreach (var cspSubscriptionModuleField in dropdowlistSource)
				{
					dropdowlist.Items.Add(new RadListBoxItem
					                      	{
					                      		Text = cspSubscriptionModuleField.Name,
												Value = cspSubscriptionModuleField.FieldId.ToString(),
												Checked = (fields.Any() && fields.Contains(cspSubscriptionModuleField.FieldId.ToString()))
					                      	});
				}
			}
		}

		/// <summary>
		/// Handles the OnItemDataBound event of the rgWorkflowCondition control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Telerik.Web.UI.GridItemEventArgs"/> instance containing the event data.</param>
		protected void rgWorkflowCondition_OnItemDataBound(object sender, GridItemEventArgs e)
		{
			if (e.Item is GridDataItem)
			{
				GridDataItem item = (GridDataItem)e.Item;
				Guid? fieldId = (Guid?)item.GetDataKeyValue("FieldId");
				var control = item["FieldId"].Controls[0];

				if (fieldId.HasValue)
					(control as Literal).Text = _dnnContext.CSPSubscriptionModule_Fields.FirstOrDefault(a => a.FieldId == fieldId.Value).Name;
				else
					(control as Literal).Text = "No Field";

				//Display Target fieldname
				var tbxTargetFieldIds = (Label) item["TargetFieldIds"].FindControl("tbxTargetFieldIds");
				var targetFieldIds = item.GetDataKeyValue("TargetFieldIds").ToString();
				var fields = targetFieldIds.Trim().Split(',');
				var fieldNames = fields.Aggregate(string.Empty, (current, field) => current + ("[" + _dnnContext.CSPSubscriptionModule_Fields.Single(a => a.FieldId == Guid.Parse(field)).Name + "],"));
				tbxTargetFieldIds.Text = fieldNames.Substring(0, fieldNames.Length - 1); //Remove last comma
			}
		}

		/// <summary>
		/// Handles the OnItemDataBound event of the rgWorkFlow control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Telerik.Web.UI.GridItemEventArgs"/> instance containing the event data.</param>
		protected void rgWorkFlow_OnItemDataBound(object sender, GridItemEventArgs e)
		{
			if (e.Item is GridDataItem && !e.Item.IsInEditMode)
			{
				GridDataItem item = (GridDataItem) e.Item;
				var parentformId = (Guid?) item.GetDataKeyValue("ParentFormId");
				var control = item["ParentFormName"].Controls[0];

				if (parentformId != Guid.Empty)
					(control as Literal).Text = _dnnContext.CSPSubscriptionModule_Forms.FirstOrDefault(a => a.FormId == parentformId).Name;
				else
					(control as Literal).Text = GetString("CompanyInfo.Header");

				var formId = (Guid?) item.GetDataKeyValue("FormId");
				control = item["FormName"].Controls[0];
				if (formId != Guid.Empty)
					(control as Literal).Text = _dnnContext.CSPSubscriptionModule_Forms.FirstOrDefault(a => a.FormId == formId).Name;
				else
					(control as Literal).Text = GetString("CompanyInfo.Header");
				
			}
			if (e.Item is GridEditableItem && (e.Item as GridEditableItem).IsInEditMode)
			{
				var editableItem = (GridEditableItem)e.Item;
				if (editableItem.RowIndex < 0)
					return;
				var parentFormId = (Guid)editableItem.GetDataKeyValue("ParentFormId");
				var formId = (Guid)editableItem.GetDataKeyValue("FormId");
				GridEditManager editManager = editableItem.EditManager;

				//Display dropdow Form Name
				var parentFormEditor = (GridDropDownColumnEditor)editManager.GetColumnEditor("ParentFormName");
				var formEditor = (GridDropDownColumnEditor)editManager.GetColumnEditor("FormName");
				CSPSubscriptionModule_Form firstForm = new CSPSubscriptionModule_Form();
				firstForm.FormId = Guid.Empty;
				firstForm.Name = GetString("CompanyInfo.Header");
				firstForm.Active = true;
				List<CSPSubscriptionModule_Form> formList = new List<CSPSubscriptionModule_Form>(); //_dnnContext.CSPSubscriptionModule_Forms.Where(a => a.Active).ToList();
				formList.Add(firstForm);
				formList.AddRange(_dnnContext.CSPSubscriptionModule_Forms.Where(a => a.Active).ToList());
				parentFormEditor.DataSource = formList;
				parentFormEditor.DataTextField = "Name";
				parentFormEditor.DataValueField = "FormId";
				parentFormEditor.DataBind();
				parentFormEditor.SelectedValue = parentFormId.ToString();

				formEditor.DataSource = _dnnContext.CSPSubscriptionModule_Forms.Where(a => a.Active).ToList();
				formEditor.DataTextField = "Name";
				formEditor.DataValueField = "FormId";
				formEditor.DataBind();
				formEditor.SelectedValue = formId.ToString();
			}
		}

		/// <summary>
		/// Handles the OnItemCommand event of the rgWorkflowCondition control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Telerik.Web.UI.GridCommandEventArgs"/> instance containing the event data.</param>
		protected void rgWorkflowCondition_OnItemCommand(object sender, GridCommandEventArgs e)
		{
			List<object> value;
			if (e.CommandName == RadGrid.UpdateCommandName) //Edit workflow condition
			{
				var id = (Guid) (e.Item as GridEditableItem).GetDataKeyValue("WorkflowConditionId");
				value = GetAutoGeneratedColumns(e.Item);
				var workflowCondition = _dnnContext.CSPSubscriptionModule_WorkflowConditions.Single(a => a.WorkflowConditionId == id);
				workflowCondition.FieldId = Guid.Parse(value[0].ToString());
				workflowCondition.Operator = value[1].ToString();
				workflowCondition.Value = value[2].ToString();
				workflowCondition.TargetFieldIds = value[3].ToString();

				_dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
			}
			else if (e.CommandName == RadGrid.PerformInsertCommandName) //Insert new workflow condition
			{
				var workflowid = new Guid(((HiddenField)e.Item.OwnerTableView.OwnerGrid.Parent.FindControl("workflowid")).Value);
				value = GetAutoGeneratedColumns(e.Item);
				_dnnContext.CSPSubscriptionModule_WorkflowConditions.InsertOnSubmit(new CSPSubscriptionModule_WorkflowCondition
				                                                                    	{
				                                                                    		WorkflowConditionId = Guid.NewGuid(),
																							WorkflowId = workflowid,
																							FieldId = Guid.Parse(value[0].ToString()),
																							Operator = value[1].ToString(),
																							Value = value[2].ToString(),
																							TargetFieldIds = value[3].ToString()
				                                                                    	});
				_dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
				rgWorkFlow.Rebind();
			}
			else if (e.CommandName == RadGrid.DeleteCommandName) //Delete selected workflow condition
			{
				var id = (Guid)(e.Item as GridEditableItem).GetDataKeyValue("WorkflowConditionId");
				//Delete workflow condition
				_dnnContext.CSPSubscriptionModule_WorkflowConditions.DeleteAllOnSubmit(_dnnContext.CSPSubscriptionModule_WorkflowConditions.Where(a => a.WorkflowConditionId == id));
				_dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
				rgWorkFlow.Rebind();				 
			}
		}
		/// <summary>
		/// Handles the OnItemCommand event of the rgWorkFlow control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Telerik.Web.UI.GridCommandEventArgs"/> instance containing the event data.</param>
		protected void rgWorkFlow_OnItemCommand(object sender, GridCommandEventArgs e)
		{
			List<object> value;
			if (e.CommandName == RadGrid.UpdateCommandName)
			{
				var id = (Guid) (e.Item as GridEditableItem).GetDataKeyValue("Id");
				value = GetAutoGeneratedColumns(e.Item);
				var workflow = _dnnContext.CSPSubscriptionModule_Workflows.SingleOrDefault(a => a.WorkflowId == id);
				if (value[0].ToString() == "0" || value[0].ToString() == Guid.Empty.ToString())
					workflow.ParentFormId = null;
				else
					workflow.ParentFormId = Guid.Parse(value[0].ToString());
				workflow.FormId = Guid.Parse(value[1].ToString());
				_dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
			}
			else if (e.CommandName == RadGrid.PerformInsertCommandName)
			{
				value = GetAutoGeneratedColumns(e.Item);
				var workflow = new CSPSubscriptionModule_Workflow
				               	{
									WorkflowId = Guid.NewGuid(),
									PortalId = PortalId,
									TabModuleId = _tabModuleId,
				               	};
				if (value[0].ToString() == "0")
				{
					workflow.ParentFormId = null;
				}
				else
				{
					workflow.ParentFormId = Guid.Parse(value[0].ToString());
				}
				if (value[1].ToString() == "0")
				{
					workflow.FormId = null;
				}
				else
				{
					workflow.FormId = Guid.Parse(value[1].ToString());
				}
				_dnnContext.CSPSubscriptionModule_Workflows.InsertOnSubmit(workflow);
				_dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
			}
			else if (e.CommandName == RadGrid.DeleteCommandName)
			{
				var id = (Guid)(e.Item as GridEditableItem).GetDataKeyValue("Id");
				//Delete workflow condition
				_dnnContext.CSPSubscriptionModule_WorkflowConditions.DeleteAllOnSubmit(_dnnContext.CSPSubscriptionModule_WorkflowConditions.Where(a => a.WorkflowId == id));
				_dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
				//Delete workflow
				_dnnContext.CSPSubscriptionModule_Workflows.DeleteAllOnSubmit(_dnnContext.CSPSubscriptionModule_Workflows.Where(a => a.WorkflowId == id));
				_dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
			}
		}

		/// <summary>
		/// get all input for auto-generated fields from Grid.
		/// </summary>
		/// <param name="item">Grid Item</param>
		/// <returns></returns>
		private List<object> GetAutoGeneratedColumns(GridItem item)
		{
			var gridEditableItem = (GridEditableItem)item;
			var gridEditManager = gridEditableItem.EditManager;
			var values = new List<object>();
			foreach (GridColumn column in item.OwnerTableView.RenderColumns)
			{
				// only insert what can edit
				if (column is IGridEditableColumn)
				{
					var editableColumn = (IGridEditableColumn)column;
					if (editableColumn.IsEditable)
					{
						IGridColumnEditor columnEditor = gridEditManager.GetColumnEditor(editableColumn);
						object value = null;

						if (columnEditor is GridTextColumnEditor)
						{
							value = (columnEditor as GridTextColumnEditor).Text;
						}
						else if (columnEditor is GridBoolColumnEditor)
						{
							value = (columnEditor as GridBoolColumnEditor).Value;
						}
						else if (columnEditor is GridDropDownColumnEditor)
						{
							value = (columnEditor as GridDropDownColumnEditor).SelectedValue;
						}
						else if (columnEditor is GridTemplateColumnEditor)
						{
							var str = ((RadListBox) columnEditor.ContainerControl.FindControl("lbxTargetFieldIds")).CheckedItems.Aggregate(string.Empty, (current, checkItem) => current + (checkItem.Value + ","));
							value = str.Substring(0, str.Length - 1);
						}
						if (value != null)
						{
							values.Add(value);
						}
					}
				}
			}
			return values;
		}
	}
}