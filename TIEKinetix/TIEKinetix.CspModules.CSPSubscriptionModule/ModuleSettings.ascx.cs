﻿// ***********************************************************************
// Assembly         : TIEKinetix.CspModules.CSPSubscriptionModule
// Author           : Vu Dinh
// Created          : 01-15-2014
//
// Last Modified By : Vu Dinh
// Last Modified On : 02-12-2014
// ***********************************************************************
// <copyright file="ModuleSettings.ascx.cs" company="Coder Rental Team">
//     Copyright (c) Coder Rental Team. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using DotNetNuke.Entities.Modules;

namespace TIEKinetix.CspModules.CSPSubscriptionModule
{

    /// <summary>
    /// Module Settings
    /// </summary>
    /// <remarks>
    /// ltu 4/15/14: added feature to allow custom base parameters per theme selection
    /// for now the field contains name value pair with a semi colon delimiter (;)
    /// todo: turn the name value pair into a nicer UX
    /// </remarks>
    public partial class ModuleSettings : ModuleSettingsBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #region Overrides of ModuleSettingsBase

        /// <summary>
        /// Loads the settings.
        /// </summary>
        /// <author>Vu Dinh</author>
        /// <modified>02/12/2014 15:59:22</modified>
        public override void LoadSettings()
        {
            if (TabModuleSettings[ModuleSetting.TabName] != null)
                tbTabName.Text = TabModuleSettings[ModuleSetting.TabName].ToString();
            if (TabModuleSettings[ModuleSetting.SupplierId] != null)
                tbSupplierId.Text = TabModuleSettings[ModuleSetting.SupplierId].ToString();
            #region Social consumer key - consumer secret
            if (TabModuleSettings[ModuleSetting.TwitterConsumerKey] != null)
                tbTwitterConsumerKey.Text = TabModuleSettings[ModuleSetting.TwitterConsumerKey].ToString();
            if (TabModuleSettings[ModuleSetting.TwitterConsumerSecret] != null)
                tbTwitterConsumerSecret.Text = TabModuleSettings[ModuleSetting.TwitterConsumerSecret].ToString();
            if (TabModuleSettings[ModuleSetting.LinkedInConsumerKey] != null)
                tbLinkedInConsumerKey.Text = TabModuleSettings[ModuleSetting.LinkedInConsumerKey].ToString();
            if (TabModuleSettings[ModuleSetting.LinkedInConsumerSecret] != null)
                tbLinkedInConsumerSecret.Text = TabModuleSettings[ModuleSetting.LinkedInConsumerSecret].ToString();
            if (TabModuleSettings[ModuleSetting.LinkedInConsumerKey] != null)
                tbLinkedInConsumerKey.Text = TabModuleSettings[ModuleSetting.LinkedInConsumerKey].ToString();
            if (TabModuleSettings[ModuleSetting.LinkedInConsumerSecret] != null)
                tbLinkedInConsumerSecret.Text = TabModuleSettings[ModuleSetting.LinkedInConsumerSecret].ToString();
            if (TabModuleSettings[ModuleSetting.FacebookConsumerKey] != null)
                tbFacebookConsumerKey.Text = TabModuleSettings[ModuleSetting.FacebookConsumerKey].ToString();
            if (TabModuleSettings[ModuleSetting.FacebookConsumerSecret] != null)
                tbFacebookConsumerSecret.Text = TabModuleSettings[ModuleSetting.FacebookConsumerSecret].ToString();
            #endregion
            if (TabModuleSettings[ModuleSetting.ApproveTweetParameterName] != null)
                tbApproveTweetParameterName.Text = TabModuleSettings[ModuleSetting.ApproveTweetParameterName].ToString();
	        if (TabModuleSettings[ModuleSetting.HideMailingCheckbox] != null)
		        cbHideMailingCheckbox.Checked = bool.Parse(TabModuleSettings[ModuleSetting.HideMailingCheckbox].ToString());
            if (TabModuleSettings[ModuleSetting.ManualOptIn] != null)
                cbManualOptIn.Checked = bool.Parse(TabModuleSettings[ModuleSetting.ManualOptIn].ToString());
            if (TabModuleSettings[ModuleSetting.CustomBaseParamsForThemes] != null)
                tbCustomBaseParamsForThemes.Text = TabModuleSettings[ModuleSetting.CustomBaseParamsForThemes].ToString();
            if (TabModuleSettings[ModuleSetting.ThemeRoleAssociation] != null)
                tbThemeRoleAssociation.Text = TabModuleSettings[ModuleSetting.ThemeRoleAssociation].ToString();
            if (TabModuleSettings[ModuleSetting.DefaultCountry] != null)
                tbDefaultCountry.Text = TabModuleSettings[ModuleSetting.DefaultCountry].ToString();
            if (TabModuleSettings[ModuleSetting.ProjectName] != null)
                tbProjectName.Text = TabModuleSettings[ModuleSetting.ProjectName].ToString();
            if (TabModuleSettings[ModuleSetting.ProjectMode] != null)
                tbProjectMode.Text = TabModuleSettings[ModuleSetting.ProjectMode].ToString();
            if (TabModuleSettings[ModuleSetting.GenerateDeeplink] != null)
                cbGenerateDeeplink.Checked = bool.Parse(TabModuleSettings[ModuleSetting.GenerateDeeplink].ToString());
            else
                cbGenerateDeeplink.Checked = true;
        }

        /// <summary>
        /// Updates the settings.
        /// </summary>
        /// <author>Vu Dinh</author>
        /// <modified>02/12/2014 15:59:39</modified>
        public override void UpdateSettings()
        {
            var objModules = new ModuleController();
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.TabName, tbTabName.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.SupplierId, tbSupplierId.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.TwitterConsumerKey, tbTwitterConsumerKey.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.TwitterConsumerSecret, tbTwitterConsumerSecret.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.LinkedInConsumerKey, tbLinkedInConsumerKey.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.LinkedInConsumerSecret, tbLinkedInConsumerSecret.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.FacebookConsumerKey, tbFacebookConsumerKey.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.FacebookConsumerSecret, tbFacebookConsumerSecret.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.ApproveTweetParameterName, tbApproveTweetParameterName.Text);
	        objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.HideMailingCheckbox, cbHideMailingCheckbox.Checked.ToString());
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.ManualOptIn, cbManualOptIn.Checked.ToString());
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.CustomBaseParamsForThemes, tbCustomBaseParamsForThemes.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.ThemeRoleAssociation, tbThemeRoleAssociation.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.DefaultCountry, tbDefaultCountry.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.ProjectName, tbProjectName.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.ProjectMode, tbProjectMode.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, ModuleSetting.GenerateDeeplink, cbGenerateDeeplink.Checked.ToString());
            //refresh cache
            ModuleController.SynchronizeModule(ModuleId);
        }

        #endregion
    }

    public class ModuleSetting
    {
        public static string TabName = "TabName";
        public static string TwitterConsumerKey = "TwitterConsumerKey";
        public static string TwitterConsumerSecret = "ConsumerSecret";
        public static string FollowTwitterCompanyParameter = "FollowTwitterCompanyParameter";
        public static string LinkedInConsumerKey = "LinkedInConsumerKey";
        public static string LinkedInConsumerSecret = "LinkedInConsumerSecret";
        public static string FacebookConsumerKey = "FacebookConsumerKey";
        public static string FacebookConsumerSecret = "FacebookConsumerSecret";
        public static string ApproveTweetParameterName = "ApproveTweetParameterName";
        public static string SupplierId = "SupplierId";
		public static string HideMailingCheckbox = "HideMailingCheckbox";
        public static string ManualOptIn = "ManualOptIn";
        public static string CustomBaseParamsForThemes = "CustomBaseParamsForThemes ";
        public static string ThemeRoleAssociation = "ThemeRoleAssociation";
        public static string DefaultCountry = "DefaultCountry";
        public static string ProjectName = "ProjectName";
        public static string ProjectMode = "ProjectMode";
        public static string GenerateDeeplink = "GenerateDeeplink";
    }
}
