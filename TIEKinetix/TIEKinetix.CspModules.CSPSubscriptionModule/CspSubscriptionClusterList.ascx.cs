﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web.UI.WebControls;
using TIEKinetix.CspModules.CSPSubscriptionModule.Components;
using TIEKinetix.CspModules.CSPSubscriptionModule.Components.Data;
using DotNetNuke.Common.Utilities;
using Telerik.Web.UI;

namespace TIEKinetix.CspModules.CSPSubscriptionModule
{
    public partial class CspSubscriptionClusterList : DnnModuleBase
    {
        private DnnCspSubscriptionModuleContext _dnnContext;

        protected override void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
            _dnnContext = new DnnCspSubscriptionModuleContext(Config.GetConnectionString());
        }

        protected void Page_Load()
        {
           
        }

        protected void rgCluster_Language_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if(e.Item is GridDataInsertItem && e.Item.OwnerTableView.IsItemInserted)
            {
                e.Item.OwnerTableView.GetColumn("editcolumn").Visible = true;
                foreach (GridDataItem dataItem in ViewState)
                {
                    ((ImageButton)dataItem["editcolumn"].Controls[0] as ImageButton).Visible = true;
                }
            }
            else
            {
                e.Item.OwnerTableView.GetColumn("editcolumn").Visible = false;
            }
            if (e.Item.IsInEditMode)
            {
                var item = (GridEditableItem)e.Item;
                var rlbCountry = (RadComboBox)item.FindControl("rlbLanguage");

                var clusterId = int.Parse(((HiddenField)e.Item.OwnerTableView.OwnerGrid.Parent.FindControl("clusterId")).Value);

                var clusterLanguages = (from lang in _dnnContext.CSPSubscriptionModule_Cluster_Languages.ToList() where lang.ClusterId == clusterId select lang.LanguageId).ToList();
                rlbCountry.DataSource = CspContext.languages.Where(a => !clusterLanguages.Contains(a.languages_Id));
                rlbCountry.DataTextField = "description";
                rlbCountry.DataValueField = "languages_Id";
                rlbCountry.DataBind();
            }
        }

        protected void rgCluster_Country_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataInsertItem && e.Item.OwnerTableView.IsItemInserted)
            {
                e.Item.OwnerTableView.GetColumn("editcolumn").Visible = true;
                foreach (GridDataItem dataItem in e.Item.OwnerTableView.Items)
                {
                    ((ImageButton)dataItem["editcolumn"].Controls[0]).Visible = false;
                }
            }
            else
            {
                e.Item.OwnerTableView.GetColumn("editcolumn").Visible = false;
            } 
            if (e.Item.IsInEditMode)
            {
                var item = (GridEditableItem)e.Item;
                var rlbCountry = (RadComboBox)item.FindControl("rlbCountry");

                var clusterId = int.Parse(((HiddenField)e.Item.OwnerTableView.OwnerGrid.Parent.FindControl("clusterId")).Value);
                var countries =
                    _dnnContext.CSPSubscriptionModule_Countries.Where(
                        a =>
                        _dnnContext.CSPSubscriptionModule_Cluster_Countries.Where(b => b.CountryId == a.Id).Count(
                            b => b.ClusterId == clusterId) == 0);
                rlbCountry.DataSource = countries;
                rlbCountry.DataTextField = "Name";
                rlbCountry.DataValueField = "Id";
                rlbCountry.DataBind();
            }
        }

        protected void rgCluster_Language_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            var clusterId = ((HiddenField)e.Item.OwnerTableView.OwnerGrid.Parent.FindControl("clusterId")).Value;
            var rlbLanguage = (RadComboBox)e.Item.FindControl("rlbLanguage");
            switch (e.CommandName)
            {
                case RadGrid.PerformInsertCommandName:
                    var items = rlbLanguage.CheckedItems.ToList();
                    var listLanguagesToInsert = new List<CSPSubscriptionModule_Cluster_Language>();
                    foreach (RadComboBoxItem item in items)
                    {
                        listLanguagesToInsert.Add(new CSPSubscriptionModule_Cluster_Language
                                                      {
                                                          ClusterId = int.Parse(clusterId),
                                                          LanguageId = int.Parse(item.Value)
                                                      });
                    }
                    _dnnContext.CSPSubscriptionModule_Cluster_Languages.InsertAllOnSubmit(listLanguagesToInsert);
                    _dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                    UpdateClusterLanguage(int.Parse(clusterId), e);
                    //rgClusterList.Rebind();
                    break;
                case RadGrid.DeleteCommandName:
                    var id = int.Parse((e.Item as GridEditableItem).GetDataKeyValue("languages_Id").ToString());
                    _dnnContext.CSPSubscriptionModule_Cluster_Languages.DeleteAllOnSubmit(_dnnContext.CSPSubscriptionModule_Cluster_Languages.Where(a => a.LanguageId == id));
                    _dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                    UpdateClusterLanguage(int.Parse(clusterId), e);
                    //rgClusterList.Rebind();
                    break;
            }
        }

        protected void rgCluster_Country_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            var clusterId = ((HiddenField) e.Item.OwnerTableView.OwnerGrid.Parent.FindControl("clusterId")).Value;
            var rlbCountry = (RadComboBox) e.Item.FindControl("rlbCountry");
            switch (e.CommandName)
            {
                case RadGrid.PerformInsertCommandName:
                    var items = rlbCountry.CheckedItems.ToList();
                    var listCountriesToInsert = new List<CSPSubscriptionModule_Cluster_Country>();
                    foreach (RadComboBoxItem item in items)
                    {
                        listCountriesToInsert.Add(new CSPSubscriptionModule_Cluster_Country
                                                      {
                                                          ClusterId = int.Parse(clusterId),
                                                          CountryId = int.Parse(item.Value)
                                                      });
                    }
                    _dnnContext.CSPSubscriptionModule_Cluster_Countries.InsertAllOnSubmit(listCountriesToInsert);
                    _dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                    UpdateClusterCountry(int.Parse(clusterId), e);
                    //rgClusterList.Rebind();
                    break;
                case RadGrid.DeleteCommandName:
                    var id = int.Parse((e.Item as GridEditableItem).GetDataKeyValue("Id").ToString());
                    _dnnContext.CSPSubscriptionModule_Cluster_Countries.DeleteAllOnSubmit(_dnnContext.CSPSubscriptionModule_Cluster_Countries.Where(a => a.CountryId == id));
                    _dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                    UpdateClusterCountry(int.Parse(clusterId), e);
                    //rgClusterList.Rebind();
                    break;
            }
            
        }

        protected void rgClusterList_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            var id = 0;
            var value = new List<object>();
            switch (e.CommandName)
            {
                case RadGrid.PerformInsertCommandName:
                    value = GetAutoGeneratedColumn(e.Item);
                    if(string.IsNullOrEmpty(value[0].ToString()))
                    {
                        break;
                    }
                    _dnnContext.CSPSubscriptionModule_Clusters.InsertOnSubmit(new CSPSubscriptionModule_Cluster { ClusterName = value[0].ToString(), Desciption = value[1].ToString() });
                    _dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                    rgClusterList.Rebind();
                    break;
                case RadGrid.UpdateCommandName:
                    id = (int) (e.Item as GridEditableItem).GetDataKeyValue("ClusterId");
                    value = GetAutoGeneratedColumn(e.Item);
                    if(string.IsNullOrEmpty(value[0].ToString()))
                    {
                        break;
                    }
                    var item = _dnnContext.CSPSubscriptionModule_Clusters.SingleOrDefault(a => a.ClusterId == id);
                    if(item != null)
                    {
                        item.ClusterName = value[0].ToString();
                        _dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                    }
                    rgClusterList.Rebind();
                    break;
                case RadGrid.DeleteCommandName:
                    id = (int)(e.Item as GridEditableItem).GetDataKeyValue("ClusterId");
                    _dnnContext.CSPSubscriptionModule_Clusters.DeleteAllOnSubmit(_dnnContext.CSPSubscriptionModule_Clusters.Where(a => a.ClusterId == id));
                    _dnnContext.CSPSubscriptionModule_Cluster_Languages.DeleteAllOnSubmit(_dnnContext.CSPSubscriptionModule_Cluster_Languages.Where(a => a.ClusterId == id));
                    _dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                    rgClusterList.Rebind();
                    break;
            }
        }

        protected void rgClusterList_OnItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridNestedViewItem)
            {
                var item = (GridNestedViewItem)e.Item;
                var rgClusterCountry = (RadGrid)item.FindControl("rgCluster_Country");
                var rgClusterLanguage = (RadGrid)item.FindControl("rgCluster_Language");
                var id = int.Parse(item.ParentItem.GetDataKeyValue("ClusterId").ToString());
                
                //Country data
                rgClusterCountry.DataSource = _dnnContext.CSPSubscriptionModule_Countries.Where(a => _dnnContext.CSPSubscriptionModule_Cluster_Countries.Where(b => b.CountryId == a.Id).Count(b => b.ClusterId == id) > 0);

                //Language data
                var clusterLanguages = new List<int>();
                foreach (var lang in _dnnContext.CSPSubscriptionModule_Cluster_Languages.ToList())
                {
                    if (lang.ClusterId == id)
                        clusterLanguages.Add(lang.LanguageId);
                }
                rgClusterLanguage.DataSource = CspContext.languages.Where(a => clusterLanguages.Contains(a.languages_Id));
            }
        }

        private List<object> GetAutoGeneratedColumn(GridItem item)
        {
            var gridEditableItem = (GridEditableItem)item;
            var gridEditManager = gridEditableItem.EditManager;
            var values = new List<object>();
            foreach (GridColumn column in item.OwnerTableView.RenderColumns)
            {
                if(column is IGridEditableColumn)
                {
                    var editAbleColumn = (IGridEditableColumn) column;
                    if(editAbleColumn.IsEditable)
                    {
                        IGridColumnEditor columnEditor = gridEditManager.GetColumnEditor(editAbleColumn);
                        object value = null;

                        if (columnEditor is GridTextColumnEditor)
                        {
                            value = (columnEditor as GridTextColumnEditor).Text;
                        }
                        else if (columnEditor is GridBoolColumnEditor)
                        {
                            value = (columnEditor as GridBoolColumnEditor).Value;
                        }
                        else if (columnEditor is GridDropDownColumnEditor)
                        {
                            value = (columnEditor as GridDropDownColumnEditor).SelectedValue;
                        }
                        if (value != null)
                        {
                            values.Add(value);
                        }
                    }
                }
            }
            return values;
        }

        protected void rgClusterList_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            rgClusterList.DataSource = _dnnContext.CSPSubscriptionModule_Clusters.ToList();
        }

        private void UpdateClusterCountry(int Id, GridCommandEventArgs e)
        {
            var clusterCountries = _dnnContext.CSPSubscriptionModule_Cluster_Countries.Where(a => a.ClusterId == Id);
            var countries = clusterCountries.Select(cspSubscriptionModuleClusterCountry => _dnnContext.CSPSubscriptionModule_Countries.SingleOrDefault(a => a.Id == cspSubscriptionModuleClusterCountry.CountryId)).ToList();
            e.Item.OwnerTableView.DataSource = countries;
        }

        private void UpdateClusterLanguage(int Id, GridCommandEventArgs e)
        {
            var clusterLanguages = (from lang in _dnnContext.CSPSubscriptionModule_Cluster_Languages.ToList() where lang.ClusterId == Id select lang.LanguageId).ToList();
            e.Item.OwnerTableView.DataSource = CspContext.languages.Where(a => clusterLanguages.Contains(a.languages_Id));
        }
    }
}