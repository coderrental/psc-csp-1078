﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using Telerik.Web.UI;
using TIEKinetix.CspModules.CSPSubscriptionModule.Components;
using TIEKinetix.CspModules.CSPSubscriptionModule.Components.Data;

namespace TIEKinetix.CspModules.CSPSubscriptionModule
{
    public partial class CspSubscriptionTranslateForm : DnnModuleBase
    {
        private Guid _formId;
        private DnnCspSubscriptionModuleContext _dnnContext;
        private CSPSubscriptionModule_Form _form;

        #region Overrides of UserControl

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            _formId = new Guid(Request.QueryString["id"]);

            _dnnContext = new DnnCspSubscriptionModuleContext(Config.GetConnectionString());
            _form = _dnnContext.CSPSubscriptionModule_Forms.Single(a => a.FormId == _formId);
            lName.Text = _form.Name;

            // go back event
            Toolbar.ButtonClick += new RadToolBarEventHandler(Toolbar_ButtonClick);

            // render fields
            RenderTranslationFields();

            // get values using the current portal language
            GetTranslatedFieldValues(CurrentLanguage);
        }

        #endregion

        #region Page Events
        void Toolbar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            switch (e.Item.Value)
            {
                case "Save":
                    // save form translation
                    SaveTranslation(_formId, tbFormName.Text, tbFormDesc.Text);

                    // save form field translation
                    string controlId, shortDesc, longDesc;
                    Control control;
                    foreach (CSPSubscriptionModule_Field field in _dnnContext.CSPSubscriptionModule_Fields.Where(a => a.FormId == _formId).OrderBy(a => a.DisplayOrder))
                    {
                        controlId = field.FormId + "_" + field.FieldId;
                        shortDesc = longDesc = string.Empty;

                        // name
                        control = formFields.FindControl("n" + controlId);
                        shortDesc = control != null ? (control as TextBox).Text : field.Name;

                        // description
                        control = formFields.FindControl("d" + controlId);
                        longDesc = control != null ? (control as TextBox).Text : field.Name;

                        SaveTranslation(field.FieldId, shortDesc, longDesc);
                    }
                    break;
                case "Back":
                    string url = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Configuration", "mid", ModuleId.ToString());
                    Response.Redirect(url, true);
                    break;
                default:
                    break;

            }
        }

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        #endregion

        

        #region Functions

        /// <summary>
        /// render form field textboxes to translate name and description of each field
        /// </summary>
        private void RenderTranslationFields()
        {
            Table table = new Table();
            formFields.Controls.Add(table);
            string controlId = string.Empty;

            foreach (CSPSubscriptionModule_Field field in _dnnContext.CSPSubscriptionModule_Fields.Where(a => a.FormId == _formId).OrderBy(a => a.DisplayOrder))
            {
                controlId = field.FormId + "_" + field.FieldId;
                TableRow tr = new TableRow();
                table.Rows.Add(tr);
                TableCell td = new TableCell();

                // field name
                td.RowSpan = 2;
                td.VerticalAlign = VerticalAlign.Top;
                td.Controls.Add(new Label
                {
                    Text = field.Name
                });
                tr.Cells.Add(td);

                // field name translation
                td = new TableCell();
                td.Controls.Add(new TextBox { ID = "n" + controlId, Width = new Unit(350), CssClass = "csp-form-field" });
                tr.Cells.Add(td);

                // field desc translation
                tr = new TableRow();
                table.Rows.Add(tr);
                td = new TableCell();
                td.Controls.Add(new TextBox { ID = "d" + controlId, Rows = 5, TextMode = TextBoxMode.MultiLine, Width = new Unit(350), CssClass = "csp-form-field" });
                tr.Cells.Add(td);
            }
        }

        /// <summary>
        /// get translated text for all fields per requested form
        /// </summary>
        /// <param name="currentLanguage"></param>
        private void GetTranslatedFieldValues(string currentLanguage)
        {
            // find tranlation for form
            TranslationText translation = GetTranslation(_form.FormId, _form.Name);
            tbFormName.Text = translation.ShortDescription;
            tbFormDesc.Text = translation.LongDescription;

            // find translation for form fields
            string controlId = string.Empty;
            Control control = null;
            
            foreach (CSPSubscriptionModule_Field field in _dnnContext.CSPSubscriptionModule_Fields.Where(a => a.FormId == _formId).OrderBy(a => a.DisplayOrder))
            {
                controlId = field.FormId + "_" + field.FieldId;
                translation = GetTranslation(field.FieldId, field.Name);

                // name
                control = formFields.FindControl("n" + controlId);
                if (control != null)
                {
                    (control as TextBox).Text = translation.ShortDescription;
                }

                // description
                control = formFields.FindControl("d" + controlId);
                if (control != null)
                {
                    (control as TextBox).Text = translation.LongDescription;
                }
            }
        }

        /// <summary>
        /// get translation record (short & long) for current dnn language
        /// </summary>
        /// <param name="id">record id to look up</param>
        /// <param name="defaultValue">return default value if record isnt found</param>
        /// <returns></returns>
        private TranslationText GetTranslation(Guid id, string defaultValue)
        {
            CSPSubscriptionModule_Translation t = _dnnContext.CSPSubscriptionModule_Translations.FirstOrDefault(a => a.Id == id && a.CultureCode == CurrentLanguage);
            return t == null ? new TranslationText(!string.IsNullOrEmpty(defaultValue) ? defaultValue : "Need Translation For [" + id + "]") : new TranslationText(t);
        }

        /// <summary>
        /// save/create tranlation record
        /// </summary>
        /// <param name="id">record id to look up</param>
        /// <param name="shortDesc"></param>
        /// <param name="longDesc"></param>
        private void SaveTranslation(Guid id, string shortDesc, string longDesc)
        {
            CSPSubscriptionModule_Translation translation = _dnnContext.CSPSubscriptionModule_Translations.SingleOrDefault(a => a.Id == id && a.CultureCode == CurrentLanguage);
            if (translation != null)
            {
                translation.ShortDesc = shortDesc;
                translation.LongDesc = longDesc;
            }
            else
            {
                _dnnContext.CSPSubscriptionModule_Translations.InsertOnSubmit(new CSPSubscriptionModule_Translation
                {
                    CultureCode = CurrentLanguage,
                    ShortDesc = shortDesc,
                    LongDesc = longDesc,
                    TranslationId = Guid.NewGuid(),
                    Id = id
                });
            }
            _dnnContext.SubmitChanges();
        }


        #endregion
    }
}