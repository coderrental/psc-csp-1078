﻿using System;
using CR.DnnModules.Common;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;
using DotNetNuke.Services.Log.EventLog;
using DocumentFormat.OpenXml.Extensions;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using CR.ContentObjectLibrary.Data;

namespace CR.CspContentExporter.Common
{
    public class DnnModuleBase : PortalModuleBase
    {
        protected EventLogController DnnEventLog;
        protected ModuleActionCollection MyActions;
        private string _localResourceFile;
        protected CspDataContext CspDataContext;

        /// <summary>
        /// Handles the Init event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected virtual void Page_Init(object sender, EventArgs e)
        {
            //Init Local Resource file
            _localResourceFile = Utils.GetCommonResourceFile("CR.CspContentExporter", LocalResourceFile);
            //Init module title
            ModuleConfiguration.ModuleTitle = GetLocalizedText("ModuleTitle");
            //Init csp data context
            CspDataContext = new CspDataContext(Utils.GetConnectionString(PortalId));
        }

        /// <summary>
        /// Get localized text for the current portal language
        /// </summary>
        /// <param name="key">Resource Key</param>
        /// <returns>Localized text</returns>
        public string GetLocalizedText(string key)
        {
            return Localization.GetString(key, _localResourceFile);
        }

        /// <summary>
        /// Write message to Dnn event view
        /// </summary>
        /// <param name="message">message to write</param>
        public void Log(string message)
        {
            DnnEventLog.AddLog("DEBUG", message, PortalSettings, UserId, EventLogController.EventLogType.ADMIN_ALERT);
        }
    }
}