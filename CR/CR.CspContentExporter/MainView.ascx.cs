﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using DocumentFormat.OpenXml.Extensions;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using Telerik.Web.UI;
using CR.CspContentExporter.Common;
using Telerik.Web.UI.GridExcelBuilder;
using CR.CspContentExporter.Entities;

namespace CR.CspContentExporter
{
    public partial class MainView : DnnModuleBase
    {
        private List<Entities.SheetEntity> _listSheets;
        private List<string> _customeColumns; 
        /// <summary>
        /// Handles the Init event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 2/15/2013 - 9:34 AM
        protected override void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);

            _customeColumns = new List<string> {"CATEGORIES", "STAGEID", "LANG", "SUPPLIER" };

            //Localize the controls
            LocalizedControls();
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 2/15/2013 - 9:34 AM
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region [ Private Functions ]
        /// <summary>
        /// Localizeds the text.
        /// </summary>
        /// Author: Vu Dinh
        /// 2/15/2013 - 10:19 AM
        private void LocalizedControls()
        {
            tbExport.Text = GetLocalizedText("Label.Export");
        }

        /// <summary>
        /// Gets the list sheets.
        /// </summary>
        /// Author: Vu Dinh
        /// 2/15/2013 - 10:29 AM
        private void GetListSheets()
        {
            _listSheets = new List<SheetEntity>();
            //Get list of content type
            var contentTypes = CspDataContext.content_types.ToList();
            foreach (var contentType in contentTypes)
            {
                var data = GetDataFromContentType(contentType);
                if (data == null)
                    continue;
                _listSheets.Add(new SheetEntity
                {
                    SheetName = contentType.description,
                    Data = GetDataFromContentType(contentType)
                });
            }
        }


        /// <summary>
        /// Exports the data.
        /// </summary>
        /// Author: Vu Dinh
        /// 2/15/2013 - 10:48 AM
        private void ExportData()
        {
            if (_listSheets.Any())
            {
                //Create excel stream
                MemoryStream stream = SpreadsheetReader.Create();
                // Open the document for editing.
                using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(stream, true))
                {
                    for (int i = 1; i <= _listSheets.Count; i++)
                    {
                        var item = _listSheets[i - 1];
                        if (i <= 3)
                        {
                            var sheets = spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>();
                            Sheet sheet = sheets.Elements<Sheet>().Single(a => a.Name == "Sheet" + i);
                            sheet.Name = item.SheetName;
                            spreadSheet.WorkbookPart.Workbook.Save();
                        }
                        else
                        {
                            // Add a blank WorksheetPart.
                            var newWorksheetPart = spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();
                            newWorksheetPart.Worksheet = new Worksheet(new SheetData());

                            var sheets = spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>();

                            // Get a unique ID for the new worksheet.
                            uint sheetId = 1;
                            if (sheets.Elements<Sheet>().Any())
                            {
                                sheetId = sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                            }

                            // Append the new worksheet and associate it with the workbook.
                            var sheet = new Sheet() { Id = spreadSheet.WorkbookPart.GetIdOfPart(newWorksheetPart), SheetId = sheetId, Name = item.SheetName };
                            sheets.Append(sheet);
                            spreadSheet.WorkbookPart.Workbook.Save();
                        }

                        var worksheetPart = SpreadsheetReader.GetWorksheetPartByName(spreadSheet, item.SheetName);
                        var writer = new WorksheetWriter(spreadSheet, worksheetPart);
                        //Write data to sheet. Col name at A1, data from A2
                        writer.PasteDataTable(item.Data, "A1");
                    }
                }



                // Write to response stream
                Response.Clear();
                Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", "ContentExport.xlsx"));
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                stream.WriteTo(Response.OutputStream);
                Response.End();
            }
        }

        /// <summary>
        /// Gets the type of the data from content.
        /// </summary>
        /// <param name="contentType">Type of the content.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 2/15/2013 - 10:43 AM
        private DataTable GetDataFromContentType(content_type contentType)
        {
            var table = new DataTable();
            var contentTypeFields = CspDataContext.content_types_fields.Where(a => a.content_types_Id == contentType.content_types_Id);
            if (!contentTypeFields.Any()) //return if there are no content type fields
                return null;

            //Setup first row
            var firstRow = table.NewRow();

            //Add custom column
            foreach (var customcolumn in _customeColumns)
            {
                table.Columns.Add(customcolumn);
                firstRow[customcolumn] = customcolumn;
            }

            //Setup column
            foreach (var contentTypesField in contentTypeFields)
            {
                table.Columns.Add(contentTypesField.fieldname);
                firstRow[contentTypesField.fieldname] = contentTypesField.fieldname;

            }

            //Add first row
            table.Rows.Add(firstRow);
            var listContents = GetContents(table.Columns, contentType.content_types_Id);
            foreach (var contentEntity in listContents)
            {
                var row = table.NewRow();
                foreach (var column in table.Columns)
                {
                    if (column.ToString() == "CATEGORIES")
                    {
                        row[column.ToString()] = GetContentCategories(contentEntity.ContentId);
                    }
                    else // Fetch row data (content with content type fields)
                    {
                        row[column.ToString()] = contentEntity.FieldValuePair[column.ToString()];
                    }

                }
                table.Rows.Add(row);
            }

            return table;
        }

        /// <summary>
        /// Gets the contents.
        /// </summary>
        /// <param name="columns">The columns.</param>
        /// <param name="contentTypeId">The content type id.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 2/15/2013 - 4:52 PM
        private IEnumerable<ContentEntity> GetContents(DataColumnCollection columns, int contentTypeId)
        {
            var listContentEntities = new List<ContentEntity>();

            var query = @"select * from (
select cf.value_text, ctf.fieldname, c.content_Id, lg.description + ' (' + cast (lg.languages_Id as varchar(10)) + ')' as LANG, cp.companyname + ' (' + cast(cm.supplier_Id as varchar(10)) + ')' as SUPPLIER, c.stage_Id as STAGEID
from content c
join content_main cm on cm.content_main_Id = c.content_main_Id
join content_fields cf on cf.content_Id=c.content_Id
join companies cp on cm.supplier_Id = cp.companies_Id
join languages lg on c.content_types_languages_Id = lg.languages_Id
inner loop join content_types_fields ctf on ctf.content_types_fields_Id=cf.content_types_fields_Id
where cm.content_types_Id = [contenttypeid]
) as p
pivot
(
max(value_text) for fieldname in ([pivotcolumn])
) as p1";

            query = query.Replace("[contenttypeid]", contentTypeId.ToString());
            var pivotColumn = new StringBuilder();
            foreach (var column in columns)
            {
                if (_customeColumns.Contains(column.ToString()))
                    continue;
                pivotColumn.Append("[" + column + "],");
            }
            query = query.Replace("[pivotcolumn]", pivotColumn.ToString(0, pivotColumn.Length - 1));

            var connection = (SqlConnection)CspDataContext.Connection;
            if (connection.State != ConnectionState.Open)
                connection.Open();
            SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted);
            using (var command = new SqlCommand("", connection, transaction))
            {
                command.CommandText = query;
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var contentEntity = new ContentEntity { ContentId = (Guid)reader["content_Id"] };
                    foreach (var column in columns)
                    {
                        if (column.ToString() == "CATEGORIES") // ignore categories field
                            continue;
                         contentEntity.FieldValuePair.Add(column.ToString(), reader[column.ToString()].ToString());
                    }
                    listContentEntities.Add(contentEntity);
                }
                reader.Close();
            }
            transaction.Commit();
            connection.Close();
            return listContentEntities;
        }



        /// <summary>
        /// Gets the content categories.
        /// </summary>
        /// <param name="contentId">The content id.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 2/15/2013 - 5:26 PM
        private string GetContentCategories(Guid contentId)
        {
            string result = string.Empty;
            var query = @"
DECLARE @str NVARCHAR(MAX)
SELECT @str = COALESCE(@str  + ',' + cast(p.categoryId as varchar(10)) + '' , cast(p.categoryId as varchar(10)))
from 
(
select cat.categoryId, c.content_Id from categories cat
join content_categories cc on cc.category_Id = cat.categoryId
join content_main cm on cm.content_main_Id = cc.content_main_Id
join content c on c.content_main_Id = cm.content_main_Id 
where c.content_Id = '{0}'
) as p
select @str as CATEGORIES
";
            query = string.Format(query, contentId);

            var connection = (SqlConnection)CspDataContext.Connection;
            if (connection.State != ConnectionState.Open)
                connection.Open();
            SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted);
            using (var command = new SqlCommand("", connection, transaction))
            {
                command.CommandText = query;
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    result = reader["CATEGORIES"].ToString();
                }
                reader.Close();
            }
            transaction.Commit();
            connection.Close();
            return result;
        }

        /// <summary>
        /// Gets the content language.
        /// </summary>
        /// <param name="contentPiece">The content piece.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 2/15/2013 - 11:28 AM
        private string GetContentLanguage(content contentPiece)
        {
            if (contentPiece.content_types_languages_Id.HasValue)
            {
                var language = CspDataContext.languages.SingleOrDefault(a => a.languages_Id == contentPiece.content_types_languages_Id.Value);
                if (language != null)
                {
                    return string.Format("{0} ({1})", language.description, language.languages_Id);
                }
                return string.Format("({0})", contentPiece.content_types_languages_Id);
            }
            return string.Empty;
        }

        private string GetContentSupplier(content contentPiece)
        {
            var supplier = CspDataContext.companies.SingleOrDefault(a => a.companies_Id == contentPiece.content_main.supplier_Id);
            if (supplier != null)
            {
                return string.Format("{0} ({1})", supplier.companyname, supplier.companies_Id);
            }
            return string.Empty;
        }

        #endregion


        #region [ Event Handlers ]
        /// <summary>
        /// Handles the OnClick event of the tbExport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 2/15/2013 - 10:20 AM
        protected void tbExport_OnClick(object sender, EventArgs e)
        {
            //Get list of sheets to export
            GetListSheets();

            //Export list
            ExportData();
        }
        #endregion
    }
}