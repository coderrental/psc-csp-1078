﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;

namespace CR.CspContentExporter.Entities
{
    /// <summary>
    /// Content Entity
    /// </summary>
    /// Author: Vu Dinh
    /// 2/15/2013 - 1:13 PM
    public class ContentEntity
    {
        public ContentEntity()
        {
            FieldValuePair = new Dictionary<string, string>();
        }
        /// <summary>
        /// Gets or sets the content id.
        /// </summary>
        /// <value>
        /// The content id.
        /// </value>
        /// Author: Vu Dinh
        /// 2/15/2013 - 1:13 PM
        public Guid ContentId { get; set; }

        /// <summary>
        /// Gets or sets the field value pair.
        /// </summary>
        /// <value>
        /// The field value pair.
        /// </value>
        /// Author: Vu Dinh
        /// 2/15/2013 - 1:15 PM
        public Dictionary<string, string> FieldValuePair { get; set; }
    }
}