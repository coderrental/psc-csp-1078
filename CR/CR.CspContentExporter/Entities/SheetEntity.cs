﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
namespace CR.CspContentExporter.Entities
{
    /// <summary>
    /// Sheet Entity
    /// </summary>
    /// Author: Vu Dinh
    /// 2/15/2013 - 10:25 AM
    public class SheetEntity
    {
        /// <summary>
        /// Gets or sets the name of the sheet.
        /// </summary>
        /// <value>
        /// The name of the sheet.
        /// </value>
        /// Author: Vu Dinh
        /// 2/15/2013 - 10:26 AM
        public string SheetName { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        /// Author: Vu Dinh
        /// 2/15/2013 - 10:27 AM
        public DataTable Data { get; set; }
    }

    
}