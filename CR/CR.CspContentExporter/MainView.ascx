﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainView.ascx.cs" Inherits="CR.CspContentExporter.MainView" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Panel runat="server" ID="mainPn">
    <h2 class="header"><%= GetLocalizedText("ModuleTitle.Text")%></h2>
    <div style="text-align: center">
        <telerik:RadButton runat="server" ID="tbExport" OnClick="tbExport_OnClick">
            <Icon PrimaryIconCssClass="rbDownload" PrimaryIconLeft="4" PrimaryIconTop="4" />
        </telerik:RadButton>
    </div>
</asp:Panel>