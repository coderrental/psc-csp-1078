﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CR.Tools.Crawler.Entities;
using CR.Tools.Crawler.Helpers;
using CR.Tools.Crawler.Providers;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace CR.Tools.Crawler.Website
{
    class Selenium : ICRCrawlers
    {
        /// <summary>
        /// Gets the content of the list page to get.
        /// </summary>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 4/10/2013 - 6:38 PM
        public void SaveToXml()
        {
            var firefox = new FirefoxDriver();
            try
            {
                
                firefox.Navigate().GoToUrl("http://docs.seleniumhq.org");
                var menus = firefox.FindElements(By.CssSelector("#header ul li"));
                var listPage = menus.Select(menu => new PageItem
                {
                    PageName = menu.Text,
                    PageUrl = menu.FindElement(By.TagName("a")).GetAttribute("href")
                }).ToList();


                foreach (var pageItem in listPage)
                {
                    firefox.Navigate().GoToUrl(pageItem.PageUrl);
                    pageItem.PageContent = firefox.FindElementById("mainContent").GetAttribute("innerHTML");
                }


                var xmlHelper = new XmlHelper("C:\\", "test.xml");
                xmlHelper.CreateXmlFile(listPage);
                
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
            finally
            {
                firefox.Close();
            }
           
        }
    }
}
