﻿#region

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using CR.Tools.Crawler.Entities;
using CR.Tools.Crawler.Providers;
using HtmlAgilityPack;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using log4net;
using log4net.Config;

#endregion

namespace CR.Tools.Crawler.Website
{
	internal class Mitelv1 : ICRCrawlers
	{
		private FirefoxDriver _firefox;
		private WebDriverWait _wait;
		private CspDataContext _cspDataContext;
		private CspUtils _cspUtils;

		private int _categoryContentTypeId, _pagesContentTypeId, _languageId, _supplierId, _rootCategoryId, _themeId;
		private string _connectionstring;
		private List<string> _langCodeList;
		private WebClient _wb;
		private List<PageItem> _pageList;
		private List<PageItem> _failedList = new List<PageItem>();
		private const string MITEL_URL = "http://www.mitel.com";
		private string _crrLangCode = "";
		private bool _isReset;
		private int _failedCount = 0;
		private string _imagesFolderUrl = "";
		private string _docFolderUrl = "";
		private readonly List<DownloadFile> _fileList = new List<DownloadFile>();
		private bool _reachTheFailed = false;
		private HtmlDocument _htmlDocument;

		private static readonly ILog log = LogManager.GetLogger("Crawler");

		/// <summary>
		/// Saves to XML.
		/// </summary>
		/// <author>ducuytran - 4/26/2013</author>
		public void SaveToXml()
		{
			XmlConfigurator.Configure(); // default
			InitResource();
			if (!_isReset)
				InitPageList(); //Get first pages
			foreach (string langCode in _langCodeList)
			{
				if (_isReset)
				{
					if (langCode != _crrLangCode)
						continue;
					_isReset = false;
				}
				Log("+ Language code: " + langCode);
				_crrLangCode = langCode;
				_languageId = int.Parse(ConfigurationManager.AppSettings[langCode + "LangId"]);
				
				if (!_pageList.Any())
					break;

				//Get all pages and pages' contents
				if (_pageList.Any())
					GetContent();

				MapPageListToCategories();

				SaveToDb(_pageList.FirstOrDefault(p => p.ParentPageId == _rootCategoryId).PageId);
				ResetPageList();

				//break;
			}

			GoDownload();
		}

		/// <summary>
		/// Gets the content.
		/// </summary>
		/// <author>ducuytran - 4/26/2013</author>
		private void GetContent()
		{
			if (!_pageList.Any(p => p.PageContent == String.Empty))
				return;
			if (_failedCount >= 3)
			{
				Log("GAME OVER");
				return;
			}
			foreach (PageItem pageItem in _pageList)
			{
				bool foundNewItems = false;
				if (!String.IsNullOrEmpty(pageItem.PageContent))
					continue;

				pageItem.PageContent = @"N\A";
				_htmlDocument = new HtmlDocument();
				_wb.Headers.Set(HttpRequestHeader.Cookie, "com.ibm.wps.state.preprocessors.locale.LanguageCookie=" + _crrLangCode);
				try
				{
					Stream data = _wb.OpenRead(pageItem.PageUrl);
					if (data == null)
					{
						throw new Exception("data empty");
					}
					Log("[+] Start getting " + pageItem.PageUrl);
					_htmlDocument.Load(data, Encoding.UTF8);
					HtmlNode mainContent = ParseHtmlContent(_htmlDocument, pageItem);

					if (mainContent != null)
					{
						if (UpdatePageList(mainContent, pageItem))
						{
							foundNewItems = true;
						}
						
						foreach (HtmlNode listItem in mainContent.Descendants().Where(n => n.Name == "div" && n.Attributes["class"] != null && n.Attributes["class"].Value.IndexOf("section-sub-topic") > -1))
						{
							listItem.ParentNode.RemoveChild(listItem);
						}
						pageItem.PageContent = mainContent.OuterHtml;
					}
					data.Close();
					_wb.Dispose();
					if (foundNewItems)
						break;
				}
				catch (Exception)
				{
					pageItem.PageContent = "Not available";
					Log(pageItem.PageName + "-" + pageItem.PageId + "-Not available");
					//_failedCount++;
					break;
				}
			}
			GetContent();
		}

		/// <summary>
		/// Maps the page list to categories.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>4/27/2013</datetime>
		private void MapPageListToCategories()
		{
			Log("Mapping page list to categories...");
			foreach (PageItem pageItem in _pageList)
			{
				category existCategory =
					_cspDataContext.categories.FirstOrDefault(a => a.categoryText.ToLower() == pageItem.PageName.ToLower());
				if (existCategory == null)
				{
					existCategory = new category
						{
							parentId = pageItem.ParentPageId,
							categoryText = pageItem.PageName,
							description = pageItem.PageName,
							ExternalCategoryCode = pageItem.PageName,
							dateAdded = DateTime.Now,
							active = true,
							publication_schemeID = 11,
							apply_to_childs = false,
							IsSubscribable = true,
							DisplayOrder = pageItem.PageOrder
						};
					_cspDataContext.categories.InsertOnSubmit(existCategory);
					_cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
				}
				foreach (PageItem item in _pageList.Where(p => p.ParentPageId == pageItem.PageId))
				{
					item.ParentPageId = existCategory.categoryId;
				}
				pageItem.PageId = existCategory.categoryId;
				pageItem.PageOrder = (int) existCategory.DisplayOrder;
			}
		}

		/// <summary>
		/// Saves to db.
		/// </summary>
		/// <param name="pageId">The page id.</param>
		/// <author>ducuytran</author>
		/// <datetime>4/27/2013</datetime>
		private void SaveToDb(int pageId)
		{
			PageItem pageItem = _pageList.FirstOrDefault(p => p.PageId == pageId);
			if (pageItem == null)
				return;
			Log("Updating " + pageItem.PageNameByCulture);

			List<PageItem> childrenPages = _pageList.Where(p => p.ParentPageId == pageItem.PageId).ToList();

			//Check if category is exist
			category cat = _cspDataContext.categories.FirstOrDefault(a => a.categoryText.ToLower() == pageItem.PageName.ToLower());
			
			//Add to Theme and Consumer Themes
			AddCategoryToThemeCatAndConsumerThemes(cat);
			//Insert/Update Category Content
			AddCategoryContent(pageItem, cat);
			//Insert/Update Content Page
			AddContentPage(pageItem, cat);

			//Update pageid by categoryid
			foreach (PageItem childPage in childrenPages)
			{
				childPage.ParentPageId = cat.categoryId;
				SaveToDb(childPage.PageId);
			}
		}

		/// <summary>
		/// Adds the category to theme cat and consumer themes.
		/// </summary>
		/// <param name="cat">The cat.</param>
		/// <author>ducuytran</author>
		/// <datetime>4/27/2013</datetime>
		private void AddCategoryToThemeCatAndConsumerThemes(category cat)
		{
			//add category to themes_categories
			var parentCat = _cspDataContext.categories.SingleOrDefault(a => a.categoryId == cat.parentId);
			themes_category existThemesCategorie =
				_cspDataContext.themes_categories.FirstOrDefault(a => a.category_Id == cat.categoryId);
			if (parentCat != null && existThemesCategorie == null)
			{
				List<themes_category> themeCat = _cspDataContext.themes_categories.Where(a => a.category_Id == parentCat.categoryId).ToList();
				var themeCatToInsert = new List<themes_category>();
				if (parentCat.categoryId == _rootCategoryId)
				{
					themeCatToInsert.Add(new themes_category
						{
							themes_Id = _themeId,
							category_Id = cat.categoryId
						});
				}
				else
				{
					foreach (var themesCategory in themeCat)
					{
						themeCatToInsert.Add(new themes_category
							{
								themes_Id = themesCategory.themes_Id,
								category_Id = cat.categoryId
							});
					}
				}
				if (themeCatToInsert.Any())
				{
					_cspDataContext.themes_categories.InsertAllOnSubmit(themeCatToInsert);
					_cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
				}
			}

			//add category to consumers_themes
			var companies = _cspDataContext.companies.ToList();
			var listConsumerTheme = new List<consumers_theme>();

			foreach (var company in companies)
			{
				if (parentCat.categoryId == _rootCategoryId && cat.IsSubscribable == false)
				{
					//Check if this theme was assigned to the company
					companies_theme assignedCompanyTheme = _cspDataContext.companies_themes.FirstOrDefault(a => a.theme.themes_Id == _themeId && a.company == company);
					if (assignedCompanyTheme == null)
						continue;
					List<themes_supplier> suppliers =
						_cspDataContext.themes_suppliers.Where(a => a.themes_Id == _themeId).ToList();

					if (suppliers.Count == 0)
						continue;
					foreach (themes_supplier themesSupplier in suppliers)
					{
						listConsumerTheme.Add(new consumers_theme
							{
								category_Id = cat.categoryId,
								consumer_Id = company.companies_Id,
								themes_Id = _themeId,
								supplier_Id = themesSupplier.companies_Id
							});
					}
				}
				else
				{
					foreach (var consumerTheme in _cspDataContext.consumers_themes.Where(a => a.company == company && a.category_Id == parentCat.categoryId))
					{
						if (_cspDataContext.consumers_themes.Any(a => a.company == company && a.category_Id == cat.categoryId))
							continue;
						listConsumerTheme.Add(new consumers_theme
							{
								category_Id = cat.categoryId,
								consumer_Id = company.companies_Id,
								themes_Id = consumerTheme.themes_Id,
								supplier_Id = consumerTheme.supplier_Id
							});
					}
				}
			}
			if (listConsumerTheme.Any())
			{
				_cspDataContext.consumers_themes.InsertAllOnSubmit(listConsumerTheme);
				_cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
			}
		}

		/// <summary>
		/// Adds the content of the category.
		/// </summary>
		/// <param name="pageItem">The page item.</param>
		/// <param name="cat">The cat.</param>
		/// <author>ducuytran</author>
		/// <datetime>4/27/2013</datetime>
		private void AddCategoryContent(PageItem pageItem, category cat)
		{
			content newContentCategory = CreateContents(cat, _categoryContentTypeId);

			if (newContentCategory == null)
			{
				throw new Exception("Create category content failed!");
			}
			_cspUtils.SetContentFieldValue(_categoryContentTypeId, newContentCategory, "Content_Title", pageItem.PageNameByCulture);
			_cspUtils.SetContentFieldValue(_categoryContentTypeId, newContentCategory, "Status_Sort_Order", pageItem.PageOrder.ToString("0000"));
			_cspUtils.SetContentFieldValue(_categoryContentTypeId, newContentCategory, "Content_Description_Short", String.Empty);
			_cspUtils.SetContentFieldValue(_categoryContentTypeId, newContentCategory, "Content_Image_Thumbnail", String.Empty);
		}

		/// <summary>
		/// Adds the content page.
		/// </summary>
		/// <param name="pageItem">The page item.</param>
		/// <param name="cat">The cat.</param>
		/// <author>ducuytran</author>
		/// <datetime>4/27/2013</datetime>
		private void AddContentPage(PageItem pageItem, category cat)
		{
			content newContentPage = CreateContents(cat, _pagesContentTypeId, pageItem);

			if (newContentPage == null)
			{
				throw new Exception("Create content page failed!");
			}

			_cspUtils.SetContentFieldValue(_pagesContentTypeId, newContentPage, "Content_Page", pageItem.PageContent);
		}

		/// <summary>
		/// Creates the contents.
		/// </summary>
		/// <param name="cat">The cat.</param>
		/// <param name="contentTypeId">The content type id.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>4/27/2013</datetime>
		private content CreateContents(category cat, int contentTypeId, PageItem pageItem = null)
		{
			int supplierId = _supplierId;
			int consumerId = 0;
			//category tmpCat = cat;
			content_main newContentMain =
				_cspDataContext.content_mains.FirstOrDefault(a => a.content_categories.Any(b => b.category_Id == cat.categoryId) && a.content_types_Id == contentTypeId);

			if (newContentMain == null)
			{
				//Find exist content main to assign to this category
				if (pageItem != null)
				{
					foreach (PageItem clonedItem in _pageList.Where(p => p.PageNameByCulture == pageItem.PageNameByCulture && p.PageId != pageItem.PageId))
					{
						category clonedCategory = _cspDataContext.categories.FirstOrDefault(a => a.categoryText == clonedItem.PageName);
						if (clonedCategory == null)
							continue;

						newContentMain =
							_cspDataContext.content_mains.FirstOrDefault(
								a =>
								a.content_categories.Any(b => b.category_Id == clonedCategory.categoryId) &&
								a.content_types_Id == contentTypeId);
						if (newContentMain == null)
							continue;

						content sampleContent =
							_cspDataContext.contents.FirstOrDefault(
								a => a.content_main == newContentMain && a.content_types_languages_Id == _languageId);
						String sampleContentPage = _cspUtils.GetContentFieldValue(sampleContent, "Content_Page");
						if (sampleContentPage != pageItem.PageContent)
							continue;

						break;
					}
				}
				if (newContentMain == null)
				{
					newContentMain = new content_main
						{
							content_main_Id = Guid.NewGuid(),
							content_main_key = cat.categoryText,
							content_identifier = cat.categoryText,
							content_types_Id = contentTypeId,
							content_staging_scheme_Id = 1,
							supplier_Id = supplierId,
							consumer_Id = consumerId
						};

					_cspDataContext.content_mains.InsertOnSubmit(newContentMain);
				}
				//cat = tmpCat;
				newContentMain.content_categories.Add(new content_category
					{
						content_main = newContentMain,
						category = cat,
						publication_scheme_Id = 11
					});
				_cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
			}

			content newContentCategory = _cspDataContext.contents.FirstOrDefault(a =>
			                                                                     a.content_main == newContentMain &&
			                                                                     a.content_types_languages_Id == _languageId);
			if (newContentCategory == null)
			{
				newContentCategory = new content
					{
						content_Id = Guid.NewGuid(),
						content_main = newContentMain,
						active = true,
						stage_Id = 50,
						content_types_languages_Id = _languageId
					};


				_cspDataContext.contents.InsertOnSubmit(newContentCategory);
				_cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
			}
			return newContentCategory;
		}

		/// <summary>
		/// Parses the content of the HTML.
		/// </summary>
		/// <param name="htmlDocument">The HTML document.</param>
		/// <param name="pageItem">The page item.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>4/26/2013</datetime>
		private HtmlNode ParseHtmlContent(HtmlDocument htmlDocument, PageItem pageItem)
		{
			if (htmlDocument == null)
				return null;
			HtmlNode mainContent = htmlDocument.DocumentNode.Descendants().FirstOrDefault(n => n.Name == "table" && n.Attributes["class"] != null && n.Attributes["class"].Value == "layoutColumn");
			if (mainContent == null)
				return null;

			if (pageItem.ParentPageId == _rootCategoryId)
			{
				HtmlNode navBreadCrumb =
					htmlDocument.DocumentNode.Descendants().FirstOrDefault(
						n => n.Name == "ol" && n.Attributes["class"] != null && n.Attributes["class"].Value == "breadcrumbs");
				if (navBreadCrumb != null)
				{
					HtmlNode bcActiveNode = navBreadCrumb.ChildNodes.FirstOrDefault(
						n => n.Name == "li" && n.Attributes["class"] != null && n.Attributes["class"].Value == "active");
					if (bcActiveNode != null)
						pageItem.PageNameByCulture = bcActiveNode.InnerText.Trim();
				}

				List<PageItem> lv1Items = _pageList.Where(p => p.ParentPageId == pageItem.PageId).ToList();
				HtmlNode mainMenu = htmlDocument.DocumentNode.Descendants().FirstOrDefault(n => n.Name == "ul" && n.Attributes["class"] != null && n.Attributes["class"].Value == "menu menu_black");
				HtmlNode liSolution =
					mainMenu.Descendants().FirstOrDefault(n => n.Name == "a" && n.Attributes["href"].Value.EndsWith("/solutions"));
				HtmlNode mainLi = liSolution.ParentNode;
				List<HtmlNode> lv2MenuNodes = mainLi.Descendants().Where(n => n.Name == "h3").ToList();
				foreach (HtmlNode lv2MenuNode in lv2MenuNodes)
				{
					HtmlNode aNode = lv2MenuNode.ChildNodes.FirstOrDefault(n => n.Name == "a");

					if (aNode != null)
					{
						PageItem targetItem = lv1Items.FirstOrDefault(p => p.PageUrl.Contains(aNode.Attributes["href"].Value));
						if (targetItem != null)
						{
							targetItem.PageNameByCulture = aNode.InnerText.Trim();
						}
					}
				}
			}

			//Modify page banner
			HtmlNode bannerNode = mainContent.Descendants().FirstOrDefault(
				n => n.Name == "div" && n.Attributes["class"] != null && n.Attributes["class"].Value.IndexOf("section-media") >= 0);
			if (bannerNode != null)
			{
				bannerNode.Attributes.Remove("class");
				bannerNode.Attributes.Add("id", "page-banner");
				HtmlNode bannerText =
					bannerNode.ChildNodes.FirstOrDefault(n => n.Name == "div" && n.Attributes["class"] != null && n.Attributes["class"].Value == "media-content");
				if (bannerText != null)
				{
					bannerText.Attributes.Remove("class");
					bannerText.Attributes.Remove("style");
					bannerText.Attributes.Add("id", "banner-main-text");
					bannerText.Attributes.Add("style", "position: absolute; top: 10px; left: 10px; width: 400px");
				}
			}

			//Modify img src
			foreach (HtmlNode imgNode in mainContent.Descendants().Where(n => n.Name == "img"))
			{
				String imgSrc = (imgNode.Attributes["src"] != null) ? imgNode.Attributes["src"].Value : String.Empty;
				if (String.IsNullOrEmpty(imgSrc))
					continue;
				if (!imgSrc.Contains("mitel.com") && !imgSrc.StartsWith("/"))
					imgSrc = "/" + imgSrc;
				string imgFileName = imgSrc.Split('?').First().Split('/').Last();
				if (!_fileList.Any(f => f.FileUrl == imgSrc))
				{
					_fileList.Add(new DownloadFile
						{
							FileName = imgFileName,
							FileUrl = (imgSrc.Contains("mitel.com")) ? imgSrc : MITEL_URL + imgSrc
						});
				}

				imgSrc = imgSrc.Split('?').First();
				imgSrc = _imagesFolderUrl + imgSrc.Split('/').Last();
				imgNode.Attributes["src"].Value = imgSrc;
				imgNode.Attributes.Remove("width");
				imgNode.Attributes.Remove("height");
			}

			//Modify learn-more-links
			HtmlNode readMoreLinks =
				mainContent.Descendants().FirstOrDefault(
					n =>
					n.Name == "div" && n.Attributes["class"] != null && n.Attributes["class"].Value.IndexOf("learn-more-links") >= 0);
			int externalLinksCount = 0;
			if (readMoreLinks != null)
			{
				List<HtmlNode> link2Remove = new List<HtmlNode>();
				List<HtmlNode> allLinks = readMoreLinks.Descendants().Where(n => n.Name == "a").ToList();
				foreach (HtmlNode pdfFileLink in allLinks)
				{
					String pdfSrc = (pdfFileLink.Attributes["href"] != null) ? pdfFileLink.Attributes["href"].Value : String.Empty;
					if (String.IsNullOrEmpty(pdfSrc))
					{
						link2Remove.Add(pdfFileLink);
						externalLinksCount++;
						continue;
					}
					if (!pdfSrc.Contains("mitel.com") && !pdfSrc.StartsWith("/"))
						pdfSrc = "/" + pdfSrc;
					string pdfFileName = pdfSrc.Split('?').First();
					pdfFileName = pdfFileName.Split('/').Last();
					if (!pdfFileName.ToLower().EndsWith(".pdf") || pdfSrc.ToLower().IndexOf(".pdf") == -1)
					{
						link2Remove.Add(pdfFileLink);
						externalLinksCount++;
						continue;
					}
					if (_fileList.Any(f => f.FileUrl == pdfSrc || f.FileName.ToLower() == pdfFileName.ToLower()))
					{
						pdfFileLink.Attributes["href"].Value = _docFolderUrl + pdfFileName;
						//continue;
					}
					else
					{
						_fileList.Add(new DownloadFile
							{
								FileName = pdfFileName,
								FileUrl = (pdfSrc.Contains("mitel.com")) ? pdfSrc : MITEL_URL + pdfSrc
							});

						pdfFileLink.Attributes["href"].Value = _docFolderUrl + pdfFileName;
					}

					Log("PDF File: {0} in {1}/{2}", pdfFileName, pageItem.PageName, pageItem.PageId);
				}
				if (externalLinksCount == allLinks.Count)
				{
					readMoreLinks.ParentNode.RemoveChild(readMoreLinks);
				}
				else
				{
					for (int i = link2Remove.Count - 1; i >= 0; i--)
					{
						HtmlNode parentNode = link2Remove[i].ParentNode;
						if (parentNode != null)
							parentNode.ParentNode.RemoveChild(parentNode);
						link2Remove.RemoveAt(i);
					}
				}
			}

			HtmlNode richTextNode =
				mainContent.Descendants().FirstOrDefault(
					n => n.Name == "div" && n.Attributes["class"] != null && n.Attributes["class"].Value.IndexOf("rich-text") >= 0);
			if (richTextNode != null)
			{
				List<HtmlNode> linkNodes = richTextNode.Descendants().Where(n => n.Name == "a").ToList();
				for (int i = linkNodes.Count - 1; i >= 0; i--)
				{
					if (linkNodes[i].Attributes["href"].Value.ToLower().Contains(".pdf") || linkNodes[i].ParentNode.Name.ToLower() == "li")
						continue;
					linkNodes[i].ParentNode.RemoveChild(linkNodes[i], true);
					linkNodes.RemoveAt(i);
				}
			}

			return mainContent;
		}

		/// <summary>
		/// Updates the page list.
		/// </summary>
		/// <param name="maincontent">The maincontent.</param>
		/// <param name="pageitem">The pageitem.</param>
		/// <returns></returns>
		/// <author>ducuytran - 4/26/2013</author>
		private bool UpdatePageList(HtmlNode maincontent, PageItem pageitem)
		{
			Log("Getting children pages...");
			List<HtmlNode> subList = maincontent.Descendants().Where(n => n.Name == "div" && n.Attributes["class"] != null && n.Attributes["class"].Value.IndexOf("section-sub-topic") > -1).ToList();
			/*List<PageItem> clonedPages =
				_pageList.Where(
					p => p.PageNameByCulture.ToLower() == pageitem.PageNameByCulture.ToLower() && p.PageId != pageitem.PageId).ToList();
			if (clonedPages.Any() && _crrLangCode == "en")
			{
				PageItem originalItem = null;
				foreach (PageItem clonedPage in clonedPages)
				{
					if (_pageList.Any(p => p.ParentPageId == clonedPage.PageId))
					{
						originalItem = clonedPage;
						break;
					}
				}
				if (originalItem != null)
				{
					List<PageItem> clonedChildrenList = _pageList.Where(p => p.ParentPageId == originalItem.PageId).ToList();
					if (!clonedChildrenList.Any())
						return false;
					Log("Found exist node");
					foreach (PageItem childItem in clonedChildrenList)
					{
						Log("Adding " + childItem.PageNameByCulture);
						if (IsNodeExist(childItem.PageNameByCulture, childItem.PageUrl, pageitem.PageId) == 1)
						{
							Log("Add Failed: " + childItem.PageNameByCulture + " - Duplicate");
							continue;
						}
						string pageIdentifier = "_" + _pageList.Count(p => p.PageNameByCulture == childItem.PageNameByCulture).ToString();
						_pageList.Add(new PageItem
							{
								ParentPageId = pageitem.PageId,
								PageId = _pageList.Count + _rootCategoryId + 1,
								PageName = childItem.PageNameByCulture + pageIdentifier,
								PageNameByCulture = childItem.PageNameByCulture,
								PageUrl = childItem.PageUrl,
								PageContent = childItem.PageContent
							});
						Log("Added: " + childItem.PageNameByCulture + pageIdentifier);
					}

					foreach (HtmlNode listItem in subList)
					{
						//pageitem.PageOrder = subList.IndexOf(listItem) + 1;
						listItem.ParentNode.RemoveChild(listItem);
					}
					return true;
				}
			}*/
			//List<HtmlNode> subList = maincontent.Descendants().Where(n => n.Name == "div" && n.Attributes["class"] != null && n.Attributes["class"].Value.IndexOf("section-sub-topic") > -1).ToList();

			if (!subList.Any())
				return false;
			int crrPageCount = _pageList.Count;
			foreach (HtmlNode listItem in subList)
			{
				if (_crrLangCode == "en")
					AddChildrenNodes(listItem, pageitem);
				else
				{
					UpdateChildrenNodesName(listItem, pageitem);
				}
				//pageitem.PageOrder = subList.IndexOf(listItem) + 1;
				listItem.ParentNode.RemoveChild(listItem);
			}
			int newPageCount = _pageList.Count;
			if (newPageCount - crrPageCount == 0)
				return false;
			Log("[+] Found " + (newPageCount - crrPageCount).ToString() + " new pages");
			return true;
		}

		/// <summary>
		/// Inits the page list.
		/// </summary>
		/// <author>ducuytran - 4/26/2013</author>
		private void InitPageList()
		{
			Log("Init page list...");
			HtmlDocument htmlDocument = new HtmlDocument();
			_wb.Headers.Add(HttpRequestHeader.Cookie, "com.ibm.wps.state.preprocessors.locale.LanguageCookie=" + _crrLangCode);
			Stream data = _wb.OpenRead("http://www.mitel.com");
			htmlDocument.Load(data, Encoding.UTF8);

			HtmlNode mainMenu = htmlDocument.DocumentNode.Descendants().FirstOrDefault(n => n.Name == "ul" && n.Attributes["class"] != null && n.Attributes["class"].Value == "menu menu_black");
			_pageList = new List<PageItem>();

			if (mainMenu == null)
				throw new System.ArgumentException("Main menu not found");
			foreach (HtmlNode htmlNode in mainMenu.Descendants().Where(n => n.Name == "li"))
			{
				if (htmlNode.Descendants().FirstOrDefault(n => n.Name == "a" && n.InnerText.ToLower() == "solutions") != null)
				{
					HtmlNode mainLi = htmlNode;
					_pageList.Add(new PageItem
						{
							ParentPageId = _rootCategoryId,
							PageId = _rootCategoryId + 1,
							PageName = "Solutions",
							PageUrl = MITEL_URL + htmlNode.Descendants().FirstOrDefault(n => n.Name == "a" && n.Attributes["href"].Value.Contains("/solutions")).Attributes["href"].Value,
							PageNameByCulture = "Solutions",
							PageContent = ""
						});

					List<HtmlNode> lv2Menus = mainLi.Descendants().Where(n => n.Name == "h3").ToList();
					foreach (HtmlNode lv2Menu in lv2Menus)
					{
						HtmlNode aNode = lv2Menu.ChildNodes.FirstOrDefault(n => n.Name == "a");

						if (aNode != null)
						{
							int crrId = _pageList.Count + _rootCategoryId + 1;
							_pageList.Add(new PageItem
								{
									ParentPageId = _rootCategoryId + 1,
									PageId = crrId,
									PageName = aNode.InnerText.Trim(),
									PageNameByCulture = aNode.InnerText.Trim(),
									PageUrl = MITEL_URL + aNode.Attributes["href"].Value,
									PageContent = "",
									PageOrder = lv2Menus.IndexOf(lv2Menu) + 1
								});
						}
					}
					break;
				}
			}
			data.Close();
			Log("Init page list done!");
		}

		/// <summary>
		/// Resets the page list.
		/// </summary>
		/// <author>ducuytran - 4/26/2013</author>
		private void ResetPageList()
		{
			foreach (PageItem pageItem in _pageList)
			{
				pageItem.PageContent = "";
			}
		}

		/// <summary>
		/// Adds the children nodes.
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="parentId">The parent id.</param>
		/// <param name="gotoNext">if set to <c>true</c> [goto next].</param>
		/// <author>ducuytran - 4/26/2013</author>
		private void AddChildrenNodes(HtmlNode container, PageItem parentPage)
		{
			if (container == null)
				return;
			List<HtmlNode> linkList =
				container.Descendants().Where(
					n => n.Name == "a" && n.Attributes["class"] != null && n.Attributes["class"].Value == "index-title").ToList();
			if (!linkList.Any())
				return;
			foreach (HtmlNode htmlNode in linkList)
			{
				Log("Adding " + htmlNode.InnerText.Trim());
				String pageName = htmlNode.InnerText.Trim();
				String pageIdentifier = "";
				string pageContent = "";
				int newId = _pageList.Count + _rootCategoryId + 1;
				if (String.IsNullOrEmpty(pageName))
				{
					Log("Add failed " + htmlNode.InnerText.Trim() + " - Duplicate");
					continue;
				}
				string nodeUrl = htmlNode.Attributes["href"].Value;
				if (!nodeUrl.Contains("mitel.com"))
					nodeUrl = MITEL_URL + nodeUrl;

				int existCode = IsNodeExist(pageName, nodeUrl, parentPage.PageId);
				if (existCode == 1)
					continue;
				
				if (existCode == -1)
				{
					int noIdentifier = _pageList.Count(p => p.PageNameByCulture.ToLower() == pageName.ToLower());
					pageIdentifier = "_" + noIdentifier.ToString();
				}
				_pageList.Add(new PageItem
					{
						ParentPageId = parentPage.PageId,
						PageId = newId,
						PageName = pageName + pageIdentifier,
						PageNameByCulture = pageName,
						PageUrl = nodeUrl,
						PageContent = pageContent
					});
				Log("Added " + pageName + pageIdentifier + "");
				/*PageItem newPage = new PageItem
				                   	{
										ParentPageId = parentPage.PageId,
				                   		PageId = newId,
				                   		PageName = pageName + pageIdentifier,
				                   		PageNameByCulture = pageName,
				                   		PageUrl = nodeUrl,
				                   		PageContent = pageContent
				                   	};
				_pageList.Add(newPage);

				if (existCode == 0)
				{
					//PageItem parentNode = _pageList.FirstOrDefault(p => p.PageId == parentId);
					List<PageItem> clonedList =
						_pageList.Where(
							p => p.PageNameByCulture.ToLower() == parentPage.PageNameByCulture.ToLower() && p.PageId != parentPage.PageId).ToList();
					if (clonedList.Any())
					{
						for (int i = 0; i < clonedList.Count; i++)
						{
							if (IsNodeExist(pageName, nodeUrl, clonedList[i].PageId) == 1)
								continue;
							int newIdentifier = i + 1;
							_pageList.Add(new PageItem
							{
								ParentPageId = clonedList[i].PageId,
								PageId = _pageList.Count,
								PageName = pageName + "_" + newIdentifier.ToString(),
								PageNameByCulture = pageName,
								PageUrl = "",
								PageContent = ""
							});
						}
					}
				}
				else if (existCode == -1)
				{
					int noIdentifier = _pageList.Count(p => p.PageNameByCulture.ToLower() == pageName.ToLower());
					noIdentifier--;
					//noIdentifier++;
					pageIdentifier = "_" + noIdentifier.ToString();
					PageItem existPage = _pageList.FirstOrDefault(p => p.PageName.ToLower() == pageName.ToLower());
					List<PageItem> existPageChildren = _pageList.Where(p => p.ParentPageId == existPage.PageId).ToList();

					newPage.PageContent = existPage.PageContent;
					newPage.PageName = pageName + pageIdentifier;
					if (!existPageChildren.Any())
						return;
					foreach (PageItem existPageChild in existPageChildren)
					{
						if (IsNodeExist(existPageChild.PageName, existPageChild.PageUrl, newPage.PageId) == 1)
							continue;
						int newIdentifier = _pageList.Count(p => p.PageNameByCulture.ToLower() == existPageChild.PageNameByCulture.ToLower());
						_pageList.Add(new PageItem
											{
												ParentPageId = newPage.PageId,
												PageId = _pageList.Count,
												PageName = existPageChild.PageName + "_" + newIdentifier.ToString(),
												PageNameByCulture = existPageChild.PageName,
												PageUrl = existPageChild.PageUrl,
												PageContent = existPageChild.PageContent
											});
					}
				}*/
			}
		}

		private void UpdateChildrenNodesName(HtmlNode container, PageItem parentPage)
		{
			/*List<PageItem> clonedList =
				_pageList.Where(p => p.PageNameByCulture.ToLower() == parentPage.PageNameByCulture.ToLower()).ToList();
			List<HtmlNode> nodeList =
				container.Descendants().Where(
					n => n.Name == "a" && n.Attributes["class"] != null && n.Attributes["class"].Value == "index-title").ToList();
			if (!nodeList.Any())
				return;
			foreach (PageItem clonedItem in clonedList)
			{
				List<PageItem> childrenList = _pageList.Where(p => p.ParentPageId == clonedItem.PageId).ToList();
				int itemCount = childrenList.Count;
				if (nodeList.Count < itemCount)
					itemCount = nodeList.Count;
				for (int i = 0; i < itemCount; i++)
				{
					childrenList[i].PageNameByCulture = nodeList[i].InnerText.Trim();
				}
			}*/
			List<HtmlNode> nodeList =
				container.Descendants().Where(
					n => n.Name == "a" && n.Attributes["class"] != null && n.Attributes["class"].Value == "index-title").ToList();
			if (!nodeList.Any())
				return;

			List<PageItem> childrenList = _pageList.Where(p => p.ParentPageId == parentPage.PageId).ToList();

			int itemCount = childrenList.Count;
			if (nodeList.Count < itemCount)
				itemCount = nodeList.Count;
			for (int i = 0; i < itemCount; i++)
			{
				childrenList[i].PageNameByCulture = nodeList[i].InnerText.Trim();
			}
		}

		/// <summary>
		/// Determines whether [is node exist] [the specified URL].
		/// </summary>
		/// <param name="url">The URL.</param>
		/// <returns>
		///   <c>true</c> if [is node exist] [the specified URL]; otherwise, <c>false</c>.
		/// </returns>
		/// <author>ducuytran - 4/26/2013</author>
		private int IsNodeExist(string pagename, string pageUrl, int parentid)
		{
			int existCode = 0;
			List<PageItem> pageItems = _pageList.Where(p => p.PageNameByCulture.ToLower() == pagename.ToLower()).ToList();
			if (!pageItems.Any())
				return existCode;
			existCode = -1;
			//List<int> AncestorList = PageLineage(parentid);
			foreach (PageItem pageItem in pageItems)
			{
				if (pageItem.ParentPageId == parentid && pageItem.PageUrl == pageUrl)
				{
					existCode = 1;
					break;
				}
			}
			return existCode;
		}

		private List<int> PageLineage(int pageId)
		{
			PageItem crrNode = _pageList.FirstOrDefault(p => p.PageId == pageId);
			List<int> linage = new List<int>();
			linage.Add(pageId);
			PageItem pageNode = _pageList.FirstOrDefault(p => p.PageId == crrNode.ParentPageId);
			while (pageNode != null)
			{
				linage.Add(pageNode.PageId);
				pageNode = _pageList.FirstOrDefault(p => p.PageId == pageNode.ParentPageId);
			}
			if (linage.IndexOf(_rootCategoryId) > -1)
				linage.RemoveAt(linage.IndexOf(_rootCategoryId));
			PageItem solutionNode = _pageList.FirstOrDefault(p => p.ParentPageId == _rootCategoryId);
			if (solutionNode != null)
			{
				int SolutionId = solutionNode.PageId;
				linage.RemoveAt(linage.IndexOf(SolutionId));
			}


			return linage;
		}

		/// <summary>
		/// Inits the resource.
		/// </summary>
		/// Author: Vu Dinh
		/// 4/17/2013 - 8:09 PM
		private void InitResource()
		{
			//Get settings
			_connectionstring = ConfigurationManager.ConnectionStrings["db"].ConnectionString;
			_categoryContentTypeId = int.Parse(ConfigurationManager.AppSettings["categorycontenttypeid"]);
			_supplierId = int.Parse(ConfigurationManager.AppSettings["supplierid"]);
			_pagesContentTypeId = int.Parse(ConfigurationManager.AppSettings["pagescontenttypeid"]);
			_languageId = int.Parse(ConfigurationManager.AppSettings["enLangId"]);
			_rootCategoryId = int.Parse(ConfigurationManager.AppSettings["rootcategoryid"]);
			_themeId = int.Parse(ConfigurationManager.AppSettings["themeid"]);
			_imagesFolderUrl = ConfigurationManager.AppSettings["imageurl"];
			if (!_imagesFolderUrl.EndsWith("/"))
				_imagesFolderUrl += "/";
			_docFolderUrl = ConfigurationManager.AppSettings["docurl"];
			if (!_docFolderUrl.EndsWith("/"))
				_docFolderUrl += "/";
			_cspDataContext = new CspDataContext(_connectionstring);
			_cspUtils = new CspUtils(_cspDataContext);
			_wb = new WebClient();
			_wb.Headers.Add(HttpRequestHeader.Cookie, "com.ibm.wps.state.preprocessors.locale.LanguageCookie=en");
			_langCodeList = new List<string>(new[] {"en", "fr", "nl", "es", "de"});
			/*_firefox = new FirefoxDriver();
			_wait = new WebDriverWait(_firefox, TimeSpan.FromMinutes(10));*/
		}

		private void GoDownload()
		{
			if (!_fileList.Any())
				return;
			string localFolder = ConfigurationManager.AppSettings["downloadfolder"];

			if (!Directory.Exists(localFolder))
				Directory.CreateDirectory(localFolder);

			// allow async download to make it go faster


			ThreadPool.SetMaxThreads(10, 10);

			foreach (DownloadFile downloadFile in _fileList)
			{
				try
				{
					DownloadFile file = downloadFile;
					var resetEvent = new ManualResetEvent(false);
					ThreadPool.QueueUserWorkItem(_ =>
						{
							if (File.Exists(localFolder + downloadFile.FileName))
							{
								Log("[+] File: {1} has been downloaded. Path: {2} ", file.FileName, file.FileUrl, localFolder + downloadFile.FileName);
							}
							else
							{
								Log("[+] Downloading file: {0}{1}, saving to: {2} ", localFolder, file.FileName, file.FileUrl);
								WebClient wb = new WebClient();
								wb.DownloadFile(file.FileUrl, localFolder + file.FileName);
							}

							resetEvent.Set();
						});
					resetEvent.WaitOne();
				}
				catch (Exception)
				{
					continue;
				}
			}
		}

		/// <summary>
		/// Makes the name of the valid file.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <returns></returns>
		/// <author>ducuytran - 4/26/2013</author>
		private static string MakeValidFileName(string name)
		{
			name = name.Replace(" ", "_");
			string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
			string invalidReStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);
			return System.Text.RegularExpressions.Regex.Replace(name, invalidReStr, "_");
		}

		private static void Log(string text)
		{
			Console.WriteLine(text);
			log.Info(text);
		}
		private static void Log(string text, params object[] values)
		{
			Console.WriteLine(text, values);
			log.InfoFormat(text, values);
		}
	}

	public class DownloadFile
	{
		public string FileName { get; set; }
		public string FileUrl { get; set; }
	}
}