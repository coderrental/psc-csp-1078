﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using CR.Tools.Crawler.Entities;
using CR.Tools.Crawler.Helpers;
using CR.Tools.Crawler.Providers;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;
using CR.DnnModules.Common;
using CR.ContentObjectLibrary;
using CR.ContentObjectLibrary.Data;
using System.Configuration;


namespace CR.Tools.Crawler.Website
{


    class Mitel : ICRCrawlers
    {

        private FirefoxDriver _firefox;
        private WebDriverWait _wait;
        private CspDataContext _cspDataContext;
        private CspUtils _cspUtils;

        private int _categoryContentTypeId, _pagesContentTypeId, _languageId, _supplierId, _rootCategoryId, _themeId;
        private string _connectionstring;
        /// <summary>
        /// Saves to XML.
        /// </summary>
        /// Author: Vu Dinh
        /// 4/12/2013 - 11:13 AM
        public void SaveToXml()
        {
            InitResource();
            try
            {
                PrintMessage("Opening website...");
                _firefox.Navigate().GoToUrl("http://mitel.com");

                var menus = _wait.Until((browser) => browser.FindElements(By.CssSelector(".menu_black li")).Where(a => a.FindElement(By.TagName("a")).Text == "SOLUTIONS").ToList());

                PrintMessage("Get data...");
                var listEnglishPage = GetPageItems(menus, ".dropdown_3columns div ul h3 a", _rootCategoryId);

                //PrintMessage("Save content to database");
                //SaveContent(listEnglishPage);

                //get france language
                PrintMessage("Update content to French");
                _firefox.ExecuteScript("changeLanguage('fr')");
                UpdateContentFromOtherLanguage(listEnglishPage, int.Parse(ConfigurationManager.AppSettings["frLangId"]));

                PrintMessage("Update content to Netherland");
                _firefox.ExecuteScript("changeLanguage('nl')");
                UpdateContentFromOtherLanguage(listEnglishPage, int.Parse(ConfigurationManager.AppSettings["nlLangId"]));

                PrintMessage("Update content to Espanol");
                _firefox.ExecuteScript("changeLanguage('es')");
                UpdateContentFromOtherLanguage(listEnglishPage, int.Parse(ConfigurationManager.AppSettings["esLangId"]));

                PrintMessage("Update content to Deutsch");
                _firefox.ExecuteScript("changeLanguage('de')");
                UpdateContentFromOtherLanguage(listEnglishPage, int.Parse(ConfigurationManager.AppSettings["deLangId"]));

                PrintMessage("Done!");
                Console.ReadLine();
                _firefox.Close();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.ReadLine();
                _firefox.Close();
            }
        }

        /// <summary>
        /// Inits the resource.
        /// </summary>
        /// Author: Vu Dinh
        /// 4/17/2013 - 8:09 PM
        private void InitResource()
        {
            //Get settings
            _connectionstring = ConfigurationManager.ConnectionStrings["db"].ConnectionString;
            _categoryContentTypeId = int.Parse(ConfigurationManager.AppSettings["categorycontenttypeid"]);
            _supplierId = int.Parse(ConfigurationManager.AppSettings["supplierid"]);
            _pagesContentTypeId = int.Parse(ConfigurationManager.AppSettings["pagescontenttypeid"]);
            _languageId = int.Parse(ConfigurationManager.AppSettings["enLangId"]);
            _rootCategoryId = int.Parse(ConfigurationManager.AppSettings["rootcategoryid"]);
            _themeId = int.Parse(ConfigurationManager.AppSettings["themeid"]);


            _cspDataContext = new CspDataContext(_connectionstring);
            _cspUtils = new CspUtils(_cspDataContext);
            _firefox = new FirefoxDriver();
            _wait = new WebDriverWait(_firefox, TimeSpan.FromMinutes(10));
        }

        /// <summary>
        /// Prints the message.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        /// Author: Vu Dinh
        /// 4/16/2013 - 7:22 PM
        private void PrintMessage(string msg)
        {
            Console.WriteLine(msg);
        }

        /// <summary>
        /// Updates the content from other language.
        /// </summary>
        /// Author: Vu Dinh
        /// 4/23/2013 - 3:51 PM
        private void UpdateContentFromOtherLanguage (IEnumerable<PageItem> listEnPages, int langId)
        {
            foreach (var page in listEnPages)
            {
                var pageItem = new PageItem();
                _firefox.Navigate().GoToUrl(page.PageUrl);
                var content = _wait.Until((browser) => browser.FindElement(By.ClassName("layoutColumn")));
                var title = _wait.Until((browser) => browser.FindElement(By.CssSelector(".breadcrumbs li.active")));
                pageItem.PageContent = ParseContent(content);
                pageItem.PageName = title.Text;
                PrintMessage(string.Format("Update content: {0}",page.PageName));
                //update category content
                var englishCategoryContent = _cspUtils.GetContentsFromCategoryId(_languageId, _categoryContentTypeId, _supplierId, page.PageId).FirstOrDefault();
                if (englishCategoryContent != null)
                {
                    var categorycontent =_cspUtils.GetContentsFromCategoryId(langId, _categoryContentTypeId, _supplierId, page.PageId).FirstOrDefault();
                    if (categorycontent == null)
                    {
                        //Create new content
                       categorycontent = new content
                        {
                            active = true,
                            content_main_Id = englishCategoryContent.content_main_Id,
                            stage_Id = 50,
                            content_types_languages_Id = langId,
                            content_Id = Guid.NewGuid()
                        };
                        _cspDataContext.contents.InsertOnSubmit(categorycontent);
                        _cspDataContext.SubmitChanges();

                        //Update category content
                        _cspUtils.SetContentFieldValue(_categoryContentTypeId, categorycontent, "Content_Title", ToTitleCase(pageItem.PageName.ToLower()));
                        var order = page.PageOrder;
                        _cspUtils.SetContentFieldValue(_categoryContentTypeId, categorycontent, "Status_Sort_Order", order.ToString("0000"));
                    }
                    
                }

                //update page content
                var englishPageContent = _cspUtils.GetContentsFromCategoryId(_languageId, _pagesContentTypeId, _supplierId, page.PageId).FirstOrDefault();
                if (englishPageContent != null)
                {
                    var pagecontent = _cspUtils.GetContentsFromCategoryId(langId, _pagesContentTypeId, _supplierId, page.PageId).FirstOrDefault();
                    if (pagecontent == null)
                    {
                        //Create new content
                        pagecontent = new content
                        {
                            active = true,
                            content_main_Id = englishPageContent.content_main_Id,
                            stage_Id = 50,
                            content_types_languages_Id = langId,
                            content_Id = Guid.NewGuid()
                        };
                        _cspDataContext.contents.InsertOnSubmit(pagecontent);
                        _cspDataContext.SubmitChanges();

                        _cspUtils.SetContentFieldValue(_pagesContentTypeId, pagecontent, "Content_Page", pageItem.PageContent);
                    }
                   
                }

                if (page.ChildPage != null && page.ChildPage.Any())
                {
                    UpdateContentFromOtherLanguage(page.ChildPage, langId);
                }
            }
        }

        /// <summary>
        /// Saves the content.
        /// </summary>
        /// <param name="listPage">The list page.</param>
        /// Author: Vu Dinh
        /// 4/17/2013 - 8:13 PM
        private void SaveContent(IEnumerable<PageItem> listPage )
        {
            foreach (var pageItem in listPage)
            {
                var cat = _cspDataContext.categories.SingleOrDefault(a => a.categoryText.ToLower() == pageItem.PageName.ToLower());
                if (cat == null)
                    continue;
                
                //create or update Category content
                UpdateCategoryContent(pageItem, cat);

                //Create or update Page content
                UpdatePageContent(pageItem, cat);

                if (pageItem.ChildPage != null && pageItem.ChildPage.Any())
                    SaveContent(pageItem.ChildPage);
            }
        }

        /// <summary>
        /// Updates the content of the page.
        /// </summary>
        /// <param name="pageItem">The page item.</param>
        /// <param name="category">The category.</param>
        /// Author: Vu Dinh
        /// 4/17/2013 - 8:48 PM
        private void UpdatePageContent(PageItem pageItem, category category)
        {
            var content = _cspUtils.GetContentsFromCategoryId(_languageId, _pagesContentTypeId, _supplierId,pageItem.PageId).FirstOrDefault();
            if (content == null) //create new content
            {
                content = CreateContent(pageItem, _pagesContentTypeId);
            }

            //update page content
            _cspUtils.SetContentFieldValue(_pagesContentTypeId, content, "Content_Page", pageItem.PageContent);
        }

        /// <summary>
        /// Updates the content of the category.
        /// </summary>
        /// <param name="pageItem">The page item.</param>
        /// <param name="category">The category.</param>
        /// Author: Vu Dinh
        /// 4/17/2013 - 8:47 PM
        private void UpdateCategoryContent(PageItem pageItem, category category)
        {
            var content = _cspUtils.GetContentsFromCategoryId(_languageId, _categoryContentTypeId,_supplierId, pageItem.PageId).FirstOrDefault();
            if (content == null) //create new content
            {
                content = CreateContent(pageItem, _categoryContentTypeId);
            }
            
            //Update category content
             _cspUtils.SetContentFieldValue(_categoryContentTypeId, content, "Content_Title",ToTitleCase(pageItem.PageName.ToLower()));
            var order = int.Parse(category.DisplayOrder.ToString());
            _cspUtils.SetContentFieldValue(_categoryContentTypeId, content,"Status_Sort_Order", order.ToString("0000"));
        }

        /// <summary>
        /// Creates the content.
        /// </summary>
        /// <param name="pageItem">The page item.</param>
        /// <param name="contentTypeId">The content type id.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 4/17/2013 - 9:08 PM
        private content CreateContent(PageItem pageItem, int contentTypeId)
        {
            //Create new content main
            var contentMain = new content_main
            {
                content_main_Id = Guid.NewGuid(),
                content_main_key = pageItem.PageName.Replace(" ", string.Empty),
                content_identifier = pageItem.PageName,
                content_types_Id = contentTypeId,
                supplier_Id = _supplierId,
                consumer_Id = 0,
                content_staging_scheme_Id = 1
            };
            _cspDataContext.content_mains.InsertOnSubmit(contentMain);
            _cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);

            //Create new content
            var content = new content
            {
                active = true,
                content_main_Id = contentMain.content_main_Id,
                stage_Id = 50,
                content_types_languages_Id = _languageId,
                content_Id = Guid.NewGuid()
            };
            _cspDataContext.contents.InsertOnSubmit(content);
            _cspDataContext.SubmitChanges();

            //Assign to category
            UpdateCategories(pageItem.PageId, content);
            return content;
        }

        /// <summary>
        /// Updates the categories.
        /// </summary>
        /// <param name="catId">The cat id.</param>
        /// <param name="content">The content.</param>
        /// Author: Vu Dinh
        /// 4/17/2013 - 11:30 PM
        protected void UpdateCategories(int catId, content content)
        {
            var catContent = _cspDataContext.content_categories.Where(a => a.content_main_Id == content.content_main_Id).ToList();
            if (catContent.Count == 0) //Create new content category
            {
                _cspDataContext.content_categories.InsertOnSubmit(new content_category
                {
                    category_Id = catId,
                    content_main_Id = content.content_main_Id,
                    publication_scheme_Id = 11
                });
                _cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
            }
            else
            {
                _cspDataContext.content_categories.DeleteAllOnSubmit(catContent);
                _cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                _cspDataContext.content_categories.InsertOnSubmit(new content_category
                {
                    category_Id = catId,
                    content_main_Id = content.content_main_Id,
                    publication_scheme_Id = 11
                });
                _cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
            }
        }



        /// <summary>
        /// Gets the page items.
        /// </summary>
        /// <param name="elements">The elements.</param>
        /// <param name="cssSelector">The CSS selector.</param>
        /// <param name="parentCategoryId">The parent category id.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 4/17/2013 - 8:34 PM
        private List<PageItem> GetPageItems (IEnumerable<IWebElement> elements, string cssSelector, int parentCategoryId)
        {
            var listPage = new List<PageItem>();
            foreach (var element in elements)
            {
                
                var items = element.FindElements(By.CssSelector(cssSelector));
                foreach (var item in items)
                {
                    if (!listPage.Any(a => a.PageUrl == item.GetAttribute("href")))
                    {
                        listPage.Add(new PageItem
                                         {
                                             ParentPageId = parentCategoryId,
                                             PageName = item.Text,
                                             PageUrl = item.GetAttribute("href")
                                         });
                    }
                }
            }
            foreach (var pageItem in listPage)
            {
                pageItem.PageOrder = listPage.IndexOf(pageItem);

                //create categories if doest not exist
                var cat = _cspDataContext.categories.SingleOrDefault(a => a.categoryText.ToLower() == pageItem.PageName.ToLower());
                if (cat == null)
                {
                    cat = CreateCategory(ToTitleCase(pageItem.PageName.ToLower()), parentCategoryId, pageItem.PageOrder);
                }
                else
                {
                    cat.parentId = pageItem.ParentPageId;
                    _cspDataContext.Refresh(RefreshMode.KeepChanges, cat);
                    _cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                }
                pageItem.PageId = cat.categoryId;


                
                PrintMessage(string.Format("Get content of page: {0} ({1}) ...", pageItem.PageName, pageItem.PageUrl));
                _firefox.Navigate().GoToUrl(pageItem.PageUrl);
                var content = _wait.Until((browser) => browser.FindElement(By.ClassName("layoutColumn")));
                pageItem.PageContent = ParseContent(content);
                pageItem.ChildPage = GetChildPage(content, pageItem.PageId);
            }
            return listPage;
        }

        /// <summary>
        /// Creates the category.
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="parentCategoryId">The parent category id.</param>
        /// <param name="order">The order.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 4/17/2013 - 8:43 PM
        private category CreateCategory(string categoryName, int parentCategoryId, int order)
        {
            var parentCat = _cspDataContext.categories.Single(a => a.categoryId == parentCategoryId);
            bool isSubscribable = (parentCat.depth < 2);
            //create new category
            var cat = new category
                 {
                     IsSubscribable = isSubscribable,
                     active = true,
                     categoryText = categoryName,
                     description = categoryName,
                     parentId = parentCategoryId,
                     apply_to_childs = false,
                     publication_schemeID = 11,
                     dateAdded = DateTime.Now,
                     DisplayOrder = order,
                     ExternalCategoryCode = categoryName
                 };
            _cspDataContext.categories.InsertOnSubmit(cat);
            _cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);

            // Add to theme categories
            _cspDataContext.themes_categories.InsertOnSubmit(new themes_category
                                                                 {
                                                                     category_Id = cat.categoryId,
                                                                     themes_Id = _themeId
                                                                 });
            _cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
            return cat;
        }

        /// <summary>
        /// Toes the title case.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 4/17/2013 - 8:38 PM
        private string ToTitleCase( string text)
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;

            return textInfo.ToTitleCase(text);
        }


        /// <summary>
        /// Parses the content.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 4/16/2013 - 5:02 PM
        private string ParseContent(IWebElement element)
        {
            var content = element.GetAttribute("innerHTML");
            //add missing outer html tag
            content = "<table class=\"layoutColumn\" cellspacing=\"0\" cellpadding=\"0\">" + content + "</table>";
            content = content.Replace("class=\"feature columns\"", "class=\"feature columns\" id=\"page-banner\"");
            content = content.Replace("class=\"feature-info column\"","class=\"feature-info column\" id=\"banner-main-text\"");

            //remove child categories section
            try
            {
                var childSection = element.FindElement(By.ClassName("section-media")).GetAttribute("innerHTML");
                content = content.Replace(childSection, string.Empty);
            }
            catch
            {
                //Ignore if this page have no sub topic
            }

            try
            {
                var subTopic = element.FindElement(By.ClassName("section-sub-topic")).GetAttribute("innerHTML");
                content = content.Replace(subTopic, string.Empty);
            }
            catch
            {
                //Ignore if this page have no sub topic
            }
            

            //remove learn more section
            try
            {
                var learnmoreSection = element.FindElement(By.ClassName("learn-more-links")).GetAttribute("innerHTML");
                content = content.Replace(learnmoreSection, string.Empty);
            }
            catch
            {
                //Ignore if this page have no learn more links
            }
           
            return content;
        }

        /// <summary>
        /// Gets the child page.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="parentCategoryId">The parent category id.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 4/16/2013 - 7:22 PM
        private List<PageItem> GetChildPage(IWebElement element, int parentCategoryId)
        {
            var listChildElem = element.FindElements(By.CssSelector(".section-sub-topic ul li"));
            if (listChildElem.Any())
                return GetPageItems(listChildElem, "a", parentCategoryId);
            return null;
        }
    }
}
