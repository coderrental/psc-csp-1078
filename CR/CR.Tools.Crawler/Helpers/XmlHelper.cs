﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace CR.Tools.Crawler.Helpers
{
    class XmlHelper
    {
        private readonly string _savePath;
        private readonly string _fileName;

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlHelper"/> class.
        /// </summary>
        /// <param name="savePath">The save path.</param>
        /// <param name="fileName">Name of the file.</param>
        /// Author: Vu Dinh
        /// 4/10/2013 - 5:09 PM
        public XmlHelper(string savePath, string fileName)
        {
            _savePath = savePath;
            _fileName = fileName;
        }


        /// <summary>
        /// Creates the XML file.
        /// </summary>
        /// <param name="listPageItem">The list page item.</param>
        /// Author: Vu Dinh
        /// 4/10/2013 - 6:16 PM
        public void CreateXmlFile(List<Entities.PageItem> listPageItem)
        {
            var wSettings = new XmlWriterSettings { Indent = true };
            var ms = new MemoryStream();
            XmlWriter xw = XmlWriter.Create(ms, wSettings);// Write Declaration
            xw.WriteStartDocument();

            // Write the root node
            xw.WriteStartElement("pages");

          
            #region [ Write data nodes ]
            foreach (var page in listPageItem)
            {
                //write note name
                xw.WriteStartElement("page");

                //write note attributes
                xw.WriteStartAttribute("name");
                xw.WriteString(page.PageName);
                xw.WriteEndAttribute();

                //write note content
                xw.WriteStartElement("content");
                xw.WriteString(page.PageContent);
                xw.WriteEndElement();

                //write end node
                xw.WriteEndElement();
            }
            #endregion

            //write end root node
            xw.WriteEndElement();
            // write end document
            xw.WriteEndDocument();

            xw.Flush();
            var fileFullPath = Path.Combine(_savePath, _fileName);
            if (File.Exists(fileFullPath))
            {
                File.Delete(fileFullPath);
            }
            var file = new FileStream(fileFullPath, FileMode.CreateNew, FileAccess.ReadWrite);
            ms.WriteTo(file);
            ms.Close();
            ms.Dispose();
            file.Close();
            file.Dispose();
        }

    }
}
