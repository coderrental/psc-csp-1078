﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CR.Tools.Crawler.Helpers;
using CR.Tools.Crawler.Providers;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using CR.Tools.Crawler.Entities;

namespace CR.Tools.Crawler
{
    class Program
    {
        /// <summary>
        /// Mains the specified args.
        /// </summary>
        /// <param name="args">The args.</param>
        /// Author: Vu Dinh
        /// 4/10/2013 - 6:37 PM
        static void Main(string[] args)
        {
            CrawlIt("Mitelv1");
        }

        /// <summary>
        /// Craws it.
        /// </summary>
        /// <param name="websiteName">Name of the website.</param>
        /// Author: Vu Dinh
        /// 4/10/2013 - 6:37 PM
        static void CrawlIt(string websiteName)
        {
            Type type = Type.GetType("CR.Tools.Crawler.Website." + websiteName);
            if (type == null)
                return;
            var crawlerItem = Activator.CreateInstance(type) as ICRCrawlers;
            if (crawlerItem != null)
            {
                crawlerItem.SaveToXml();
            }
        }
    }
}
