﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CR.Tools.Crawler.Entities
{
    class PageItem
    {
        /// <summary>
        /// Gets or sets the page id.
        /// </summary>
        /// <value>
        /// The page id.
        /// </value>
        /// Author: Vu Dinh
        /// 4/17/2013 - 8:29 PM
        public int PageId { get; set; }

        /// <summary>
        /// Gets or sets the parent page id.
        /// </summary>
        /// <value>
        /// The parent page id.
        /// </value>
        /// Author: Vu Dinh
        /// 4/17/2013 - 8:29 PM
        public int ParentPageId { get; set; }

        /// <summary>
        /// Gets or sets the name of the page.
        /// </summary>
        /// <value>
        /// The name of the page.
        /// </value>
        /// Author: Vu Dinh
        /// 4/10/2013 - 6:11 PM
        public string PageName { get; set; }

		/// <summary>
		/// Gets or sets the page name by culture.
		/// </summary>
		/// <value>
		/// The page name by culture.
		/// </value>
		/// <author>ducuytran</author>
		/// <datetime>4/26/2013</datetime>
		public string PageNameByCulture { get; set; }

        /// <summary>
        /// Gets or sets the page URL.
        /// </summary>
        /// <value>
        /// The page URL.
        /// </value>
        /// Author: Vu Dinh
        /// 4/10/2013 - 6:11 PM
        public string PageUrl { get; set; }

        /// <summary>
        /// Gets or sets the content of the page.
        /// </summary>
        /// <value>
        /// The content of the page.
        /// </value>
        /// Author: Vu Dinh
        /// 4/10/2013 - 6:11 PM
        public string PageContent { get; set; }

        /// <summary>
        /// Gets or sets the page order.
        /// </summary>
        /// <value>
        /// The page order.
        /// </value>
        /// Author: Vu Dinh
        /// 4/16/2013 - 4:51 PM
        public int PageOrder { get; set; }

        /// <summary>
        /// Gets or sets the child page.
        /// </summary>
        /// <value>
        /// The child page.
        /// </value>
        /// Author: Vu Dinh
        /// 4/16/2013 - 4:49 PM
        public List<PageItem> ChildPage { get; set; }
    }
}
