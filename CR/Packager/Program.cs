﻿using System;
using System.IO;
using System.Linq;
using System.Xml;
using ICSharpCode.SharpZipLib.Zip;

namespace TIEKinetix.CspModule.Packager
{
    internal class Program
    {
    	private static string _prjName;
        private static void Main(string[] args)
        {
            if (args.Length < 4) PrintUsage();
            if (args.Length >= 4)
            {
            	_prjName = args[3];
                string sSourceDirectory = args[0];
                string sManifestFileName = args[1];
                string sManifestFilePathFull = sSourceDirectory + "\\" + sManifestFileName;
                string sTargetPath = args[2];
                string sPackageName = "";
                var xManifest = new XmlDocument();
                xManifest.Load(sManifestFilePathFull);
                
                if (xManifest.DocumentElement != null)
                {
                    string sManifestRootNode = xManifest.DocumentElement.Name;
                    if (sManifestRootNode == "dotnetnuke")
                    {
                        XmlNodeList xPackages = xManifest.SelectNodes("//packages/package[@type='Module']");
                        if (xPackages.Count > 0)
                        {
                            var xFirstPackageNode = (XmlElement)xPackages[0];
                            string sPkg1Name = xFirstPackageNode.GetAttribute("name");
                            string sPkg1Version = xFirstPackageNode.GetAttribute("version");
                            if (sPkg1Name != "" && sPkg1Version != "")
                            {
                                // update the version

                                int[] versions = sPkg1Version.Split('.').Select(a => int.Parse(a)).ToArray();
                                int n = versions.Length - 1;
                                versions[n]++;
                                while (versions[n]>=100 && n>=1)
                                {
                                    versions[n] = 0;
                                    versions[n - 1]++;
                                    n--;
                                }
                                
                                sPkg1Version = "";
                                for (int i = 0; i < versions.Length-1; i++)
                                {
                                    sPkg1Version += versions[i] + ".";
                                }
                                sPkg1Version += versions[versions.Length - 1];

                                // update the package dnn file
                                xFirstPackageNode.SetAttribute("version", sPkg1Version);
                                xManifest.Save(sManifestFilePathFull);

                                sPackageName = sPkg1Name + "_" + sPkg1Version;
                            }
                            else 
                                ExitWith(2, "Cant find name and version");

                            //string sWorkingDirectory = Directory.GetCurrentDirectory() + "\\Temp";
                            Console.WriteLine(System.Reflection.Assembly.GetExecutingAssembly().Location);
                            string sWorkingDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Temp";

                            if (Directory.Exists(sWorkingDirectory)) Directory.Delete(sWorkingDirectory, true);
                            DirectoryInfo di = Directory.CreateDirectory(sWorkingDirectory);
                            foreach (XmlElement xPackage in xPackages)
                            {
                                CopyFiles(sSourceDirectory, di.FullName, xPackage);
                            }
                            CopyFile(sSourceDirectory, di.FullName, sManifestFileName);
                            if (!string.IsNullOrEmpty(sTargetPath) && Directory.Exists(sTargetPath))
                                BuildCompressedPackage(di.FullName, sTargetPath, sPackageName + ".zip");
                            else
                                BuildCompressedPackage(di.FullName, Directory.GetCurrentDirectory(), sPackageName + ".zip");

                        }
                        else ExitWith(1, "Unable to find any module package declarations");
                    }
                    else ExitWith(1, "Not a DNN package type");
                }
            }
        }

        private static void CopyFiles(string sSourceDir, string sTargetDir, XmlNode xPackage)
        {
            var xLicense = (XmlElement)xPackage.SelectSingleNode("license");
            var xReleaseNotes = (XmlElement)xPackage.SelectSingleNode("releaseNotes");
            if (xLicense != null)
            {
                if (xLicense.HasAttribute("src")) CopyFile(sSourceDir, sTargetDir, xLicense.GetAttribute("src"));
            }
            if (xReleaseNotes != null)
            {
                if (xReleaseNotes.HasAttribute("src")) CopyFile(sSourceDir, sTargetDir, xReleaseNotes.GetAttribute("src"));
            }

            XmlNode xScriptComponent = xPackage.SelectSingleNode("components/component[@type='Script']");
            XmlNode xModuleComponent = xPackage.SelectSingleNode("components/component[@type='Module']");
            XmlNode xAssemblyComponent = xPackage.SelectSingleNode("components/component[@type='Assembly']");
            XmlNode xFileComponent = xPackage.SelectSingleNode("components/component[@type='File']");
            if (xScriptComponent != null) CopyFilesFromScriptComponents(sSourceDir, sTargetDir, xScriptComponent);
            if (xModuleComponent != null) CopyFilesFromModuleComponents(sSourceDir, sTargetDir, xModuleComponent);
			if (xAssemblyComponent != null) CopyFilesFromAssemblyComponents(sSourceDir, sTargetDir, xAssemblyComponent);
			if (xFileComponent != null) CopyFilesFromFileComponents(sSourceDir, sTargetDir, xFileComponent);
        }
        private static void CopyFilesFromFileComponents(string sSourceDir, string sTargetDir, XmlNode xFileComponent)
        {
			Console.WriteLine("Start copying File Components:");
            string sbasePath = "";
            var xBasePath = (XmlElement)xFileComponent.SelectSingleNode("files/basePath");
            if (xBasePath != null) sbasePath = xBasePath.InnerText.Trim();
            if (sSourceDir.EndsWith(sbasePath) || sSourceDir.EndsWith(sbasePath+"\\")) sbasePath = "";
        	sbasePath = "";
            XmlNodeList xFiles = xFileComponent.SelectNodes("files/file");
            foreach (XmlNode xFile in xFiles)
            {
                XmlNode xPath = xFile.SelectSingleNode("path");
                XmlNode xName = xFile.SelectSingleNode("name");
                string sFileName = xName.InnerText.Trim();
                string sSourceDir2 = sSourceDir + "\\" + sbasePath + "\\";
                string sTargetDir2 = sTargetDir + "\\" + sbasePath + "\\";

                if (xPath != null && xPath.InnerText != "")
                {
                    sSourceDir2 += xPath.InnerText.Trim();
                    sTargetDir2 += xPath.InnerText.Trim();
                }
                CopyFile(sSourceDir2, sTargetDir2, sFileName);
            }
        }
        private static void CopyFilesFromAssemblyComponents(string sSourceDir, string sTargetDir, XmlNode xAssemblyComponent)
        {
			Console.WriteLine("Start copying Assembly Components:");
            string sbasePath = "";
            var xBasePath = (XmlElement)xAssemblyComponent.SelectSingleNode("assemblies/basePath");
            if (xBasePath != null) sbasePath = xBasePath.InnerText.Trim();
            if (sSourceDir.EndsWith(sbasePath) || sSourceDir.EndsWith(sbasePath + "\\")) sbasePath = "";
        	sbasePath += "\\";
            XmlNodeList xAssemblies = xAssemblyComponent.SelectNodes("assemblies/assembly");
        	try
        	{
				foreach (XmlNode xAssembly in xAssemblies)
				{
					XmlNode xName = xAssembly.SelectSingleNode("name");
					string sFileName = xName.InnerText.Trim();
					string sSourceDir2 = sSourceDir + "\\" + sbasePath;
					string sTargetDir2 = sTargetDir + "\\" + sbasePath + "\\";
					CopyFile(sSourceDir2, sTargetDir2, sFileName);
					//XmlNode xName = xAssembly.SelectSingleNode("name");
					//string sFileName = xName.InnerText.Trim();
					//string sReplPath = "";
					//if (sSourceDir.ToLower().IndexOf("\\desktopmodules") > 0)
					//{
					//    sReplPath = sSourceDir.Substring(sSourceDir.ToLower().IndexOf("\\desktopmodules"));
					//}
					//string sSourceDir2 = sSourceDir.Replace(sReplPath, "") + "\\" + sbasePath;
					//string sTargetDir2 = sTargetDir + "\\" + sbasePath + "\\";

					//CopyFile(sSourceDir2, sTargetDir2, sFileName);
				}
        	}
        	catch (Exception)
        	{
				ExitWith(2, "Failed in copying Assembly Components");
        	}
            
        }
        private static void CopyFilesFromScriptComponents(string sSourceDir, string sTargetDir, XmlNode xScriptComponent)
        {
			Console.WriteLine("Start copying Script Components:");
            string sbasePath = "";
            var xBasePath = (XmlElement)xScriptComponent.SelectSingleNode("scripts/basePath");
            if (xBasePath != null) sbasePath = xBasePath.InnerText.Trim();
            if (sSourceDir.EndsWith(sbasePath) || sSourceDir.EndsWith(sbasePath + "\\")) sbasePath = "";
            else
            {
                string k = "DesktopModules";
                int i = sSourceDir.IndexOf(k);
                int j = sbasePath.IndexOf(k);
                if (i > 0)
                {
                    while (i < sSourceDir.Length && j <sbasePath.Length)
                    {
                        if (sSourceDir[i] == sbasePath[j])
                        {
                            i++;
                            j++;
                        }
                    }
                    sbasePath = sbasePath.Substring(j);
                    if (sbasePath.StartsWith("\\")) sbasePath = sbasePath.Substring(1);
                    if (sbasePath.EndsWith("\\")) sbasePath = sbasePath.Remove(sbasePath.Length - 1);
                }
            }

			//Added by @ducuytran
        	sbasePath = "";

            XmlNodeList xInstallScripts = xScriptComponent.SelectNodes("scripts/script[@type='Install' or @type='UnInstall']");
            if (xInstallScripts != null)
                foreach (XmlNode xScript in xInstallScripts)
                {
                    XmlNode xPath = xScript.SelectSingleNode("path");
                    XmlNode xName = xScript.SelectSingleNode("name");
                    string sFileName = xName.InnerText.Trim();
                    string sSourceDir2 = sSourceDir + "\\" + sbasePath + "\\";
                    string sTargetDir2 = sTargetDir + "\\" + sbasePath + "\\";
                    if (xPath != null && xPath.InnerText != "")
                    {
                        sSourceDir2 += xPath.InnerText.Trim();
                        sTargetDir2 += xPath.InnerText.Trim();
                    }
                    CopyFile(sSourceDir2, sTargetDir2, sFileName);
                }
        }
        private static void CopyFilesFromModuleComponents(string sSourceDir, string sTargetDir, XmlNode xModuleComponent)
        {
			Console.WriteLine("Start copying Module Components:");
            XmlNodeList xControlSrcs = xModuleComponent.SelectNodes("desktopModule/moduleDefinitions/moduleDefinition/moduleControls/moduleControl/controlSrc");
            foreach (XmlNode xControlSrc in xControlSrcs)
            {
				string sControlPath = (xControlSrc).InnerText.Trim();
				//Console.WriteLine("In CopyFilesFromModuleComponents(): " + sControlPath);
				string[] sSeparator = new string[1] { _prjName };
				string[] sFolders = sControlPath.Split(sSeparator, StringSplitOptions.RemoveEmptyEntries);
				string controlFile = sFolders.Last().Replace("/", "");
            	try
            	{
					CopyFile(sSourceDir, sTargetDir, controlFile);
            	}
            	catch (Exception)
            	{
            		ExitWith(3, "Failed in copying Module Components");
            	}
				
            	//string sControlPath = (xControlSrc).InnerText.Trim();
            	//string sReplPath = "";
            	//if (sSourceDir.ToLower().IndexOf("\\desktopmodules") > 0)
            	//{
            	//    sReplPath = sSourceDir.Substring(sSourceDir.ToLower().IndexOf("\\desktopmodules"));
            	//}
            	//sControlPath = sControlPath.Replace("/", "\\");
            	////string sTargetDir2 = sTargetDir + sReplPath;
            	//if (sReplPath.StartsWith("\\")) sReplPath = sReplPath.Substring(1);
            	//if (sReplPath.EndsWith("\\")) sReplPath = sReplPath.Remove(sReplPath.Length - 1);
            	//sControlPath = sControlPath.Replace(sReplPath + "\\", "");
            	//CopyFile(sSourceDir, sTargetDir, sControlPath);
            }
        }
        private static void CopyFile(string sSourceDir, string sTargetDir, string sFileName)
        {
            try
            {
                string sSourcePath = sSourceDir + "\\" + sFileName;
                string sTargetPath = sTargetDir + "\\" + sFileName;
                sSourcePath = sSourcePath.Replace("\\\\", "\\");
                sTargetPath = sTargetPath.Replace("\\\\", "\\");
                sSourcePath = sSourcePath.Replace("\\\\", "\\");
                sTargetPath = sTargetPath.Replace("\\\\", "\\");
				if (!Directory.Exists(sTargetPath.Replace("\\" + sFileName, ""))) Directory.CreateDirectory(sTargetPath.Replace("\\" + sFileName, ""));
				if(File.Exists(sSourcePath))
					File.Copy(sSourcePath, sTargetPath, true);
				else
				{
					Console.WriteLine("Could not find File " + sSourcePath);
				}
            }
            catch (Exception exp)
            {
                Console.WriteLine("Could not find File " + exp.Message);
				return;
            }
        }

        private static void BuildCompressedPackage(string SourceDirectory, string PackagePath, string PackageName)
        {
            try
            {
                using (var s = new ZipOutputStream(File.Create(PackagePath + "\\" + PackageName + "")))
                {
                    s.SetLevel(9); // 0-9, 9 being the highest compression
                    CompressDir(SourceDirectory, SourceDirectory, s);
                    s.Finish();
                    s.Close();
                }
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp);
            }
        }

        private static void CompressDir(string ParentPath, string SourceDirectory, ZipOutputStream s)
        {
            string[] filenames = Directory.GetFiles(SourceDirectory);
            var buffer = new byte[4096];
            foreach (string file in filenames)
            {
                string sFilePath = SourceDirectory.Replace(ParentPath, "");
                if (sFilePath.Trim() != "")
                {
                    if (sFilePath.StartsWith("\\")) sFilePath = sFilePath.Substring(1);
                    if (!sFilePath.EndsWith("\\")) sFilePath += "\\";
                }
                var entry = new ZipEntry(sFilePath + Path.GetFileName(file));
                entry.DateTime = DateTime.Now;
                s.PutNextEntry(entry);
                using (FileStream fs = File.OpenRead(file))
                {
                    int sourceBytes;
                    do
                    {
                        sourceBytes = fs.Read(buffer, 0, buffer.Length);
                        s.Write(buffer, 0, sourceBytes);
                    } while (sourceBytes > 0);
                }
            }
            string[] directories = Directory.GetDirectories(SourceDirectory);
            foreach (string directroy in directories)
            {
                CompressDir(ParentPath, directroy, s);
            }
        }

        private static void PrintUsage()
        {
            Console.WriteLine("\t Packager Usage: ");
            Console.WriteLine("\t\t Packager.exe <source-dir> <manifest-file> <target-path> <projectname>");
        }
        private static void ExitWith(int errorCode, string errorMessage)
        {
            Console.WriteLine("Error Encountered");
            Console.WriteLine(errorMessage);
            Environment.Exit(errorCode);
        }
    }
}