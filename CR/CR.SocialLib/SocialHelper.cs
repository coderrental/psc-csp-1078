﻿// ***********************************************************************
// Assembly         : CR.SocialLib
// Author           : Vu Dinh
// Created          : 01-15-2014
//
// Last Modified By : Vu Dinh
// Last Modified On : 02-11-2014
// ***********************************************************************
// <copyright file="SocialHelper.cs" company="Coder Rental Team">
//     Copyright (c) Coder Rental Team. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text.RegularExpressions;
using CR.SocialLib.OAuth;
using CR.SocialLib.OAuth.Entities;

namespace CR.SocialLib
{
    public class SocialHelper
    {
        private readonly OAuthContext _oAuthContext;
        private readonly OAuthClientBase _oAuthClient;
        private readonly SocialLib.Common.Socialtype _socialtype;
        private readonly string _callBackUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="SocialHelper" /> class.
        /// </summary>
        /// <param name="socialType">Type of the social.</param>
        /// <param name="consumerKey">The consumer key.</param>
        /// <param name="consumerSecret">The consumer secret.</param>
        /// <param name="callBackUrl">The call back URL.</param>
        /// <remarks>Author - Vu Dinh</remarks>
        public SocialHelper(Common.Socialtype socialType, string consumerKey, string consumerSecret,string callBackUrl)
        {
            _callBackUrl = callBackUrl;
            _socialtype = socialType;

            var clientType = Type.GetType(string.Format("CR.SocialLib.OAuth.OAuth{0}Client" , socialType));
            if (clientType != null)
            {
                _oAuthClient = Activator.CreateInstance(clientType) as OAuthClientBase;
                if (_oAuthClient != null)
                {
                    _oAuthContext = _oAuthClient.GetInstance(consumerKey, consumerSecret);
                    _oAuthContext.CallbackUrl = callBackUrl;
                }
                    
            }
        }

        /// <summary>
        /// Begins the authentication.
        /// </summary>
        public void BeginAuthentication()
        {
            _oAuthContext.GetRequestToken();
        }

        /// <summary>
        /// Ends the authentication and register tokens.
        /// </summary>
        /// <returns>AccessToken.</returns>
        public AccessToken EndAuthenticationAndRegisterTokens()
        {
            try
            {
                _oAuthContext.EndAuthenticationAndRegisterTokens().GetAccessToken();
                var profile = _oAuthContext.GetProfileResponse();
                return new AccessToken
                {
                    Token = _oAuthContext.AccessToken,
                    TokenSecret = _oAuthContext.AccessTokenSecret,
                    ProfileResponse = profile.StatusCode == HttpStatusCode.OK ? profile.ResponseText : string.Empty,
                    TokenExpireIn = _oAuthContext.TokenExpireIn
                };
            }
            catch (Exception exception)
            {
                return null;

            }
           
        }

        /// <summary>
        /// Gets the user profile.
        /// </summary>
        /// <param name="accessToken">The access token.</param>
        /// <param name="accessTokenSecret">The access token secret.</param>
        /// <returns>System.String.</returns>
        public OAuthResponse GetUserProfile(string accessToken, string accessTokenSecret)
        {
            if (_oAuthContext.OAuthVersion == OAuth.OAuthVersion.V2)
            {
                if (_socialtype == Common.Socialtype.Facebook)
                    accessToken = _oAuthContext.ExtendAccessToken(accessToken);
            }
            else
            {
                //_oAuthContext.GetRequestToken();
            }
            
            _oAuthContext.AccessToken = accessToken;
            _oAuthContext.AccessTokenSecret = accessTokenSecret;
            return _oAuthContext.GetProfileResponse();
        }

        /// <summary>
        /// Posts the message.
        /// </summary>
        /// <param name="accessToken">The access token.</param>
        /// <param name="accessTokenSecret">The access token secret.</param>
        /// <param name="message">The message.</param>
        /// <returns>OAuthResponse.</returns>
        /// <author>Vu Dinh</author>
        /// <modified>02/12/2014 18:26:00</modified>
        public OAuthResponse PostMessage(string accessToken, string accessTokenSecret, string message)
        {
            if (_oAuthContext.OAuthVersion == OAuth.OAuthVersion.V2)
            {
                if (_socialtype == Common.Socialtype.Facebook)
                    accessToken = _oAuthContext.ExtendAccessToken(accessToken);
                _oAuthContext.AccessToken = accessToken;
                _oAuthContext.AccessTokenSecret = accessTokenSecret;
            }
            else
            {
                if (string.IsNullOrEmpty(_oAuthContext.Token) && string.IsNullOrEmpty(_oAuthContext.TokenSecret) &&
               string.IsNullOrEmpty(_oAuthContext.AccessToken) && string.IsNullOrEmpty(_oAuthContext.AccessTokenSecret))
                {
                    //_oAuthContext.GetRequestToken();
                    _oAuthContext.AccessToken = accessToken;
                    _oAuthContext.AccessTokenSecret = accessTokenSecret;
                }
            }
           
            //vu-dinh 9-8-2014 twitter has problem (error 32) when post message included both special chars and url with http:// . Need to remove http:// from url
           if (_socialtype == Common.Socialtype.Twitter)
           {
               if (message.IndexOf(" http://", StringComparison.OrdinalIgnoreCase) > 0)
                   message = message.Replace("http://", string.Empty).Replace("HTTP://", string.Empty);
               else if (message.IndexOf("http://", StringComparison.OrdinalIgnoreCase) > 0)
                   message = message.Replace("http://", " ").Replace("HTTP://", " ");

               var isNonAscii = Regex.IsMatch(message, @"[^\u0000-\u007F]", RegexOptions.IgnoreCase);
               if (isNonAscii) // if the msg has non ascii chars, remove !, :...
               {
                   //var listStripout = new[] {"!", ":", "’", "'","*", "(",")"};
                   var listStripout = new Dictionary<char, string>()
                   {
                       //{'’',"'"},
                       //{'…',"..."},
                       //{'–',"-"},
                       //{'—',"-"},
                   };
                   foreach (var c in listStripout)
                   {
                       message = message.Replace(c.Key.ToString(), c.Value);
                   }
               }
           }

#if DEBUG
            Debug.WriteLine("Send: "+message);
           foreach (var c in message)
           {
               Debug.WriteLine(string.Format("Char: {0}, Int: {1}", c, (int)c));
           }
#endif

            return _oAuthContext.PostMessage(message);
        }
		
		 public OAuthResponse RemoveMsg(string accessToken, string accessTokenSecret, string msgId)
        {
            if (_oAuthContext.OAuthVersion == OAuth.OAuthVersion.V2)
            {
                accessToken = _oAuthContext.ExtendAccessToken(accessToken);
                _oAuthContext.AccessToken = accessToken;
                _oAuthContext.AccessTokenSecret = accessTokenSecret;
            }
            else
            {
                if (string.IsNullOrEmpty(_oAuthContext.Token) && string.IsNullOrEmpty(_oAuthContext.TokenSecret) &&
               string.IsNullOrEmpty(_oAuthContext.AccessToken) && string.IsNullOrEmpty(_oAuthContext.AccessTokenSecret))
                {
                    _oAuthContext.GetRequestToken();
                    _oAuthContext.AccessToken = accessToken;
                    _oAuthContext.AccessTokenSecret = accessTokenSecret;
                }
            }

            return _oAuthContext.RemoveMsg(msgId);
        }
		
		public OAuthResponse SearchMsg(string accessToken, string accessTokenSecret,string msg)
        {
            if (_oAuthContext.OAuthVersion == OAuth.OAuthVersion.V2)
            {
                accessToken = _oAuthContext.ExtendAccessToken(accessToken);
                _oAuthContext.AccessToken = accessToken;
                _oAuthContext.AccessTokenSecret = accessTokenSecret;
            }
            else
            {
                if (string.IsNullOrEmpty(_oAuthContext.Token) && string.IsNullOrEmpty(_oAuthContext.TokenSecret) &&
               string.IsNullOrEmpty(_oAuthContext.AccessToken) && string.IsNullOrEmpty(_oAuthContext.AccessTokenSecret))
                {
                    _oAuthContext.GetRequestToken();
                    _oAuthContext.AccessToken = accessToken;
                    _oAuthContext.AccessTokenSecret = accessTokenSecret;
                }
            }

            return _oAuthContext.SearchMsg(msg);
        }

        public OAuthResponse RemoveMsg(string accessToken, string accessTokenSecret, string msgId)
        {
            if (_oAuthContext.OAuthVersion == OAuth.OAuthVersion.V2)
            {
                accessToken = _oAuthContext.ExtendAccessToken(accessToken);
                _oAuthContext.AccessToken = accessToken;
                _oAuthContext.AccessTokenSecret = accessTokenSecret;
            }
            else
            {
                if (string.IsNullOrEmpty(_oAuthContext.Token) && string.IsNullOrEmpty(_oAuthContext.TokenSecret) &&
               string.IsNullOrEmpty(_oAuthContext.AccessToken) && string.IsNullOrEmpty(_oAuthContext.AccessTokenSecret))
                {
                    _oAuthContext.GetRequestToken();
                    _oAuthContext.AccessToken = accessToken;
                    _oAuthContext.AccessTokenSecret = accessTokenSecret;
                }
            }

            return _oAuthContext.RemoveMsg(msgId);
        }

        public OAuthResponse SearchMsg(string accessToken, string accessTokenSecret,string msg)
        {
            if (_oAuthContext.OAuthVersion == OAuth.OAuthVersion.V2)
            {
                accessToken = _oAuthContext.ExtendAccessToken(accessToken);
                _oAuthContext.AccessToken = accessToken;
                _oAuthContext.AccessTokenSecret = accessTokenSecret;
            }
            else
            {
                if (string.IsNullOrEmpty(_oAuthContext.Token) && string.IsNullOrEmpty(_oAuthContext.TokenSecret) &&
               string.IsNullOrEmpty(_oAuthContext.AccessToken) && string.IsNullOrEmpty(_oAuthContext.AccessTokenSecret))
                {
                    _oAuthContext.GetRequestToken();
                    _oAuthContext.AccessToken = accessToken;
                    _oAuthContext.AccessTokenSecret = accessTokenSecret;
                }
            }

            return _oAuthContext.SearchMsg(msg);
        }

        /// <summary>
        /// Gets the authentication link.
        /// </summary>
        /// <returns>System.String.</returns>
        public string GetAuthenticationLink()
        {
            return _oAuthContext.GetAuthenticationLink();
        }

        /// <summary>
        /// Gets the authentication link.
        /// </summary>
        /// <returns>System.String.</returns>
        public string GetAuthenticationLink(string state)
        {
            return _oAuthContext.GetAuthenticationLink(state);
        }
    }
}
