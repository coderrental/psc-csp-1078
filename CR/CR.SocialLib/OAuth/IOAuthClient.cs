﻿// ***********************************************************************
// Assembly         : SocialLib
// Author           : VU DINH
// Created          : 08-13-2013
//
// Last Modified By : VU DINH
// Last Modified On : 08-13-2013
// ***********************************************************************
// <copyright file="IOAuthClient.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace CR.SocialLib.OAuth
{
    /// <summary>
    /// Interface IOAuthClient
    /// </summary>
    public interface IOAuthClient
    {
        OAuthContext GetInstance(string comsumerKey, string consumerSecret);

    }

    /// <summary>
    /// Class OAuthClientBase
    /// </summary>
    public class OAuthClientBase : IOAuthClient
    {
        public virtual OAuthContext GetInstance(string comsumerKey, string consumerSecret)
        {
            return new OAuthContext();
            //throw new System.NotImplementedException();
        }
    }
}