﻿using System.IO;
using System.Net;

namespace CR.SocialLib.OAuth
{
    public class OAuthResponse
    {
        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        /// <value>The status code.</value>
        public HttpStatusCode StatusCode { get; set; }

        /// <summary>
        /// Gets or sets the response text.
        /// </summary>
        /// <value>The response text.</value>
        public string ResponseText { get; set; }

        /// <summary>
        /// Gets the auth response.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>OAuthResponse.</returns>
        public OAuthResponse GetAuthResponse(HttpWebRequest request)
        {
            try
            {
                var response = request.GetResponse();
                using (var responseStream = response.GetResponseStream())
                {
                    var reader = new StreamReader(responseStream);
                    var responseText = reader.ReadToEnd();
                    reader.Close();

                    StatusCode = HttpStatusCode.OK;
                    ResponseText = responseText;
                    return this;
                }
            }
            catch (WebException we)
            {
                var errrorResponse = we.Response as HttpWebResponse;

                if (errrorResponse != null)
                {
                    var sr = new StreamReader(errrorResponse.GetResponseStream());
                    string responseData = sr.ReadToEnd();
                    sr.Close();

                    StatusCode = errrorResponse.StatusCode;
                    ResponseText = responseData;
                }
                return this;
            }
        }
    }
}
