﻿// ***********************************************************************
// Assembly         : CR.SocialLib
// Author           : VU DINH
// Created          : 08-12-2013
//
// Last Modified By : VU DINH
// Last Modified On : 08-15-2013
// ***********************************************************************
// <copyright file="AccessToken.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace CR.SocialLib.OAuth.Entities
{
    /// <summary>
    /// Class AccessToken
    /// </summary>
    public class AccessToken
    {
        /// <summary>
        /// Gets or sets the token.
        /// </summary>
        /// <value>The token.</value>
        public string Token { get; set; }

        /// <summary>
        /// Gets or sets the token secret.
        /// </summary>
        /// <value>The token secret.</value>
        public string TokenSecret { get; set; }

        /// <summary>
        /// Gets or sets the profile response.
        /// </summary>
        /// <value>The profile response.</value>
        public string ProfileResponse { get; set; }

        /// <summary>
        /// Gets or sets the token expire in.
        /// </summary>
        /// <value>The token expire in.</value>
        public int TokenExpireIn { get; set; }
    }
}
