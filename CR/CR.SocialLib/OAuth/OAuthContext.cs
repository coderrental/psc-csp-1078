﻿// ***********************************************************************
// Assembly         : SocialLib
// Author           : VU DINH
// Created          : 08-05-2013
//
// Last Modified By : VU DINH
// Last Modified On : 08-05-2013
// ***********************************************************************
// <copyright file="OAuthContext.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using Newtonsoft.Json.Linq;

namespace CR.SocialLib.OAuth
{
    public class OAuthContext
    {
        #region " OAuth params "

        public string ConsumerKey;
        public string Realm;
        public string ConsumerSecret;
        public string RequestTokenUrl;
        public string VerifierUrl;
        public string CallbackUrl;
        public string RequestAccessTokenUrl;
        public string RequestProfileUrl;
        public string AccessToken;
        public string AccessTokenSecret;
        public string Token;
        public string TokenSecret;
        public string Verifier;
        public string Scope;
        public string OAuthVersion;
        public string SocialSiteName;
        public string PostStatusUrl;
        public string RequestExtendAccessTokenUrl;
        public int TokenExpireIn;
		public string RemoveMsgUrl;
        public string SearchMsgUrl;
        public string Code; //for version v2 only

        public string ScreenName; //for twitter only
        public string UserId; //for twitter only

        #endregion

        #region " Variables "

        private Dictionary<string, object> _oauthParameters = new Dictionary<string, object>();
        private const string ReservedCharacters = "!*'();:@&=+$,/?%#[]";
        private static readonly Random Random = new Random();

        /// <summary>
        /// Parameters that may appear in the list, but should never be included in the header or the request.
        /// </summary>
        private static readonly string[] SecretParameters = new[]
            {
                "oauth_consumer_secret",
                "oauth_token_secret",
                "oauth_signature"
            };

        #endregion

        /// <summary>
        /// To get active context object
        /// </summary>
        public static OAuthContext Current
        {
            get { return (OAuthContext) HttpContext.Current.Session["oAuthContext"]; }
            set { HttpContext.Current.Session["oAuthContext"] = value; }
        }

        #region " Private methods "

        /// <summary>
        /// Encodes a value for inclusion in a URL querystring.
        /// </summary>
        /// <param name="value">The value to Url encode</param>
        /// <returns>Returns a Url encoded string</returns>
        private string UrlEncodeString(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            value = Uri.EscapeDataString(value);

            // UrlEncode escapes with lowercase characters (e.g. %2f) but oAuth needs %2F
            value = Regex.Replace(value, "(%[0-9a-f][0-9a-f])", c => c.Value.ToUpper());

            // these characters are not escaped by UrlEncode() but needed to be escaped
           value = value
                .Replace("(", "%28")
                .Replace(")", "%29")
                .Replace("$", "%24")
                .Replace("!", "%21")
                .Replace("*", "%2A")
                .Replace("'", "%27");
            
            // these characters are escaped by UrlEncode() but will fail if unescaped!
            value = value.Replace("%7E", "~");

            return value;
        }

        /// <summary>
        /// Encodes a series of key/value pairs for inclusion in a URL querystring.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns>A string of all the <paramref name="parameters"/> keys and value pairs with the values encoded.</returns>
        private string UrlEncode(IEnumerable<KeyValuePair<string, object>> parameters)
        {
            var parameterString = new StringBuilder();

            var paramsSorted = from p in parameters
                               orderby p.Key, p.Value
                               select p;

            foreach (var item in paramsSorted)
            {
                if (!(item.Value is string)) continue;
                if (parameterString.Length > 0)
                {
                    parameterString.Append("&");
                }

                parameterString.Append(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "{0}={1}",
                        UrlEncodeString(item.Key),
                        UrlEncodeString((string) item.Value)));
            }

            return UrlEncodeString(parameterString.ToString());
        }

        /// <summary>
        /// generates timestamp
        /// </summary>
        /// <returns></returns>
        private string GenerateTimeStamp()
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1);
            return Math.Truncate(ts.TotalSeconds).ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// generates nonce
        /// </summary>
        /// <param name="timestamp"></param>
        /// <returns></returns>
        private string GenerateNonce(string timestamp)
        {
            var buffer = new byte[256];
            Random.NextBytes(buffer);
            var hmacsha1 = new HMACSHA1 { Key = Encoding.UTF8.GetBytes(Encoding.UTF8.GetString(buffer)) };
            return ComputeHash(hmacsha1, timestamp);
        }

        /// <summary>
        /// Compte hash. Only for "HMAC-SHA1"
        /// </summary>
        /// <param name="hashAlgorithm"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private string ComputeHash(HashAlgorithm hashAlgorithm, string data)
        {
            if (hashAlgorithm == null)
                throw new ArgumentNullException("hashAlgorithm");
            if (string.IsNullOrEmpty(data))
                throw new ArgumentNullException("data");

            byte[] buffer = Encoding.UTF8.GetBytes(data);
            byte[] bytes = hashAlgorithm.ComputeHash(buffer);

            return Convert.ToBase64String(bytes);
        }

        /// <summary>
        /// function to encode url
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private string NormalizeUrl(string url)
        {
            int questionIndex = url.IndexOf('?');
            if (questionIndex == -1)
                return url;

            var parameters = url.Substring(questionIndex + 1);
            var result = new StringBuilder();
            result.Append(url.Substring(0, questionIndex + 1));

            if (!String.IsNullOrEmpty(parameters))
            {
                string[] parts = parameters.Split('&');
                bool hasQueryParameters = parts.Length > 0;
                foreach (var part in parts)
                {
                    var nameValue = part.Split('=');
                    result.Append(nameValue[0] + "=");
                    if (nameValue.Length == 2)
                        result.Append(UrlEncode(nameValue[1]));
                    result.Append("&");
                }
                if (hasQueryParameters)
                    result = result.Remove(result.Length - 1, 1);
            }
            return result.ToString();
        }

        /// <summary>
        /// Fetch query string and create name value pairs. Used in adding url querystring to OAuth header. For eg. "scope" in google
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private Dictionary<string, object> ExtractQueryStrings(string url)
        {
            int questionIndex = url.IndexOf('?');
            if (questionIndex == -1)
                return new Dictionary<string, object>();

            var parameters = url.Substring(questionIndex + 1);
            var result = new Dictionary<string, object>();

            if (!String.IsNullOrEmpty(parameters))
            {
                string[] parts = parameters.Split('&');
                foreach (var part in parts)
                {
                    if (!string.IsNullOrEmpty(part) && !part.StartsWith("oauth_"))
                    {
                        if (part.IndexOf('=') != -1)
                        {
                            string[] nameValue = part.Split('=');
                            result.Add(nameValue[0], nameValue[1]);
                        }
                        else
                            result.Add(part, String.Empty);
                    }
                }
            }
            return result;
        }


        /// <summary>
        /// Generates the signature.
        /// </summary>
        /// <returns></returns>
        private string GenerateSignature(string httpMethod, string url)
        {
            IEnumerable<KeyValuePair<string, object>> nonSecretParameters = (from p in _oauthParameters
                                                                             where (!SecretParameters.Contains(p.Key))
                                                                             select p);


            // Create the base string. This is the string that will be hashed for the signature.
            string signatureBaseString = string.Format(
                CultureInfo.InvariantCulture,
                "{0}&{1}&{2}",
                httpMethod.ToUpper(CultureInfo.InvariantCulture),
                UrlEncode(NormalizeUrl(url)),
                UrlEncode(nonSecretParameters));


            string key;
            // Create our hash key (you might say this is a password)
            if (!string.IsNullOrEmpty(AccessTokenSecret))
            {
                key = string.Format(
                    CultureInfo.InvariantCulture,
                    "{0}&{1}",
                    UrlEncode(ConsumerSecret),
                    string.IsNullOrEmpty(AccessTokenSecret) ? "" : UrlEncode(AccessTokenSecret));
            }
            else
            {
                var tokenSecret = TokenSecret;
                if (tokenSecret == null)
                {
					if (HttpContext.Current != null && HttpContext.Current.Session[Token] != null)
						tokenSecret = HttpContext.Current.Session[Token].ToString();
                }
                key = string.Format(
                    CultureInfo.InvariantCulture,
                    "{0}&{1}",
                    UrlEncode(ConsumerSecret),
                    string.IsNullOrEmpty(tokenSecret) ? "" : UrlEncode(tokenSecret));
            }


            // Generate the hash
            var hmacsha1 = new HMACSHA1(Encoding.UTF8.GetBytes(key));
            byte[] signatureBytes = hmacsha1.ComputeHash(Encoding.UTF8.GetBytes(signatureBaseString));
            return Convert.ToBase64String(signatureBytes);
        }

        /// <summary>
        /// Generates the authorization header.
        /// </summary>
        /// <returns>The string value of the HTTP header to be included for OAuth requests.</returns>
        private string GenerateAuthorizationHeader(string httpMethod, string url)
        {
            var authHeaderBuilder = new StringBuilder();
            authHeaderBuilder.AppendFormat("OAuth realm=\"{0}\"", Realm);


            var oAuthParametersToIncludeInHeader = new List<string>
                {
                    "oauth_version",
                    "oauth_nonce",
                    "oauth_timestamp",
                    "oauth_signature_method",
                    "oauth_consumer_key",
                    "oauth_token",
                    "oauth_verifier"
                    // Leave signature omitted from the list, it is added manually
                    // "oauth_signature",
                };

            if (!string.IsNullOrEmpty(CallbackUrl))
                oAuthParametersToIncludeInHeader.Add(OAuthParameters.CALLBACK);

            var sortedParameters = from p in _oauthParameters
                                   where oAuthParametersToIncludeInHeader.Contains(p.Key)
                                   orderby p.Key, UrlEncode((p.Value is string) ? (string) p.Value : string.Empty)
                                   select p;

            foreach (var item in sortedParameters)
            {
                authHeaderBuilder.AppendFormat(
                    ",{0}=\"{1}\"",
                    UrlEncode(item.Key),
                    UrlEncode(item.Value as string));
            }

            var signature = GenerateSignature(httpMethod, url);
            authHeaderBuilder.AppendFormat(",oauth_signature=\"{0}\"", UrlEncode(signature));

            return authHeaderBuilder.ToString();
        }

        /// <summary>
        /// Url encoding
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private string UrlEncode(string value)
        {
            if (String.IsNullOrEmpty(value))
                return String.Empty;

            var sb = new StringBuilder();

            foreach (char @char in value)
            {
                if (ReservedCharacters.IndexOf(@char) == -1)
                    sb.Append(@char);
                else
                    sb.AppendFormat("%{0:X2}", (int) @char);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Create query string for different providers
        /// </summary>
        /// <returns></returns>
        private string CreateQueryString()
        {
            string queryString;
            switch (SocialSiteName.ToLower())
            {
                case "twitter":
                    queryString = string.Empty;
                    break;
                case "facebook":
                    queryString = "?access_token=" + AccessToken;
                    break;
                case "linkedin":
                    queryString = "?oauth2_access_token=" + AccessToken + "&format=json";
                    break;
                case "google":
                    queryString = string.Empty;
                    break;
                default:
                    queryString = string.Empty;
                    break;
            }

            return (queryString);
        }

        #endregion Private methods        

        #region " Social site Apis "

        /// <summary>
        /// Extends the access token. (for facebook because facebook's access token has short live)
        /// </summary>
        /// <param name="oldAccessToken">The old access token.</param>
        /// <returns>System.String.</returns>
        /// <author>Vu Dinh</author>
        /// <modified>02/12/2014 18:09:24</modified>
        public string ExtendAccessToken(string oldAccessToken)
        {
            try
            {
                var response = MakeHttpRequest(string.Format(RequestExtendAccessTokenUrl, ConsumerKey, ConsumerSecret, oldAccessToken), string.Empty, "GET");
                var data = GetResponseByte(response);
                using (var reader = new StreamReader(new MemoryStream(data)))
                {
                    var queryStrings = HttpUtility.ParseQueryString(reader.ReadToEnd());
                    return queryStrings["access_token"];
                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }

        /// <summary>
        /// function to get request token
        /// </summary>
        public void GetRequestToken()
        {
            try
            {
                var requestHeader = GetRequestTokenAuthorizationHeader();

                /*Make a request and convert data in Byte[]*/
                var response = MakeHttpRequest(RequestTokenUrl, requestHeader);
                var data = GetResponseByte(response);
                /*----------------------------------------*/

                //Read byte[] and get Token and TokenSecret
                using (var reader = new StreamReader(new MemoryStream(data)))
                {
                    var resultString = reader.ReadToEnd();
                    var queryStrings = HttpUtility.ParseQueryString(resultString);

                    Token = queryStrings["oauth_token"];
                    TokenSecret = queryStrings["oauth_token_secret"];

                    //add tokensecret to session - especially use for LinkedIn authentication
					if (HttpContext.Current!=null)
                	{
						HttpContext.Current.Session.Add(Token, TokenSecret);
                	}
                    
                }
            }
            catch (WebException webException)
            {
                Debug.WriteLine("**************************************");
                Debug.WriteLine(webException.Message);
                Debug.WriteLine(webException.StackTrace);
                Debug.WriteLine("**************************************");
            }
        }

        /// <summary>
        /// function to get access token
        /// </summary>
        public void GetAccessToken()
        {
            try
            {
                /*Make a request and convert data in Byte[]*/
                WebResponse response;
                if (OAuthVersion == OAuth.OAuthVersion.V1)
                {
                    var requestHeader = GetAccessTokenAuthorizationHeader();
                    response = MakeHttpRequest(RequestAccessTokenUrl, requestHeader);
                }
                else // for version 2.0
                    response =
                        MakeHttpRequest(
                            string.Format(RequestAccessTokenUrl, ConsumerKey, CallbackUrl, ConsumerSecret,
                                          Code), string.Empty, "GET");

                var data = GetResponseByte(response);
                /*----------------------------------------*/

                //Read byte[] and get Token and TokenSecret
                using (var reader = new StreamReader(new MemoryStream(data)))
                {
                    var resultString = reader.ReadToEnd();
                    var queryStrings = HttpUtility.ParseQueryString(resultString);

                    if (OAuthVersion == OAuth.OAuthVersion.V1)
                    {
                        AccessToken = queryStrings["oauth_token"];
                    }
                    else
                    {
                        if (SocialSiteName == Common.Socialtype.LinkedIn.ToString()) //Authentication return json format
                        {
                            dynamic obj = JObject.Parse(queryStrings[0]);
                            try
                            {
                                AccessToken = obj["access_token"].ToString();
                                TokenExpireIn = (int)obj["expires_in"];
                            }
                            catch (Exception)
                            {

                                AccessToken = null;
                            }
                        }
                        else // facebook
                        {
                            AccessToken = queryStrings["access_token"];
                        }
                    }
                    AccessTokenSecret = queryStrings["oauth_token_secret"];

                    ScreenName = queryStrings["screen_name"];
                    UserId = queryStrings["user_id"];
                }
            }
            catch (WebException webException)
            {
                Debug.WriteLine("**************************************");
                Debug.WriteLine(webException.Message);
                Debug.WriteLine(webException.StackTrace);
                Debug.WriteLine("**************************************");
            }
        }


        /// <summary>
        /// function to get user profile response
        /// </summary>
        /// <returns></returns>
        public OAuthResponse GetProfileResponse()
        {
            var request = (HttpWebRequest)WebRequest.Create(RequestProfileUrl + CreateQueryString());
            if (OAuthVersion == OAuth.OAuthVersion.V1)
            {
                var requestHeader = GetAuthorizationHeader(RequestProfileUrl);
                request.Headers.Add("Authorization", requestHeader);
            }
            request.Method = HttpMethod.Get;
            return new OAuthResponse().GetAuthResponse(request);
        }
		
		public OAuthResponse RemoveMsg(string msgId)
        {
            string requestUri = string.Empty, requestHeader = string.Empty;

            if (SocialSiteName == Common.Socialtype.Twitter.ToString())
            {
                requestUri = RemoveMsgUrl + string.Format("{0}.json",msgId);
                requestHeader = GetRemoveMsgHeader(requestUri);
            }
         

            var request = (HttpWebRequest)WebRequest.Create(requestUri);
            request.Headers.Add("Authorization", requestHeader);
            request.AutomaticDecompression = DecompressionMethods.None;
            request.Credentials = CredentialCache.DefaultCredentials;
            request.UseDefaultCredentials = true;
            request.AllowWriteStreamBuffering = true;
            request.ServicePoint.Expect100Continue = false;
            request.UserAgent = "CRSocialLib/1.0.0.0";
            request.Method = HttpMethod.Post;
            request.ContentLength = 0;
           

            return new OAuthResponse().GetAuthResponse(request);
        }

        public OAuthResponse SearchMsg(string msg)
        {
            var requestUri = SearchMsgUrl+ "?q=" + UrlEncodeString(msg);
            var requestHeader = GetSearchMsgHeader(SearchMsgUrl, msg);

            var request = (HttpWebRequest)WebRequest.Create(requestUri + CreateQueryString());
            request.Headers.Add("Authorization", requestHeader);
            request.Method = HttpMethod.Get;
            return new OAuthResponse().GetAuthResponse(request);
        }

        public OAuthResponse RemoveMsg(string msgId)
        {
            string requestUri = string.Empty, requestHeader = string.Empty;

            if (SocialSiteName == Common.Socialtype.Twitter.ToString())
            {
                requestUri = RemoveMsgUrl + string.Format("{0}.json",msgId);
                requestHeader = GetRemoveMsgHeader(requestUri);
            }
         

            var request = (HttpWebRequest)WebRequest.Create(requestUri);
            request.Headers.Add("Authorization", requestHeader);
            request.AutomaticDecompression = DecompressionMethods.None;
            request.Credentials = CredentialCache.DefaultCredentials;
            request.UseDefaultCredentials = true;
            request.AllowWriteStreamBuffering = true;
            request.ServicePoint.Expect100Continue = false;
            request.UserAgent = "CRSocialLib/1.0.0.0";
            request.Method = HttpMethod.Post;
            request.ContentLength = 0;
           

            return new OAuthResponse().GetAuthResponse(request);
        }

        public OAuthResponse SearchMsg(string msg)
        {
            var requestUri = SearchMsgUrl+ "?q=" + UrlEncodeString(msg);
            var requestHeader = GetSearchMsgHeader(SearchMsgUrl, msg);

            var request = (HttpWebRequest)WebRequest.Create(requestUri + CreateQueryString());
            request.Headers.Add("Authorization", requestHeader);
            request.Method = HttpMethod.Get;
            return new OAuthResponse().GetAuthResponse(request);
        }

        /// <summary>
        /// Posts the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns>System.String.</returns>
        public OAuthResponse PostMessage(string message)
        {
            string requestUri = string.Empty, requestHeader = string.Empty;

            if (SocialSiteName == Common.Socialtype.LinkedIn.ToString())
            {
                requestUri = PostStatusUrl + "?oauth2_access_token=" + AccessToken + "&format=json"; ;
            }
            else if (SocialSiteName == Common.Socialtype.Twitter.ToString())
            {
                requestUri = PostStatusUrl + "?status=" + UrlEncodeString(message);
                requestHeader = GetPostStatusHeader(PostStatusUrl, message, HttpMethod.Post);
            }
            else if (SocialSiteName == Common.Socialtype.Facebook.ToString())
            {
                requestUri = PostStatusUrl + "?message=" + UrlEncodeString(message) + "&access_token=" + AccessToken + "&client_id="  + ConsumerKey;
            }

            var request = (HttpWebRequest) WebRequest.Create(requestUri);
            if (OAuthVersion == OAuth.OAuthVersion.V1) //linked oauth 2 dont use authorization header
                request.Headers.Add("Authorization", requestHeader);
            request.AutomaticDecompression = DecompressionMethods.None ;
            request.Credentials = CredentialCache.DefaultCredentials;
            request.UseDefaultCredentials = true;
            request.AllowWriteStreamBuffering = true;
            request.ServicePoint.Expect100Continue = false;
            request.UserAgent = "CRSocialLib/1.0.0.0";

            if (SocialSiteName == Common.Socialtype.LinkedIn.ToString())
            {
                request.Method = HttpMethod.Post;
                string xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                xml += string.Format(@"<share>
  <comment><![CDATA[{0}]]></comment>
  <visibility>
	 <code>anyone</code>
  </visibility>
</share>",message);

                byte[] content = Encoding.UTF8.GetBytes(xml);
                request.ContentLength = content.Length;

                Stream reqStream = request.GetRequestStream();

                reqStream.Write(content, 0, content.Length);
                reqStream.Close();
            }
            else
            {
                request.Method = HttpMethod.Post;
                request.ContentLength = 0;
            }

            return new OAuthResponse().GetAuthResponse(request);
        }

        /// <summary>
        /// Gets the authentication link.
        /// </summary>
        /// <returns>System.String.</returns>
        public string GetAuthenticationLink()
        {
            if (SocialSiteName == Common.Socialtype.LinkedIn.ToString())
            {
                VerifierUrl += "&state=socialapp" ;
            }
            Current = this;
            if (OAuthVersion == OAuth.OAuthVersion.V1)
                return (VerifierUrl + "?oauth_token=" + Token + "&oauth_callback=" + CallbackUrl);
            return (string.Format(VerifierUrl, ConsumerKey, CallbackUrl, Scope));
        }

        /// <summary>
        /// Gets the authentication link.
        /// </summary>
        /// <returns>System.String.</returns>
        public string GetAuthenticationLink(string state)
        {
            if (SocialSiteName == Common.Socialtype.LinkedIn.ToString())
            {
                VerifierUrl += "&state=" + state;
            }
            
            Current = this;
            if (OAuthVersion == OAuth.OAuthVersion.V1)
                return (VerifierUrl + "?oauth_token=" + Token + "&oauth_callback=" + CallbackUrl);
            return (string.Format(VerifierUrl, ConsumerKey, CallbackUrl, Scope));
        }

        /// <summary>
        /// Last step of request verification. Then we begin with GetAccessToken and then GetProfile
        /// </summary>
        public OAuthContext EndAuthenticationAndRegisterTokens()
        {
            var request = HttpContext.Current.Request;
            if (OAuthVersion == OAuth.OAuthVersion.V1)
            {
                if (!string.IsNullOrEmpty(request.QueryString["oauth_verifier"]))
                    Verifier = request.QueryString["oauth_verifier"];
                if (!string.IsNullOrEmpty(request.QueryString["oauth_token"]))
                    Token = request.QueryString["oauth_token"];
                if (!string.IsNullOrEmpty(request.QueryString["oauth_token_secret"]))
                    TokenSecret = request.QueryString["oauth_token_secret"];
            }
            else
            {
                if (!string.IsNullOrEmpty(request.QueryString["code"]))
                    Code = request.QueryString["code"];
            }
            return this;
        }

        /// <summary>
        /// function to make HttpRequests
        /// </summary>
        /// <param name="url"></param>
        /// <param name="authorizationHeader"></param>
        /// <param name="requestMethod"></param>
        /// <returns></returns>
        private WebResponse MakeHttpRequest(string url, string authorizationHeader, string requestMethod = "POST")
        {
            var normalizedUrl = NormalizeUrl(url);
            var request =  WebRequest.Create(normalizedUrl);
            request.Method = requestMethod;
            if (authorizationHeader != string.Empty)
            {
                request.Headers.Add("Authorization", authorizationHeader);
                //request.GetResponse();
            }
            try
            {
                return (request.GetResponse());
            }
            catch (WebException e)
            {
                using (var resp = e.Response)
                {

                    using (var sr = new StreamReader(resp.GetResponseStream()))
                    {
                        var errorMessage = sr.ReadToEnd();
						if (HttpContext.Current!=null)
                    	{
							//HttpContext.Current.Response.Write(errorMessage);
							((Page)HttpContext.Current.CurrentHandler).ClientScript.RegisterStartupScript(GetType(),
																										   "Error: ",
																										   "<script>alert('Error: "+e.Message+"');</script> ");
							
                    	}
						throw new WebException(errorMessage, e);
                        
                    }
                }
            }
        }

        /// <summary>
        /// function to get results in byte[]
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        private byte[] GetResponseByte(WebResponse response)
        {
            var ms = new MemoryStream();
            var binaryWriter = new BinaryWriter(ms);

            using (var binaryReader = new BinaryReader(response.GetResponseStream()))
            {
                int readedByteCount;
                const int bufferLength = 65536;
                do
                {
                    var data = new byte[bufferLength];
                    readedByteCount = binaryReader.Read(data, 0, data.Length);

                    if (readedByteCount == 0)
                        break;
                    binaryWriter.Write(data, 0, readedByteCount);
                } while (readedByteCount <= bufferLength);
            }

            ms.Position = 0;
            byte[] resultData = ms.ToArray();
            return resultData;
        }

        #endregion

        #region " Authorization Header functions "

        /// <summary>
        /// resets _oauthParameters var
        /// </summary>
        private void ResetOAuthParameters()
        {
            _oauthParameters.Clear();
        }

        /// <summary>
        /// Register basic oauth parameters
        /// </summary>
        /// <param name="url"></param>
        /// <param name="timestamp"></param>
        /// <param name="nounce"></param>
        private void RegisterBasicOAuthParameters(string url, string timestamp, string nounce)
        {
            _oauthParameters = ExtractQueryStrings(url);
            _oauthParameters.Add(OAuthParameters.CONSUMER_KEY, ConsumerKey);
            _oauthParameters.Add(OAuthParameters.SIGNATURE_METHOD, SignatureMethod.HMACSHA1);
            _oauthParameters.Add(OAuthParameters.TIMESTAMP, timestamp);
            _oauthParameters.Add(OAuthParameters.NOUNCE, nounce);
            _oauthParameters.Add(OAuthParameters.VERSION, OAuthVersion);
        }

        /// <summary>
        /// function to return request token authorization header
        /// </summary>
        /// <returns></returns>
        private string GetRequestTokenAuthorizationHeader()
        {
            var timestamp = GenerateTimeStamp();
            var nounce = GenerateNonce(timestamp);

            ResetOAuthParameters();

            //Register basic oauth parameters
            RegisterBasicOAuthParameters(RequestTokenUrl, timestamp, nounce);

            _oauthParameters.Add(OAuthParameters.CALLBACK, CallbackUrl);

            return GenerateAuthorizationHeader(HttpMethod.Post, RequestTokenUrl);
        }

        /// <summary>
        /// function to return access token authorization header
        /// </summary>
        /// <returns></returns>
        private string GetAccessTokenAuthorizationHeader()
        {
            var timestamp = GenerateTimeStamp();
            var nounce = GenerateNonce(timestamp);

            ResetOAuthParameters();

            //Register basic oauth parameters
            RegisterBasicOAuthParameters(RequestAccessTokenUrl, timestamp, nounce);

            _oauthParameters.Add(OAuthParameters.TOKEN, Token);
            _oauthParameters.Add(OAuthParameters.VERIFIER, Verifier);

            return GenerateAuthorizationHeader(HttpMethod.Post, RequestAccessTokenUrl);
        }


        /// <summary>
        /// Gets the authorization header.
        /// </summary>
        /// <param name="url">The request URL.</param>
        /// <returns>System.String.</returns>
        private string GetAuthorizationHeader(string url)
        {
            var timestamp = GenerateTimeStamp();
            var nounce = GenerateNonce(timestamp);

            ResetOAuthParameters();

            //Register basic oauth parameters
            RegisterBasicOAuthParameters(url + CreateQueryString(), timestamp, nounce);

            _oauthParameters.Add(OAuthParameters.TOKEN, AccessToken);
            return GenerateAuthorizationHeader(HttpMethod.Get, url);
        }

        private string GetSearchMsgHeader(string url, string search)
        {
            var timestamp = GenerateTimeStamp();
            var nounce = GenerateNonce(timestamp);

            ResetOAuthParameters();

            //Register basic oauth parameters
            RegisterBasicOAuthParameters(url, timestamp, nounce);

            _oauthParameters.Add(OAuthParameters.TOKEN, AccessToken);
            if (SocialSiteName == Common.Socialtype.Twitter.ToString())
            {
                _oauthParameters.Add("q", search);
            }


            var header = GenerateAuthorizationHeader(HttpMethod.Get, url);
            return header;
        }

        private string GetRemoveMsgHeader(string url)
        {
            var timestamp = GenerateTimeStamp();
            var nounce = GenerateNonce(timestamp);

            ResetOAuthParameters();

            //Register basic oauth parameters
            RegisterBasicOAuthParameters(url, timestamp, nounce);

            _oauthParameters.Add(OAuthParameters.TOKEN, AccessToken);

            var header = GenerateAuthorizationHeader(HttpMethod.Post, url);
            return header;
        }

        /// <summary>
        /// Gets the post status header.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="status">The status.</param>
        /// <param name="httpMethod">The HTTP method.</param>
        /// <returns>System.String.</returns>
        private string GetPostStatusHeader(string url, string status, string httpMethod)
        {
            var timestamp = GenerateTimeStamp();
            var nounce = GenerateNonce(timestamp);

            ResetOAuthParameters();

            //Register basic oauth parameters
            RegisterBasicOAuthParameters(url, timestamp, nounce);

            _oauthParameters.Add(OAuthParameters.TOKEN, AccessToken);
            
            if (SocialSiteName == Common.Socialtype.Twitter.ToString())
            {
                _oauthParameters.Add("status", status);
            }

            var header = GenerateAuthorizationHeader(httpMethod, url);
            return header;
        }

        #endregion
    }

    #region " Supporting classes"

    /// <summary>
    /// OAuth parameters constants
    /// </summary>
    public static class OAuthParameters
    {
        public const string CONSUMER_KEY = "oauth_consumer_key";
        public const string SIGNATURE_METHOD = "oauth_signature_method";
        public const string SIGNATURE = "oauth_signature";
        public const string TIMESTAMP = "oauth_timestamp";
        public const string NOUNCE = "oauth_nonce";
        public const string VERSION = "oauth_version";
        public const string CALLBACK = "oauth_callback";
        public const string VERIFIER = "oauth_verifier";
        public const string TOKEN = "oauth_token";
        public const string TOKEN_SECRET = "oauth_token_secret";
    }

    /// <summary>
    /// Signature methods constants
    /// </summary>
    public static class SignatureMethod
    {
        public const string HMACSHA1 = "HMAC-SHA1";
        public const string RSASHA1 = "RSA-SHA1";
        public const string PLAINTEXT = "PLAINTEXT";
    }

    /// <summary>
    /// Name value collections
    /// </summary>
    public class NameValue
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public NameValue(string name, string value)
        {
            Name = name;
            Value = value;
        }
    }

    /// <summary>
    /// OAuth versions
    /// </summary>
    public class OAuthVersion
    {
        public static string V1 = "1.0";
        public static string V2 = "2.0";
    }

    /// <summary>
    /// HttpMethods 
    /// </summary>
    public class HttpMethod
    {
        public static string Get = "GET";
        public static string Post = "POST";
        public static string Put = "PUT";
    }

    #endregion
}