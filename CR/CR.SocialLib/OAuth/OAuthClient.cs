﻿// ***********************************************************************
// Assembly         : SocialLib
// Author           : VU DINH
// Created          : 08-05-2013
//
// Last Modified By : VU DINH
// Last Modified On : 08-05-2013
// ***********************************************************************
// <copyright file="OAuthClient.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Web;

namespace CR.SocialLib.OAuth
{
    /// <summary>
    /// Class OAuthFacebookClient
    /// </summary>
    public class OAuthFacebookClient : OAuthClientBase
    {
        private OAuthContext _oAuthContext;

        public override OAuthContext GetInstance(string consumerKey, string consumerSecret)
        {
            if (_oAuthContext != null)
                return _oAuthContext;
            _oAuthContext = new OAuthContext
            {
                ConsumerKey = consumerKey,
                ConsumerSecret = consumerSecret,
                VerifierUrl = Credentials.Facebook.VerifierUrl,
                RequestAccessTokenUrl = Credentials.Facebook.RequestAccessTokenUrl,
                RequestProfileUrl = Credentials.Facebook.RequestProfileUrl,
                Scope = Credentials.Facebook.Scope,
                OAuthVersion = OAuthVersion.V2,
                SocialSiteName = Common.Socialtype.Facebook.ToString(),
                RequestExtendAccessTokenUrl = Credentials.Facebook.RequestExtendAccessTokenUrl,
                PostStatusUrl = Credentials.Facebook.PostStatusUrl
            };
            return _oAuthContext;
        }
    }

    /// <summary>
    /// Class OAuthLinkedInClient
    /// </summary>
    public class OAuthLinkedInClient : OAuthClientBase
    {
        private OAuthContext _oAuthContext;

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <param name="consumerKey">The consumer key.</param>
        /// <param name="consumerSecret">The consumer secret.</param>
        /// <returns>OAuthContext.</returns>
        public override OAuthContext GetInstance(string consumerKey, string consumerSecret)
        {
            if (_oAuthContext != null)
                return _oAuthContext;
            _oAuthContext = new OAuthContext
                {
                    /*
                    ConsumerKey = consumerKey,
                    ConsumerSecret = consumerSecret,
                    RequestTokenUrl = Credentials.LinkedIn.RequestTokenUrl,
                    VerifierUrl = Credentials.LinkedIn.VerifierUrl,
                    RequestAccessTokenUrl = Credentials.LinkedIn.RequestAccessTokenUrl,
                    RequestProfileUrl = Credentials.LinkedIn.RequestProfileUrl,
					Realm = @"http://tiekinetix.net",
					OAuthVersion = OAuthVersion.V1,
                    SocialSiteName = Common.Socialtype.LinkedIn.ToString(),
                    PostStatusUrl = Credentials.LinkedIn.PostStatusUrl
                     * */
                    ConsumerKey = consumerKey,
                    ConsumerSecret = consumerSecret,
                    VerifierUrl = Credentials.LinkedIn.VerifierUrl,
                    RequestAccessTokenUrl = Credentials.LinkedIn.RequestAccessTokenUrl,
                    RequestProfileUrl = Credentials.LinkedIn.RequestProfileUrl,
                    Scope = Credentials.LinkedIn.Scope,
                    OAuthVersion = OAuthVersion.V2,
                    SocialSiteName = Common.Socialtype.LinkedIn.ToString(),
                    PostStatusUrl = Credentials.LinkedIn.PostStatusUrl
                };
            return _oAuthContext;
        }
    }

    /// <summary>
    /// Class OAuthLinkedInClient
    /// </summary>
    public class OAuthTwitterClient : OAuthClientBase
    {
        private OAuthContext _oAuthContext;

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <param name="consumerKey">The consumer key.</param>
        /// <param name="consumerSecret">The consumer secret.</param>
        /// <returns>OAuthContext.</returns>
        public override OAuthContext GetInstance(string consumerKey, string consumerSecret)
        {
            if (_oAuthContext != null)
                return _oAuthContext;

            _oAuthContext = new OAuthContext
            {
                ConsumerKey = consumerKey,
                ConsumerSecret = consumerSecret,
                RequestTokenUrl = Credentials.Twitter.RequestTokenUrl,
                VerifierUrl = Credentials.Twitter.VerifierUrl,
                RequestAccessTokenUrl = Credentials.Twitter.RequestAccessTokenUrl,
                RequestProfileUrl = Credentials.Twitter.RequestProfileUrl,
				Realm = "http://tiekinetix.net",
				OAuthVersion = OAuthVersion.V1,
                SocialSiteName = Common.Socialtype.Twitter.ToString(),
                PostStatusUrl = Credentials.Twitter.PostStatusUrl,
                RemoveMsgUrl =  Credentials.Twitter.RemoveMsgUrl,
                SearchMsgUrl =  Credentials.Twitter.SearchMsgUrl,
            };
            return _oAuthContext;
        }
    }
}