﻿// ***********************************************************************
// Assembly         : LinkedInLib
// Author           : VU DINH
// Created          : 08-05-2013
//
// Last Modified By : VU DINH
// Last Modified On : 08-05-2013
// ***********************************************************************
// <copyright file="Credentials.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace CR.SocialLib.OAuth
{
    /// <summary>
    /// Class Credentials
    /// </summary>
    public class Credentials
    {
        /// <summary>
        /// Class Facebook
        /// </summary>
        public static class Facebook
        {          
            public static string VerifierUrl = "https://graph.facebook.com/oauth/authorize?client_id={0}&redirect_uri={1}&scope={2}";
            public static string RequestAccessTokenUrl = "https://graph.facebook.com/oauth/access_token?client_id={0}&redirect_uri={1}&client_secret={2}&code={3}";
            public static string RequestProfileUrl = "https://graph.facebook.com/me";
            public static string PostStatusUrl = "https://graph.facebook.com/me/feed";
            public static string RequestExtendAccessTokenUrl ="https://graph.facebook.com/oauth/access_token?client_id={0}&client_secret={1}&grant_type=fb_exchange_token&fb_exchange_token={2}";
            public static string Scope = "publish_stream,read_stream,email,offline_access,read_friendlists,user_friends,status_update"; 
        }


        /// <summary>
        /// Class LinkedIn
        /// </summary>
        public static class LinkedIn
        {
            /*
            public static string RequestTokenUrl = "https://api.linkedin.com/uas/oauth/requestToken";
            public static string VerifierUrl = "https://www.linkedin.com/uas/oauth/authenticate";
            public static string RequestAccessTokenUrl = "https://api.linkedin.com/uas/oauth/accessToken";
            public static string RequestProfileUrl = "http://api.linkedin.com/v1/people/~/connections";
            public static string PostStatusUrl = "http://api.linkedin.com/v1/people/~/current-status";
            public static string Scope = ""; 
             * */
            public static string VerifierUrl = "https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id={0}&redirect_uri={1}&scope={2}";
            public static string RequestAccessTokenUrl = "https://www.linkedin.com/uas/oauth2/accessToken?grant_type=authorization_code&code={3}&redirect_uri={1}&client_id={0}&client_secret={2}";
            public static string RequestProfileUrl = "https://api.linkedin.com/v1/people/~/connections";
            //public static string PostStatusUrl = "https://api.linkedin.com/v1/people/~/current-status";
            public static string PostStatusUrl = "https://api.linkedin.com/v1/people/~/shares";
            public static string Scope = "r_basicprofile%20r_network%20rw_nus"; 
        }

        public static class Twitter
        {
            public static string RequestTokenUrl = "https://api.twitter.com/oauth/request_token";
            public static string VerifierUrl = "https://api.twitter.com/oauth/authorize";
            public static string RequestAccessTokenUrl = "https://api.twitter.com/oauth/access_token";
            public static string RequestProfileUrl = "https://api.twitter.com/1.1/account/verify_credentials.json";
            public static string PostStatusUrl = "https://api.twitter.com/1.1/statuses/update.json";
            public static string Scope = "";
            public static string RemoveMsgUrl = "https://api.twitter.com/1.1/statuses/destroy/";
            public static string SearchMsgUrl = "https://api.twitter.com/1.1/search/tweets.json";
        }
    }
}