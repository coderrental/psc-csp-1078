﻿// <summary></summary>
// ***********************************************************************

using System;
using System.Collections.Generic;
using System.Net;

namespace CR.SocialLib.Bitly.Entities
{
    public class ClicksResponse : BaseResponse
    {
        public class BitlyClicksDaily
        {
           // public int Id { get; set; }
          //  public int BitlyLinkId { get; set; }
            public int Clicks { get; set; }
            public DateTime Date { get; set; }
            public string BitlyLink { get; set; }
        }

        public List<BitlyClicksDaily> BitlyClicks;

        /// <summary>
        /// Deseriallizes the specified json.
        /// </summary>
        /// <param name="json">The json.</param>
        /// <param name="url"></param>
        /// <param name="bitlyUrl"></param>
        /// <returns>ReferringDomainsResponse.</returns>
        public new ClicksResponse Deseriallize(string json,string bitlyUrl)
        {
            base.Deseriallize(json);
            BitlyClicks = new List<BitlyClicksDaily>();
            if (StatusCode == (int)HttpStatusCode.OK)
            {
                var records = Obj["data"]["link_clicks"];
                foreach (var record in records)
                {
                    BitlyClicks.Add(new BitlyClicksDaily
                    {
                        Clicks = int.Parse(record["clicks"].ToString()),
                        Date = UnixTimeStampToDateTime(double.Parse(record["dt"].ToString())),
                        BitlyLink = bitlyUrl
                    });
                }

            }
            return this;
        }
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
    }
}
