﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;

namespace CR.SocialLib.Bitly.Entities
{
    public class ShortenURLClicksResponse
    {
        public string Title;
        public string Domain;
        public string Url;
        public string Visits;
        public string CreatedAt;
        public string Parameter;
        public string Messages;

        public new ShortenURLClicksResponse Deseriallize(string json)
        {
            var Obj = JObject.Parse(json.TrimStart('[').TrimEnd(']'));
            if (Obj["messages"] == null)
            {
                Title = Obj["title"].ToString();
                Domain = Obj["domain"].ToString();
                Url = Obj["url"].ToString();
                Visits = Obj["visits"].ToString();
                CreatedAt = Obj["createdAt"].ToString();
                Title = Obj["title"].ToString();
                Messages = "OK";
            }
            else
            {
                Parameter = Obj["parameter"].ToString();
                Messages = Obj["messages"].ToString();
            }
            return this;
        }
    }
}
