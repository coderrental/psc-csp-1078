﻿
using System.Net;

namespace CR.SocialLib.Bitly.Entities
{
    public class ExpandResponse : BaseResponse
    {
        public string LongUrl;
        public string ShortenUrl;
        public string UserHash;
        public string GlobalHash;

        /// <summary>
        /// Deseriallizes the specified json.
        /// </summary>
        /// <param name="json">The json.</param>
        /// <returns>BaseResponse.</returns>
        public new ExpandResponse Deseriallize(string json)
        {
            base.Deseriallize(json);
            if (StatusCode == (int)HttpStatusCode.OK)
            {
                var records = Obj["data"]["expand"];
                foreach (var record in records)
                {
                    LongUrl = record["long_url"].ToString();
                    ShortenUrl = record["short_url"].ToString();
                    UserHash = record["user_hash"].ToString();
                    GlobalHash = record["global_hash"].ToString();
                }
            }
            return this;
        }
    }
}
