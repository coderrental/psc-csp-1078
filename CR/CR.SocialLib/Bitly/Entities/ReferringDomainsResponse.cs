﻿// ***********************************************************************
// Assembly         : CR.SocialLib
// Author           : VU DINH
// Created          : 08-30-2013
//
// Last Modified By : VU DINH
// Last Modified On : 08-30-2013
// ***********************************************************************
// <copyright file="ReferringDomainsResponse.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Collections.Generic;
using System.Net;

namespace CR.SocialLib.Bitly.Entities
{
    public class ReferringDomainsResponse:BaseResponse
    {
        public class BitlyClickStatistic
        {
            /// <summary>
            /// Gets or sets the clicks.
            /// </summary>
            /// <value>The clicks.</value>
            public int Clicks { get; set; }

            /// <summary>
            /// Gets or sets the domain.
            /// </summary>
            /// <value>The domain.</value>
            public string Domain { get; set; }

            /// <summary>
            /// Gets or sets the URL.
            /// </summary>
            /// <value>The URL.</value>
            public string Url { get; set; }
        }

        public List<BitlyClickStatistic> ReferringDomains;

        /// <summary>
        /// Deseriallizes the specified json.
        /// </summary>
        /// <param name="json">The json.</param>
        /// <returns>ReferringDomainsResponse.</returns>
        public new ReferringDomainsResponse Deseriallize(string json)
        {
            base.Deseriallize(json);
            ReferringDomains = new List<BitlyClickStatistic>();
            if (StatusCode == (int) HttpStatusCode.OK)
            {
                var records = Obj["data"]["referring_domains"];
                foreach (var record in records)
                {
                   ReferringDomains.Add(new BitlyClickStatistic
                       {
                           Clicks = int.Parse(record["clicks"].ToString()),
                           Domain = record["domain"].ToString(),
                           Url = record["url"] != null ? record["url"].ToString() : string.Empty
                       });
                }
               
            }
            return this;
        }
    }
}
