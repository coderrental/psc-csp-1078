﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;

namespace CR.SocialLib.Bitly.Entities
{
    public class ShortenUrlDefaultResponse
    {
        public string ShortenUrl;
        public string Alias;
        public string LongUrl;
        public string Title;
        public string Parameter;
        public string Messages;
        public new ShortenUrlDefaultResponse Deseriallize(string json)
        {
            var Obj = JObject.Parse(json.TrimStart('[').TrimEnd(']'));
            if (Obj["messages"] == null)
            {
                ShortenUrl = Obj["shortUrl"].ToString();
                Alias = Obj["alias"].ToString();
                LongUrl = Obj["longUrl"].ToString();
                Title = Obj["title"].ToString();
                Messages = "OK";
            }
            else
            {
                Parameter = Obj["parameter"].ToString();
                Messages = Obj["messages"].ToString();
            }
            return this;
        }
    }
}
