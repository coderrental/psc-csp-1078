﻿// ***********************************************************************
// Assembly         : CR.SocialLib
// Author           : VU DINH
// Created          : 08-30-2013
//
// Last Modified By : VU DINH
// Last Modified On : 08-30-2013
// ***********************************************************************
// <copyright file="BaseResponse.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using Newtonsoft.Json.Linq;

namespace CR.SocialLib.Bitly.Entities
{
    public class BaseResponse
    {
        protected dynamic Obj;
        public virtual BaseResponse Deseriallize(string json)
        {
            Obj = JObject.Parse(json);
            StatusCode = int.Parse(Obj["status_code"].ToString());
            StatusText = Obj["status_txt"].ToString();
            return this;
        }

        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        /// <value>The status code.</value>
        public int StatusCode { get; set; }

        /// <summary>
        /// Gets or sets the status text.
        /// </summary>
        /// <value>The status text.</value>
        public string StatusText { get; set; }
    }
}
