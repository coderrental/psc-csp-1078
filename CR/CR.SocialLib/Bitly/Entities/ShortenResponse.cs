﻿// ***********************************************************************
// Assembly         : CR.SocialLib
// Author           : VU DINH
// Created          : 08-30-2013
//
// Last Modified By : VU DINH
// Last Modified On : 08-30-2013
// ***********************************************************************
// <copyright file="ShortenResponse.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Net;

namespace CR.SocialLib.Bitly.Entities
{
    public class ShortenResponse : BaseResponse
    {
        public string LongUrl;
        public string ShortenUrl;
        public string Hash;
        public string GlobalHash;



        /// <summary>
        /// Deseriallizes the specified json.
        /// </summary>
        /// <param name="json">The json.</param>
        /// <returns>BaseResponse.</returns>
        public new ShortenResponse Deseriallize(string json)
        {
            base.Deseriallize(json);
            if (StatusCode == (int)HttpStatusCode.OK)
            {
                LongUrl = Obj["data"]["long_url"].ToString();
                ShortenUrl = Obj["data"]["url"].ToString();
                Hash = Obj["data"]["hash"].ToString();
                GlobalHash = Obj["data"]["global_hash"].ToString();
            }
            return this;
        }
    }
}
