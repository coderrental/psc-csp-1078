﻿// ***********************************************************************
// Assembly         : CR.SocialLib
// Author           : VU DINH
// Created          : 08-27-2013
//
// Last Modified By : VU DINH
// Last Modified On : 08-27-2013
// ***********************************************************************
// <copyright file="BitlyHelper.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Net;
using System.Text.RegularExpressions;
using CR.SocialLib.Bitly.Entities;
using CR.SocialLib.OAuth;

namespace CR.SocialLib.Bitly
{
    public class BitlyHelper
    {
        private readonly string _accessToken;

        /// <summary>
        /// Initializes a new instance of the <see cref="BitlyHelper"/> class.
        /// </summary>
        /// <param name="accessToken">The access token.</param>
        public BitlyHelper(string accessToken)
        {
            _accessToken = accessToken;
        }

        /// <summary>
        /// Gets the link statistic.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <returns>OAuthResponse.</returns>
        public ReferringDomainsResponse GetLinkStatistic(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(string.Format("https://api-ssl.bitly.com/v3/link/referring_domains?access_token={0}&link={1}", _accessToken, UrlEncodeString(url)));
            request.Method = "GET";
            var response = new OAuthResponse().GetAuthResponse(request);
            var responseData = new ReferringDomainsResponse().Deseriallize(response.ResponseText);
            if (responseData.StatusText == "RATE_LIMIT_EXCEEDED")
            {
                throw new Exception("Bitly API: " + responseData.StatusText);
            }
            return responseData;
           // throw new Exception("Bitly API: " + responseData.StatusText);
        }

        public ClicksResponse GetClicksDaily(string url,string unit, string units,string timezone)
        {
            if (url.EndsWith("/"))
            {
                url = url.TrimEnd('/');
            }
            var request = (HttpWebRequest)WebRequest.Create(string.Format("https://api-ssl.bitly.com/v3/link/clicks?access_token={0}&link={1}&rollup=false&unit={2}&units={3}&timezone={4}", _accessToken, UrlEncodeString(url),unit,units,timezone));
            request.Method = "GET";
            var response = new OAuthResponse().GetAuthResponse(request);

            var responseData = new ClicksResponse().Deseriallize(response.ResponseText,url);
            if (responseData.StatusText == "RATE_LIMIT_EXCEEDED")
            {
                throw new Exception("Bitly API: " + responseData.StatusText);
            }
            //throw new Exception("Bitly API: " + responseData.StatusText);

            return responseData;
        }

        public long GetTime()
        {
            DateTime dtCurTime = DateTime.Now.Subtract(new TimeSpan(1,0,0,0)).ToUniversalTime();
            DateTime dtEpochStartTime = Convert.ToDateTime("1/1/1970 0:00:00 AM");

            TimeSpan ts = dtCurTime.Subtract(dtEpochStartTime);

            double epochtime;

            epochtime = ((((((ts.Days * 24) + ts.Hours) * 60) + ts.Minutes) * 60) + ts.Seconds);

            return Convert.ToInt64(epochtime);
        }

        /// <summary>
        /// Gets the shorten URL.
        /// </summary>
        /// <param name="longUrl">The long URL.</param>
        /// <returns>ShortenResponse.</returns>
        public ShortenResponse GetShortenUrl(string longUrl)
        {
            var request = (HttpWebRequest)WebRequest.Create(string.Format("https://api-ssl.bitly.com/v3/shorten?access_token={0}&longUrl={1}", _accessToken, UrlEncodeString(longUrl)));
            request.Method = "GET";
            var response =  new OAuthResponse().GetAuthResponse(request);
            var responseData =  new ShortenResponse().Deseriallize(response.ResponseText);
            if (responseData.StatusCode == (int) HttpStatusCode.OK)
            {
                //System.Threading.Thread.Sleep(1000 * 60);
                return responseData;
            }

            if (responseData.StatusCode == (int) HttpStatusCode.InternalServerError && responseData.StatusText=="ALREADY_A_BITLY_LINK")
            {
                // ltu 7/3/14 since we cant bitlify a bitlified message, we need to remove the ?message and stuff
                // ex: "http://ift.tt/1k3wQfy/?messageid=a91875b0-401e-432a-98be-0172316abd3e&cspid=0&socialid=0"
                int index = longUrl.LastIndexOf("/?messageid=", System.StringComparison.Ordinal);
                if (index > 0)
                    longUrl = longUrl.Substring(0, index);
                else
                {
                    index = longUrl.LastIndexOf("?messageid=", System.StringComparison.Ordinal);
                    if (index > 0)
                        longUrl = longUrl.Substring(0, index);
                }
                responseData.ShortenUrl = longUrl;
                responseData.LongUrl = longUrl;
                //System.Threading.Thread.Sleep(1000 * 60);
                return responseData;
            }
            if (responseData.StatusText == "RATE_LIMIT_EXCEEDED")
            {
                throw new Exception("Bitly API: " + responseData.StatusText);
            }
            return responseData;
            // throw new Exception("Bitly API: " + responseData.StatusText);
        }

        /// <summary>
        /// Gets the Long URL.
        /// </summary>
        /// <param name="bitlyUrl"></param>
        /// <returns>ShortenResponse.</returns>
        public ExpandResponse GetLongUrl(string bitlyUrl)
        {
            var request = (HttpWebRequest)WebRequest.Create(string.Format("https://api-ssl.bitly.com/v3/expand?access_token={0}&shortUrl={1}", _accessToken, UrlEncodeString(bitlyUrl)));
            request.Method = "GET";
            var response = new OAuthResponse().GetAuthResponse(request);
            var responseData = new ExpandResponse().Deseriallize(response.ResponseText);
            if (responseData.StatusText == "RATE_LIMIT_EXCEEDED")
            {
                throw new Exception("Bitly API: " + responseData.StatusText);
            }
            return responseData;
            // throw new Exception("Bitly API: " + responseData.StatusText);
        }

        public ShortenUrlDefaultResponse GetShortenUrlDefault(string longUrl,string apiKey)
        {
            var request = (HttpWebRequest)WebRequest.Create(string.Format("http://tieki.net/s/api/?url={0}&apikey={1}", UrlEncodeString(longUrl), apiKey));
            request.Method = "GET";
            var response = new OAuthResponse().GetAuthResponse(request);
            var responseData = new ShortenUrlDefaultResponse().Deseriallize(response.ResponseText);
            if (responseData.Messages == "OK")
            {
                //System.Threading.Thread.Sleep(1000 * 60);
                return responseData;
            }

            if (responseData.Messages.Contains("Cannot shorten url for its own domain."))
            {
                // ltu 7/3/14 since we cant bitlify a bitlified message, we need to remove the ?message and stuff
                // ex: "http://ift.tt/1k3wQfy/?messageid=a91875b0-401e-432a-98be-0172316abd3e&cspid=0&socialid=0"
                int index = longUrl.LastIndexOf("/?messageid=", System.StringComparison.Ordinal);
                if (index > 0)
                    longUrl = longUrl.Substring(0, index);
                else
                {
                    index = longUrl.LastIndexOf("?messageid=", System.StringComparison.Ordinal);
                    if (index > 0)
                        longUrl = longUrl.Substring(0, index);
                }
                responseData.ShortenUrl = longUrl;
                responseData.LongUrl = longUrl;
                //System.Threading.Thread.Sleep(1000 * 60);
                return responseData;
            }
            return responseData;
            // throw new Exception("Bitly API: " + responseData.StatusText);
        }

        public ShortenURLClicksResponse GetClicksDailyShortenUrlDefault(string apikey,string url)
        {
            if (url.EndsWith("/"))
            {
                url = url.TrimEnd('/');
            }
            var request = (HttpWebRequest)WebRequest.Create(string.Format("http://tieki.net/s/api/view/?shorturl={0}&apikey={1}", url, apikey));
            request.Method = "GET";
            var response = new OAuthResponse().GetAuthResponse(request);

            var responseData = new ShortenURLClicksResponse().Deseriallize(response.ResponseText);
            if (responseData.Messages != "OK")
            {
                throw new Exception("Bitly API: " + responseData.Messages);
            }
            //throw new Exception("Bitly API: " + responseData.StatusText);

            return responseData;
        }
        #region [Private methods]
        private string UrlEncodeString(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            value = Uri.EscapeDataString(value);

            // UrlEncode escapes with lowercase characters (e.g. %2f) but oAuth needs %2F
            value = Regex.Replace(value, "(%[0-9a-f][0-9a-f])", c => c.Value.ToUpper());

            // these characters are not escaped by UrlEncode() but needed to be escaped
            value = value
                .Replace("(", "%28")
                .Replace(")", "%29")
                .Replace("$", "%24")
                .Replace("!", "%21")
                .Replace("*", "%2A")
                .Replace("'", "%27");

            // these characters are escaped by UrlEncode() but will fail if unescaped!
            value = value.Replace("%7E", "~");

            return value;
        }
        #endregion
    }
}
