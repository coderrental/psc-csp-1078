﻿// ***********************************************************************
// Assembly         : SocialLib
// Author           : VU DINH
// Created          : 08-08-2013
//
// Last Modified By : VU DINH
// Last Modified On : 08-08-2013
// ***********************************************************************
// <copyright file="Common.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.IO;
using System.Net;
using Newtonsoft.Json.Linq;

namespace CR.SocialLib
{
    public static class Common
    {
        public enum Socialtype
        {
            Twitter = 1,
            LinkedIn,
            Facebook
        }

        /// <summary>
        /// Get number of followers of current social user from social.
        /// </summary>
        /// <param name="socialType">Type of the social.</param>
        /// <param name="responseMessage">The profiles response message.</param>
        /// <returns>System.Int32.</returns>
        public static int NumOfFollowers(CR.SocialLib.Common.Socialtype socialType, string responseMessage)
        {
            try
            {
                dynamic obj = JObject.Parse(responseMessage);
                switch (socialType)
                {
                    case Socialtype.Twitter:
                        return int.Parse(obj["followers_count"].ToString());
                    case Socialtype.LinkedIn:
                        return int.Parse(obj["_total"].ToString());
                    case Socialtype.Facebook:
                        {
                            //for facebook, need to request number of friend via fql
                            var userId = long.Parse(obj["id"].ToString());
                            var queryUrl = string.Format("https://graph.facebook.com/fql?q=SELECT friend_count FROM user WHERE uid = {0}",userId);
                            WebRequest request = WebRequest.Create(queryUrl);
                            WebResponse response = request.GetResponse();
                            var stream = response.GetResponseStream();
                            if (stream != null)
                            {
                                var sr = new StreamReader(stream);
                                var responseContent = sr.ReadToEnd();
                                if (!string.IsNullOrEmpty(responseContent)) // content found
                                {
                                    dynamic objRes = JObject.Parse(responseContent);
                                    return int.Parse(objRes["data"][0]["friend_count"].ToString());
                                }
                            }
                        }
                        break;
                }
            }
            catch (Exception)
            {

                return 0;
            }

            return 0;
        }
    }
}
