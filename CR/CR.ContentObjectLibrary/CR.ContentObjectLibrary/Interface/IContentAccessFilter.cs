﻿using System.Collections.Generic;
using CR.ContentObjectLibrary.Data;

namespace CR.ContentObjectLibrary.Interface
{
	public interface IContentAccessFilter
	{
		List<content> Search();
	}
}