﻿using System.Collections.Generic;

namespace CR.ContentObjectLibrary.Interface
{
	public abstract class SearchExpression
	{
		internal char CloseChar = ')', OpenChar = '(';

		public SearchExpression Left;
		public SearchExpression Right;
		public List<SearchExpression> Siblings;
		public List<SearchExpression> Children;
		public SearchOperator Operator;
		public object Value;

		private string _query;

		protected SearchExpression()
		{
			Left = Right = null;
			Operator = SearchOperator.None;
			Value = null;
			Children = new List<SearchExpression>();
			Siblings = new List<SearchExpression>();
		}
	}
}