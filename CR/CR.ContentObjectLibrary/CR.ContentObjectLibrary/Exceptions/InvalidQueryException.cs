﻿using System;

namespace CR.ContentObjectLibrary.Exceptions
{
    public class InvalidQueryException : Exception
    {
        public InvalidQueryException(string message, params object[] parameters) : base(string.Format(message, parameters))
        {
        }
    }
}
