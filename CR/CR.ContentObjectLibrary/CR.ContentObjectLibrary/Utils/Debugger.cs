﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace CR.ContentObjectLibrary.Utils
{
    public class QueueDictionary<TKey, TValue> : Dictionary<TKey, TValue>
    {
        List<TKey> _q = new List<TKey>();
        readonly object _padLock = new object();

        public QueueDictionary() : base() { }
        new public void Add(TKey key, TValue value)
        {
            lock (_padLock)
            {
                base.Add(key, value);
                _q.Add(key);
            }
        }
        public KeyValuePair<TKey, TValue>? Dequeue()
        {
            lock (_padLock)
            {
                if (_q.Count == 0)
                    return null;

                TKey key = _q[_q.Count - 1];
                _q.Remove(key);

                if (base.ContainsKey(key))
                {
                    TValue val = base[key];
                    base.Remove(key);
                    return new KeyValuePair<TKey, TValue>(key, val);
                }
                return null;
            }
        }
    }

    public class ColDebug
    {
        private DateTime _timer;
        private string _message;
        private QueueDictionary<string, DateTime> _dict;
        public ColDebug()
        {
            _timer = DateTime.Now;
            _dict = new QueueDictionary<string, DateTime>();
        }

        public void Start(string message)
        {
            _timer = DateTime.Now;
            _message = message;

            if (_dict.ContainsKey(message))
            {
                // reset timer if key exists
                _dict[message] = DateTime.Now;
            }
            else
            {
                _dict.Add(message, DateTime.Now);
            }
        }

        public void Start()
        {
            Start(Guid.NewGuid().ToString());
        }

        public void Stop()
        {
            KeyValuePair<string, DateTime>? timer = _dict.Dequeue();
            if (timer.HasValue)
            {
                TimeSpan ts = DateTime.Now.Subtract(timer.Value.Value);
                for (int i = 0; i < _dict.Count; i++)
                {
                    Debug.Write("*");
                }
                Debug.WriteLine("[" + timer.Value.Key + "]".PadRight(75 - timer.Value.Key.Length) + string.Format("{0}m {1}s {2}ms ({3})", ts.Minutes, ts.Seconds, ts.Milliseconds, ts.TotalMilliseconds));
            }
            else
            {
                Debug.WriteLine("Can't Dequeue Debug.Queue");
            }
        }
    }
}
