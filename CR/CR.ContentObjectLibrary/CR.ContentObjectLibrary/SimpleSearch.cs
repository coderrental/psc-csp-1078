﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using CR.ContentObjectLibrary.Data;
using CR.ContentObjectLibrary.Exceptions;
using CR.ContentObjectLibrary.Interface;

namespace CR.ContentObjectLibrary
{
	public class SimpleSearch : SearchExpression
	{
		public SimpleSearch() : base()
		{
		}

		public SimpleSearch(object contentType) : base()
		{
			Value = contentType;
		}

		#region Overrides of Object

		public override string ToString()
		{
			string s = OpenChar + "";

			if (Value != null)
			{
				if (Value is content_type)
				{
					s += "cm.content_types_Id = " + (Value as content_type).content_types_Id;
				}
				else if (Value is company)
				{
					s += "cm.supplier_Id = " + (Value as company).companies_Id;
				}
				else if (Value is language)
				{
					s += "c.content_types_languages_Id = " + (Value as language).languages_Id;
				}
				else
				{
					throw new InvalidQueryException("Unknown Value {0}", Value.GetType());
				}
			}
			else
			{
				s += Left.ToString();
				switch (this.Operator)
				{
				case SearchOperator.And:
					s += " and ";
					break;

				case SearchOperator.Or:
					s += " or ";
					break;

				default:
					break;
				}
				s += Right.ToString();
			}

			foreach (SearchExpression exp in Siblings)
			{
				s += exp.ToString();
			}

			return s + CloseChar;
		}
		#endregion
	}
}