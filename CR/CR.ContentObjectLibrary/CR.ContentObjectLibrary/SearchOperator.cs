﻿namespace CR.ContentObjectLibrary
{
	public enum SearchOperator
	{
		None, Equals, Greater, Lesser, And, Or
	}
}