﻿using CR.ContentObjectLibrary.Interface;

namespace CR.ContentObjectLibrary.Factory
{
	public class SearchExpressionFactory
	{
		public static SearchExpression CreateSearch()
		{
			return new SimpleSearch();
		}

		public static SearchExpression CreateSearch(object value)
		{
			return new SimpleSearch(value);
		}
	}
}