﻿using System;
using CR.ContentObjectLibrary.Data;
using CR.ContentObjectLibrary.Interface;

namespace CR.ContentObjectLibrary.Factory
{
	public class ContentAccessFilterFactory
	{
		/// <summary>
		/// Create Content Access Filter
		/// </summary>
		/// <param name="expression">Search Expression</param>
		/// <param name="context">Sql2Linq Data Context</param>
		/// <returns>Content Access Filter</returns>
		public static IContentAccessFilter CreateContentAccessFilter(SearchExpression expression, CspDataContext context)
		{
			return new SimpleContentAccessFilter(expression, context);
		}

		/// <summary>
		/// Create Content Access Filter
		/// </summary>
		/// <param name="expression">Search Expression</param>
		/// <param name="context">Sql2Linq Data Context</param>
		/// <param name="categoryId">Category to search in (int)</param>
		/// <returns>Content Access Filter</returns>
		public static IContentAccessFilter CreateContentAccessFilter(SimpleSearch expression, CspDataContext context, int categoryId)
		{
			SimpleContentAccessFilter caf = new SimpleContentAccessFilter(expression, context);
			caf.CategoryId = categoryId;
			return caf;
		}

		public static IContentAccessFilter CreateContentAccessFilter(SimpleSearch expression, CspDataContext context, int categoryId, bool isExclude)
		{
			SimpleContentAccessFilter caf = new SimpleContentAccessFilter(expression,context);
			caf.CategoryId = categoryId;
			caf.IsExclude = isExclude;
			return caf;
		}
	}
}
