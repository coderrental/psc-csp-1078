﻿using System;
using System.Collections.Generic;
using System.Linq;
using CR.ContentObjectLibrary.Data;
using CR.ContentObjectLibrary.Interface;
using CR.ContentObjectLibrary.Utils;

namespace CR.ContentObjectLibrary
{
	public class SimpleContentAccessFilter : IContentAccessFilter
	{
		private readonly SearchExpression _searchExpression;
		private readonly CspDataContext _context;

		public SimpleContentAccessFilter(SearchExpression searchExpression, CspDataContext context)
		{
			_searchExpression = searchExpression;
			_context = context;
			CategoryId = null;
		}

		public int? CategoryId { get; set; }
		public bool? IsExclude { get; set; }

		#region Implementation of IContentAccessFilter

		public List<content> Search()
		{
			ColDebug debug = new ColDebug();

			string query = @"
declare @categoryIds table (
id int primary key
)
declare @lng int = 1

";

			List<content> data = new List<content>();

			if (CategoryId.HasValue)
			{
				if (IsExclude.HasValue && IsExclude == true)
				{
					query += string.Format(@"
					declare @cats table (id int primary key)
					;with cte(id,pid,l) as
					(
					select c.categoryId,c.parentId,0
					from categories c
					where c.categoryId={0}

					union all

					select c1.categoryId, c1.parentId, cte.l+1
					from cte
					join categories c1 on c1.parentId=cte.id
					)
					insert into @categoryIds(id)
					select a.id
					from cte a	
					order by a.l

					insert into @cats(id) 
					select b.categoryId
					from categories b
					order by b.categoryId
					
					delete from @cats
					where id in (select c.id from @categoryIds c)

					select c.*
					from content_main cm 
					join content c on cm.content_main_Id = c.content_main_Id	
					join content_categories cc on cc.content_main_Id=cm.content_main_Id
					join @cats cid on cid.id=cc.category_Id
					", CategoryId);
				}
				else
				{
					query += string.Format(@"
					;with cte(id,pid,l) as
					(
					select c.categoryId,c.parentId,0
					from categories c
					where c.categoryId={0}

					union all

					select c1.categoryId, c1.parentId, cte.l+1
					from cte
					join categories c1 on c1.parentId=cte.id
					)
					insert into @categoryIds(id)
					select a.id
					from cte a	
					order by a.l

					select c.*
					from content_main cm 
					join content c on cm.content_main_Id = c.content_main_Id	
					join content_categories cc on cc.content_main_Id=cm.content_main_Id
					join @categoryIds cid on cid.id=cc.category_Id
					", CategoryId);
				}
			}
			else
			{

				query += @"
					select c.*
					from content_main cm 
					join content c on cm.content_main_Id = c.content_main_Id	
					left join content_categories cc on cc.content_main_Id=cm.content_main_Id
					";
			}

			query += " where " + _searchExpression.ToString() + "  and c.active = 1";

			debug.Start("Execute query.");
			data = _context.ExecuteQuery<content>(query).ToList();
			debug.Stop();
			return data;
		}

		#endregion
	}
}