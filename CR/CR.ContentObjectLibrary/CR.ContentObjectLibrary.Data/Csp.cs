using System;
using System.Linq;

namespace CR.ContentObjectLibrary.Data
{
    partial class content_category
    {
    }

    partial class content
    {
        /// <summary>
        /// Get content field name
        /// </summary>
        /// <param name="fieldName">field name</param>
        /// <returns>content field is found, if not then null</returns>
        public content_field this[string fieldName]
        {
            get
            {
            	try
            	{
					return content_fields.FirstOrDefault(a => a.content_types_field.fieldname == fieldName);
            	}
            	catch (Exception)
            	{
            		return null;
            	}
            }
        }
    }
}
