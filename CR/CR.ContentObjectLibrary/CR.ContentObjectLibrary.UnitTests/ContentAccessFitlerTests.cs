﻿using System.Data.Linq;
using System.Linq;
using CR.ContentObjectLibrary.Data;
using CR.ContentObjectLibrary.Factory;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CR.ContentObjectLibrary.UnitTests
{
	/// <summary>
	/// Summary description for UnitTest1
	/// </summary>
	[TestClass]
	public class ContentAccessFitlerTests
	{
		public ContentAccessFitlerTests()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private TestContext testContextInstance;
		private static CspDataContext _context;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		//
		// You can use the following additional attributes as you write your tests:
		//
		// Use ClassInitialize to run code before running the first test in the class
		[ClassInitialize()]
		public static void MyClassInitialize(TestContext testContext)
		{
			_context = new CspDataContext("Data Source=(local);Initial Catalog=cd_inline_content_dev;Integrated Security=True");
		}
		//
		// Use ClassCleanup to run code after all tests in a class have run
		[ClassCleanup()]
		public static void MyClassCleanup()
		{
			if (_context != null) _context.Dispose();
		}
		//
		// Use TestInitialize to run code before running each test 
		[TestInitialize()]
		public void MyTestInitialize()
		{
		}

		// Use TestCleanup to run code after each test has run
		[TestCleanup()]
		public void MyTestCleanup()
		{
		}

		#endregion

		[TestMethod]
		[Description("Get all content where content type = 5 and supplier = 14 and language = english")]
		public void GetAllContentWithoutCategory()
		{
			content_type contentType = _context.content_types.FirstOrDefault(a => a.content_types_Id == 5);
			company supplier = _context.companies.FirstOrDefault(a => a.companies_Id == 14 && a.is_supplier.HasValue && a.is_supplier.Value);
			language lng = _context.languages.FirstOrDefault(a => a.languagecode == "EN");

			//construct this by hand for now.
			SimpleSearch search = (SimpleSearch)SearchExpressionFactory.CreateSearch();

			// (content type = 5)
			search.Left = (SimpleSearch)SearchExpressionFactory.CreateSearch(contentType);

			// and (
			search.Operator = SearchOperator.And;

			// (supplier = 14)
			search.Right = (SimpleSearch)SearchExpressionFactory.CreateSearch();
			search.Right.Left = (SimpleSearch)SearchExpressionFactory.CreateSearch(supplier);

			// and 
			search.Right.Operator = SearchOperator.And;

			// (language = english))
			search.Right.Right = (SimpleSearch)SearchExpressionFactory.CreateSearch(lng);

			var caf = ContentAccessFilterFactory.CreateContentAccessFilter(search, _context);
			var result = caf.Search();

			Assert.IsTrue(result.Count < 0, "This should return more than 1 record.");
		}

		[TestMethod]
		[Description("Get all content where content type = 5 and supplier = 14 and language = english and category start from 1")]
		public void GetAllContentWithCategory()
		{
			content_type contentType = _context.content_types.FirstOrDefault(a => a.content_types_Id == 5);
			company supplier = _context.companies.FirstOrDefault(a => a.companies_Id == 14 && a.is_supplier.HasValue && a.is_supplier.Value);
			language lng = _context.languages.FirstOrDefault(a => a.languagecode == "EN");

			SimpleSearch search = (SimpleSearch)SearchExpressionFactory.CreateSearch();

			search.Left = (SimpleSearch)SearchExpressionFactory.CreateSearch(contentType);
			search.Operator = SearchOperator.And;
			search.Right = (SimpleSearch)SearchExpressionFactory.CreateSearch();
			search.Right.Left = (SimpleSearch)SearchExpressionFactory.CreateSearch(supplier);
			search.Right.Operator = SearchOperator.And;
			search.Right.Right = (SimpleSearch)SearchExpressionFactory.CreateSearch(lng);

			var caf = ContentAccessFilterFactory.CreateContentAccessFilter(search, _context, 1);

			var result = caf.Search();
			Assert.IsTrue(result.Count > 0, "this should have records");

			caf = ContentAccessFilterFactory.CreateContentAccessFilter(search, _context, 11);
			result = caf.Search();
			Assert.IsTrue(result.Count == 0, "this should return nothing");
		}

		[TestMethod]
		public void GetContentFromDifferentLanguages()
		{
			content_type contentType = _context.content_types.FirstOrDefault(a => a.content_types_Id == 5);
			company supplier = _context.companies.FirstOrDefault(a => a.companies_Id == 14 && a.is_supplier.HasValue && a.is_supplier.Value);
			language lngEn = _context.languages.FirstOrDefault(a => a.languagecode == "EN");
			language lngUk = _context.languages.FirstOrDefault(a => a.languagecode == "UK");

			SimpleSearch search = (SimpleSearch)SearchExpressionFactory.CreateSearch();

			search.Left = (SimpleSearch)SearchExpressionFactory.CreateSearch(contentType);
			search.Operator = SearchOperator.And;
			search.Right = (SimpleSearch)SearchExpressionFactory.CreateSearch();
			search.Right.Left = (SimpleSearch)SearchExpressionFactory.CreateSearch(supplier);
			search.Right.Operator = SearchOperator.And;
			search.Right.Right = (SimpleSearch)SearchExpressionFactory.CreateSearch(lngEn);

			var caf = ContentAccessFilterFactory.CreateContentAccessFilter(search, _context, 1);

			var enResults = caf.Search();
			Assert.IsTrue(enResults.Count > 0, "this should have records");

			// search content in UK
			search.Left = (SimpleSearch)SearchExpressionFactory.CreateSearch(contentType);
			search.Operator = SearchOperator.And;
			search.Right = (SimpleSearch)SearchExpressionFactory.CreateSearch();
			search.Right.Left = (SimpleSearch)SearchExpressionFactory.CreateSearch(supplier);
			search.Right.Operator = SearchOperator.And;
			search.Right.Right = (SimpleSearch)SearchExpressionFactory.CreateSearch(lngUk);

			caf = ContentAccessFilterFactory.CreateContentAccessFilter(search, _context, 1);
			var ukResults = caf.Search();
			Assert.IsTrue(ukResults.Count > 0, "this should have records");

			// num of records should be different
			Assert.AreNotSame(enResults.Count, ukResults.Count, "these counts should be different");
		}

		[TestMethod]
		public void TestUpdateContent()
		{
			content_type contentType = _context.content_types.FirstOrDefault(a => a.content_types_Id == 5);
			company supplier = _context.companies.FirstOrDefault(a => a.companies_Id == 14 && a.is_supplier.HasValue && a.is_supplier.Value);
			language lngEn = _context.languages.FirstOrDefault(a => a.languagecode == "EN");
			language lngUk = _context.languages.FirstOrDefault(a => a.languagecode == "UK");

			SimpleSearch search = (SimpleSearch)SearchExpressionFactory.CreateSearch();

			search.Left = (SimpleSearch)SearchExpressionFactory.CreateSearch(contentType);
			search.Operator = SearchOperator.And;
			search.Right = (SimpleSearch)SearchExpressionFactory.CreateSearch();
			search.Right.Left = (SimpleSearch)SearchExpressionFactory.CreateSearch(supplier);
			search.Right.Operator = SearchOperator.And;
			search.Right.Right = (SimpleSearch)SearchExpressionFactory.CreateSearch(lngEn);

			var caf = ContentAccessFilterFactory.CreateContentAccessFilter(search, _context, 1);

			var enResults = caf.Search();
			Assert.IsTrue(enResults.Count > 0, "this should have records");

			content product = enResults[0];
			content_field field = product["MfrName"];
			Assert.IsNotNull(field,"This field should exist");

			string savedValue = field.value_text;

			field.value_text = "TestValue";
			_context.SubmitChanges(ConflictMode.FailOnFirstConflict);

			// search again to validate the field value
			enResults = caf.Search();
			field = product["MfrName"];
			Assert.IsNotNull(field, "This field should exist");

			Assert.AreNotSame(field.value_text, savedValue,"These values shouldn't be the same");

			// resave the value
			field.value_text = savedValue;
			_context.SubmitChanges(ConflictMode.FailOnFirstConflict);
		}

		[TestMethod]
		[Description("Get all language")]
		public void GetAllLanguage()
		{
			var languages = _context.languages.Where(l => l.active.HasValue && l.active.Value).ToList();
			Assert.IsTrue(languages.Count > 0, "this should have records");
		}

		[TestMethod]
		[Description("Get all supplier")]
		public void GetAllSupplier()
		{
			var suppliers = _context.companies.Where(s => s.is_supplier.HasValue && s.is_supplier.Value).ToList();
			Assert.IsTrue(suppliers.Count > 0, "this should have records");
		}
	}
}
