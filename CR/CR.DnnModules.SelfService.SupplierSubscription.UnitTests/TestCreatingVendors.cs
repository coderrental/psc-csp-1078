﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace CR.DnnModules.SelfService.SupplierSubscription.UnitTests
{
	[TestClass]
	public class TestCreatingVendors
	{
		private string _dnnUrl = "http://localhost/dnn/0602/selfservice/",
					   _registerPageUrl = "Registernewvendor.aspx";

		private FirefoxDriver _ffDriver;
		private WebDriverWait _wait;
		[TestInitialize]
		public void Init()
		{
			_ffDriver = new FirefoxDriver();
			_wait = new WebDriverWait(_ffDriver, TimeSpan.FromMinutes(5));
		}

		[TestCleanup]
		public void CleanUp()
		{
			_ffDriver.Dispose();
		}

		[TestMethod]
		public void CreateNewVendor()
		{
			_ffDriver.Navigate().GoToUrl(_dnnUrl+_registerPageUrl);

			IWebElement element;
			element = FindElementByNameUsingLike("input", "companyname");
			Assert.IsNotNull(element);
			element.SendKeys("test " + DateTime.Now.Ticks);

			element = FindElementByNameUsingLike("input", "address1");
			Assert.IsNotNull(element);
			element.SendKeys("test address");
		}

		private IWebElement FindElementByNameUsingLike(string tag, string value)
		{
			try
			{
				var el = _wait.Until(d =>
				                     	{
				                     		var temp =
				                     			_ffDriver.FindElements(By.TagName(tag)).Where(e => e.GetAttribute("name").EndsWith(value));
				                     		return temp.FirstOrDefault();
				                     	});
				return el;
			}
			catch(Exception ex)
			{
			}
			return null;
		}
	}
}
