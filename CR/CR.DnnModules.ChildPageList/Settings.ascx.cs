﻿using System;
using System.IO;
using System.Web.UI.WebControls;
using DotNetNuke.Common;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Exceptions;

namespace CR.DnnModules.ChildPageList
{
    partial class Settings : PortalModuleBase
    {
        private const string ParentTab = "ParentTab";
        private const string IncludeSelf = "IncludeSelf";
        private const string IncludeInvisible = "IncludeInvisible";
        private const string ListTemplate = "ListTemplate";
        private const string ColumnPerRow = "ColumnPerRow";
        private const string ListTemplatePath = "Template";
        private const string LinkTarget = "LinkTarget";
        private const string Recursive = "Recursive";
        private const string DisplayIcon = "DisplayIcon";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 1/30/2013 - 11:18 AM
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {

                    ddlParentTab.DataSource = Globals.GetPortalTabs(PortalSettings.DesktopTabs, true, false);
                    ddlParentTab.DataBind();

                    var ctlModule = new ModuleController();

                    //Get parent tab
                    if (ctlModule.GetModuleSettings(ModuleId)[ParentTab] != null && Convert.ToString(ctlModule.GetModuleSettings(ModuleId)[ParentTab]) != "")
                    {
                        string parentTab = Convert.ToString(ctlModule.GetModuleSettings(ModuleId)[ParentTab]);
                        if (ddlParentTab.Items.FindByValue(parentTab) != null)
                        {
                            ddlParentTab.SelectedValue = parentTab;
                        }
                    }
                    else
                    {
                        ddlParentTab.SelectedValue = TabId.ToString();
                    }

                    //Check include self
                    if (ctlModule.GetModuleSettings(ModuleId)[IncludeSelf] != null && ctlModule.GetModuleSettings(ModuleId)[IncludeSelf] != "")
                    {

                        chkIncludeSelf.Checked = Convert.ToBoolean(Convert.ToString(ctlModule.GetModuleSettings(ModuleId)[IncludeSelf]));
                    }

                    //check include invisible
                    if (ctlModule.GetModuleSettings(ModuleId)[IncludeInvisible] != null && ctlModule.GetModuleSettings(ModuleId)[IncludeInvisible] != "")
                    {

                        chkIncludeInvisible.Checked = Convert.ToBoolean(Convert.ToString(ctlModule.GetModuleSettings(ModuleId)[IncludeInvisible]));
                    }

                    //check recursive option
                    if (ctlModule.GetModuleSettings(ModuleId)[Recursive] != null && ctlModule.GetModuleSettings(ModuleId)[Recursive] != "")
                    {
                        chkRecursive.Checked = Convert.ToBoolean(Convert.ToString(ctlModule.GetModuleSettings(ModuleId)[Recursive]));
                    }



                    //check displayicon option
                    if (ctlModule.GetModuleSettings(ModuleId)[DisplayIcon] != null && ctlModule.GetModuleSettings(ModuleId)[DisplayIcon] != "")
                    {
                        chkDisplayIcon.Checked = Convert.ToBoolean(Convert.ToString(ctlModule.GetModuleSettings(ModuleId)[DisplayIcon]));
                    }




                    //Check columns per row
                    if (ctlModule.GetModuleSettings(ModuleId)[ColumnPerRow] != null && ctlModule.GetModuleSettings(ModuleId)[ColumnPerRow] != "")
                    {
                        txtColumnCount.Text = Convert.ToString(ctlModule.GetModuleSettings(ModuleId)[ColumnPerRow]);
                    }

                    //Check list template
                    string listTemplatePath = "List.ascx";
                    if (ctlModule.GetModuleSettings(ModuleId)[ListTemplate] != null && ctlModule.GetModuleSettings(ModuleId)[ListTemplate] != "")
                    {
                        listTemplatePath = Convert.ToString(ctlModule.GetModuleSettings(ModuleId)[ListTemplate]);
                    }

                    ddlListTemplate = FillSubDirectoryTemplate(ddlListTemplate, Request.MapPath(ModulePath + ListTemplatePath), listTemplatePath, "*.ascx");

                    //Check link target
                    if (ctlModule.GetModuleSettings(ModuleId)[LinkTarget] != null && Convert.ToString(ctlModule.GetModuleSettings(ModuleId)[LinkTarget]) != "")
                    {
                        string linkTarget = Convert.ToString(ctlModule.GetModuleSettings(ModuleId)[LinkTarget]);
                        if (ddlLinkTarget.Items.FindByValue(linkTarget) != null)
                        {
                            ddlLinkTarget.SelectedValue = linkTarget;
                        }
                    }

                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }


        /// <summary>
        /// Handles the Click event of the cmdUpdate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 1/30/2013 - 11:18 AM
        protected void CmdUpdateClick(object sender, EventArgs e)
        {
            var ctlModule = new ModuleController();

            ctlModule.UpdateModuleSetting(ModuleId, ParentTab, ddlParentTab.SelectedValue);
            ctlModule.UpdateModuleSetting(ModuleId, IncludeSelf, chkIncludeSelf.Checked.ToString());
            ctlModule.UpdateModuleSetting(ModuleId, IncludeInvisible, chkIncludeInvisible.Checked.ToString());

            ctlModule.UpdateModuleSetting(ModuleId, Recursive, chkRecursive.Checked.ToString());

            ctlModule.UpdateModuleSetting(ModuleId, DisplayIcon, chkDisplayIcon.Checked.ToString());

            ctlModule.UpdateModuleSetting(ModuleId, ColumnPerRow, txtColumnCount.Text);
            ctlModule.UpdateModuleSetting(ModuleId, ListTemplate, ddlListTemplate.SelectedValue);
            ctlModule.UpdateModuleSetting(ModuleId, LinkTarget, ddlLinkTarget.SelectedValue);

            Response.Redirect(Globals.NavigateURL(TabId), true);
        }

        /// <summary>
        /// Handles the Click event of the cmdReturn control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 1/30/2013 - 11:18 AM
        protected void CmdReturnClick(Object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Globals.NavigateURL(TabId), true);
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }

        #region Base Method Implementations



        /// <summary>
        /// Fills the sub directory template.
        /// </summary>
        /// <param name="ddlTemplate">The DDL template.</param>
        /// <param name="path">The path.</param>
        /// <param name="selectedValue">The selected value.</param>
        /// <param name="fileExtension">The file extension.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 1/30/2013 - 11:18 AM
        private DropDownList FillSubDirectoryTemplate(DropDownList ddlTemplate, String path, string selectedValue, string fileExtension)
        {
            if (Directory.Exists(path))
            {
                string[] files = Directory.GetFiles(path, fileExtension);
                foreach (string file in files)
                {
                    var item = new ListItem
                                   {
                                       Text = Path.GetFileNameWithoutExtension(file), 
                                       Value = Path.GetFileName(file)
                                   };
                    if (item.Value == selectedValue)
                    {
                        item.Selected = true;
                    }
                    ddlTemplate.Items.Add(item);
                }
            }
            return ddlTemplate;
        }

        #endregion
    }
}