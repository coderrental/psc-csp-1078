﻿using System;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;

namespace CR.DnnModules.ChildPageList
{
    partial class View : PortalModuleBase, IActionable
    {
        private const string ListTemplate = "ListTemplate";

        #region Event Handlers
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 1/30/2013 - 11:16 AM
        protected void Page_Load(Object sender, EventArgs e)
        {
            try
            {
                //Check list template
                string listTemplatePath = "List.ascx";

                var ctlModule = new ModuleController();
                if (ctlModule.GetModuleSettings(ModuleId)[ListTemplate] != null && (string) ctlModule.GetModuleSettings(ModuleId)[ListTemplate] != "")
                {
                    listTemplatePath = Convert.ToString(ctlModule.GetModuleSettings(ModuleId)[ListTemplate]);
                }

                var objListTemplate = (PortalModuleBase)LoadControl("Template/" + listTemplatePath);
                objListTemplate.ModuleConfiguration = ModuleConfiguration;
                phTemplate.Controls.Add(objListTemplate);

            }
            catch (Exception exc)
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }

        }


        #endregion

        #region IActionable

        /// <summary>
        /// Gets the module actions.
        /// </summary>
        /// Author: Vu Dinh
        /// 1/30/2013 - 11:16 AM
        public ModuleActionCollection ModuleActions
        {
            get
            {
                var actions = new ModuleActionCollection
                                  {
                                      {
                                          GetNextActionID(),
                                          Localization.GetString(ModuleActionType.AddContent, LocalResourceFile),
                                          ModuleActionType.AddContent, "", "", EditUrl(), false,
                                          SecurityAccessLevel.Edit, true, false
                                          }
                                  };
                return actions;
            }
        }

        #endregion
    }
}