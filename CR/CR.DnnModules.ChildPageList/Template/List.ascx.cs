﻿using System;
using System.Collections;
using System.Web.UI.WebControls;
using System.IO;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Tabs;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using Telerik.Web.UI;

namespace CR.DnnModules.ChildPageList.Template
{
    partial class List : PortalModuleBase
    {
        private const string ParentTab = "ParentTab";
        private const string IncludeSelf = "IncludeSelf";
        private const string IncludeInvisible = "IncludeInvisible";
        private const string Link_Target = "LinkTarget";
        private const string Recursive = "Recursive";
        private const string DisplayIcon = "DisplayIcon";

        #region public members

        /// <summary>
        /// Gets the link target.
        /// </summary>
        /// Author: Vu Dinh
        /// 1/30/2013 - 11:10 AM
        public string LinkTarget
        {
            get
            {
                var ctlModule = new ModuleController();
                if (ctlModule.GetModuleSettings(ModuleId)[Link_Target] != null && (string) ctlModule.GetModuleSettings(ModuleId)[Link_Target] != "")
                {
                    return Convert.ToString(ctlModule.GetModuleSettings(ModuleId)[Link_Target]);
                }
                return "_self";
            }
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 1/30/2013 - 11:11 AM
        protected void Page_Load(Object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptInclude("jbackgroundanimate", ControlPath + "js/jcoloranimated.js");
            try
            {
                LocalResourceFile = Localization.GetResourceFile(this, "List.ascx");

                var ctlModule = new ModuleController();

                int parentTab = TabId;//Default value

                if (ctlModule.GetModuleSettings(ModuleId)[ParentTab] != null && Convert.ToString(ctlModule.GetModuleSettings(ModuleId)[ParentTab]) != "")
                {
                    parentTab = Convert.ToInt32(ctlModule.GetModuleSettings(ModuleId)[ParentTab]);
                }


                var ctlTab = new TabController();
                TabInfo objParent = ctlTab.GetTab(parentTab);

                if ((parentTab != Null.NullInteger) && (objParent == null || !objParent.HasChildren))
                {
                    return;
                }

                //Check include self
                bool includeSelf = false;
                if (ctlModule.GetModuleSettings(ModuleId)[IncludeSelf] != null && (string) ctlModule.GetModuleSettings(ModuleId)[IncludeSelf] != "")
                {
                    includeSelf = Convert.ToBoolean(ctlModule.GetModuleSettings(ModuleId)[IncludeSelf]);
                }

                //check include invisible
                bool includeInvisible = false;
                if (ctlModule.GetModuleSettings(ModuleId)[IncludeInvisible] != null && (string) ctlModule.GetModuleSettings(ModuleId)[IncludeInvisible] != "")
                {
                    includeInvisible = Convert.ToBoolean(ctlModule.GetModuleSettings(ModuleId)[IncludeInvisible]);
                }

                //check recursive
                bool isRecursive = false;
                if (ctlModule.GetModuleSettings(ModuleId)[Recursive] != null && (string) ctlModule.GetModuleSettings(ModuleId)[Recursive] != "")
                {
                    isRecursive = Convert.ToBoolean(ctlModule.GetModuleSettings(ModuleId)[Recursive]);
                }

                //check display icon
                bool isDisplayIcon = false;
                if (ctlModule.GetModuleSettings(ModuleId)[DisplayIcon] != null && (string) ctlModule.GetModuleSettings(ModuleId)[DisplayIcon] != "")
                {
                    isDisplayIcon = Convert.ToBoolean(ctlModule.GetModuleSettings(ModuleId)[DisplayIcon]);
                }

                var arrTab = new ArrayList();

                if (isRecursive)
                {
                    arrTab = GetChildPageList(parentTab,
                                              parentTab == Null.NullInteger ? -1 : objParent.Level,
                                              includeSelf, includeInvisible, isDisplayIcon);
                }
                else
                {
                    var tabList = new ArrayList();
                    if (parentTab == Null.NullInteger)
                    {
                        foreach (TabInfo tabInfo in ctlTab.GetTabs(PortalId))
                        {
                            if (tabInfo.Level == 0 && tabInfo.ParentId == -1)
                            {
                                tabList.Add(tabInfo);
                            }
                        }
                    }
                    else
                    {
                        tabList = ctlTab.GetTabsByParentId(parentTab);
                    }
                    foreach (TabInfo tabInfo in tabList)
                    {
                        if (PortalSecurity.IsInRoles(tabInfo.AuthorizedRoles) && (!tabInfo.IsDeleted) && (!tabInfo.DisableLink))
                        {
                            if (tabInfo.IsVisible || includeInvisible)
                            {
                                if (tabInfo.TabID != TabId || includeSelf)
                                {
                                    TabInfo childTab = tabInfo.Clone();
                                    string iconUrl = "";
                                    if (isDisplayIcon)
                                    {
                                        if (tabInfo.IconFile == string.Empty)
                                        {
                                            iconUrl = string.Format("<img src='{0}' border='0' width='16px' height='16px'/>", Page.ResolveUrl("~/images/icon_unknown_16px.gif"));
                                        }
                                        else
                                        {
                                            iconUrl = string.Format("<img src='{0}' border='0' width='16px' height='16px'/>", Page.ResolveUrl(tabInfo.IconFile));
                                        }
                                    }
                                    childTab.TabName = iconUrl + tabInfo.TabName;
                                    arrTab.Add(childTab);
                                }
                            }

                        }
                    }
                }
                if (arrTab.Count > 0)
                {
                    subTabList.DataSource = arrTab;
                    subTabList.DataBind();

                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }

        }

        /// <summary>
        /// Gets the image thubnail.
        /// </summary>
        /// <param name="tabName">Name of the tab.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 2/6/2013 - 11:40 PM
        public string GetImageThubnail(string tabName)
        {
            //Kiem tra co link folder trong webconfig hay ko, neu ko co tra ve string.empty
            //search img thumbnail with tabname
            //if (hasThumbnail)
            if (System.Configuration.ConfigurationSettings.AppSettings["TilesThubmailFolderName"] != null)
            {
                string tilesThubnailFolderName = string.Format(System.Configuration.ConfigurationSettings.AppSettings["TilesThubmailFolderName"]);
                if (!string.IsNullOrEmpty(tilesThubnailFolderName))
                {
                    DirectoryInfo dir = new DirectoryInfo(Server.MapPath(tilesThubnailFolderName)); //tilesThubmailFolderName = ~TileThumbnail
                    foreach (FileInfo file in dir.GetFiles("*.*"))
                    {
                        string result = Path.GetFileNameWithoutExtension(file.Name);
                        if (result.Equals(tabName))
                        {
                            var imgLink = string.Format("background-image: url('{0}'); background-color:none; background-size:160px 122px;", Page.ResolveUrl(tilesThubnailFolderName + "/" + file.Name));
                            return imgLink;
                        }
                    }
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Gets the child page list.
        /// </summary>
        /// <param name="parentTabId">The parent tab id.</param>
        /// <param name="parentTabLevel">The parent tab level.</param>
        /// <param name="includeSelf">if set to <c>true</c> [include self].</param>
        /// <param name="includeInvisible">if set to <c>true</c> [include invisible].</param>
        /// <param name="isDisplayIcon">if set to <c>true</c> [is display icon].</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 1/30/2013 - 11:14 AM
        private ArrayList GetChildPageList(int parentTabId, int parentTabLevel, bool includeSelf, bool includeInvisible, bool isDisplayIcon)
        {
            var arrTab = new ArrayList();
            var tabList = new ArrayList();
            var ctlTab = new TabController();
            if (parentTabId == Null.NullInteger)
            {
                foreach (TabInfo tabInfo in ctlTab.GetTabs(PortalId))
                {
                    if (tabInfo.Level == 0 && tabInfo.ParentId == -1)
                    {
                        tabList.Add(tabInfo);
                    }
                }
            }
            else
            {
                tabList = ctlTab.GetTabsByParentId(parentTabId);
            }

            if (parentTabId != Null.NullInteger)
            {
                TabInfo objParent = ctlTab.GetTab(parentTabId);
                if (objParent == null || !objParent.HasChildren)
                {
                    return arrTab;
                }
            }

            foreach (TabInfo tabInfo in tabList)
            {
                if (PortalSecurity.IsInRoles(tabInfo.AuthorizedRoles) && (!tabInfo.IsDeleted) && (!tabInfo.DisableLink))
                {
                    if (tabInfo.IsVisible || includeInvisible)
                    {
                        if (tabInfo.TabID != TabId || includeSelf)
                        {
                            TabInfo childTab = tabInfo.Clone();
                            int levelDiff = tabInfo.Level - parentTabLevel;
                            string prefix = "";
                            for (int i = 2; i < levelDiff; i++)
                            {
                                prefix += string.Format("<img src='{0}' border='0'/>", ControlPath + "images/line.gif");
                            }
                            if (levelDiff > 1)
                            {
                                prefix += string.Format("<img src='{0}' border='0'/>", ControlPath + "images/node.gif");
                            }

                            string iconUrl = "";
                            if (isDisplayIcon)
                            {
                                if (tabInfo.IconFile == string.Empty)
                                {
                                    iconUrl = string.Format("<img src='{0}' border='0' width='16px' height='16px'/>", Page.ResolveUrl("~/images/icon_unknown_16px.gif"));
                                }
                                else
                                {
                                    iconUrl = string.Format("<img src='{0}' border='0' width='16px' height='16px'/>", Page.ResolveUrl(tabInfo.IconFile));
                                }
                            }
                            childTab.TabName = prefix + iconUrl + tabInfo.TabName;
                            arrTab.Add(childTab);
                            ArrayList subTabArr = GetChildPageList(tabInfo.TabID, parentTabLevel, includeSelf, includeInvisible, isDisplayIcon);
                            foreach (object subTabItem in subTabArr)
                            {
                                arrTab.Add(subTabItem);
                            }

                        }
                    }

                }

            }

            return arrTab;
        }

        #endregion
    }
}