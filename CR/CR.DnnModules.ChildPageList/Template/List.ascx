﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="CR.DnnModules.ChildPageList.Template.List" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadListView ID="subTabList" runat="server" ItemPlaceholderID="pnCrosslink">
    <LayoutTemplate>
        <asp:Panel id="pnCrosslink" runat="server"></asp:Panel>
    </LayoutTemplate>
    <ItemTemplate>
        <div class="crosslink_container">
            <a href='<%#DotNetNuke.Common.Globals.NavigateURL(Convert.ToInt32(Eval("TabID"))) %>' class="cross_link" style="<%# GetImageThubnail(Convert.ToString(Eval("TabName")))%>">
                <%# Server.HtmlDecode(Convert.ToString(Eval("TabName"))) %>
            </a>
		</div>
    </ItemTemplate>
</telerik:RadListView>
<div class="clear"></div>
<script type="text/javascript">
    jQuery(function () {
        jQuery('.crosslink_container').hover(function () {
            jQuery(this).stop().animate({ borderColor: "#AAAAAA" });
        }, function () {
            jQuery(this).stop().animate({ borderColor: "#CCCCCC" });
        });
    });
</script>
