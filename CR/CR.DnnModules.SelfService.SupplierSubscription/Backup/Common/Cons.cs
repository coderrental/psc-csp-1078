﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CR.DnnModules.SelfService.SupplierSubscription.Common
{
	public class Cons
	{
		public const string SETTING_CSPMASTER_CONNECTIONSTRING = "SettingCspMasterConnectionString";
		public const string SETTING_CSPDNN_CONNECTIONSTRING = "SettingCspDnnConnectionString";
		public const string SETTING_EXTERNAL_IDENTIFIER = "SettingExternalIdentifier";
		public const string SETTING_BASE_DOMAIN = "SettingBaseDomain";
		public const string SETTING_DEFAULT_PARAMETERS = "SettingDefaultParameters";
		public const string SETTING_BASE_PUBLICATION_ID = "SettingBasePublicationId";
		//public const string SETTING_ALIAS_VALUE = "SettingAliasValue";
		public const string SETTING_DEFAULT_CATEGORY_ID = "SettingDefaultCategoryId";
		public const string SETTING_MASTER_ENCODED_CONNECTION = "SettingMasterEncodedConnection";
		public const string SETTING_MASTER_CONNECTION_MODE = "SettingMasterConnectionMode";
	    public const string SETTING_SELF_SERVICE_ROLE_NAME = "SettingSelfSeriveRoleName";
		public const string SETTING_ADMIN_EMAIL = "SettingAdminEmail";
		public const string SETTING_CSP_RESET_URL = "SettingCspResetUrl";
        public const string SETTING_EFFECTIVE_DAY = "SettingEffectiveDay";
        public const string SETTING_RECAPTCHA_PUBLIC_KEY = "SettingRecaptchaPublicKey";
        public const string SETTING_RECAPTCHA_PRIVATE_KEY = "SettingRecaptchaPrivateKey";
		public const string SETTING_REDIRECT_URL = "SettingRedirectUrl";
		public const string SETTING_CHANNEL_BASE_DOMAIN = "SettingChannelBaseDomain";
		public const string SETTING_WEBTRENDS_FOLDER = "SettingWebtrendsFolder";
		public const string SETTING_CONTENT_TYPE_ID = "SettingContentTypeId";
		public const string SETTING_CONTENT_TYPE_FIELD_ID = "SettingContentTypeFieldId";
		public const string SETTING_BRAINTREE_MERCHANT_ID = "SettingBraintreeMerchantId";
		public const string SETTING_BRAINTREE_PUBLIC_KEY = "SettingBraintreePublicKey";
		public const string SETTING_BRAINTREE_PRIVATE_KEY = "SettingBraintreePrivateKey";
		public const string SETTING_BRAINTREE_CLIENT_SIDE_ENCRYPTION_KEY = "SettingBraintreeClientSideEncryptionKey";
		public const string SETTING_BRAINTREE_PLAN_ID = "SettingBraintreeId";
		public const string SETTING_GENERAL_SUPPLIER_ID = "SettingGeneralSupplierId";
	}
}