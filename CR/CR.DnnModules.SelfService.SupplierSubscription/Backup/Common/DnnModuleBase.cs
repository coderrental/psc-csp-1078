﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CR.ContentObjectLibrary.Data;
using CR.ContentObjectLibrary.Data.CspMaster;
using CR.DnnModules.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;
using DotNetNuke.Services.Log.EventLog;

namespace CR.DnnModules.SelfService.SupplierSubscription.Common
{
	public class DnnModuleBase : PortalModuleBase
	{
		protected EventLogController DnnEventLog;
		protected ModuleActionCollection MyActions;
		private string _localResourceFile;
		protected CspDataContext CspDataContext;
		protected CspMasterDataContext CspMasterDataContext;
		protected DnnDataContext DnnData;
		protected int _cspId;

		/// <summary>
		/// Handles the Init event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected virtual void Page_Init(object sender, EventArgs e)
		{
			//Init Local Resource file
			_localResourceFile = Utils.GetCommonResourceFile("CR.DnnModules.SelfService.SupplierSubscription", LocalResourceFile);
			//Init module title
			ModuleConfiguration.ModuleTitle = GetLocalizedText("ModuleTitle");
			//Init csp data context
			CspDataContext = new CspDataContext(Utils.GetConnectionString(PortalId));

			if (Settings.ContainsKey(Cons.SETTING_CSPDNN_CONNECTIONSTRING) && !String.IsNullOrEmpty(Settings[Cons.SETTING_CSPDNN_CONNECTIONSTRING].ToString()))
				CspDataContext = new CspDataContext(Config.GetConnectionString(Settings[Cons.SETTING_CSPDNN_CONNECTIONSTRING].ToString()));
			if (Settings.ContainsKey(Cons.SETTING_CSPMASTER_CONNECTIONSTRING) && !String.IsNullOrEmpty(Settings[Cons.SETTING_CSPMASTER_CONNECTIONSTRING].ToString()))
				CspMasterDataContext = new CspMasterDataContext(Config.GetConnectionString(Settings[Cons.SETTING_CSPMASTER_CONNECTIONSTRING].ToString()));

			DnnData = new DnnDataContext(Config.GetConnectionString());
			_cspId = Utils.GetIntegrationKey(UserInfo);
			if (_cspId < 0) return;
		}

		/// <summary>
		/// Get localized text for the current portal language
		/// </summary>
		/// <param name="key">Resource Key</param>
		/// <returns>Localized text</returns>
		public string GetLocalizedText(string key)
		{
			return Localization.GetString(key, _localResourceFile);
		}

		/// <summary>
		/// Write message to Dnn event view
		/// </summary>
		/// <param name="message">message to write</param>
		public void Log(string message)
		{
			DnnEventLog.AddLog("DEBUG", message, PortalSettings, UserId, EventLogController.EventLogType.ADMIN_ALERT);
		}

		/// <summary>
		/// Gets the content field value.
		/// </summary>
		/// <param name="contentPage">The content page.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns></returns>
		/// <author>ducuytran - 2/27/2013</author>
		public string GetContentFieldValue(content contentPage, string fieldName)
		{
			if (contentPage == null)
				return string.Empty;

			content_field cf = contentPage[fieldName];
			return (cf != null) ? (string.IsNullOrEmpty(cf.value_text) ? string.Empty : cf.value_text) : string.Empty;
		}

		/// <summary>
		/// Haves the admin permission.
		/// </summary>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>5/22/2013</datetime>
		public bool HaveAdminPermission()
		{
			if (UserInfo.IsSuperUser || UserInfo.IsInRole(PortalSettings.AdministratorRoleName))
				return true;
			return false;
		}
	}
}