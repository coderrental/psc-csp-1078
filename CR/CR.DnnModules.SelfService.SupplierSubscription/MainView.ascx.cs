﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Linq;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.ContentObjectLibrary.Data;
using CR.ContentObjectLibrary.Data.CspMaster;
using CR.DnnModules.SelfService.SupplierSubscription.Common;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Data;
using DotNetNuke.Entities.Profile;
using DotNetNuke.Entities.Users;
using DotNetNuke.Security.Membership;
using DotNetNuke.Security.Roles;
using DotNetNuke.Services.Mail;
using Braintree;
using Recaptcha;
using Telerik.Web.UI;
namespace CR.DnnModules.SelfService.SupplierSubscription
{
	public partial class MainView : DnnModuleBase
	{
		private bool _isSet = true;
		protected string ExternalIdentifier, BaseDomain, DefaultParameters, MasterAliasValue, SelfServiceRoleName, CspResetUrl, RcPublicKey, RcPrivateKey, RedirectUrl, ChannelBaseDomain, WebtrendsFolder, AdminEmail, CspProjectName, Isvalidtext, MessagePayment, Strbillingaddress, BraintreeMerchantId, BraintreePublicKey, BraintreePrivateKey, BraintreeClientSideEncryptionKey, BraintreePlanId,GeneralSupplierIds;
		protected int BasePublicationId, DefaultCategoryId,EffectiveDay,ContentTypeId,ContentTypeFieldId;
		protected company NewCompany;
		protected Channel NewChannel;
		protected theme NewTheme;
		protected Regex BaseDomainRegExp = new Regex("[^a-zA-Z0-9.]");
		protected BraintreeGateway gateway;

		protected override void Page_Init(object sender, EventArgs e)
		{
			base.Page_Init(sender, e);
			InitSettings();
		    InitCountry();
			InitExpirationDate();
		}
		protected void Page_Load(object sender, EventArgs e)
		{
			ConfirmPane.Visible = false;
			if (!_isSet)
			{
				MainPane.Visible = false;
				PageMessage.InnerHtml = GetLocalizedText("PageMessage.MissingModuleSettings");
				PageMessage.Visible = true;
				return;
			}
			
			if (Request.Headers["X-OFFICIAL-REQUEST"] == "TRUE")
			{
				if (Request.Params["action"] == "username_validation")
				{
					ValidateUsername();
				}
				else if (Request.Params["action"] == "companyname_validation")
					ValidateCompanyname();
				else if (Request.Params["action"] == "basedomain_validation")
					ValidateBaseDomain();
                else if (Request.Params["btn-email-clicked"] == "1")
					GoEmailToSupplier();
				else
				{
					SaveCompany();
				}
				return;
			}
			if (!IsPostBack)
			{
				recaptcha.PrivateKey = RcPrivateKey;
				recaptcha.PublicKey = RcPublicKey;
			}
			lblResult.Visible = false;
			Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel='stylesheet' href='{0}css/kendo.common.min.css' type='text/css' />", ControlPath)));
			Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel='stylesheet' href='{0}css/kendo.metro.min.css' type='text/css' />", ControlPath)));
			Page.ClientScript.RegisterClientScriptInclude("kendo.web.min", ControlPath + "js/kendo.web.min.js");
			Page.ClientScript.RegisterClientScriptInclude("braintree", ControlPath + "js/braintree.js");
		}

		private string ReplaceText(string text)
		{
			if (string.IsNullOrEmpty(text))
				return string.Empty;

			text = text.Replace("[CompanyName]", Server.HtmlEncode(companyname.Value));
			text = text.Replace("[Address]", Server.HtmlEncode(address1.Value));
			text = text.Replace("[City]", Server.HtmlEncode(city.Value));
			text = text.Replace("[State]", Server.HtmlEncode(state.Value));
			text = text.Replace("[Zipcode]", Server.HtmlEncode(zipcode.Value));
			text = text.Replace("[Country]", Server.HtmlEncode(rcbCountry.Value));
			text = text.Replace("[URL]", Server.HtmlEncode(companywebsite.Value));
			text = text.Replace("[FirstName]", Server.HtmlEncode(firstname.Value));
			text = text.Replace("[LastName]", Server.HtmlEncode(lastname.Value));
			text = text.Replace("[JobTitle]", Server.HtmlEncode(jobtitle.Value));
			text = text.Replace("[PhoneNumber]", Server.HtmlEncode(phonenumber.Value));
			text = text.Replace("[EmailAddress]", Server.HtmlEncode(emailaddress.Value));
			text = text.Replace("[Username]", Server.HtmlEncode(username.Value));
			text = text.Replace("[Password]", password.Value);
			text = text.Replace("[BaseDomain]", BaseDomainRegExp.Replace(tbxBaseDomain.Value, ""));
			return text;
		}

		private void GoEmailToSupplier()
		{
			string templateEmail = GetLocalizedText("Template.Email");
			string templateEmailPassword = GetLocalizedText("Template.EmailAboutPassword");
			if (String.IsNullOrEmpty(templateEmail) || String.IsNullOrEmpty(templateEmailPassword))
			{
				ControlMessage.InnerText = GetLocalizedText("Msg.MissingEmailTemplate");
				return;
			}
			string passwordFailedMsg = "";
			templateEmail = ReplaceText(templateEmail);
			templateEmailPassword = ReplaceText(templateEmailPassword);
			
			String firstMailResult = "";
			String secondMailResult = "";
			string emailSubject = ReplaceText(GetLocalizedText("SubjectEmail.Text"));

			firstMailResult = Mail.SendMail(Settings[Cons.SETTING_ADMIN_EMAIL].ToString(), emailaddress.Value, Settings[Cons.SETTING_ADMIN_EMAIL].ToString(), emailSubject, templateEmail, "", "HTML", "", "", "", "");
			if (String.IsNullOrEmpty(firstMailResult))
				secondMailResult = Mail.SendMail(Settings[Cons.SETTING_ADMIN_EMAIL].ToString(), emailaddress.Value, Settings[Cons.SETTING_ADMIN_EMAIL].ToString(), emailSubject, templateEmailPassword, "", "HTML", "", "", "", "");
            
			if (String.IsNullOrEmpty(firstMailResult) && String.IsNullOrEmpty(secondMailResult))
			{
				passwordFailedMsg = "";
			}
            else
            {
				if (firstMailResult != secondMailResult)
				{
					if (!String.IsNullOrEmpty(firstMailResult))
						passwordFailedMsg += GetLocalizedText("AccountInfomationEmail.Text") + firstMailResult;
					if (!String.IsNullOrEmpty(secondMailResult))
						passwordFailedMsg += GetLocalizedText("PasswordInfomationEmail.Text") + secondMailResult;
				}
				else
				{
					passwordFailedMsg = firstMailResult;
				}
            }
			if (!String.IsNullOrEmpty(passwordFailedMsg))
			{
				ControlMessage.InnerText = passwordFailedMsg;
			}
		}

		/// <summary>
		/// Inits the settings.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>5/31/2013</datetime>
		private void InitSettings()
		{
			if (!Settings.ContainsKey(Cons.SETTING_BASE_DOMAIN) || !Settings.ContainsKey(Cons.SETTING_BASE_PUBLICATION_ID) 
				|| !Settings.ContainsKey(Cons.SETTING_DEFAULT_PARAMETERS) || !Settings.ContainsKey(Cons.SETTING_EXTERNAL_IDENTIFIER)
				|| !Settings.ContainsKey(Cons.SETTING_CSPMASTER_CONNECTIONSTRING) || !Settings.ContainsKey(Cons.SETTING_ADMIN_EMAIL)
				|| !Settings.ContainsKey(Cons.SETTING_CSPDNN_CONNECTIONSTRING) || !Settings.ContainsKey(Cons.SETTING_DEFAULT_CATEGORY_ID)
				|| !Settings.ContainsKey(Cons.SETTING_MASTER_CONNECTION_MODE) || !Settings.ContainsKey(Cons.SETTING_MASTER_ENCODED_CONNECTION)
                || !Settings.ContainsKey(Cons.SETTING_SELF_SERVICE_ROLE_NAME) || !Settings.ContainsKey(Cons.SETTING_CSP_RESET_URL) || !Settings.ContainsKey(Cons.SETTING_EFFECTIVE_DAY) 
                || !Settings.ContainsKey(Cons.SETTING_RECAPTCHA_PUBLIC_KEY) || !Settings.ContainsKey(Cons.SETTING_RECAPTCHA_PRIVATE_KEY)
				|| !Settings.ContainsKey(Cons.SETTING_REDIRECT_URL) || !Settings.ContainsKey(Cons.SETTING_CHANNEL_BASE_DOMAIN) || !Settings.ContainsKey(Cons.SETTING_WEBTRENDS_FOLDER)
				|| !Settings.ContainsKey(Cons.SETTING_CONTENT_TYPE_ID) || !Settings.ContainsKey(Cons.SETTING_CONTENT_TYPE_FIELD_ID) || !Settings.ContainsKey(Cons.SETTING_GENERAL_SUPPLIER_ID)
				|| !Settings.ContainsKey(Cons.SETTING_BRAINTREE_MERCHANT_ID) || !Settings.ContainsKey(Cons.SETTING_BRAINTREE_PUBLIC_KEY)
				|| !Settings.ContainsKey(Cons.SETTING_BRAINTREE_PRIVATE_KEY) || !Settings.ContainsKey(Cons.SETTING_BRAINTREE_CLIENT_SIDE_ENCRYPTION_KEY)
				|| !Settings.ContainsKey(Cons.SETTING_BRAINTREE_PLAN_ID))
			{
				_isSet = false;
				return;
			}
			if (String.IsNullOrEmpty(Settings[Cons.SETTING_BASE_DOMAIN].ToString()) || String.IsNullOrEmpty(Settings[Cons.SETTING_BASE_PUBLICATION_ID].ToString())
				|| String.IsNullOrEmpty(Settings[Cons.SETTING_DEFAULT_PARAMETERS].ToString()) || String.IsNullOrEmpty(Settings[Cons.SETTING_ADMIN_EMAIL].ToString())
				|| String.IsNullOrEmpty(Settings[Cons.SETTING_CSPDNN_CONNECTIONSTRING].ToString()) || String.IsNullOrEmpty(Settings[Cons.SETTING_CSPDNN_CONNECTIONSTRING].ToString())
				|| String.IsNullOrEmpty(Settings[Cons.SETTING_CSPMASTER_CONNECTIONSTRING].ToString()) || String.IsNullOrEmpty(Settings[Cons.SETTING_DEFAULT_CATEGORY_ID].ToString())
				|| String.IsNullOrEmpty(Settings[Cons.SETTING_MASTER_CONNECTION_MODE].ToString()) || String.IsNullOrEmpty(Settings[Cons.SETTING_MASTER_ENCODED_CONNECTION].ToString())
                || String.IsNullOrEmpty(Settings[Cons.SETTING_SELF_SERVICE_ROLE_NAME].ToString()) || String.IsNullOrEmpty(Settings[Cons.SETTING_CSP_RESET_URL].ToString())
                || String.IsNullOrEmpty(Settings[Cons.SETTING_EFFECTIVE_DAY].ToString()) || String.IsNullOrEmpty(Settings[Cons.SETTING_RECAPTCHA_PUBLIC_KEY].ToString())
                || String.IsNullOrEmpty(Settings[Cons.SETTING_RECAPTCHA_PRIVATE_KEY].ToString())
				|| String.IsNullOrEmpty(Settings[Cons.SETTING_REDIRECT_URL].ToString()) || String.IsNullOrEmpty(Settings[Cons.SETTING_CHANNEL_BASE_DOMAIN].ToString())
				|| String.IsNullOrEmpty(Settings[Cons.SETTING_WEBTRENDS_FOLDER].ToString()) || String.IsNullOrEmpty(Settings[Cons.SETTING_BRAINTREE_MERCHANT_ID].ToString())
				|| String.IsNullOrEmpty(Settings[Cons.SETTING_BRAINTREE_PUBLIC_KEY].ToString()) || String.IsNullOrEmpty(Settings[Cons.SETTING_BRAINTREE_PRIVATE_KEY].ToString())
				|| String.IsNullOrEmpty(Settings[Cons.SETTING_BRAINTREE_CLIENT_SIDE_ENCRYPTION_KEY].ToString()) || String.IsNullOrEmpty(Settings[Cons.SETTING_BRAINTREE_PLAN_ID].ToString()))
			{
				_isSet = false;
				return;
			}
			ExternalIdentifier = Settings[Cons.SETTING_EXTERNAL_IDENTIFIER].ToString();
			BaseDomain = Settings[Cons.SETTING_BASE_DOMAIN].ToString();
			DefaultParameters = Settings[Cons.SETTING_DEFAULT_PARAMETERS].ToString();
			BasePublicationId = int.Parse(Settings[Cons.SETTING_BASE_PUBLICATION_ID].ToString());
			DefaultCategoryId = int.Parse(Settings[Cons.SETTING_DEFAULT_CATEGORY_ID].ToString());
		    SelfServiceRoleName = Settings[Cons.SETTING_SELF_SERVICE_ROLE_NAME].ToString();
			CspResetUrl = Settings[Cons.SETTING_CSP_RESET_URL].ToString();
		    EffectiveDay = int.Parse(Settings[Cons.SETTING_EFFECTIVE_DAY].ToString());
            RcPublicKey = Settings[Cons.SETTING_RECAPTCHA_PUBLIC_KEY].ToString();
            RcPrivateKey = Settings[Cons.SETTING_RECAPTCHA_PRIVATE_KEY].ToString();
			RedirectUrl = Settings[Cons.SETTING_REDIRECT_URL].ToString();
			ChannelBaseDomain = Settings[Cons.SETTING_CHANNEL_BASE_DOMAIN].ToString();
			//MasterAliasValue = Settings[Cons.SETTING_ALIAS_VALUE].ToString();
			recaptcha.PublicKey = RcPublicKey;
			recaptcha.PrivateKey = RcPrivateKey;
			WebtrendsFolder = Settings[Cons.SETTING_WEBTRENDS_FOLDER].ToString();
			AdminEmail = Settings[Cons.SETTING_ADMIN_EMAIL].ToString();
			ContentTypeId = String.IsNullOrEmpty(Settings[Cons.SETTING_CONTENT_TYPE_ID].ToString()) ? 1 : int.Parse(Settings[Cons.SETTING_CONTENT_TYPE_ID].ToString());
			ContentTypeFieldId = String.IsNullOrEmpty(Settings[Cons.SETTING_CONTENT_TYPE_FIELD_ID].ToString()) ? 77 : int.Parse(Settings[Cons.SETTING_CONTENT_TYPE_FIELD_ID].ToString());
			GeneralSupplierIds = String.IsNullOrEmpty(Settings[Cons.SETTING_GENERAL_SUPPLIER_ID].ToString()) ? string.Empty : Settings[Cons.SETTING_GENERAL_SUPPLIER_ID].ToString();
			BraintreeMerchantId = Settings[Cons.SETTING_BRAINTREE_MERCHANT_ID].ToString();
			BraintreePublicKey = Settings[Cons.SETTING_BRAINTREE_PUBLIC_KEY].ToString();
			BraintreePrivateKey = Settings[Cons.SETTING_BRAINTREE_PRIVATE_KEY].ToString();
			BraintreeClientSideEncryptionKey = Settings[Cons.SETTING_BRAINTREE_CLIENT_SIDE_ENCRYPTION_KEY].ToString();
			BraintreePlanId = Settings[Cons.SETTING_BRAINTREE_PLAN_ID].ToString();

		}

		/// <summary>
		/// Saves the company.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>5/31/2013</datetime>
		private void SaveCompany()
		{
			if (!SaveNewUser()) return;
			SaveCompanyParameters();
			SaveChannel();
			SaveCompanyConsumer();
			SaveTheme();
			SaveCspMasterData();
			GoEmailToSupplier();
			
			//SupplierCreated.Value = "1";
		}

		private bool FormValidation()
		{
			Isvalidtext = "";
			bool isvalid = true;
			if (string.IsNullOrEmpty(companyname.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingCompanyName") + "<br>";
				isvalid = false;
			}
			if (string.IsNullOrEmpty(address1.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingAddress1") + "<br>";
				isvalid = false;
			}
			if (string.IsNullOrEmpty(city.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingCity") + "<br>";
				isvalid = false;
			}
			if (string.IsNullOrEmpty(state.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingState") + "<br>";
				isvalid = false;
			}
			if (string.IsNullOrEmpty(rcbCountry.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingCountry") + "<br>";
				isvalid = false;
			}
			if (string.IsNullOrEmpty(state.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingState") + "<br>";
				isvalid = false;
			}
			if (string.IsNullOrEmpty(zipcode.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingZipCode") + "<br>";
				isvalid = false;
			}
			if (string.IsNullOrEmpty(companywebsite.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingCompanyWebsite") + "<br>";
				isvalid = false;
			}
			if (string.IsNullOrEmpty(firstname.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingFirstName") + "<br>";
				isvalid = false;
			}
			if (string.IsNullOrEmpty(lastname.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingLastName") + "<br>";
				isvalid = false;
			}
			if (string.IsNullOrEmpty(jobtitle.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingJobTitle") + "<br>";
				isvalid = false;
			}
			if (string.IsNullOrEmpty(phonenumber.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingPhoneNumber") + "<br>";
				isvalid = false;
			}
			if (string.IsNullOrEmpty(emailaddress.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingEmailAddress") + "<br>";
				isvalid = false;
			}
			if (string.IsNullOrEmpty(confirmemailaddress.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingConfirmEmailAddress") + "<br>";
				isvalid = false;
			}
			if (string.IsNullOrEmpty(username.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingUserName") + "<br>";
				isvalid = false;
			}
			if (string.IsNullOrEmpty(password.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingPassword") + "<br>";
				isvalid = false;
			}
			if (string.IsNullOrEmpty(infor.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingInfor") + "<br>";
				isvalid = false;
			}
			if (string.IsNullOrEmpty(creditcardnumber.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingCreditCardNumber") + "<br>";
				isvalid = false;
			}
			if (string.IsNullOrEmpty(securitycode.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingSecurityCode") + "<br>";
				isvalid = false;
			}
/*			if (string.IsNullOrEmpty(expirationdate.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingExpirationDate") + "<br>";
				isvalid = false;
			}*/
			if (string.IsNullOrEmpty(expirationmonth.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingExpirationMonth") + "<br>";
				isvalid = false;
			}
			if (string.IsNullOrEmpty(expirationyear.Value))
			{
				Isvalidtext += GetLocalizedText("Msg.MissingExpirationYear") + "<br>";
				isvalid = false;
			}
			if (billingaddresscheck.Checked)
			{
				if (string.IsNullOrEmpty(billingfirstname.Value))
				{
					Isvalidtext += GetLocalizedText("Msg.MissingBillingFirstName") + "<br>";
					isvalid = false;
				}
				if (string.IsNullOrEmpty(billinglastname.Value))
				{
					Isvalidtext += GetLocalizedText("Msg.MissingBillingLastName") + "<br>";
					isvalid = false;
				}
				if (string.IsNullOrEmpty(billingaddress.Value))
				{
					Isvalidtext += GetLocalizedText("Msg.MissingBillingAddress") + "<br>";
					isvalid = false;
				}
				if (string.IsNullOrEmpty(billingcity.Value))
				{
					Isvalidtext += GetLocalizedText("Msg.MissingBillingCity") + "<br>";
					isvalid = false;
				}
				if (string.IsNullOrEmpty(billingcountry.Value))
				{
					Isvalidtext += GetLocalizedText("Msg.MissingBillingCountry") + "<br>";
					isvalid = false;
				}
				if (string.IsNullOrEmpty(billingstate.Value))
				{
					Isvalidtext += GetLocalizedText("Msg.MissingBillingState") + "<br>";
					isvalid = false;
				}
				if (string.IsNullOrEmpty(billingzipcode.Value))
				{
					Isvalidtext += GetLocalizedText("Msg.MissingBillingZipCode") + "<br>";
					isvalid = false;
				}
			}
			return isvalid;
		} 

		/// <summary>
		/// Saves the company parameters.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>6/6/2013</datetime>
		private void SaveCompanyParameters()
		{
			string ParameterTypeName = "CSP_Project_Name";
			string PTypeFirstName = "Personalize_Contact_Firstname";
			string PTypeLastName = "Personalize_Contact_Lastname";
			string PTypeJobTitle = "Personalize_Contact_Jobtitle";
			string PTypeEmail = "Personalize_Contact_Emailaddress";
			string PTypePhoneNumber = "Personalize_Contact_Phonenumber";
			string PTypeConsumerType = "ConsumerType";
			string PTypeInfor = "CSP_Referrer_Name";

			//Project Name
			companies_parameter_type parameterTypeProjectName =
				CspDataContext.companies_parameter_types.FirstOrDefault(a => a.parametername == ParameterTypeName);
			if (parameterTypeProjectName == null)
			{
				parameterTypeProjectName = new companies_parameter_type
				                           	{
												parametername = ParameterTypeName,
												parametertype = 1,
												sortorder = 99
				                           	};
				CspDataContext.companies_parameter_types.InsertOnSubmit(parameterTypeProjectName);
				CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
			}
			companies_parameter companiesParameter = new companies_parameter
			                                         	{
			                                         		companies_parameter_type = parameterTypeProjectName,
			                                         		companies_Id = NewCompany.companies_Id,
															value_text = CspProjectName
			                                         	};
			CspDataContext.companies_parameters.InsertOnSubmit(companiesParameter);

			// Contact First Name
			var ctypeFirstName = CspDataContext.companies_parameter_types.FirstOrDefault(a => a.parametername == PTypeFirstName);
			if(ctypeFirstName==null)
			{
				ctypeFirstName = new companies_parameter_type
										{
											parametername = PTypeFirstName,
											parametertype = 1,
											sortorder = 0
										};
				CspDataContext.companies_parameter_types.InsertOnSubmit(ctypeFirstName);
				CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);

			}
			companies_parameter newCPFirstName = new companies_parameter
			{
				companies_parameter_type = ctypeFirstName,
				companies_Id = NewCompany.companies_Id,
				value_text = firstname.Value
			};
			CspDataContext.companies_parameters.InsertOnSubmit(newCPFirstName);

			// Contact Last Name
			var ctypeLastName = CspDataContext.companies_parameter_types.FirstOrDefault(a => a.parametername == PTypeLastName);
			if (ctypeLastName == null)
			{
				ctypeLastName = new companies_parameter_type
										{
											parametername = PTypeLastName,
											parametertype = 1,
											sortorder = 0
										};
				CspDataContext.companies_parameter_types.InsertOnSubmit(ctypeLastName);
				CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);

			}
			companies_parameter newCPLastName = new companies_parameter
										{
											companies_parameter_type = ctypeLastName,
											companies_Id = NewCompany.companies_Id,
											value_text = lastname.Value
										};
			CspDataContext.companies_parameters.InsertOnSubmit(newCPLastName);

			// Contact Job Title
			var ctypeJobTitle = CspDataContext.companies_parameter_types.FirstOrDefault(a => a.parametername == PTypeJobTitle);
			if (ctypeJobTitle == null)
			{
				ctypeJobTitle = new companies_parameter_type
				{
					parametername = PTypeJobTitle,
					parametertype = 1,
					sortorder = 0
				};
				CspDataContext.companies_parameter_types.InsertOnSubmit(ctypeJobTitle);
				CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);

			}
			companies_parameter newCPJobTitle = new companies_parameter
			{
				companies_parameter_type = ctypeJobTitle,
				companies_Id = NewCompany.companies_Id,
				value_text = jobtitle.Value
			};
			CspDataContext.companies_parameters.InsertOnSubmit(newCPJobTitle);

			// Contact Email
			var ctypeEmail = CspDataContext.companies_parameter_types.FirstOrDefault(a => a.parametername == PTypeEmail);
			if (ctypeEmail == null)
			{
				ctypeEmail = new companies_parameter_type
				{
					parametername = PTypeEmail,
					parametertype = 1,
					sortorder = 0
				};
				CspDataContext.companies_parameter_types.InsertOnSubmit(ctypeEmail);
				CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);

			}
			companies_parameter newCPEmail = new companies_parameter
			{
				companies_parameter_type = ctypeEmail,
				companies_Id = NewCompany.companies_Id,
				value_text = emailaddress.Value
			};
			CspDataContext.companies_parameters.InsertOnSubmit(newCPEmail);

			// Contact Phone Number
			var ctypePhoneNumber = CspDataContext.companies_parameter_types.FirstOrDefault(a => a.parametername == PTypePhoneNumber);
			if (ctypePhoneNumber == null)
			{
				ctypePhoneNumber = new companies_parameter_type
				{
					parametername = PTypePhoneNumber,
					parametertype = 1,
					sortorder = 0
				};
				CspDataContext.companies_parameter_types.InsertOnSubmit(ctypePhoneNumber);
				CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
			}
			companies_parameter newCPPhoneNumber = new companies_parameter
			{
				companies_parameter_type = ctypePhoneNumber,
				companies_Id = NewCompany.companies_Id,
				value_text = phonenumber.Value
			};
			CspDataContext.companies_parameters.InsertOnSubmit(newCPPhoneNumber);

			//CSP Referrer Name
			var ctypeReferrerName = CspDataContext.companies_parameter_types.FirstOrDefault(a => a.parametername == PTypeInfor);
			if (ctypeReferrerName == null)
			{
				ctypeReferrerName = new companies_parameter_type
				{
					parametername = PTypeInfor,
					parametertype = 1,
					sortorder = 0
				};
				CspDataContext.companies_parameter_types.InsertOnSubmit(ctypeReferrerName);
				CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
			}
			companies_parameter newCPReferrerName = new companies_parameter
			{
				companies_parameter_type = ctypeReferrerName,
				companies_Id = NewCompany.companies_Id,
				value_text = infor.Value
			};
			CspDataContext.companies_parameters.InsertOnSubmit(newCPReferrerName);

			//Consumer Type
			if(MasterAliasValue.ToLower().StartsWith("test"))
			{
				var ctypeConsumerType = CspDataContext.companies_parameter_types.FirstOrDefault(a => a.parametername == PTypeConsumerType);
				if (ctypeConsumerType == null)
				{
					ctypeConsumerType = new companies_parameter_type
					{
						parametername = PTypeConsumerType,
						parametertype = 1,
						sortorder = 99
					};
					CspDataContext.companies_parameter_types.InsertOnSubmit(ctypeConsumerType);
					CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
				}
				companies_parameter newCPComsumerType = new companies_parameter
				{
					companies_parameter_type = ctypeConsumerType,
					companies_Id = NewCompany.companies_Id,
					value_text = "T"
				};
				CspDataContext.companies_parameters.InsertOnSubmit(newCPComsumerType);
			}
			CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
		}

		private void SaveCspMasterData()
		{

			Alia newAlias = new Alia
			                	{
									Alias = CspProjectName,
									Value = CspProjectName
			                	};
			CspMasterDataContext.Alias.InsertOnSubmit(newAlias);
			CspMasterDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);

			Regex rgx = new Regex("[^a-zA-Z0-9]");
			//str = rgx.Replace(str, "");
			Domain_Mask newDomainMask = new Domain_Mask
			                            	{
												Mask = BaseDomainRegExp.Replace(tbxBaseDomain.Value, "").ToLower(),
			                            		//Project = rgx.Replace(NewCompany.companyname, "")
												Project = CspProjectName
			                            	};
			CspMasterDataContext.Domain_Masks.InsertOnSubmit(newDomainMask);
			CspMasterDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);

			Connection newConnection = new Connection
			                           	{
			                           		Mode = Settings[Cons.SETTING_MASTER_CONNECTION_MODE].ToString(),
											Project = CspProjectName,
			                           		Connection1 = Settings[Cons.SETTING_MASTER_ENCODED_CONNECTION].ToString(),
											//Domain = BaseDomainRegExp.Replace(tbxBaseDomain.Value, "")
											Domain = ChannelBaseDomain
			                           	};
			CspMasterDataContext.Connections.InsertOnSubmit(newConnection);
			CspMasterDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
			
			ProjectContentTypeMap newProjectContentTypeMap = new ProjectContentTypeMap
			                     	                           	{
			                     	                           		Identifier = "campaign",
																	Project = CspProjectName,
																	ContentTypeId = ContentTypeId,
																	ContentTypeFieldId = ContentTypeFieldId
			                     	                           	};
			CspMasterDataContext.ProjectContentTypeMaps.InsertOnSubmit(newProjectContentTypeMap);
			CspMasterDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
		}

		/// <summary>
		/// Saves the theme.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>5/31/2013</datetime>
		private void SaveTheme()
		{
			//Insert theme
			theme newTheme = new theme
			                 	{
									description = NewCompany.companyname,
									active = true,
									externalCode = NewCompany.companyname
			                 	};
			CspDataContext.themes.InsertOnSubmit(newTheme);
			CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);

			NewTheme = newTheme;

			//Insert theme supplier
			themes_supplier newThemeSupplier = new themes_supplier
			                                   	{
			                                   		themes_Id = NewTheme.themes_Id,
			                                   		companies_Id = NewCompany.companies_Id
			                                   	};
			CspDataContext.themes_suppliers.InsertOnSubmit(newThemeSupplier);
			// Insert General Ids to theme_Supplier
			if(!string.IsNullOrEmpty(GeneralSupplierIds))
			{
				string[] ids = GeneralSupplierIds.Split(',');
				
				foreach (var id in ids)
				{
					company companies = CspDataContext.companies.SingleOrDefault(a => a.companies_Id == int.Parse(id)&&a.is_supplier==true);
					if (companies != null)
					{
						themes_supplier newThemeSupplierGeneral = new themes_supplier
						                                          	{
						                                          		themes_Id = NewTheme.themes_Id,
						                                          		companies_Id = int.Parse(id)
						                                          	};
						CspDataContext.themes_suppliers.InsertOnSubmit(newThemeSupplierGeneral);
					}
				}
			}
			CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);

			//Insert theme category
			themes_category newThemeCategory = new themes_category
			                                   	{
			                                   		themes_Id = newTheme.themes_Id,
			                                   		category_Id = DefaultCategoryId
			                                   	};
			CspDataContext.themes_categories.InsertOnSubmit(newThemeCategory);
			CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);

			//Insert channel theme association
			/*ChannelThemeAssocation newChannelThemeAssociation = new ChannelThemeAssocation
			                                                    	{
			                                                    		ThemeId = NewTheme.themes_Id,
			                                                    		ChannelId = NewChannel.Id
			                                                    	};
			CspDataContext.ChannelThemeAssocations.InsertOnSubmit(newChannelThemeAssociation);
			CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);*/
			string ctQuery = String.Format("INSERT INTO ChannelThemeAssocation VALUES ({0}, {1})", NewTheme.themes_Id,
			                               NewChannel.Id);
			CspDataContext.ExecuteCommand(ctQuery);

			companies_theme newCompanyTheme = new companies_theme
			                                  	{
			                                  		themes_Id = NewTheme.themes_Id,
			                                  		companies_Id = NewCompany.companies_Id
			                                  	};
			CspDataContext.companies_themes.InsertOnSubmit(newCompanyTheme);
			CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
		}

		/// <summary>
		/// Saves the company consumer.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>5/31/2013</datetime>
		private void SaveCompanyConsumer()
		{
			int defaultLangId = 1;
			int fallBackLangId = 1;
			companies_consumer newCompanyConsumer = new companies_consumer
			                                        	{
															companies_Id = NewCompany.companies_Id,
															base_domain = BaseDomainRegExp.Replace(tbxBaseDomain.Value, ""),
															base_publication_Id = BasePublicationId,
															base_publication_parameters = DefaultParameters,
															active = true,
															default_language_Id = defaultLangId,
															fallback_language_Id = fallBackLangId
			                                        	};
			CspDataContext.companies_consumers.InsertOnSubmit(newCompanyConsumer);
			CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
		}

		/// <summary>
		/// Saves the channel.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>5/31/2013</datetime>
		private void SaveChannel()
		{
			Channel newChannel = new Channel
			                     	{
										CompanyId = NewCompany.companies_Id,
										Name = NewCompany.companyname,
										Description = NewCompany.companyname,
										//ExternalIdentifier = ExternalIdentifier,
										ExternalIdentifier = CspProjectName,
										BaseDomain = ChannelBaseDomain,
										DefaultParameters = DefaultParameters,
										BasePublicationId = BasePublicationId
			                     	};
			CspDataContext.Channels.InsertOnSubmit(newChannel);
			CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
			NewChannel = newChannel;
		}

		/// <summary>
		/// Saves the new user.
		/// </summary>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>5/31/2013</datetime>
		private bool SaveNewUser()
		{
			var memberShipProvider = new SqlMembershipProvider();
			int contentStagingScheme = 1;
			
			var mObjUser = new UserInfo
			{
				AffiliateID = Null.NullInteger,
				Email = Server.HtmlEncode(emailaddress.Value),
				FirstName = Server.HtmlEncode(firstname.Value),
				LastName = Server.HtmlEncode(lastname.Value),
				IsSuperUser = false,
				PortalID = PortalId,
				Username = Server.HtmlEncode(username.Value),
				DisplayName = Server.HtmlEncode(firstname.Value) + " " + Server.HtmlEncode(lastname.Value),
				Membership =
				{
					Password = password.Value,
					Approved = true,
					UpdatePassword = false
				}
			};

			UserCreateStatus mObjCreateStatus = DotNetNuke.Entities.Users.UserController.CreateUser(ref mObjUser);
			if (mObjCreateStatus != UserCreateStatus.Success)
			{
				/*Response.Write(mObjCreateStatus.ToString());
				Response.Flush();
				Response.End();*/
				ControlMessage.InnerHtml = mObjCreateStatus.ToString();
				return false;
			}

            AssignRole(mObjUser);
			company newCompany = new company
			                     	{
										companyname = Server.HtmlEncode(companyname.Value).Length > 75 ? Server.HtmlEncode(companyname.Value).Substring(0, 75) : Server.HtmlEncode(companyname.Value),
										address1 = Server.HtmlEncode(address1.Value).Length > 50 ? Server.HtmlEncode(address1.Value).Substring(0, 50) : Server.HtmlEncode(address1.Value),
										city = Server.HtmlEncode(city.Value).Length > 50 ? Server.HtmlEncode(city.Value).Substring(0, 50) : Server.HtmlEncode(city.Value),
										zipcode = Server.HtmlEncode(zipcode.Value).Length > 50 ? Server.HtmlEncode(zipcode.Value).Substring(0, 50) : Server.HtmlEncode(zipcode.Value),
										state = Server.HtmlEncode(state.Value).Length > 20 ? Server.HtmlEncode(state.Value).Substring(0, 20) : Server.HtmlEncode(state.Value),
										country = Server.HtmlEncode(rcbCountry.Value).Length > 20 ? Server.HtmlEncode(rcbCountry.Value).Substring(0, 20) : Server.HtmlEncode(rcbCountry.Value),
										website_url = Server.HtmlEncode(companywebsite.Value).Length > 200 ? Server.HtmlEncode(companywebsite.Value).Substring(0, 200) : Server.HtmlEncode(companywebsite.Value),
										is_consumer = true,
										is_supplier = true,
										content_staging_scheme = contentStagingScheme,
										displayname = Server.HtmlEncode(companyname.Value),
										type = "both",
										created = DateTime.Now,
										date_changed = DateTime.Now
			                     	};
			CspDataContext.companies.InsertOnSubmit(newCompany);
			CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
			newCompany.parent_companies_Id = newCompany.companies_Id;
			
			mObjUser.Profile.InitialiseProfile(PortalId);
			//Update profile cspid
			mObjUser.Profile.SetProfileProperty("CspId", newCompany.companies_Id.ToString());
			mObjUser.Profile.SetProfileProperty("Telephone", phonenumber.Value);
			if (mObjUser.Profile.GetProperty("UserAgreement") != null)
				mObjUser.Profile.SetProfileProperty("UserAgreement", "True");
			ProfileController.UpdateUserProfile(mObjUser);
			UserController.UpdateUser(PortalId, mObjUser);
			CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);

			Regex rgx = new Regex("[^a-zA-Z0-9]");
			NewCompany = newCompany;
			MasterAliasValue = rgx.Replace(newCompany.companyname.ToLower(), "");
			CspProjectName = Server.HtmlEncode(Request.Params["fake-basedomain-prefix"]);
			return true;
		}
	    /// <summary>
		/// Validates the companyname.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>5/31/2013</datetime>
		private void ValidateCompanyname()
		{
			int resultCode = 1;
			string coName = Server.HtmlEncode(Request.Params["companyname"]);
			company theCompany = CspDataContext.companies.FirstOrDefault(a => a.companyname.ToLower() == coName.ToLower());
			if (theCompany != null)
				resultCode = 0;
			Response.Write(resultCode);
			Response.Flush();
			Response.End();
		}

		/// <summary>
		/// Validates the username.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>5/31/2013</datetime>
		private void ValidateUsername()
		{
			int resultCode = 1;
			string userName = Request.Params["username"];
			User dnnUser = DnnData.Users.FirstOrDefault(a => a.Username.ToLower() == userName.ToLower());
			if (dnnUser != null)
				resultCode = 0;
			Response.Write(resultCode);
			Response.Flush();
			Response.End();
		}


		/// <summary>
		/// Validates the base domain.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>5/31/2013</datetime>
		private void ValidateBaseDomain()
		{
			int resultCode = 1;
			string baseDomain = BaseDomainRegExp.Replace(Request.Params["basedomain"], "").ToLower();
			companies_consumer existCompanyConsumer =
				CspDataContext.companies_consumers.FirstOrDefault(a => a.base_domain.ToLower() == baseDomain);
			if (existCompanyConsumer != null)
				resultCode = 0;
			Response.Write(resultCode);
			Response.Flush();
			Response.End();
		}
        private void AssignRole(UserInfo mObjUser)
        {
            var oDnnRoleController = new RoleController();
            var oRole = oDnnRoleController.GetRoleByName(PortalId, SelfServiceRoleName);
            if (oRole == null)
            {
                oRole = new RoleInfo
                {
                    PortalID = PortalId,
                    RoleName = SelfServiceRoleName,
                    IsPublic = false,
                    AutoAssignment = false,
                    RoleGroupID = Null.NullInteger,
                    Status = RoleStatus.Approved
                };
                oDnnRoleController.AddRole(oRole);
            }
            oDnnRoleController.AddUserRole(PortalId, mObjUser.UserID, oRole.RoleID,DateTime.Now,DateTime.Now.AddDays(EffectiveDay));
            //oDnnRoleController.AddUserRole(PortalId, mObjUser.UserID, oRole.RoleID, DateTime.Now.AddDays(Null.NullInteger), Null.NullDate);
        }
        private void InitCountry()
        {
            Hashtable h = new Hashtable();
            Dictionary<string, string> objDic = new Dictionary<string, string>();

            foreach (CultureInfo ObjCultureInfo in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                RegionInfo objRegionInfo = new RegionInfo(ObjCultureInfo.Name);

                if (!objDic.ContainsKey(objRegionInfo.EnglishName))
                {
                    objDic.Add(objRegionInfo.EnglishName, objRegionInfo.TwoLetterISORegionName.ToLower());
                }
            }
            SortedList<string, string> sortedList = new SortedList<string, string>(objDic);
            foreach (KeyValuePair<string, string> val in sortedList)
            {
               //rcbCountry.Items.Add(new RadComboBoxItem(val.Key, val.Key));
				rcbCountry.Items.Add(new ListItem(val.Key, val.Key));
				billingcountry.Items.Add(new ListItem(val.Key, val.Key));
            }


			if (rcbCountry.Items.FindByValue(GetLocalizedText("Country.DefaultValue")) != null)
				rcbCountry.Items.FindByValue(GetLocalizedText("Country.DefaultValue")).Selected = true;
			else if (rcbCountry.Items.FindByValue("United States") != null)
			{
				rcbCountry.Items.FindByValue("United States").Selected = true;
			}
			if(billingcountry.Items.FindByValue(GetLocalizedText("Country.DefaultValue"))!=null)
				billingcountry.Items.FindByValue(GetLocalizedText("Country.DefaultValue")).Selected = true;
			else if (billingcountry.Items.FindByValue("United States") != null)
			{
				billingcountry.Items.FindByValue("United States").Selected = true;
			}
        }
		private void InitExpirationDate()
		{
			var currentYear = DateTime.Now.Year;
			for (int i = 1; i < 13; i++)
			{
				expirationmonth.Items.Add(new ListItem(i.ToString("D2"),i.ToString(CultureInfo.InvariantCulture)));
			}
			for(int i=currentYear-5;i<currentYear+11;i++)
			{
				expirationyear.Items.Add(new ListItem(i.ToString(CultureInfo.InvariantCulture),i.ToString(CultureInfo.InvariantCulture)));
			}
		}
		protected string GetCspUrl()
		{
			companies_consumer sampleCC =
				CspDataContext.companies_consumers.Where(a => !String.IsNullOrEmpty(a.base_domain)).OrderByDescending(
					a => a.companies_consumers_Id).FirstOrDefault();
			if (sampleCC != null)
				return sampleCC.base_domain;
			return String.Empty;
		}

        protected void BtnSubmitClick(object sender, EventArgs e)
        {

        	if (!recaptcha.IsValid)
            {		
				lblResult.Visible = true;
            	lblResult.Text = GetLocalizedText("Recaptcha.Incorrect");
                lblResult.ForeColor = System.Drawing.Color.Red;
            }
			else if (!FormValidation())
			{
				string radalertscript = "<script language='javascript'> window.onload = function(){radalert('" + GetLocalizedText("Msg.FillAllRequiredFields") + "<br>" + Isvalidtext + "', 330, 210,'" + GetLocalizedText("Msg.MissingTitle") + "');}</script>";
				Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript); 
			}
        	else if (!tbxBaseDomain.Value.EndsWith(Settings[Cons.SETTING_BASE_DOMAIN].ToString())||tbxBaseDomain.Value.Length>=255)
			{
				string radalertscript = "<script language='javascript'> window.onload = function(){radalert('" + GetLocalizedText("Msg.InvalidBaseDomain") + "', 330, 210,'" + GetLocalizedText("Msg.MissingTitle") + "');}</script>";
				Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript); 
			}
			else if (!ValidateExpirationDate())
			{
				string radalertscript = "<script language='javascript'> window.onload = function(){radalert('" + GetLocalizedText("Msg.FillAllRequiredFields") + "<br>" + GetLocalizedText("Msg.InvalidExpriationDate") + "<br>" +"', 330, 210,'" + GetLocalizedText("Msg.MissingTitle") + "');}</script>";
				Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript); 
			}
			else if(!CreateCustomer())
			{
				string radalertscript = "<script language='javascript'> window.onload = function(){radalert('" + GetLocalizedText("Msg.FillAllRequiredFields") + "<br>" + MessagePayment + "', 330, 210,'" + GetLocalizedText("Msg.MissingTitle") + "');}</script>";
				Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript); 
			}
			else
            {

                lblResult.Text = "";
				SaveCompany();
				ConfirmPane.Visible = true;
				MainPane.Visible = false;
            	WelcomePane.Visible = false;
            	CreatWebtrendsFile();
				if (CspResetUrl.IndexOf("http://") == -1)
					CspResetUrl = "http://" + CspResetUrl;
            	CspResetUrl = CspResetUrl.Replace("[ticks]", DateTime.Now.Ticks.ToString());
				Uri resetUri = new Uri(CspResetUrl);
            	Controls.Add(new Literal
            	             	{
            	             		Text = "<iframe class='helloworld' style='display:none' src='"+CspResetUrl+"'></iframe>"
				             	});
				/*
				HttpWebRequest request = HttpWebRequest.Create(resetUri) as HttpWebRequest;
				request.Method = WebRequestMethods.Http.Get;
				HttpWebResponse response = request.GetResponse() as HttpWebResponse;
				response.Close();
				 */
				Thread thread = new Thread(new ParameterizedThreadStart(ReloadCsp));
				thread.Start(resetUri);
            }
        }

		private void ReloadCsp(object uri)
		{
			try
			{
				//HttpWebRequest request = WebRequest.Create((Uri) uri) as HttpWebRequest;
				//request.Method = WebRequestMethods.Http.Get;
				WebRequest request = WebRequest.Create((Uri) uri);
				using (var response = request.GetResponse())
				{
				}
			}
			catch (Exception ex){}
		}

		public string RedirectLink()
		{
			if (!string.IsNullOrEmpty(RedirectUrl))
			{
				var dataProvider = DataProvider.Instance();
				var result = dataProvider.ExecuteSQL(string.Format("SELECT TabID FROM Tabs WHERE TabName = '{0}'", RedirectUrl));
				int tabId = 0;
				while (result.Read())
				{
					tabId = int.Parse(result["TabId"].ToString());
					break;
				}
				if (tabId != 0)
				{
					return Globals.NavigateURL(tabId);
				}
			}
			return Globals.NavigateURL(TabId, "", "mid", ModuleId.ToString());
		}
		public void CreatWebtrendsFile()
		{
			string webtrendsPath = WebtrendsFolder + "/" + CspProjectName + "/script/";
			webtrendsPath = Server.MapPath(webtrendsPath);
			string jsFile = Path.Combine(webtrendsPath, "webtrends.js");
			if (!File.Exists(jsFile))
			{
				SetupFolder(webtrendsPath);
				using (Stream fs = new FileStream(jsFile, FileMode.CreateNew, FileAccess.ReadWrite))
				{
					using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
					{
						//string content = string.Format(@"<body><iframe style='width:100%;height:100%' src='{0}'></iframe></body>", Filename);
						string content = GetLocalizedText("Webtrends.Content");
						content = content.Replace("[CSP_PROJECT_NAME]", CspProjectName);
						w.WriteLine(content);
					}
				}
			}
		}
		public void SetupFolder(string folderPath)
		{
			if (String.IsNullOrEmpty(folderPath)) return;
			if (!Directory.Exists(folderPath))
				Directory.CreateDirectory(folderPath);
		}
		//Test payments
		public bool CreateCustomer()
		{
			string bfirstname,blastname,baddress,bcity,bcountry,bzipcode,bstate;
			//check if user use billing address
			if (billingaddresscheck.Checked)
			{
				bfirstname = billingfirstname.Value;
				blastname = billinglastname.Value;
				baddress = billingaddress.Value;
				bcity = billingcity.Value;
				bcountry = GetCountryBraintree(billingcountry.Value);
				bzipcode = billingzipcode.Value;
				bstate = billingstate.Value;
			}
			else
			{
				bfirstname = firstname.Value;
				blastname = lastname.Value;
				baddress = address1.Value;
				bcity = city.Value;
				bcountry = GetCountryBraintree(rcbCountry.Value);
				bzipcode = zipcode.Value;
				bstate = state.Value;
			}
			//var datepart = expirationdate.Value.Split('/');
			gateway = new BraintreeGateway
			{
				Environment = Braintree.Environment.SANDBOX,
				MerchantId = BraintreeMerchantId,
				PublicKey = BraintreePublicKey,
				PrivateKey = BraintreePrivateKey
			};
			var request = new CustomerRequest
			{
				Company = companyname.Value,
				Email = emailaddress.Value,
				Phone = phonenumber.Value,
				Website = companywebsite.Value,
				FirstName = firstname.Value,
				LastName = lastname.Value,
				CreditCard = new CreditCardRequest
				{
					BillingAddress = new CreditCardAddressRequest
					{
						FirstName = bfirstname,
						LastName = blastname,
						StreetAddress = baddress,
						Company = companyname.Value,
						Locality = bcity,
						Region = bstate,
						CountryName = bcountry,
						PostalCode = bzipcode
					},
					Number = creditcardnumber.Value,
					ExpirationMonth = expirationmonth.Value,
					ExpirationYear = expirationyear.Value,
					CVV = securitycode.Value
				}
			};

			Result<Customer> result = gateway.Customer.Create(request);

			if (result.IsSuccess())
			{

				Customer customer = gateway.Customer.Find(result.Target.CreditCards[0].CustomerId);
				string paymentMethodToken = customer.CreditCards[0].Token;
				var subscriptionRequest = new SubscriptionRequest
				{
					PlanId = BraintreePlanId,
					PaymentMethodToken = paymentMethodToken
				};
				Result<Subscription> subscriptionResult = gateway.Subscription.Create(subscriptionRequest);
				if (subscriptionResult.IsSuccess())
				{
					return true;
				}
				MessagePayment = subscriptionResult.Message;
				MessagePayment = "_" + MessagePayment.Replace("\n", "<br>_");
				return false;
			}
			MessagePayment = result.Message;
			MessagePayment = "_"+ MessagePayment.Replace("\n", "<br>_");
			return false;
		}
		public string GetCountryBraintree(string country)
		{
			var dic = new Dictionary<string, string>
			          	{
			          		{"Bolivarian Republic of Venezuela", "Bolivia"},
			          		{"Hong Kong S.A.R.", "Hong Kong"},
			          		{"Islamic Republic of Pakistan", "Pakistan"},
			          		{"Lao P.D.R.", "Laos"},
			          		{"Macao S.A.R.", "Macau"},
			          		{"Macedonia (FYROM)", "Macedonia"},
			          		{"People's Republic of China", "China"},
			          		{"Principality of Monaco", "Monaco"},
			          		{"Republic of the Philippines", "Philippines"},
			          		{"Serbia and Montenegro (Former)", "Serbia"},
			          		{"U.A.E.", "United Arab Emirates"},
			          		{"United States", "United States of America"}
			          	};
			if (dic.ContainsKey(country))
			{
				return dic[country];
			}
			return country;
		}
		public bool ValidateExpirationDate()
		{
			var month = int.Parse(expirationmonth.Value);
			var year = int.Parse(expirationyear.Value);
			if (month < 1 || month > 12 || year < DateTime.Now.Year || month + year < DateTime.Now.Year + DateTime.Now.Month)
				return false;
			return true;
		}
	}
}
