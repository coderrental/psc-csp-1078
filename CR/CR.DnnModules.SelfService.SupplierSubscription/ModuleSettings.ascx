﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleSettings.ascx.cs" Inherits="CR.DnnModules.SelfService.SupplierSubscription.ModuleSettings" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div>
	<table>
		<tr>
			<td>CSP Master Connection String:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxCspMasterConnectionString"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>CSP DB Connection String:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxCspDnnConnectionString"></telerik:RadTextBox></td>
		</tr>
		<%--<tr>
			<td>Alias Value:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxAliasValue"></telerik:RadTextBox></td>
		</tr>--%>
		<tr>
			<td>Master Encoded Connection:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxMasterEncodedConnection"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>Master Connection Mode:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxMasterConnectionMode"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>External Identifier:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxExternalIdentifier"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>Vendor Base Domain:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxBaseDomain"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>Channel Base Domain:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxChannelBaseDomain"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>Default Parameters:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxDefaultParameters"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>Base Publication Id:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxBasePublicationId"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>Default Category Id:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxDefaultCategoryId"></telerik:RadTextBox></td>
		</tr>
        <tr>
			<td>Self Service Role Name:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxSelfServiceRoleName"></telerik:RadTextBox></td>
		</tr>
        <tr>
			<td>Effective Day</td>
			<td><telerik:RadTextBox runat="server" ID="tbxEffectiveDay"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>Admin Email:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxAdminEmail"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>CSP Reset Url:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxCspResetUrl"></telerik:RadTextBox></td>
		</tr>
        <tr>
			<td>reCaptchar Public Key:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxRcPublicKey"></telerik:RadTextBox></td>
		</tr>
        <tr>
			<td>reCaptchar Private Key:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxRcPrivateKey"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>Redirect Tab Name</td>
			<td><telerik:RadTextBox runat="server" ID="tbxRedirectUrl"></telerik:RadTextBox></td>
		</tr>
		 <tr>
			<td>Webtrends Folder</td>
			<td><telerik:RadTextBox runat="server" ID="tbxWebtrendsFolder"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>Content Type Id</td>
			<td><telerik:RadTextBox runat="server" ID="tbxContentTypeId"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>Content Type Field Id</td>
			<td><telerik:RadTextBox runat="server" ID="tbxContentTypeFieldId"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>General Supplier Ids</td>
			<td><telerik:RadTextBox runat="server" ID="tbxGeneralSupplierId"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>Braintree Merchant Id</td>
			<td><telerik:RadTextBox runat="server" ID="tbxBraintreeMerchantId"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>Braintree Public Key</td>
			<td><telerik:RadTextBox runat="server" ID="tbxBraintreePublicKey"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>Braintree Private Key</td>
			<td><telerik:RadTextBox runat="server" ID="tbxBraintreePrivateKey"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>Braintree Client Side Encryption Key</td>
			<td><telerik:RadTextBox runat="server" ID="tbxBraintreeClientSideEncryptionKey"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>Braintree Plan Id</td>
			<td><telerik:RadTextBox runat="server" ID="tbxBraintreePlanId"></telerik:RadTextBox></td>
		</tr>
	</table>
</div>