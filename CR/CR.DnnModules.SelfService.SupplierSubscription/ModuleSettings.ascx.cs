﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.DnnModules.SelfService.SupplierSubscription.Common;
using DotNetNuke.Entities.Modules;

namespace CR.DnnModules.SelfService.SupplierSubscription
{
	public partial class ModuleSettings : ModuleSettingsBase
	{
		public override void LoadSettings()
		{
			if (TabModuleSettings[Cons.SETTING_CSPMASTER_CONNECTIONSTRING] != null)
			{
				tbxCspMasterConnectionString.Text = TabModuleSettings[Cons.SETTING_CSPMASTER_CONNECTIONSTRING].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_CSPDNN_CONNECTIONSTRING] != null)
			{
				tbxCspDnnConnectionString.Text = TabModuleSettings[Cons.SETTING_CSPDNN_CONNECTIONSTRING].ToString();
			}
			/*if (TabModuleSettings[Cons.SETTING_ALIAS_VALUE] != null)
			{
				tbxAliasValue.Text = TabModuleSettings[Cons.SETTING_ALIAS_VALUE].ToString();
			}*/
			if (TabModuleSettings[Cons.SETTING_EXTERNAL_IDENTIFIER] != null)
			{
				tbxExternalIdentifier.Text = TabModuleSettings[Cons.SETTING_EXTERNAL_IDENTIFIER].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_BASE_DOMAIN] != null)
			{
				tbxBaseDomain.Text = TabModuleSettings[Cons.SETTING_BASE_DOMAIN].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_DEFAULT_PARAMETERS] != null)
			{
				tbxDefaultParameters.Text = TabModuleSettings[Cons.SETTING_DEFAULT_PARAMETERS].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_BASE_PUBLICATION_ID] != null)
			{
				tbxBasePublicationId.Text = TabModuleSettings[Cons.SETTING_BASE_PUBLICATION_ID].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_DEFAULT_CATEGORY_ID] != null)
			{
				tbxDefaultCategoryId.Text = TabModuleSettings[Cons.SETTING_DEFAULT_CATEGORY_ID].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_MASTER_ENCODED_CONNECTION] != null)
			{
				tbxMasterEncodedConnection.Text = TabModuleSettings[Cons.SETTING_MASTER_ENCODED_CONNECTION].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_MASTER_CONNECTION_MODE] != null)
			{
				tbxMasterConnectionMode.Text = TabModuleSettings[Cons.SETTING_MASTER_CONNECTION_MODE].ToString();
			}
            if (TabModuleSettings[Cons.SETTING_SELF_SERVICE_ROLE_NAME] != null)
            {
                tbxSelfServiceRoleName.Text = TabModuleSettings[Cons.SETTING_SELF_SERVICE_ROLE_NAME].ToString();
            }
			if (TabModuleSettings[Cons.SETTING_ADMIN_EMAIL] != null)
			{
				tbxAdminEmail.Text = TabModuleSettings[Cons.SETTING_ADMIN_EMAIL].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_CSP_RESET_URL] != null)
			{
				tbxCspResetUrl.Text = TabModuleSettings[Cons.SETTING_CSP_RESET_URL].ToString();
			}
            if (TabModuleSettings[Cons.SETTING_EFFECTIVE_DAY] != null)
            {
                tbxEffectiveDay.Text = TabModuleSettings[Cons.SETTING_EFFECTIVE_DAY].ToString();
            }
            if (TabModuleSettings[Cons.SETTING_RECAPTCHA_PUBLIC_KEY] != null)
            {
                tbxRcPublicKey.Text = TabModuleSettings[Cons.SETTING_RECAPTCHA_PUBLIC_KEY].ToString();
            }
            if (TabModuleSettings[Cons.SETTING_RECAPTCHA_PRIVATE_KEY] != null)
            {
                tbxRcPrivateKey.Text = TabModuleSettings[Cons.SETTING_RECAPTCHA_PRIVATE_KEY].ToString();
            }
			if (TabModuleSettings[Cons.SETTING_REDIRECT_URL] != null)
			{
				tbxRedirectUrl.Text = TabModuleSettings[Cons.SETTING_REDIRECT_URL].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_CHANNEL_BASE_DOMAIN] != null)
			{
				tbxChannelBaseDomain.Text = TabModuleSettings[Cons.SETTING_CHANNEL_BASE_DOMAIN].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_WEBTRENDS_FOLDER] != null)
			{
				tbxWebtrendsFolder.Text = TabModuleSettings[Cons.SETTING_WEBTRENDS_FOLDER].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_CONTENT_TYPE_ID] != null)
			{
				tbxContentTypeId.Text = TabModuleSettings[Cons.SETTING_CONTENT_TYPE_ID].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_CONTENT_TYPE_FIELD_ID] != null)
			{
				tbxContentTypeFieldId.Text = TabModuleSettings[Cons.SETTING_CONTENT_TYPE_FIELD_ID].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_BRAINTREE_MERCHANT_ID] != null)
			{
				tbxBraintreeMerchantId.Text = TabModuleSettings[Cons.SETTING_BRAINTREE_MERCHANT_ID].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_BRAINTREE_PUBLIC_KEY] != null)
			{
				tbxBraintreePublicKey.Text = TabModuleSettings[Cons.SETTING_BRAINTREE_PUBLIC_KEY].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_BRAINTREE_PRIVATE_KEY] != null)
			{
				tbxBraintreePrivateKey.Text = TabModuleSettings[Cons.SETTING_BRAINTREE_PRIVATE_KEY].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_BRAINTREE_CLIENT_SIDE_ENCRYPTION_KEY] != null)
			{
				tbxBraintreeClientSideEncryptionKey.Text = TabModuleSettings[Cons.SETTING_BRAINTREE_CLIENT_SIDE_ENCRYPTION_KEY].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_BRAINTREE_PLAN_ID] != null)
			{
				tbxBraintreePlanId.Text = TabModuleSettings[Cons.SETTING_BRAINTREE_PLAN_ID].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_GENERAL_SUPPLIER_ID] != null)
			{
				tbxGeneralSupplierId.Text = TabModuleSettings[Cons.SETTING_GENERAL_SUPPLIER_ID].ToString();
			}

		}

		public override void UpdateSettings()
		{
			var moduleController = new ModuleController();
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_CSPMASTER_CONNECTIONSTRING, tbxCspMasterConnectionString.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_CSPDNN_CONNECTIONSTRING, tbxCspDnnConnectionString.Text);
			//moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_ALIAS_VALUE, tbxAliasValue.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_EXTERNAL_IDENTIFIER, tbxExternalIdentifier.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_BASE_DOMAIN, tbxBaseDomain.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_DEFAULT_PARAMETERS, tbxDefaultParameters.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_BASE_PUBLICATION_ID, tbxBasePublicationId.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_DEFAULT_CATEGORY_ID, tbxDefaultCategoryId.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_MASTER_ENCODED_CONNECTION, tbxMasterEncodedConnection.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_MASTER_CONNECTION_MODE, tbxMasterConnectionMode.Text);
            moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_SELF_SERVICE_ROLE_NAME, tbxSelfServiceRoleName.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_ADMIN_EMAIL, tbxAdminEmail.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_CSP_RESET_URL, tbxCspResetUrl.Text);
            moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_EFFECTIVE_DAY, tbxEffectiveDay.Text);
            moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_RECAPTCHA_PUBLIC_KEY, tbxRcPublicKey.Text);
            moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_RECAPTCHA_PRIVATE_KEY, tbxRcPrivateKey.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_REDIRECT_URL, tbxRedirectUrl.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_CHANNEL_BASE_DOMAIN, tbxChannelBaseDomain.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_WEBTRENDS_FOLDER, tbxWebtrendsFolder.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_CONTENT_TYPE_ID, tbxContentTypeId.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_CONTENT_TYPE_FIELD_ID, tbxContentTypeFieldId.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_BRAINTREE_MERCHANT_ID, tbxBraintreeMerchantId.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_BRAINTREE_PUBLIC_KEY, tbxBraintreePublicKey.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_BRAINTREE_PRIVATE_KEY, tbxBraintreePrivateKey.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_BRAINTREE_CLIENT_SIDE_ENCRYPTION_KEY, tbxBraintreeClientSideEncryptionKey.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_BRAINTREE_PLAN_ID, tbxBraintreePlanId.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_GENERAL_SUPPLIER_ID, tbxGeneralSupplierId.Text);
			ModuleController.SynchronizeModule(ModuleId);
		}
	}
}