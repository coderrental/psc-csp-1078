﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainView.ascx.cs" Inherits="CR.DnnModules.SelfService.SupplierSubscription.MainView" %>
<%@ Import Namespace="CR.DnnModules.SelfService.SupplierSubscription.Common" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>
<span runat="server" class="page-message" ID="PageMessage" Visible="False"></span>
<asp:Panel runat="server" ID="ConfirmPane">
	<div id="description-block-right">
		<div id="description-block-right-images"></div>
		<div id="description-block-right-button"><a href= "<%=RedirectLink() %>"><%= GetLocalizedText("ConfirmPage.button")%></a></div>
	</div>
	<div id="description-block">
		<div id="description-text">
			<h1 class="darkblue-text"><%= GetLocalizedText("ConfirmPage.title")%></h1>
			<p class="darkblue-text"><%= GetLocalizedText("ConfirmPage.description1")%></p>
			<p class="darkblue-text"><%= GetLocalizedText("ConfirmPage.description2")%> <a style="color:#ed9737;" href= "mailto:<%= AdminEmail %>" ><%= AdminEmail %>.</a></p>
			<div id="description-image">
				<div class="email-description darkblue-text">
					<%= GetLocalizedText("ConfirmPage.username") %>
				</div>
				<div class="email-description darkblue-text">
					<%= GetLocalizedText("ConfirmPage.password")%>
				</div>
				<div class="clear"></div>								
			</div>
			<p class="darkblue-text"><%= GetLocalizedText("ConfirmPage.description3")%></p>
		</div>
	</div>
	<div class="clear"></div>
</asp:Panel>
<asp:Panel runat="server" ID="WelcomePane">
	<%= GetLocalizedText("Block.Welcome") %>
</asp:Panel>
<asp:Panel runat="server" ID="MainPane">
<div id="wrapper">
	<div id="content-wrapper">
		<div id="indicated-required-msg">
			<%= GetLocalizedText("Label.IndicatedRequiredFields") %>
		</div>
		
		<div class="content-block">
			<div class="block-title-orange">
				<%= GetLocalizedText("Label.CompanyInformation")%>
			</div>
				
			<div class="form-row">
				<div class="short-col" id="companyname-field-col">
					<p class="field-label"><%= GetLocalizedText("Label.CompanyName")%></p>
					<input type="text"  runat="server" id="companyname" name="companyname" class="short-required-field" autocomplete="off" />
					<span class="hide" id="companyname-msg"></span>
				</div>
				<div class="long-col" id="address1-field-col">
					<p class="field-label long-label"><%= GetLocalizedText("Label.Address1")%></p>
					<input type="text"  runat="server" id="address1" name="address1" class="long-required-field" autocomplete="off" />
				</div>
				<div class="short-col" id ="city-field-col">
					<p class="field-label"><%= GetLocalizedText("Label.City")%></p>
					<input type="text"  runat="server" id="city" name="city" class="short-required-field" autocomplete="off" />
				</div>
				<div class="clear"></div>
			</div>
				
			<div class="form-row">
				<div class="short-col item-right-padding" id="state-field-col">
					<p class="field-label"><%= GetLocalizedText("Label.State")%></p>
					<input type="text"  runat="server" id="state" name="state" class="short-required-field" autocomplete="off" />
				</div>
				<div class="short-col item-right-padding" id="zipcode-field-col">
					<p class="field-label"><%= GetLocalizedText("Label.Zipcode")%></p>
					<input type="text"  runat="server" id="zipcode" name="zipcode" class="short-required-field" autocomplete="off" />
				</div>
				<div class="short-col item-right-padding" id="country-field-col">
					<p class="field-label"><%= GetLocalizedText("Label.Country")%></p>
					<select runat="server" ID="rcbCountry" name="country" class="dropdown-country"></select>
                    <%--<input type="text"  runat="server" id="country" name="country" class="short-required-field" />--%>
				</div>
				<div class="short-col" id="companywebsite-field-col">
					<p class="field-label"><%= GetLocalizedText("Label.URL")%></p>
					<input type="text"  runat="server" id="companywebsite" name="companywebsite" class="short-required-field" autocomplete="off" />
					<span class="hide" id="companywebsite-msg"></span>
				</div>
				<div class="clear"></div>
			</div>
		</div>
			
		<div class="content-block">
			<div class="block-title-orange">
				<%= GetLocalizedText("Label.ContactPersonInformation")%>
			</div>
				
			<div class="form-row">
				<div class="short-col item-right-padding" id="firstname-field-col">
					<p class="field-label"><%= GetLocalizedText("Label.FirstName")%></p>
					<input type="text"  runat="server" class="short-required-field" id="firstname" name="firstname" autocomplete="off"/>
				</div>
				<div class="short-col item-right-padding" id="lastname-field-col">
					<p class="field-label"><%= GetLocalizedText("Label.LastName")%></p>
					<input type="text"  runat="server" class="short-required-field" id="lastname" name="lastname" autocomplete="off"/>
				</div>
				<div class="short-col item-right-padding" id="jobtitle-field-col">
					<p class="field-label"><%= GetLocalizedText("Label.JobTitle")%></p>
					<input type="text"  runat="server" id="jobtitle" name="jobtitle" class="short-required-field" autocomplete="off" />
				</div>
				<div class="short-col" id="phonenumber-field-col">
					<p class="field-label"><%= GetLocalizedText("Label.PhoneNumber")%></p>
					<input type="text"  runat="server" id="phonenumber" name="phonenumber" class="short-required-field" autocomplete="off" />
				</div>
				<div class="clear"></div>
			</div>
				
			<div class="form-row">
				<div class="long-col left-align" id="emailaddress-field-col">
					<p class="field-label long-label float-left"><%= GetLocalizedText("Label.EmailAddress")%></p>
					<input type="text"  runat="server" id="emailaddress" name="emailaddress" class="long-required-field" autocomplete="off" />
					<div class="clear"></div>
					<div class="hide" id="emailaddress-msg"></div>
					
				</div>
				<div class="long-col left-align" id="confirm-emailaddress-field-col">
					<p class="field-label long-label float-left"><%= GetLocalizedText("Label.ConfirmEmailAddress")%></p>
					<input type="text" runat="server" id="confirmemailaddress" name="confirm-emailaddress" class="long-required-field" autocomplete="off" />
					<div class="clear"></div>
					<div class="hide" id="confirm-emailaddress-msg"></div>
				</div>
				<div class="clear"></div>
				<div id="mailing-check">
					<input type="checkbox" id="recievemail" runat="server" name="recievemail"/><label for="<%= recievemail.ClientID %>"><%= GetLocalizedText("Label.CheckTheBoxToRecieveMailings")%></label>
				</div>
			</div>
		</div>
		
		<div class="content-block">
			<div class="block-title-orange">
				<%= GetLocalizedText("Label.AccountInformation")%>
			</div>
			<div class="form-row">
				<div class="short-col item-right-padding" id="username-field-col">
					<p class="field-label"><%= GetLocalizedText("Label.UserName")%></p>
					<input type="text"  runat="server" id="username" name="username" class="short-required-field" autocomplete="off" />
					<span class="hide" id="username-msg"></span>
				</div>

				<div class="short-col item-right-padding" id="password-field-col">
					<p class="field-label"><%= GetLocalizedText("Label.Password")%></p>
					<input type="password" runat="server" id="password" name="password" class="short-required-field" autocomplete="off" />
					<input type="text" class="short-required-field" id="fake-password" style="display: none" name="fake-password" autocomplete="off" />
					<div class="hide" id="password-msg"></div>
				</div>
				<div class="short-col" id="password-confirm-field-col">
					<p class="field-label"><%= GetLocalizedText("Label.ConfirmPassword")%></p>
					<input type="password" id="confirm-password" name="confirm-password" class="short-required-field" autocomplete="off" />
					<input type="text" class="short-required-field" id="fake-confirm-password" style="display: none" name="fake-confirm-password" autocomplete="off" />
				</div>
				<div class="clear"></div>
			</div>
			<div class="form-row">
				<div class="long-col" id="basedomain-field-col">
					<p class="field-label long-label"><%= GetLocalizedText("Label.BaseDomain")%></p>
					<div style="overflow: hidden">
						<input type="text" class="short-required-field" id="fake-basedomain-prefix" autocomplete="off" name="fake-basedomain-prefix"/>
						<span id="fake-basedomain-suffix"><%= "." + BaseDomain %></span>
					</div>
					<input type="hidden" runat="server" class="long-required-field" ID="tbxBaseDomain" name="basedomain" autocomplete="off"/>
					<div class="hide" id="basedomain-msg"></div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="content-block">
			<div class="block-title-orange">
				<%= GetLocalizedText("Label.PaymentInfomation")%>
			</div>
			<div id="security-block">
				<p><%= GetLocalizedText("Label.SecurityBlockTitle")%></p>
				<span><%= GetLocalizedText("Label.SecurityBlockDescription")%></span>
			</div>
			<div class="form-row" style="margin-top: 5px;">
					<div class="short-col" id="creditcardnumber-field-col">
						<p class="field-label"><%= GetLocalizedText("Label.CreditCardNumber")%></p>
						<span ><%= GetLocalizedText("Label.CreditCardNumberDescription")%></span>
						<input type="text"  runat="server" id="creditcardnumber" class="short-required-field" autocomplete="off" />
						<span id="creditcard-type"></span>
						<span class="hide" id="creditcardnumber-msg"></span>
					</div>
					<div class="short-col item-right-padding" id="securitycode-field-col">
						<p class="field-label"><%= GetLocalizedText("Label.SecurityCode")%></p>
						<span ><%= GetLocalizedText("Label.SecurityCodeDescription")%></span>
						<input type="text" runat="server" id="securitycode" class="short-required-field" autocomplete="off" />
						<span id="security-icon"></span>
						<span class="hide" id="securitycode-msg"></span>
					</div>
					<div class="short-col" id="expirationdate-field-col">
						<p class="field-label"><%= GetLocalizedText("Label.ExpirationDate")%></p>
						<span ><%= GetLocalizedText("Label.ExpirationDateDescription")%></span>
						<%--<input type="text"  runat="server" id="expirationdate" class="short-required-field" autocomplete="off" />--%>
						<select runat="server" ID="expirationmonth" name="expirationmonth" class="dropdown-month"></select>
						<select runat="server" ID="expirationyear" name="expirationyear" class="dropdown-year"></select>
						<span class="hide" id="expirationdate-msg"></span>
					</div>
					<div class="clear"></div>
					<div id="address-check">
						<input type="checkbox" id="billingaddresscheck" name="billingaddresscheck" runat="server"/>
						<label for="<%= billingaddresscheck.ClientID %>"><%= GetLocalizedText("Label.CheckTheBoxToUseCompanyAddress")%></label>
					</div>	
				<div class="clear"></div>
			</div>
			<div id="billing-address-infomation">
				<div class="form-row">
					<div class="short-col item-right-padding" id="billing-firstname-field-col">
						<p class="field-label"><%= GetLocalizedText("Label.FirstName")%></p>
						<input type="text"  runat="server" class="short-required-field" id="billingfirstname" name="firstname" autocomplete="off"/>
					</div>
					<div class="short-col item-right-padding" id="billing-lastname-field-col">
						<p class="field-label"><%= GetLocalizedText("Label.LastName")%></p>
						<input type="text"  runat="server" class="short-required-field" id="billinglastname" name="lastname" autocomplete="off"/>
					</div>
					<div class="long-col left-align" id="billing-address-field-col">
						<p class="field-label"><%= GetLocalizedText("Label.Address")%></p>
						<input type="text"  runat="server" id="billingaddress" name="billingaddress" class="long-required-field" autocomplete="off" />
					</div>
					<div class="clear"></div>
				</div>
				<div class="form-row">				
					<div class="short-col item-right-padding" id ="billing-city-field-col">
						<p class="field-label"><%= GetLocalizedText("Label.City")%></p>
						<input type="text"  runat="server" id="billingcity" name="billingcity" class="short-required-field" autocomplete="off" />
					</div>
					<div class="short-col item-right-padding" id="billing-state-field-col">
						<p class="field-label"><%= GetLocalizedText("Label.State")%></p>
						<input type="text"  runat="server" id="billingstate" name="billingstate" class="short-required-field" autocomplete="off" />
					</div>
					<div class="short-col item-right-padding" id="billing-country-field-col">
						<p class="field-label"><%= GetLocalizedText("Label.Country")%></p>
						<select runat="server" ID="billingcountry" name="billingcountry" class="dropdown-country"></select>
					</div>
					<div class="short-col" id="billing-zipcode-field-col">
						<p class="field-label"><%= GetLocalizedText("Label.Zipcode")%></p>
						<input type="text"  runat="server" id="billingzipcode" name="billingzipcode" class="short-required-field" autocomplete="off" />
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
        <div class="content-block" id="submit-information">
            <div class="block-title-orange">
                Submit Information
            </div>
			<div class="form-row">
				<span id="infor-label" class="field-label"><%= GetLocalizedText("Label.Infor")%></span>
				<input type="text"  runat="server" id="infor" name="infor" class="short-required-field" autocomplete="off" />
				<span class="hide" id="infor-msg"></span>
			</div>
            <div class="form-row">
                        <div id="accept-terms-conditions">
                            <input type="checkbox" runat="server" id="termsandconditions" name="termsandconditions"/>
                            <label for="<%= termsandconditions.ClientID %>"><%= GetLocalizedText("Label.Accept")%> </label><label><a id="viewTermsAndConditions" href="<%= GetLocalizedText("Terms.Link")%>" style="text-decoration:underline;" title="Terms and Conditions"><%= GetLocalizedText("Label.TermsAndConditions")%></a></label>
							<br><span class="hide" id="accept-msg"></span></br>
						</div>
                        <div id="recaptchar-div">
                            <recaptcha:RecaptchaControl
                                 ID="recaptcha"
                                 runat="server"
                                 Theme="red"
 								 PrivateKey="6LcRneISAAAAAF3Md7RCiu4qyLUK7h7aDDzVJvBd"
								 PublicKey="6LcRneISAAAAAO9ZDh3wHhkFRWt6LGRZxeA7oIXD"
                            />
                            <asp:Label Visible="True" ID="lblResult" runat="server" /> 
                        </div>
                        <div id="summit-div">
                            <div style="text-align: center; padding: 20px 0px;">                                
				               <asp:LinkButton runat="server" id="BtnSubmit" OnClick="BtnSubmitClick" UseSubmitBehaviour="false"><span class="button button-left" >
					                <span class="button-right">
						                <span class="button-mid">
							                <span class="button-text">
								                <%= GetLocalizedText("Btn.Submit")%>
							                </span>
						                </span>
					                </span>
				                </span></asp:LinkButton> 

                                <%--<span class="button button-left" id="BtnCreatNewVendor">
					                <span class="button-right">
						                <span class="button-mid">
							                <span class="button-text">
								                <%= GetLocalizedText("Btn.CreateNewVendor")%>
							                </span>
						                </span>
					                </span>
				                </span>
                                <span class="button button-left" id="BtnSendEmail">
					                <span class="button-right">
						                <span class="button-mid">
							                <span class="button-text">
								                <%= GetLocalizedText("Btn.SendEmail")%>
							                </span>
						                </span>
					                </span>
				                </span>
--%>
                                <input type="hidden" id="btn-email-clicked"  name="btn-email-clicked"/>
                                <input type="hidden" runat="server" id="isvalidcaptcha" name="isvalidcaptcha"/>
				                <div runat="server" ID="ControlMessage"></div>
			                </div>
                        </div>
            </div>
        </div>
	</div>
	 <telerik:RadWindow ID="modalPopup" runat="server" Width="600" Height="600" Behaviors="Close,Move">
	 </telerik:RadWindow>
	 <div id="kendoWnd" style="overflow:hidden;"></div>
	<!-- } EOF Content section -->
</div>
<telerik:RadAjaxLoadingPanel runat="server" ID="RadLoadingPanel" Skin="Vista"></telerik:RadAjaxLoadingPanel>
<telerik:RadScriptBlock runat="server">
<telerik:RadWindowManager runat="server" id="RadWindowManager1" Skin="Metro"></telerik:RadWindowManager>
<script type="text/javascript">
	var ajaxLoadingPanelId = '<%= RadLoadingPanel.ClientID %>';
	var mainPane = '<%= MainPane.ClientID %>';
	var BaseDomain = '<%= BaseDomain %>';
	var CspResetUrl = '<%= CspResetUrl %>';
	var invalidCoName = 0;
	var invalidCoWebsite = 0;
	var invalidEmailAddress = 0;
	var invalidUserName = 0;
	var invalidPassword = 0;
	var invalidBaseDomain = 0;
	var invalidExpirationDate = 0;
	var arraylist = '';
	var braintree = Braintree.create("<%= BraintreeClientSideEncryptionKey %>");
	braintree.onSubmitEncryptForm('Form', ajaxSubmit);
	$(function () {
		var w = $("#kendoWnd").kendoWindow({ modal: true, visible: false, close: function () { $("#kendoWnd").html(""); } }).data("kendoWindow");
		$("#viewTermsAndConditions").click(function (ev) {
			ev.preventDefault();
			var el = $(this);
			w.refresh({ url: el.attr("href") });
			w.title(el.text());
			w.wrapper.css({ width: 900, height: 600 });
			w.center();
			w.open();
			return false;
		});
		ValidateBillingAddress();
		if ($('#<%= tbxBaseDomain.ClientID %>').val() != '') {
			$('#fake-basedomain-prefix').val($('#<%= tbxBaseDomain.ClientID %>').val().split('.')[0]);
		}
		$('#<%= username.ClientID %>').focusout(function () {
			var usernameLength = 4;
			var userName = $(this).val();
			userName = userName.replace(/\W/g, '').replace(/_/g, '').toLowerCase();
			userName = userName.replace(/\s+/g, '');
			$(this).val(userName);
			if ($(this).val() == '' || $(this).val().length < usernameLength) {
				$('#username-field-col').removeClass('field-valid').addClass('field-invalid');
				$('#username-msg').html('<%= GetLocalizedText("Msg.InvalidUsername")%>').show();
				invalidUserName = 1;
				return;
			}
			ValidateUsername(userName);
		});

		$('#<%= password.ClientID %>').focusout(function () {
			var passwordValue = $(this).val();
			$('#fake-password').val(passwordValue);
			if (passwordValue == '' || $('#confirm-password').val() != '')
				ValidatePassword();
		});


		$('#confirm-password').focusout(function () {
			var passwordValue = $(this).val();
			$('#fake-confirm-password').val(passwordValue);
			ValidatePassword();
		});
		$('#address-check').click(function () {
			ValidateBillingAddress();

		});
		$('#<%= companyname.ClientID %>').focusout(function () {
			var conameLength = 2;
			var companyName = $(this).val();
			companyName = companyName.replace(/[^a-zA-Z0-9_. ]/g, "");
			$(this).val(companyName);
			var baseDomain = companyName.replace(/\W/g, '').replace(/_/g, '').toLowerCase();
			var fakeBaseDomainPrefix = '';
			if (endsWith(baseDomain, '.'))
				baseDomain = baseDomain.substring(0, baseDomain.length - 1);
			fakeBaseDomainPrefix = baseDomain;
			baseDomain += '.' + BaseDomain;
			if ($(this).val() == '' || $(this).val().length < conameLength) {
				$('#companyname-field-col').removeClass('field-valid').addClass('field-invalid');
				$('#companyname-msg').html('<%= GetLocalizedText("Msg.InvalidCompanyName")%>').show();
				invalidCoName = 1;
				return;
			}
			//Long edit code 19/06/2013
			if ($('#fake-basedomain-prefix').val() == '') {
				$('#fake-basedomain-prefix').val(fakeBaseDomainPrefix);
				$('#<%= tbxBaseDomain.ClientID %>').val(baseDomain);
				ValidateBaseDomain(baseDomain);
			}
			$('#companyname-msg').html('').hide();
			$('#companyname-field-col').removeClass('field-invalid').addClass('field-valid');
			/*$.ajax({
			type: 'POST',
			url: document.URL,
			dataType: 'text',
			data: { action: 'companyname_validation', companyname: $(this).val() },
			beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
			},
			success: function (responseMsg) {
			if (responseMsg == '1') {
			$('#companyname-msg').html('').hide();
			$('#companyname-field-col').removeClass('field-invalid').addClass('field-valid');
			$('#fake-basedomain-prefix').val(fakeBaseDomainPrefix);
			$('#<%= tbxBaseDomain.ClientID %>').val(baseDomain);
			ValidateBaseDomain(baseDomain);
			invalidCoName = 0;
			return;
			}
			$('#companyname-msg').html('"' + companyName + '" <%= GetLocalizedText("Msg.IsAlreadyTaken")%>').show();
			$('#companyname-field-col').removeClass('field-valid').addClass('field-invalid');
			invalidCoName = 1;
			}
			});*/
		});

		$('#<%= companywebsite.ClientID %>').focusout(function () {
			var theUrl = $(this).val().replace(/\s/g, '');
			$(this).val(theUrl);
			ValidateCompnayUrl(theUrl);
			if (invalidCoWebsite == 0) {
				if (theUrl.indexOf('http') < 0)
					$(this).val('http://' + theUrl);
			}
		});

		$('#<%= emailaddress.ClientID %>').focusout(function () {
			var emailAddress = $(this).val();
			ValidateUserEmail(emailAddress);
			ValidateMatchEmails();
		});

		$('#<%= confirmemailaddress.ClientID %>').focusout(function () {
			var emailAddress = $(this).val();
			ValidateConfirmUserEmail(emailAddress);
		});

		$('#<%= tbxBaseDomain.ClientID %>').focusout(function () {
			if ($(this).val() == '') {
				$('#basedomain-field-col').removeClass('field-valid').addClass('field-invalid');
				$('#basedomain-msg').html('<%= GetLocalizedText("Msg.InvalidBaseDomain")%>').show();
				invalidBaseDomain = 1;
				return;
			}
			ValidateBaseDomain($(this).val());
		});

		$('#fake-basedomain-prefix').focusout(function () {
			if ($(this).val() == '') {
				$('#basedomain-field-col').removeClass('field-valid').addClass('field-invalid');
				$('#basedomain-msg').html('<%= GetLocalizedText("Msg.InvalidBaseDomain")%>').show();
				invalidBaseDomain = 1;
				return;
			}
			var baseDomainPrefix = $(this).val().replace(/\W/g, '').replace(/_/g, '').toLowerCase();
			baseDomainPrefix = baseDomainPrefix.replace(/\s+/g, '');
			$(this).val(baseDomainPrefix);
			var fullBaseDomain = $.trim(baseDomainPrefix) + $.trim($('#fake-basedomain-suffix').html());
			ValidateBaseDomain(fullBaseDomain);
			if (invalidBaseDomain == 0)
				$('#<%= tbxBaseDomain.ClientID %>').val(fullBaseDomain);
		});

		$('#<%= BtnSubmit.ClientID %>').unbind().click(function (e) {
			ValidateSubmit(e);
		});

		/*$("#viewTermsAndConditions").click(function (ev) {
		ev.preventDefault();
		var oWnd = $find("<%=modalPopup.ClientID%>");
		oWnd.show();
		oWnd.setUrl('<%= GetLocalizedText("Terms.Link")%>');
		return false;
		});*/
	});

	function ValidateBillingAddress() {
		if ($('#<%= billingaddresscheck.ClientID %>').is(":checked")) {
			$('#billing-address-infomation').removeClass('hide');
		} else {
			$('#billing-address-infomation').addClass('hide');

		}
	}
	function validateExpirationDate() {
		var month = $('#<%= expirationmonth.ClientID %>').val();
		var year = $('#<%= expirationyear.ClientID %>').val();
		var currentYear = new Date().getFullYear();
		var currentMonth = new Date().getMonth()+1;
		if (month < 1 || month > 12 || year < currentYear || parseInt(month) + parseInt(year) < currentMonth + currentYear)
			return false;
		return true;
			
	}
	function CloseRadAlert() {
		var closeRadAlertMethod = $('.rwPopupButton').attr('onclick');
		eval(closeRadAlertMethod);
		console.log(closeRadAlertMethod);
	}
	function UpdateRealBaseDomain() {
		var baseDomainPrefix = $('#fake-basedomain-prefix').val().replace(/\W/g, '').replace(/_/g, '').toLowerCase();
		baseDomainPrefix = baseDomainPrefix.replace(/\s+/g, '');
		$(this).val(baseDomainPrefix);
		var fullBaseDomain = $.trim(baseDomainPrefix) + $.trim($('#fake-basedomain-suffix').html());
		$('#<%= tbxBaseDomain.ClientID %>').val(fullBaseDomain);
	}
	
	function ValidatePassword() {
		if ($('#<%= password.ClientID %>').val() == '' || $('#<%= password.ClientID %>').val().length < 7) {
			invalidPassword = 1;
			$('#password-msg').html('<%= GetLocalizedText("Msg.InvalidPassword")%>').show();
			return;
		}
		if ($('#<%= password.ClientID %>').val() != $('#confirm-password').val()) {
			invalidPassword = 1;
			$('#password-msg').html('<%= GetLocalizedText("Msg.PasswordNotMatch")%>').show();
		} else {
			invalidPassword = 0;
			$('#password-msg').html('').hide();
		}
	}
	function ValidateUserEmail(emailAddress) {
		if (!ValidateEmail(emailAddress)) {
			$('#emailaddress-field-col').removeClass('field-valid').addClass('field-invalid');
			$('#emailaddress-msg').html('<%= GetLocalizedText("Msg.InvalidEmailAddress")%>').show();
			invalidEmailAddress = 1;
		} else {
			$('#emailaddress-field-col').removeClass('field-invalid').addClass('field-valid');
			$('#emailaddress-msg').hide();
			invalidEmailAddress = 0;
		}
	}
	
	function ValidateConfirmUserEmail(emailAddress) {
		invalidEmailAddress = 1;	
		if (!ValidateEmail(emailAddress)) {
			$('#confirm-emailaddress-field-col').removeClass('field-valid').addClass('field-invalid');
			$('#confirm-emailaddress-msg').html('<%= GetLocalizedText("Msg.InvalidEmailAddress")%>').show();
		} else if ($.trim(emailAddress) == $.trim($('#<%= emailaddress.ClientID %>').val())) {
			$('#confirm-emailaddress-field-col').removeClass('field-invalid').addClass('field-valid');
			$('#confirm-emailaddress-msg').hide();
			invalidEmailAddress = 0;
		} else {
			$('#confirm-emailaddress-field-col').removeClass('field-valid').addClass('field-invalid');
			$('#confirm-emailaddress-msg').html('<%= GetLocalizedText("Msg.EmailAddressNotMatch")%>').show();
		}
	}

	function ValidateMatchEmails() {
		if ($.trim($('#<%= emailaddress.ClientID %>').val()) != $.trim($('#<%= confirmemailaddress.ClientID %>').val())) {
			invalidEmailAddress = 1;
			$('#confirm-emailaddress-msg').html('<%= GetLocalizedText("Msg.EmailAddressNotMatch")%>').show();
		} else if ($.trim($('#<%= emailaddress.ClientID %>').val() == '')) {
			invalidEmailAddress = 0;
			$('#confirm-emailaddress-msg').html('').hide();
		}
	}
	
	function ValidateUsername(userName) {
		$.ajax({
			type: 'POST',
			url: document.URL,
			dataType: 'text',
			data: { action: 'username_validation', username: userName },
			beforeSend: function (xhr) {
				xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
			},
			success: function (responseMsg) {
				if (responseMsg == '1') {
					$('#username-msg').html('').hide();
					$('#username-field-col').removeClass('field-invalid').addClass('field-valid');
					invalidUserName = 0;
					return;
				}
				$('#username-msg').html('"' + userName + '" <%= GetLocalizedText("Msg.IsAlreadyTaken")%>').show();
				$('#username-field-col').removeClass('field-valid').addClass('field-invalid');
				invalidUserName = 1;
			}
		});
	}

	function ValidateBaseDomain(baseDomain) {
		$.ajax({
			type: 'POST',
			url: document.URL,
			dataType: 'text',
			data: { action: 'basedomain_validation', basedomain: baseDomain },
			beforeSend: function (xhr) {
				xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
			},
			success: function (responseMsg) {
				if (responseMsg == '1') {
					$('#basedomain-msg').html('').hide();
					$('#basedomain-field-col').removeClass('field-invalid').addClass('field-valid');
					invalidBaseDomain = 0;
					return;
				}
				$('#basedomain-msg').html('"' + baseDomain + '" <%= GetLocalizedText("Msg.IsAlreadyTaken")%>').show();
				$('#basedomain-field-col').removeClass('field-valid').addClass('field-invalid');
				invalidBaseDomain = 1;
			}
		});
	}

	function endsWith(str, suffix) {
		return str.indexOf(suffix, str.length - suffix.length) !== -1;
	}

	function ValidateCompnayUrl(theUrl) {
		if (!ValidateURL(theUrl)) {
			$('#companywebsite-field-col').removeClass('field-valid').addClass('field-invalid');
			$('#companywebsite-msg').html('<%= GetLocalizedText("Msg.InvalidUrl")%>').show();
			invalidCoWebsite = 1;
		} else {
			$('#companywebsite-field-col').removeClass('field-invalid').addClass('field-valid');
			$('#companywebsite-msg').hide();
			invalidCoWebsite = 0;
		}
	}

	function ValidateURL(textval) {
		//var urlregex = new RegExp("^(http:\/\/|https:\/\/|ftp:\/\/){1}(www\.){0,1}([0-9A-Za-z]+\.)");
		return /^(http:\/\/|https:\/\/|ftp:\/\/){0,1}(www\.){0,1}([0-9A-Za-z]+\.)/.test(textval);
	}

	function ValidateEmail(email) {
		var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return regex.test(email);
	}
	function switchLoadingPanel(flag) {
		var loadingPanel = $find(ajaxLoadingPanelId);
		if (loadingPanel == null) {
			return;
		}
		if (flag) {
			loadingPanel.show(mainPane);
			return;
		}
		loadingPanel.hide(mainPane);
	}
	function ValidateInput() {
		var result = true;
		if ($('#<%= companyname.ClientID %>').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingCompanyName")%>' + "<br>";
			result = false;
		}
		if ($('#<%= address1.ClientID %>').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingAddress1")%>' + "<br>";
			result = false;
		}
		if ($('#<%= city.ClientID %>').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingCity")%>' + "<br>";
			result = false;
		}
		if ($('#<%= state.ClientID %>').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingState")%>' + "<br>";
			result = false;
		}
		if ($('#<%= rcbCountry.ClientID %>').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingCountry")%>' + "<br>";
			result = false;
		}
		if ($('#<%= zipcode.ClientID %>').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingZipCode")%>' + "<br>";
			result = false;
		}
		if ($('#<%= companywebsite.ClientID %>').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingCompanyWebsite")%>' + "<br>";
			result = false;
		}
		if ($('#<%= firstname.ClientID %>').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingFirstName")%>' + "<br>";
			result = false;
		}
		if ($('#<%= lastname.ClientID %>').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingLastName")%>' + "<br>";
			result = false;
		}
		if ($('#<%= jobtitle.ClientID %>').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingJobTitle")%>' + "<br>";
			result = false;
		}
		if ($('#<%= phonenumber.ClientID %>').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingPhoneNumber")%>' + "<br>";
			result = false;
		}
		if ($('#<%= emailaddress.ClientID %>').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingEmailAddress")%>' + "<br>";
			result = false;
		}
		if ($('#<%= confirmemailaddress.ClientID %>').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingConfirmEmailAddress")%>' + "<br>";
			result = false;
		}
		if ($('#<%= username.ClientID %>').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingUserName")%>' + "<br>";
			result = false;
		}
		if ($('#<%= password.ClientID %>').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingPassword")%>' + "<br>";
			result = false;
		}
		if ($('#confirm-password').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingConfirmPassword")%>' + "<br>";
			result = false;
		}
		if ($('#fake-basedomain-prefix').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingBaseDomain")%>' + "<br>";
			result = false;
		}
		if ($('#<%=creditcardnumber.ClientID %>').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingCreditCardNumber")%>' + "<br>";
			result = false;
		}
		if ($('#<%=securitycode.ClientID %>').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingSecurityCode")%>' + "<br>";
			result = false;
		}

		if ($('#<%=expirationmonth.ClientID %>').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingExpirationMonth")%>' + "<br>";
			result = false;
		}
		if ($('#<%=expirationyear.ClientID %>').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingExpirationYear")%>' + "<br>";
			result = false;
		}
		if ($('#<%= infor.ClientID %>').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingInfor")%>' + "<br>";
			result = false;
		}
		if ($('#recaptcha_response_field').val() == '') {
			arraylist += '<%= GetLocalizedText("Msg.MissingCaptcha")%>' + "<br>";
			result = false;
		}
		if ($('#<%= billingaddresscheck.ClientID %>').is(":checked")) {
			if ($('#<%=billingfirstname.ClientID %>').val() == '') {
				arraylist += '<%= GetLocalizedText("Msg.MissingBillingFirstName")%>' + "<br>";
				result = false;
			}
			if ($('#<%=billinglastname.ClientID %>').val() == '') {
				arraylist += '<%= GetLocalizedText("Msg.MissingBillingLastName")%>' + "<br>";
				result = false;
			}
			if ($('#<%=billingaddress.ClientID %>').val() == '') {
				arraylist += '<%= GetLocalizedText("Msg.MissingBillingAddress")%>' + "<br>";
				result = false;
			}
			if ($('#<%=billingcity.ClientID %>').val() == '') {
				arraylist += '<%= GetLocalizedText("Msg.MissingBillingCity")%>' + "<br>";
				result = false;
			}
			if ($('#<%=billingstate.ClientID %>').val() == '') {
				arraylist += '<%= GetLocalizedText("Msg.MissingBillingState")%>' + "<br>";
				result = false;
			}
			if ($('#<%=billingzipcode.ClientID %>').val() == '') {
				arraylist += '<%= GetLocalizedText("Msg.MissingBillingZipCode")%>' + "<br>";
				result = false;
			}
			if ($('#<%=billingcountry.ClientID %>').val() == '') {
				arraylist += '<%= GetLocalizedText("Msg.MissingBillingCountry")%>' + "<br>";
				result = false;
			}
		}
		return result;
	}
	function resizePopup(oAlert) {
		var contentElement = oAlert.get_contentElement();
		var height = contentElement.offsetHeight;
		var width = contentElement.offsetWidth;
		if (height < 180) height = 180;
		if (width < 330) width = 330;
		oAlert.setSize(width, height);
	} 
    function ValidateSubmit(e) {
    	var isAllFilled = true;
    	var buttonTemplate = '<span id="btnRadalertOK" onclick="javascript: CloseRadAlert();" class="button button-left"><span class="button-right"><span class="button-mid"><span class="button-text"> OK </span></span></span></span>';
    	//console.log($('#wrapper').find('input[type="text"]').length);
        $('#wrapper').find('input[type="text"]').each(function () {
            if ($.trim($(this).val()) == '')
            	isAllFilled = false;
        });
        if ($('#<%= password.ClientID %>').val() == '' || $('#<%= password.ClientID %>').val() != $('#confirm-password').val()) {
            invalidPassword = 1;
        }
        if (!ValidateInput()) {
        	var oAlert = radalert('<%= GetLocalizedText("Msg.FillAllRequiredFields")%>' + "<br>" + arraylist + "<br>" + buttonTemplate, 330, 180, '<%= GetLocalizedText("Msg.MissingTitle")%>');
        	resizePopup(oAlert);
        	oAlert.center();
        	arraylist = '';
           	e.preventDefault();
           	return;
           }
        if (!validateExpirationDate()) {
			var oaAlert = radalert('<%= GetLocalizedText("Msg.FillAllRequiredFields")%>' + "<br>" + '<%= GetLocalizedText("Msg.InvalidExpriationDate")%>' + "<br>" + "<br>" + buttonTemplate, 330, 180, '<%= GetLocalizedText("Msg.MissingTitle")%>');
			resizePopup(oaAlert);
			oaAlert.center();
			e.preventDefault();
			return;
        }
    	ValidateMatchEmails();
    	ValidatePassword();
        if (invalidCoWebsite + invalidEmailAddress + invalidUserName + invalidPassword + invalidBaseDomain +invalidExpirationDate > 0) {
        	radalert('<%= GetLocalizedText("Msg.CheckRequiredFields")%>', 330, 210, '<%= GetLocalizedText("Msg.MissingTitle")%>');
            e.preventDefault();
            return;
        }
        if ($('#<%= termsandconditions.ClientID %>').is(':checked') == false) {
        	radalert('<%= GetLocalizedText("Msg.AcceptTermsConditions") %>', 330, 210, '<%= GetLocalizedText("Msg.MissingTitle")%>');
        	e.preventDefault();
        	return;
        }
        UpdateRealBaseDomain();
    	switchLoadingPanel(true);
    	
    }
    
    function ReloadCsp() {
        if (CspResetUrl != '') {
        	if (CspResetUrl.indexOf('http') == -1) CspResetUrl = 'http://' + CspResetUrl;
        	$.get(CspResetUrl);
        }
       }
       //Long add code
    var ajaxSubmit = function (d) {
    var form = $('#Form');
       	d.preventDefault();
       	$.post(form.attr('action'), form.serialize(), function (data) {
       		form.parent().replaceWith(data);
       	});
    };
       //End long code
</script>
</telerik:RadScriptBlock>
</asp:Panel>