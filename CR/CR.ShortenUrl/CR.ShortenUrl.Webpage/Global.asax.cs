﻿using System.Net;
using System.Web;
using Elmah;
using MvcExtensions.Unity;

namespace CR.ShortenUrl.Webpage
{
    public class MvcApplication : UnityMvcApplication
    {
        public void ErrorLog_Filtering(object sender, ExceptionFilterEventArgs e)
        {
            var exception = e.Exception.GetBaseException() as HttpException;

            if ((exception != null) && (exception.GetHttpCode() == (int) HttpStatusCode.NotFound))
            {
                e.Dismiss();
            }
        }
    }
}