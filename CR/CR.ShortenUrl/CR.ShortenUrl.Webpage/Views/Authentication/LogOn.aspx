﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    CR Shorten Url : Log On
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <form class="openid" action="<%= ViewContext.HttpContext.Request.RawUrl %>" method="post">
    <div class="logonform">
        <div class="logonform-main">
            <div class="logonform-heading">Log In</div>
            <p>Enter your Username and Password</p>
            <table>
                <tr>
                    <td width="200">Username</td>
                    <td><input type="text" id="logon_username" name="logon_username" class="defaultTxbox" /></td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td><input type="password" id="logon_password" name="logon_password" class="defaultTxbox" /></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <label><input id="logon_remember" name="logon_remember" type="checkbox" value="true"/>Keep me logged in on this computer</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" value="Log on" class="logonBtn"/></td>
                </tr>
            </table>
            <p><%= Html.CustomValidationMessage("userName") %><%= Html.ValidationMessage("userName", new {style = "padding:0 10px"}) %></p>
        </div>
    </div>
          </form>
    <% Html.Telerik()
           .ScriptRegistrar()
           .OnDocumentReady(() =>
               { %>
        $('form.openid').openid();
    <% }
           ); %>
</asp:Content>