﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    CR Shorten Url : Log Off
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm())
               { %>
     <div class="logonform">
        <!--<div class="logonform-logo"></div>-->
        <div class="logonform-main">
            <div class="logonform-heading">Log Off</div>
            <table>
                <tr>
                    <td width="200">Are you sure you want to log off?</td>
                    <td><input type="submit" value="Log off" class="logonBtn"/></td>
                </tr>
            </table>
        </div>
    </div>
     <% } %>
</asp:Content>