﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    CR Shorten Url : Setup
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <form class="openid" action="<%= ViewContext.HttpContext.Request.RawUrl %>" method="post">
    <div class="logonform">
        <div class="logonform-main">
            <div class="logonform-heading">Setup</div>
            <p>You need to create Administrator account first</p>
            <table>
                <tr>
                    <td width="200">Username</td>
                    <td><input type="text" id="createadmin_name" name="createadmin_name" class="defaultTxbox" /></td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td><input type="password" id="createadmin_password" name="createadmin_password" class="defaultTxbox" /></td>
                </tr>
                  <tr>
                    <td>Email</td>
                    <td><input type="text" id="createadmin_email" name="createadmin_email" class="defaultTxbox" /></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" value="Create Administrator account" class="logonBtn"/></td>
                </tr>
            </table>
            <p><%= Html.CustomValidationMessage("userName") %><%= Html.ValidationMessage("userName", new {style = "padding:0 10px"}) %></p>
        </div>
    </div>
          </form>
    <% Html.Telerik()
           .ScriptRegistrar()
           .OnDocumentReady(() =>
               { %>
        $('form.openid').openid();
    <% }
           ); %>
</asp:Content>
