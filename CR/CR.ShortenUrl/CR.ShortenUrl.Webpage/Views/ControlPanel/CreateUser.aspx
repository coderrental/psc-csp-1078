﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
   CR Shorten Url : Control Panel
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="shrinkBox">
        <div class="shrinkForm detailForm">
              <% using (Html.BeginForm("CreateUser", "ControlPanel", FormMethod.Post))
               { %>
             <div style="margin-bottom:20px;"><span class="heading">Create new User</span></div>
               
             <p>
                 <label style="float: left; width: 100px">Name:</label>
                 <input type="text" name="createuser_name" id="createuser_name"/>
             </p>
            <p>
                 <label style="float: left; width: 100px">Email:</label>
                 <input type="text" name="createuser_email" id="createuser_email"/>
             </p>
            <p>
                 <label style="float: left; width: 100px">Api Access:</label>
                 <select name="createuser_dailyLimit" id="createuser_dailyLimit">
                    <option value="-1">Not Allowed</option>
                    <option value="250">250</option>
                    <option value="500">500</option>
                    <option value="1000">1000</option>
                    <option value="2500">2500</option>
                    <option value="0" selected="selected">Unlimited</option>
                </select>
             </p>
            <p>
                 <input type="submit" value="Create new User" class="command"/><a class="command" href="<%= Url.Users(1) %>" style="margin-left: 20px">« Back to Users</a>
            </p>
            <p><%= Html.CustomValidationMessage("createuser") %><%= Html.ValidationMessage("createuser", new {style = "padding:0 10px; color:#000;"}) %></p>
            <% } %>
         </div>
</div>

</asp:Content>
