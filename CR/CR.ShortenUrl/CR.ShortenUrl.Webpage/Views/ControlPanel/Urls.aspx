﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<GridModel<ShortUrlDTO>>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    CR Shorten Url : Control Panel
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="shrinkBox">
        <div class="shrinkForm">
            <% Html.Telerik()
                   .TabStrip()
                   .Name("tabs")
                   .Items(tabs =>
                       {
                           tabs.AddSummary();

                           tabs.Add()
                               .Text("Urls")
                               .Selected(true)
                               .Content(() =>
                                   { %>
                    <% Html.Telerik()
                           .Grid(Model.Data)
                           .Name("urls")
                           .PrefixUrlParameters(false)
                           .Columns(columns =>
                               {
                                   columns.Bound(u => u.Domain).Width(150).Format("<a href=\"http://{0}\" title='{0}'>{0}</a>").Encoded(false);
                                   columns.Bound(u => u.Title).Width(350);
                                   columns.Bound(u => u.Alias).Width(80).Format("<a href=\"" + Url.Content("~/") + "{0}\" target='_blank' >{0}</a>").Encoded(false);
                                   columns.Bound(u => u.Visits).Width(100);
                                   columns.Bound(u => u.CreatedAt).Width(150).Format("{0:G}").Title("Created");
                                   columns.Bound(o => o.UserName).Template(o => 
                                    {
                                         %> <a href='<%= Url.User(o.UserId) %>'><%= o.UserName %></a> <%  
                                    }).Title("User");
                                   columns.Bound(u => u.Alias).Width(75).Format("<a class=\"command\" href=\"Url/{0}\">Details</a>").Encoded(false).Filterable(false).Sortable(false).Title("Action");
                               })
                           .DataBinding(databinding => databinding.Ajax().Enabled(false))
                           .Filterable()
                           .Pageable(page => page.PageSize(20))
                           //.Scrollable(scrolling => scrolling.Height(250))
                           //.Sortable(sort => sort.SortMode(GridSortMode.MultipleColumn).OrderBy(orderby => orderby.Add(u => u.CreatedAt).Descending()))
                           .Render(); %>
            <% }
                               );

                           tabs.AddUsers()
                               .AddBannedIPs()
                               .AddBannedDomains()
                               .ShortenUrl();
                           //.AddBadWords();
                       }
                   )
                   .Render(); %>
        </div>
    </div>
</asp:Content>