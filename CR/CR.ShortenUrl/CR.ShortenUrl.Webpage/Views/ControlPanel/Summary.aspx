﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<GridModel<ShortUrlDTO>>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    CR Shorten Url : Control Panel
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="shrinkBox">
        <div class="shrinkForm">
            <% Html.Telerik()
                   .TabStrip()
                   .Name("tabs")
                   .Items(tabs =>
                       {
                           tabs.Add()
                               .Text("Summary")
                               .Selected(true)
                               .Content(() =>
                                   { %>
                                     <% using (Html.BeginForm("Summary", "ControlPanel", FormMethod.Post))
                                        {%>
                                    <div class="summary-header"><label>Statistics by:</label> 
                                        <%= Html.DropDownList("listDays") %>
                                    </div> 
                
                                    <% } %>
                                    <div id="chart"></div>
                                    <div style="text-align: center;margin-top: 20px;"><span class="summary-title">Top URLs Clicks</span></div>
                                    <div id="grid-user">
                                         <% Html.Telerik()
                                           .Grid(Model.Data)
                                           .Name("urls")
                                           .PrefixUrlParameters(false)
                                           .Columns(columns =>
                                               {
                                                   columns.Bound(u => u.Domain).Width(150).Format("<a href=\"http://{0}\" title='{0}'>{0}</a>").Encoded(false);
                                                   columns.Bound(u => u.Title).Width(350);
                                                   columns.Bound(u => u.Alias).Width(80).Format("<a href=\"" + Url.Content("~/") + "{0}\" target='_blank' >{0}</a>").Encoded(false);
                                                   columns.Bound(u => u.Visits).Width(100);
                                                   columns.Bound(u => u.CreatedAt).Width(150).Format("{0:G}").Title("Created");
                                                   columns.Bound(o => o.UserName).Template(o => 
                                                    {
                                                         %> <a href='<%= Url.User(o.UserId) %>'><%= o.UserName %></a> <%  
                                                    }).Title("User");
                                                   //columns.Bound(u => u.Alias).Width(75).Format("<a class=\"command\" href=\"Url/{0}\">Details</a>").Encoded(false).Filterable(false).Sortable(false).Title("Action");
                                               })
                                           .DataBinding(databinding => databinding.Ajax().Enabled(false))
                                           .Filterable()
                                           .Pageable(page => page.PageSize(20))
                                           //.Scrollable(scrolling => scrolling.Height(250))
                                           //.Sortable(sort => sort.SortMode(GridSortMode.MultipleColumn).OrderBy(orderby => orderby.Add(u => u.CreatedAt).Descending()))
                                           .Render(); 
                                          %>
                                    </div>
            <% }
                               );

                           tabs.AddUrls()
                               .AddUsers()
                               .AddBannedIPs()
                               .AddBannedDomains()
                               .ShortenUrl();
                           //.AddReservedAliases()
                           //.AddBadWords();
                       }
                   )
                   .Render(); %>
        </div>
        <script src="scripts/summary.js"></script>
        <script type="text/javascript">
            jQuery(function () {
                $("#listDays").change(function () {
                    $('form').submit();
                });
                $("#chart").kendoChart({
                    title: {
                        text: "Statics by day (<%= ViewData["staticsDay"] %>)",
                        color: "#023e79"
                    },
                    legend: {
                        position: "top"
                    },
                    seriesDefaults: {
                        type: "column"
                    },
                    series: [{
                        name: "Number Of URLs",
                        data: [<%= ViewData["listUrlString"] %>]
                    }, ],
                    valueAxis: {
                        line: {
                            visible: false
                        },
                        majorUnit: 1,
                    },
                    categoryAxis: {
                        categories: [<%= ViewData["listUserString"] %>],
                        majorGridLines: {
                            visible: false
                        }
                    },
                    tooltip: {
                        visible: true,
                        format: "{0}"
                    }
                });
            });
        </script>
    </div>
</asp:Content>