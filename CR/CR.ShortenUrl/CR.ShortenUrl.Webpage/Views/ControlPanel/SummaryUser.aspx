﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    CR Shorten Url : Control Panel
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="shrinkBox">
        <div class="shrinkForm">
            <% Html.Telerik()
                   .TabStrip()
                   .Name("tabs")
                   .Items(tabs =>
                       {
                           tabs.Add()
                               .Text("Summary")
                               .Selected(true)
                               .Content(() =>
                                   { %>
                                    <div class="summary-wrapper">
                                         <div id="chart"></div>
                                    <select id="slurl">
                                      <option value="" disabled selected style="display: none">Please select URL</option>
                                      <option value="1">Google</option>
                                      <option value="2">GameVn</option>
                                      <option value="3">TinhTe</option>
                                    </select>
                                    <div id="chart2"></div>
                                    </div>
                                   
            <% }
                               );

                           tabs.AddUrls()
                               .AddUsers()
                               .AddBannedIPs()
                               .AddBannedDomains()
                               .ShortenUrl();
                           //.AddReservedAliases()
                           //.AddBadWords();
                       }
                   )
                   .Render(); %>
        </div>
        
        <script src="../scripts/summary.js"></script>
        <script type="text/javascript">
            jQuery(function () {
                $("#chart").kendoChart({
                    title: {
                        text: "Top ten URL's Visitors",
                        color: "#023e79"
                    },
                    legend: {
                        visible: false
                    },
                    seriesDefaults: {
                        type: "bar"
                    },
                    series: [{
                        name: "Total Visits",
                        data: [500, 300, 200, 700, 900, 250, 450, 300, 650, 100]
                    }],
                    valueAxis: {
                        max: 1000,
                        line: {
                            visible: false
                        },
                        minorGridLines: {
                            visible: true
                        }
                    },
                    categoryAxis: {
                        categories: ["DFS Login", "Shrinkr - An ASP.NET MVC", "Kenh14", "Url Shrinking Service", "Url Shrinking Service", "Google", "Tintuc", "24h", "BongDa", "TruyenCuoi"],
                        majorGridLines: {
                            visible: false
                        }
                    },
                    tooltip: {
                        visible: true,
                        template: "#= series.name #: #= value #"
                    }
                });
                
                $("#chart2").kendoChart({
                    title: {
                        text: "Statistics of visits Google",
                        color: "#023e79"
                    },
                    legend: {
                        position: "top"
                    },
                    seriesDefaults: {
                        type: "column"
                    },
                    series: [{
                        name: "Visitors",
                        data: [2000, 1000, 1500, 3000, 4000, 2000, 2500, 3500, 2800, 2900, 1200, 3400]
                    }],
                    valueAxis: {
                        labels: {
                            format: "0"
                        },
                        line: {
                            visible: false
                        },
                        axisCrossingValue: 0
                    },
                    categoryAxis: {
                        categories: ["Dec", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov"],
                        line: {
                            visible: false
                        },

                    },
                    tooltip: {
                        visible: true,
                        format: "{0}%",
                        template: "#= series.name #: #= value #"
                    }
                });
            });
        </script>
    </div>

</asp:Content>
