﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    CR Shorten Url : Control Panel
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="shrinkBox">
        <div class="shrinkForm">
            <% Html.Telerik()
                   .TabStrip()
                   .Name("tabs")
                   .Items(tabs =>
                       {
                           tabs.AddSummary().AddUrls();
                           tabs.AddUsers()
                               .AddBannedIPs()
                               .AddBannedDomains();
                           tabs.Add()
                               .Text("Shorten Url")
                               .Selected(true)
                               .Content(() =>
                                   { %>
             <% using (Html.BeginForm("ShortenUrl", "ControlPanel", FormMethod.Post))
               { %>
            <div class="shortenurl-wrapper">
            <p><label>Original Url:</label> <input type="text" id="url" name="url" class="url" value="<%=ViewData["OriginalUrl"] %>"/></p>   
            <% var url = string.Empty;
                if (ViewData["ShortenUrl"] != null)
                {
                    url = ViewData["ShortenUrl"].ToString();
                } %>
            <p><label>User:</label><%= Html.DropDownList("ListUsers") %></p> 
            <p><input type="submit" value="Shorten Url" class="smallButton"/></p>
                <% if (!string.IsNullOrEmpty(url))
                   { %>
                      <div class="shortenurl-border">       
                        <span class="shortenurl"><%=url %></span>
                     </div>   
                <% } %>

          
            <p><%= Html.CustomValidationMessage("shortenurl") %><%= Html.ValidationMessage("shortenurl", new {style = "padding:0 10px; color:#000;"}) %></p></div>
               <% } 
            %>
            <% }
                               );
                       }
                   )
                   .Render(); %>
        </div>
    </div>

</asp:Content>
