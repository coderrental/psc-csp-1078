﻿using System;
using System.Web.Security;
using Shrinkr.DataTransferObjects;
using Shrinkr.DomainObjects;
using Shrinkr.Extensions;
using Shrinkr.Infrastructure;
using Shrinkr.Repositories;
using System.Linq;
using System.Collections;

namespace Shrinkr.Services
{
    public class UserService : IUserService
    {
        private readonly IEventAggregator eventAggregator;
        private readonly Settings settings;
        private readonly IUnitOfWork unitOfWork;
        private readonly IUserRepository userRepository;

        public UserService(Settings settings, IUserRepository userRepository, IUnitOfWork unitOfWork,
                           IEventAggregator eventAggregator)
        {
            this.settings = settings;
            this.userRepository = userRepository;
            this.unitOfWork = unitOfWork;
            this.eventAggregator = eventAggregator;
        }

        public UserDTO GetByName(string name)
        {
            User user = userRepository.GetByName(name);

            return (user != null) ? new UserDTO(user) : null;
        }

        public UserResult Save(string userName, string email)
        {
            UserResult result =
                Validation.Validate<UserResult>(() => string.IsNullOrWhiteSpace(userName), "userName",
                                                TextMessages.UserNameCannotBeBlank)
                          .And(() => !string.IsNullOrWhiteSpace(email) && !email.IsEmail(), "email",
                               TextMessages.EmailAddressIsNotInCorrectFormat)
                          .Result();

            if (result.RuleViolations.IsEmpty())
            {
                User user = userRepository.GetByName(userName);
                User tempUser = user;

                result =
                    Validation.Validate<UserResult>(() => (tempUser != null) && tempUser.IsLockedOut, "userName",
                                                    TextMessages.UserIsCurrentlyLockedOut.FormatWith(userName)).Result();

                if (result.RuleViolations.IsEmpty())
                {
                    bool isNewUser = false;

                    if (user == null)
                    {
                        user = new User {Name = userName};

                        if (settings.Api.Allowed)
                        {
                            user.AllowApiAccess(settings.Api.DailyLimit);
                        }

                        userRepository.Add(user);

                        isNewUser = true;
                    }

                    if (!string.IsNullOrWhiteSpace(email) && string.IsNullOrWhiteSpace(user.Email))
                    {
                        user.Email = email.ToLower(Culture.Invariant);
                    }

                    user.LastActivityAt = SystemTime.Now();

                    unitOfWork.Commit();

                    if (isNewUser)
                    {
                        eventAggregator.GetEvent<UserCreatedEvent>().Publish(new EventArgs<User>(user));
                    }

                    result = new UserResult(new UserDTO(user));
                }
            }

            return result;
        }

        public UserResult UpdateLastActivity(string userName)
        {
            UserResult result =
                Validation.Validate<UserResult>(() => string.IsNullOrWhiteSpace(userName), "userName",
                                                TextMessages.UserNameCannotBeBlank).Result();

            if (result.RuleViolations.IsEmpty())
            {
                User user = userRepository.GetByName(userName);

                result =
                    Validation.Validate<UserResult>(() => user == null, "userName",
                                                    TextMessages.UserDoesNotExist.FormatWith(userName))
                              .Or(() => user.IsLockedOut, "userName",
                                  TextMessages.UserIsCurrentlyLockedOut.FormatWith(userName))
                              .Result();

                if (result.RuleViolations.IsEmpty())
                {
                    user.LastActivityAt = SystemTime.Now();
                    unitOfWork.Commit();

                    result = new UserResult(new UserDTO(user));
                }
            }

            return result;
        }

        public UserResult RegenerateApiKey(string userName)
        {
            UserResult result =
                Validation.Validate<UserResult>(() => string.IsNullOrWhiteSpace(userName), "userName",
                                                TextMessages.UserNameCannotBeBlank)
                          .Result();

            if (result.RuleViolations.IsEmpty())
            {
                User user = userRepository.GetByName(userName);

                result =
                    Validation.Validate<UserResult>(() => user == null, "apiKey",
                                                    TextMessages.UserDoesNotExist.FormatWith(userName))
                              .Or(() => user.IsLockedOut, "userName",
                                  TextMessages.UserIsCurrentlyLockedOut.FormatWith(userName))
                              .Result();

                if (result.RuleViolations.IsEmpty())
                {
                    try
                    {
                        user.GenerateApiKey();
                        unitOfWork.Commit();

                        result = new UserResult(new UserDTO(user));
                    }
                    catch (InvalidOperationException e)
                    {
                        result.RuleViolations.Add(new RuleViolation("userName", e.Message));
                    }
                }
            }

            return result;
        }

        public int GetNumberOfUsers()
        {
            return Membership.GetAllUsers().Count;
        }

        /// <summary>
        /// Creates the administrator.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="email">The email.</param>
        /// <author>Vu Dinh</author>
        /// <modified>11/04/2014 14:30:44</modified>
        public void CreateAdministrator(string username, string password, string email)
        {
           Membership.CreateUser(username, password,email);
           var user = new User
                        {
                            Email = email,
                            Name = username,
                            CreatedAt = DateTime.Now,
                            Role = Role.Administrator,
                            IsLockedOut = false,
                        };
                        user.AllowApiAccess(0);
            userRepository.Add(user);
            unitOfWork.Commit();

        }

        /// <summary>
        /// Determines whether [is super administrator] [the specified username].
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns><c>true</c> if [is super administrator] [the specified username]; otherwise, <c>false</c>.</returns>
        /// <author>Vu Dinh</author>
        /// <modified>10/20/2014 10:30:00</modified>
        public bool IsSuperAdministrator(string username, string password)
        {
           if (Membership.ValidateUser(username, password))
           {
               var user = userRepository.GetByName(username);
               return user.Role == Role.Administrator;
           }
            return false;
        }
    }
}