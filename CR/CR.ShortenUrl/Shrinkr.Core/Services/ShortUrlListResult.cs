﻿using System.Collections.Generic;
using Shrinkr.DataTransferObjects;

namespace Shrinkr.Services
{
    public class ShortUrlListResult : ServiceResultBase
    {
        public ShortUrlListResult() : this(new List<RuleViolation>())
        {
        }

        public ShortUrlListResult(PagedResult<ShortUrlDTO> pagedResult) : this()
        {
            ShortUrls = new List<ShortUrlDTO>(pagedResult.Result);
            Total = pagedResult.Total;
        }

        public ShortUrlListResult(IEnumerable<RuleViolation> ruleViolations) : base(ruleViolations)
        {
        }

        public IList<ShortUrlDTO> ShortUrls { get; private set; }

        public int Total { get; private set; }
    }
}