﻿using System.Diagnostics;

namespace Shrinkr.Services
{
    public class RuleViolation
    {
        [DebuggerStepThrough]
        public RuleViolation(string parameterName, string errorMessage)
        {
            ParameterName = parameterName;
            ErrorMessage = errorMessage;
        }

        public string ParameterName { get; private set; }

        public string ErrorMessage { get; private set; }
    }
}