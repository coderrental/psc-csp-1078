﻿using System.Collections.Generic;
using Shrinkr.DataTransferObjects;

namespace Shrinkr.Services
{
    public class UserResult : ServiceResultBase
    {
        public UserResult() : this(new List<RuleViolation>())
        {
        }

        public UserResult(UserDTO user) : this()
        {
            User = user;
        }

        public UserResult(IEnumerable<RuleViolation> ruleViolations) : base(ruleViolations)
        {
        }

        public UserDTO User { get; private set; }
    }
}