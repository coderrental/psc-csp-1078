﻿using Shrinkr.DataTransferObjects;

namespace Shrinkr.Services
{
    public interface IUserService
    {
        UserDTO GetByName(string name);

        UserResult Save(string userName, string email);

        UserResult UpdateLastActivity(string userName);

        UserResult RegenerateApiKey(string userName);

        bool IsSuperAdministrator(string username, string password);
    }
}