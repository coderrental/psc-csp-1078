﻿using System.Collections.Generic;
using Shrinkr.DataTransferObjects;

namespace Shrinkr.Services
{
    public class VisitResult : ServiceResultBase
    {
        public VisitResult() : this(new List<RuleViolation>())
        {
        }

        public VisitResult(VisitDTO visit) : this()
        {
            Visit = visit;
        }

        public VisitResult(IEnumerable<RuleViolation> ruleViolations) : base(ruleViolations)
        {
        }

        public VisitDTO Visit { get; private set; }
    }
}