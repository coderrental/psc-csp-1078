﻿using System.Collections.Generic;
using System.Diagnostics;

namespace Shrinkr.Services
{
    public abstract class ServiceResultBase
    {
        [DebuggerStepThrough]
        protected ServiceResultBase(IEnumerable<RuleViolation> ruleViolations)
        {
            RuleViolations = new List<RuleViolation>(ruleViolations);
        }

        public IList<RuleViolation> RuleViolations { get; private set; }
    }
}