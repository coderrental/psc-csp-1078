﻿using System.Collections.Generic;
using Shrinkr.DataTransferObjects;

namespace Shrinkr.Services
{
    public class ShortUrlResult : ServiceResultBase
    {
        public ShortUrlResult() : this(new List<RuleViolation>())
        {
        }

        public ShortUrlResult(ShortUrlDTO shortUrl) : this()
        {
            ShortUrl = shortUrl;
        }

        public ShortUrlResult(IEnumerable<RuleViolation> ruleViolations) : base(ruleViolations)
        {
        }

        public ShortUrlDTO ShortUrl { get; private set; }
    }
}