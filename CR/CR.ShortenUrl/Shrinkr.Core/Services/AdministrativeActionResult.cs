﻿using System.Collections.Generic;
using Shrinkr.DomainObjects;

namespace Shrinkr.Services
{
    public class AdministrativeActionResult<TItem> : ServiceResultBase where TItem : IEntity
    {
        public AdministrativeActionResult() : this(new List<RuleViolation>())
        {
        }

        public AdministrativeActionResult(TItem item) : this()
        {
            Item = item;
        }

        public AdministrativeActionResult(IEnumerable<RuleViolation> ruleViolations) : base(ruleViolations)
        {
        }

        public TItem Item { get; private set; }
    }
}