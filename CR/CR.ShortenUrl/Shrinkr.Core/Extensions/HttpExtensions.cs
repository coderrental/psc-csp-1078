﻿using System;
using System.Collections.Generic;

namespace Shrinkr.Infrastructure
{
    public static class HttpExtensions
    {
        public static HttpResponse Get(this IHttp instance, string url)
        {
            return instance.Get(url, Http.DefaultUserAgent, Http.DefaultTimeout, Http.DefaultRequestCompressed,
                                Http.DefaultMaximumRedirects, null, null, null, null, null);
        }

        public static void GetAsync(this IHttp instance, string url)
        {
            instance.GetAsync(url, Http.DefaultUserAgent, Http.DefaultTimeout, Http.DefaultRequestCompressed,
                              Http.DefaultMaximumRedirects, null, null, null, null, null, null, null);
        }

        public static void GetAsync(this IHttp instance, string url, Action<HttpResponse> onComplete)
        {
            instance.GetAsync(url, Http.DefaultUserAgent, Http.DefaultTimeout, Http.DefaultRequestCompressed,
                              Http.DefaultMaximumRedirects, null, null, null, null, null, onComplete, null);
        }

        public static void PostAsync(this IHttp instance, string url, string userName, string password,
                                     IDictionary<string, string> formFields)
        {
            instance.PostAsync(url, formFields, Http.DefaultUserAgent, Http.DefaultTimeout,
                               Http.DefaultRequestCompressed, Http.DefaultMaximumRedirects, userName, password, null,
                               null, null, null, null);
        }
    }
}