namespace Shrinkr.DomainObjects
{
    public class ReservedAlias : IEntity
    {
        public virtual string Name { get; set; }
        public virtual long Id { get; set; }
    }
}