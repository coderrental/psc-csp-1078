namespace Shrinkr.DomainObjects
{
    public class BannedDomain : IEntity
    {
        public virtual string Name { get; set; }
        public virtual long Id { get; set; }
    }
}