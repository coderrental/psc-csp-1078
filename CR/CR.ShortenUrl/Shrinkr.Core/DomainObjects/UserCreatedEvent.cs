﻿using Shrinkr.Infrastructure;

namespace Shrinkr.DomainObjects
{
    public class UserCreatedEvent : EventBase<EventArgs<User>>
    {
    }
}