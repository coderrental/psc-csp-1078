﻿namespace Shrinkr.DomainObjects
{
    public class BadWord : IEntity
    {
        public virtual string Expression { get; set; }
        public virtual long Id { get; set; }
    }
}