﻿using Shrinkr.Infrastructure;

namespace Shrinkr.DomainObjects
{
    public class ShortUrlVisitedEvent : EventBase<EventArgs<Visit>>
    {
    }
}