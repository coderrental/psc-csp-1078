﻿using System;
using System.ComponentModel;

namespace Shrinkr.DomainObjects
{
    [Flags]
    public enum SpamStatus
    {
        None = 0,

        [Description("Bad Word")] BadWord = 1,

        Phishing = 2,

        Malware = 4,

        Clean = 8
    }
}