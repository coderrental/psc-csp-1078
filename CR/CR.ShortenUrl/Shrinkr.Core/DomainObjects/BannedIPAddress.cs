namespace Shrinkr.DomainObjects
{
    public class BannedIPAddress : IEntity
    {
        public virtual string IPAddress { get; set; }
        public virtual long Id { get; set; }
    }
}