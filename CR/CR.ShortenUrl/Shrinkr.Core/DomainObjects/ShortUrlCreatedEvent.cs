﻿using Shrinkr.Infrastructure;

namespace Shrinkr.DomainObjects
{
    public class ShortUrlCreatedEvent : EventBase<EventArgs<Alias>>
    {
    }
}