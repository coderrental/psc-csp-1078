﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using Shrinkr.DomainObjects;
using Shrinkr.Extensions;
using Shrinkr.Repositories;

namespace Shrinkr.Infrastructure
{
    public class TextMatchingSpamDetector : ISpamDetector
    {
        public static readonly string CacheKey = typeof (TextMatchingSpamDetector).FullName;

        private readonly IBadWordRepository badWordRepository;
        private readonly ICacheManager cacheManager;
        private readonly IExternalContentService externalContentService;

        public TextMatchingSpamDetector(IExternalContentService externalContentService,
                                        IBadWordRepository badWordRepository, ICacheManager cacheManager)
        {
            this.externalContentService = externalContentService;
            this.badWordRepository = badWordRepository;
            this.cacheManager = cacheManager;
        }

        public SpamStatus CheckStatus(ShortUrl shortUrl)
        {
            ExternalContent externalContent = null;

            try
            {
                externalContent = externalContentService.Retrieve(shortUrl.Url);
            }
            catch (WebException)
            {
            }

            if (externalContent == null)
            {
                return SpamStatus.None;
            }

            if (string.IsNullOrEmpty(externalContent.Content))
            {
                return SpamStatus.None;
            }

            string title = externalContent.Title;
            string plainContent = externalContent.Content.StripHtml().Trim();

            if (!string.IsNullOrWhiteSpace(title) &&
                GetBadWordExpressions().Any(expression => expression.IsMatch(title)))
            {
                return SpamStatus.BadWord;
            }

            if (!string.IsNullOrWhiteSpace(plainContent) &&
                GetBadWordExpressions().Any(expression => expression.IsMatch(plainContent)))
            {
                return SpamStatus.BadWord;
            }

            return SpamStatus.Clean;
        }

        private IEnumerable<Regex> GetBadWordExpressions()
        {
            return cacheManager.GetOrCreate(CacheKey, GetBadWordsAndConvertToExpression);
        }

        private IEnumerable<Regex> GetBadWordsAndConvertToExpression()
        {
            const RegexOptions Option = RegexOptions.IgnoreCase |
                                        RegexOptions.Singleline |
                                        RegexOptions.Multiline |
                                        RegexOptions.CultureInvariant |
                                        RegexOptions.Compiled;

            return badWordRepository.All()
                                    .ToList()
                                    .Select(badWord => new Regex(badWord.Expression, Option))
                                    .ToList();
        }
    }
}