﻿using Shrinkr.DomainObjects;

namespace Shrinkr.Infrastructure
{
    public class GoogleSafeBrowsingSpamDetector : ISpamDetector
    {
        private readonly IGoogleSafeBrowsing googleSafeBrowsing;

        public GoogleSafeBrowsingSpamDetector(IGoogleSafeBrowsing googleSafeBrowsing)
        {
            this.googleSafeBrowsing = googleSafeBrowsing;
        }

        public SpamStatus CheckStatus(ShortUrl shortUrl)
        {
            int phishingCount;
            int malwareCount;

            googleSafeBrowsing.Verify(shortUrl.Url, out phishingCount, out malwareCount);

            var status = SpamStatus.None;

            if ((phishingCount > 0) || (malwareCount > 0))
            {
                if (phishingCount > 0)
                {
                    status |= SpamStatus.Phishing;
                }

                if (malwareCount > 0)
                {
                    status |= SpamStatus.Malware;
                }
            }
            else
            {
                status = SpamStatus.Clean;
            }

            return status;
        }
    }
}