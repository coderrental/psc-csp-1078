using Shrinkr.DomainObjects;

namespace Shrinkr.Infrastructure
{
    public interface ISpamDetector
    {
        SpamStatus CheckStatus(ShortUrl shortUrl);
    }
}