﻿using System;
using System.Threading;

namespace Shrinkr.Infrastructure
{
    public static class RetryPolicy
    {
        public static void Retry(Action action, Func<bool> successCondition, int numberOfRetries, TimeSpan interval)
        {
            do
            {
                action();

                if (successCondition())
                {
                    break;
                }

                if (interval > TimeSpan.Zero)
                {
                    Thread.Sleep(interval);
                }
            } while (numberOfRetries-- > 1);
        }
    }
}