using System;

namespace Shrinkr.Infrastructure
{
    public interface IDelegateReference
    {
        Delegate Target { get; }
    }
}