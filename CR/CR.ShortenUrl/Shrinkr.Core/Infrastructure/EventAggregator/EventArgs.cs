﻿using System;
using System.Diagnostics;

namespace Shrinkr.Infrastructure
{
    public class EventArgs<TValue> : EventArgs
    {
        [DebuggerStepThrough]
        public EventArgs(TValue value)
        {
            Value = value;
        }

        public TValue Value { get; private set; }
    }
}