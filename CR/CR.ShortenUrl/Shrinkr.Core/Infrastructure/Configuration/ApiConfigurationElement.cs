﻿using System.Configuration;
using System.Diagnostics;

namespace Shrinkr.Infrastructure
{
    public class ApiConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("allowed")]
        public bool Allowed
        {
            [DebuggerStepThrough] get { return (bool) base["allowed"]; }

            [DebuggerStepThrough] set { base["allowed"] = value; }
        }

        [ConfigurationProperty("dailyLimit")]
        public int DailyLimit
        {
            [DebuggerStepThrough] get { return (int) base["dailyLimit"]; }

            [DebuggerStepThrough] set { base["dailyLimit"] = value; }
        }
    }
}