using Shrinkr.Extensions;

namespace Shrinkr.Infrastructure
{
    public class PageGlimpseThumbnail : IThumbnail
    {
        private readonly IHttp http;
        private readonly Settings settings;

        public PageGlimpseThumbnail(Settings settings, IHttp http)
        {
            this.settings = settings;
            this.http = http;
        }

        public string UrlFor(string url, ThumbnailSize size)
        {
            return "{0}?devkey={1}&url={2}&size={3}&root=no".FormatWith(settings.Thumbnail.Endpoint,
                                                                        settings.Thumbnail.ApiKey, url,
                                                                        size.ToString().ToLower(Culture.Invariant));
        }

        public void Capture(string url)
        {
            string requestUrl = "{0}/request?devkey={1}&url={2}".FormatWith(settings.Thumbnail.Endpoint,
                                                                            settings.Thumbnail.ApiKey, url);

            http.GetAsync(requestUrl);
        }
    }
}