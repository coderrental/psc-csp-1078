﻿using System.Collections.Generic;
using Shrinkr.DomainObjects;

namespace Shrinkr.Infrastructure
{
    public class Settings
    {
        public Settings(bool redirectPermanently, int urlPerPage, BaseType baseType, ApiSettings api,
                        ThumbnailSettings thumbnail, GoogleSafeBrowsingSettings google, TwitterSettings twitter)
        {
            RedirectPermanently = redirectPermanently;
            UrlPerPage = urlPerPage;
            BaseType = baseType;
            Api = api;
            Thumbnail = thumbnail;
            Google = google;
            Twitter = twitter;
        }

        internal Settings()
        {
        }

        public bool RedirectPermanently { get; internal set; }

        public int UrlPerPage { get; internal set; }

        public BaseType BaseType { get; internal set; }

        public ApiSettings Api { get; internal set; }

        public ThumbnailSettings Thumbnail { get; internal set; }

        public GoogleSafeBrowsingSettings Google { get; private set; }

        public TwitterSettings Twitter { get; internal set; }

    }
}