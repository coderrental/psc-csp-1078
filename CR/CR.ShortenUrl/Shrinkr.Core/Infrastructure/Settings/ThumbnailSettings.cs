﻿namespace Shrinkr.Infrastructure
{
    public class ThumbnailSettings
    {
        public ThumbnailSettings(string apiKey, string endpoint)
        {
            ApiKey = apiKey;
            Endpoint = endpoint;
        }

        public string ApiKey { get; private set; }

        public string Endpoint { get; private set; }
    }
}