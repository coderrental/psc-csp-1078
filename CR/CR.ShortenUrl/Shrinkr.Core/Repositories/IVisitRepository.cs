﻿using Shrinkr.DomainObjects;

namespace Shrinkr.Repositories
{
    public interface IVisitRepository : IRepository<Visit>
    {
        int Count(string aliasName);
    }
}