﻿using Shrinkr.DomainObjects;

namespace Shrinkr.Repositories
{
    public interface IReservedAliasRepository : IRepository<ReservedAlias>
    {
        bool IsMatching(string aliasName);
    }
}