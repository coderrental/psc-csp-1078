﻿using Shrinkr.DomainObjects;

namespace Shrinkr.Repositories
{
    public interface IBannedDomainRepository : IRepository<BannedDomain>
    {
        bool IsMatching(string url);
    }
}