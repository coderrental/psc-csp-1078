using System.Collections.Generic;
using Shrinkr.DomainObjects;

namespace Shrinkr.Repositories
{
    public interface IRepository<TEntity> where TEntity : IEntity
    {
        void Add(TEntity entity);

        void Delete(TEntity entity);

        TEntity GetById(long id);

        IEnumerable<TEntity> All();
    }
}