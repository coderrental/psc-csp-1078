﻿using Shrinkr.DomainObjects;

namespace Shrinkr.Repositories
{
    public interface IBannedIPAddressRepository : IRepository<BannedIPAddress>
    {
        bool IsMatching(string ipAddress);
    }
}