using System;
using Shrinkr.DomainObjects;

namespace Shrinkr.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        User GetByName(string name);

        User GetByApiKey(string apiKey);

        int GetCreatedCount(DateTime fromDate, DateTime toDate);

        int GetVisitedCount(DateTime fromDate, DateTime toDate);
    }
}