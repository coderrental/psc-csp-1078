﻿using Shrinkr.DomainObjects;

namespace Shrinkr.Repositories
{
    public interface IBadWordRepository : IRepository<BadWord>
    {
        bool IsMatching(string expression);
    }
}