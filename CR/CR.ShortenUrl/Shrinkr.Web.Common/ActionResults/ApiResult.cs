﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Shrinkr.Extensions;

namespace Shrinkr.Web
{
    public class ApiResult : ActionResult
    {
        public ApiResult(ApiResponseFormat responseFormat)
        {
            ResponseFormat = responseFormat;
        }

        public ApiResponseFormat ResponseFormat { get; private set; }

        public override void ExecuteResult(ControllerContext context)
        {
            string action = context.RouteData.Values["action"].ToString().ToLower();
            ViewDataDictionary viewData = context.Controller.ViewData;

            IList<Error> errors = null;

            if (viewData.Model == null)
            {
                errors = viewData.ModelState.IsValid
                             ? null
                             : viewData.ModelState.Select(
                                 ms =>
                                 new Error
                                     {
                                         Parameter = ms.Key,
                                         Messages =
                                             new MessageCollection(
                                     ms.Value.Errors.Select(
                                         error =>
                                         (error.Exception == null) ? error.ErrorMessage : error.Exception.Message)
                                       .Where(error => !string.IsNullOrWhiteSpace(error))
                                       .ToList())
                                     })
                                       .Where(ms => ms.Messages.Any())
                                   // No need to include items that does not have any errors
                                       .ToList();
            }

            string contentType;
            string content;
            /*
            switch (ResponseFormat)
            {
                case ApiResponseFormat.Json:
                    {
                        contentType = "application/json";
                        content = JsonSerialize(viewData.Model, errors,action);
                        break;
                    }

                case ApiResponseFormat.Xml:
                    {
                        contentType = "application/xml";
                        content = XmlSerialize(model, errors);

                        break;
                    }

                default:
                    {
                        contentType = "text/plain";
                        content = TextSerialize(model, errors);

                        break;
                    }
            }
            */

            contentType = "application/json";
            content = JsonSerialize(viewData.Model, errors, action);
            HttpResponseBase response = context.HttpContext.Response;

            response.Clear();
            response.ContentType = contentType;
            response.Write(content);
        }

        private static string JsonSerialize(object model, IList<Error> errors, string action)
        {
            string content = null;

            if (model != null)
            {
                switch (action)
                {
                    case "create":
                        var createModel = (CreateUrlViewModel) model;

                        content = Serialize(new DataContractJsonSerializer(typeof (CreateResult)), new CreateResult
                            {
                                Alias = createModel.Alias,
                                LongUrl = createModel.Url,
                                //PreviewUrl = createModel.PreviewUrl,
                                ShortUrl = createModel.VisitUrl,
                                Title = createModel.Title
                            });
                        break;
                    case "view":
                        var visitModel = (VisitUrlViewModel) model;
                        content = Serialize(new DataContractJsonSerializer(typeof (ViewResult)), new ViewResult
                            {
                                CreatedAt = visitModel.CreatedAt.ToString(),
                                Domain = visitModel.Domain,
                                Visits = visitModel.Visits.ToString(),
                                Title = visitModel.Title,
                                Url = visitModel.Url
                            });
                        break;
                }
            }
            else if (errors != null)
            {
                content = Serialize(new DataContractJsonSerializer(typeof (IList<Error>)), errors);
            }

            if (string.IsNullOrWhiteSpace(content))
            {
                content = "{\"errors\":[{\"error\":{\"key\":\"\",\"messages\":[\"" + TextMessages.SomethingUnholyGoingOn +
                          "\"]}}]}";
            }

            return content;
        }

        private static string XmlSerialize(CreateResult model, IList<Error> errors)
        {
            string content = null;

            if (model != null)
            {
                content = Serialize(new DataContractSerializer(typeof (CreateResult)), model);
            }
            else if (errors != null)
            {
                content = Serialize(new DataContractSerializer(typeof (IList<Error>), "errors", string.Empty), errors);
            }

            if (string.IsNullOrWhiteSpace(content))
            {
                content =
                    "<errors><error><key/><messages><message>{0}</message></messages></error></errors>".FormatWith(
                        TextMessages.SomethingUnholyGoingOn);
            }

            return content;
        }

        private static string TextSerialize(CreateResult model, IEnumerable<Error> errors)
        {
            string content = null;

            if (model != null)
            {
                content = model.ShortUrl;
            }
            else if (errors != null)
            {
                var errorBuilder = new StringBuilder();

                foreach (Error error in errors)
                {
                    foreach (string message in error.Messages)
                    {
                        errorBuilder.AppendLine("{0}: {1}".FormatWith(error.Parameter, message));
                    }
                }

                content = errorBuilder.ToString();
            }

            if (string.IsNullOrWhiteSpace(content))
            {
                content = TextMessages.SomethingUnholyGoingOn;
            }

            return content;
        }

        private static string Serialize(XmlObjectSerializer serializer, object target)
        {
            using (var ms = new MemoryStream())
            {
                serializer.WriteObject(ms, target);

                ms.Seek(0, SeekOrigin.Begin);

                return Encoding.UTF8.GetString(ms.ToArray());
            }
        }

        [DataContract(Name = "create", Namespace = "")]
        private sealed class CreateResult
        {
            [DataMember(Name = "shortUrl", Order = 0)]
            public string ShortUrl { get; set; }

            //[DataMember(Name = "previewUrl", Order = 1)]
            //public string PreviewUrl
            //{
            //    get;
            //    set;
            //}

            [DataMember(Name = "alias", Order = 2)]
            public string Alias { get; set; }

            [DataMember(Name = "longUrl", Order = 3)]
            public string LongUrl { get; set; }

            [DataMember(Name = "title", Order = 4)]
            public string Title { get; set; }
        }

        [DataContract(Name = "error", Namespace = "")]
        private sealed class Error
        {
            [DataMember(Name = "parameter", Order = 0)]
            public string Parameter { get; set; }

            [DataMember(Name = "messages", Order = 1)]
            public MessageCollection Messages { get; set; }
        }

        [CollectionDataContract(Name = "messages", ItemName = "message", Namespace = "")]
        private sealed class MessageCollection : Collection<string>
        {
            public MessageCollection()
            {
            }

            public MessageCollection(IList<string> messages) : base(messages)
            {
            }
        }

        [DataContract(Name = "view", Namespace = "")]
        private sealed class ViewResult
        {
            [DataMember(Name = "title", Order = 0)]
            public string Title { get; set; }

            [DataMember(Name = "domain", Order = 1)]
            public string Domain { get; set; }

            [DataMember(Name = "url", Order = 2)]
            public string Url { get; set; }

            [DataMember(Name = "visits", Order = 3)]
            public string Visits { get; set; }

            [DataMember(Name = "createdAt", Order = 4)]
            public string CreatedAt { get; set; }
        }
    }
}