﻿using System;
using System.Security.Principal;
using System.Web.Mvc;
using MvcExtensions;
using Shrinkr.DataTransferObjects;
using Shrinkr.DomainObjects;
using Shrinkr.Services;

namespace Shrinkr.Web
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = false),
     CLSCompliant(false)]
    public class ShrinkrAuthorizeAttribute : ExtendedAuthorizeAttribute
    {
        public ShrinkrAuthorizeAttribute(IUserService userService)
        {
            UserService = userService;
        }

        public IUserService UserService { get; private set; }

        public Role? AllowedRole { get; set; }

        public override bool IsAuthorized(AuthorizationContext filterContext)
        {
            IPrincipal principal = filterContext.HttpContext.User;
            return principal.Identity.IsAuthenticated;
            /*
            if (principal.Identity.IsAuthenticated)
            {
                UserDTO user = UserService.GetByName(principal.Identity.Name);

                if ((user != null) && !user.IsLockedOut)
                {
                    if (AllowedRole.HasValue)
                    {
                        return user.Role == AllowedRole.Value;
                    }

                    return true;
                }
                
            }

            return false;
             * */
        }
    }
}