﻿using System;
using System.Web.Mvc;
using Shrinkr.Services;

namespace Shrinkr.Web
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = false),
     CLSCompliant(false)]
    public class UpdateUserLastActivityAttribute : FilterAttribute, IResultFilter
    {
        public UpdateUserLastActivityAttribute(IUserService userService)
        {
            UserService = userService;
        }

        public IUserService UserService { get; private set; }

        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
            // Do nothing, just sleep.
        }

        public void OnResultExecuted(ResultExecutedContext filterContext)
        {
            string userName = filterContext.HttpContext.User.Identity.IsAuthenticated
                                  ? filterContext.HttpContext.User.Identity.Name
                                  : null;

            if (!string.IsNullOrEmpty(userName))
            {
                UserService.UpdateLastActivity(userName);
            }
        }
    }
}