﻿// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Error List, point to "Suppress Message(s)", and click 
// "In Project Suppression File".
// You do not need to add suppressions to this file manually.

using System.Diagnostics.CodeAnalysis;

[assembly:
    SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Scope = "member",
        Target = "Shrinkr.Web.CacheManager.#Get`1(System.String)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Scope = "member",
        Target = "Shrinkr.Web.ICookie.#GetValue`1(System.String,System.Boolean)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Scope = "member",
        Target = "Shrinkr.Web.ICookie.#GetValue`1(System.String)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Scope = "member",
        Target = "Shrinkr.Web.Cookie.#GetValue`1(System.String,System.Boolean)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Scope = "member",
        Target = "Shrinkr.Web.Cookie.#GetValue`1(System.String)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Scope = "member",
        Target = "Shrinkr.Web.ListHtmlHelper.#Pager`1()")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Scope = "member",
        Target = "Shrinkr.Web.UserController.#GenerateKey(Shrinkr.Web.ProfileCommand)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Scope = "member",
        Target = "Shrinkr.Web.UserController.#Profile(Shrinkr.Web.ProfileCommand)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Scope = "member",
        Target = "Shrinkr.Web.ControllerExtensions.#AdaptiveView(System.Web.Mvc.Controller)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId = "1#", Scope = "member",
        Target = "Shrinkr.Web.UrlHelperNavigationExtensions.#ToAbsolute(System.Web.Mvc.UrlHelper,System.String)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId = "0#", Scope = "member",
        Target = "Shrinkr.Web.AuthenticationController.#LogOff(System.String)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId = "3#", Scope = "member",
        Target =
            "Shrinkr.Web.ControllerExtensions.#AdaptivePostRedirectGet(System.Web.Mvc.Controller,System.Collections.Generic.IEnumerable`1<Shrinkr.Services.RuleViolation>,System.Object,System.String)"
        )]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId = "1#", Scope = "member",
        Target = "Shrinkr.Web.ControllerExtensions.#AdaptivePostRedirectGet(System.Web.Mvc.Controller,System.String)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1055:UriReturnValuesShouldNotBeStrings", Scope = "member",
        Target = "Shrinkr.Web.UrlHelperNavigationExtensions.#Url(System.Web.Mvc.UrlHelper,System.String)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings", Scope = "member",
        Target = "Shrinkr.Web.CreateShortUrlApiCommand.#Url")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings", Scope = "member",
        Target = "Shrinkr.Web.CreateShortUrlCommand.#Url")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings", Scope = "member",
        Target = "Shrinkr.Web.CreateUrlViewModel.#PreviewUrl")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings", Scope = "member",
        Target = "Shrinkr.Web.CreateUrlViewModel.#Url")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings", Scope = "member",
        Target = "Shrinkr.Web.CreateUrlViewModel.#VisitUrl")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings", Scope = "member",
        Target = "Shrinkr.Web.IFormsAuthentication.#LogOnUrl")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings", Scope = "member",
        Target = "Shrinkr.Web.LogOnCommand.#ReturnUrl")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        ,
        Target =
            "Shrinkr.Web.AddReservedAliasFromRoutes.#ExecuteCore(Microsoft.Practices.ServiceLocation.IServiceLocator)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        ,
        Target =
            "Shrinkr.Web.ApiCommandBinder.#BindModel(System.Web.Mvc.ControllerContext,System.Web.Mvc.ModelBindingContext)"
        )]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.ApiController.#Create(Shrinkr.Web.CreateShortUrlApiCommand)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.ApiResult.#ExecuteResult(System.Web.Mvc.ControllerContext)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        ,
        Target = "Shrinkr.Web.AppendOpenIdXrdsLocationAttribute.#OnResultExecuted(System.Web.Mvc.ResultExecutedContext)"
        )]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.AuthenticationController.#LogOn(Shrinkr.Web.LogOnCommand)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.BlockRestrictedIPAddress.#ExecuteCore(MvcExtensions.PerRequestExecutionContext)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        ,
        Target =
            "Shrinkr.Web.ConfigureBackgroundServices.#ExecuteCore(Microsoft.Practices.ServiceLocation.IServiceLocator)")
]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.ConfigureRoutes.#Register(System.Web.Routing.RouteCollection)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        ,
        Target =
            "Shrinkr.Web.ControllerExtensions.#AdaptivePostRedirectGet(System.Web.Mvc.Controller,System.Collections.Generic.IEnumerable`1<Shrinkr.Services.RuleViolation>,System.Object,System.String)"
        )]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.ControllerExtensions.#AdaptiveView(System.Web.Mvc.Controller)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        ,
        Target =
            "Shrinkr.Web.ControllerExtensions.#AdaptiveView(System.Web.Mvc.Controller,System.Collections.Generic.IEnumerable`1<Shrinkr.Services.RuleViolation>,System.Object)"
        )]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.CreateDefaultUsers.#ExecuteCore(Microsoft.Practices.ServiceLocation.IServiceLocator)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.CreateUrlViewModel.#.ctor(Shrinkr.DataTransferObjects.ShortUrlDTO)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.ElmahHandleErrorAttribute.#OnException(System.Web.Mvc.ExceptionContext)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.HtmlHelperExtensions.#CustomValidationMessage(System.Web.Mvc.HtmlHelper,System.String)")
]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        ,
        Target =
            "Shrinkr.Web.HtmlHelperExtensions.#CustomValidationSummary(System.Web.Mvc.HtmlHelper,System.String,System.Object)"
        )]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.HttpContextBaseExtensions.#ApplicationRoot(System.Web.HttpContextBase)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "1", Scope = "member"
        ,
        Target =
            "Shrinkr.Web.LogOnCommandBinder.#BindModel(System.Web.Mvc.ControllerContext,System.Web.Mvc.ModelBindingContext)"
        )]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.RemoveWww.#ExecuteCore(MvcExtensions.PerRequestExecutionContext)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.ShortUrlController.#Create(Shrinkr.Web.CreateShortUrlCommand)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.ShortUrlController.#List(Shrinkr.Web.ShortUrlListCommand)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.ShortUrlController.#Preview(Shrinkr.Web.ShortUrlVisitCommand)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.ShortUrlController.#Visit(Shrinkr.Web.ShortUrlVisitCommand)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.ShrinkrAuthorizeAttribute.#IsAuthorized(System.Web.Mvc.AuthorizationContext)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.TabStripItemFactoryExtensions.#AddBadWords(Telerik.Web.Mvc.UI.TabStripItemFactory)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.TabStripItemFactoryExtensions.#AddBannedDomains(Telerik.Web.Mvc.UI.TabStripItemFactory)"
        )]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.TabStripItemFactoryExtensions.#AddBannedIPs(Telerik.Web.Mvc.UI.TabStripItemFactory)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        ,
        Target = "Shrinkr.Web.TabStripItemFactoryExtensions.#AddReservedAliases(Telerik.Web.Mvc.UI.TabStripItemFactory)"
        )]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.TabStripItemFactoryExtensions.#AddSummary(Telerik.Web.Mvc.UI.TabStripItemFactory)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.TabStripItemFactoryExtensions.#AddUrls(Telerik.Web.Mvc.UI.TabStripItemFactory)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.TabStripItemFactoryExtensions.#AddUsers(Telerik.Web.Mvc.UI.TabStripItemFactory)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.UpdateUserLastActivityAttribute.#OnResultExecuted(System.Web.Mvc.ResultExecutedContext)"
        )]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.UrlHelperAssetExtensions.#InputValidationErrorIcon(System.Web.Mvc.UrlHelper)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.UrlHelperAssetExtensions.#OpenIdIcon(System.Web.Mvc.UrlHelper,System.String)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.UrlHelperNavigationExtensions.#ApplicationRoot(System.Web.Mvc.UrlHelper)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.UrlHelperNavigationExtensions.#BadWords(System.Web.Mvc.UrlHelper,System.Int32)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.UrlHelperNavigationExtensions.#BannedDomains(System.Web.Mvc.UrlHelper,System.Int32)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.UrlHelperNavigationExtensions.#BannedIPAddresses(System.Web.Mvc.UrlHelper,System.Int32)"
        )]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.UrlHelperNavigationExtensions.#Home(System.Web.Mvc.UrlHelper)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.UrlHelperNavigationExtensions.#ReservedAliases(System.Web.Mvc.UrlHelper,System.Int32)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.UrlHelperNavigationExtensions.#Summary(System.Web.Mvc.UrlHelper)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.UrlHelperNavigationExtensions.#ToAbsolute(System.Web.Mvc.UrlHelper,System.String)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.UrlHelperNavigationExtensions.#Url(System.Web.Mvc.UrlHelper,System.String)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.UrlHelperNavigationExtensions.#Urls(System.Web.Mvc.UrlHelper,System.Int32)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.UrlHelperNavigationExtensions.#User(System.Web.Mvc.UrlHelper,System.Int64)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.UrlHelperNavigationExtensions.#Users(System.Web.Mvc.UrlHelper,System.Int32)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        ,
        Target =
            "Shrinkr.Web.UserCommandBinder.#BindModel(System.Web.Mvc.ControllerContext,System.Web.Mvc.ModelBindingContext)"
        )]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.UserController.#GenerateKey(Shrinkr.Web.ProfileCommand)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member"
        , Target = "Shrinkr.Web.UserController.#Profile(Shrinkr.Web.ProfileCommand)")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "IPs",
        Scope = "member",
        Target = "Shrinkr.Web.TabStripItemFactoryExtensions.#AddBannedIPs(Telerik.Web.Mvc.UI.TabStripItemFactory)")]
[assembly:
    SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "filter", Scope = "member",
        Target = "Shrinkr.Web.ControlPanelController.#Urls(System.Nullable`1<System.Int32>,System.String,System.String)"
        )]
[assembly:
    SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "filter", Scope = "member",
        Target =
            "Shrinkr.Web.ControlPanelController.#Users(System.Nullable`1<System.Int32>,System.String,System.String)")]
[assembly:
    SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "orderBy", Scope = "member",
        Target = "Shrinkr.Web.ControlPanelController.#Urls(System.Nullable`1<System.Int32>,System.String,System.String)"
        )]
[assembly:
    SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "orderBy", Scope = "member",
        Target =
            "Shrinkr.Web.ControlPanelController.#Users(System.Nullable`1<System.Int32>,System.String,System.String)")]
[assembly:
    SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "page", Scope = "member",
        Target = "Shrinkr.Web.ControlPanelController.#Urls(System.Nullable`1<System.Int32>,System.String,System.String)"
        )]
[assembly:
    SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "page", Scope = "member",
        Target =
            "Shrinkr.Web.ControlPanelController.#Users(System.Nullable`1<System.Int32>,System.String,System.String)")]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1813:AvoidUnsealedAttributes", Scope = "type",
        Target = "Shrinkr.Web.AppendOpenIdXrdsLocationAttribute")]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1813:AvoidUnsealedAttributes", Scope = "type",
        Target = "Shrinkr.Web.ElmahHandleErrorAttribute")]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1813:AvoidUnsealedAttributes", Scope = "type",
        Target = "Shrinkr.Web.ShrinkrAuthorizeAttribute")]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1813:AvoidUnsealedAttributes", Scope = "type",
        Target = "Shrinkr.Web.UpdateUserLastActivityAttribute")]
[assembly:
    SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Scope = "member",
        Target =
            "Shrinkr.Web.CacheManager.#InsertInCache(System.String,System.Object,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.DateTime>,System.Nullable`1<System.TimeSpan>,System.Action`1<System.Boolean>)"
        )]
[assembly:
    SuppressMessage("Microsoft.Security", "CA2116:AptcaMethodsShouldOnlyCallAptcaMethods", Scope = "member",
        Target = "Shrinkr.Web.ElmahHandleErrorAttribute.#IsFiltered(System.Web.Mvc.ExceptionContext)")]
[assembly:
    SuppressMessage("Microsoft.Security", "CA2116:AptcaMethodsShouldOnlyCallAptcaMethods", Scope = "member",
        Target = "Shrinkr.Web.ElmahHandleErrorAttribute.#LogException(System.Exception)")]
[assembly:
    SuppressMessage("Microsoft.Security", "CA2116:AptcaMethodsShouldOnlyCallAptcaMethods", Scope = "member",
        Target = "Shrinkr.Web.ElmahHandleErrorAttribute.#RaiseErrorSignal(System.Exception)")]
[assembly:
    SuppressMessage("Microsoft.Security", "CA2141:TransparentMethodsMustNotSatisfyLinkDemandsFxCopRule",
        Scope = "member", Target = "Shrinkr.Web.ElmahHandleErrorAttribute.#IsFiltered(System.Web.Mvc.ExceptionContext)")
]
[assembly:
    SuppressMessage("Microsoft.Security", "CA2141:TransparentMethodsMustNotSatisfyLinkDemandsFxCopRule",
        Scope = "member", Target = "Shrinkr.Web.ElmahHandleErrorAttribute.#LogException(System.Exception)")]
[assembly:
    SuppressMessage("Microsoft.Security", "CA2141:TransparentMethodsMustNotSatisfyLinkDemandsFxCopRule",
        Scope = "member", Target = "Shrinkr.Web.ElmahHandleErrorAttribute.#RaiseErrorSignal(System.Exception)")]
[assembly: SuppressMessage("Microsoft.Design", "CA2210:AssembliesShouldHaveValidStrongNames")]