﻿using System.Collections.Generic;
using System.Web.Mvc;
using MvcExtensions;
using Shrinkr.Services;

namespace Shrinkr.Web
{
    public static class ControllerExtensions
    {
        public static ActionResult AdaptivePostRedirectGet(this Controller instance, string url)
        {
            return new AdaptivePostRedirectGetResult(url, new[] {new CamelCasedJsonConverter()});
        }

        public static ActionResult AdaptivePostRedirectGet(this Controller instance,
                                                           IEnumerable<RuleViolation> ruleViolations, object model,
                                                           string url)
        {
            if (ruleViolations != null)
            {
                instance.ModelState.Merge(ruleViolations);
            }

            instance.ViewData.Model = model;

            return AdaptivePostRedirectGet(instance, url);
        }

        public static ActionResult AdaptiveView(this Controller instance)
        {
            return new AdaptiveViewResult(new[] {new CamelCasedJsonConverter()})
                {
                    ViewData = instance.ViewData,
                    TempData = instance.TempData
                };
        }

        public static ActionResult AdaptiveView(this Controller instance, IEnumerable<RuleViolation> ruleViolations,
                                                object model)
        {
            if (ruleViolations != null)
            {
                instance.ModelState.Merge(ruleViolations);
            }

            instance.ViewData.Model = model;

            return AdaptiveView(instance);
        }
    }
}