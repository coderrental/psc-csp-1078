using System.Web.Mvc;
using Shrinkr.Extensions;

namespace Shrinkr.Web
{
    public static class UrlHelperAssetExtensions
    {
        public static string OpenIdIcon(this UrlHelper instance, string icon)
        {
            return instance.Content("~/Content/images/openid/{0}.png".FormatWith(icon));
        }

        public static string InputValidationErrorIcon(this UrlHelper instance)
        {
            return instance.Content("~/Content/images/forms/exclamation.png");
        }
    }
}