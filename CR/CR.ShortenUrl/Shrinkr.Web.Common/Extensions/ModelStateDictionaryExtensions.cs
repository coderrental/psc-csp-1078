﻿using System.Collections.Generic;
using System.Web.Mvc;
using Shrinkr.Extensions;
using Shrinkr.Services;

namespace Shrinkr.Web
{
    public static class ModelStateDictionaryExtensions
    {
        public static void Merge(this ModelStateDictionary instance, IEnumerable<RuleViolation> ruleViolations)
        {
            ruleViolations.Each(violation => instance.AddModelError(violation.ParameterName, violation.ErrorMessage));
        }
    }
}