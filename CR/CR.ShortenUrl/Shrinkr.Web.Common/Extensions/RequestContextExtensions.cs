﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Shrinkr.Web
{
    public static class RequestContextExtensions
    {
        public static UrlHelper UrlHelper(this RequestContext instance)
        {
            return new UrlHelper(instance);
        }
    }
}