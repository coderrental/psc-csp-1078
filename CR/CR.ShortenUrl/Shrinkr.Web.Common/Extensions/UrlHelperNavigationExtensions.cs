using System.Web.Mvc;
using System.Web.Routing;

namespace Shrinkr.Web
{
    public static class UrlHelperNavigationExtensions
    {
        public static string ApplicationRoot(this UrlHelper instance)
        {
            return instance.RequestContext.HttpContext.ApplicationRoot();
        }

        public static string Home(this UrlHelper instance)
        {
            return instance.Content("~/");
        }

        public static string Api(this UrlHelper instance)
        {
            return instance.ToAbsolute("/api");
        }

        public static string Create(this UrlHelper instance)
        {
            return RouteUrl(instance, "Default", null);
        }

        public static string Preview(this UrlHelper instance, string alias)
        {
            return RouteUrl(instance, "Preview", new {alias});
        }

        public static string Visit(this UrlHelper instance, string alias)
        {
            return RouteUrl(instance, "Visit", new {alias});
        }

        public static string Xrds(this UrlHelper instance)
        {
            return RouteUrl(instance, "Xrds", null);
        }

        public static string LogOn(this UrlHelper instance)
        {
            return RouteUrl(instance, "LogOn", null);
        }

        public static string List(this UrlHelper instance, int page)
        {
            return RouteUrl(instance, "List", new {page});
        }

        public static string Profile(this UrlHelper instance)
        {
            return RouteUrl(instance, "Profile", null);
        }

        public static string LogOff(this UrlHelper instance)
        {
            return RouteUrl(instance, "LogOff", null);
        }

        public static string Summary(this UrlHelper instance,int days)
        {
            return instance.Action("Summary", "ControlPanel", new {days});
        }

        public static string Setup(this UrlHelper instance)
        {
            return instance.Action("Setup", "Setup");
        }

        public static string Urls(this UrlHelper instance, int page)
        {
            return instance.Action("Urls", "ControlPanel", new {page});
        }

        public static string Url(this UrlHelper instance, string alias)
        {
            return instance.Action("Url", "ControlPanel", new {alias});
        }

        public static string CreateUser(this UrlHelper instance)
        {
            return instance.Action("CreateUser", "ControlPanel");
        }

        public static string Users(this UrlHelper instance, int page)
        {
            return instance.Action("Users", "ControlPanel", new {page});
        }

        public static string User(this UrlHelper instance, long id)
        {
            return instance.Action("User", "ControlPanel", new {id});
        }

        public static string BannedIPAddresses(this UrlHelper instance, int page)
        {
            return instance.Action("BannedIPAddresses", "ControlPanel", new {page});
        }

        public static string BannedDomains(this UrlHelper instance, int page)
        {
            return instance.Action("BannedDomains", "ControlPanel", new {page});
        }

        public static string ReservedAliases(this UrlHelper instance, int page)
        {
            return instance.Action("ReservedAliases", "ControlPanel", new {page});
        }

        public static string BadWords(this UrlHelper instance, int page)
        {
            return instance.Action("BadWords", "ControlPanel", new {page});
        }

        public static string ToAbsolute(this UrlHelper instance, string relativeUrl)
        {
            return instance.RequestContext.HttpContext.ApplicationRoot() + relativeUrl;
        }

        private static string RouteUrl(UrlHelper helper, string routeName, object routeValues)
        {
            return (routeValues == null) ? helper.RouteUrl(routeName) : helper.RouteUrl(routeName, routeValues);
        }
    }
}