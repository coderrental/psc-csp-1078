﻿using System.Collections.Generic;
using MvcExtensions;
using Shrinkr.Infrastructure;

namespace Shrinkr.Web
{
    public class ConfigureBackgroundServices : BootstrapperTask
    {
        private readonly IEnumerable<IBackgroundService> backgroundServices;

        public ConfigureBackgroundServices(IBackgroundService[] backgroundServices)
        {
            this.backgroundServices = backgroundServices;
        }

        public override TaskContinuation Execute()
        {
            backgroundServices.Each(service => service.Start());

            return TaskContinuation.Continue;
        }

        protected override void DisposeCore()
        {
            backgroundServices.Each(service => service.Stop());
        }
    }
}