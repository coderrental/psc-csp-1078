using System.Diagnostics;
using System.Web;
using System.Web.Mvc;
using Shrinkr.Infrastructure;

namespace Shrinkr.Web
{
    public class UrlResolver : IUrlResolver
    {
        private readonly UrlHelper urlHelper;

        public UrlResolver(HttpContextBase httpContext)
        {
            urlHelper = httpContext.RequestContext().UrlHelper();
        }

        public string ApplicationRoot
        {
            [DebuggerStepThrough] get { return urlHelper.RequestContext.HttpContext.ApplicationRoot(); }
        }

        public string Preview(string aliasName)
        {
            return urlHelper.Preview(aliasName);
        }

        public string Visit(string aliasName)
        {
            return urlHelper.Visit(aliasName);
        }

        public string Absolute(string relativeUrl)
        {
            string baseUrl = urlHelper.RequestContext.HttpContext.Request.Url.Scheme + "://" +
                             urlHelper.RequestContext.HttpContext.Request.Url.Authority.TrimEnd('/');
            //return urlHelper.ToAbsolute(relativeUrl);
            return baseUrl + relativeUrl;
        }
    }
}