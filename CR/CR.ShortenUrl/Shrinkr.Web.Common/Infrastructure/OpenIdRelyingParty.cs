using System.Diagnostics;
using DotNetOpenAuth.OpenId;
using DotNetOpenAuth.OpenId.RelyingParty;

namespace Shrinkr.Web
{
    using OpenId = DotNetOpenAuth.OpenId.RelyingParty.OpenIdRelyingParty;

    public interface IOpenIdRelyingParty
    {
        IAuthenticationResponse Response { get; }

        IAuthenticationRequest CreateRequest(Identifier userSuppliedIdentifier, Realm realm);
    }

    public class OpenIdRelyingParty : IOpenIdRelyingParty
    {
        private OpenId openId;

        private OpenId OpenId
        {
            [DebuggerStepThrough] get { return openId ?? (openId = new OpenId()); }
        }

        public IAuthenticationResponse Response
        {
            [DebuggerStepThrough] get { return OpenId.GetResponse(); }
        }

        public IAuthenticationRequest CreateRequest(Identifier userSuppliedIdentifier, Realm realm)
        {
            return OpenId.CreateRequest(userSuppliedIdentifier, realm);
        }
    }
}