﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shrinkr.Web
{
    public class CreateUserCommand
    {
        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>The username.</value>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the API access.
        /// </summary>
        /// <value>The API access.</value>
        public int ApiAccess { get; set; }
    }
}
