﻿namespace Shrinkr.Web
{
    public class ShortUrlVisitApiCommand : ApiCommand
    {
        /// <summary>
        ///     Gets or sets the short URL.
        /// </summary>
        /// <value>The short URL.</value>
        public string ShortUrl { get; set; }
    }
}