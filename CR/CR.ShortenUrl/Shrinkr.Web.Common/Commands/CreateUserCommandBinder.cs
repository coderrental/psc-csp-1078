﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using MvcExtensions;
namespace Shrinkr.Web
{
    [BindingType(typeof(CreateUserCommand))]
    public class CreateUserCommandBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            Invariant.IsNotNull(bindingContext, "bindingContext");

            var command = (CreateUserCommand)base.BindModel(controllerContext, bindingContext);

            if (string.IsNullOrWhiteSpace(command.Username))
            {
                ValueProviderResult username = bindingContext.ValueProvider.GetValue("createuser_name");

                if (username != null)
                {
                    command.Username = username.AttemptedValue;
                }
            }

            if (string.IsNullOrWhiteSpace(command.Email))
            {
                var email = bindingContext.ValueProvider.GetValue("createuser_email");
                if (email != null)
                    command.Email = email.AttemptedValue;
            }


            ValueProviderResult dailylimit = bindingContext.ValueProvider.GetValue("createuser_dailyLimit");

            if (dailylimit != null)
            {
                command.ApiAccess = (int)dailylimit.ConvertTo(typeof(int?), Culture.Current);
            }
            

            return command;
        }
    }
}
