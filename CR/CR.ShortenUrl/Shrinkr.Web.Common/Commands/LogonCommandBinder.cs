﻿using System.Web.Mvc;
using MvcExtensions;

namespace Shrinkr.Web
{
    [BindingType(typeof (LogOnCommand))]
    public class LogOnCommandBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            Invariant.IsNotNull(bindingContext, "bindingContext");

            var command = (LogOnCommand) base.BindModel(controllerContext, bindingContext);

            if (string.IsNullOrWhiteSpace(command.UserName))
            {
                ValueProviderResult result = bindingContext.ValueProvider.GetValue("logon_username");

                if (result != null)
                {
                    command.UserName = result.AttemptedValue;
                }
            }

            if (string.IsNullOrWhiteSpace(command.Password))
            {
                var result = bindingContext.ValueProvider.GetValue("logon_password");
                if (result != null)
                    command.Password = result.AttemptedValue;
            }

            if (!command.RememberMe.HasValue)
            {
                ValueProviderResult result = bindingContext.ValueProvider.GetValue("logon_remember");

                if (result != null)
                {
                    command.RememberMe = (bool?) result.ConvertTo(typeof (bool?), Culture.Current);
                }
            }

            return command;
        }
    }
}