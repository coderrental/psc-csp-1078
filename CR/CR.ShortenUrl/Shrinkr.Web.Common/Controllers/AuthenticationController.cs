﻿using System;
using System.Web.Mvc;
using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OpenId;
using DotNetOpenAuth.OpenId.Extensions.SimpleRegistration;
using DotNetOpenAuth.OpenId.RelyingParty;
using Shrinkr.Extensions;
using Shrinkr.Infrastructure;
using Shrinkr.Services;

namespace Shrinkr.Web
{
    public class AuthenticationController : Controller
    {
        internal const string ModelStateUserNameKey = "userName";

        internal const string CookieRememberMe = "oidr";
        internal const string CookieReturnUrl = "oidu";

        private const int SevenDays = 60*60*24*7;

        private readonly ICookie cookie;
        private readonly IFormsAuthentication formsAuthentication;
        private readonly IOpenIdRelyingParty openId;
        private readonly UserService userService;

        public AuthenticationController(IOpenIdRelyingParty openId, IFormsAuthentication formsAuthentication,
                                        ICookie cookie, UserService userService)
        {
            this.openId = openId;
            this.formsAuthentication = formsAuthentication;
            this.cookie = cookie;
            this.userService = userService;
        }

        [OutputCache(Duration = SevenDays, VaryByParam = "none")]
        public ActionResult Xrds()
        {
            const string Xrds = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                                "<xrds:XRDS xmlns:xrds=\"xri://$xrds\" xmlns:openid=\"http://openid.net/xmlns/1.0\" xmlns=\"xri://$xrd*($v*2.0)\">" +
                                "<XRD>" +
                                "<Service priority=\"1\">" +
                                "<Type>http://specs.openid.net/auth/2.0/return_to</Type>" +
                                "<URI>{0}</URI>" +
                                "</Service>" +
                                "</XRD>" +
                                "</xrds:XRDS>";

            string url = Url.ToAbsolute(Url.LogOn());

            string xml = Xrds.FormatWith(url);

            return Content(xml, "application/xrds+xml");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LogOn()
        {
           if (userService.GetNumberOfUsers() == 0)
           {
               //need setup
               return Redirect(Url.Setup());
           }
            
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LogOn(LogOnCommand command)
        {
            var errorMessage = string.Empty;
            if (string.IsNullOrWhiteSpace(command.UserName) || string.IsNullOrWhiteSpace(command.Password))
            {
                errorMessage = "Missing username or password";
                ModelState.AddModelError(ModelStateUserNameKey, errorMessage);
            }
            else
            {
                if (!userService.IsSuperAdministrator(command.UserName, command.Password))
                {
                    errorMessage = "The username and password does not match";
                    ModelState.AddModelError(ModelStateUserNameKey, errorMessage);
                }
                else // is administrator
                {
                    cookie.SetValue(CookieRememberMe, command.RememberMe ?? false);
                    cookie.SetValue(CookieReturnUrl,
                                    !string.IsNullOrWhiteSpace(command.ReturnUrl) ? command.ReturnUrl : Url.Home());

                    var persistCookie = cookie.GetValue<bool>(CookieRememberMe);

                    formsAuthentication.SetAuthenticationCookie(command.UserName, persistCookie);

                    var returnUrl = cookie.GetValue<string>(CookieReturnUrl);

                    return Redirect(returnUrl ?? Url.Home());
                }
            }
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LogOff()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LogOff(string returnUrl)
        {
            formsAuthentication.LogOff();

            return Redirect(string.IsNullOrWhiteSpace(returnUrl) ? Url.LogOn() : returnUrl);
        }
    }
}