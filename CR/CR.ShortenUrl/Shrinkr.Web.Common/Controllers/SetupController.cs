﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using Shrinkr.Services;
namespace Shrinkr.Web
{
    public class SetupController : Controller
    {
        private UserService _userService;
        public SetupController(UserService userService)
        {
            _userService = userService;
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Setup()
        {
            if (_userService.GetNumberOfUsers() != 0)
                return Redirect(Url.LogOn());
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Setup(FormCollection form)
        {
            
            var username = form.GetValue("createadmin_name").AttemptedValue;
            var password = form.GetValue("createadmin_password").AttemptedValue;
            var email = form.GetValue("createadmin_email").AttemptedValue;

            //validate field
            var errorMessage = string.Empty;
            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password) || string.IsNullOrWhiteSpace(email))
            {
                errorMessage = "Missing required fields";
            }
            else if (!Check.Argument.IsValidEmail(email))
            {
                errorMessage = "Invalid email address";
                
            }
            else if (password.Length < Membership.MinRequiredPasswordLength)
            {
                var minimum = Membership.MinRequiredPasswordLength.ToString();
                errorMessage = "Minimum required password lenght is " + minimum;
            }

           if (string.IsNullOrEmpty(errorMessage))
           {
               _userService.CreateAdministrator(username, password, email);
               return Redirect(Url.LogOn());
           }
           else
           {
               ModelState.AddModelError("userName", errorMessage);
           }
            return View();
        }
    }
}
