﻿using System.Linq;
using System.Web.Mvc;
using Shrinkr.Services;

namespace Shrinkr.Web
{
    public class ApiController : Controller
    {
        private readonly IShortUrlService shortUrlService;

        public ApiController(IShortUrlService shortUrlService)
        {
            this.shortUrlService = shortUrlService;
        }

        public ActionResult Create(CreateShortUrlApiCommand command)
        {
            ShortUrlResult result = shortUrlService.CreateWithApiKey(command.Url, command.Alias, command.IPAddress,
                                                                     command.ApiKey);

            ModelState.Merge(result.RuleViolations);

            if (result.ShortUrl != null)
            {
                ViewData.Model = new CreateUrlViewModel(result.ShortUrl);
            }

            return new ApiResult(command.ResponseFormat);
        }

        public ActionResult View(ShortUrlVisitApiCommand command)
        {
            string alias = command.ShortUrl.Split('/').Last();
            ShortUrlResult result = shortUrlService.GetByAlias(alias);

            ModelState.Merge(result.RuleViolations);

            if (result.ShortUrl != null)
            {
                ViewData.Model = new VisitUrlViewModel(result.ShortUrl);
            }

            return new ApiResult(command.ResponseFormat);
        }
    }
}