﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using MvcExtensions;
using Newtonsoft.Json.Linq;
using Shrinkr.DataTransferObjects;
using Shrinkr.DomainObjects;
using Shrinkr.Infrastructure;
using Shrinkr.Services;
using Shrinkr.Web.ViewModels;
using Telerik.Web.Mvc;

namespace Shrinkr.Web
{
    public class ControlPanelController : Controller
    {
        private const int ItemPerPage = 10;

        private readonly IAdministrativeService administrativeService;
        private string _connectionString;
        public ControlPanelController(IAdministrativeService administrativeService)
        {
            this.administrativeService = administrativeService;
            _connectionString = ConfigurationManager.ConnectionStrings["CRShortenUrl"].ConnectionString;
        }

        public ActionResult SummaryUser()
        {
            return View();
        }

        public ActionResult Summary(FormCollection form)
        {
           
            var days = 90;
            if (form != null && form["listDays"] != null)
                days = int.Parse(form["listDays"]);


            var listDays = new List<SelectListItem>();
            listDays.Add(new SelectListItem() { Selected = false, Text = "30 days", Value = "30" });
            listDays.Add(new SelectListItem() { Selected = false, Text = "60 days", Value = "60" });
            listDays.Add(new SelectListItem() { Selected = true, Text = "90 days", Value = "90" });
           
            ViewData["listDays"] = new SelectList(listDays, "Value", "Text", days);
            ViewData["staticsDay"] = days + " days";

            var userUrls = new Dictionary<UserDTO, int>();
            var users = administrativeService.GetUsers().Where(a => !a.IsLockedOut && a.CreatedAt > DateTime.Now.AddDays(-days));
            foreach (var user in users)
            {
                using (var conn = new SqlConnection(_connectionString))
                {
                    if (conn.State != ConnectionState.Open)
                        conn.Open();
                    var query = "Select Count (*) as total from Alias where UserId = " + user.Id;
                    using (var command = new SqlCommand(query, conn))
                    {
                        var total = (int) command.ExecuteScalar();
                        userUrls.Add(user, total);
                    }
                }
               if (!userUrls.ContainsKey(user))
                   userUrls.Add(user, 0);
            }

            var listUserString = string.Empty;
            var listUrlString = string.Empty;
            foreach (var i in userUrls)
            {
                listUserString += string.Format("'{0}',", i.Key.Name);
                listUrlString += string.Format("{0},", i.Value);
            }
            ViewData.Add("listUserString",(listUserString.Length > 0) ? listUserString.Remove(listUserString.Length-1): string.Empty);
            ViewData.Add("listUrlString", (listUrlString.Length > 0) ? listUrlString.Remove(listUrlString.Length - 1) : string.Empty);


            IEnumerable<ShortUrlDTO> urls = administrativeService.GetShortUrls().Where(a => a.CreatedAt >= DateTime.Now.AddDays(-days)).OrderByDescending(a => a.Visits).Take(10);
            //ViewData["urlsGrid"] = urls;
            return View(new GridModel<ShortUrlDTO> { Data = urls });

           // return View();
        }

        [GridAction]
        public ActionResult Urls(int? page, string orderBy, string filter)
        {
            IEnumerable<ShortUrlDTO> urls = administrativeService.GetShortUrls().OrderByDescending(a => a.CreatedAt);
            return View(new GridModel<ShortUrlDTO> {Data = urls});
        }

        [ActionName("Url")]
        public ActionResult ShortUrl(string alias)
        {
            ShortUrlDTO model = administrativeService.GetShortUrl(alias);

            return View(model);
        }

        [ActionName("CreateUser")]
        public ActionResult CreateUser()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateUser(CreateUserCommand command)
        {
            if (string.IsNullOrEmpty(command.Username) || string.IsNullOrEmpty(command.Email))
            {
                ModelState.AddModelError("createuser", "Missing username or email");
            }
            else if (!Check.Argument.IsValidEmail(command.Email))
            {
                ModelState.AddModelError("createuser", "Invalid email address");   
            }
            else
            {
                var user = administrativeService.GetUserByName(command.Username);
                if (user != null)
                {
                    ModelState.AddModelError("createuser", string.Format("The user with the following username: {0} existed", user.Name));
                }
                else
                {
                    try
                    {
                        user = new User
                        {
                            Email = command.Email,
                            Name = command.Username,
                            CreatedAt = DateTime.Now,
                            Role = Role.User,
                            IsLockedOut = false,
                        };
                        user.AllowApiAccess(command.ApiAccess);
                        var newUser = administrativeService.CreateUser(user);
                        if (newUser != null)
                            return this.AdaptivePostRedirectGet(Url.User(newUser.Id));
                    }
                    catch (Exception exception)
                    {
                        ModelState.AddModelError("createuser", "Can't create new user. Error: " + exception.Message);
                    }
                }
                
            }
            return View();
        }

        [HttpPost]
        public ActionResult MarkUrlAsSpam(string alias)
        {
            return UpdateShortUrlSpamStatus(alias, SpamStatus.BadWord);
        }

        [HttpPost]
        public ActionResult MarkUrlAsSafe(string alias)
        {
            return UpdateShortUrlSpamStatus(alias, SpamStatus.Clean);
        }

        [GridAction]
        public ActionResult Users(int? page, string orderBy, string filter)
        {
            IEnumerable<UserDTO> users = administrativeService.GetUsers();

            return View(new GridModel<UserDTO> {Data = users});
        }

        [ActionName("User")]
        public ActionResult Member(long id)
        {
            UserDTO model = administrativeService.GetUser(id);

            return View(model);
        }

        [HttpPost]
        public ActionResult LockUser(long id)
        {
            return LockOrUnlockUser(id, false);
        }

        [HttpPost]
        public ActionResult UnlockUser(long id)
        {
            return LockOrUnlockUser(id, true);
        }

        [HttpPost]
        public ActionResult UpdateUserRole(long id, Role role)
        {
            administrativeService.UpdateUserRole(id, role);

            return this.AdaptivePostRedirectGet(null, administrativeService.GetUser(id), Url.User(id));
        }

        [HttpPost]
        public ActionResult UpdateUserApiAccess(long id, int dailyLimit)
        {
            administrativeService.UpdateUserApiAccess(id, dailyLimit);

            return this.AdaptivePostRedirectGet(null, administrativeService.GetUser(id), Url.User(id));
        }

        [HttpPost]
        public ActionResult ChangeUserRoleToAdministrator(long id)
        {
            return UpdateUserRole(id, Role.Administrator);
        }

        [HttpPost]
        public ActionResult ChangeUserRoleToUser(long id)
        {
            return UpdateUserRole(id, Role.User);
        }

        [ImportViewDataFromTempData]
        public ActionResult BannedIPAddresses(int? page)
        {
            return PrepareListActionResult(page, () => administrativeService.GetBannedIPAddresses());
        }

        [HttpPost, ExportViewDataToTempData]
        public ActionResult CreateBannedIPAddress(string ipAddress)
        {
            AdministrativeActionResult<BannedIPAddress> result = administrativeService.CreateBannedIPAddress(ipAddress);

            return this.AdaptivePostRedirectGet(result.RuleViolations, result.Item, Url.BannedIPAddresses(1));
        }

        [HttpPost, ExportViewDataToTempData]
        public ActionResult DeleteBannedIPAddress(long id)
        {
            administrativeService.DeleteBannedIPAddress(id);

            return this.AdaptivePostRedirectGet(Url.BannedIPAddresses(1));
        }

        [ImportViewDataFromTempData]
        public ActionResult BannedDomains(int page)
        {
            return PrepareListActionResult(page, () => administrativeService.GetBannedDomains());
        }

        [HttpPost, ExportViewDataToTempData]
        public ActionResult CreateBannedDomain(string name)
        {
            AdministrativeActionResult<BannedDomain> result = administrativeService.CreateBannedDomain(name);

            return this.AdaptivePostRedirectGet(result.RuleViolations, result.Item, Url.BannedDomains(1));
        }

        [HttpPost, ExportViewDataToTempData]
        public ActionResult DeleteBannedDomain(long id)
        {
            administrativeService.DeleteBannedDomain(id);

            return this.AdaptivePostRedirectGet(Url.BannedDomains(1));
        }

        [ImportViewDataFromTempData]
        public ActionResult ReservedAliases(int page)
        {
            return PrepareListActionResult(page, () => administrativeService.GetReservedAliases());
        }

        [HttpPost, ExportViewDataToTempData]
        public ActionResult CreateReservedAlias(string aliasName)
        {
            AdministrativeActionResult<ReservedAlias> result = administrativeService.CreateReservedAlias(aliasName);

            return this.AdaptivePostRedirectGet(result.RuleViolations, result.Item, Url.ReservedAliases(1));
        }

        [HttpPost, ExportViewDataToTempData]
        public ActionResult DeleteReservedAlias(long id)
        {
            administrativeService.DeleteReservedAlias(id);

            return this.AdaptivePostRedirectGet(Url.ReservedAliases(1));
        }

        [ImportViewDataFromTempData]
        public ActionResult BadWords(int page)
        {
            return PrepareListActionResult(page, () => administrativeService.GetBadWords());
        }

        [HttpPost, ExportViewDataToTempData]
        public ActionResult CreateBadWord(string expression)
        {
            AdministrativeActionResult<BadWord> result = administrativeService.CreateBadWord(expression);

            return this.AdaptivePostRedirectGet(result.RuleViolations, result.Item, Url.BadWords(1));
        }

        [HttpPost, ExportViewDataToTempData]
        public ActionResult DeleteBadWord(long id)
        {
            administrativeService.DeleteBadWord(id);

            return this.AdaptivePostRedirectGet(Url.BadWords(1));
        }

        private ActionResult UpdateShortUrlSpamStatus(string alias, SpamStatus status)
        {
            administrativeService.UpdateShortUrlSpamStatus(alias, status);

            return this.AdaptivePostRedirectGet(null, administrativeService.GetShortUrl(alias), Url.Url(alias));
        }

        private ActionResult LockOrUnlockUser(long id, bool unlock)
        {
            administrativeService.LockOrUnlockUser(id, unlock);

            return this.AdaptivePostRedirectGet(null, administrativeService.GetUser(id), Url.User(id));
        }

        private ActionResult PrepareListActionResult<TItem>(int? page, Func<IEnumerable<TItem>> getItems)
            where TItem : class
        {
            IEnumerable<TItem> items = getItems().Skip(PageCalculator.StartIndex(page, ItemPerPage))
                                                 .Take(ItemPerPage);

            int count = getItems().Count();

            ViewData.Model = new PagedListViewModel<TItem>(items, page ?? 1, ItemPerPage, count);

            return this.AdaptiveView();
        }

        [HttpGet]
        public ActionResult ShortenUrl()
        {
            var listUsers = GetDropdownListUser();
            ViewData.Add(new KeyValuePair<string, object>("ListUsers", listUsers));
            return View();
        }
        [HttpPost]
        public ActionResult ShortenUrl(FormCollection form)
        {
            var ApiKeySelected = form.Get("ListUsers");
            var listUsers = GetDropdownListUser();
            ViewData["ListUsers"] = new SelectList(listUsers, "Value", "Text", ApiKeySelected);
            var url = Url.Encode(form.GetValue("url").AttemptedValue);
            ViewData["OriginalUrl"] = form.GetValue("url").AttemptedValue;
            if (string.IsNullOrEmpty(url))
            {
                ModelState.AddModelError("shortenurl", "Missing url");
            }
            else
            {
                var apiurl = Url.Api();
                if (!string.IsNullOrEmpty(apiurl))
                {
                    apiurl = apiurl + "?url=" + url + "&apikey=" + ApiKeySelected;
                    WebRequest request = WebRequest.Create(apiurl);
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        using (Stream stream = response.GetResponseStream())
                        {
                            if (stream != null)
                            {
                                var sr = new StreamReader(stream);
                                var responseUrl = sr.ReadToEnd();
                                if (responseUrl.EndsWith("]")|| responseUrl.StartsWith("["))
                                {
                                    responseUrl = responseUrl.TrimStart('[').TrimEnd(']');
                                }
                                var Obj = JObject.Parse(responseUrl);
                                if (Obj["shortUrl"] != null)
                                {
                                    ViewData.Add(new KeyValuePair<string, object>("ShortenUrl", Obj["shortUrl"].ToString()));    
                                }
                                if (Obj["messages"] != null)
                                {
                                    ModelState.AddModelError("shortenurl", Obj["messages"].ToString());
                                }
                            }
                        }
                    }
                }             
            }
            return View();
        }
        public IList<SelectListItem> GetDropdownListUser()
        {
            var listUsers = new List<SelectListItem>();
            var users = administrativeService.GetUsers();
            if (users.Any())
            {
                foreach (var userDto in users.Where(a=>a.IsLockedOut == false && a.CanAccessApi && !a.HasExceededApiDailyLimit && a.ApiAccessAllowed))
                {
                    listUsers.Add(new SelectListItem
                        {
                            Text = userDto.Name,
                            Value = userDto.ApiKey
                        });
                }
            }
            return listUsers;
        }

    }
}