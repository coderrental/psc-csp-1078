﻿using System.Web.Mvc;
using MvcExtensions;
using Shrinkr.DataTransferObjects;
using Shrinkr.Services;

namespace Shrinkr.Web
{
    public class UserController : Controller
    {
        private readonly IUserService userService;

        public UserController(IUserService userService)
        {
            this.userService = userService;
        }

        [ImportViewDataFromTempData]
        public ActionResult Profile(ProfileCommand command)
        {
            UserDTO model = userService.GetByName(command.UserName);

            return View(model);
        }

        [HttpPost, ExportViewDataToTempData]
        public ActionResult GenerateKey(ProfileCommand command)
        {
            UserResult result = userService.RegenerateApiKey(command.UserName);

            object model = result.User != null ? new {apiKey = result.User.ApiKey} : null;

            return this.AdaptivePostRedirectGet(result.RuleViolations, model, Url.Profile());
        }
    }
}