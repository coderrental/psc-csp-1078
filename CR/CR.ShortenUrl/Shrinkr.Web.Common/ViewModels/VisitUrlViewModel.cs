﻿using System;
using Shrinkr.DataTransferObjects;

namespace Shrinkr.Web
{
    public class VisitUrlViewModel
    {
        public VisitUrlViewModel(ShortUrlDTO shortUrl)
        {
            Title = shortUrl.Title;
            Url = shortUrl.Url;
            Domain = shortUrl.Domain;
            Visits = shortUrl.Visits;
            CreatedAt = shortUrl.CreatedAt;
        }

        public string Title { get; private set; }

        public string Url { get; private set; }

        /// <summary>
        ///     Gets or sets the domain.
        /// </summary>
        /// <value>The domain.</value>
        public string Domain { get; set; }

        public DateTime CreatedAt { get; set; }

        /// <summary>
        ///     Gets or sets the visits.
        /// </summary>
        /// <value>The visits.</value>
        public int Visits { get; set; }
    }
}