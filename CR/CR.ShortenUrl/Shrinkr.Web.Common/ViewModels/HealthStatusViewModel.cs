﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shrinkr.Web.ViewModels
{
    public class HealthStatusViewModel
    {
        /// <summary>
        /// Gets or sets the duration.
        /// </summary>
        /// <value>The duration.</value>
        public string Duration { get; set; }

        /// <summary>
        /// Gets or sets the URL created.
        /// </summary>
        /// <value>The URL created.</value>
        public int UrlCreated { get; set; }

        /// <summary>
        /// Gets or sets the URL visited.
        /// </summary>
        /// <value>The URL visited.</value>
        public int UrlVisited { get; set; }

        /// <summary>
        /// Gets or sets the user created.
        /// </summary>
        /// <value>The user created.</value>
        public int UserCreated { get; set; }

        /// <summary>
        /// Gets or sets the user visited.
        /// </summary>
        /// <value>The user visited.</value>
        public int UserVisited { get; set; }
    }
}
