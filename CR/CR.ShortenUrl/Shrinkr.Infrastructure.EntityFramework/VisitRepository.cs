﻿using Shrinkr.DomainObjects;
using Shrinkr.Repositories;

namespace Shrinkr.Infrastructure.EntityFramework
{
    public class VisitRepository : RepositoryBase<Visit>, IVisitRepository
    {
        public VisitRepository(IDatabaseFactory databaseFactory, IQueryFactory queryFactory)
            : base(databaseFactory, queryFactory)
        {
        }

        public int Count(string aliasName)
        {
            IQuery<int> query = QueryFactory.CreateVisitCountByAlias(aliasName);

            return query.Execute(Database);
        }
    }
}