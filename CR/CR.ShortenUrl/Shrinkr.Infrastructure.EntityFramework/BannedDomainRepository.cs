﻿using Shrinkr.DomainObjects;
using Shrinkr.Repositories;

namespace Shrinkr.Infrastructure.EntityFramework
{
    public class BannedDomainRepository : RepositoryBase<BannedDomain>, IBannedDomainRepository
    {
        public BannedDomainRepository(IDatabaseFactory databaseFactory, IQueryFactory queryFactory)
            : base(databaseFactory, queryFactory)
        {
        }

        public bool IsMatching(string url)
        {
            return QueryFactory.CreateBannedDomainMatching(url).Execute(Database);
        }
    }
}