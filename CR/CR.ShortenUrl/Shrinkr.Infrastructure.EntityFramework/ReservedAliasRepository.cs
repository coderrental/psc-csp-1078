﻿using Shrinkr.DomainObjects;
using Shrinkr.Repositories;

namespace Shrinkr.Infrastructure.EntityFramework
{
    public class ReservedAliasRepository : RepositoryBase<ReservedAlias>, IReservedAliasRepository
    {
        public ReservedAliasRepository(IDatabaseFactory databaseFactory, IQueryFactory queryFactory)
            : base(databaseFactory, queryFactory)
        {
        }

        public bool IsMatching(string aliasName)
        {
            return QueryFactory.CreateReservedAliasMatching(aliasName).Execute(Database);
        }
    }
}