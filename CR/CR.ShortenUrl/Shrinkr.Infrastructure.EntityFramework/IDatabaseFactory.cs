﻿using System;

namespace Shrinkr.Infrastructure.EntityFramework
{
    public interface IDatabaseFactory : IDisposable
    {
        Database Get();
    }
}