﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;
using Shrinkr.DomainObjects;

namespace Shrinkr.Infrastructure.EntityFramework.Configuration
{
    public class ReservedAliasConfiguration : EntityTypeConfiguration<ReservedAlias>
    {
        public ReservedAliasConfiguration()
        {
            HasKey(r => r.Id);

            Property(r => r.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(r => r.Name).IsUnicode().IsRequired().IsVariableLength().HasMaxLength(440);

            ToTable("ReservedAlias");
        }
    }
}