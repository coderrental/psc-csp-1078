﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;
using Shrinkr.DomainObjects;

namespace Shrinkr.Infrastructure.EntityFramework.Configuration
{
    public class BannedDomainConfiguration : EntityTypeConfiguration<BannedDomain>
    {
        public BannedDomainConfiguration()
        {
            HasKey(b => b.Id);

            Property(b => b.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(b => b.Name).IsUnicode().IsRequired().IsVariableLength().HasMaxLength(440);

            ToTable("BannedDomain");
        }
    }
}