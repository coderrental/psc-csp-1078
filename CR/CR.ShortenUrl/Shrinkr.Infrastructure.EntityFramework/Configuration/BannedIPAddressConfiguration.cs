﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;
using Shrinkr.DomainObjects;

namespace Shrinkr.Infrastructure.EntityFramework.Configuration
{
    public class BannedIPAddressConfiguration : EntityTypeConfiguration<BannedIPAddress>
    {
        public BannedIPAddressConfiguration()
        {
            HasKey(b => b.Id);

            Property(b => b.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(b => b.IPAddress).IsUnicode(false).IsRequired().IsVariableLength().HasMaxLength(15);

            ToTable("BannedIPAddress");
        }
    }
}