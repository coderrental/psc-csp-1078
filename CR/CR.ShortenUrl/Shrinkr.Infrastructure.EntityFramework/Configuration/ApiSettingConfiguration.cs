using System.Data.Entity.ModelConfiguration;
using Shrinkr.DomainObjects;

namespace Shrinkr.Infrastructure.EntityFramework.Configuration
{
    public class ApiSettingConfiguration : ComplexTypeConfiguration<ApiSetting>
    {
        public ApiSettingConfiguration()
        {
            Property(s => s.Key).IsUnicode()
                                .IsOptional()
                                .IsVariableLength()
                                .HasMaxLength(36)
                                .HasColumnName("ApiKey");
            Property(s => s.Allowed).HasColumnName("ApiAllowed");
            Property(s => s.DailyLimit).HasColumnName("DailyLimit");
        }
    }
}