﻿using System;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using Shrinkr.DomainObjects;

namespace Shrinkr.Infrastructure.EntityFramework.Query
{
    public class ShortUrlByHashQuery : QueryBase<ShortUrl>
    {
        private static readonly Expression<Func<Database, string, ShortUrl>> expression =
            (database, hash) => database.ShortUrls.FirstOrDefault(shortUrl => shortUrl.Hash == hash);

        private static readonly Func<Database, string, ShortUrl> plainQuery = expression.Compile();
        private static readonly Func<Database, string, ShortUrl> compiledQuery = CompiledQuery.Compile(expression);

        private readonly string urlHash;

        public ShortUrlByHashQuery(bool useCompiled, string urlHash) : base(useCompiled)
        {
            this.urlHash = urlHash;
        }

        public override ShortUrl Execute(Database database)
        {
            return UseCompiled
                       ? compiledQuery(database, urlHash)
                       : plainQuery(database, urlHash);
        }
    }
}