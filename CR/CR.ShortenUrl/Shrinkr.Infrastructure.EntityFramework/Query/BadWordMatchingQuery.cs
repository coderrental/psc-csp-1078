﻿using System;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;

namespace Shrinkr.Infrastructure.EntityFramework.Query
{
    public class BadWordMatchingQuery : QueryBase<bool>
    {
        private static readonly Expression<Func<Database, string, bool>> expression =
            (database, e) => database.BadWords.Any(word => word.Expression == e);

        private static readonly Func<Database, string, bool> plainQuery = expression.Compile();
        private static readonly Func<Database, string, bool> compiledQuery = CompiledQuery.Compile(expression);

        private readonly string badWord;

        public BadWordMatchingQuery(bool useCompiled, string badWord) : base(useCompiled)
        {
            this.badWord = badWord;
        }

        public override bool Execute(Database database)
        {
            return UseCompiled
                       ? compiledQuery(database, badWord)
                       : plainQuery(database, badWord);
        }
    }
}