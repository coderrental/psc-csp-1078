﻿using System;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using Shrinkr.DomainObjects;

namespace Shrinkr.Infrastructure.EntityFramework.Query
{
    public class ShortUrlByIdQuery : QueryBase<ShortUrl>
    {
        private static readonly Expression<Func<Database, long, ShortUrl>> expression =
            (database, id) => database.ShortUrls.SingleOrDefault(shortUrl => shortUrl.Id == id);

        private static readonly Func<Database, long, ShortUrl> plainQuery = expression.Compile();
        private static readonly Func<Database, long, ShortUrl> compiledQuery = CompiledQuery.Compile(expression);

        private readonly long shortUrlId;

        public ShortUrlByIdQuery(bool useCompiled, long shortUrlId) : base(useCompiled)
        {
            this.shortUrlId = shortUrlId;
        }

        public override ShortUrl Execute(Database database)
        {
            return UseCompiled
                       ? compiledQuery(database, shortUrlId)
                       : plainQuery(database, shortUrlId);
        }
    }
}