﻿using System;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using Shrinkr.DomainObjects;

namespace Shrinkr.Infrastructure.EntityFramework.Query
{
    public class UserByNameQuery : QueryBase<User>
    {
        private static readonly Expression<Func<Database, string, User>> expression =
            (database, name) => database.Users.SingleOrDefault(user => user.Name == name);

        private static readonly Func<Database, string, User> plainQuery = expression.Compile();
        private static readonly Func<Database, string, User> compiledQuery = CompiledQuery.Compile(expression);

        private readonly string userName;

        public UserByNameQuery(bool useCompiled, string userName) : base(useCompiled)
        {
            this.userName = userName;
        }

        public override User Execute(Database database)
        {
            return UseCompiled
                       ? compiledQuery(database, userName)
                       : plainQuery(database, userName);
        }
    }
}