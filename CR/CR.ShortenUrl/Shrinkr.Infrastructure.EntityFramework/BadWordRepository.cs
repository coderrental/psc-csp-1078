﻿using Shrinkr.DomainObjects;
using Shrinkr.Repositories;

namespace Shrinkr.Infrastructure.EntityFramework
{
    public class BadWordRepository : RepositoryBase<BadWord>, IBadWordRepository
    {
        public BadWordRepository(IDatabaseFactory databaseFactory, IQueryFactory queryFactory)
            : base(databaseFactory, queryFactory)
        {
        }

        public bool IsMatching(string expression)
        {
            return QueryFactory.CreateBadWordMatching(expression).Execute(Database);
        }
    }
}