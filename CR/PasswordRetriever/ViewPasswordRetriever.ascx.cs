
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;

namespace CR.Modules.PasswordRetriever
{

    /// ----------------------------------------------------------------------------- 
    /// <summary> 
    /// The ViewPasswordRetriever class displays the content 
    /// </summary> 
    /// <remarks> 
    /// </remarks> 
    /// <history> 
    /// </history> 
    /// ----------------------------------------------------------------------------- 
    partial class ViewPasswordRetriever : PortalModuleBase
    {
        #region "Event Handlers"

        /// ----------------------------------------------------------------------------- 
        /// <summary> 
        /// Page_Load runs when the control is loaded 
        /// </summary> 
        /// ----------------------------------------------------------------------------- 
        protected void Page_Init(object sender, System.EventArgs e)
        {
            if (Request.Headers["X-OFFICIAL-REQUEST"] == "TRUE")
            {
                AjaxWrapper();
                return;
            }
        }
        
        protected void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (Page.IsPostBack == false)
                {
                    this.GetAllDNNUser();
                }
            }

            catch (Exception exc)
            {
                //Module failed to load 
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }

        public void AjaxWrapper()
        {
            Response.ContentType = "text/xml";
            string action = Request.Params["action"];
            string resultCode = "0";
            int userId = Convert.ToInt32(Request.Params["userid"]);
            PasswordRetrieverController pwrController = new PasswordRetrieverController();
            if (action == "resendpassword")
            {
                if (pwrController.ResendPassword(PortalId, userId) == true)
                    resultCode = "1";
            }
            else if (action == "unlockaccount")
            {
                if (pwrController.UnlockAccount(PortalId, userId) == true)
                    resultCode = "1";
            }
            string strData = "<xmlresult><resultcode>" + resultCode + "</resultcode></xmlresult>";
            Response.Write(strData);
            Response.Flush();
            
            Response.End();
        }

        private void GetAllDNNUser()
        {
            PasswordRetrieverController prController = new PasswordRetrieverController();
            List<PasswordRetrieverInfo> dnnUserList = prController.GetAllUsers(PortalId);
            gridDNNUser.DataSource = dnnUserList;
            gridDNNUser.DataBind();
        }

        protected void bSearchByUserName_Click(object sender, EventArgs e)
        {
            string userName = txtUserName.Text;
            if (String.IsNullOrEmpty(userName))
            {
                this.GetAllDNNUser();
                bSearchByUserName.Text = "Search";
                return;
            }
            PasswordRetrieverController prController = new PasswordRetrieverController();
            PasswordRetrieverInfo dnnUserInfo = prController.GetByUserName(PortalId, userName);
            List<PasswordRetrieverInfo> dnnUserList = new List<PasswordRetrieverInfo>();
            if (String.IsNullOrEmpty(dnnUserInfo.UserName) == false)
                dnnUserList.Add(dnnUserInfo);
            else
            {
                txtUserName.Text = "";
                bSearchByUserName.Text = "Refresh list";
            }
            gridDNNUser.DataSource = dnnUserList;
            gridDNNUser.DataBind();
        }

        protected void bSearchByEmail_Click(object sender, EventArgs e)
        {
            string email = txtEmail.Text;
            if (String.IsNullOrEmpty(email))
            {
                this.GetAllDNNUser();
                bSearchByEmail.Text = "Search";
                return;
            }
            PasswordRetrieverController prController = new PasswordRetrieverController();
            List<PasswordRetrieverInfo> dnnUserList = prController.GetByEmail(PortalId, email);
            if (dnnUserList.Count == 0)
            {
                txtEmail.Text = "";
                bSearchByEmail.Text = "Refresh list";
            }
            gridDNNUser.DataSource = dnnUserList;
            gridDNNUser.DataBind();
        }

        #endregion
    }

}