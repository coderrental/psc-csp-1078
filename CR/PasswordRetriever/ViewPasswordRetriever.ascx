<%@ Control Language="C#" Inherits="CR.Modules.PasswordRetriever.ViewPasswordRetriever"
    AutoEventWireup="true" CodeBehind="ViewPasswordRetriever.ascx.cs" %>
<%@ Register assembly="Telerik.Web.UI, Version=2012.2.703.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" namespace="Telerik.Web.UI" tagprefix="telerik" %>
   <script type="text/javascript">
       function textboxEnterPrevent(sender, e) {
           if (e.keyCode == 13) {
               if (e.preventDefault) {
                   e.preventDefault();
               }
           }
       }

       function resendPassword(userId) {
           goAction('resendpassword', userId);
       }

       function unlockAccount(userId) {
           goAction('unlockaccount', userId);
       }

       function goAction(actionType, userId) {
           $.ajax({
               url: location.href,
               type: "POST",
               dataType: "xml",
               data: { action: actionType, userid: userId },
               success: function (data) {
                   var resultCode = $(data).find('resultcode').text();
                   var msg = 'Request failed!';
                   if (resultCode == 1) {
                       if (actionType == 'unlockaccount') {
                           msg = 'User unlocked';
                           $('#ua-' + userId).remove();
                       }
                       else
                           msg = 'Password sent';
                   }
                   alert(msg);
               },
               error: function (xhr, ajaxOptions, thrownError) {
                   alert(ajaxOptions);
               },
               beforeSend: function (xhr) {
                   xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
               }
           });
       }
    </script>
    <!-- This DIV use CSS relative pos for the children using absolute pos -->
    <div style="position: relative; margin-bottom: 15px">
        <!-- Search by user name -->
        <telerik:RadTextBox ID="txtUserName" Runat="server" 
            DisplayText="Search by user name" EmptyMessage="Search by user name" 
            LabelWidth="64px" Skin="Office2010Blue" type="text" value="" Width="160px" 
            onkeypress="textboxEnterPrevent(this, event)">
        </telerik:RadTextBox>
        <telerik:RadButton ID="bSearchByUserName" runat="server" Text="Search" 
            onclick="bSearchByUserName_Click">
        </telerik:RadButton>

        <!-- User CSS absolute pos to move this box to the right -->
        <span style="position: absolute; right: 0px">
            <!-- Search by password -->
            <telerik:RadTextBox ID="txtEmail" Runat="server" 
                DisplayText="Search by email" EmptyMessage="Search by email" 
                LabelWidth="64px" Skin="Office2010Blue" type="text" value="" Width="160px"
                 onkeypress="textboxEnterPrevent(this, event)">
            </telerik:RadTextBox>
            <telerik:RadButton ID="bSearchByEmail" runat="server" Skin="Default" 
            Text="Search" onclick="bSearchByEmail_Click">
            </telerik:RadButton>
        </span>
    </div>

    <!-- User list -->
    <telerik:RadGrid ID="gridDNNUser" runat="server" AutoGenerateColumns="False" 
    CellSpacing="0" GridLines="None">
        <MasterTableView>
            <CommandItemSettings ExportToPdfText="Export to PDF">
            </CommandItemSettings>
            <RowIndicatorColumn Visible="True" 
        FilterControlAltText="Filter RowIndicator column">
                <HeaderStyle Width="20px"></HeaderStyle>
            </RowIndicatorColumn>
            <ExpandCollapseColumn Visible="True" 
        FilterControlAltText="Filter ExpandColumn column">
                <HeaderStyle Width="20px"></HeaderStyle>
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn FilterControlAltText="Filter username column" 
            HeaderText="User Name" UniqueName="username" DataField="UserName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlAltText="Filter email column" 
            HeaderText="Email" UniqueName="email" DataField="Email">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlAltText="Filter password column" 
            HeaderText="Password" UniqueName="password" DataField="Password">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Action" UniqueName="action"
                DataField="UserId">
                    <ItemTemplate>
                        <a href="javascript: resendPassword(<%# Eval("UserId") %>)">[Resend Password]</a>&nbsp;
                        <%# ((bool)Eval("IsLocked") ? "<a id=\"ua-" + Eval("UserId") + "\" href=\"javascript: unlockAccount(" + Eval("UserId") + ")\">[Unlock]<a>" : "")%>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
            </Columns>
            <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
            </EditFormSettings>
        </MasterTableView>
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
    </telerik:RadGrid>