
using System;
using System.Configuration;
using System.Data;

namespace CR.Modules.PasswordRetriever
{

    /// ----------------------------------------------------------------------------- 
    /// <summary> 
    /// The Info class for PasswordRetriever 
    /// </summary> 
    /// <remarks> 
    /// </remarks> 
    /// <history> 
    /// </history> 
    /// ----------------------------------------------------------------------------- 
    public class PasswordRetrieverInfo
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsLocked { get; set; }

        public PasswordRetrieverInfo(int userId, string userName, string email, string pwd, bool isLocked)
        {
            this.UserId = userId;
            this.UserName = userName;
            this.Email = email;
            this.Password = pwd;
            this.IsLocked = isLocked;
        }

        public PasswordRetrieverInfo()
        {
            this.UserId = 0;
            this.UserName = "";
            this.Email = "";
            this.Password = "";
            this.IsLocked = false;
        }
    }

}