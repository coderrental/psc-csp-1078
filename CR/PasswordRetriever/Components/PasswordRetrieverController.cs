
using System;
using System.Configuration;
using System.Data;
using System.Xml;
using System.Web;
using System.Collections;
using System.Collections.Generic;

using DotNetNuke;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Services.Search;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Users;

namespace CR.Modules.PasswordRetriever
{

    /// ----------------------------------------------------------------------------- 
    /// <summary> 
    /// The Controller class for PasswordRetriever 
    /// </summary> 
    /// <remarks> 
    /// </remarks> 
    /// <history> 
    /// </history> 
    /// ----------------------------------------------------------------------------- 
    public class PasswordRetrieverController
    {
        #region "Public Methods"
        public List<PasswordRetrieverInfo> GetAllUsers(int PortalId)
        {
            List<PasswordRetrieverInfo> userList = new List<PasswordRetrieverInfo>();
            ArrayList rawUserList = UserController.GetUsers(PortalId);

            foreach (UserInfo dnnUser in rawUserList)
            {
                UserInfo tmpUser = UserController.GetUserById(PortalId, dnnUser.UserID);
                string userPassword = "";
                if (dnnUser.Membership.LockedOut == false)
                    userPassword = UserController.GetPassword(ref tmpUser, "");
                userList.Add(new PasswordRetrieverInfo(dnnUser.UserID, dnnUser.Username, dnnUser.Email, userPassword, dnnUser.Membership.LockedOut));
            }
            return userList;
        }

        public PasswordRetrieverInfo GetByUserName(int PortalId, string userName)
        {
            try
            {
                UserInfo dnnUser = UserController.GetUserByName(PortalId, userName);
                UserInfo tmpUser = UserController.GetUserById(PortalId, dnnUser.UserID);
                string userPassword = UserController.GetPassword(ref tmpUser, "");
                PasswordRetrieverInfo resultUserInfo = new PasswordRetrieverInfo(dnnUser.UserID, dnnUser.Username, dnnUser.Email, userPassword, dnnUser.Membership.LockedOut);
                return resultUserInfo;
            }
            catch (Exception exc)
            {
                return new PasswordRetrieverInfo();
            }
        }

        public List<PasswordRetrieverInfo> GetByEmail(int PortalId, string email)
        {
            try
            {
                List<PasswordRetrieverInfo> userList = this.GetAllUsers(PortalId);
                List<PasswordRetrieverInfo> resultList = new List<PasswordRetrieverInfo>();
                for (int i = 0; i < userList.Count; i++)
                {
                    if (String.Compare(userList[i].Email, email) == 0)
                        resultList.Add(userList[i]);
                }
                return resultList;
            }
            catch (Exception exc)
            {
                return new List<PasswordRetrieverInfo>();
            }
        }

        public bool ResendPassword(int portalId, int userId)
        {
            UserInfo dnnUser = UserController.GetUserById(portalId, userId);
            if (dnnUser.Membership.LockedOut == true)
                return false;
            DotNetNuke.Services.Mail.Mail.SendMail(dnnUser, DotNetNuke.Services.Mail.MessageType.PasswordReminder, DotNetNuke.Entities.Portals.PortalSettings.Current);
            return true;
        }

        public bool UnlockAccount(int portalId, int userId)
        {
            UserInfo dnnUser = UserController.GetUserById(portalId, userId);
            if (dnnUser.Membership.LockedOut == true)
                return false;
            return UserController.UnLockUser(dnnUser);
        }
        #endregion
    }
}