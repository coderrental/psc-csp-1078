﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainView.ascx.cs" Inherits="CR.DnnModules.SocialSendEmails.MainView" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server">  
</telerik:RadWindowManager>
<div class="containerModule">
<div class="headerModule">
    <div class="title left"><%=GetLocalizedText("Label.SocialSendEmail") %></div>
    <div class ="right header-right">
        <span class="header-link"><%=GetLocalizedText("Label.PartnerExpired") %></span>
        <span class="inactive"><a id="linkOldView" href="<%=GetUrlPartnerOldView() %>"><%=GetLocalizedText("Label.PartnerOld") %></a></span>
    </div>
    <div class="clear"></div>
</div>
<div class="border-top"></div>
<div class="btnContainer">
<asp:Button ID="btnUncheck" runat="server" CssClass="left" OnClick="btnUncheck_OnClick"/>
<asp:Button ID="btnCheck" runat="server" OnClick="btnCheck_OnClick" class="left button"/>
<asp:Button ID="btnSendEmails" OnClick="btnSendEmails_OnClick" CssClass="right" runat="server"/>
</div>
<div class="clear"></div>
<telerik:RadGrid ID="RadGridPartner" CssClass="rgPartnerModule" runat="server" AllowPaging="True" OnItemDataBound="RadGridPartner_OnItemDataBound" OnNeedDataSource="RadGridPartner_OnNeedDataSource" PageSize="20">
        <MasterTableView DataMember="Customers" AutoGenerateColumns="False" DataKeyNames="PartnerId">
            <Columns>
                <telerik:GridTemplateColumn UniqueName="CheckTemp" > 
                        <ItemTemplate> 
                            <asp:CheckBox ID="CheckBox1" runat="server" OnCheckedChanged="CheckChanged" AutoPostBack="True"/>             
                        </ItemTemplate> 
                </telerik:GridTemplateColumn> 
                <telerik:GridBoundColumn DataField="PartnerId" UniqueName="PartnerId" HeaderText="Partner Id" />
                <telerik:GridBoundColumn DataField="CompanyName" HeaderText="Company Name" />
			    <telerik:GridBoundColumn DataField="LastRenewDate" HeaderText="Last Renew Date" />
                <telerik:GridBoundColumn DataField="NextRenewDate" HeaderText="Next Renew Days"/>
                <telerik:GridBoundColumn DataField="Social" HeaderText="SocialId"/>
                <telerik:GridBoundColumn DataField="EmailRenewSentAt" HeaderText="Email Renew Sent At"/>          
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
</div>
<telerik:RadScriptBlock runat="server">
    <script type="text/javascript">
        function PushAlert(message) {
            setTimeout(function () {
                var oWnd = window.radalert(message, 330, 210, "<%=GetLocalizedText("Msg.Info") %>");
                var currentUrl = "<%=GetUrlCurrent()%>";
                oWnd.add_close(function () { window.location.href = currentUrl; });
            }, 0);  
        }

</script>
</telerik:RadScriptBlock>