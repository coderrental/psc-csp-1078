﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Users;
using DotNetNuke.Services.Localization;
using DnnDataContext = CR.DnnModules.SocialSendEmails.Data.DnnDataContext;

namespace CR.DnnModules.SocialSendEmails.Common
{
    public class DnnModuleBase : PortalModuleBase
    {
        private string _localResourceFile;
        public DnnDataContext DnnDataContext;
        public CspDataContext CspDataContext;
        public int ExpirationDay;
        protected string RenewTokenUrl;
        protected virtual void Page_Init(object sender, EventArgs e)
        {
            _localResourceFile = Utils.GetCommonResourceFile("SocialSendEmails", LocalResourceFile);
            DnnDataContext = new DnnDataContext(Config.GetConnectionString());
            CspDataContext = new CspDataContext(Utils.GetConnectionString(PortalId));

            if (Settings[Cons.SETTING_RENEWTOKEN_URL] != null && Settings[Cons.SETTING_RENEWTOKEN_URL].ToString() != string.Empty)
            {
                RenewTokenUrl = Settings[Cons.SETTING_RENEWTOKEN_URL].ToString();
            }
            ExpirationDay = 10;
            if (Settings[Cons.SETTING_EXPIRATION_DAY] != null && Settings[Cons.SETTING_EXPIRATION_DAY].ToString() != string.Empty)
            {
                int.TryParse(Settings[Cons.SETTING_EXPIRATION_DAY].ToString(), out ExpirationDay);
            }
        }
        /// <summary>
        /// Get localized text for the current portal language
        /// </summary>
        /// <param name="key">Resource Key</param>
        /// <returns>Localized text</returns>
        public string GetLocalizedText(string key)
        {
            return Localization.GetString(key, _localResourceFile);
        }

        public UserInfo GetUserInfoByCspId(int cspid)
        {
            UserInfo user = null;
            using (var conn = new SqlConnection(Config.GetConnectionString()))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                var query = string.Format("SELECT  ProfileID,UserID,dbo.UserProfile.PropertyDefinitionID ,PropertyName ,PropertyValue FROM dbo.UserProfile JOIN dbo.ProfilePropertyDefinition ON dbo.UserProfile.PropertyDefinitionID = dbo.ProfilePropertyDefinition.PropertyDefinitionID where PropertyName = 'CspId' and PropertyValue = '{0}'",cspid);
                var command = new SqlCommand(query, conn);
                using (var reader = command.ExecuteReader())
                {
                    try
                    {
                        while (reader.Read())
                        {
                            var userId = int.Parse(reader["UserID"].ToString());
                            user = UserController.GetUserById(PortalId, userId);
                            break;
                        }
                    }
                    finally 
                    {
                        
                        reader.Close();
                    }
                   
                }
            }
            return user;
        }
    }
}