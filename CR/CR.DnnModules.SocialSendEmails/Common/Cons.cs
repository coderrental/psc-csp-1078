﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CR.DnnModules.SocialSendEmails.Common
{
    public class Cons
    {
        public const string SETTING_RENEWTOKEN_URL = "settingrenewtokenurl";
        public const string SETTING_EXPIRATION_DAY = "settingexpirationday";
        public const string KEY_PARTNER_CHECK_STATE = "partnerstate";
    }
}