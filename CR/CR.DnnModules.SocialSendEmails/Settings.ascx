﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Settings.ascx.cs" Inherits="CR.DnnModules.SocialSendEmails.Settings" %>
<table>
    <tbody>
        <tr>
            <td><asp:Label ID="lblRenewtokenUrl" runat="server" ControlName="tbRenewtokenUrl" Text="Renew Token page url (eg. http://domain/renewtoken.aspx)" /></td>
            <td><asp:TextBox runat="server" ID="tbRenewtokenUrl"></asp:TextBox></td>
        </tr>
        <tr>
            <td><asp:Label ID="lblExpirationDay" runat="server" ControlName="tbExpirationDay" Text="Nearly expired token (days - 10 days by default)" /></td>
            <td><asp:TextBox runat="server" ID="tbExpirationDay"></asp:TextBox></td>
        </tr>
    </tbody>
</table>