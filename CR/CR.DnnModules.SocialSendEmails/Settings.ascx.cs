﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.DnnModules.SocialSendEmails.Common;
using DotNetNuke.Entities.Modules;

namespace CR.DnnModules.SocialSendEmails
{
    public partial class Settings : ModuleSettingsBase
    {
        /// <summary>
        /// Loads the settings.
        /// </summary>
        public override void LoadSettings()
        {
            if (TabModuleSettings[Cons.SETTING_RENEWTOKEN_URL] != null)
                tbRenewtokenUrl.Text = TabModuleSettings[Cons.SETTING_RENEWTOKEN_URL].ToString();
            if (TabModuleSettings[Cons.SETTING_EXPIRATION_DAY] != null)
                tbExpirationDay.Text = TabModuleSettings[Cons.SETTING_EXPIRATION_DAY].ToString();
        }

        public override void UpdateSettings()
        {
            var objModules = new ModuleController();
            objModules.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_RENEWTOKEN_URL, tbRenewtokenUrl.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_EXPIRATION_DAY, tbExpirationDay.Text);
            //refresh cache
            ModuleController.SynchronizeModule(ModuleId);
        }
    }
}