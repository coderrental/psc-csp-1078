﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.DnnModules.SocialSendEmails.Common;
using CR.DnnModules.SocialSendEmails.Data;
using DotNetNuke.Common;
using DotNetNuke.Services.Mail;
using Telerik.Web.UI;

namespace CR.DnnModules.SocialSendEmails
{
    public partial class PartnerOldView : DnnModuleBase
    {
        private int _companyParameterTypeEmailId;
        private List<int> _listPartnerSendCompleted;
        private Hashtable _listPartnerCheckState = new Hashtable();
        private List<CSPTSMM_Partner> _listPartner; 

        protected void Page_Load(object sender, EventArgs e)
        {
            btnSendEmails.Text = GetLocalizedText("btn.SendEmails");
            btnCheckAll.Text = GetLocalizedText("btnCheckAll.text");
            btnUncheck.Text = GetLocalizedText("btnUncheck.text");
        }
        protected override void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
            var listPartnerSubscriptionInfos = DnnDataContext.CSPTSMM_PartnersSubscriptionInfos.Where(a => a.PortalId == PortalId && a.SocialId == (int)SocialLib.Common.Socialtype.LinkedIn).Select(a => a.PartnerId);
            _listPartner = DnnDataContext.CSPTSMM_Partners.Where(a => a.PortalId == PortalId && a.SocialId == (int)SocialLib.Common.Socialtype.LinkedIn && !listPartnerSubscriptionInfos.Contains(a.CspId) && a.IsSubscribed).ToList();

        }
        private void GetListPartnerCheckState()
        {
            if (ViewState[Cons.KEY_PARTNER_CHECK_STATE] == null)
                _listPartnerCheckState = new Hashtable();
            else
                _listPartnerCheckState = (Hashtable)ViewState[Cons.KEY_PARTNER_CHECK_STATE];
        }
        private void UpdateListPartnerCheckState()
        {
            ViewState[Cons.KEY_PARTNER_CHECK_STATE] = _listPartnerCheckState;
        }
        protected void btnSendEmails_OnClick(object sender, EventArgs e)
        {
            try
            {
                GetListPartnerCheckState();
                if (_listPartnerCheckState.ContainsValue(true))
                {
                    _listPartnerSendCompleted = new List<int>();
                    var companyParameterTypeEmail = CspDataContext.companies_parameter_types.FirstOrDefault(a => a.parametername == "Personalize_Contact_Emailaddress");
                    if (companyParameterTypeEmail != null)
                    {
                        _companyParameterTypeEmailId = companyParameterTypeEmail.companies_parameter_types_Id;
                        var count = 0;
                        var smtpProblem = false;
                        foreach (DictionaryEntry partner in _listPartnerCheckState)
                        {
                            if ((bool)partner.Value)
                            {
                                var result = SendEmails(int.Parse(partner.Key.ToString()));
                                if (!string.IsNullOrEmpty(result) && count == 0 &&
                                    result.IndexOf("There is a problem with the configuration of your SMTP Server",
                                                   StringComparison.InvariantCultureIgnoreCase) > -1)
                                {
                                    smtpProblem = true;
                                    PushAlertMessage(result);
                                    break;
                                }


                            }
                            count++;
                        }
                        if (!smtpProblem)
                        {
                            var message = GetLocalizedText("Msg.SendEmailCompleted").Replace("{count}", _listPartnerSendCompleted.Count.ToString());
                            PushAlertMessage(message);
                        }

                    }
                    else
                    {
                        PushAlertMessage(GetLocalizedText("Msg.MissingCompanyParameter"));
                    }
                }
                else
                {
                    PushAlertMessage(GetLocalizedText("Msg.NoPartnerSeleted"));
                }
            }
            catch (Exception ex)
            {
                
               PushAlertMessage(ex.Message);
            }
        }

        private string SendEmails(int cspId)
        {
            var companyParameter = CspDataContext.companies_parameters.FirstOrDefault(a => a.companies_Id == cspId && a.companies_parameter_types_Id == _companyParameterTypeEmailId);
            var userInfo = GetUserInfoByCspId(cspId);
            if (companyParameter != null && userInfo != null)
            {
                var companyEmail = companyParameter.value_text;
                if (!string.IsNullOrEmpty(companyEmail))
                {
                    var tokenReplace = new DotNetNuke.Services.Tokens.TokenReplace();
                    var paramToEncrypt = DnnModules.Common.CryptoServiceProvider.Encode(string.Format("action=renew&cspid={0}&time={1}", cspId, DateTime.Now.Ticks));
                    var renewTokenUrl = string.Format("{0}?key={1}", RenewTokenUrl, HttpUtility.UrlEncode(paramToEncrypt));
                    var emailContent =
                       tokenReplace.ReplaceEnvironmentTokens(GetLocalizedText("Email.Content"))
                           .Replace("[Link]", renewTokenUrl)
                           .Replace("[DisplayName]", userInfo.DisplayName);
                    var result = Mail.SendMail(PortalSettings.Email, companyEmail, "", GetLocalizedText("Email.Title"), emailContent, "", "HTML", "", "", "", "");
                    if (string.IsNullOrEmpty(result))
                    {
                        _listPartnerSendCompleted.Add(cspId);
                    }
                    return result;
                }
            }
            return string.Empty;
        }
        private void PushAlertMessage(string message)
        {
            Page.ClientScript.RegisterStartupScript(GetType(), "alert", "<script type='text/javascript'>window.onload = function(){PushAlert('" + message + "')};</script>");
        }
        protected void RadGridPartner_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            RadGridPartner.DataSource = GetDataTable();
        }

        public string GetUrlMainView()
        {
            return Globals.NavigateURL(TabId);
        }

        private object GetDataTable()
        {
            var dt = new DataTable();
            dt.Columns.AddRange(new[] { new DataColumn("PartnerId"), new DataColumn("CompanyName"), new DataColumn("Social")});
            var firstRow = dt.NewRow();
            foreach (var column in dt.Columns)
            {
                firstRow[dt.Columns.IndexOf(column.ToString())] = column.ToString();
            }
           

            foreach (var partner in _listPartner)
            {
                var socialId = partner.SocialId;
                var socialType = string.Empty;
                switch (socialId)
                {
                    case 1:
                        socialType = "Twitter";
                        break;
                    case 2:
                        socialType = "LinkedIn";
                        break;
                    case 3:
                        socialType = "Facebook";
                        break;
                }
                var company = CspDataContext.companies.First(a => a.companies_Id == partner.CspId);
                var row = dt.NewRow();
                row[0] = partner.CspId;
                row[1] = company.companyname;
                row[2] = socialType;
                dt.Rows.Add(row);
            }
            return dt;
        }
        protected void CheckChanged(Object sender, System.EventArgs e)
        {
            GetListPartnerCheckState();
            var box = (CheckBox)sender;
            var item = (GridDataItem)box.NamingContainer;
            _listPartnerCheckState[int.Parse(item["PartnerId"].Text)] = box.Checked;
            UpdateListPartnerCheckState();
        }
        protected void btnUncheck_OnClick(object sender, EventArgs e)
        {
            _listPartnerCheckState = new Hashtable();
            UpdateListPartnerCheckState();
            RadGridPartner.Rebind();
        }

        protected void btnCheck_OnClick(object sender, EventArgs e)
        {
            GetListPartnerCheckState();
            //var listPartnerChecked = DnnDataContext.CSPTSMM_PartnersSubscriptionInfos.Where(a => a.SocialId == (int)SocialLib.Common.Socialtype.LinkedIn && a.PortalId == PortalId);
            foreach (var partner in _listPartner)
            {
                _listPartnerCheckState[partner.CspId] = true;
            }
            UpdateListPartnerCheckState();
            RadGridPartner.Rebind();
        }

        protected void RadGridPartner_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            GetListPartnerCheckState();
            if (e.Item is GridDataItem)
            {
                GridDataItem item = e.Item as GridDataItem;
                CheckBox box = (CheckBox)item.FindControl("CheckBox1");
                var id = int.Parse(item.GetDataKeyValue("PartnerId").ToString());
                if (_listPartnerCheckState[id] != null)
                    box.Checked = (bool)_listPartnerCheckState[id];
                else
                {
                    box.Checked = false;
                }
            }
        }
    }
}