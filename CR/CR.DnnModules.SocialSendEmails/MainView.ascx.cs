﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.DnnModules.SocialSendEmails.Common;
using CR.DnnModules.SocialSendEmails.Data;
using DotNetNuke;
using DotNetNuke.Common;
using DotNetNuke.Services.Mail;
using Telerik.Web.UI;

namespace CR.DnnModules.SocialSendEmails
{
    public partial class MainView : DnnModuleBase
    {
        private  Hashtable _listPartnerCheckState = new Hashtable();
        private int _companyParameterTypeEmailId;
        private List<int> _listPartnerSendCompleted;
        private IQueryable<int> _listPartnerIsSubscribed; 
        private List<CSPTSMM_PartnersSubscriptionInfo> _listPartnerExpiring;
        protected void Page_Load(object sender, EventArgs e)
        {

            btnSendEmails.Text = GetLocalizedText("btn.SendEmails");
            btnCheck.Text = GetLocalizedText("btnCheck.text");
            btnUncheck.Text = GetLocalizedText("btnUncheck.text");
        }
        protected override void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
            _listPartnerIsSubscribed = DnnDataContext.CSPTSMM_Partners.Where(a =>a.PortalId == PortalId && a.SocialId == (int) SocialLib.Common.Socialtype.LinkedIn && a.IsSubscribed).Select(a => a.CspId);
            _listPartnerExpiring = DnnDataContext.CSPTSMM_PartnersSubscriptionInfos.Where(a => a.SocialId == (int)SocialLib.Common.Socialtype.LinkedIn && a.PortalId == PortalId && _listPartnerIsSubscribed.Contains(a.PartnerId) && a.NextRenewDate.Value.AddDays(-ExpirationDay) <= DateTime.Now).ToList();

        }
        public DataTable GetDataTable()
        {
            var dt = new DataTable();
            dt.Columns.AddRange(new[] {new DataColumn("PartnerId"),new DataColumn("CompanyName"),new DataColumn("LastRenewDate"), new DataColumn("NextRenewDate"),new DataColumn("Social"),new DataColumn("EmailRenewSentAt"), });
            var firstRow = dt.NewRow();
            foreach (var column in dt.Columns)
            {
                firstRow[dt.Columns.IndexOf(column.ToString())] = column.ToString();
            }
            var listPartner = DnnDataContext.CSPTSMM_PartnersSubscriptionInfos.Where(a => a.SocialId == (int)SocialLib.Common.Socialtype.LinkedIn && a.PortalId == PortalId && _listPartnerIsSubscribed.Contains(a.PartnerId)).OrderBy(a=>a.NextRenewDate);
            foreach (var partner in listPartner)
            {
                var socialId = partner.SocialId;
                var socialType = string.Empty;
                switch (socialId)
                {
                    case 1:
                        socialType = "Twitter";
                        break;
                    case 2:
                        socialType = "LinkedIn";
                        break;
                    case 3:
                        socialType = "Facebook";
                        break;
                }
                if (partner.NextRenewDate != null)
                {
                    var partnerDate = new DateTime(partner.NextRenewDate.Value.Year, partner.NextRenewDate.Value.Month,
                                                   partner.NextRenewDate.Value.Day);
                    var today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    var expiredDays = (partnerDate - today).Days;
                    var expired = expiredDays.ToString();
                    if (expiredDays<-1)
                    {
                        var day = (-expiredDays).ToString();
                        expired = string.Format("Expired {0} days ", day);
                    }
                    else if(expiredDays==-1)
                    {
                        expired = string.Format("Expired 1 day ");
                    }
                    else if (expiredDays == 0)
                    {
                        expired = string.Format("Expired in today");
                    }

                    var company = CspDataContext.companies.First(a => a.companies_Id == partner.PartnerId);
                    var row = dt.NewRow();
                    row[0] = partner.PartnerId;
                    row[1] = company.companyname;
                    row[2] = partner.LastRenewDate;
                    row[3] = expired;
                    row[4] = socialType;
                    row[5] = partner.EmailRenewSentAt;
                    dt.Rows.Add(row);
                }
            }
            return dt;
        }

        private void GetListPartnerCheckState()
        {
             if (ViewState[Cons.KEY_PARTNER_CHECK_STATE] == null)
                 _listPartnerCheckState = new Hashtable();
             else
                 _listPartnerCheckState = (Hashtable)ViewState[Cons.KEY_PARTNER_CHECK_STATE];
        }
        private void UpdateListPartnerCheckState()
        {
            ViewState[Cons.KEY_PARTNER_CHECK_STATE] = _listPartnerCheckState;
        }

        public string GetUrlPartnerOldView()
        {
            return Globals.NavigateURL(TabId, "PartnerOldView", "mid", ModuleId.ToString());  
        }

        public string GetUrlCurrent()
        {
            return Globals.NavigateURL(TabId);
        }

        protected void RadGridPartner_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            RadGridPartner.DataSource = GetDataTable();
        }

        protected void CheckChanged(Object sender, System.EventArgs e)
        {
            GetListPartnerCheckState();
            var box = (CheckBox) sender;
            var item = (GridDataItem) box.NamingContainer;
            _listPartnerCheckState[int.Parse(item["PartnerId"].Text)] = box.Checked;

           UpdateListPartnerCheckState();
        }
      
        protected void RadGridPartner_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            GetListPartnerCheckState();
            if (e.Item is GridDataItem)
            {
                GridDataItem item = e.Item as GridDataItem;
                CheckBox box = (CheckBox)item.FindControl("CheckBox1");
                var id = int.Parse(item.GetDataKeyValue("PartnerId").ToString());
                if (_listPartnerExpiring.Any(a=>a.PartnerId == id))
                {
                    item.CssClass = "highlight";
                }
                if (_listPartnerCheckState[id] != null)
                    box.Checked = (bool)_listPartnerCheckState[id];
                else
                {
                    box.Checked = false;
                }
            }
        }

        protected void btnCheck_OnClick(object sender, EventArgs e)
        {
            GetListPartnerCheckState();
            //var listPartnerChecked = DnnDataContext.CSPTSMM_PartnersSubscriptionInfos.Where(a => a.SocialId == (int)SocialLib.Common.Socialtype.LinkedIn && a.PortalId == PortalId);
            foreach (var partner in _listPartnerExpiring)
            {
                _listPartnerCheckState[partner.PartnerId] = true;
            }
            UpdateListPartnerCheckState();
            RadGridPartner.Rebind();
        }

        protected void btnUncheck_OnClick(object sender, EventArgs e)
        {
            _listPartnerCheckState = new Hashtable();
            UpdateListPartnerCheckState();
            RadGridPartner.Rebind();
        }

        protected void btnSendEmails_OnClick(object sender, EventArgs e)
        {
            try
            {
                GetListPartnerCheckState();
                if (_listPartnerCheckState.ContainsValue(true))
                {
                    _listPartnerSendCompleted = new List<int>();
                    var companyParameterTypeEmail = CspDataContext.companies_parameter_types.FirstOrDefault(a => a.parametername == "Personalize_Contact_Emailaddress");
                    if (companyParameterTypeEmail != null)
                    {
                        _companyParameterTypeEmailId = companyParameterTypeEmail.companies_parameter_types_Id;
                        var count = 0;
                        var smtpProblem = false;
                        foreach (DictionaryEntry partner in _listPartnerCheckState)
                        {
                            if ((bool)partner.Value)
                            {
                                var result = SendEmailToPartner(int.Parse(partner.Key.ToString()));
                                if (!string.IsNullOrEmpty(result) && count == 0 &&
                                    result.IndexOf("There is a problem with the configuration of your SMTP Server",
                                                   StringComparison.InvariantCultureIgnoreCase) > -1)
                                {
                                    smtpProblem = true;
                                    PushAlertMessage(result);
                                    break;
                                }
                                    

                            }
                            count++;
                        }
                        if (!smtpProblem)
                        {
                            var message = GetLocalizedText("Msg.SendEmailCompleted").Replace("{count}", _listPartnerSendCompleted.Count.ToString());
                            PushAlertMessage(message);
                        }
                       
                    }
                    else
                    {
                        PushAlertMessage(GetLocalizedText("Msg.MissingCompanyParameter"));
                    }
                }
                else
                {
                    PushAlertMessage(GetLocalizedText("Msg.NoPartnerSeleted"));
                }
            }
            catch (Exception ex)
            {
                PushAlertMessage(ex.Message);
            }
           
        }

        private string SendEmailToPartner(int partnerId)
        {
            var companyEmailParameter = CspDataContext.companies_parameters.FirstOrDefault(a =>a.companies_Id == partnerId && a.companies_parameter_types_Id == _companyParameterTypeEmailId);
            var userInfo = GetUserInfoByCspId(partnerId);
            if (companyEmailParameter != null && userInfo!=null)
            {
                var companyEmail = companyEmailParameter.value_text;
                if (!string.IsNullOrEmpty(companyEmail))
                {
                    var tokenReplace = new DotNetNuke.Services.Tokens.TokenReplace();
                    var paramToEncrypt = DnnModules.Common.CryptoServiceProvider.Encode(string.Format("action=renew&cspid={0}&time={1}", partnerId, DateTime.Now.Ticks));
                    var renewTokenUrl = string.Format("{0}?key={1}", RenewTokenUrl, HttpUtility.UrlEncode(paramToEncrypt));
                    var emailContent =
                        tokenReplace.ReplaceEnvironmentTokens(GetLocalizedText("Email.Content"))
                            .Replace("[Link]", renewTokenUrl)
                            .Replace("[DisplayName]",userInfo.DisplayName);
                        

                    var result = Mail.SendMail(PortalSettings.Email, companyEmail, "", GetLocalizedText("Email.Title"), emailContent, "", "HTML", "", "", "", "");
                    if (string.IsNullOrEmpty(result))
                    {
                        var partnerRecord = DnnDataContext.CSPTSMM_PartnersSubscriptionInfos.FirstOrDefault(a => a.PartnerId == partnerId && a.PortalId == PortalId && a.SocialId == (int)SocialLib.Common.Socialtype.LinkedIn && _listPartnerIsSubscribed.Contains(a.PartnerId));
                        if (partnerRecord != null)
                        {
                            partnerRecord.EmailRenewSentAt = DateTime.Now;
                            DnnDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                            _listPartnerSendCompleted.Add(partnerId);
                        }
                       
                    }
                    return result;
                }
            }
            return string.Empty;
        }
        private void PushAlertMessage(string message)
        {
            Page.ClientScript.RegisterStartupScript(GetType(), "alert", "<script type='text/javascript'>window.onload = function(){PushAlert('" + message + "')};</script>");
        }
    }
}