﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PartnerOldView.ascx.cs" Inherits="CR.DnnModules.SocialSendEmails.PartnerOldView" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server">  
</telerik:RadWindowManager> 
<div class="containerModule">
<div class="headerModule oldview-header">
    <div class="title left"><%=GetLocalizedText("Label.SocialSendEmail") %></div>
    <div class ="right header-right">
        <span class="inactive"><a href="<%=GetUrlMainView()%>"><%=GetLocalizedText("Label.PartnerExpired") %></a></span>
        <span class="header-link oldview-link"><%=GetLocalizedText("Label.PartnerOld") %></span>
    </div>
    <div class="clear"></div>
</div>
<div class="border-top"></div>
<div class="btnContainer">
<asp:Button ID="btnUncheck" runat="server" CssClass="left" OnClick="btnUncheck_OnClick"/>
<asp:Button ID="btnCheckAll" runat="server" OnClick="btnCheck_OnClick" class="left button"/>
<asp:Button ID="btnSendEmails" OnClick="btnSendEmails_OnClick" CssClass="right" runat="server"/>
</div>
<div class="clear"></div>
<telerik:RadGrid ID="RadGridPartner" CssClass="rgPartnerModule" runat="server" AllowPaging="True" OnItemDataBound="RadGridPartner_OnItemDataBound" OnNeedDataSource="RadGridPartner_OnNeedDataSource" PageSize="20">
        <MasterTableView DataMember="Customers" AutoGenerateColumns="False" DataKeyNames="PartnerId">
            <Columns>
                 <telerik:GridTemplateColumn UniqueName="CheckTemp" > 
                        <ItemTemplate> 
                            <asp:CheckBox ID="CheckBox1" runat="server" OnCheckedChanged="CheckChanged" AutoPostBack="True"/>             
                        </ItemTemplate> 
                </telerik:GridTemplateColumn> 
                <telerik:GridBoundColumn DataField="PartnerId" UniqueName="PartnerId" HeaderText="Partner Id" />
                <telerik:GridBoundColumn DataField="CompanyName" HeaderText="Company Name" />
                <telerik:GridBoundColumn DataField="Social" HeaderText="SocialId"/>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
</div>
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function PushAlert(message) {
            setTimeout(function () {
                window.radalert(message, 330, 210, "<%=GetLocalizedText("Msg.Info") %>");
            }, 0);
        }

</script>
</telerik:RadScriptBlock>