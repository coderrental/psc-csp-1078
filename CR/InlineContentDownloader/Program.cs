﻿using System;
using TIEKinetix.SystemOnDemandEngine.Common.Utilities;
using System.Collections.Generic;
using System.IO;
using log4net;
using log4net.Config;

namespace TheFirstProjectWithCRTeam
{
    class Program
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(Program));
        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();

            Arguments arg = new Arguments(args);
            string dirPath = arg["directoryPath"];
            string dest = arg["destPath"];
            string zipFileName;

            Functions f = new Functions();

            // validation
            if (string.IsNullOrEmpty(dirPath.ToString()) || string.IsNullOrEmpty(dest))
            {
                log.Info(".File Path or Destination Path are null!");
                Console.WriteLine("File Path or Destination Path are null!");
                return;
            }
            // check mode ftp
            if (arg["ftpMode"] == "true")
            {
                dirPath = f.DownloadFiles();
                zipFileName = f.DownloadAndArchiveHTML(dirPath, dest);

                //f.DelListDi(new List<DirectoryInfo>());
                if (!zipFileName.Equals("null"))
                {
                    f.Upload(zipFileName);
                }

            }
            else
                zipFileName = f.DownloadAndArchiveHTML(dirPath, dest);
            log.Info("Completed !");
            log.Info(zipFileName);
            Console.WriteLine("Completed !\n" + zipFileName);
            Console.Read();
            
        }
        
    }
    
}
