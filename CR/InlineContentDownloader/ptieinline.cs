﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TheFirstProjectWithCRTeam
{
    class ptieinline
    {
        string mfrname;
        string mfrsku;
        string number;

        public string Mfrname
        {
            get { return mfrname; }
            set { mfrname = value; }
        }
        public string Mfrsku
        {
            get { return mfrsku; }
            set { mfrsku = value; }
        }
        public string Number
        {
            get { return number; }
            set { number = value; }
        }

        public ptieinline() 
        { 
        }

        public ptieinline(string name, string sku, string num)
        {
            mfrname = name;
            mfrsku = sku;
            number = num;
        }
    }

}
