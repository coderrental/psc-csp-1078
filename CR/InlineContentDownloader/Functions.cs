﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;
using System.Configuration;
using System.Net;
using System.IO;
using Ionic.Zip;
using log4net;

namespace TheFirstProjectWithCRTeam
{
    class Functions
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(Functions));
        private string connectionString;
        private List<string> sheetsList = new List<string>();
        private string formatName = ConfigurationManager.AppSettings["formatName"];
        private string url = ConfigurationManager.AppSettings["URL"];
        private string ftphost = ConfigurationManager.AppSettings["FtpAddress"];
        private string user = ConfigurationManager.AppSettings["UserName"];
        private string pass = ConfigurationManager.AppSettings["Password"];
        private DirectoryInfo TempDirInfo;

        #region excel

        private void OpenXLFile(string fileName)
        {
            log4net.Config.XmlConfigurator.Configure();
            log.Info("Open, read file Excel: " + fileName);
            
            //open the excel file using OLEDB
            string[] splitByDots = fileName.Split(new char[1] { '.' });
            string[] workSheetNames = new string[] { };

            if (splitByDots[splitByDots.Length - 1] == "xlsx")
                //read a 2007 file
                connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";
            else
                //read a 97-2003 file
                connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName + ";Extended Properties=Excel 8.0;";

            using (var con = new OleDbConnection(connectionString)) {
                try
                {
                    con.Open();
                    //get all the available sheets
                    using (var dataSet = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null))
                    {
                        //get the number of sheets in the file
                        workSheetNames = new String[dataSet.Rows.Count];
                        int i = 0;
                        foreach (DataRow row in dataSet.Rows)
                        {
                            //insert the sheet's name in the current element of the array
                            //and remove the $ sign at the end
                            workSheetNames[i] = row["TABLE_NAME"].ToString().Trim(new[] { '$' });
                            sheetsList.Add(row["TABLE_NAME"].ToString().Trim(new[] { '$' }));
                            i++;
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Debug("Can not open connection !");
                    log.Debug(ex);
                }
            }
        }

        private DataTable GetWorksheet(string worksheetName)
        {
            log4net.Config.XmlConfigurator.Configure();
            log.Info("Get Data from file Excel");
            //log.Debug("log.Debug");
            try
            {
                OleDbConnection con = new System.Data.OleDb.OleDbConnection(connectionString);
                OleDbDataAdapter cmd = new System.Data.OleDb.OleDbDataAdapter("select * from [" + worksheetName + "$]", con);

                con.Open();
                System.Data.DataSet excelDataSet = new DataSet();
                cmd.Fill(excelDataSet);
                con.Close();

                return excelDataSet.Tables[0];
            }
            catch (Exception ex)
            {
                log.Debug("Can not open connection !");
                log.Debug(ex);
            }
            return null;
        }

        #endregion

        #region URL

        private string ReplaceURL(string url, string name, string sku)
        {
            log4net.Config.XmlConfigurator.Configure();
            url = url.Replace("[MfrName]", name);
            url = url.Replace("[MfrPN]", sku);
            url = url.Replace("[MID]", DateTime.Now.Ticks.ToString());
            log.Info("After replace: " + url);
            return url;
        }

        private string SetHtmlName(string HtmlName, string name, string sku)
        {
            log4net.Config.XmlConfigurator.Configure();
            HtmlName = HtmlName.Replace("{MfrName}", name);
            HtmlName = HtmlName.Replace("{MfrPN}", sku);
            HtmlName = HtmlName.Replace("{Time}", DateTime.Now.Ticks.ToString());
            log.Info("Set HtmlName: " + HtmlName);
            return HtmlName;
        }

        private string GetHtmlFromUrl(string url)
        {
            log4net.Config.XmlConfigurator.Configure();
            log.Info("Get Html from Uri: " + url);
            try
            {
                string html = "";
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                // get the response stream.
                //Stream responseStream = response.GetResponseStream();
                // use a stream reader that understands UTF8
                StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                html = reader.ReadToEnd();
                // close the reader
                reader.Close();
                response.Close();
                return html;//return content html
            }
            catch (Exception ex)
            {
                log.Debug("Can not request !");
                log.Debug(ex);
                return null;
            }
        }

        #endregion
        
        #region Save, Zip & Del

        public string DownloadAndArchiveHTML(string dirPath, string destPath)
        {
            log4net.Config.XmlConfigurator.Configure();
            log.Info("Download and achive HTML");

            List<FileInfo> ListFi = new List<FileInfo>();
            
            // check dirPath or filePath
            if (dirPath.EndsWith(@"\"))
            {
                DirectoryInfo dir = new DirectoryInfo(dirPath);
                ListFi = GetFilesXL(dir);
                if (ListFi.Count() == 0)
                {
                    log.Debug("There isn't any excel file in this directory");
                    Console.WriteLine("There isn't any excel file in this directory");
                    return "null";
                }
            }
            else 
                ListFi.Add(new FileInfo(dirPath));

            List<DirectoryInfo> ListDi = SaveToDirs(ListFi, destPath);
            return zipToFile(ListDi, destPath);
        }

        private List<FileInfo> GetFilesXL(DirectoryInfo dir)
        {
            log4net.Config.XmlConfigurator.Configure();
            List<FileInfo> FilesXL = new List<FileInfo>();
            try
            {
                foreach (FileInfo f in dir.GetFiles())
                {
                    if (f.Extension.Equals(".xls") || f.Extension.Equals(".xlsx"))
                    {
                        log.Info("Get Files EXCEL: " + f.Name);
                        FilesXL.Add(f);
                    }
                }
            }
            catch
            {
                log.Debug("Directory "+ dir.FullName + " could not be accessed!!!!");
                Console.WriteLine("Directory {0}  \n could not be accessed!!!!", dir.FullName);
            }
            return FilesXL;
        }

        private List<DirectoryInfo> SaveToDirs(List<FileInfo> listXLs, string destPath)
        {
            log4net.Config.XmlConfigurator.Configure();
            log.Info("Save to Directorys ");

            List<DirectoryInfo> ListDi = new List<DirectoryInfo>();
            foreach (FileInfo fi in listXLs)
            {
                ListDi.Add(SaveToDir(fi));
                log.Info("Save " + fi.Name);
            }
            return ListDi;
        }

        private DirectoryInfo SaveToDir(FileInfo fileXL)
        {
            log4net.Config.XmlConfigurator.Configure();
            log.Info("Save to Directory: " + fileXL.Name);

            string DirName = Path.GetFileNameWithoutExtension(fileXL.Name);
            DirectoryInfo di = Directory.CreateDirectory(DirName);
            OpenXLFile(fileXL.FullName);
            Console.WriteLine(fileXL.Name + ":");

            //Get data from sheet1
            DataTable dt = GetWorksheet(sheetsList[0]);
            SaveToFiles(url, dt, DirName);
            return di;
        }
        
        private void SaveToFiles(string url, DataTable dt, string DirName)
        {
            log4net.Config.XmlConfigurator.Configure();
            log.Info("Save to Files");

            var l = new List<ptieinline> { };
            int i = 0, j = 0;
            foreach (DataRow row in dt.Rows)
            {
                if (!string.IsNullOrEmpty(row[0].ToString()) && !string.IsNullOrEmpty(row[1].ToString()))
                {
                    ptieinline ptie = new ptieinline(row[0].ToString(), row[1].ToString(), DateTime.Now.Ticks.ToString());
                    l.Add(ptie);

                    string urlTmp = ReplaceURL(url, ptie.Mfrname, ptie.Mfrsku);

                    string htmlFileName = DirName + @"\" + ConfigurationManager.AppSettings["HTMLFileName"];
                    htmlFileName = SetHtmlName(htmlFileName, ptie.Mfrname, ptie.Mfrsku);
                    File.WriteAllText(htmlFileName, GetHtmlFromUrl(urlTmp));

                    i++;
                    // check null content
                    if (!File.ReadAllText(htmlFileName).Equals("//") && !File.ReadAllText(htmlFileName).Equals(""))
                    {
                        j++;
                        ChangeContentInHTML(htmlFileName);
                        Console.WriteLine("File {0} is downloaded!", i);
                        log.Info("File " + i + " is downloaded! (" + htmlFileName+ ")");
                    }
                    else
                    {
                        File.Delete(htmlFileName);
                        Console.WriteLine("File {0} null!", i);
                        log.Info("File " + i + " null! (" + htmlFileName + ")");
                    }
                }
            }
            Console.WriteLine("Save to " + j + " files !!!");
            log.Info("Directory "+ DirName + " have " + j + " files !!!");
        }

        public string zipToFile(List<DirectoryInfo> ListDi, string destPath)
        {
            log4net.Config.XmlConfigurator.Configure();
            log.Info("Zip file to " + destPath);

            string zipFileName = destPath + DateTime.Now.ToString(formatName) + ".zip";
            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    foreach (DirectoryInfo di in ListDi)
                        foreach (FileInfo fi in di.GetFiles())
                            zip.AddFile(di.Name + @"\" + fi.Name);
                    zip.Save(zipFileName);
                }
            }
            catch (Exception ex)
            {
                log.Debug("Can not zip file !");
                log.Debug(ex);
            }
            DelListDi(ListDi);
            return zipFileName;
        }

        private void ChangeContentInHTML(string HTMLFileName)
        {
            log4net.Config.XmlConfigurator.Configure();
            log.Info("Remove image tag into html file: (" + HTMLFileName +" )");

            HtmlAgilityPack.HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();

            // There are various options, set as needed
            htmlDoc.OptionFixNestedTags = true;

            // filePath is a path to a file containing the html
            htmlDoc.Load(HTMLFileName);

            // Use:  htmlDoc.LoadHtml(xmlString);  to load from a string (was htmlDoc.LoadXML(xmlString)

            // ParseErrors is an ArrayList containing any errors from the Load statement
            if (htmlDoc.ParseErrors != null && htmlDoc.ParseErrors.Any())
            {
                // Handle any parse errors as required

            }
            else
            {
                if (htmlDoc.DocumentNode != null)
                {
                    if (htmlDoc.DocumentNode.SelectNodes("//img").Any())
                    {
                        foreach (var item in htmlDoc.DocumentNode.SelectNodes("//img"))
                        {
                            item.Remove();
                        }
                        File.WriteAllText(HTMLFileName, htmlDoc.DocumentNode.WriteContentTo());
                    }
                }
            }
        }

        public void DelListDi(List<DirectoryInfo> ListDi)
        {
            log4net.Config.XmlConfigurator.Configure();
            log.Info("Delele temps file : ");

            foreach (DirectoryInfo di in ListDi)
            {
                try
                {
                    foreach (FileInfo fi in di.GetFiles())
                    {
                        fi.Delete();
                        log.Info("Delele temp file : "+ fi.Name );
                    }
                    di.Delete();
                    log.Info("Delele temp directory : " + di.Name);
                }
                catch (System.IO.IOException e)
                {
                    log.Debug(e);
                    Console.WriteLine(e.Message);
                }
            }
        }

        #endregion

        #region FTP

        public string DownloadFiles()
        {
            log4net.Config.XmlConfigurator.Configure();
            List<FileInfo> ListXL = new List<FileInfo>();
            TempDirInfo = Directory.CreateDirectory(@"C:\Windows\Temp\" + DateTime.Now.Ticks.ToString());
            string ftpDirPath = ftphost + ConfigurationManager.AppSettings["FtpPathIn"];
            string FtpFileName = ConfigurationManager.AppSettings["FtpFile"];

            log.Info("Download Files from FTP: " + ftpDirPath);
            if (FtpFileName.Equals(""))
            {
                ListXL = GetListFiXL(ftpDirPath);
            }
            else
            {
                ListXL.Add(new FileInfo(FtpFileName));
            } 
            foreach (FileInfo fi in ListXL)
            {
                Download(fi, TempDirInfo);
            }
            return TempDirInfo.FullName + "\\";
        }

        private List<FileInfo> GetListFiXL(string ftpDirPath)
        {
            log4net.Config.XmlConfigurator.Configure();
            log.Info("Get list file Excel from Fpt Sever: " + ftpDirPath);
            List<FileInfo> ListFiXL = new List<FileInfo>();

            try
            {
                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(ftpDirPath);
                ftpRequest.Credentials = new NetworkCredential(user, pass);
                ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;
                FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse();
                StreamReader streamReader = new StreamReader(response.GetResponseStream());

                List<string> directories = new List<string>();

                string line = streamReader.ReadLine();
                while (!string.IsNullOrEmpty(line))
                {
                    if (line.EndsWith(".xls") || line.EndsWith(".xlsx"))
                        ListFiXL.Add(new FileInfo(line));
                    line = streamReader.ReadLine();
                }

                streamReader.Close();
            }
            catch (Exception ex)
            {
                log.Debug("Can not get list file Excel from Fpt Sever");
                log.Debug(ex);
                Console.WriteLine(ex);
            }
            return ListFiXL;
        }

        private string Download(FileInfo fileXL, DirectoryInfo TempDirInfo)
        {
            log4net.Config.XmlConfigurator.Configure();
            string fileSave = TempDirInfo.FullName + "\\" + fileXL.Name;
            string ftpfilepath = ftphost + ConfigurationManager.AppSettings["FtpPathIn"] + ConfigurationManager.AppSettings["FtpFile"];
            
            // 
            if (ConfigurationManager.AppSettings["FtpFile"] == "")
                ftpfilepath+=fileXL.Name;
            try
            {
                log.Info("Download file excel: " + ftpfilepath);
                Console.WriteLine("Downloading excel file: " + ftpfilepath);
                WebClient request = new WebClient();
                request.Credentials = new NetworkCredential(user, pass);
                byte[] fileData = request.DownloadData(ftpfilepath);

                FileStream file = File.Create(fileSave);
                file.Write(fileData, 0, fileData.Length);
                file.Close();
                Console.WriteLine("Download Complete, File length: " + fileData.Length);
                log.Info("Download Complete, File length: " + fileData.Length);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Debug(e);
            }
            return fileSave;
        }

        public void Upload(string filename)
        {
            log4net.Config.XmlConfigurator.Configure();
            FileInfo fileInf = new FileInfo(filename);
            string uri = ftphost + ConfigurationManager.AppSettings["FtpPathOut"] + fileInf.Name;
            FtpWebRequest reqFTP;
            log.Info("Upload file to Fpt, Uri: " + uri);

            // Create FtpWebRequest object from the Uri provided
            reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(uri));

            // Provide the WebPermission Credintials
            reqFTP.Credentials = new NetworkCredential(user, pass);

            // By default KeepAlive is true, where the control connection
            // is not closed after a command is executed.
            reqFTP.KeepAlive = false;

            // Specify the command to be executed.
            reqFTP.Method = WebRequestMethods.Ftp.UploadFile;

            // Specify the data transfer type.
            reqFTP.UseBinary = true;

            // Notify the server about the size of the uploaded file
            reqFTP.ContentLength = fileInf.Length;

            // The buffer size is set to 2kb
            int buffLength = 2048;
            byte[] buff = new byte[buffLength];
            int contentLen;

            // Opens a file stream (System.IO.FileStream) to read the file
            // to be uploaded
            FileStream fs = fileInf.OpenRead();

            try
            {
                // Stream to which the file to be upload is written
                Stream strm = reqFTP.GetRequestStream();

                // Read from the file stream 2kb at a time
                contentLen = fs.Read(buff, 0, buffLength);

                // Till Stream content ends
                while (contentLen != 0)
                {
                    // Write Content from the file stream to the FTP Upload
                    // Stream
                    strm.Write(buff, 0, contentLen);
                    contentLen = fs.Read(buff, 0, buffLength);
                }

                // Close the file stream and the Request Stream
                strm.Close();
                fs.Close();
            }
            catch (Exception ex)
            {
                log.Debug("Can not request");
                log.Debug(ex);
                Console.WriteLine(ex);
            }
        }

        #endregion

        
    }

}
