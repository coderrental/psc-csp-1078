﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace TIEKinetix.SystemOnDemandEngine.Common.Utilities
{
    public class Arguments
    {
        #region Private Fields

        private readonly Dictionary<string, string> _parameters = new Dictionary<string, string>();

        #endregion

        #region Constructors

        public Arguments(string[] arguments)
        {
            Regex splitExpression = new Regex(@"^-{1,2}|^/|=|:", RegexOptions.IgnoreCase | RegexOptions.Compiled);
            Regex cleanUpExpression = new Regex(@"^['""]?(.*?)['""]?$", RegexOptions.IgnoreCase | RegexOptions.Compiled);

            string parameter = null;
            string[] blocks;

            // Valid parameters forms:
            // {-,/,--}param{ ,=,:}((",')value(",'))
            // Examples: -param1 value1 --param2 /param3:"Test-:-work" /param4=happy -param5 '--=nice=--'
            foreach (string argument in arguments)
            {
                blocks = splitExpression.Split(argument, 3);

                switch (blocks.Length)
                {
                    case 1:
                        if (parameter != null)
                        {
                            if (!_parameters.ContainsKey(parameter))
                            {
                                blocks[0] = cleanUpExpression.Replace(blocks[0], "$1");
                                _parameters.Add(parameter, blocks[0]);
                            }

                            parameter = null;
                        }

                        break;

                    case 2:

                        if (parameter != null)
                        {
                            if (!_parameters.ContainsKey(parameter))
                            {
                                _parameters.Add(parameter, "true");
                            }
                        }
                        parameter = blocks[1];

                        break;

                    case 3:

                        if (parameter != null)
                        {
                            if (!_parameters.ContainsKey(parameter))
                            {
                                _parameters.Add(parameter, "true");
                            }
                        }

                        parameter = blocks[1];
                        if (!_parameters.ContainsKey(parameter))
                        {
                            blocks[2] = cleanUpExpression.Replace(blocks[2], "$1");
                            _parameters.Add(parameter, blocks[2]);
                        }
                        parameter = null;

                        break;
                }
            }

            if (parameter != null)
            {
                if (!_parameters.ContainsKey(parameter))
                {
                    _parameters.Add(parameter, "true");
                }
            }
        }

        #endregion

        #region Indexer

        public string this[string parameter]
        {
            get
            {
                string value = null;
                try
                {
                    value = _parameters[parameter];
                }
                catch { }

                return value;
            }
        }

        #endregion
    }
}