﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CR.DnnModules.SecuredPasswordReset.Common
{
	public class Cons
	{
		public const string MODULE_NAME = "PasswordReset";
		public const string SETTING_ADMIN_EMAIL = "SettingAdminEmail";
		public const string SETTING_EMAIL_TITLE_RESET_PASSWORD = "SettingEmailTitleResetPassword";
		public const string SETTING_EMAIL_TITLE_UPDATE_PASSWORD = "SettingEmailTitleUpdatePassword";
		public const string SETTING_REQUEST_EXPIRED_HOUR = "SettingRequestExpiredHour";

		public const int DEFAULT_REQUEST_EXPIRED_HOUR = 48;
	}
}