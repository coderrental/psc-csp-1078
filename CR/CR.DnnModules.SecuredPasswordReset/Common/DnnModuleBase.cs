﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CR.DnnModules.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Localization;
using CR.DnnModules.SecuredPasswordReset.DataContext;

namespace CR.DnnModules.SecuredPasswordReset.Common
{
	public class DnnModuleBase : PortalModuleBase
	{
		private string _localResourceFile;
		public Dnn0602DataContext DnnDataContext;

		protected virtual void Page_Init(object sender, EventArgs e)
		{
			//Init Local Resource file
			_localResourceFile = Utils.GetCommonResourceFile(Cons.MODULE_NAME, LocalResourceFile);

			DnnDataContext = new Dnn0602DataContext(Config.GetConnectionString());
		}

		/// <summary>
		/// Get localized text for the current portal language
		/// </summary>
		/// <param name="key">Resource Key</param>
		/// <returns>Localized text</returns>
		public string GetLocalizedText(string key)
		{
			return Localization.GetString(key, _localResourceFile);
		}
	}
}