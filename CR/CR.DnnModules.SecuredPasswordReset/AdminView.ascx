﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminView.ascx.cs" Inherits="CR.DnnModules.SecuredPasswordReset.AdminView" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div id="tabs">
	<ul>
		<li><a href="#tabs-1"><%= GetLocalizedText("Tab.RequestList") %></a></li>
		<li><a href="#tabs-2"><%= GetLocalizedText("Tab.Configuration") %></a></li>
	</ul>
	<div id="tabs-1">
		<telerik:RadGrid runat="server" ID="RequestList" AllowPaging="True" PageSize="20" EnableHeaderContextMenu="true" OnPreRender="Radgrid_Prerender" AutoGenerateColumns="False" AllowSorting="True">
			<MasterTableView>
				<Columns>
					<%--<telerik:GridBoundColumn DataField="Id" UniqueName="Id" HeaderText="ID">
					</telerik:GridBoundColumn>--%>
					<telerik:GridBoundColumn DataField="UserName" DataType="System.String" HeaderText="User Name"
						SortExpression="UserName" UniqueName="UserName">
					</telerik:GridBoundColumn>
					<telerik:GridBoundColumn UniqueName="UserEmail" HeaderText="Email">
					</telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="RequestedDate" DataType="System.String" HeaderText="Requested Date"
						SortExpression="RequestedDate" UniqueName="RequestedDate">
					</telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="ResetDate" DataType="System.String" HeaderText="Reset Date"
						SortExpression="ResetDate" UniqueName="ResetDate">
					</telerik:GridBoundColumn>
				</Columns>
			</MasterTableView>
		</telerik:RadGrid>
	</div>
	<div id="tabs-2">
		<div id="config-wrapper">
			<div class="config-row" id="config-man-msg">
				
			</div>
			<div class="config-row">
				<div class="row-label">Admin Email:</div>
				<div class="row-control"><input type="text" runat="server" id="TxtAdminEmail" ClientIDMode="Static" /></div>
			</div>
			<div class="config-row">
				<div class="row-label">Expiry of Reset Link (hour):</div>
				<div class="row-control"><input type="text" runat="server" id="TxtExpiryResetLink" ClientIDMode="Static" /></div>
			</div>
			<div class="config-row">
				<div class="row-label">Password Reset Email Title:</div>
				<div class="row-control"><input type="text" runat="server" id="TxtPasswordResetEmailTitle" ClientIDMode="Static" /></div>
			</div>
			<div class="config-row">
				<div class="row-label">Password Changed Email Title:</div>
				<div class="row-control"><input type="text" runat="server" id="TxtPasswordChangedEmailTitle" ClientIDMode="Static" /></div>
			</div>
			<div class="config-row" style="text-align: center">
				<input type="button" id="btn-update-config" value="Update"/>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$("#tabs").tabs();

		$('#btn-update-config').click(function() {
			var adminEmail = $('#TxtAdminEmail').val();
			var expiryResetLink = $('#TxtExpiryResetLink').val();
			var resetEmailTitle = $('#TxtPasswordResetEmailTitle').val();
			var updateEmailTitle = $('#TxtPasswordChangedEmailTitle').val();
			
			if (expiryResetLink != '' && isNaN(expiryResetLink)) {
				alert('<%= GetLocalizedText("Msg.NumberRequired") %>');
				return;
			}
			
			if (adminEmail != '') {
				var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				if (!regex.test(adminEmail)) {
					alert('<%= GetLocalizedText("Msg.InvalidEmailAddress") %>');
					return;
				}
			}
			$('#btn-update-config').prop('disabled', true);
			$.ajax({
				type: 'POST',
				url: document.URL,
				dataType: 'json',
				data: { adminemail: adminEmail, expiryofrequest: expiryResetLink, resetemailtitle: resetEmailTitle, updateemailtitle: updateEmailTitle },
				beforeSend: function (xhr) {
					xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE");
				},
				success: function (responseData) {
					var rCode = parseInt(responseData.resultcode);
					if (rCode == 1) {
						$('#config-man-msg').text('<%= GetLocalizedText("Msg.ConfigurationsUpdated") %>');
						setTimeout(function() { $('#config-man-msg').text(''); }, 3000);
					}
					$('#btn-update-config').prop('disabled', false);
				},
				error: function () {
					alert('<%= GetLocalizedText("Msg.SubmitFailed") %>');
				}
			});
		});
	});
</script>