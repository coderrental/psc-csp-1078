﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.DnnModules.SecuredPasswordReset.Common;
using CR.DnnModules.SecuredPasswordReset.DataContext;
using DotNetNuke.Common;
using DotNetNuke.Entities.Users;
using DotNetNuke.Services.Mail;

namespace CR.DnnModules.SecuredPasswordReset
{
	public partial class ResetPassword : DnnModuleBase
	{
		protected CSPPasswordReset_User _resetRequest;
		public string isValid = "0";

		protected override void Page_Init(object sender, EventArgs e)
		{
			base.Page_Init(sender, e);
			if (Request.Headers["X-OFFICIAL-REQUEST"] == "TRUE")
			{
				Response.Clear();

				if (!String.IsNullOrEmpty(Request.Params["newpassword"]))
					DoResetPassword();

				Response.Flush();
				Response.End();
			}
			ValidateToken();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RegisterHeaderResources();
		}

		private void RegisterHeaderResources()
		{
			Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel='stylesheet' href='{0}css/Style.css' type='text/css' />", ControlPath)));
		}

		/// <summary>
		/// Validates the token.
		/// </summary>
		/// <param name="targetToken">The target token.</param>
		private void ValidateToken(string targetToken = null)
		{
			if (targetToken == null && String.IsNullOrEmpty(Request.Params["resettoken"]))
			{
				Response.Redirect(Globals.NavigateURL("Login"), true);
				return;
			}
			string tokenValue = targetToken != null ? targetToken : Request.Params["resettoken"];
			Guid requestToken;// = Guid.Parse(Request.Params["resettoken"].ToString());
			if (!Guid.TryParse(tokenValue, out requestToken))
				return;
			_resetRequest = DnnDataContext.CSPPasswordReset_Users.FirstOrDefault(a => a.Id == requestToken && a.PortalId == PortalId && a.ResetDate == null);
			if (_resetRequest == null)
			{
				return;
			}

			//Check if token has expired or not
			DateTime timeNow = DateTime.Now;
			DateTime requestedTime = _resetRequest.RequestedDate;
			TimeSpan diff = timeNow - requestedTime;
			int limitHour = Settings[Cons.SETTING_REQUEST_EXPIRED_HOUR] == null ||
			                String.IsNullOrEmpty(Settings[Cons.SETTING_REQUEST_EXPIRED_HOUR].ToString())
				                ? Cons.DEFAULT_REQUEST_EXPIRED_HOUR
				                : Convert.ToInt32(Settings[Cons.SETTING_REQUEST_EXPIRED_HOUR].ToString());
			if (diff.TotalHours > limitHour)
				return;

			isValid = "1";
		}

		/// <summary>
		/// Does the reset password.
		/// </summary>
		private void DoResetPassword()
		{
			Response.ContentType = "application/json";
			if (!String.IsNullOrEmpty(Request.Params["requesttoken"]) && !String.IsNullOrEmpty(Request.Params["newpassword"]) &&
			    !String.IsNullOrEmpty(Request.Params["confirmpassword"]))
			{
				ValidateToken(Request.Params["requesttoken"]);
				if (_resetRequest != null)
				{
					UserInfo userInfo = UserController.GetUserByName(PortalId, _resetRequest.UserName);
					if (userInfo == null)
					{
						isValid = "0";
					}
					else
					{
						//Update new password
						string oldPassword = Membership.Provider.GetPassword(_resetRequest.UserName, String.Empty);
						Membership.Provider.ChangePassword(_resetRequest.UserName, oldPassword, Request.Params["newpassword"]);

						//Mark request as done
						_resetRequest.ResetDate = DateTime.Now;
						DnnDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);

						string adminEmail = Settings[Cons.SETTING_ADMIN_EMAIL] == null || String.IsNullOrEmpty(Settings[Cons.SETTING_ADMIN_EMAIL].ToString())
									? PortalSettings.Email
									: Settings[Cons.SETTING_ADMIN_EMAIL].ToString();
						string emailTitle = Settings[Cons.SETTING_EMAIL_TITLE_UPDATE_PASSWORD] == null || String.IsNullOrEmpty(Settings[Cons.SETTING_EMAIL_TITLE_UPDATE_PASSWORD].ToString())
												? GetLocalizedText("DefaultSetting.EmailTitleUpdatePasswordSucceed")
												: Settings[Cons.SETTING_EMAIL_TITLE_UPDATE_PASSWORD].ToString();
						string emailTemplate = GetLocalizedText("Template.UpdatePasswordSucceedEmail");
						string fullName = userInfo.FirstName + " " + userInfo.LastName;
						fullName = fullName.Trim();
						string loginLink = Globals.NavigateURL("Login");

						if (String.IsNullOrEmpty(fullName)) fullName = userInfo.Username;

						emailTemplate = emailTemplate.Replace("[fullname]", fullName);
						emailTemplate = emailTemplate.Replace("[loginlink]", loginLink);
						emailTemplate = emailTemplate.Replace("[portalname]", PortalSettings.PortalName);

						string firstMailResult = Mail.SendMail(adminEmail, userInfo.Email, "", emailTitle, emailTemplate, "", "HTML", "", "", "", "");
					}
				}
			}
			Response.Write("{\"resultcode\": \"" + isValid + "\"}");
		}
	}
}