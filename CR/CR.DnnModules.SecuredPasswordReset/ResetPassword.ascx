﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResetPassword.ascx.cs" Inherits="CR.DnnModules.SecuredPasswordReset.ResetPassword" %>
<%@ Import Namespace="DotNetNuke.Common" %>
<div id="forgot-pwd-section">
	<div id="forgot-pwd-section-label">
		<h2><%= GetLocalizedText("Label.ResetPassword") %></h2>
	</div>
	<div id="forgot-pwd-section-form" class="reset-form">
		<div id="newpwd-label"><%= GetLocalizedText("Label.NewPassword") %></div>
		<div id="newpwd-input"><input type="password" id="fps-newpasswod"/></div>
		<div id="confirmpwd-label"><%= GetLocalizedText("Label.ConfirmPassword") %></div>
		<div id="confirmpwd-input"><input type="password" id="fps-confirmpassword"/></div>
	</div>
	<div id="forgot-pwd-section-footer">
		<input type="button" value="Submit" id="submit-fps"/>
	</div>
</div>
<script type="text/javascript">
	var isValid = '<%= isValid %>';
	var requestToken = '<%= _resetRequest != null ? _resetRequest.Id : Guid.Empty %>';
	$(function () {
		$('#submit-fps').click(function(e) {
			SubmitNewPassword(e);
		});
		
		$('#fps-newpassword, #fps-confirmpassword').keypress(function (e) {
			if (e.which == 13) {
				SubmitNewPassword(e);
				$(this).blur();
			}
		});
		
		if (isValid == '0') {
			$('#forgot-pwd-section-form').children('div:not(#newpwd-label)').remove();
			$('#newpwd-label').text('<%= GetLocalizedText("Msg.InvalidOrExpiredRequest") %>');
			$('#submit-fps').val('<%= GetLocalizedText("Btn.Close") %>');
			$('#submit-fps').unbind('click').click(function() {
				window.location = '<%= DotNetNuke.Common.Globals.NavigateURL("Login") %>';
			});
		}

		setTimeout(function() {
			$('#forgot-pwd-section-form').fadeIn();
		}, 1000);
	});
	
	function SubmitNewPassword(e) {
		e.preventDefault();
		var newPwd = $('#fps-newpasswod').val().trim();
		var confirmPwd = $('#fps-confirmpassword').val().trim();
		var validPassword = true;
		if (newPwd != confirmPwd)
			validPassword = false;
		else if (newPwd.length < 7)
			validPassword = false;
		
		if (!validPassword) {
			alert('<%= GetLocalizedText("Msg.InvalidPassword") %>');
			return;
		}
		
		$.ajax({
			type: 'POST',
			url: document.URL,
			dataType: 'json',
			data: { newpassword: newPwd, confirmpassword: confirmPwd, requesttoken: requestToken },
			beforeSend: function (xhr) {
				xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE");
			},
			success: function (responseData) {
				var rCode = parseInt(responseData.resultcode);
				switch (rCode) {
					case 1:
						var loginLink = '<%= DotNetNuke.Common.Globals.NavigateURL("Login") %>';
						var passwordMsg = '<%= GetLocalizedText("Msg.PasswordChanged") %>';
						passwordMsg = passwordMsg.replace('[loginlink]', loginLink);
						$('#forgot-pwd-section-form').children('div:not(#newpwd-label)').remove();
						$('#newpwd-label').html(passwordMsg);
						$('#submit-fps').val('<%= GetLocalizedText("Btn.GoToLogin") %>');
						$('#submit-fps').unbind('click').click(function () {
							window.location = loginLink;
						});
						break;
					default:
						alert('<%= GetLocalizedText("Msg.PasswordChangeFailed") %>');
						break;
				}
			},
			error: function () {
				alert('<%= GetLocalizedText("Msg.SubmitFailed") %>');
			}
		});
	}
</script>