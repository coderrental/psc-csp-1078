﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.DnnModules.SecuredPasswordReset.Common;
using CR.DnnModules.SecuredPasswordReset.DataContext;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Users;
using Telerik.Web.UI;

namespace CR.DnnModules.SecuredPasswordReset
{
	public partial class AdminView : DnnModuleBase
	{
		public int UpdatedFooter = 0;

		protected override void Page_Init(object sender, EventArgs e)
		{
			base.Page_Init(sender, e);
			if (Request.Headers["X-OFFICIAL-REQUEST"] == "TRUE")
			{
				Response.Clear();

				if (Request.Params["adminemail"] != null)
					UpdateModuleConfig();

				Response.Flush();
				Response.End();
				return;
			}
			InitSetup();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (UserInfo == null || !UserInfo.IsInRole(PortalSettings.AdministratorRoleName))
				Response.Redirect(DotNetNuke.Common.Globals.NavigateURL("Login"), true);
			RegisterHeaderResources();
			GetRequestList();
			LoadModuleConfig();
		}

		private void RegisterHeaderResources()
		{
			Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel='stylesheet' href='{0}css/jquery-ui.min.css' type='text/css' />", ControlPath)));
			Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel='stylesheet' href='{0}css/Style.css' type='text/css' />", ControlPath)));
			//Page.ClientScript.RegisterClientScriptInclude("jquerymin", ControlPath + "js/jquery-1.10.2.min.js");
			Page.ClientScript.RegisterClientScriptInclude("jqueryuimin", ControlPath + "js/jquery-ui.min.js");
		}

		/// <summary>
		/// Loads the module config.
		/// </summary>
		private void LoadModuleConfig()
		{
			if (Settings.ContainsKey(Cons.SETTING_ADMIN_EMAIL))
				TxtAdminEmail.Value = Settings[Cons.SETTING_ADMIN_EMAIL].ToString();
			if (Settings.ContainsKey(Cons.SETTING_REQUEST_EXPIRED_HOUR))
				TxtExpiryResetLink.Value = Settings[Cons.SETTING_REQUEST_EXPIRED_HOUR].ToString();
			if (Settings.ContainsKey(Cons.SETTING_EMAIL_TITLE_RESET_PASSWORD))
				TxtPasswordResetEmailTitle.Value = Settings[Cons.SETTING_EMAIL_TITLE_RESET_PASSWORD].ToString();
			if (Settings.ContainsKey(Cons.SETTING_EMAIL_TITLE_UPDATE_PASSWORD))
				TxtPasswordChangedEmailTitle.Value = Settings[Cons.SETTING_EMAIL_TITLE_UPDATE_PASSWORD].ToString();
		}

		/// <summary>
		/// Updates the module config.
		/// </summary>
		protected void UpdateModuleConfig()
		{
			var moduleController = new ModuleController();
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_ADMIN_EMAIL, Request.Params["adminemail"]);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_REQUEST_EXPIRED_HOUR, Request.Params["expiryofrequest"]);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_EMAIL_TITLE_RESET_PASSWORD, Request.Params["resetemailtitle"]);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_EMAIL_TITLE_UPDATE_PASSWORD, Request.Params["updateemailtitle"]);
			ModuleController.SynchronizeModule(ModuleId);

			Response.ContentType = "application/json";
			Response.Write("{\"resultcode\": \"1\"}");
		}

		/// <summary>
		/// Inits the setup.
		/// </summary>
		private void InitSetup()
		{
			//Create module table in DNN DB if not exist
			CheckModuleTables();

			//Check if footer script added
			UpdatedFooter = LoadAccountLoginFooter();

			UpdatedFooter = 0;

			//If footer script not add yet
			if (UpdatedFooter == 0)
			{
				string footerInfo = GetLocalizedText("AccountLogin.FooterScript");
				footerInfo = footerInfo.Replace("[crr_portal_url]", DotNetNuke.Common.Globals.NavigateURL());
				var TabModuleInfo = from tm in DnnDataContext.TabModules
									join m in DnnDataContext.Modules on tm.ModuleID equals m.ModuleID
									join md in DnnDataContext.ModuleDefinitions on m.ModuleDefID equals md.ModuleDefID
									where md.FriendlyName == "Account Login" && m.PortalID == PortalId
									select tm;
				foreach (TabModule tabModule in TabModuleInfo)
				{
					tabModule.Footer = footerInfo;
				}
				DnnDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
			}
		}

		/// <summary>
		/// Checks the module tables.
		/// </summary>
		private void CheckModuleTables()
		{
			string strQuery = @"IF (NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[CSPPasswordReset_User]')))
CREATE TABLE [dbo].[CSPPasswordReset_User](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [int] NOT NULL,
	[UserName] [nvarchar](100) NOT NULL,
	[RequestedDate] [datetime2](7) NOT NULL,
	[ResetDate] [datetime2](7) NULL,
	[PortalId] [int] NOT NULL,
 CONSTRAINT [PK_CSPPasswordReset_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]";
			DnnDataContext.ExecuteCommand(strQuery);
		}

		/// <summary>
		/// Check the account login footer state.
		/// </summary>
		/// <returns>System.Int32.</returns>
		private int LoadAccountLoginFooter()
		{
			int footerState = 0;
			//Get TabModule info to get footer value
			var TabModuleInfo = from tm in DnnDataContext.TabModules
			                          join m in DnnDataContext.Modules on tm.ModuleID equals m.ModuleID
			                          join md in DnnDataContext.ModuleDefinitions on m.ModuleDefID equals md.ModuleDefID
			                          where md.FriendlyName == "Account Login" && m.PortalID == PortalId
			                          select tm;
			if (!TabModuleInfo.Any())
				footerState = -1;
			var footerInfo = TabModuleInfo.FirstOrDefault(a => a.Footer != null);
			if (footerInfo != null)
				footerState = 1;
			return footerState;
		}

		private void GetRequestList()
		{
			List<CSPPasswordReset_User> requestList =
				DnnDataContext.CSPPasswordReset_Users.Where(a => a.PortalId == PortalId)
				              .OrderByDescending(a => a.RequestedDate)
				              .ToList();
			RequestList.DataSource = requestList;
			RequestList.Rebind();
		}

		private void AddSampleData()
		{
			for (int i = 0; i < 60; i++)
			{
				CSPPasswordReset_User newRec = new CSPPasswordReset_User
					{
						Id = Guid.NewGuid(),
						RequestedDate = DateTime.Now,
						UserId = i,
						UserName = "Test User " + i.ToString(),
						PortalId = PortalId
					};
				DnnDataContext.CSPPasswordReset_Users.InsertOnSubmit(newRec);
			}
			DnnDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
		}

		protected void Radgrid_Prerender(object sender, EventArgs e)
		{
			foreach (GridDataItem dataItem in RequestList.Items)
			{
				UserInfo userInfo = UserController.GetUserByName(PortalId, dataItem["UserName"].Text);
				dataItem["UserEmail"].Text = userInfo.Email;
			}
		}
	}
}