﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.DnnModules.SecuredPasswordReset.Common;
using CR.DnnModules.SecuredPasswordReset.DataContext;
using DotNetNuke.Common;
using DotNetNuke.Entities.Users;
using DotNetNuke.Services.Mail;

namespace CR.DnnModules.SecuredPasswordReset
{
	public partial class MainView : DnnModuleBase
	{
		protected override void Page_Init(object sender, EventArgs e)
		{
			base.Page_Init(sender, e);
			if (Request.Headers["X-OFFICIAL-REQUEST"] == "TRUE")
			{
				Response.Clear();

				if (!String.IsNullOrEmpty(Request.Params["username"]))
					MakePasswordReset();

				Response.Flush();
				Response.End();
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (UserInfo != null && UserInfo.IsInRole(PortalSettings.AdministratorRoleName))
				Response.Redirect(DotNetNuke.Common.Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "AdminView", "mid", ModuleId.ToString()), true);
			RegisterHeaderResources();
		}

		private void RegisterHeaderResources()
		{
			Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel='stylesheet' href='{0}css/Style.css' type='text/css' />", ControlPath)));
		}

		private void MakePasswordReset()
		{
			Response.ContentType = "application/json";
			//string userEmail = "";
			string emailLocalPart = "";
			string securedEmail = "";
			int visibleChars = 3;
			string userName = Request.Params["username"];
			UserInfo userInfo = UserController.GetUserByName(PortalId, userName);
			if (userInfo == null)
			{
				Response.Write("{\"resultcode\": \"-1\"}");
				return;
			}
			if (userInfo.IsInRole(PortalSettings.AdministratorRoleName))
			{
				Response.Write("{\"resultcode\": \"-2\"}");
				return;
			}
			emailLocalPart = userInfo.Email.Split('@')[0];
			if (emailLocalPart.Length < visibleChars)
				securedEmail = userInfo.Email;
			else
			{
				for (int i = 0; i < emailLocalPart.Length; i++)
				{
					if (i >= visibleChars)
					{
						securedEmail += "*";
						continue;
					}
					securedEmail += emailLocalPart[i];

				}
				securedEmail += "@" + userInfo.Email.Split('@')[1];
			}
			SendNextSteps(userInfo);
			Response.Write("{\"resultcode\": \"1\", \"email\": \"" + securedEmail + "\"}");
		}

		private void SendNextSteps(UserInfo userInfo)
		{
			CSPPasswordReset_User resetInfo =
				DnnDataContext.CSPPasswordReset_Users.FirstOrDefault(a => a.UserId == userInfo.UserID && a.PortalId == PortalId && a.ResetDate == null);
			if (resetInfo == null)
			{
				resetInfo = new CSPPasswordReset_User
					{
						Id = Guid.NewGuid(),
						PortalId = PortalId,
						UserId = userInfo.UserID,
						UserName = userInfo.Username,
						RequestedDate = DateTime.Now
					};
				DnnDataContext.CSPPasswordReset_Users.InsertOnSubmit(resetInfo);
			}
			else
			{
				resetInfo.RequestedDate = DateTime.Now;
			}
			DnnDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);

			string adminEmail = Settings[Cons.SETTING_ADMIN_EMAIL] == null || String.IsNullOrEmpty(Settings[Cons.SETTING_ADMIN_EMAIL].ToString())
									? PortalSettings.Email
				                    : Settings[Cons.SETTING_ADMIN_EMAIL].ToString();
			string emailTitle = Settings[Cons.SETTING_EMAIL_TITLE_RESET_PASSWORD] == null || String.IsNullOrEmpty(Settings[Cons.SETTING_EMAIL_TITLE_RESET_PASSWORD].ToString())
				                    ? GetLocalizedText("DefaultSetting.EmailTitleResetPassword")
				                    : Settings[Cons.SETTING_EMAIL_TITLE_RESET_PASSWORD].ToString();
			string emailTemplate = GetLocalizedText("Template.ResetPasswordEmail");
			string fullName = userInfo.FirstName + " " + userInfo.LastName;
			fullName = fullName.Trim();
			string resetLink = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "ResetPassword", "mid", ModuleId.ToString(), "resettoken", resetInfo.Id.ToString());
			int limitHour = Settings[Cons.SETTING_REQUEST_EXPIRED_HOUR] == null ||
							String.IsNullOrEmpty(Settings[Cons.SETTING_REQUEST_EXPIRED_HOUR].ToString())
								? Cons.DEFAULT_REQUEST_EXPIRED_HOUR
								: Convert.ToInt32(Settings[Cons.SETTING_REQUEST_EXPIRED_HOUR].ToString());

			if (String.IsNullOrEmpty(fullName)) fullName = userInfo.Username;

			emailTemplate = emailTemplate.Replace("[fullname]", fullName);
			emailTemplate = emailTemplate.Replace("[resetlink]", resetLink);
			emailTemplate = emailTemplate.Replace("[expirytime]", limitHour.ToString());
			emailTemplate = emailTemplate.Replace("[portalname]", PortalSettings.PortalName);

			string firstMailResult = Mail.SendMail(adminEmail, userInfo.Email, "", emailTitle, emailTemplate, "", "HTML", "", "", "", "");
		}
	}
}