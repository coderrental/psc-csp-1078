﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainView.ascx.cs" Inherits="CR.DnnModules.SecuredPasswordReset.MainView" %>
<div id="forgot-pwd-section">
	<div id="forgot-pwd-section-label">
		<h2>Forgot Password</h2>
	</div>
	<div id="forgot-pwd-section-form">
		<div id="username-label">Your Username</div>
		<div id="username-input"><input type="text" id="fps-username"/></div>
	</div>
	<div id="forgot-pwd-section-footer">
		<input type="button" value="Submit" id="submit-fps"/>
		<input type="button" value="Cancel" id="cancel-fps"/>
	</div>
</div>

<script type="text/javascript">
	$(function () {
		$('#cancel-fps').click(function() {
			window.location = '<%= DotNetNuke.Common.Globals.NavigateURL("Login") %>';
		});

		$('#submit-fps').click(function(e) {
			GoSubmit(e);
		});
		
		$('#fps-username').keypress(function (e) {
			if (e.which == 13) {
				GoSubmit(e);
				$(this).blur();
			}
		});
	});
	
	function GoSubmit(e) {
		e.preventDefault();
		$('#submit-fps').prop('disabled', true);
		var userName = $('#fps-username').val();
		userName = userName.trim();
		if (userName == '') {
			alert('<%= GetLocalizedText("Msg.EnterUsername") %>');
			$('#submit-fps').prop('disabled', false);
			return;
		}
		
		$.ajax({
			type: 'POST',
			url: document.URL,
			dataType: 'json',
			data: { username: userName },
			beforeSend: function (xhr) {
				xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE");
			},
			success: function (responseData) {
				var rCode = parseInt(responseData.resultcode);
				switch (rCode) {
					case 1:
						var msgSucceed = '<%= GetLocalizedText("Msg.ResetPasswordRequestSucceed") %>';
						msgSucceed = msgSucceed.replace('[email_address]', responseData.email);
						$('#username-label').text(msgSucceed);
						$('#username-input').remove();
						$('#submit-fps').prop('disabled', true);
						$('#cancel-fps').val('Close');
						break;
					case -1:
						alert('<%= GetLocalizedText("Msg.UsernameNotFound") %>');
						$('#submit-fps').prop('disabled', false);
						break;
					case -2:
						alert('<%= GetLocalizedText("Msg.AccessDenied") %>');
						$('#submit-fps').prop('disabled', false);
						break;
				}
			},
			error: function () {
				alert('<%= GetLocalizedText("Msg.SubmitFailed") %>');
			}
		});
	}
</script>