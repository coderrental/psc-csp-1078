﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainView.ascx.cs" Inherits="CR.DnnModules.ShowcaseContentEntry.MainView" %>
<%@ Import Namespace="CR.ContentObjectLibrary.Data" %>
<%@ Import Namespace="DotNetNuke.Common" %>
<%@ Import Namespace="DotNetNuke.Services.Localization" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadScriptBlock runat="server">
<script type="text/javascript">
	var controlUrl = '<%= DotNetNuke.Common.Globals.NavigateURL(TabId, "") %>';
	var emptyGuid = '<%= Guid.Empty %>';
	var missingContentList = new Array();
	var showAll = '';
	if (window.location.href.indexOf('showall') >= 0) {
		showAll = '&showall=true';
	}
	<asp:Repeater ID="rptList" runat="server">
		<ItemTemplate>
			// Content Instance id, depth, CategoryID,ParentCategoryID,Title, Sortorder,Lineage, Level, DescriptionShort, Partnumber, ReportingID
			(function(c){ c.Add("<%# Eval("CategoryContentId").ToString() %>", "<%# Eval("ContentPageId").ToString() %>", <%# Eval("Depth").ToString() %>,<%# Eval("Id").ToString() %>,<%# Eval("ParentId").ToString() %>,"<%# Eval("Text").ToString() %>","<%# Eval("StatusSortOrder").ToString() %>","<%# Eval("Lineage").ToString() %>",<%# Eval("Depth").ToString() %>,"<%# Eval("Description").ToString() %>", "<%# Eval("Thumbnail").ToString() %>", "", "", <%# Eval("Active").ToString().ToLower() %>, <%# Eval("StagePublished").ToString().ToLower() %>, <%# Eval("IsSubscribable").ToString().ToLower() %> , controlUrl);})(categories);
		</ItemTemplate>
	</asp:Repeater>
</script>
</telerik:RadScriptBlock>
<p runat="server" Visible="False" ID="ErrorMessage"></p>
<asp:Panel runat="server" ID="MainPane">
	<div id="list-controls">
		<div id="list-controls-inner">
			<a href="javascript: void(0)" id="side-controls-trigger">
				<img src="<%= ControlPath %>images/side-menu-icon.png" />
			</a>
		</div>
	</div>
    <div id="csp-wrapper">
		<div id="csp-top-section">
			<div id="csp-logo">
				
			</div>
			<div id="csp-nav">
				
			</div>
			<div class="clear"></div>
		</div>
		<div id="csp-page-header">
            <div class="csp-clear" id="csp-breadcrumb"></div>
            <div id="csp-page-title" class="csp-clear">
				<span class="csp-title"></span>
			</div>
        </div>
		<div id="csp-content" class="csp-clear">
			<div id="csp-side-col" class="csp-side-col">
				<%--<ul id="side-col-list" class="csp-sibling-items">
					<telerik:RadListView runat="server" ID="LeftCategoryList">
						<ItemTemplate>
							<li id="c-<%# Eval("Id").ToString() %>">
								<a class="aside-menu-item" cateid="<%#Eval("Id").ToString() %>" href="javascript: void(0);" rel="CategoryTitleEditor-<%#Eval("Id").ToString() %>"><%# Eval("Text").ToString() %></a>
								<input type="text" id="CategoryTitleEditor-<%#Eval("Id").ToString() %>" value="<%# Eval("Text").ToString() %>" class="hide"/>
							</li>
						</ItemTemplate>
					</telerik:RadListView>
				</ul>--%>
			</div>
			<div id="csp-right-col" class="csp-right-col">
			    <telerik:RadListView runat="server" ID="CategoryContentList" OnItemCreated="RadListView_ItemDataBound">
			        <ItemTemplate>
			            <div class="content-page-detail" editorid="<%#Container.FindControl("ContentPageEditor").ClientID%>" contentid="<%#Eval("content_Id")%>">
			                <%#Eval("Content_Page")%>
                        </div>
                        <telerik:RadEditor Width="490px" Height="600px" runat="server" CssClass='hide' ID="ContentPageEditor" Content='<%#Eval("Content_Page") %>' ContentAreaMode="Div" NewLineMode="P">
						</telerik:RadEditor>
			        </ItemTemplate>
			    </telerik:RadListView>
			</div>
			<div class="csp-clear"></div>
			<div id="tieBrandingFooter">
			    <img src="<%=ControlPath %>images/TIE_KINETIX.gif" alt="TIE KINETIX LOGO" />
			</div>
		</div>
	</div>
	<div id="side-controls-menu">
		<ul>
			<li>
				<a id="sci-showactive" class="side-controls-item" href="<%=DotNetNuke.Common.Globals.NavigateURL(TabId, "") %>">
					<%=GetLocalizedText("btn.ShowActiveList") %>
				</a>
				<div id="sci-showactive-active" class="side-control-active"></div>
			</li>
			<li>
				<a id="sci-showall" class="side-controls-item" href="<%=DotNetNuke.Common.Globals.NavigateURL(TabId, "", "showall=true") %>">
					<%=GetLocalizedText("btn.ShowAllList") %>
				</a>
				<div id="sci-showall-active" class="side-control-active"></div>
			</li>
			<li>
				<a id="sci-pageman" class="side-controls-item" href="javascript: void(0)">
					<%=GetLocalizedText("btn.PageMan") %>
				</a>
			</li>
			<li>
				<a id="sci-newrootitem" class="side-controls-item" href="javascript: void(0)">
					<%=GetLocalizedText("btn.NewRootItem") %>
				</a>
			</li>
			<li>
				<a id="sci-addmissingcontents" class="side-controls-item" href="javascript: void(0)">
					<%=GetLocalizedText("btn.AddMissingContents") %>
				</a>
			</li>
			<li id="sci-publishonfly-outter">
				<a id="sci-publishonfly" class="side-controls-item" href="javascript: void(0)">
					<%=GetLocalizedText("btn.PublishOnFly") %>
				</a>
				<div id="sci-publishonfly-switch" class="<%= (PublishOnFly || Session["pofstate"].ToString() == "true") ? String.Empty : "is-off" %>"></div>
			</li>
			<li>
				<a id="sci-broadcast" class="side-controls-item" href="javascript: void(0)">
					<%=GetLocalizedText("btn.Broadcast") %>
				</a>
			</li>
			<li id="sci-rollback-outter">
				<a id="sci-rollback" class="side-controls-item" href="javascript: void(0)">
					<%=GetLocalizedText("btn.Rollback") %>
				</a>
				<!-- Window Checkpoint List -->
				<div class="CustomWindow" id="WindowRollbackList">
					<div class="cw-top">
						<div class="cw-top-right">
							<div class="cw-top-mid"></div>
						</div>
					</div>
					<div class="cw-body">
						<div class="cw-body-right-edge">
							<div class="cw-body-content">
								<div id="grid-checkpoint"></div>
								<script id="checkpointRowTemplate" type="text/x-kendo-tmpl">
									<tr>
										<td>
											<a href="javascript: void(0)" rel="#: Id #" class="go-rollback"><img src="<%= ControlPath %>images/icon-checkpoint-rollback.png" /></a> &nbsp;
											<a href="javascript: void(0)" rel="#: Id #" class="go-sync-checkpoint"><img src="<%= ControlPath %>images/icon-checkpoint-sync.png" /></a>
										</td>
										<td>
										  <a href="javascript: void(0)" rel="#: Id #" class="checkpoint-link">#: DateTime #</a>
										</td>
										<td>
										   #: Comment #
										</td>
										<td>
											<a href="javascript: void(0)" rel="#: Id #" class="checkpoint-link">View detail</a>
										</td>
								   </tr>
								</script>
								<div id="checkpoint-detail-outter"></div>
								<div id="checkpoint-detail">
									<div id="checkpoint-detail-close">
										<img id="checkpoint-detail-close-trigger" src="<%= ControlPath %>images/btnclose-16x16.png"/>
									</div>
									<div id="grid-checkpoint-detail"></div>
									<script id="checkpointDetailRowTemplate" type="text/x-kendo-tmpl">
									<tr>
										<td>
											#: Category #
										</td>
										<td>
											#: Action #
										</td>
										<td>
										   #: DateTime #
										</td>
								   </tr>
								</script>
								</div>
							</div>
						</div>
					</div>
					<div class="cw-bottom">
						<div class="cw-bottom-right">
							<div class="cw-bottom-mid"></div>
						</div>
					</div>
				</div>
			</li>
			<li style="display: none">
				<a id="sci-staging-list" class="side-controls-item" href="javascript: void(0)">
					<%=GetLocalizedText("btn.PageVersions") %>
				</a>
			</li>
		</ul>
	</div>
	
	<telerik:RadWindow runat="server" ID="RWBannerFileMan" Behaviors="Close,Move" VisibleStatusbar="false" OnClientClose="RWBannerFileManClose"
		Width="600px" Height="370px" ShowContentDuringLoad="false" ShowOnTopWhenMaximized="True" CssClass="top-window">
		<ContentTemplate>
			<div id="wdBannerFileMan">
				<telerik:RadFileExplorer runat="server" ID="BannerFileMan" width="540" height="300" OnClientFileOpen="OnClientFileOpen" ></telerik:RadFileExplorer>
			</div>
		</ContentTemplate>
	</telerik:RadWindow>
	
	<telerik:RadWindow runat="server" ID="RWCategoryEditForm" Behaviors="Close,Move" Modal="true" VisibleStatusbar="false"
		EnableShadow="true" Width="550px" Height="300px" ShowContentDuringLoad="false" OnClientClose="clearCategoryForm" CssClass="lower-window">
		<ContentTemplate>
			<div id="wdCategoryEditForm">
				<table width="100%">
					<tr>
						<td>Parent:</td><td><span id="lblParentCategory"></span></td>
					</tr>
					<tr>
						<td>Category Text:</td><td><div class="pos-relative"><input type="text" id="txtCategoryText" class="long-input"/><span id="errormsg-txtCategoryText" class="error-msg"><%= GetLocalizedText("Msg.RequiredFilling") %></span></div></td>
					</tr>
					<tr id="CategoryThumbnailRow">
						<td>Category Thumbnail:</td><td><input type="text" id="txtCategoryThumbnail" class="long-input"/><telerik:RadButton ID="btnOpenCategoryThumbnail" runat="server" AutoPostBack="False" OnClientClicked="OpenCategoryThumbnail"></telerik:RadButton></td>
					</tr>
					<tr>
						<td valign="top">Category Description:</td><td><textarea id="textDescription" cols="29" rows="3" class="popup-textarea"></textarea></td>
					</tr>
					<tr>
						<td>Is Subscribable:</td><td><input type="checkbox" id="chkIsSubscribable"/></td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<telerik:RadButton ID="btnSaveCategory" runat="server" AutoPostBack="False" OnClientClicked="SaveCategoryForm"></telerik:RadButton>
							<telerik:RadButton ID="btnCancelCategory" runat="server" AutoPostBack="False" CssClass="btn-cancel-category" OnClientClicked="closeCategoryFormWD"></telerik:RadButton>
							<input type="hidden" id="txtParentId" value="0"/>
							<input type="hidden" id="txtCategoryId" value="0"/>
						</td>
					</tr>
				</table>
			</div>
		</ContentTemplate>
	</telerik:RadWindow>
	
	<telerik:RadWindow runat="server" ID="RWCategoryDescriptionEditor" Behaviors="Close,Move" Modal="true" VisibleStatusbar="false"
		EnableShadow="true" Width="550px" Height="230px" ShowContentDuringLoad="false" CssClass="lower-window">
		<ContentTemplate>
			<div id="wdCategoryDescriptionEditor">
				<table width="100%">
					<tr>
						<td>Category:</td><td><span id="lblCategoryOfDesc"></span></td>
					</tr>
					<tr>
						<td>Category Thumbnail:</td><td><input type="text" id="textCrrThumbnail" class="long-input"/><telerik:RadButton ID="btnBrowseNewThumbnail" runat="server" AutoPostBack="False" OnClientClicked="OpenCategoryThumbnail" CssClass="btn-crr-thumbnail"></telerik:RadButton></td>
					</tr>
					<tr>
						<td valign="top">Category Description:</td><td><textarea id="textCrrDescription" cols="29" rows="3" class="popup-textarea"></textarea></td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<telerik:RadButton ID="btnSaveCategoryDescription" runat="server" AutoPostBack="False" OnClientClicked="SaveCategoryDescForm"></telerik:RadButton>
							<telerik:RadButton ID="btnCancelDescription" runat="server" AutoPostBack="False" OnClientClicked="closeCategoryDescFormWD"></telerik:RadButton>
							<input type="hidden" id="txtCategoryIdToEditDesc" value="0"/>
						</td>
					</tr>
				</table>
			</div>
		</ContentTemplate>
	</telerik:RadWindow>
	
	<telerik:RadWindow runat="server" ID="RWDeleteCategoryOption" Behaviors="Close,Move" Modal="true" VisibleStatusbar="false"
		EnableShadow="true" Width="400px" Height="200px" ShowContentDuringLoad="false" CssClass="lower-window">
		<ContentTemplate>
			<div id="wdDeleteCategoryOption">
				<div id="delete-caption">
					<%= GetLocalizedText("confirm.DeleteCategory")%>
				</div>
				<div style="text-align: center">
					<telerik:RadButton ID="btnDeleteAll" runat="server" AutoPostBack="False" OnClientClicked="deleteCategoryAllLang"></telerik:RadButton>
					<telerik:RadButton ID="btnDeleteByLanguage" runat="server" AutoPostBack="False" OnClientClicked="deleteCategoryThisLang"></telerik:RadButton>
					<telerik:RadButton ID="btnCancelDetele" runat="server" AutoPostBack="False" OnClientClicked="closeCategoryDeleteWD"></telerik:RadButton>
					<input type="hidden" id="txtDeleteCategoryId" value="0"/>
				</div>
			</div>
		</ContentTemplate>
	</telerik:RadWindow>
	
	<telerik:RadWindow runat="server" ID="RWCategoryTree" Behaviors="Close,Move" VisibleStatusbar="false"
		Width="600px" Height="450px" ShowContentDuringLoad="false" ShowOnTopWhenMaximized="True" CssClass="top-window" OnClientClose="RWCategoryTreeClose" OnClientShow="RWCategoryTreeShow">
		<ContentTemplate>
			<telerik:RadTreeView runat="server" ID="CategoryTree" DataFieldID="Id" DataTextField="Text" DataValueField="Id" DataFieldParentID="ParentId" OnClientDoubleClick="CategoryTreeNodeSelect">
			</telerik:RadTreeView>
		</ContentTemplate>
	</telerik:RadWindow>
	
	<telerik:RadWindow runat="server" ID="RWPageMan" Behaviors="Close,Move" VisibleStatusbar="false" ReloadOnShow="True"
		Width="600px" Height="450px" ShowContentDuringLoad="false" ShowOnTopWhenMaximized="True" CssClass="top-window" Modal="True" OnClientClose="RWPageManClose">
		
	</telerik:RadWindow>
	
	<telerik:RadWindow runat="server" ID="RWBannerLinkProperties" Behaviors="Close,Move" Modal="true" VisibleStatusbar="false"
		EnableShadow="true" Width="400px" Height="150px" ShowContentDuringLoad="false" CssClass="lower-window">
		<ContentTemplate>
			<table width="100%" style="padding: 10px 0px 0px 10px">
				<tr>
					<td>Banner URL:</td>
					<td><input type="text" class="long-input" id="textBannerUrlHref"/></td>
				</tr>
				<tr>
					<td colspan="2" align="center" style="padding-top: 15px">
						<telerik:RadButton ID="btnSaveBannerUrl" runat="server" AutoPostBack="False" OnClientClicked="SaveBannerUrl"></telerik:RadButton>
						<telerik:RadButton ID="btnCloseBannerUrlWD" runat="server" AutoPostBack="False" OnClientClicked="closeBannerUrlWD"></telerik:RadButton>
					</td>
				</tr>
			</table>
		</ContentTemplate>
	</telerik:RadWindow>

	<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" ZIndex="99999" Skin="Vista">
	</telerik:RadAjaxLoadingPanel>
</asp:Panel>

<telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
<script type="text/javascript">
	var ajaxLoadingPanelId = '<%= RadAjaxLoadingPanel1.ClientID %>';
	var mainPane = '<%= MainPane.ClientID %>';
	var crrCatId = <%=CategoryId %>;
	var categoryLevel = <%=CategoryLevel %>;
	var rootCatId = <%=_rootCategoryId %>;
	var ttMsgContentMissing = '<%= GetLocalizedText("tooltips.MsgContentMissing") %>';
	var ttMsgInactiveItem = '<%= GetLocalizedText("tooltips.MsgInactiveItem") %>';
	var ttMsgUnPublishedItem = '<%= GetLocalizedText("tooltips.MsgUnPublishedItem") %>';
	var originalCategoryCount = <%= OriginalCategoryCount() %>;
	var languagesCount = <%= Localization.GetEnabledLocales().Count %>;
	var hostUrl = '<%= Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host %>';
	var radFileExplorerId = '<%= BannerFileMan.ClientID %>';
	var uploadPath = '<%= System.Web.VirtualPathUtility.ToAbsolute(_uploadFolder) %>/';
	var RWCategoryEditForm = '<%= RWCategoryEditForm.ClientID %>';
	var RWCategoryDescriptionEditor = '<%= RWCategoryDescriptionEditor.ClientID %>';
	var RWDeleteCategoryOption = '<%= RWDeleteCategoryOption.ClientID %>';
	var RWBannerFileMan = '<%= RWBannerFileMan.ClientID %>';
	var RWCategoryTree = '<%= RWCategoryTree.ClientID %>';
	var RWBannerUrl = '<%= RWBannerLinkProperties.ClientID %>';
	var FileExplorerTrigger = '';
	var crrRadEditor = '';
	var isRWCategoryTreeOpen = false;
	var isCategoryTreeNodeDraggable = true;
	var range = null;
	
	$(function () {
		initApp();
		try {
			Telerik.Web.UI.Editor.CommandList["CoParametersDropDown"] = function (commandName, editor, args) {
				var val = args.get_value();
				editor.pasteHtml("{consumer:F" + val + "}");
				args.set_cancel(true); 
			};
			
			Telerik.Web.UI.Editor.CommandList["AddCategoryLink"] = function(commandName, editor, args)
			{
				var wdCategoryTree = $find(RWCategoryTree);
				wdCategoryTree.show();
				crrRadEditor = editor;
				range = editor.getSelection().getRange();
				isCategoryTreeNodeDraggable = false;	
				args.set_cancel(true);
			};
			
			Telerik.Web.UI.Editor.CommandList["AddDocLink"] = function(commandName, editor, args)
			{
				FileExplorerTrigger = "AddDocLink";
				var radWD = $find(RWBannerFileMan);
				radWD.show();
				crrRadEditor = editor;
				range = editor.getSelection().getRange();
				args.set_cancel(true);
			};
		} catch(e) {

		}
	});
	
	function OnClientFileOpen(sender, args) { // Called when a file is open.
		try {
			RadFileExplorerFileClicked(sender, args);
		}catch (err) {
			
		}
	}
	
	function OpenCategoryThumbnail(sender, args) {
		FileExplorerTrigger = 'CategoryThumbnail';
		if (sender.get_cssClass() == 'btn-crr-thumbnail')
			FileExplorerTrigger = 'CrrCategoryThumbnail';
		var radWD = $find(RWBannerFileMan);
		radWD.show();
	}
	
	function CategoryTreeNodeSelect(sender, eventArgs) {
        var node = eventArgs.get_node();
		if (crrRadEditor == '')
			return;
		crrRadEditor.getSelection().selectRange(range);
		crrRadEditor.pasteHtml('<a href="javascript:void(0)" class="content-page-links" id="pagelink-' + node.get_value() + '">' + node.get_text() + '</a>');
    }
	
	function RWCategoryTreeClose() {
		crrRadEditor = '';
		isRWCategoryTreeOpen = false;
	}

	function RWCategoryTreeShow() {
		isRWCategoryTreeOpen = true;
	}

	function RWBannerFileManClose() {
		FileExplorerTrigger = '';
	}
	
	function RWPageManClose() {
		try {
			switchLoadingPanel(true);
		} catch(e) {

		} 
		window.location = document.URL;
	}

	function SaveBannerUrl() {
		try {
			CustomSaveBannerUrl();
		} catch(e) {

		} 
	}
	
	function closeBannerUrlWD () {
		var radWD = $find(RWBannerUrl);
		radWD.close();
	}

	$('#sci-pageman').click(function() {
		var wdPageMan = $find('<%= RWPageMan.ClientID %>');
		wdPageMan.set_navigateUrl('<%= ControlPath %>PageManagement.aspx?<%= GetPageManParameters() %>');
		wdPageMan.show();
	});
	 
</script>
</telerik:RadScriptBlock>