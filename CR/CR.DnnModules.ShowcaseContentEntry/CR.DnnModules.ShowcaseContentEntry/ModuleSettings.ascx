﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleSettings.ascx.cs" Inherits="CR.DnnModules.ShowcaseContentEntry.ModuleSettings" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div>
    <table>
        <tbody>
			<tr>
                <td>
                    Category Content Type Id:
                </td>
                <td>
                    <telerik:RadTextBox runat="server" Width="150px" ID="tbxCategoryContentId" />
                </td>
            </tr>
			<tr>
                <td>
                    Page Content Type Id:
                </td>
                <td>
                    <telerik:RadTextBox runat="server" Width="150px" ID="tbxPageContentId" />
                </td>
            </tr>
			<tr>
                <td>
                    Main Menu Depth:
                </td>
                <td>
                    <telerik:RadTextBox runat="server" Width="150px" ID="tbxMainMenuDepth" />
                </td>
            </tr>
			<tr>
                <td>
                    Publish on the fly:
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkPublishOnTheFly"/>
                </td>
            </tr>
			<tr>
                <td>
                    Publication Scheme Id:
                </td>
                <td>
                    <telerik:RadTextBox runat="server" Width="150px" ID="tbxPublicationSchemeId" />
                </td>
            </tr>
			<tr>
                <td>
                    Content Staging Scheme Id:
                </td>
                <td>
                    <telerik:RadTextBox runat="server" Width="150px" ID="tbxContentStagingSchemeId" />
                </td>
            </tr>
			<tr>
                <td>
                    Upload Folder:
                </td>
                <td>
                    <telerik:RadTextBox runat="server" Width="350px" ID="tbxUploadFolder" />
                </td>
            </tr>
			<tr>
                <td>
                    Absolute URL:
                </td>
                <td>
                    <telerik:RadTextBox runat="server" Width="350px" ID="tbxImageUrl" />
                </td>
            </tr>
			<tr>
                <td>
                    CSS File URL:
                </td>
                <td>
                    <telerik:RadTextBox runat="server" Width="350px" ID="tbxCSSUrl" />
                </td>
            </tr>
			<tr>
                <td>
                    JS File URL:
                </td>
                <td>
                    <telerik:RadTextBox runat="server" Width="350px" ID="tbxJSUrl" />
                </td>
            </tr>
			<tr>
                <td valign="top">
                    Company Parameters:
                </td>
                <td>
                    <telerik:RadListBox runat="server" ID="cpListBoxSrc" Height="200px" Width="250px" 
					DataTextField="Text" DataValueField="Id" DataKeyField="Id"
					AllowTransfer="true" TransferToID="cpListBoxDest"></telerik:RadListBox>

					 <telerik:RadListBox runat="server" ID="cpListBoxDest" Height="200px" Width="250px" DataTextField="Text" DataValueField="Id" DataKeyField="Id"></telerik:RadListBox>
				</td>
            </tr>
		</tbody>
	</table>
</div>