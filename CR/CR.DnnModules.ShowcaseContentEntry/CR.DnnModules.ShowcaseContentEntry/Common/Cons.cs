﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CR.DnnModules.ShowcaseContentEntry.Common
{
	public class Cons
	{
		public const string SETTING_THEME = "Theme";
		public const string SETTING_CATEGORYCONTENT_CONTENTTYPE_ID = "CategoryContentTypeId";
		public const string SETTING_PAGE_CONTENTTYPE_ID = "PageContentTypeId";
		public const string SETTING_UPLOAD_FOLDER = "UploadFolder";
		public const string SETTING_IMAGE_URL = "ImageUrl";
		public const string SETTING_CSS_URL = "CSSUrl";
		public const string SETTING_JS_URL = "JSUrl";
		public const string SETTING_PUBLICATION_SCHEME_ID = "PublicationSchemeId";
		public const string SETTING_CONTENT_STAGING_SCHEME_ID = "ContentStagingSchemeId";
		public const string SETTING_COMPAMY_PARAMETERS = "CompanyParameters";
		public const string SETTING_PUBLISH_ON_THE_FLY = "SettingPublishOnTheFly";
		public const string SETTING_MAIN_MENU_DEPTH = "SettingMainMenuDepth";
		public const int STAGE_STAGING = 48;
	}
}