﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common.CategoryTree;
using CR.DnnModules.Common;

namespace CR.DnnModules.ShowcaseContentEntry.Common
{
	public class CustomCategoryNode: CustomCategory
	{
		public bool Active { get; set; }
		public bool IsSubscribable { get; set; }
		public int DisplayOrder { get; set; }
		public string StatusSortOrder { get; set; }
		public string Description { get; set; }
		public string Thumbnail { get; set; }
		public Guid CategoryContentId { get; set; }
		public Guid ContentPageId { get; set; }
		public CR.ContentObjectLibrary.Data.content ContentPage { get; set; }
		public bool StagePublished { get; set; }
		private const int STAGE_PUBLISHED_ID = 50;

		public CustomCategoryNode()
		{
			
		}

		public CustomCategoryNode (category cateItem)
		{
			Id = cateItem.categoryId;
			Text = cateItem.categoryText;
			ParentId = (int) cateItem.parentId;
			Depth = (int) cateItem.depth;
			Lineage = cateItem.lineage;
			Active = (bool) cateItem.active;
			IsSubscribable = (bool) cateItem.IsSubscribable;
			DisplayOrder = cateItem.DisplayOrder != null ? (int) cateItem.DisplayOrder : 1;
			StatusSortOrder = cateItem.DisplayOrder != null ? ((int)cateItem.DisplayOrder).ToString("0000") : "0000";
			CategoryContentId = Guid.Empty;
			ContentPageId = Guid.Empty;
			CategoryContent = null;
			ContentPage = null;
			StagePublished = false;
		}

		public CustomCategoryNode(string text, int id, int parentId, int depth, string lineage, bool active, bool isSubscribable, int displayOrder, string statusSortOrder, content categoryContent, content contentPage)
		{
			Text = text;
			Id = id;
			ParentId = parentId;
			Depth = depth;
			Lineage = lineage;
			Count = 0;
			Active = active;
			IsSubscribable = isSubscribable;
			DisplayOrder = displayOrder;
			StatusSortOrder = statusSortOrder;
			CategoryContentId = Guid.Empty;
			ContentPageId = Guid.Empty;
			StagePublished = false;
			if (string.IsNullOrEmpty(StatusSortOrder))
				StatusSortOrder = "0000";
			int publishedCount = 0;
			if (categoryContent != null)
			{
				CategoryContent = categoryContent;
				CategoryContentId = categoryContent.content_Id;
				if (categoryContent.stage_Id == STAGE_PUBLISHED_ID)
					publishedCount++;
			}
			if (contentPage != null)
			{
				ContentPageId = contentPage.content_Id;
				ContentPage = contentPage;
				if (contentPage.stage_Id == STAGE_PUBLISHED_ID)
					publishedCount++;
			}

			if (publishedCount == 2)
				StagePublished = true;
		}

		public CustomCategoryNode(category catItem, content categoryContent, content contentPage, int displayOrder, CspUtils cspUtils)
		{
			Text = catItem.categoryText;
			Id = catItem.categoryId;
			Description = "";
			Thumbnail = "";
			ParentId = (int) catItem.parentId;
			Depth = (int) catItem.depth;
			Lineage = catItem.lineage;
			Count = 0;
			Active = !(catItem.active == null || catItem.active == false);
			IsSubscribable = !(catItem.IsSubscribable == null || catItem.IsSubscribable == false);// (bool)catItem.IsSubscribable;
			DisplayOrder = displayOrder;
			StatusSortOrder = displayOrder.ToString("0000");
			CategoryContentId = Guid.Empty;
			ContentPageId = Guid.Empty;
			StagePublished = false;
			if (string.IsNullOrEmpty(StatusSortOrder))
				StatusSortOrder = "0000";
			int publishedCount = 0;
			if (categoryContent != null)
			{
				Text = categoryContent["Content_Title"].value_text;
				Description = cspUtils.GetContentFieldValue(categoryContent, "Content_Description_Short");
				Description = HttpUtility.HtmlEncode(Description);
				Description = Description.Replace("\n", "<br />");
				Description = Description.Replace("\r", "<br />");
				Thumbnail = cspUtils.GetContentFieldValue(categoryContent, "Content_Image_Thumbnail");
				CategoryContent = categoryContent;
				CategoryContentId = categoryContent.content_Id;
				if (categoryContent.stage_Id == STAGE_PUBLISHED_ID)
					publishedCount++;
			}
			if (contentPage != null)
			{
				ContentPageId = contentPage.content_Id;
				ContentPage = contentPage;
				if (contentPage.stage_Id == STAGE_PUBLISHED_ID)
					publishedCount++;
			}

			if (publishedCount == 2 || categoryContent == null || contentPage == null)
				StagePublished = true;
		}

		public CustomCategoryNode(category catItem, content srcCategoryContent, content srcContentPage, content categoryContent, content contentPage, int displayOrder, CspUtils cspUtils, bool isCreatedInStaging, string lineAge)
		{
			Text = catItem.categoryText;
			Id = catItem.categoryId;
			Description = "";
			Thumbnail = "";
			ParentId = (int)catItem.parentId;
			Depth = (int)catItem.depth;
			Lineage = lineAge;
			Count = 0;
			Active = !(catItem.active == null || catItem.active == false);
			IsSubscribable = !(catItem.IsSubscribable == null || catItem.IsSubscribable == false);// (bool)catItem.IsSubscribable;
			DisplayOrder = displayOrder;
			StatusSortOrder = displayOrder.ToString("0000");
			CategoryContentId = Guid.Empty;
			ContentPageId = Guid.Empty;
			StagePublished = false;
			if (string.IsNullOrEmpty(StatusSortOrder))
				StatusSortOrder = "0000";
			int publishedCount = 0;
			if (categoryContent != null)
			{
				Text = categoryContent["Content_Title"].value_text;
				Description = cspUtils.GetContentFieldValue(categoryContent, "Content_Description_Short");
				Description = HttpUtility.HtmlEncode(Description);
				Description = Description.Replace("\n", "<br />");
				Description = Description.Replace("\r", "<br />");
				Thumbnail = cspUtils.GetContentFieldValue(categoryContent, "Content_Image_Thumbnail");
				CategoryContent = categoryContent;
				CategoryContentId = categoryContent.content_Id;
				if (srcCategoryContent != null)
				{
					if (srcCategoryContent.stage_Id == STAGE_PUBLISHED_ID)
						publishedCount++;
				}
				else if (categoryContent.stage_Id == STAGE_PUBLISHED_ID)
					publishedCount++;
			}
			if (contentPage != null)
			{
				ContentPageId = contentPage.content_Id;
				ContentPage = contentPage;
				if (srcContentPage != null)
				{
					if (srcContentPage.stage_Id == STAGE_PUBLISHED_ID)
						publishedCount++;
				}
				else if (contentPage.stage_Id == STAGE_PUBLISHED_ID)
					publishedCount++;
			}

			if (publishedCount == 2 || categoryContent == null || contentPage == null || isCreatedInStaging)
				StagePublished = true;
		}
	}

	public class Category
	{
		public string Text { get; set; }

		public int Id { get; set; }

		public int ParentId { get; set; }

		public int DisplayOrder { get; set; }

	}
}