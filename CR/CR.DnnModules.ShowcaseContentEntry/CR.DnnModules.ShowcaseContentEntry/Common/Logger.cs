﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Threading;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Localization;
using DotNetNuke.Services.Log.EventLog;

namespace CR.DnnModules.ShowcaseContentEntry.Common
{
	public class Logger
	{
		public string GetLogName(int logId)
		{
			LogAction action = (LogAction) Enum.Parse(typeof (LogAction), logId.ToString());
			if (action == null)
				return String.Empty;
			return action.ToString();
		}
	}

	public enum LogAction
	{
		Rename = 1,
		EditContent,
		Move,
		MoveToNewParent,
		EditPageDescription,
		Created,
		GoActive,
		GoInactive,
		GoPublish,
		GoUnpublish
	}
}