﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CR.DnnModules.ShowcaseContentEntry.Common
{
    public class ContentPageObj
    {
        public Guid content_Id { get; set; }

        public String Content_Page { get; set; }

        public ContentPageObj()
        {
            content_Id = Guid.Empty;
            Content_Page = String.Empty;
        }
    }
}