﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Security;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using CR.DnnModules.Common.CategoryTree;
using CR.DnnModules.ShowcaseContentEntry.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Users;
using Telerik.Web.UI;
using Telerik.Web.UI.FileExplorer;
using category = CR.ContentObjectLibrary.Data.category;
using consumers_theme = CR.ContentObjectLibrary.Data.consumers_theme;
using themes_category = CR.ContentObjectLibrary.Data.themes_category;
using CR.DnnModules.Common;
using HtmlAgilityPack;

namespace CR.DnnModules.ShowcaseContentEntry
{
    public partial class MainView : DnnModuleBase
    {
        protected int CategoryId;
    	protected int CategoryLevel;
    	protected int _rootCategoryId;
    	protected category CurrentCategory;
    	protected List<CustomCategoryNode> CustomCategoryNodes;
    	protected int _ctContentCategoryId;
    	protected int _ctContentPageId;
    	protected int _mainMenuDepth = 3;
    	protected string _staticImgUrl;
    	protected string _uploadFolder;
    	protected int _langId;
    	protected string _langCode;
    	protected string _externalCSSUrl;
    	protected string _externalJSUrl;
    	protected List<String> _coParamTypes;
    	protected bool PublishOnFly = false;
		private const int STAGE_PUBLISHED = 50;
		private const int STAGE_UNPUBLISHED = 56;
		private const int STAGE_STAGING = Cons.STAGE_STAGING;
    	private const string CTF_CATEGORYCONTENT_STAGING = "Content_Staging";
		private const string CTF_CONTENTPAGE_STAGING = "Content_Staging";
    	private string _externalIdentifier;
    	private List<int> _themeIds = new List<int>();
		private int _publicationSchemeId = 11;
		private int _contentStagingSchemeId = 1;
    	private CspUtils _cspUtils;
		private Logger _logger = new Logger();
		private Dictionary<string, string> _originalDisplayOrder = new Dictionary<string, string>();
    	private bool _isRepublish = false;

        protected override void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
			InitGlobalVariables();
            if (Request.Headers["X-OFFICIAL-REQUEST"] == "TRUE") 
            {
                return;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
			if (Request.Headers["X-OFFICIAL-REQUEST"] == "TRUE") 
            {
                GoAjaxAction();
                return;
            }
			if (IsPostBack)
				return;
        	bool isSet = true;
			if (_externalIdentifier == String.Empty)
			{
				isSet = false;
				ErrorMessage.InnerHtml += string.Format("<p>{0}</p>", GetLocalizedText("label.InvalidChannelExternalId"));
			}
			if (_ctContentCategoryId < 1)
			{
				isSet = false;
				ErrorMessage.InnerHtml += string.Format("<p>{0}</p>", GetLocalizedText("label.SettingMissingContentCategory"));
			}
			if (_ctContentPageId < 1)
			{
				isSet = false;
				ErrorMessage.InnerHtml += string.Format("<p>{0}</p>", GetLocalizedText("label.SettingMissingContentPage"));
			}
			if (_rootCategoryId == 0)
			{
				isSet = false;
				ErrorMessage.InnerHtml += string.Format("<p>{0}</p>", GetLocalizedText("label.RootCategoryNotFound"));
			}
			if (!_themeIds.Any())
			{
				isSet = false;
				ErrorMessage.InnerHtml += string.Format("<p>{0}</p>", GetLocalizedText("label.ThemeFailed"));
			}

			if (!isSet)
			{
				ErrorMessage.Visible = true;
				MainPane.Visible = false;
				return;
			}
        	InitContentTypesFields();
            InitResources();
            GenerateCategoryToJs();
            GetCategoryContent();
			GetSideCategoryList();
        }

		/// <summary>
		/// Inits the content types fields.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>6/17/2013</datetime>
    	private void InitContentTypesFields()
    	{
			object[] parameters = new object[] { _ctContentCategoryId, _ctContentPageId, Utils.GetConnectionString(PortalId) };
			Thread thread = new Thread(new ParameterizedThreadStart(this.AddNewContentTypesFields));
			thread.Priority = ThreadPriority.Lowest;
			thread.IsBackground = true;
			thread.Start(parameters);
    	}

		/// <summary>
		/// Adds the new content types fields.
		/// </summary>
		/// <param name="args">The args.</param>
		/// <author>ducuytran</author>
		/// <datetime>6/17/2013</datetime>
		private void AddNewContentTypesFields(object args)
		{
			object[] argsArr = (object[])args;
			int ctContentCategoryId = Convert.ToInt32(argsArr[0].ToString());
			int ctContentPageId = Convert.ToInt32(argsArr[1].ToString());
			CspDataContext cspDataContext = new CspDataContext(argsArr[2].ToString());
			string ctfContentCategoryStaging = "Content_Staging";
			string ctfContentPageStaging = "Content_Staging";
			bool isAdded = false;
			if (cspDataContext == null || ctContentCategoryId <= 0 || ctContentPageId <= 0) return;
			content_types_field newCtfContentCategoryStaging;
			content_types_field newCtfContentPageStaging;
			
			if (cspDataContext.content_types_fields.FirstOrDefault(a => a.content_types_Id == ctContentCategoryId && a.fieldname == ctfContentCategoryStaging) == null)
			{
				newCtfContentCategoryStaging = new content_types_field
				                                                   	{
																		content_types_Id = ctContentCategoryId,
																		mandatory = false,
																		fieldtype = 1,
																		fieldname = ctfContentCategoryStaging,
																		@readonly = false
				                                                   	};
				cspDataContext.content_types_fields.InsertOnSubmit(newCtfContentCategoryStaging);
				isAdded = true;
			}
			if (cspDataContext.content_types_fields.FirstOrDefault(a => a.content_types_Id == ctContentPageId && a.fieldname == ctfContentPageStaging) == null)
			{
				newCtfContentPageStaging = new content_types_field
																	{
																		content_types_Id = ctContentPageId,
																		mandatory = false,
																		fieldtype = 1,
																		fieldname = ctfContentPageStaging,
																		@readonly = false
																	};
				cspDataContext.content_types_fields.InsertOnSubmit(newCtfContentPageStaging);
				isAdded = true;
			}
			if (isAdded)
				cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
		}

    	/// <summary>
		/// Inits the global variables.
		/// </summary>
		/// <author>ducuytran - 3/4/2013</author>
		private void InitGlobalVariables()
		{
			_cspUtils = new CspUtils(CspDataContext);
			_ctContentCategoryId = 0;
			_ctContentPageId = 0;
			_langId = 1;

			// check channel
			_externalIdentifier = Utils.GetSetting(PortalId, "ExternalIdentifier");
			if (string.IsNullOrEmpty(_externalIdentifier))
				_externalIdentifier = Request.Url.Host;
			if (CspDataContext.Channels.Count(a => a.ExternalIdentifier == _externalIdentifier) == 0)
				_externalIdentifier = String.Empty;

			GetThemesList();

			if (Settings.ContainsKey(Cons.SETTING_CATEGORYCONTENT_CONTENTTYPE_ID) && Settings[Cons.SETTING_CATEGORYCONTENT_CONTENTTYPE_ID].ToString() != string.Empty)
				int.TryParse(Settings[Cons.SETTING_CATEGORYCONTENT_CONTENTTYPE_ID].ToString(), out _ctContentCategoryId);

			if (Settings.ContainsKey(Cons.SETTING_PAGE_CONTENTTYPE_ID) && Settings[Cons.SETTING_PAGE_CONTENTTYPE_ID].ToString() != string.Empty)
				int.TryParse(Settings[Cons.SETTING_PAGE_CONTENTTYPE_ID].ToString(), out _ctContentPageId);

			if (Settings.ContainsKey(Cons.SETTING_MAIN_MENU_DEPTH) && Settings[Cons.SETTING_MAIN_MENU_DEPTH].ToString() != string.Empty)
				int.TryParse(Settings[Cons.SETTING_MAIN_MENU_DEPTH].ToString(), out _mainMenuDepth);

			PublishOnFly = false;
			if (Session["pofstate"] != null)
			{
				if (Session["pofstate"].ToString() == "true")
					PublishOnFly = true;
			}
			else
			{
				Session["pofstate"] = "false";
				if (Settings.ContainsKey(Cons.SETTING_PUBLISH_ON_THE_FLY) && Settings[Cons.SETTING_PUBLISH_ON_THE_FLY].ToString() == "1")
				{
					PublishOnFly = true;
					Session["pofstate"] = "true";
				}
			}

			if (Settings.ContainsKey(Cons.SETTING_PUBLICATION_SCHEME_ID) && Settings[Cons.SETTING_PUBLICATION_SCHEME_ID].ToString() != string.Empty)
				int.TryParse(Settings[Cons.SETTING_PUBLICATION_SCHEME_ID].ToString(), out _publicationSchemeId);

			if (Settings.ContainsKey(Cons.SETTING_CONTENT_STAGING_SCHEME_ID) && Settings[Cons.SETTING_CONTENT_STAGING_SCHEME_ID].ToString() != string.Empty)
				int.TryParse(Settings[Cons.SETTING_CONTENT_STAGING_SCHEME_ID].ToString(), out _contentStagingSchemeId);

			//Image URL
			if (Settings.ContainsKey(Cons.SETTING_IMAGE_URL) && Settings[Cons.SETTING_IMAGE_URL].ToString() != string.Empty)
				_staticImgUrl = Settings[Cons.SETTING_IMAGE_URL].ToString();

			//Editor upload folder
			if (Settings.ContainsKey(Cons.SETTING_UPLOAD_FOLDER) && Settings[Cons.SETTING_UPLOAD_FOLDER].ToString() != string.Empty)
			{
				_uploadFolder = Settings[Cons.SETTING_UPLOAD_FOLDER].ToString();
				if (_uploadFolder.EndsWith("/"))
					_uploadFolder = _uploadFolder.TrimEnd('/');
				if (!_uploadFolder.StartsWith("~"))
				{
					if (!_uploadFolder.StartsWith("/"))
						_uploadFolder = "/" + _uploadFolder;
					_uploadFolder = "~" + _uploadFolder;
				}
			}

			if (String.IsNullOrEmpty(_uploadFolder))
				_uploadFolder = GetLocalizedText("Var.DefaultUploadFolder");

			_externalCSSUrl = string.Format("{0}css/ShowcaseContentEntry.css", ControlPath);
			if (Settings.ContainsKey(Cons.SETTING_CSS_URL) && Settings[Cons.SETTING_CSS_URL].ToString() != string.Empty)
			{
				_externalCSSUrl = Settings[Cons.SETTING_CSS_URL].ToString();
			}

			_externalJSUrl = string.Format("{0}js/function.js", ControlPath);
			if (Settings.ContainsKey(Cons.SETTING_JS_URL) && Settings[Cons.SETTING_JS_URL].ToString() != string.Empty)
			{
				_externalJSUrl = Settings[Cons.SETTING_JS_URL].ToString();
			}

			_langCode = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
			language crrLangObj = CspDataContext.languages.SingleOrDefault(a => a.BrowserLanguageCode.ToLower() == _langCode.ToLower());
			if (crrLangObj == null)
				crrLangObj = CspDataContext.languages.SingleOrDefault(a => a.BrowserLanguageCode.ToLower() == "en-us");
			_langId = crrLangObj.languages_Id;

			category rootNode = null;
			if (_themeIds.Any())
				rootNode = CspDataContext.categories.FirstOrDefault(a => a.themes_categories.Any(b => b.themes_Id == _themeIds[0]) && a.parentId == 0);
			if (rootNode == null)
				rootNode = CspDataContext.categories.FirstOrDefault(a => a.parentId == 0 && a.active == true);
			if (rootNode != null)
				_rootCategoryId = rootNode.categoryId;

			_coParamTypes = new List<string>();
			if (!IsPostBack)
			{
				if (Settings.ContainsKey(Cons.SETTING_COMPAMY_PARAMETERS) && Settings[Cons.SETTING_COMPAMY_PARAMETERS].ToString() != string.Empty)
					_coParamTypes.AddRange(Settings[Cons.SETTING_COMPAMY_PARAMETERS].ToString().Split(','));
			}

			//Banner Image File Explorer Config
			BannerFileMan.Configuration.EnableAsyncUpload = true;
			BannerFileMan.ExplorerMode = FileExplorerMode.Thumbnails;
			BannerFileMan.Configuration.SearchPatterns = new string[] { "*.jpg", "*.jpeg", "*.gif", "*.png", "*.pdf", "*.docx", "*.doc", "*.zip" };
			BannerFileMan.Configuration.MaxUploadFileSize = 10240000;
			BannerFileMan.Configuration.UploadPaths = BannerFileMan.Configuration.ViewPaths = BannerFileMan.Configuration.DeletePaths = new[] { _uploadFolder };
		}

		public void ExternalInitResources(int portalId)
		{
			_cspUtils = new CspUtils(CspDataContext);
			_ctContentCategoryId = 0;
			_ctContentPageId = 0;
			_langId = 1;

			// check channel
			_externalIdentifier = Utils.GetSetting(portalId, "ExternalIdentifier");

			GetThemesList();
		}

		private void GetThemesList()
		{
			if (_externalIdentifier == String.Empty)
				return;
			List<Channel> channelList = CspDataContext.Channels.Where(a => a.ExternalIdentifier == _externalIdentifier).ToList();
			List<ChannelThemeAssocation> channelThemeAssocations = new List<ChannelThemeAssocation>();
			foreach (Channel channel in channelList)
			{
				channelThemeAssocations.AddRange(CspDataContext.ChannelThemeAssocations.Where(a => a.ChannelId == channel.Id));
			}
			foreach (ChannelThemeAssocation channelThemeAssocation in channelThemeAssocations)
			{
				theme tmp = CspDataContext.themes.FirstOrDefault(a => a.themes_Id == channelThemeAssocation.ThemeId && a.active);
				if (tmp != null)
					_themeIds.Add(tmp.themes_Id);
			}
		}

		/// <summary>
		/// Handles the ItemDataBound event of the RadListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Telerik.Web.UI.RadListViewItemEventArgs"/> instance containing the event data.</param>
		/// <author>ducuytran - 3/12/2013</author>
		protected void RadListView_ItemDataBound(object sender, RadListViewItemEventArgs e)
		{
			if (IsPostBack)
				return;
			string[] uploadFolder = new string[] { _uploadFolder };
			if (e.Item is RadListViewDataItem)
			{
				RadEditor theEditor = e.Item.FindControl("ContentPageEditor") as RadEditor;
				theEditor.ImageManager.ViewPaths = uploadFolder;
				theEditor.ImageManager.UploadPaths = uploadFolder;
				theEditor.ImageManager.DeletePaths = uploadFolder;
				theEditor.ImageManager.MaxUploadFileSize = 10240000;

				theEditor.DocumentManager.ViewPaths = uploadFolder;
				theEditor.DocumentManager.UploadPaths = uploadFolder;
				theEditor.DocumentManager.DeletePaths = uploadFolder;
				theEditor.DocumentManager.MaxUploadFileSize = 10240000;

				theEditor.FlashManager.ViewPaths = uploadFolder;
				theEditor.FlashManager.UploadPaths = uploadFolder;
				theEditor.FlashManager.DeletePaths = uploadFolder;
				theEditor.FlashManager.MaxUploadFileSize = 10240000;

				theEditor.TemplateManager.ViewPaths = uploadFolder;
				theEditor.TemplateManager.UploadPaths = uploadFolder;
				theEditor.TemplateManager.DeletePaths = uploadFolder;
				theEditor.TemplateManager.MaxUploadFileSize = 10240000;

				theEditor.MediaManager.ViewPaths = uploadFolder;
				theEditor.MediaManager.UploadPaths = uploadFolder;
				theEditor.MediaManager.DeletePaths = uploadFolder;
				theEditor.MediaManager.MaxUploadFileSize = 10240000;
				

				theEditor.ToolsFile = ControlPath + "EditorToolSet.xml";

				//add a new Toolbar dynamically
				EditorToolGroup dynamicToolbar = new EditorToolGroup();
				theEditor.Tools.Add(dynamicToolbar);

				//add a custom dropdown and set its items and dimension attributes
				EditorDropDown ddn = new EditorDropDown("CoParametersDropDown");
				ddn.Text = GetLocalizedText("label.CopParametersDropDown");

				//Set the popup width and height
				ddn.Attributes["width"] = "160px";
				ddn.Attributes["popupwidth"] = "240px";
				ddn.Attributes["popupheight"] = "100px";

				if (_coParamTypes != null && _coParamTypes.Count > 0)
				{
					foreach (String companiesParameterType in _coParamTypes)
					{
						ddn.Items.Add(companiesParameterType, companiesParameterType);
					}
				}

				//Add button category link
				EditorTool btnCategoryLink = new EditorTool();
				btnCategoryLink.Text = "Add Category Link";
				btnCategoryLink.Name = "AddCategoryLink";

				EditorTool btnDocEmbed = new EditorTool();
				btnDocEmbed.Text = "Add Document Link";
				btnDocEmbed.Name = "AddDocLink";
				
				
				//Add tool to toolbar
				dynamicToolbar.Tools.Add(ddn);
				dynamicToolbar.Tools.Add(btnCategoryLink);
				dynamicToolbar.Tools.Add(btnDocEmbed);
			}
		}

        /// <summary>
        /// Inits the resources.
        /// </summary>
        /// <author>ducuytran - 2/22/2013</author>
        private void InitResources()
        {
			//Import CSS files
            Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel='stylesheet' href='{0}css/jquery.contextmenu.css' type='text/css' />", ControlPath)));
            Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel='stylesheet' href='{0}css/kendo.common.min.css' type='text/css' />", ControlPath)));
            Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel='stylesheet' href='{0}css/kendo.default.min.css' type='text/css' />", ControlPath)));
			Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel='stylesheet' href='{0}css/jquery.qtip.min.css' type='text/css' />", ControlPath)));
			if (_externalCSSUrl.IndexOf(',') == -1)
				Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel='stylesheet' href='{0}' type='text/css' />", _externalCSSUrl)));
			else
			{
				foreach (string cssUrl in _externalCSSUrl.Split(','))
				{
					Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel='stylesheet' href='{0}' type='text/css' />", cssUrl)));
				}
			}

			//Import JS files
			Page.ClientScript.RegisterClientScriptInclude("jquery-ui-position", ControlPath + "js/jquery.ui.position.js");
            Page.ClientScript.RegisterClientScriptInclude("jquery-contextmenu", ControlPath + "js/jquery.contextmenu.js");
            Page.ClientScript.RegisterClientScriptInclude("kendoui", ControlPath + "js/kendo.web.min.js");
			Page.ClientScript.RegisterClientScriptInclude("qtip", ControlPath + "js/jquery.qtip.min.js");
			Page.ClientScript.RegisterClientScriptInclude("showcase-content-entry", ControlPath + "js/showcase.content.entry.js");
			if (_externalJSUrl.IndexOf(',') == -1)
				Page.ClientScript.RegisterClientScriptInclude("function", _externalJSUrl);
			else
			{
				foreach (string jsUrl in _externalJSUrl.Split(','))
				{
					string jsFileName = jsUrl.Split('/').Last();
					Page.ClientScript.RegisterClientScriptInclude(jsFileName, jsUrl);
				}
			}

			//Button text
        	btnSaveCategory.Text = GetLocalizedText("btn.Save");
        	btnCancelCategory.Text = GetLocalizedText("btn.Cancel");
			btnSaveCategoryDescription.Text = GetLocalizedText("btn.Save");
			btnCancelDescription.Text = GetLocalizedText("btn.Cancel");
        	btnCancelDetele.Text = GetLocalizedText("btn.Cancel");
        	btnDeleteAll.Text = GetLocalizedText("btn.DeleteAtAllLanguage");
        	btnDeleteByLanguage.Text = GetLocalizedText("btn.DeleteByLanguage");
			btnOpenCategoryThumbnail.Text = GetLocalizedText("btn.Browse");
			btnBrowseNewThumbnail.Text = GetLocalizedText("btn.Browse");
        	btnSaveBannerUrl.Text = GetLocalizedText("btn.Save");
        	btnCloseBannerUrlWD.Text = GetLocalizedText("btn.Cancel");
        }

        /// <summary>
        /// Generates the category to JS.
        /// </summary>
        /// <author>ducuytran - 2/22/2013</author>
		public void GenerateCategoryToJs()
        {
			CustomCategoryNodes = new List<CustomCategoryNode>();
			List<category> uncheckedCategoryList = new List<category>();
        	List<category> rawCategoryList = new List<category>();
			List<Category> categoryTree = new List<Category>();

        	category rootNode =
				CspDataContext.categories.FirstOrDefault(a => a.categoryId == _rootCategoryId);
			categoryTree.Add(new Category
			                 	{
			                 		Id = _rootCategoryId,
									ParentId = 0,
									DisplayOrder = 1,
									Text = rootNode.categoryText
			                 	});
        	foreach (int themeId in _themeIds)
        	{
				if (!PublishOnFly || !String.IsNullOrEmpty(Request.Params["showall"]))
				{
					uncheckedCategoryList.AddRange(
						CspDataContext.categories.Where(a => a.themes_categories.Any(b => b.themes_Id == themeId) && a.depth <= _mainMenuDepth && a.parentId > 0).
							OrderBy(a => a.DisplayOrder).
							ThenByDescending(a => a.categoryId).ToList());
				}
				else
				{
					uncheckedCategoryList.AddRange(
						CspDataContext.categories.Where(a => a.themes_categories.Any(b => b.themes_Id == themeId) && a.depth <= _mainMenuDepth && a.parentId > 0 && a.active == true).
							OrderBy(a => a.DisplayOrder).
							ThenByDescending(a => a.categoryId).ToList());
				}
        	}

			if (!String.IsNullOrEmpty(Request.Params["cat"]))
			{
				List<category> relatedList = GetRelatedCategories(Convert.ToInt32(Request.Params["cat"]));
				if (relatedList != null && relatedList.Any())
					uncheckedCategoryList.AddRange(relatedList);
			}

        	foreach (category cateItem in uncheckedCategoryList)
        	{
        		CustomCategoryNode newNode = new CustomCategoryNode(cateItem);
        		content ContentCategory = GetContentByStaging(cateItem, _ctContentCategoryId);
				content ContentPage = GetContentByStaging(cateItem, _ctContentPageId);
				CustomCategoryNode ccParentNode = CustomCategoryNodes.FirstOrDefault(a => a.Id == cateItem.parentId);
				bool isPublished = false;
				bool isActive = (bool)cateItem.active;
        		
        		if (PublishOnFly)
        		{
					if (ContentCategory != null && ContentPage != null && ContentCategory.stage_Id == STAGE_PUBLISHED && ContentPage.stage_Id == STAGE_PUBLISHED)
						isPublished = true;
        		}
        		else
        		{
        			CSPShowcase_Staging lastStaging = GetLatestStagingContent(cateItem.categoryId);
        			bool getStatusFromOriginal = false;
        			if (lastStaging != null)
        			{
						ccParentNode = CustomCategoryNodes.FirstOrDefault(a => a.Id == lastStaging.ParentCategoryId);
						
        				switch (lastStaging.Status)
        				{
        					case (int) LogAction.GoActive:
        						isActive = true;
        						isPublished = true;
								break;
        					case (int) LogAction.GoInactive:
        						isActive = false;
								break;
							case (int) LogAction.GoPublish:
        						isPublished = true;
								break;
							case (int) LogAction.GoUnpublish:
        						isPublished = false;
								break;
							default:
        						getStatusFromOriginal = true;
        						break;
        				}
        			}
        			else
        			{
        				getStatusFromOriginal = true;
        			}

					if (getStatusFromOriginal)
					{
						content srcContentCategory = GetContentByStaging(cateItem, _ctContentCategoryId, true);
						content srcContentPage = GetContentByStaging(cateItem, _ctContentPageId, true);
						if ((srcContentCategory != null && srcContentPage != null))
						{
							if ((srcContentCategory.stage_Id == STAGE_PUBLISHED && srcContentPage.stage_Id == STAGE_PUBLISHED) ||
							    (!String.IsNullOrEmpty(_cspUtils.GetContentFieldValue(srcContentCategory, CTF_CATEGORYCONTENT_STAGING)) ||
							     !String.IsNullOrEmpty(_cspUtils.GetContentFieldValue(srcContentPage, CTF_CONTENTPAGE_STAGING))))
								isPublished = true;
						}
					}

					//Set status according to parent's
					if (!String.IsNullOrEmpty(Request.Params["showall"]) && isActive && isPublished)
					{
						if (ccParentNode != null)
						{
							isActive = ccParentNode.Active;
							isPublished = ccParentNode.StagePublished;
						}
						else
						{
							CSPShowcase_Staging parentStaging = GetLatestStagingContent((int) cateItem.parentId);
							if (parentStaging != null)
							{
								switch (parentStaging.Status)
								{
									case (int)LogAction.GoInactive:
										isActive = false;
										break;
									case (int)LogAction.GoUnpublish:
										isPublished = false;
										break;
								}
							}
							else
							{
								category parentCategory = CspDataContext.categories.FirstOrDefault(a => a.categoryId == cateItem.parentId);
								content srcParentContentCategory = GetContentByStaging(parentCategory, _ctContentCategoryId, true);
								content srcParentContentPage = GetContentByStaging(parentCategory, _ctContentPageId, true);
								isActive = (bool) parentCategory.active;
								if (srcParentContentCategory != null && srcParentContentPage != null &&
									(srcParentContentCategory.stage_Id != STAGE_PUBLISHED || srcParentContentPage.stage_Id != STAGE_PUBLISHED))
									isPublished = false;
							}
						}
					}
        		}

				if (String.IsNullOrEmpty(Request.Params["showall"]) && (!isPublished || !isActive))
				{
					continue;
				}

        		string SSOrder = "0";
				if (PublishOnFly)
					SSOrder = (ContentCategory != null) ? _cspUtils.GetContentFieldValue(ContentCategory, "Status_Sort_Order") : ((int)cateItem.DisplayOrder).ToString("0000");
				else
				{
					String DisplayOrderList = "";
					if (_originalDisplayOrder.Any(a => a.Key == cateItem.parentId.ToString()))
						DisplayOrderList = _originalDisplayOrder[cateItem.parentId.ToString()];
					else
					{
						DisplayOrderList = GetDisplayOrderList((int)cateItem.parentId, 0);
						_originalDisplayOrder.Add(cateItem.parentId.ToString(), DisplayOrderList);
					}
					SSOrder = (DisplayOrderList.Split('-').ToList().IndexOf(cateItem.categoryId.ToString()) + 1).ToString("0000");
				}
				string Description = (ContentCategory != null) ? _cspUtils.GetContentFieldValue(ContentCategory, "Content_Description_Short") : String.Empty;
				Description = HttpUtility.HtmlEncode(Description);
				Description = Description.Replace("\n", "<br />");
				Description = Description.Replace("\r", "<br />");
				
        		newNode.CategoryContent = ContentCategory;
        		newNode.ContentPage = ContentPage;
        		newNode.StatusSortOrder = SSOrder;
        		int tempInt = 0;
        		newNode.DisplayOrder = int.TryParse(SSOrder, out tempInt) ? tempInt : 0;
				newNode.Lineage = ccParentNode == null ? cateItem.lineage : ccParentNode.Lineage + cateItem.categoryId + "/";
				newNode.Text = (ContentCategory != null) ? _cspUtils.GetContentFieldValue(ContentCategory, "Content_Title") : cateItem.categoryText;
				newNode.Description = Description;
				newNode.Thumbnail = (ContentCategory != null) ? _cspUtils.GetContentFieldValue(ContentCategory, "Content_Image_Thumbnail") : String.Empty;
				newNode.CategoryContentId = (ContentCategory != null) ? ContentCategory.content_Id : Guid.Empty;
				newNode.ContentPageId = (ContentPage != null) ? ContentPage.content_Id : Guid.Empty;
        		newNode.StagePublished = isPublished;
        		newNode.Active = isActive;

				CustomCategoryNodes.Add(newNode);

				categoryTree.Add(new Category
				{
					Id = cateItem.categoryId,
					ParentId = (int) (ccParentNode == null ? cateItem.parentId : ccParentNode.Id),
					Text = newNode.Text,
					DisplayOrder = newNode.DisplayOrder
				});
        	}
			
        	rptList.DataSource = CustomCategoryNodes;
            rptList.DataBind();

			CategoryTree.DataSource = categoryTree;
			CategoryTree.DataBind();
        	
        }

		/// <summary>
		/// Gets the related categories.
		/// </summary>
		/// <param name="cateId">The cate id.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>7/10/2013</datetime>
    	private List<category> GetRelatedCategories(int cateId)
    	{
    		List<category> relatedList = new List<category>();
    		category currentNode = CspDataContext.categories.FirstOrDefault(a => a.categoryId == cateId);
			if (currentNode == null || currentNode.depth < _mainMenuDepth)
				return null;
			if (currentNode.depth > _mainMenuDepth)
			{
				category parentNode = CspDataContext.categories.FirstOrDefault(a => a.categoryId == currentNode.parentId);
				while (parentNode != null && parentNode.depth > _mainMenuDepth)
				{
					relatedList.Add(parentNode);
					parentNode = CspDataContext.categories.FirstOrDefault(a => a.categoryId == parentNode.parentId);
				}
				relatedList.Reverse();
				relatedList.Add(currentNode);
			}
    		List<category> childrenList = CspDataContext.categories.Where(a => a.parentId == currentNode.categoryId).ToList();
			if (childrenList.Any())
				relatedList.AddRange(childrenList);
    		return relatedList;
    	}

    	/// <summary>
    	/// Gets the content by staging.
    	/// </summary>
    	/// <param name="cateItem">The cate item.</param>
    	/// <param name="ctId">The ct id.</param>
    	/// <param name="justGetSourceContent"> </param>
    	/// <returns></returns>
    	/// <author>ducuytran</author>
    	/// <datetime>7/4/2013</datetime>
    	private content GetContentByStaging(category cateItem, int ctId, bool justGetSourceContent = false)
    	{
			content theContent = null;
			if (PublishOnFly || justGetSourceContent)
			{
				theContent = CspDataContext.contents.FirstOrDefault(a =>
								a.content_main.content_categories.Any(b => b.category_Id == cateItem.categoryId) &&
								a.content_main.content_types_Id == ctId &&
								a.content_types_languages_Id == _langId && a.stage_Id != STAGE_STAGING);
				if (theContent == null && justGetSourceContent)
				{
					CSPShowcase_Staging latestStagingItem = GetLatestStagingContent(cateItem.categoryId);
					if (latestStagingItem != null)
					{
						if (ctId == _ctContentCategoryId)
							theContent = CspDataContext.contents.FirstOrDefault(a => a.content_Id == latestStagingItem.SourceContentCategoryId);
						else
							theContent = CspDataContext.contents.FirstOrDefault(a => a.content_Id == latestStagingItem.SourceContentPageId);
					}
				}
			}
			else if (!PublishOnFly)
			{
				CSPShowcase_Staging latestStagingItem = GetLatestStagingContent(cateItem.categoryId);
				if (latestStagingItem != null)
				{
					if (ctId == _ctContentCategoryId)
						theContent = CspDataContext.contents.FirstOrDefault(a => a.content_Id == (latestStagingItem.ContentCategoryId != Guid.Empty ? latestStagingItem.ContentCategoryId : latestStagingItem.SourceContentCategoryId));
					else
						theContent = CspDataContext.contents.FirstOrDefault(a => a.content_Id == (latestStagingItem.ContentPageId != Guid.Empty ? latestStagingItem.ContentPageId : latestStagingItem.SourceContentPageId));
				}
				else
				{
					theContent = CspDataContext.contents.FirstOrDefault(a =>
								a.content_main.content_categories.Any(b => b.category_Id == cateItem.categoryId) &&
								a.content_main.content_types_Id == ctId &&
								a.content_types_languages_Id == _langId && a.stage_Id != STAGE_STAGING);
				}
			}

    		return theContent;
    	}

    	/// <summary>
        /// Goes the ajax action.
        /// </summary>
        /// <author>ducuytran - 2/27/2013</author>
        private void GoAjaxAction()
        {
            Response.ContentType = "text/xml";
            int resultCode = 0;
        	string resultString = "";
        	int categoryId = 0;
        	category cat;
			if (!String.IsNullOrEmpty(Request.Params["categoryid"]))
				int.TryParse(Request.Params["categoryid"], out categoryId);

			if (CreatedInStagingDetection(categoryId))
				return;

			switch (Request.Params["ajaxaction"])
			{
				case "delete_category":
					resultCode = SetCategoryStatus(categoryId, 0);
					CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
					break;
				case "unpublish_category":
					resultCode = SetContentsStatus(categoryId, 0);
					CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
					break;
				case "publish_category":
					resultCode = SetContentsStatus(categoryId, 1);
					PublishParentNodes(categoryId);
					CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
					break;
				case "activate_category":
					resultCode = SetCategoryStatus(categoryId, 1);
					category targetCategory = CspDataContext.categories.FirstOrDefault(a => a.categoryId == categoryId);
					if (targetCategory.parentId != _rootCategoryId)
						ActivateParentNodes((int)targetCategory.parentId);
					CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
					break;
				case "rename_category":
					resultString = RenameCategory(categoryId, Request.Params["categoryname"], Request.Params["contentcategoryid"]);
					break;
				case "save_contentpage":
					//resultCode = SaveContentPage();
					resultString = SaveContentPage();
					break;
				case "save_category":
					bool isSubscribable = !(Request.Params["issubscribable"] == "0");
					int displayOrder = GetLastOrder(Convert.ToInt32(Request.Params["parentid"]));
					displayOrder++;
					resultCode = AddCategory(Convert.ToInt32(Request.Params["parentid"]), Request.Params["categorytext"], Request.Params["categorydesc"], Request.Params["categorythumbnail"], true,
											 isSubscribable, displayOrder);
					if (resultCode > 0)
					{
						category recentCat = CspDataContext.categories.FirstOrDefault(a => a.categoryId == resultCode);
						resultString = String.Format("<xmlresult><category><id>{0}</id><title>{1}</title><level>{2}</level></category></xmlresult>", recentCat.categoryId, recentCat.categoryText, recentCat.depth);
						resultCode = 1;
					}
					break;
				case "order_category":
					resultCode = ReorderCategory();
					break;
				case "add_content":
					resultCode = AddMissingContent(categoryId);
					break;
				case "add_missing_contents":
					resultCode = AddMissingContents();
					break;
				case "change_subscribe_status":
					cat = CspDataContext.categories.FirstOrDefault(a => a.categoryId == categoryId);
					cat.IsSubscribable = Request.Params["status"] != "0";
					if (Request.Params["status"] == "0")
						ChangeCategorySubscribeStatus(cat.categoryId, (bool)cat.IsSubscribable);
					CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
					resultCode = 1;
					break;
				case "save_category_description":
					resultCode = UpdateCategoryDescription(categoryId, Request.Params["categorydesc"].ToString(), Request.Params["categorythumbnail"].ToString());
					break;
				case "pof_state":
					Session["pofstate"] = Request.Params["pofstate"];
					if (Request.Params["pofstate"] == "true")
						PublishOnFly = true;
					break;
				case "broadcast":
					resultCode = GoBroadcastPublish();
					GoBroadcastPublishOriginal();
					break;
				case "rollback":
					resultCode = GoRollback();
					break;
				case "synccheckpoint":
					resultCode = GoSyncCheckpoint();
					break;
				case "getrollbacklist":
					resultString = GetPublishCheckpoints();
					break;
				case "checkpointdetail":
					resultString = GetCheckpointDetail(new Guid(Request.Params["checkpointid"]));
					break;
				default:
					resultCode = 1;
					break;
			}
            string strData = "<xmlresult><resultcode>" + resultCode + "</resultcode></xmlresult>";
			if (!String.IsNullOrEmpty(resultString))
				strData = resultString;
            Response.Write(strData);
            Response.Flush();
            Response.End();
        }

		/// <summary>
		/// Goes the broadcast publish original.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>7/10/2013</datetime>
    	private void GoBroadcastPublishOriginal()
    	{
    		List<CSPShowcase_Staging> originalList =
    			DnnData.CSPShowcase_Stagings.Where(
					a => a.IsOriginalVersion == true && a.PortalId == PortalId && a.TabModuleId == TabModuleId).ToList();
			List<CSPShowcase_Staging> originalToPublish = new List<CSPShowcase_Staging>();
    		foreach (CSPShowcase_Staging cspShowcaseStaging in originalList)
    		{
    			CSPShowcase_Staging lastStaging = GetLatestStagingContent(cspShowcaseStaging.CategoryId);
				if (lastStaging == cspShowcaseStaging)
					originalToPublish.Add(cspShowcaseStaging);
    		}
    		originalToPublish = originalToPublish.OrderByDescending(a => a.DateTime).ToList();
    		DoPublish(originalToPublish);
    	}

    	/// <summary>
		/// Goes the sync checkpoint.
		/// </summary>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>7/9/2013</datetime>
    	private int GoSyncCheckpoint()
    	{
			int resultCode = 0;
			bool isReset = Request.Params["resetall"] == "true";
			Guid checkpointId = new Guid(Request.Params["checkpointid"]);

			CSPShowcase_Checkpoint targetCheckpoint =
				DnnData.CSPShowcase_Checkpoints.FirstOrDefault(a => a.CheckpointId == checkpointId);
			if (targetCheckpoint == null)
				return resultCode;
			List<CSPShowcase_Checkpoint> checkpointList =
				DnnData.CSPShowcase_Checkpoints.Where(a => a.PortalId == PortalId && a.TabModuleId == TabModuleId).OrderBy(
					a => a.DateTime).ToList();
			CSPShowcase_Checkpoint firstCheckpoint = checkpointList.First();
			CSPShowcase_Checkpoint lastCheckpoint = checkpointList.Last();
			
			List<CSPShowcase_Staging> stagingToRemove =
				DnnData.CSPShowcase_Stagings.Where(a => a.DateTime > targetCheckpoint.DateTime && (a.IsOriginalVersion == false || a.IsOriginalVersion == null) && 
													a.PortalId == PortalId && a.TabModuleId == TabModuleId).OrderByDescending(a => a.DateTime).ToList();

			List<CSPShowcase_Checkpoint> checkpointToRemove =
				checkpointList.Where(a => a.DateTime > targetCheckpoint.DateTime).ToList();

			if (isReset)
			{
				checkpointToRemove.Add(targetCheckpoint);
				stagingToRemove = DnnData.CSPShowcase_Stagings.Where(a => (a.IsOriginalVersion == false || a.IsOriginalVersion == null) && 
																	a.PortalId == PortalId && a.TabModuleId == TabModuleId).OrderByDescending(a => a.DateTime).ToList();
			}

			if (!stagingToRemove.Any())
				return resultCode;

			//Get categories, content_categories, content_main, content_fields & contents to remove
			List<category> categoriesToRemove = new List<category>();
			List<category> categoriesEffected = new List<category>();
			List<content_category> theContentsCategoriesToRemove = new List<content_category>();
			List<content_main> theContentMainToRemove = new List<content_main>();
			List<content_field> fieldsCreatedInStaging = new List<content_field>();
			List<content> contentCreatedInStaging = new List<content>();
			foreach (CSPShowcase_Staging cspShowcaseStaging in stagingToRemove)
			{
				if (cspShowcaseStaging.ActionId == (int)LogAction.Created)
				{
					if (!categoriesToRemove.Any(a => a.categoryId == cspShowcaseStaging.CategoryId))
					{
						category newCategoryCreatedInStaging =
							CspDataContext.categories.FirstOrDefault(a => a.categoryId == cspShowcaseStaging.CategoryId);
						if (newCategoryCreatedInStaging != null)
						{
							categoriesToRemove.Add(newCategoryCreatedInStaging);
							content_category ccCreatedInStaging =
								CspDataContext.content_categories.FirstOrDefault(a => a.category_Id == cspShowcaseStaging.CategoryId);
							if (!theContentsCategoriesToRemove.Any(a => a.content_categories_Id == ccCreatedInStaging.content_categories_Id))
							{
								theContentsCategoriesToRemove.Add(ccCreatedInStaging);
								theContentMainToRemove.Add(CspDataContext.content_mains.FirstOrDefault(a => a.content_categories.Any(b => b.content_categories_Id == ccCreatedInStaging.content_categories_Id)));
							}
						}
					}

					if (!contentCreatedInStaging.Any(a => a.content_Id == cspShowcaseStaging.SourceContentCategoryId))
					{
						List<content_field> newFieldsCreatedInStaging =
							CspDataContext.content_fields.Where(a => a.content_Id == cspShowcaseStaging.SourceContentCategoryId ||
							                                         a.content_Id == cspShowcaseStaging.SourceContentPageId).ToList();
						List<content> newContentCreatedInStaging =
							CspDataContext.contents.Where(a => a.content_Id == cspShowcaseStaging.SourceContentCategoryId ||
							                                   a.content_Id == cspShowcaseStaging.SourceContentPageId).ToList();
						if (newContentCreatedInStaging.Any())
						{
							contentCreatedInStaging.AddRange(newContentCreatedInStaging);
							if (newFieldsCreatedInStaging.Any())
								fieldsCreatedInStaging.AddRange(newFieldsCreatedInStaging);
						}
					}
				}
				else
				{
					if (!categoriesEffected.Any(a => a.categoryId == cspShowcaseStaging.CategoryId))
						categoriesEffected.Add(CspDataContext.categories.FirstOrDefault(a => a.categoryId == cspShowcaseStaging.CategoryId));
				}

				if (cspShowcaseStaging.ContentCategoryId != Guid.Empty && !contentCreatedInStaging.Any(a => a.content_Id == cspShowcaseStaging.ContentCategoryId))
				{
					List<content_field> contentCategoryFields =
						CspDataContext.content_fields.Where(a => a.content_Id == cspShowcaseStaging.ContentCategoryId).ToList();
					
					content stagingContentCategory =
						CspDataContext.contents.FirstOrDefault(a => a.content_Id == cspShowcaseStaging.ContentCategoryId);
					if (stagingContentCategory != null)
					{
						contentCreatedInStaging.Add(stagingContentCategory);
						if (contentCategoryFields.Any())
							fieldsCreatedInStaging.AddRange(contentCategoryFields);
					}
				}

				if (cspShowcaseStaging.ContentPageId != Guid.Empty && !contentCreatedInStaging.Any(a => a.content_Id == cspShowcaseStaging.ContentPageId))
				{
					List<content_field> contentPageFields =
						CspDataContext.content_fields.Where(a => a.content_Id == cspShowcaseStaging.ContentPageId).ToList();

					content stagingContentPage =
						CspDataContext.contents.FirstOrDefault(a => a.content_Id == cspShowcaseStaging.ContentPageId);
					if (stagingContentPage != null)
					{
						contentCreatedInStaging.Add(stagingContentPage);
						if (contentPageFields.Any())
							fieldsCreatedInStaging.AddRange(contentPageFields);
					}
				}
			}

			if (categoriesToRemove.Any())
			{
				CspDataContext.content_categories.DeleteAllOnSubmit(theContentsCategoriesToRemove);
				CspDataContext.content_mains.DeleteAllOnSubmit(theContentMainToRemove);
				CspDataContext.categories.DeleteAllOnSubmit(categoriesToRemove);
			}

			if (contentCreatedInStaging.Any() && fieldsCreatedInStaging.Any())
			{
				CspDataContext.content_fields.DeleteAllOnSubmit(fieldsCreatedInStaging);
				CspDataContext.contents.DeleteAllOnSubmit(contentCreatedInStaging);
			}

			//Get checkpoint detail to remove
			List<CSPShowcase_CheckpointDetail> checkpointDetailToRemove = new List<CSPShowcase_CheckpointDetail>();
			foreach (CSPShowcase_Checkpoint cspShowcaseCheckpoint in checkpointToRemove)
			{
				checkpointDetailToRemove.AddRange(DnnData.CSPShowcase_CheckpointDetails.Where(a => a.CSPShowcase_Checkpoint == cspShowcaseCheckpoint));
			}
			if (checkpointDetailToRemove.Any())
			{
				DnnData.CSPShowcase_CheckpointDetails.DeleteAllOnSubmit(checkpointDetailToRemove);
				DnnData.CSPShowcase_Checkpoints.DeleteAllOnSubmit(checkpointToRemove);
				DnnData.CSPShowcase_Stagings.DeleteAllOnSubmit(stagingToRemove);
			}

			/*if (isReset)
			{
				foreach (category cateItem in categoriesEffected)
				{
					List<CSPShowcase_Staging> orginigalStaging =
						DnnData.CSPShowcase_Stagings.Where(
							a =>
							a.CategoryId == cateItem.categoryId && a.IsOriginalVersion == true && a.PortalId == PortalId &&
							a.TabModuleId == TabModuleId).ToList();
					foreach (CSPShowcase_Staging cspShowcaseStaging in orginigalStaging)
					{
						cspShowcaseStaging.Published = true;
						cspShowcaseStaging.IsBackupVersion = false;
					}
				}
			}*/

			CspDataContext.SubmitChanges();
			DnnData.SubmitChanges();
			resultCode = 1;

			return resultCode;
    	}

    	/// <summary>
		/// Gets the checkpoint detail.
		/// </summary>
		/// <param name="guid">The GUID.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>7/7/2013</datetime>
    	private string GetCheckpointDetail(Guid checkpointId)
		{
			string checkpointDetail = "<xmlresult><actionlist>{0}</actionlist></xmlresult>";
			string actionTemplate = "<action><category>{0}</category><detail>{1}</detail><datetime>{2}</datetime></action>";
			string actionListDetail = "";
			List<CSPShowcase_Staging> stagingList =
				DnnData.CSPShowcase_Stagings.Where(
					a => a.CSPShowcase_CheckpointDetails.Any(b => b.CheckpointId == checkpointId && b.IsBackupVersion == false)).
					OrderBy(a => a.DateTime).ToList();
			foreach (CSPShowcase_Staging cspShowcaseStaging in stagingList)
			{
				category theCategory = CspDataContext.categories.FirstOrDefault(a => a.categoryId == cspShowcaseStaging.CategoryId);
				content contentCategory = GetContentByStaging(theCategory, _ctContentCategoryId, true);
				string categoryText = string.Format("{0} ({1})", SecurityElement.Escape(_cspUtils.GetContentFieldValue(contentCategory, "Content_Title")), SecurityElement.Escape(theCategory.categoryId.ToString()));
				string actionInfo = GetActionString((int)cspShowcaseStaging.ActionId) + "-" + cspShowcaseStaging.ActionComment;
				actionListDetail += string.Format(actionTemplate, categoryText, SecurityElement.Escape(actionInfo), SecurityElement.Escape(cspShowcaseStaging.DateTime.ToString()));
			}
			//checkpointDetail = String.Format(checkpointDetail, actionListDetail);
			return String.Format(checkpointDetail, actionListDetail);
		}

		/// <summary>
		/// Gets the publish checkpoints.
		/// </summary>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>7/7/2013</datetime>
    	private string GetPublishCheckpoints()
    	{
    		string strData = "<xmlresult><resultcode>{0}</resultcode></xmlresult>";
    		string checkpointListStr = "";
			List<CSPShowcase_Checkpoint> checkpointList = DnnData.CSPShowcase_Checkpoints.Where(a => a.PortalId == PortalId && a.TabModuleId == TabModuleId).OrderByDescending(a => a.DateTime).ToList();
    		foreach (CSPShowcase_Checkpoint cspShowcaseCheckpoint in checkpointList)
    		{
				string checkpointTemplate = "<checkpoint><id>{0}</id><datetime>{1}</datetime><comment>{2}</comment><detail>{3}</detail></checkpoint>";
    			checkpointListStr += string.Format(checkpointTemplate, cspShowcaseCheckpoint.CheckpointId, cspShowcaseCheckpoint.DateTime, cspShowcaseCheckpoint.Comment, String.Empty);
    		}
    		strData = string.Format(strData, checkpointListStr);
    		return strData;
    	}

    	/// <summary>
		/// Createds the in staging detection.
		/// </summary>
		/// <param name="categoryId">The category id.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>6/29/2013</datetime>
    	private bool CreatedInStagingDetection(int categoryId)
    	{
			if (Request.Params["ajaxaction"] == "delete_category" || Request.Params["ajaxaction"] == "unpublish_category" || Request.Params["ajaxaction"] == "publish_category")
			{
				List<CSPShowcase_Staging> stagingList =
					DnnData.CSPShowcase_Stagings.Where(a => a.CategoryId == categoryId && a.LanguageId == _langId &&
														(a.IsBackupVersion == false || a.IsBackupVersion == null) &&
														(a.IsOriginalVersion == false || a.IsOriginalVersion == null) &&
														a.PortalId == PortalId && a.TabModuleId == TabModuleId).OrderByDescending(
						a => a.DateTime).ToList();
				if (stagingList.Any())
				{
					CSPShowcase_Staging latestStagingItem = stagingList.FirstOrDefault();
					content sourceContentCategory =
						CspDataContext.contents.FirstOrDefault(a => a.content_Id == latestStagingItem.SourceContentCategoryId);
					content sourceContentPage =
						CspDataContext.contents.FirstOrDefault(a => a.content_Id == latestStagingItem.SourceContentPageId);
					if (!String.IsNullOrEmpty(_cspUtils.GetContentFieldValue(sourceContentCategory, CTF_CATEGORYCONTENT_STAGING)) ||
					    !String.IsNullOrEmpty(_cspUtils.GetContentFieldValue(sourceContentPage, CTF_CONTENTPAGE_STAGING)))
					{
						string strData = "<xmlresult><resultcode>" + GetLocalizedText("Msg.CreatedInStaging") + "</resultcode></xmlresult>";
						Response.Write(strData);
						Response.Flush();
						Response.End();
						return true;
					}
				}
			}
    		return false;
    	}

    	/// <summary>
		/// Goes the broadcast publish.
		/// </summary>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>6/29/2013</datetime>
    	private int GoBroadcastPublish()
    	{
    		List<CSPShowcase_Staging> allStagingList =
    			DnnData.CSPShowcase_Stagings.Where(a => (a.IsBackupVersion == false || a.IsBackupVersion == null) && (a.IsOriginalVersion == false || a.IsOriginalVersion == null) &&
													a.PortalId == PortalId && a.TabModuleId == TabModuleId).OrderByDescending(a => a.DateTime).ToList();
			if (!allStagingList.Any() || allStagingList.First().Published)
				return 2;
			
			_isRepublish = DnnData.CSPShowcase_CheckpointDetails.FirstOrDefault(a => a.CSPShowcase_Staging == allStagingList.First()) != null;

    		DateTime checkPoint = DateTime.Now;
			List<CSPShowcase_Staging> stagingByCategory = GetDistinctStages(allStagingList);
    		stagingByCategory = stagingByCategory.OrderBy(a => a.DateTime).ToList();
    		int result = 0;

			CSPShowcase_Checkpoint newCheckpoint = null;
			if (!_isRepublish)
			{
				newCheckpoint = new CSPShowcase_Checkpoint
				{
					CheckpointId = Guid.NewGuid(),
					DateTime = checkPoint,
					PortalId = PortalId,
					TabModuleId = TabModuleId
				};
				DnnData.CSPShowcase_Checkpoints.InsertOnSubmit(newCheckpoint);
			}
    		foreach (CSPShowcase_Staging cspShowcaseStaging in stagingByCategory)
    		{
				if (cspShowcaseStaging.Published) continue;
				if (DoBackup(cspShowcaseStaging, newCheckpoint))
					result = DoPublish(cspShowcaseStaging);
				if (result != 1)
					break;

				if (!_isRepublish && DnnData.CSPShowcase_CheckpointDetails.FirstOrDefault(a => a.CSPShowcase_Staging == cspShowcaseStaging && a.IsBackupVersion == false) == null)
				{
					CSPShowcase_CheckpointDetail newCheckpointDetail = new CSPShowcase_CheckpointDetail
					                                                   	{
					                                                   		CheckpointDetailId = Guid.NewGuid(),
					                                                   		CSPShowcase_Checkpoint = newCheckpoint,
					                                                   		CSPShowcase_Staging = cspShowcaseStaging,
					                                                   		IsBackupVersion = false
					                                                   	};
					DnnData.CSPShowcase_CheckpointDetails.InsertOnSubmit(newCheckpointDetail);
				}
    		}

			DnnData.SubmitChanges(ConflictMode.FailOnFirstConflict);
    		return result;
    	}

    	/// <summary>
		/// Goes the rollback.
		/// </summary>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>7/2/2013</datetime>
		private int GoRollback()
		{
			int resultCode = 0;
			Guid checkpointId = new Guid(Request.Params["checkpointid"]);

			CSPShowcase_Checkpoint targetCheckpoint =
				DnnData.CSPShowcase_Checkpoints.FirstOrDefault(a => a.CheckpointId == checkpointId);
			if (targetCheckpoint == null)
				return resultCode;
			CSPShowcase_Checkpoint firstCheckpoint = DnnData.CSPShowcase_Checkpoints.Where(a => a.PortalId == PortalId && a.TabModuleId == TabModuleId).OrderBy(a => a.DateTime).First();
			List<CSPShowcase_Staging> targetCheckpointPublishList = null;
			List<CSPShowcase_Staging> backupList = null;
			CSPShowcase_Checkpoint currentCheckpoint = GetCurrentCheckpoint();
			if (currentCheckpoint.CheckpointId == checkpointId)
			{
				backupList =
					DnnData.CSPShowcase_Stagings.Where(
						a => a.CSPShowcase_CheckpointDetails.Any(b => b.CheckpointId == checkpointId && b.IsBackupVersion == true &&
							b.CheckpointId == checkpointId)).OrderBy(a => a.DateTime).ToList();
			}
			else if (currentCheckpoint.DateTime > targetCheckpoint.DateTime)
			{
				backupList = GetBackupList(checkpointId);
				targetCheckpointPublishList =
					DnnData.CSPShowcase_Stagings.Where(
						a => a.CSPShowcase_CheckpointDetails.Any(b => b.CheckpointId == checkpointId && b.IsBackupVersion == false)).
						OrderBy(a => a.DateTime).ToList();

				if (targetCheckpoint == firstCheckpoint)
				{
					targetCheckpointPublishList = null;
					backupList.AddRange(DnnData.CSPShowcase_Stagings.Where(
						a => a.CSPShowcase_CheckpointDetails.Any(b => b.CheckpointId == checkpointId && b.IsBackupVersion == true &&
							b.CheckpointId == checkpointId)).OrderBy(a => a.DateTime).ToList());
					backupList = backupList.OrderByDescending(a => a.DateTime).ToList();
				}
			}
			else
			{
				targetCheckpointPublishList =
					DnnData.CSPShowcase_Stagings.Where(
						a => a.CSPShowcase_CheckpointDetails.Any(b => b.CheckpointId == checkpointId && b.IsBackupVersion == false && 
							b.CSPShowcase_Checkpoint.DateTime >= targetCheckpoint.DateTime && 
							b.CSPShowcase_Checkpoint.DateTime <= currentCheckpoint.DateTime) &&
							a.PortalId == PortalId && a.TabModuleId == TabModuleId).
						OrderBy(a => a.DateTime).ToList();
			}

			DoRollBack(backupList);

			DoPublish(targetCheckpointPublishList);

			resultCode = 1;

			return resultCode;
		}

		/// <summary>
		/// Does the publish.
		/// </summary>
		/// <param name="publishList">The publish list.</param>
		/// <author>ducuytran</author>
		/// <datetime>7/7/2013</datetime>
		private void DoPublish(List<CSPShowcase_Staging> publishList)
		{
			if (publishList == null || !publishList.Any())
				return;
			foreach (CSPShowcase_Staging cspShowcaseStaging in publishList)
			{
				DoPublish(cspShowcaseStaging);
			}
		}

		/// <summary>
		/// Does the roll back.
		/// </summary>
		/// <param name="backupList">The backup list.</param>
		/// <author>ducuytran</author>
		/// <datetime>7/7/2013</datetime>
		private void DoRollBack(List<CSPShowcase_Staging> backupList)
		{
			if (backupList == null || !backupList.Any())
				return;
			foreach (CSPShowcase_Staging bkItem in backupList)
			{
				CSPShowcase_Staging publishedItem =
					DnnData.CSPShowcase_Stagings.FirstOrDefault(a => a.CategoryId == bkItem.CategoryId && a.Published == true && a.PortalId == PortalId && a.TabModuleId == TabModuleId);
				if (publishedItem != null)
					publishedItem.Published = false;
				bkItem.Published = true;
				bkItem.IsBackupVersion = false;

				content sourceContentCategory =
					CspDataContext.contents.FirstOrDefault(a => a.content_Id == bkItem.SourceContentCategoryId);
				content stagingContentCategory =
					CspDataContext.contents.FirstOrDefault(a => a.content_Id == bkItem.ContentCategoryId);
				if (stagingContentCategory == null)
				{
					CSPShowcase_Staging originalContentCategoryStage =
						DnnData.CSPShowcase_Stagings.FirstOrDefault(a => a.CategoryId == bkItem.CategoryId && a.IsOriginalVersion == true && a.PortalId == PortalId && a.TabModuleId == TabModuleId);
					stagingContentCategory =
						CspDataContext.contents.FirstOrDefault(a => a.content_Id == originalContentCategoryStage.ContentCategoryId);
				}
				content sourceContentPage =
					CspDataContext.contents.FirstOrDefault(a => a.content_Id == bkItem.SourceContentPageId);
				content stagingContentPage =
					CspDataContext.contents.FirstOrDefault(a => a.content_Id == bkItem.ContentPageId);
				if (stagingContentPage == null)
				{
					CSPShowcase_Staging originalContentPageStage =
						DnnData.CSPShowcase_Stagings.FirstOrDefault(a => a.CategoryId == bkItem.CategoryId && a.IsOriginalVersion == true && a.PortalId == PortalId && a.TabModuleId == TabModuleId);
					stagingContentPage =
						CspDataContext.contents.FirstOrDefault(a => a.content_Id == originalContentPageStage.ContentPageId);
				}
				String displayOrderString = _cspUtils.GetContentFieldValue(stagingContentCategory, "Status_Sort_Order");

				_cspUtils.SetContentFieldValue(_ctContentCategoryId, sourceContentCategory, "Content_Title", _cspUtils.GetContentFieldValue(stagingContentCategory, "Content_Title"));
				_cspUtils.SetContentFieldValue(_ctContentCategoryId, sourceContentCategory, "Status_Sort_Order", displayOrderString);
				_cspUtils.SetContentFieldValue(_ctContentCategoryId, sourceContentCategory, "Content_Description_Short", _cspUtils.GetContentFieldValue(stagingContentCategory, "Content_Description_Short"));
				_cspUtils.SetContentFieldValue(_ctContentCategoryId, sourceContentCategory, "Content_Image_Thumbnail", _cspUtils.GetContentFieldValue(stagingContentCategory, "Content_Image_Thumbnail"));

				_cspUtils.SetContentFieldValue(_ctContentPageId, sourceContentPage, "Content_Page", _cspUtils.GetContentFieldValue(stagingContentPage, "Content_Page"));

				category parentCategory = CspDataContext.categories.FirstOrDefault(a => a.categoryId == bkItem.ParentCategoryId);
				category targetCategory = CspDataContext.categories.FirstOrDefault(a => a.categoryId == bkItem.CategoryId);
				if (parentCategory.categoryId != targetCategory.parentId)
				{
					targetCategory.parentId = bkItem.ParentCategoryId;
					UpdateSubcategoriesLineage(targetCategory.categoryId, parentCategory.lineage + targetCategory.categoryId + "/");

				}

				if (bkItem.ActionId == (int)LogAction.Created)
				{
					targetCategory.active = true;
					sourceContentCategory.stage_Id = STAGE_STAGING;
					sourceContentPage.stage_Id = STAGE_STAGING;
					_cspUtils.SetContentFieldValue(_ctContentCategoryId, sourceContentCategory, CTF_CATEGORYCONTENT_STAGING, "Staging");
					_cspUtils.SetContentFieldValue(_ctContentPageId, sourceContentPage, CTF_CONTENTPAGE_STAGING, "Staging");
				}
				else if (bkItem.ActionId == (int)LogAction.GoActive || bkItem.ActionId == (int)LogAction.GoPublish)
				{
					targetCategory.active = true;
					sourceContentCategory.stage_Id = STAGE_PUBLISHED;
					sourceContentPage.stage_Id = STAGE_PUBLISHED;
				}
				else if (bkItem.ActionId == (int)LogAction.GoInactive)
				{
					targetCategory.active = false;
				}
				else if (bkItem.ActionId == (int)LogAction.GoUnpublish)
				{
					sourceContentCategory.stage_Id = STAGE_UNPUBLISHED;
					sourceContentPage.stage_Id = STAGE_UNPUBLISHED;
				}

				UpdateCategoriesOrder(bkItem);

				CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
				DnnData.SubmitChanges(ConflictMode.FailOnFirstConflict);
			}
		}

		/// <summary>
		/// Gets the current checkpoint.
		/// </summary>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>7/7/2013</datetime>
    	private CSPShowcase_Checkpoint GetCurrentCheckpoint()
		{
			CSPShowcase_Staging lastPublish =
				DnnData.CSPShowcase_Stagings.Where(a => a.Published && a.PortalId == PortalId && a.TabModuleId == TabModuleId).OrderByDescending(a => a.DateTime).First();
			if (lastPublish == null)
				return null;
			CSPShowcase_Checkpoint currentCheckpoint =
				DnnData.CSPShowcase_Checkpoints.FirstOrDefault(
					a => a.CSPShowcase_CheckpointDetails.Any(b => b.CSPShowcase_Staging == lastPublish));
			return currentCheckpoint;
		}

    	/// <summary>
		/// Gets the backup list.
		/// </summary>
		/// <param name="checkpointId">The checkpoint id.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>7/7/2013</datetime>
    	private List<CSPShowcase_Staging> GetBackupList(Guid checkpointId)
    	{
    		CSPShowcase_Checkpoint targetCheckpoint =
    			DnnData.CSPShowcase_Checkpoints.FirstOrDefault(a => a.CheckpointId == checkpointId);
			if (targetCheckpoint == null)
				return null;
    		List<CSPShowcase_Checkpoint> checkpointList =
				DnnData.CSPShowcase_Checkpoints.Where(a => a.DateTime > targetCheckpoint.DateTime && a.PortalId == PortalId && a.TabModuleId == TabModuleId).OrderByDescending(
    				a => a.DateTime).ToList();
			List<CSPShowcase_Staging> bkList = new List<CSPShowcase_Staging>();
    		foreach (CSPShowcase_Checkpoint cspShowcaseCheckpoint in checkpointList)
    		{
    			bkList.AddRange(
    				DnnData.CSPShowcase_Stagings.Where(
    					a =>
    					a.CSPShowcase_CheckpointDetails.Any(b => b.CSPShowcase_Checkpoint == cspShowcaseCheckpoint) &&
    					a.CSPShowcase_CheckpointDetails.Any(b => b.IsBackupVersion == true)).ToList());
    		}
			return bkList.OrderByDescending(a => a.DateTime).ToList();
    	}

    	/// <summary>
		/// Does the publish.
		/// </summary>
		/// <param name="cspShowcaseStaging">The CSP showcase staging.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>6/29/2013</datetime>
    	private int DoPublish(CSPShowcase_Staging cspShowcaseStaging)
    	{
			int categoryId = cspShowcaseStaging.CategoryId;
			int langId = cspShowcaseStaging.LanguageId;
    		List<string> displayOrderList = cspShowcaseStaging.DisplayOrder.Split('-').ToList();
			int newDisplayOrder = displayOrderList.IndexOf(categoryId.ToString()) + 1;
    		category targetCategory = CspDataContext.categories.FirstOrDefault(a => a.categoryId == categoryId);
			if (targetCategory == null)
				return -1;
			content sourceContentCategory =
					CspDataContext.contents.FirstOrDefault(a => a.content_Id == cspShowcaseStaging.SourceContentCategoryId);
			content sourceContentPage =
				CspDataContext.contents.FirstOrDefault(a => a.content_Id == cspShowcaseStaging.SourceContentPageId);

			if (sourceContentCategory == null || sourceContentPage == null)
				return -2;

			if (cspShowcaseStaging.Status == null || cspShowcaseStaging.Status == (int)LogAction.GoActive || cspShowcaseStaging.Status == (int)LogAction.GoPublish)
			{
				targetCategory.active = true;
				sourceContentCategory.stage_Id = STAGE_PUBLISHED;
				sourceContentPage.stage_Id = STAGE_PUBLISHED;
			}
			else if (cspShowcaseStaging.Status == (int)LogAction.GoInactive || cspShowcaseStaging.Status == (int)LogAction.GoUnpublish)
			{
				if (cspShowcaseStaging.Status == (int)LogAction.GoInactive)
					SetCategoryStatus(categoryId, 0, true);
				else
					SetContentsStatus(categoryId, 0, true);
			}

			targetCategory.parentId = cspShowcaseStaging.ParentCategoryId;
			targetCategory.DisplayOrder = newDisplayOrder;

    		if (cspShowcaseStaging.ContentCategoryId != Guid.Empty)
			{
				content newContentCategory =
					CspDataContext.contents.FirstOrDefault(a => a.content_Id == cspShowcaseStaging.ContentCategoryId);
				_cspUtils.SetContentFieldValue(_ctContentCategoryId, sourceContentCategory,
												   "Content_Title",
												   _cspUtils.GetContentFieldValue(newContentCategory, "Content_Title"));

				_cspUtils.SetContentFieldValue(_ctContentCategoryId, sourceContentCategory,
											   "Status_Sort_Order",
											   _cspUtils.GetContentFieldValue(newContentCategory, "Status_Sort_Order"));

				_cspUtils.SetContentFieldValue(_ctContentCategoryId, sourceContentCategory, "Content_Description_Short",
												_cspUtils.GetContentFieldValue(newContentCategory, "Content_Description_Short"));

				_cspUtils.SetContentFieldValue(_ctContentCategoryId, sourceContentCategory, "Content_Image_Thumbnail",
												_cspUtils.GetContentFieldValue(newContentCategory, "Content_Image_Thumbnail"));
			}

			if (cspShowcaseStaging.ContentPageId != Guid.Empty)
			{
				content newContentPage =
					CspDataContext.contents.FirstOrDefault(a => a.content_Id == cspShowcaseStaging.ContentPageId);
				_cspUtils.SetContentFieldValue(_ctContentPageId, sourceContentPage,
												   "Content_Page",
												   _cspUtils.GetContentFieldValue(newContentPage, "Content_Page"));
			}

			_cspUtils.SetContentFieldValue(_ctContentCategoryId, sourceContentCategory, "Status_Sort_Order", newDisplayOrder.ToString("0000"));
			_cspUtils.SetContentFieldValue(_ctContentCategoryId, sourceContentCategory,
											   CTF_CATEGORYCONTENT_STAGING, string.Empty);
			_cspUtils.SetContentFieldValue(_ctContentPageId, sourceContentPage,
											   CTF_CONTENTPAGE_STAGING, string.Empty);

    		cspShowcaseStaging.Published = true;

			category parentCategory = CspDataContext.categories.FirstOrDefault(a => a.categoryId == targetCategory.parentId);
			string newLinage = parentCategory.lineage + categoryId + "/";
			UpdateSubcategoriesLineage(categoryId, newLinage);
    		UpdateCategoriesOrder(cspShowcaseStaging);

			CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
			DnnData.SubmitChanges(ConflictMode.FailOnFirstConflict);
    		return 1;
    	}

		/// <summary>
		/// Updates the categories order.
		/// </summary>
		/// <param name="parentCategoryId">The parent category id.</param>
		/// <param name="displayOrder">The display order.</param>
		/// <author>ducuytran</author>
		/// <datetime>7/2/2013</datetime>
    	private void UpdateCategoriesOrder(CSPShowcase_Staging cspStaging)
		{
			List<string> categoryIds = cspStaging.DisplayOrder.Split('-').ToList();
			int newOrder = 1, tempId = 0;
			foreach (string categoryId in categoryIds)
			{
				int cateId = int.TryParse(categoryId, out tempId) ? tempId : 0;
				//if (cateId == cspStaging.CategoryId) continue;
				category targetCategory = CspDataContext.categories.FirstOrDefault(a => a.categoryId == cateId);
				content categoryContent = CspDataContext.contents.FirstOrDefault(a =>
								a.content_main.content_categories.Any(b => b.category_Id == cateId) &&
								a.content_main.content_types_Id == _ctContentCategoryId &&
								a.content_types_languages_Id == cspStaging.LanguageId && a.stage_Id != STAGE_STAGING);
				if (categoryContent == null)
				{
					categoryContent = CspDataContext.contents.FirstOrDefault(a =>
								a.content_main.content_categories.Any(b => b.category_Id == cateId) &&
								a.content_main.content_types_Id == _ctContentCategoryId &&
								a.content_types_languages_Id == cspStaging.LanguageId && a.stage_Id == STAGE_STAGING);
				}
				if (categoryContent == null)
					continue;
				targetCategory.DisplayOrder = newOrder;
				_cspUtils.SetContentFieldValue(_ctContentCategoryId, categoryContent, "Status_Sort_Order", newOrder.ToString("0000"));
				//CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
				newOrder++;
			}
		}

    	/// <summary>
		/// Updates the subcategories lineage.
		/// </summary>
		/// <param name="categoryId">The category id.</param>
		/// <param name="newLineage">The new lineage.</param>
		/// <author>ducuytran</author>
		/// <datetime>6/30/2013</datetime>
    	private void UpdateSubcategoriesLineage(int categoryId, string newLineage)
    	{
    		List<category> subList = CspDataContext.categories.Where(a => a.parentId == categoryId).ToList();
			if (!subList.Any()) return;
    		foreach (category cateItem in subList)
    		{
    			cateItem.parentId = cateItem.parentId;
    			cateItem.lineage = newLineage + cateItem.categoryId + "/";
				//CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
				UpdateSubcategoriesLineage(cateItem.categoryId, cateItem.lineage);
    		}
    	}

    	/// <summary>
		/// Does the backup.
		/// </summary>
		/// <param name="cspShowcaseStaging">The CSP showcase staging.</param>
		/// <param name="checkPoint">The check point.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>6/29/2013</datetime>
		private bool DoBackup(CSPShowcase_Staging cspShowcaseStaging, CSPShowcase_Checkpoint checkPoint)
    	{
    		int categoryId = cspShowcaseStaging.CategoryId;
    		int langId = cspShowcaseStaging.LanguageId;
    		CSPShowcase_Staging firstBackup =
    			DnnData.CSPShowcase_Stagings.FirstOrDefault(
    				a =>
    				a.CategoryId == categoryId && a.LanguageId == langId &&
					a.IsOriginalVersion == true && a.PortalId == PortalId && a.TabModuleId == TabModuleId);
			CSPShowcase_CheckpointDetail newCheckpointDetail = new CSPShowcase_CheckpointDetail
			                                                   	{
			                                                   		CheckpointDetailId = Guid.NewGuid(),
																	CSPShowcase_Checkpoint = checkPoint,
																	IsBackupVersion = true
			                                                   	};
			if (firstBackup != null)
			{
				CSPShowcase_Staging currentPublish =
					DnnData.CSPShowcase_Stagings.FirstOrDefault(
						a => a.CategoryId == categoryId && a.LanguageId == langId && a.Published == true &&
						a.PortalId == PortalId && a.TabModuleId == TabModuleId);
				CSPShowcase_Staging currentBackup = DnnData.CSPShowcase_Stagings.FirstOrDefault(
						a => a.CategoryId == categoryId && a.LanguageId == langId && a.IsBackupVersion == true &&
						a.PortalId == PortalId && a.TabModuleId == TabModuleId);
				if (currentPublish != null)
				{
					currentPublish.Published = false;
					currentPublish.IsBackupVersion = true;
					newCheckpointDetail.CSPShowcase_Staging = currentPublish;
				}
				else if (currentBackup != null)
				{
					newCheckpointDetail.CSPShowcase_Staging = currentBackup;
				}

				if (currentBackup != null && currentPublish != null)
					currentBackup.IsBackupVersion = false;
			}
			else
			{
				bool isCreatedInStaging = false;
				int crrStatus = (int) LogAction.GoInactive;
				category categoryItem = CspDataContext.categories.FirstOrDefault(a => a.categoryId == categoryId);
				if (categoryItem == null)
					return false;
				content sourceContentCategory =
					CspDataContext.contents.FirstOrDefault(a => a.content_Id == cspShowcaseStaging.SourceContentCategoryId);
				content sourceContentPage =
					CspDataContext.contents.FirstOrDefault(a => a.content_Id == cspShowcaseStaging.SourceContentPageId);
				content newContentCategory = null, newContentPage = null;

				if ((bool)categoryItem.active && sourceContentCategory.stage_Id == STAGE_PUBLISHED && sourceContentPage.stage_Id == STAGE_PUBLISHED)
					crrStatus = (int)LogAction.GoActive;
				else if (!(bool)categoryItem.active)
					crrStatus = (int)LogAction.GoInactive;
				else
					crrStatus = (int)LogAction.GoUnpublish;

				if (!String.IsNullOrEmpty(_cspUtils.GetContentFieldValue(sourceContentCategory, CTF_CATEGORYCONTENT_STAGING)))
					isCreatedInStaging = true;
				

				if (cspShowcaseStaging.SourceContentCategoryId != Guid.Empty && sourceContentCategory != null)
				{
					newContentCategory = new content
					                             	{
					                             		content_Id = Guid.NewGuid(),
					                             		content_main = sourceContentCategory.content_main,
					                             		active = true,
					                             		stage_Id = STAGE_STAGING,
					                             		content_types_languages_Id = _langId
					                             	};
					CspDataContext.contents.InsertOnSubmit(newContentCategory);
					CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);

					_cspUtils.SetContentFieldValue(_ctContentCategoryId, newContentCategory,
					                               "Content_Title",
					                               _cspUtils.GetContentFieldValue(sourceContentCategory, "Content_Title"));

					_cspUtils.SetContentFieldValue(_ctContentCategoryId, newContentCategory,
					                               CTF_CATEGORYCONTENT_STAGING, "Staging");

					_cspUtils.SetContentFieldValue(_ctContentCategoryId, newContentCategory,
					                               "Status_Sort_Order",
					                               _cspUtils.GetContentFieldValue(sourceContentCategory, "Status_Sort_Order"));

					_cspUtils.SetContentFieldValue(_ctContentCategoryId, newContentCategory, "Content_Description_Short", 
													_cspUtils.GetContentFieldValue(sourceContentCategory, "Content_Description_Short"));

					_cspUtils.SetContentFieldValue(_ctContentCategoryId, newContentCategory, "Content_Image_Thumbnail", 
													_cspUtils.GetContentFieldValue(sourceContentCategory, "Content_Image_Thumbnail"));
				}
				if (cspShowcaseStaging.SourceContentPageId != Guid.Empty && sourceContentPage != null)
				{
					newContentPage = new content
												{
													content_Id = Guid.NewGuid(),
													content_main = sourceContentPage.content_main,
													active = true,
													stage_Id = STAGE_STAGING,
													content_types_languages_Id = _langId
												};
					CspDataContext.contents.InsertOnSubmit(newContentPage);
					CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);

					_cspUtils.SetContentFieldValue(_ctContentPageId, newContentPage,
												   "Content_Page",
												   _cspUtils.GetContentFieldValue(sourceContentPage, "Content_Page"));

					_cspUtils.SetContentFieldValue(_ctContentPageId, newContentPage,
												   CTF_CONTENTPAGE_STAGING, "Staging");
				}

				if (newContentCategory == null || newContentPage == null)
					return false;

				String DisplayOrderList = "";
				List<category> categoriesList = CspDataContext.categories.Where(a => a.parentId == categoryItem.parentId).OrderBy(a => a.DisplayOrder).ToList();
				if (categoriesList.Any())
				{
					foreach (category theCategory in categoriesList)
					{
						if (!String.IsNullOrEmpty(DisplayOrderList))
							DisplayOrderList += "-";
						DisplayOrderList += theCategory.categoryId.ToString();
					}
				}
				CSPShowcase_Staging newStagingItem = new CSPShowcase_Staging
																			{
																				Id = Guid.NewGuid(),
																				ParentCategoryId = (int) categoryItem.parentId,
																				CategoryId = categoryId,
																				DisplayOrder = _originalDisplayOrder[categoryItem.parentId.ToString()],
																				SourceContentCategoryId = sourceContentCategory.content_Id,
																				ContentCategoryId = newContentCategory.content_Id,
																				ContentCategoryMainId = sourceContentCategory.content_main_Id,
																				SourceContentPageId = sourceContentPage.content_Id,
																				ContentPageId = newContentPage.content_Id,
																				ContentPageMainId = newContentPage.content_main_Id,
																				Published = false,
																				LanguageId = cspShowcaseStaging.LanguageId,
																				DateTime = checkPoint.DateTime,
																				PortalId = PortalId,
																				TabModuleId = TabModuleId,
																				IsOriginalVersion = true,
																				IsBackupVersion = true
																			};
				if (isCreatedInStaging)
					newStagingItem.ActionId = (int) LogAction.Created;
				else
				{
					newStagingItem.Status = crrStatus;
					newStagingItem.ActionId = crrStatus;
				}
				newCheckpointDetail.CSPShowcase_Staging = newStagingItem;
				DnnData.CSPShowcase_Stagings.InsertOnSubmit(newStagingItem);
			}
			if (!_isRepublish && DnnData.CSPShowcase_CheckpointDetails.FirstOrDefault(a => a.CSPShowcase_Staging == newCheckpointDetail.CSPShowcase_Staging && a.IsBackupVersion == true) == null)
				DnnData.CSPShowcase_CheckpointDetails.InsertOnSubmit(newCheckpointDetail);
			DnnData.SubmitChanges(ConflictMode.FailOnFirstConflict);
    		return true;
    	}

		/// <summary>
		/// Gets the distinct stages.
		/// </summary>
		/// <param name="allStagingList">All staging list.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>6/28/2013</datetime>
    	private List<CSPShowcase_Staging> GetDistinctStages(List<CSPShowcase_Staging> allStagingList)
    	{
    		List<CSPShowcase_Staging> distinctList = new List<CSPShowcase_Staging>();
			List<string> distinctCategory = new List<string>();
    		foreach (CSPShowcase_Staging showcaseStaging in allStagingList)
    		{
    			string categoryStageIdentify = showcaseStaging.CategoryId + "-" + showcaseStaging.LanguageId;
				if (distinctCategory.IndexOf(categoryStageIdentify) == -1)
				{
					distinctCategory.Add(categoryStageIdentify);
					distinctList.Add(showcaseStaging);
				}

				if (!_originalDisplayOrder.Any(a => a.Key == showcaseStaging.ParentCategoryId.ToString()))
				{
					String DisplayOrderList = "";
					CSPShowcase_Staging originalStaging = DnnData.CSPShowcase_Stagings.FirstOrDefault(
						a => a.ParentCategoryId == showcaseStaging.ParentCategoryId && a.IsOriginalVersion == true &&
						     a.PortalId == PortalId && a.TabModuleId == TabModuleId);
					if (originalStaging != null)
					{
						List<String> categoryList = originalStaging.DisplayOrder.Split('-').ToList();
						if (categoryList.IndexOf(showcaseStaging.CategoryId.ToString()) >= 0)
							DisplayOrderList = originalStaging.DisplayOrder;
					}
					if (String.IsNullOrEmpty(DisplayOrderList))
					{
						List<category> categoriesList =
							CspDataContext.categories.Where(a => a.parentId == showcaseStaging.ParentCategoryId).OrderBy(a => a.DisplayOrder)
								.ToList();
						if (categoriesList.Any())
						{
							foreach (category theCategory in categoriesList)
							{
								if (!String.IsNullOrEmpty(DisplayOrderList))
									DisplayOrderList += "-";
								DisplayOrderList += theCategory.categoryId.ToString();
							}
						}
					}
					_originalDisplayOrder.Add(showcaseStaging.ParentCategoryId.ToString(), DisplayOrderList);
				}
    		}
    		return distinctList;
    	}

    	/// <summary>
		/// Changes the category subscribe status.
		/// </summary>
		/// <param name="categoryId">The category id.</param>
		/// <param name="status">The status.</param>
		/// <author>ducuytran - 3/26/2013</author>
		private void ChangeCategorySubscribeStatus(int categoryId, bool status)
		{
			if (categoryId < 1)
				return;
			List<category> subCategoriesList = CspDataContext.categories.Where(a => a.parentId == categoryId).ToList();
			if (subCategoriesList.Count > 0)
			{
				foreach (category subItem in subCategoriesList)
				{
					ChangeCategorySubscribeStatus(subItem.categoryId, status);
				}
			}
			category targetCategory = CspDataContext.categories.FirstOrDefault(a => a.categoryId == categoryId);
			if (targetCategory == null)
				return;
			targetCategory.IsSubscribable = status;
		}

		/// <summary>
		/// Renames the category.
		/// </summary>
		/// <param name="categoryId">The category id.</param>
		/// <param name="categoryName">Name of the category.</param>
		/// <param name="categoryContentId">The category content id.</param>
		/// <returns></returns>
		/// <author>ducuytran - 3/3/2013</author>
		private string RenameCategory(int categoryId, string categoryName, string categoryContentId)
		{
			int result = 0;
			string strData = "<xmlresult><resultcode>{0}</resultcode></xmlresult>"; //<xmlresult><resultcode>{0}</resultcode></xmlresult>
			if (categoryId < 1 || String.IsNullOrEmpty(categoryName))
				return String.Format(strData, result, string.Empty);
			Guid contentId = new Guid(categoryContentId);
			if (contentId != Guid.Empty)
			{
				content contentCategory = GetSourceContentCategory(contentId);// CspDataContext.contents.FirstOrDefault(a => a.content_Id == contentId);
				result = -1;
				if (contentCategory == null)
					return String.Format(strData, result);
				result = 1;

				//The condition:
				//_ !String.IsNullOrEmpty(_cspUtils.GetContentFieldValue(contentCategory, CTF_CATEGORYCONTENT_STAGING)) 
				//means this category created in staging mode
				if (PublishOnFly || !String.IsNullOrEmpty(_cspUtils.GetContentFieldValue(contentCategory, CTF_CATEGORYCONTENT_STAGING)))
				{
					_cspUtils.SetContentFieldValue(_ctContentCategoryId, contentCategory, "Content_Title", categoryName);
				}
				else
				{
					CreateStaging(categoryId, contentCategory, categoryName);
				}
			}
			return String.Format(strData, result);
		}

		/// <summary>
		/// Updates the category description.
		/// </summary>
		/// <param name="categoryId">The category id.</param>
		/// <param name="categoryDesc">The category desc.</param>
		/// <returns></returns>
		/// <author>ducuytran - 4/14/2013</author>
		private int UpdateCategoryDescription(int categoryId, string categoryDesc, string categoryThumbnail = "")
		{
			int result = 0;
			if (categoryId < 1)
				return result;
			content categoryContent =
					CspDataContext.contents.FirstOrDefault(
						a =>
						a.content_main.content_categories.Any(b => b.category_Id == categoryId) &&
						a.content_main.content_types_Id == _ctContentCategoryId && a.content_types_languages_Id == _langId && a.stage_Id != STAGE_STAGING);
			categoryDesc = categoryDesc.Replace("\n", String.Empty);
			categoryDesc = categoryDesc.Replace("\r", String.Empty);
			if (PublishOnFly)
			{
				if (categoryContent != null)
				{
					_cspUtils.SetContentFieldValue(_ctContentCategoryId, categoryContent, "Content_Description_Short", Server.HtmlEncode(categoryDesc));
					_cspUtils.SetContentFieldValue(_ctContentCategoryId, categoryContent, "Content_Image_Thumbnail", categoryThumbnail);
					return 1;
				}
			}

			category targetCategory = CspDataContext.categories.FirstOrDefault(a => a.categoryId == categoryId);
			CSPShowcase_Staging latestStaging = GetLatestStagingContent(categoryId);
			CSPShowcase_Staging newStaging;
			if (latestStaging == null && categoryContent == null)
				return result;
			if (categoryContent == null)
				categoryContent = CspDataContext.contents.FirstOrDefault(a => a.content_Id == latestStaging.SourceContentCategoryId);

			content newContent = new content
			{
				content_Id = Guid.NewGuid(),
				content_main = categoryContent.content_main,
				active = true,
				stage_Id = STAGE_STAGING,
				content_types_languages_Id = _langId
			};
			CspDataContext.contents.InsertOnSubmit(newContent);
			CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
			_cspUtils.SetContentFieldValue(_ctContentCategoryId, newContent, "Content_Title", _cspUtils.GetContentFieldValue(categoryContent, "Content_Title"));
			_cspUtils.SetContentFieldValue(_ctContentCategoryId, newContent, "Status_Sort_Order", _cspUtils.GetContentFieldValue(categoryContent, "Status_Sort_Order"));
			_cspUtils.SetContentFieldValue(_ctContentCategoryId, newContent, CTF_CATEGORYCONTENT_STAGING, "Staging");
			_cspUtils.SetContentFieldValue(_ctContentCategoryId, newContent, "Content_Description_Short", Server.HtmlEncode(categoryDesc));
			_cspUtils.SetContentFieldValue(_ctContentCategoryId, newContent, "Content_Image_Thumbnail", categoryThumbnail);

			if (latestStaging == null)
			{
				newStaging = InitFirstStaging(categoryId, categoryContent, newContent);
			}
			else
			{
				newStaging = new CSPShowcase_Staging
				{
					Id = Guid.NewGuid(),
					ParentCategoryId = latestStaging.ParentCategoryId,
					CategoryId = categoryId,
					DisplayOrder = latestStaging.DisplayOrder,
					SourceContentCategoryId = latestStaging.SourceContentCategoryId,
					ContentCategoryId = newContent.content_Id,
					ContentCategoryMainId = latestStaging.ContentCategoryMainId,
					SourceContentPageId = latestStaging.SourceContentPageId,
					ContentPageId = latestStaging.ContentPageId,
					ContentPageMainId = latestStaging.ContentPageMainId,
					Published = false,
					LanguageId = _langId,
					DateTime = DateTime.Now,
					PortalId = PortalId,
					TabModuleId = TabModuleId,
					IsBackupVersion = false,
					IsOriginalVersion = false,
					Status = latestStaging.Status
				};
			}
			newStaging.ActionId = (int) LogAction.EditPageDescription;
			DnnData.CSPShowcase_Stagings.InsertOnSubmit(newStaging);
			DnnData.SubmitChanges(ConflictMode.FailOnFirstConflict);
			return result;
		}

		/// <summary>
		/// Saves the content page.
		/// </summary>
		/// <returns></returns>
		/// <author>ducuytran - 3/3/2013</author>
		private string SaveContentPage()
		{
			int result = 0;
			string resultString = "<xmlresult><resultcode>{0}</resultcode><contentpageid>{1}</contentpageid></xmlresult>";
			int categoryId = 0;
			if (!String.IsNullOrEmpty(Request.Params["categoryid"]))
				int.TryParse(Request.Params["categoryid"], out categoryId);

			if (String.IsNullOrEmpty(Request.Params["contentid"]))
				return string.Format(resultString, result, string.Empty);

			Guid contentId = new Guid(Request.Params["contentid"]);
			string stringToReplace = HttpContext.Current.Request.ApplicationPath;
			string value = Request.Params["contenttext"];
			if (!String.IsNullOrEmpty(_staticImgUrl))
			{
				value = value.Replace("src=\"" + stringToReplace, "src=\"" + _staticImgUrl);
				value = value.Replace("href=\"" + stringToReplace, "href=\"" + _staticImgUrl);
			}

			if (contentId != Guid.Empty)
			{
				content contentPage = GetSourceContentPage(contentId);// CspDataContext.contents.FirstOrDefault(a => a.content_Id == contentId);
				result = -1;
				if (contentPage == null)
					return string.Format(resultString, result, string.Empty);
				result = 1;

				//The condition:
				//_ !String.IsNullOrEmpty(_cspUtils.GetContentFieldValue(contentPage, CTF_CONTENTPAGE_STAGING))
				//means this category created in staging mode
				if (PublishOnFly || !String.IsNullOrEmpty(_cspUtils.GetContentFieldValue(contentPage, CTF_CONTENTPAGE_STAGING)))
				{
					_cspUtils.SetContentFieldValue(_ctContentPageId, contentPage, "Content_Page", value);
				}
				else
				{
					CreateStaging(categoryId, contentPage, value);
				}
			}
			else
			{
				content_category contentCat =
						CspDataContext.content_categories.FirstOrDefault(
							a => a.category_Id == categoryId && a.content_main.content_types_Id == _ctContentPageId);
				if (contentCat == null)
					result = -2;
				else
				{
					content_main targetContentMain =
						CspDataContext.content_mains.FirstOrDefault(a => a.content_main_Id == contentCat.content_main_Id);
					content newContentPage = new content
														{
															content_Id = Guid.NewGuid(),
															content_main = targetContentMain,
															active = true,
															stage_Id = STAGE_PUBLISHED,
															content_types_languages_Id = _langId
														};
					List<content_types_field> fieldDefs =
					CspDataContext.content_types_fields.Where(a => a.content_types_Id == _ctContentPageId).ToList();
					string valueText = "";
					foreach (content_types_field contentTypesField in fieldDefs)
					{
						if (contentTypesField.fieldname == "Content_Page")
							valueText = value;
						else
						{
							valueText = "";
						}

						newContentPage.content_fields.Add(new content_field
						{
							content_types_field = contentTypesField,
							content = newContentPage,
							value_text = valueText
						});
					}

					CspDataContext.contents.InsertOnSubmit(newContentPage);
					CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
					contentId = newContentPage.content_Id;
					result = 1;
				}
			}
			return string.Format(resultString, result, contentId);
		}

		/// <summary>
		/// Gets the content of the source.
		/// </summary>
		/// <param name="contentId">The content id.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>6/21/2013</datetime>
    	private content GetSourceContentPage(Guid contentId)
    	{
    		CSPShowcase_Staging stagingItem = DnnData.CSPShowcase_Stagings.FirstOrDefault(a => a.ContentPageId == contentId);
			if (stagingItem == null)
				return CspDataContext.contents.FirstOrDefault(a => a.content_Id == contentId);
    		return CspDataContext.contents.FirstOrDefault(a => a.content_Id == stagingItem.SourceContentPageId);
    	}

		/// <summary>
		/// Gets the source content category.
		/// </summary>
		/// <param name="contentId">The content id.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>6/23/2013</datetime>
		private content GetSourceContentCategory(Guid contentId)
		{
			CSPShowcase_Staging stagingItem = DnnData.CSPShowcase_Stagings.FirstOrDefault(a => a.ContentCategoryId == contentId);
			if (stagingItem == null)
				return CspDataContext.contents.FirstOrDefault(a => a.content_Id == contentId);
			return CspDataContext.contents.FirstOrDefault(a => a.content_Id == stagingItem.SourceContentCategoryId);
		}

		/// <summary>
		/// Gets the content of the latest staging.
		/// </summary>
		/// <param name="categoryId">The category id.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>6/22/2013</datetime>
		private CSPShowcase_Staging GetLatestStagingContent(int categoryId)
		{
			List<CSPShowcase_Staging> stagingList =
				DnnData.CSPShowcase_Stagings.Where(a => a.CategoryId == categoryId && a.LanguageId == _langId &&
													(a.IsBackupVersion == false || a.IsBackupVersion == null) &&
													(a.IsOriginalVersion == false || a.IsOriginalVersion == null) &&
													a.PortalId == PortalId && a.TabModuleId == TabModuleId).OrderByDescending(a => a.DateTime).ToList();
			if (!stagingList.Any())
			{
				CSPShowcase_Staging currentPublish =
					DnnData.CSPShowcase_Stagings.FirstOrDefault(a => a.CategoryId == categoryId && a.LanguageId == _langId &&
					                                                 a.IsOriginalVersion == true && a.PortalId == PortalId &&
					                                                 a.TabModuleId == TabModuleId);
				return currentPublish;
			}
				
			return stagingList.First();
		}

    	/// <summary>
		/// Creates the staging.
		/// </summary>
		/// <param name="sourceContent">The content page.</param>
		/// <param name="value">The value.</param>
		/// <author>ducuytran</author>
		/// <datetime>6/18/2013</datetime>
    	private void CreateStaging(int categoryId, content sourceContent, string value)
    	{
    		string stagingContentTypeFieldName = CTF_CATEGORYCONTENT_STAGING;
    		string valueFieldName = "Content_Title";
    		int actionLogId = (int) LogAction.Rename;
    		String actionLogComment = "";
			if (sourceContent.content_main.content_types_Id == _ctContentPageId)
			{
				stagingContentTypeFieldName = CTF_CONTENTPAGE_STAGING;
				valueFieldName = "Content_Page";
				actionLogId = (int) LogAction.EditContent;
			}

    		content newContent = new content
												{
													content_Id = Guid.NewGuid(),
													content_main = sourceContent.content_main,
													active = true,
													stage_Id = STAGE_STAGING,
													content_types_languages_Id = _langId
												};
			CspDataContext.contents.InsertOnSubmit(newContent);
			CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
			_cspUtils.SetContentFieldValue(sourceContent.content_main.content_types_Id, newContent, valueFieldName, value);
			_cspUtils.SetContentFieldValue(sourceContent.content_main.content_types_Id, newContent, stagingContentTypeFieldName, "Staging");
			if (sourceContent.content_main.content_types_Id == _ctContentCategoryId)
			{
				_cspUtils.SetContentFieldValue(sourceContent.content_main.content_types_Id, newContent, "Status_Sort_Order", _cspUtils.GetContentFieldValue(sourceContent, "Status_Sort_Order"));
			}

    		CSPShowcase_Staging newStagingItem = null;
    		CSPShowcase_Staging lastStaging = GetLatestStagingContent(categoryId);
			content lastContentCategoryStage = (sourceContent.content_main.content_types_Id == _ctContentCategoryId) ? sourceContent : null;
			if (lastStaging != null)
			{
				newStagingItem = new CSPShowcase_Staging
														{
															Id = Guid.NewGuid(),
															ParentCategoryId = lastStaging.ParentCategoryId,
															CategoryId = categoryId,
															DisplayOrder = GetDisplayOrderList(lastStaging.ParentCategoryId, 0),
															SourceContentCategoryId = lastStaging.SourceContentCategoryId,
															ContentCategoryId = (sourceContent.content_main.content_types_Id == _ctContentPageId) ? lastStaging.ContentCategoryId : newContent.content_Id,
															ContentCategoryMainId = lastStaging.ContentCategoryMainId,
															SourceContentPageId = lastStaging.SourceContentPageId,
															ContentPageId = (sourceContent.content_main.content_types_Id == _ctContentPageId) ? newContent.content_Id : lastStaging.ContentPageId,
															ContentPageMainId = lastStaging.ContentPageMainId,
															Published = false,
															LanguageId = _langId,
															DateTime = DateTime.Now,
															PortalId = PortalId,
															TabModuleId = TabModuleId,
															Status = lastStaging.Status
														};
				if (sourceContent.content_main.content_types_Id == _ctContentCategoryId && lastStaging.ContentCategoryId != Guid.Empty)
				{
					lastContentCategoryStage =
						CspDataContext.contents.FirstOrDefault(a => a.content_Id == lastStaging.ContentCategoryId);
				}
			}
			else
			{
				newStagingItem = InitFirstStaging(categoryId, sourceContent, newContent);
				if (sourceContent.content_main.content_types_Id == _ctContentCategoryId)
					lastContentCategoryStage = sourceContent;
			}

			if (lastContentCategoryStage != null)
			{
				_cspUtils.SetContentFieldValue(_ctContentCategoryId, newContent, "Content_Description_Short", _cspUtils.GetContentFieldValue(lastContentCategoryStage, "Content_Description_Short"));
				_cspUtils.SetContentFieldValue(_ctContentCategoryId, newContent, "Content_Image_Thumbnail", _cspUtils.GetContentFieldValue(lastContentCategoryStage, "Content_Image_Thumbnail"));
				actionLogComment = GetLocalizedText("ActionLogComment.Rename").Replace("[old_title]",
																								   _cspUtils.GetContentFieldValue(
																									lastContentCategoryStage,
																									"Content_Title"));
				actionLogComment = actionLogComment.Replace("[new_title]", value);
			}
			newStagingItem.ActionComment = actionLogComment;
			newStagingItem.ActionId = actionLogId;
			DnnData.CSPShowcase_Stagings.InsertOnSubmit(newStagingItem);
			DnnData.SubmitChanges(ConflictMode.FailOnFirstConflict);
    	}

		/// <summary>
		/// Inits the first staging.
		/// </summary>
		/// <param name="categoryId">The category id.</param>
		/// <param name="newContent">The new content.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>6/22/2013</datetime>
		private CSPShowcase_Staging InitFirstStaging(int categoryId, content sourceContent, content newContent)
		{
			content contentCategory = newContent, srcContentCategory = sourceContent, contentPage = null, srcContentPage = null;
			category crrCategory = CspDataContext.categories.FirstOrDefault(a => a.categoryId == categoryId);
			/*int status = (int) LogAction.GoUnpublish;
			int active = (bool) crrCategory.active ? (int) LogAction.GoActive : (int) LogAction.GoInactive;*/
			if (crrCategory == null)
				return null;
			if (newContent.content_main.content_types_Id == _ctContentPageId)
			{
				contentPage = newContent;
				srcContentPage = sourceContent;
				srcContentCategory = CspDataContext.contents.FirstOrDefault(a => a.content_main.content_categories.Any(b => b.category_Id == categoryId) 
																		&& a.content_main.content_types_Id == _ctContentCategoryId && a.content_types_languages_Id == _langId
																		&& a.stage_Id != STAGE_STAGING);
				contentCategory = null;
			}
			else
			{
				srcContentPage = CspDataContext.contents.FirstOrDefault(a => a.content_main.content_categories.Any(b => b.category_Id == categoryId)
																		&& a.content_main.content_types_Id == _ctContentPageId && a.content_types_languages_Id == _langId
																		&& a.stage_Id != STAGE_STAGING);
			}

			CSPShowcase_Staging newStagingItem = new CSPShowcase_Staging
																		{
																			Id = Guid.NewGuid(),
																			ParentCategoryId = (int) crrCategory.parentId,
																			CategoryId = categoryId,
																			DisplayOrder = GetDisplayOrderList((int)crrCategory.parentId, 0),
																			SourceContentCategoryId = (srcContentCategory != null) ? srcContentCategory.content_Id : Guid.Empty,
																			ContentCategoryId = (contentCategory != null) ? contentCategory.content_Id : Guid.Empty,
																			ContentCategoryMainId = (srcContentCategory != null) ? srcContentCategory.content_main.content_main_Id : Guid.Empty,
																			SourceContentPageId = (srcContentPage != null) ? srcContentPage.content_Id : Guid.Empty,
																			ContentPageId = (contentPage != null) ? contentPage.content_Id : Guid.Empty,
																			ContentPageMainId = (srcContentPage != null) ? srcContentPage.content_main.content_main_Id : Guid.Empty,
																			LanguageId = _langId,
																			Published = false,
																			DateTime = DateTime.Now,
																			PortalId = PortalId,
																			TabModuleId = TabModuleId
																		};
			return newStagingItem;
		}

		/// <summary>
		/// Inits the first staging.
		/// </summary>
		/// <param name="categoryId">The category id.</param>
		/// <param name="displayOrder">The display order.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>6/24/2013</datetime>
		private CSPShowcase_Staging InitFirstStaging(int categoryId, string displayOrder)
		{
			category crrCategory = CspDataContext.categories.FirstOrDefault(a => a.categoryId == categoryId);
			if (crrCategory == null)
				return null;
			content srcContentCategory = CspDataContext.contents.FirstOrDefault(a => a.content_main.content_categories.Any(b => b.category_Id == categoryId)
																		&& a.content_main.content_types_Id == _ctContentCategoryId && a.content_types_languages_Id == _langId
																		&& a.stage_Id != STAGE_STAGING);
			content srcContentPage = CspDataContext.contents.FirstOrDefault(a => a.content_main.content_categories.Any(b => b.category_Id == categoryId)
																		&& a.content_main.content_types_Id == _ctContentPageId && a.content_types_languages_Id == _langId
																		&& a.stage_Id != STAGE_STAGING);
			CSPShowcase_Staging newStagingItem = new CSPShowcase_Staging
																		{
																			Id = Guid.NewGuid(),
																			ParentCategoryId = (int)crrCategory.parentId,
																			CategoryId = categoryId,
																			DisplayOrder = displayOrder,
																			SourceContentCategoryId = (srcContentCategory != null) ? srcContentCategory.content_Id : Guid.Empty,
																			ContentCategoryId = Guid.Empty,
																			ContentCategoryMainId = (srcContentCategory != null) ? srcContentCategory.content_main.content_main_Id : Guid.Empty,
																			SourceContentPageId = (srcContentPage != null) ? srcContentPage.content_Id : Guid.Empty,
																			ContentPageId = Guid.Empty,
																			ContentPageMainId = (srcContentPage != null) ? srcContentPage.content_main.content_main_Id : Guid.Empty,
																			LanguageId = _langId,
																			Published = false,
																			DateTime = DateTime.Now,
																			PortalId = PortalId,
																			TabModuleId = TabModuleId
																		};
			return newStagingItem;
		}

		/// <summary>
		/// Inits the first staging.
		/// </summary>
		/// <param name="categoryId">The category id.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>7/2/2013</datetime>
		private CSPShowcase_Staging InitFirstStaging(category theCategory)
		{
			category crrCategory = theCategory;// CspDataContext.categories.FirstOrDefault(a => a.categoryId == categoryId);
			int categoryId = crrCategory.categoryId;
			if (crrCategory == null)
				return null;
			content srcContentCategory = CspDataContext.contents.FirstOrDefault(a => a.content_main.content_categories.Any(b => b.category_Id == categoryId)
																		&& a.content_main.content_types_Id == _ctContentCategoryId && a.content_types_languages_Id == _langId
																		&& a.stage_Id != STAGE_STAGING);
			content srcContentPage = CspDataContext.contents.FirstOrDefault(a => a.content_main.content_categories.Any(b => b.category_Id == categoryId)
																		&& a.content_main.content_types_Id == _ctContentPageId && a.content_types_languages_Id == _langId
																		&& a.stage_Id != STAGE_STAGING);
			CSPShowcase_Staging newStagingItem = new CSPShowcase_Staging
			{
				Id = Guid.NewGuid(),
				ParentCategoryId = (int)crrCategory.parentId,
				CategoryId = categoryId,
				DisplayOrder = GetDisplayOrderList((int) crrCategory.parentId, 0),
				SourceContentCategoryId = (srcContentCategory != null) ? srcContentCategory.content_Id : Guid.Empty,
				ContentCategoryId = Guid.Empty,
				ContentCategoryMainId = (srcContentCategory != null) ? srcContentCategory.content_main.content_main_Id : Guid.Empty,
				SourceContentPageId = (srcContentPage != null) ? srcContentPage.content_Id : Guid.Empty,
				ContentPageId = Guid.Empty,
				ContentPageMainId = (srcContentPage != null) ? srcContentPage.content_main.content_main_Id : Guid.Empty,
				LanguageId = _langId,
				Published = false,
				DateTime = DateTime.Now,
				PortalId = PortalId,
				TabModuleId = TabModuleId
			};
			return newStagingItem;
		}

		/// <summary>
		/// Gets the display order list.
		/// </summary>
		/// <param name="parentId">The parent id.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>6/23/2013</datetime>
    	private string GetDisplayOrderList(int parentId, int categoryId)
    	{
    		string DisplayOrderList = "";

			List<CSPShowcase_Staging> stagingInfos = new List<CSPShowcase_Staging>();
			if (parentId > 0)
				stagingInfos = DnnData.CSPShowcase_Stagings.Where(a => a.ParentCategoryId == parentId && a.LanguageId == _langId &&
																	(a.IsBackupVersion == null || a.IsBackupVersion == false) &&
																	a.PortalId == PortalId && a.TabModuleId == TabModuleId).OrderByDescending(a => a.DateTime).ToList();
			else
			{
				stagingInfos =
					DnnData.CSPShowcase_Stagings.Where(a => a.CategoryId == categoryId && a.LanguageId == _langId &&
														a.PortalId == PortalId && a.TabModuleId == TabModuleId).OrderByDescending(a => a.DateTime).ToList();
				if (!stagingInfos.Any())
				{
					parentId = (int)CspDataContext.categories.FirstOrDefault(a => a.categoryId == categoryId).parentId;
					return GetDisplayOrderList(parentId, 0);
				}
			}
			if (stagingInfos.Any())
				DisplayOrderList = stagingInfos.First().DisplayOrder;
			else
			{
				List<category> categoriesList = CspDataContext.categories.Where(a => a.parentId == parentId).OrderBy(a => a.DisplayOrder).ToList();
				if (categoriesList.Any())
				{
					foreach (category categoryItem in categoriesList)
					{
						if (!String.IsNullOrEmpty(DisplayOrderList))
							DisplayOrderList += "-";
						DisplayOrderList += categoryItem.categoryId.ToString();
					}
				}
			}
    		return DisplayOrderList;
    	}

    	/// <summary>
    	/// Sets the category status.
    	/// </summary>
    	/// <param name="categoryId">The category id.</param>
    	/// <param name="status">The status.</param>
    	/// <param name="isDoingPublish"> </param>
    	/// <returns></returns>
    	/// <author>ducuytran - 3/2/2013</author>
    	private int SetCategoryStatus(int categoryId, int status, bool isDoingPublish = false)
		{
			if (categoryId < 1)
				return 0;

			category targetCategory = CspDataContext.categories.FirstOrDefault(a => a.categoryId == categoryId);

			if (targetCategory == null)
				return 0;

			if (!PublishOnFly && !isDoingPublish)
			{
				SetCategoryStatusStaging(targetCategory, status);
				if (status != 1)
				{
					List<category> subCategoriesList = CspDataContext.categories.Where(a => a.parentId == categoryId).ToList();
					if (subCategoriesList.Count > 0)
					{
						foreach (category subItem in subCategoriesList)
						{
							SetCategoryStatus(subItem.categoryId, status);
						}
					}
				}
				return 1;
			}

			List<content> contentPages;

			if (status != 1)
			{
				targetCategory.active = false;
				List<content> categoryContents =
					CspDataContext.contents.Where(
						a =>
						a.content_main.content_categories.Any(b => b.category_Id == categoryId) &&
						a.content_main.content_types_Id == _ctContentCategoryId && a.stage_Id == STAGE_PUBLISHED).ToList();
				if (categoryContents.Any())
				{
					foreach (content categoryContent in categoryContents)
					{
						categoryContent.stage_Id = STAGE_UNPUBLISHED;
					}
				}
				contentPages =
					CspDataContext.contents.Where(
						a =>
						a.content_main.content_categories.Any(b => b.category_Id == categoryId) &&
						a.content_main.content_types_Id == _ctContentPageId && a.stage_Id == STAGE_PUBLISHED).ToList();
			}
			else
			{
				targetCategory.active = true;
				content categoryContent = CspDataContext.contents.FirstOrDefault(a =>
				                                                                 a.content_main.content_categories.Any(
				                                                                 	b => b.category_Id == categoryId) &&
				                                                                 a.content_main.content_types_Id ==
																				 _ctContentCategoryId && a.content_types_languages_Id == _langId && a.stage_Id != STAGE_STAGING);
				if (categoryContent != null)
					categoryContent.stage_Id = STAGE_PUBLISHED;

				contentPages =
					CspDataContext.contents.Where(
						a =>
						a.content_main.content_categories.Any(b => b.category_Id == categoryId) &&
						a.content_main.content_types_Id == _ctContentPageId && a.content_types_languages_Id == _langId && a.stage_Id != STAGE_STAGING).ToList();
			}
			
			if (contentPages.Any())
			{
				foreach (content contentPage in contentPages)
				{
					if (IsInUseByCategories(contentPage) && status != 1)
						continue;
					contentPage.stage_Id = status != 1 ? STAGE_UNPUBLISHED : STAGE_PUBLISHED;
				}
			}

			if (status != 1 && PublishOnFly)
			{
				List<category> subCategoriesList = CspDataContext.categories.Where(a => a.parentId == categoryId).ToList();
				if (subCategoriesList.Count > 0)
				{
					foreach (category subItem in subCategoriesList)
					{
						SetCategoryStatus(subItem.categoryId, status, isDoingPublish);
					}
				}
			}
    		return 1;
		}

		/// <summary>
		/// Sets the category status staging.
		/// </summary>
		/// <param name="targetCategory">The target category.</param>
		/// <param name="status">The status.</param>
		/// <author>ducuytran</author>
		/// <datetime>7/2/2013</datetime>
    	private void SetCategoryStatusStaging(category targetCategory, int status)
    	{
    		CSPShowcase_Staging lastStaging = GetLatestStagingContent(targetCategory.categoryId);
    		CSPShowcase_Staging newStaging = null;
			bool newStatus = status == 1;
    		int logActionId = (status == 1) ? (int) LogAction.GoActive : (int) LogAction.GoInactive;
			if (lastStaging == null)
			{
				if (newStatus != targetCategory.active)
					newStaging = InitFirstStaging(targetCategory);
			}
			else
			{
				if (lastStaging.Status == logActionId)
					return;
				newStaging = new CSPShowcase_Staging
													{
														Id = Guid.NewGuid(),
														ParentCategoryId = lastStaging.ParentCategoryId,
														CategoryId = targetCategory.categoryId,
														DisplayOrder = GetDisplayOrderList((int)targetCategory.parentId, 0),
														SourceContentCategoryId = lastStaging.SourceContentCategoryId,
														ContentCategoryId = lastStaging.ContentCategoryId,
														ContentCategoryMainId = lastStaging.ContentCategoryMainId,
														SourceContentPageId = lastStaging.SourceContentPageId,
														ContentPageId = lastStaging.ContentPageId,
														ContentPageMainId = lastStaging.ContentPageMainId,
														LanguageId = _langId,
														Published = false,
														DateTime = DateTime.Now,
														PortalId = PortalId,
														TabModuleId = TabModuleId
													};
			}
			if (newStaging == null)
				return;
    		newStaging.ActionId = logActionId;
			newStaging.Status = logActionId;
			DnnData.CSPShowcase_Stagings.InsertOnSubmit(newStaging);
			DnnData.SubmitChanges(ConflictMode.FailOnFirstConflict);
    	}

    	/// <summary>
    	/// Sets the contents status.
    	/// </summary>
    	/// <param name="categoryId">The category id.</param>
    	/// <param name="status">The status.</param>
    	/// <param name="isDoingPublish"> </param>
    	/// <returns></returns>
    	/// <author>ducuytran - 4/1/2013</author>
    	private int SetContentsStatus(int categoryId, int status, bool isDoingPublish = false)
		{
			if (categoryId < 1)
				return 0;

			bool isMultipleAssigned = false;
			
			category targetCategory = CspDataContext.categories.FirstOrDefault(a => a.categoryId == categoryId);

			if (targetCategory == null)
				return 0;

			if (!PublishOnFly && !isDoingPublish)
			{
				SetContentsStatusStaging(targetCategory, status);
				if (status != 1)
				{
					List<category> subCategoriesList = CspDataContext.categories.Where(a => a.parentId == categoryId).ToList();
					if (subCategoriesList.Count > 0)
					{
						foreach (category subItem in subCategoriesList)
						{
							SetContentsStatus(subItem.categoryId, status);
						}
					}
				}
				return 1;
			}

			content categoryContent =
				CspDataContext.contents.FirstOrDefault(
					a =>
					a.content_main.content_categories.Any(b => b.category_Id == categoryId) &&
					a.content_main.content_types_Id == _ctContentCategoryId && a.content_types_languages_Id == _langId && a.stage_Id != STAGE_STAGING);
			List<content> contentPages =
				CspDataContext.contents.Where(
					a =>
					a.content_main.content_categories.Any(b => b.category_Id == categoryId) &&
					a.content_main.content_types_Id == _ctContentPageId && a.content_types_languages_Id == _langId && a.stage_Id != STAGE_STAGING).ToList();

			if (categoryContent != null)
				categoryContent.stage_Id = status != 1 ? STAGE_UNPUBLISHED : STAGE_PUBLISHED;

			if (contentPages.Any())
			{
				foreach (content contentPage in contentPages)
				{
					if (status != 1 && IsInUseByCategories(contentPage))
					{
						isMultipleAssigned = true;
						continue;
					}
					contentPage.stage_Id = status != 1 ? STAGE_UNPUBLISHED : STAGE_PUBLISHED;
				}
			}
			if (status == 1)
				targetCategory.active = true;
			else if (isMultipleAssigned)
				targetCategory.active = false;

			if (status != 1 && PublishOnFly)
			{
				List<category> subCategoriesList = CspDataContext.categories.Where(a => a.parentId == categoryId).ToList();
				if (subCategoriesList.Count > 0)
				{
					foreach (category subItem in subCategoriesList)
					{
						SetContentsStatus(subItem.categoryId, status, isDoingPublish);
					}
				}
			}

			return 1;
		}

		/// <summary>
		/// Sets the contents status staging.
		/// </summary>
		/// <param name="targetCategory">The target category.</param>
		/// <param name="status">The status.</param>
		/// <author>ducuytran</author>
		/// <datetime>7/2/2013</datetime>
    	private void SetContentsStatusStaging(category targetCategory, int status)
    	{
			CSPShowcase_Staging lastStaging = GetLatestStagingContent(targetCategory.categoryId);
			CSPShowcase_Staging newStaging = null;
			bool newStatus = status == 1;
			int logActionId = (status == 1) ? (int)LogAction.GoPublish : (int)LogAction.GoUnpublish;
			if (lastStaging == null)
			{
				content srcContentCategory = CspDataContext.contents.FirstOrDefault(a => a.content_main.content_categories.Any(b => b.category_Id == targetCategory.categoryId)
																		&& a.content_main.content_types_Id == _ctContentCategoryId && a.content_types_languages_Id == _langId
																		&& a.stage_Id != STAGE_STAGING);
				content srcContentPage = CspDataContext.contents.FirstOrDefault(a => a.content_main.content_categories.Any(b => b.category_Id == targetCategory.categoryId)
																			&& a.content_main.content_types_Id == _ctContentPageId && a.content_types_languages_Id == _langId
																			&& a.stage_Id != STAGE_STAGING);
				bool crrStatus = srcContentCategory.stage_Id == STAGE_PUBLISHED;
				if (newStatus != crrStatus)
					newStaging = InitFirstStaging(targetCategory);
			}
			else
			{
				if (lastStaging.Status == logActionId)
					return;
				newStaging = new CSPShowcase_Staging
				{
					Id = Guid.NewGuid(),
					ParentCategoryId = lastStaging.ParentCategoryId,
					CategoryId = targetCategory.categoryId,
					DisplayOrder = GetDisplayOrderList((int)targetCategory.parentId, 0),
					SourceContentCategoryId = lastStaging.SourceContentCategoryId,
					ContentCategoryId = lastStaging.ContentCategoryId,
					ContentCategoryMainId = lastStaging.ContentCategoryMainId,
					SourceContentPageId = lastStaging.SourceContentPageId,
					ContentPageId = lastStaging.ContentPageId,
					ContentPageMainId = lastStaging.ContentPageMainId,
					LanguageId = _langId,
					Published = false,
					DateTime = DateTime.Now,
					PortalId = PortalId,
					TabModuleId = TabModuleId
				};
			}
			if (newStaging == null)
				return;
			newStaging.ActionId = logActionId;
			newStaging.Status = logActionId;
			DnnData.CSPShowcase_Stagings.InsertOnSubmit(newStaging);
			DnnData.SubmitChanges(ConflictMode.FailOnFirstConflict);
    	}

    	/// <summary>
		/// Publishes the parent nodes.
		/// </summary>
		/// <param name="categoryId">The category id.</param>
		/// <author>ducuytran - 4/23/2013</author>
		private void PublishParentNodes(int categoryId, bool isDoingPublish = false)
		{
			category crrCategory = CspDataContext.categories.FirstOrDefault(a => a.categoryId == categoryId);
			category targetCategory = CspDataContext.categories.FirstOrDefault(a => a.categoryId == crrCategory.parentId);
			if (targetCategory == null || categoryId == _rootCategoryId || targetCategory.parentId <= 0)
				return;
			SetContentsStatus(targetCategory.categoryId, 1, isDoingPublish);
			PublishParentNodes((int)targetCategory.categoryId, isDoingPublish);
		}

		/// <summary>
		/// Activates the parent nodes.
		/// </summary>
		/// <param name="categoryId">The category id.</param>
		/// <author>ducuytran - 3/26/2013</author>
		private void ActivateParentNodes(int categoryId, bool isDoingPublish = false)
		{
			category targetCategory = CspDataContext.categories.FirstOrDefault(a => a.categoryId == categoryId);
			if (targetCategory == null || categoryId == _rootCategoryId)
				return;
			/*SetContentsStatus(categoryId, 1, isDoingPublish);
			targetCategory.active = true;*/
			SetCategoryStatus(categoryId, 1, isDoingPublish);
			ActivateParentNodes((int)targetCategory.parentId, isDoingPublish);
		}

        /// <summary>
        /// Gets the content of the category.
        /// </summary>
        /// <author>ducuytran - 2/27/2013</author>
        private void GetCategoryContent()
        {
            if (!String.IsNullOrEmpty(Request.Params["cat"]))
                int.TryParse(Request.Params["cat"], out CategoryId);
			if (!String.IsNullOrEmpty(Request.Params["lvl"]))
				int.TryParse(Request.Params["lvl"], out CategoryLevel);
            if (CategoryId < 1)
            {
            	CustomCategoryNode firstNode;
				if (CustomCategoryNodes.Where(a => a.ParentId == _rootCategoryId).Any())
				{
					firstNode = CustomCategoryNodes.Where(a => a.ParentId == _rootCategoryId && a.Active == true).OrderBy(a => a.DisplayOrder).First();
					CurrentCategory = CspDataContext.categories.FirstOrDefault(a => a.categoryId == firstNode.Id);
					CategoryId = firstNode.Id;
				}
            }
            else
            {
            	CurrentCategory = CspDataContext.categories.FirstOrDefault(a => a.categoryId == CategoryId);
				if (CurrentCategory == null)
					CategoryId = 0;
            }
            if (CategoryId < 1)
                return;
			int.TryParse(CurrentCategory.depth.ToString(), out CategoryLevel);
            content_category contentCat = CspDataContext.content_categories.FirstOrDefault(a => a.category_Id == CategoryId && a.content_main.content_types_Id == _ctContentPageId);
            if (contentCat == null)
                return;
        	List<ContentPageObj> contentList = GetPageContentList();// new List<ContentPageObj>();
            
			if (!contentList.Any())
            {
            	contentList.Add(new ContentPageObj
                                        {
                                            content_Id = Guid.Empty,
                                            Content_Page = GetLocalizedText("Text.NoContentPage")
                                        });
            }
            CategoryContentList.DataSource = contentList;
        }

		/// <summary>
		/// Gets the page content list.
		/// </summary>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>6/18/2013</datetime>
		private List<ContentPageObj> GetPageContentList()
		{
			List<ContentPageObj> contentList = new List<ContentPageObj>();
			List<content> contentPages =
							CspDataContext.contents.Where(
								a =>
								a.content_main.content_categories.Any(b => b.category_Id == CategoryId) &&
								a.content_main.content_types_Id == _ctContentPageId &&
								a.content_types_languages_Id == _langId).ToList();
			if (!contentPages.Any()) return contentList;

			List<content> publishedPages = contentPages.Where(a => a.stage_Id != STAGE_STAGING).ToList();
			List<content> contentsToShow = new List<content>();
			if (PublishOnFly)
			{
				contentsToShow = publishedPages;
			}
			else
			{
				content contentPage = null;
				//Page created in staging mode
				if (!publishedPages.Any())
				{
					CSPShowcase_Staging latestStaging = GetLatestStagingContent(CategoryId);
					if (latestStaging != null)
					{
						contentPage = contentPages.FirstOrDefault(a => a.content_Id == latestStaging.SourceContentPageId);
						contentsToShow.Add(contentPage);
					}
				}
				else
				{
					foreach (content publishedPage in publishedPages)
					{
						contentPage = publishedPage;
						List<CSPShowcase_Staging> stagingInfo =
							DnnData.CSPShowcase_Stagings.Where(a => a.SourceContentPageId == publishedPage.content_Id &&
																(a.IsBackupVersion == false || a.IsBackupVersion == null) &&
																(a.IsOriginalVersion == false || a.IsOriginalVersion == null)).OrderByDescending(a => a.DateTime).ToList();
						if (stagingInfo.Any() && stagingInfo[0].ContentPageId != Guid.Empty)
						{
							contentPage = contentPages.FirstOrDefault(a => a.content_Id == stagingInfo[0].ContentPageId);
						}
						contentsToShow.Add(contentPage);
					}
				}
			}

			foreach (content contentPage in contentsToShow)
			{
				contentList.Add(new ContentPageObj
				{
					content_Id = contentPage.content_Id,
					Content_Page = GetContentPageValue(contentPage)
				});
			}
			return contentList;
		}

		/// <summary>
		/// Gets the content page value.
		/// </summary>
		/// <param name="contentPage">The content page.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>6/30/2013</datetime>
		private string GetContentPageValue(content contentPage)
		{
			String contentPageText = GetContentFieldValue(contentPage, "Content_Page");
			HtmlDocument htmlDoc = new HtmlDocument();
			htmlDoc.LoadHtml(contentPageText);
			List<HtmlNode> scriptNodes = htmlDoc.DocumentNode.Descendants().Where(n => n.Name == "script").ToList();
			if (scriptNodes.Any())
			{
				for (int i = scriptNodes.Count - 1; i >= 0; i--)
				{
					scriptNodes[i].ParentNode.RemoveChild(scriptNodes[i]);
				}
				contentPageText = htmlDoc.DocumentNode.OuterHtml;
			}

			List<HtmlNode> iframeNodes = htmlDoc.DocumentNode.Descendants().Where(n => n.Name == "iframe").ToList();
			if (iframeNodes.Any())
			{
				for (int j = iframeNodes.Count - 1; j >= 0; j--)
				{
					if (iframeNodes[j].Attributes["id"] == null)
						iframeNodes[j].Attributes.Add("id", "iframe-" + j);
				}
				contentPageText = htmlDoc.DocumentNode.OuterHtml;
			}
			return contentPageText;
		}

		/// <summary>
		/// Gets the left category list.
		/// </summary>
		/// <author>ducuytran - 2/28/2013</author>
		private void GetSideCategoryList()
		{
			if (CategoryId < 1 || CurrentCategory == null)
				return;
			List<CustomCategoryNode> LeftList = new List<CustomCategoryNode>();
			bool isActiveFilter = String.IsNullOrEmpty(Request.Params["showall"]);
			if (isActiveFilter == true)
				LeftList = CustomCategoryNodes.Where(a => a.ParentId == CategoryId && a.Active == true).ToList();
			else
			{
				LeftList = CustomCategoryNodes.Where(a => a.ParentId == CategoryId).ToList();
			}
			if (LeftList.Count == 0)
			{
				if (isActiveFilter == true)
					LeftList = CustomCategoryNodes.Where(a => a.ParentId == CurrentCategory.parentId && a.Active == isActiveFilter).OrderBy(a => a.DisplayOrder).ThenByDescending(a => a.Id).ToList();
				else
				{
					LeftList = CustomCategoryNodes.Where(a => a.ParentId == CurrentCategory.parentId).OrderBy(a => a.DisplayOrder).ThenByDescending(a => a.Id).ToList();
				}
			}
			//LeftCategoryList.DataSource = LeftList;
		}

		/// <summary>
		/// Gets the last order.
		/// </summary>
		/// <param name="parentId">The parent id.</param>
		/// <returns></returns>
		/// <author>ducuytran - 3/10/2013</author>
		private int GetLastOrder(int parentId)
		{
			List<category> childrenCats =
				CspDataContext.categories.Where(a => a.parentId == parentId).OrderByDescending(a => a.DisplayOrder).ToList();
			if (childrenCats.Count == 0)
				return 0;
			category lastChild = childrenCats.First();
			return Convert.ToInt32(lastChild.DisplayOrder);
		}

    	/// <summary>
    	/// Adds the category.
    	/// </summary>
    	/// <param name="parentid">The parentid.</param>
    	/// <param name="cattext">The cattext.</param>
    	/// <param name="catdesc"> </param>
    	/// <param name="active">if set to <c>true</c> [active].</param>
    	/// <param name="issubscribable">if set to <c>true</c> [issubscribable].</param>
    	/// <param name="displayorder">The displayorder.</param>
    	/// <returns></returns>
    	/// Author: Vu Dinh
    	/// 2/1/2013 - 5:42 PM
    	/// Updater: Uy Tran
    	/// _ Create category content & content page
    	private int AddCategory(int parentid, string cattext, string catdesc, string thumbnail, bool active, bool issubscribable, int displayorder)
    	{
    		cattext = Server.HtmlEncode(cattext);
            var cat = new category
                          {
							  parentId = parentid,
							  categoryText = cattext,
							  description = cattext,
							  ExternalCategoryCode = cattext,
                              dateAdded = DateTime.Now,
							  active = active,
                              publication_schemeID = 11,
                              apply_to_childs = false,
							  IsSubscribable = issubscribable,
							  DisplayOrder = displayorder
                          };
            CspDataContext.categories.InsertOnSubmit(cat);
            try
            {
                CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
				//Add to Theme and Consumer Themes
                AddCategoryToThemeCatAndConsumerThemes(cat);
				//Insert Category Content
				AddCategoryContent(cat, "", catdesc, thumbnail);
				//Insert Content Page
				AddContentPage(cat);
				if (!PublishOnFly)
					CreateStagingForNewCategory(cat);
            }
            catch (Exception exc)
            {
                return 0;
            }
            return cat.categoryId;
        }

		/// <summary>
		/// Creates the staging for new category.
		/// </summary>
		/// <param name="cat">The cat.</param>
		/// <author>ducuytran</author>
		/// <datetime>6/29/2013</datetime>
    	private void CreateStagingForNewCategory(category cat)
		{
			cat.active = true;
			content contentCategory = CspDataContext.contents.FirstOrDefault(a =>
										a.content_main.content_categories.Any(b => b.category_Id == cat.categoryId) &&
										a.content_main.content_types_Id == _ctContentCategoryId &&
										a.content_types_languages_Id == _langId && a.stage_Id != STAGE_STAGING);
			content contentPage = CspDataContext.contents.FirstOrDefault(a =>
										a.content_main.content_categories.Any(b => b.category_Id == cat.categoryId) &&
										a.content_main.content_types_Id == _ctContentPageId &&
										a.content_types_languages_Id == _langId && a.stage_Id != STAGE_STAGING);
			contentCategory.stage_Id = STAGE_STAGING;
			contentPage.stage_Id = STAGE_STAGING;
			_cspUtils.SetContentFieldValue(_ctContentCategoryId, contentCategory, CTF_CATEGORYCONTENT_STAGING, "Staging");
			_cspUtils.SetContentFieldValue(_ctContentPageId, contentPage, CTF_CONTENTPAGE_STAGING, "Staging");

			String DisplayOrderList = GetDisplayOrderList((int) cat.parentId, 0);
			/*List<category> categoriesList = CspDataContext.categories.Where(a => a.parentId == cat.parentId).OrderBy(a => a.DisplayOrder).ToList();
			if (categoriesList.Any())
			{
				foreach (category theCategory in categoriesList)
				{
					if (!String.IsNullOrEmpty(DisplayOrderList))
						DisplayOrderList += "-";
					DisplayOrderList += theCategory.categoryId.ToString();
				}
			}*/

			CSPShowcase_Staging newStaging = new CSPShowcase_Staging
																{
																	Id = Guid.NewGuid(),
																	ParentCategoryId = (int) cat.parentId,
																	CategoryId = cat.categoryId,
																	DisplayOrder = DisplayOrderList,
																	SourceContentCategoryId = contentCategory.content_Id,
																	ContentCategoryId = Guid.Empty,
																	ContentCategoryMainId = contentCategory.content_main_Id,
																	SourceContentPageId = contentPage.content_Id,
																	ContentPageId = Guid.Empty,
																	ContentPageMainId = contentPage.content_main_Id,
																	LanguageId = _langId,
																	Published = false,
																	DateTime = DateTime.Now,
																	PortalId = PortalId,
																	TabModuleId = TabModuleId,
																	ActionId = (int) LogAction.Created
																};
			DnnData.CSPShowcase_Stagings.InsertOnSubmit(newStaging);
			DnnData.SubmitChanges(ConflictMode.FailOnFirstConflict);
		}

    	/// <summary>
		/// Reorders the category.
		/// </summary>
		/// <returns></returns>
		/// <author>ducuytran - 4/3/2013</author>
		private int ReorderCategory()
		{
			int resultCode = -1, tempId;
			string[] idMixed = Request.Params["orderlist"].Split(Convert.ToChar("_"));
    		int categoryId = int.TryParse(Request.Params["categoryid"], out tempId) ? tempId : 0;
			int actionLogId = (int) LogAction.Move;
			string actionLogComment = "";
			if (idMixed.Count() == 0)
				return resultCode;
			if (PublishOnFly)
			{
				List<category> targetCat = new List<category>();
				List<content> targetCatContent = new List<content>();
				for (int i = 0; i < idMixed.Count(); i++)
				{
					resultCode = 0;
					int newOrder = i + 1;
					category tmpCat = CspDataContext.categories.FirstOrDefault(a => a.categoryId == Convert.ToInt32(idMixed[i]));
					if (tmpCat == null)
					{
						continue;
					}
					tmpCat.DisplayOrder = newOrder;
					resultCode = 1;
					List<content> categoryContents =
						CspDataContext.contents.Where(
							a =>
							a.content_main.content_categories.Any(b => b.category_Id == tmpCat.categoryId) &&
							a.content_main.content_types_Id == _ctContentCategoryId).ToList();
					if (!categoryContents.Any())
						continue;
					foreach (content categoryContent in categoryContents)
					{
						_cspUtils.SetContentFieldValue(_ctContentCategoryId, categoryContent, "Status_Sort_Order",
						                               newOrder.ToString("0000"));
					}
				}
				CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
			}
			else
			{
				/*List<CSPShowcase_Staging> stagingList =
					DnnData.CSPShowcase_Stagings.Where(a => a.CategoryId == categoryId && a.LanguageId == _langId &&
														(a.IsBackupVersion == false || a.IsBackupVersion == null) &&
														(a.IsOriginalVersion == false || a.IsOriginalVersion == null) &&
														a.PortalId == PortalId && a.TabModuleId == TabModuleId).OrderByDescending(a => a.DateTime).ToList();*/
				CSPShowcase_Staging lastStaging = GetLatestStagingContent(categoryId);
				CSPShowcase_Staging newStaging = null;
				String displayOrder = Request.Params["orderlist"].Replace('_', '-');

				if (lastStaging == null)
				{
					newStaging = InitFirstStaging(categoryId, displayOrder);
				}
				else
				{
					CSPShowcase_Staging latestStaging = lastStaging;
					newStaging = new CSPShowcase_Staging
														{
															Id = Guid.NewGuid(),
															ParentCategoryId = latestStaging.ParentCategoryId,
															CategoryId = latestStaging.CategoryId,
															DisplayOrder = displayOrder,
															SourceContentCategoryId = latestStaging.SourceContentCategoryId,
															ContentCategoryId = latestStaging.ContentCategoryId,
															ContentCategoryMainId = latestStaging.ContentCategoryMainId,
															SourceContentPageId = latestStaging.SourceContentPageId,
															ContentPageId = latestStaging.ContentPageId,
															ContentPageMainId = latestStaging.ContentPageMainId,
															LanguageId = _langId,
															Published = false,
															DateTime = DateTime.Now,
															PortalId = PortalId,
															TabModuleId = TabModuleId,
															Status = latestStaging.Status
														};
				}

				if (newStaging != null)
				{
					//Get new order # for logging
					List<String> displayOrderArr = displayOrder.Split('-').ToList();
					int newOrder = displayOrderArr.IndexOf(categoryId.ToString()) + 1;

					resultCode = 1;
					newStaging.ActionId = actionLogId;
					newStaging.ActionComment = GetLocalizedText("ActionLogComment.Move").Replace("[new_order]", newOrder.ToString());
					DnnData.CSPShowcase_Stagings.InsertOnSubmit(newStaging);
					DnnData.SubmitChanges(ConflictMode.FailOnFirstConflict);
				}
			}
			return resultCode;
		}

		/// <summary>
		/// Adds the content of the missing.
		/// </summary>
		/// <param name="categoryId">The category id.</param>
		/// <returns></returns>
		/// <author>ducuytran - 4/3/2013</author>
		private int AddMissingContent(int categoryId)
		{
			category cat = CspDataContext.categories.FirstOrDefault(a => a.categoryId == categoryId);
			if (cat == null)
				return 0;
			content_main contentMainCategoryContent =
					CspDataContext.content_mains.FirstOrDefault(a => a.content_categories.Any(b => b.category_Id == categoryId) && a.content_types_Id == _ctContentCategoryId && a.contents.Any(c => c.content_main_Id == a.content_main_Id));
			content_main contentMainContentPage =
				CspDataContext.content_mains.FirstOrDefault(a => a.content_categories.Any(b => b.category_Id == categoryId) && a.content_types_Id == _ctContentPageId && a.contents.Any(c => c.content_main_Id == a.content_main_Id));
			//return 1;
			if (contentMainCategoryContent == null)
			{
				//Insert Category Content
				AddCategoryContent(cat);
			}
			else
			{
				AddCategoryContent(cat, contentMainCategoryContent);
			}
			if (contentMainContentPage == null)
			{
				//Insert Content Page
				AddContentPage(cat);
			}
			else
			{
				AddContentPage(cat, contentMainContentPage);
			}
			if (!PublishOnFly)
				CreateStagingForNewCategory(cat);
			return 1;
		}

		private int AddMissingContents()
		{
			int resultCode = 0;
			string categoryIds = Request.Params["categoryids"].ToString();
			string[] idList = categoryIds.Split(Convert.ToChar(";"));
			foreach (string categoryId in idList)
			{
				int tmp = Convert.ToInt32(categoryId);
				resultCode = AddMissingContent(tmp);
			}
			return resultCode;
		}

        /// <summary>
        /// Adds the category to theme cat and consumer themes.
        /// </summary>
        /// <param name="cat">The cat.</param>
        /// Author: Vu Dinh
        /// 2/1/2013 - 6:18 PM
        /// Updater:
        /// _ @uytran 2013/03/05: Add level 1 item into themes_category
        private void AddCategoryToThemeCatAndConsumerThemes(category cat)
        {
            //add category to themes_categories
            var parentCat = CspDataContext.categories.SingleOrDefault(a => a.categoryId == cat.parentId);
            if (parentCat != null)
            {
                List<themes_category> themeCat = CspDataContext.themes_categories.Where(a => a.category_Id == parentCat.categoryId).ToList();
                var themeCatToInsert = new List<themes_category>();
				if (parentCat.categoryId == _rootCategoryId)
				{
					category firstSiblingItem = CspDataContext.categories.FirstOrDefault(a => a.parentId == parentCat.categoryId && a.categoryId != cat.categoryId);
					if (firstSiblingItem != null)
					{
						themes_category firstItem = CspDataContext.themes_categories.FirstOrDefault(a => a.category_Id == firstSiblingItem.categoryId);
						if (firstItem != null)
						{
							themeCatToInsert.Add(new themes_category
							                     	{
							                     		themes_Id = firstItem.themes_Id,
							                     		category_Id = cat.categoryId
							                     	});
						}
					}
					else
					{
						foreach(int themeId in _themeIds)
						{
							themeCatToInsert.Add(new themes_category
							{
								themes_Id = themeId,
								category_Id = cat.categoryId
							});
						}
					}
				}
				else
				{
					foreach (var themesCategory in themeCat)
					{
						themeCatToInsert.Add(new themes_category
						                     	{
						                     		themes_Id = themesCategory.themes_Id,
						                     		category_Id = cat.categoryId
						                     	});
					}
				}
            	if (themeCatToInsert.Any())
                {
                    CspDataContext.themes_categories.InsertAllOnSubmit(themeCatToInsert);
                    CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                }
            }

            //add category to consumers_themes
            var companies = CspDataContext.companies.ToList();
            var listConsumerTheme = new List<consumers_theme>();

            foreach (var company in companies)
            {
				if (parentCat.categoryId == _rootCategoryId && cat.IsSubscribable == false)
				{
					//Check if this theme was assigned to the company
					theme catTheme =
						CspDataContext.themes.FirstOrDefault(a => a.themes_categories.Any(b => b.category_Id == cat.categoryId));
					companies_theme assignedCompanyTheme = CspDataContext.companies_themes.FirstOrDefault(a => a.theme == catTheme && a.company == company);
					if (assignedCompanyTheme == null)
						continue;
					List<themes_supplier> suppliers =
						CspDataContext.themes_suppliers.Where(a => a.themes_Id == catTheme.themes_Id).ToList();
					//consumers_theme sampleConsumerTheme = CspDataContext.consumers_themes.FirstOrDefault(a => a.company == company && a.theme == catTheme);
					if (suppliers.Count == 0)
						continue;
					foreach (themes_supplier themesSupplier in suppliers)
					{
						listConsumerTheme.Add(new consumers_theme
						{
							category_Id = cat.categoryId,
							consumer_Id = company.companies_Id,
							themes_Id = catTheme.themes_Id,
							supplier_Id = themesSupplier.companies_Id
						});
					}
				}
				else
				{
					foreach (var consumerTheme in CspDataContext.consumers_themes.Where(a => a.company == company && a.category_Id == parentCat.categoryId))
					{
						listConsumerTheme.Add(new consumers_theme
						{
							category_Id = cat.categoryId,
							consumer_Id = company.companies_Id,
							themes_Id = consumerTheme.themes_Id,
							supplier_Id = consumerTheme.supplier_Id
						});
					}
				}
			}
            if (listConsumerTheme.Any())
            {
                CspDataContext.consumers_themes.InsertAllOnSubmit(listConsumerTheme);
                CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
            }
        }

		/// <summary>
		/// Adds the content of the category.
		/// </summary>
		/// <param name="cat">The cat.</param>
		/// <author>ducuytran - 3/4/2013</author>
		private void AddCategoryContent(category cat, string newTitle = "", string descriptionShort = "", string thumbnail = "")
		{
			int supplierId = 12;
			int consumerId = 0;

			Channel channel = CspDataContext.Channels.FirstOrDefault(a => a.ExternalIdentifier == _externalIdentifier);
			if (channel != null)
				supplierId = channel.CompanyId;
			
			content_main newContentMain = new content_main
			                              	{
												content_main_Id = Guid.NewGuid(),
			                              		content_main_key = cat.categoryText,
												content_identifier = cat.categoryText,
												content_types_Id = _ctContentCategoryId,
												content_staging_scheme_Id = _contentStagingSchemeId,
												supplier_Id = supplierId,
												consumer_Id = consumerId
			                              	};

			content newContentCategory = new content
			                             	{
												content_Id = Guid.NewGuid(),
												content_main = newContentMain,
												active = true,
												stage_Id = STAGE_PUBLISHED,
												content_types_languages_Id = _langId
			                             	};
			newContentMain.content_categories.Add(new content_category
			                                      	{
			                                      		content_main = newContentMain,
														category = cat,
														publication_scheme_Id = _publicationSchemeId
			                                      	});
			List<content_types_field> fieldDefs =
				CspDataContext.content_types_fields.Where(a => a.content_types_Id == _ctContentCategoryId).ToList();
			string valueText = "";
			foreach (content_types_field contentTypesField in fieldDefs)
			{
				if (contentTypesField.fieldname == "Content_Title")
				{
					valueText = cat.categoryText;
					if (!String.IsNullOrEmpty(newTitle))
						valueText = Server.HtmlEncode(newTitle);
				}
				else if (contentTypesField.fieldname == "Content_Description_Short" || contentTypesField.fieldname == "Content_Description")
					valueText = Server.HtmlEncode(descriptionShort);
				else if (contentTypesField.fieldname == "Content_Image_Thumbnail")
					valueText = thumbnail;
				else if (contentTypesField.fieldname == "Status_Sort_Order")
					valueText = ((int)cat.DisplayOrder).ToString("0000");
				else
				{
					valueText = "";
				}

				newContentCategory.content_fields.Add(new content_field
				                                      	{
				                                      		content_types_field = contentTypesField,
															content = newContentCategory,
															value_text = valueText
				                                      	});
			}

			CspDataContext.content_mains.InsertOnSubmit(newContentMain);
			CspDataContext.contents.InsertOnSubmit(newContentCategory);
			CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
		}

		/// <summary>
		/// Adds the content of the category.
		/// </summary>
		/// <param name="cat">The cat.</param>
		/// <param name="contentMain">The content main.</param>
		/// <author>ducuytran - 3/31/2013</author>
		private void AddCategoryContent(category cat, content_main contentMain)
		{
			content existCategoryContent = CspDataContext.contents.FirstOrDefault(a =>
			                                                                      a.content_main.content_categories.Any(
			                                                                      	b => b.category_Id == cat.categoryId) &&
			                                                                      a.content_main.content_types_Id ==
			                                                                      _ctContentCategoryId &&
			                                                                      a.content_types_languages_Id == _langId);
			if (existCategoryContent != null)
				return;
			content newContentCategory = new content
			{
				content_Id = Guid.NewGuid(),
				content_main = contentMain,
				active = true,
				stage_Id = STAGE_PUBLISHED,
				content_types_languages_Id = _langId
			};
			List<content_types_field> fieldDefs =
				CspDataContext.content_types_fields.Where(a => a.content_types_Id == _ctContentCategoryId).ToList();
			string valueText = "";
			foreach (content_types_field contentTypesField in fieldDefs)
			{
				if (contentTypesField.fieldname == "Content_Title" || contentTypesField.fieldname == "Content_Description")
				{
					valueText = cat.categoryText;
				}
				else if (contentTypesField.fieldname == "Status_Sort_Order")
					valueText = ((int)cat.DisplayOrder).ToString("0000");
				else
				{
					valueText = "";
				}

				newContentCategory.content_fields.Add(new content_field
				{
					content_types_field = contentTypesField,
					content = newContentCategory,
					value_text = valueText
				});
			}

			CspDataContext.contents.InsertOnSubmit(newContentCategory);
			CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
		}

		/// <summary>
		/// Adds the content page.
		/// </summary>
		/// <param name="cat">The cat.</param>
		/// <author>ducuytran - 3/4/2013</author>
		private void AddContentPage(category cat)
		{
			/*int publicationSchemeId = 11;
			int contentStagingSchemeId = 1;*/
			int supplierId = 12;
			int consumerId = 0;

			Channel channel = CspDataContext.Channels.FirstOrDefault(a => a.ExternalIdentifier == _externalIdentifier);
			if (channel != null)
				supplierId = channel.CompanyId;

			content_main newContentMain = new content_main
			{
				content_main_Id = Guid.NewGuid(),
				content_main_key = cat.categoryText,
				content_identifier = cat.categoryText,
				content_types_Id = _ctContentPageId,
				content_staging_scheme_Id = _contentStagingSchemeId,
				supplier_Id = supplierId,
				consumer_Id = consumerId
			};

			content newContentPage = new content
			{
				content_Id = Guid.NewGuid(),
				content_main = newContentMain,
				active = true,
				stage_Id = STAGE_PUBLISHED,
				content_types_languages_Id = _langId
			};
			newContentMain.content_categories.Add(new content_category
			                                      	{
			                                      		content_main = newContentMain,
														category = cat,
														publication_scheme_Id = _publicationSchemeId
			                                      	});
			List<content_types_field> fieldDefs =
				CspDataContext.content_types_fields.Where(a => a.content_types_Id == _ctContentPageId).ToList();
			string valueText = "";
			foreach (content_types_field contentTypesField in fieldDefs)
			{
				if (contentTypesField.fieldname == "Content_Page")
					valueText = "No content available";
				else
				{
					valueText = "";
				}

				newContentPage.content_fields.Add(new content_field
				{
					content_types_field = contentTypesField,
					content = newContentPage,
					value_text = valueText
				});
			}

			CspDataContext.content_mains.InsertOnSubmit(newContentMain);
			CspDataContext.contents.InsertOnSubmit(newContentPage);
			CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
		}

		/// <summary>
		/// Adds the content page.
		/// </summary>
		/// <param name="cat">The cat.</param>
		/// <param name="contentMain">The content main.</param>
		/// <author>ducuytran - 3/31/2013</author>
		private void AddContentPage(category cat, content_main contentMain)
		{
			content existContentPage = CspDataContext.contents.FirstOrDefault(a =>
																				  a.content_main.content_categories.Any(
																					b => b.category_Id == cat.categoryId) &&
																				  a.content_main.content_types_Id ==
																				  _ctContentPageId &&
																				  a.content_types_languages_Id == _langId);
			if (existContentPage != null)
				return;
			content newContentPage = new content
			{
				content_Id = Guid.NewGuid(),
				content_main = contentMain,
				active = true,
				stage_Id = STAGE_PUBLISHED,
				content_types_languages_Id = _langId
			};

			List<content_types_field> fieldDefs =
				CspDataContext.content_types_fields.Where(a => a.content_types_Id == _ctContentPageId).ToList();
			string valueText = "";
			foreach (content_types_field contentTypesField in fieldDefs)
			{
				if (contentTypesField.fieldname == "Content_Page")
					valueText = "No content available";
				else
				{
					valueText = "";
				}

				newContentPage.content_fields.Add(new content_field
				{
					content_types_field = contentTypesField,
					content = newContentPage,
					value_text = valueText
				});
			}

			CspDataContext.contents.InsertOnSubmit(newContentPage);
			CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
		}

		/// <summary>
		/// Originals the category count.
		/// </summary>
		/// <returns></returns>
		/// <author>ducuytran - 4/1/2013</author>
		protected int OriginalCategoryCount()
		{
			if (_rootCategoryId == 0)
				return CspDataContext.categories.Count(a => a.parentId != 0);
			return CspDataContext.categories.Count(a => a.categoryId != _rootCategoryId);
		}

		/// <summary>
		/// Determines whether [is in use by categories] [the specified the content].
		/// </summary>
		/// <param name="theContent">The content.</param>
		/// <returns>
		///   <c>true</c> if [is in use by categories] [the specified the content]; otherwise, <c>false</c>.
		/// </returns>
		/// <author>ducuytran</author>
		/// <datetime>5/2/2013</datetime>
		private bool IsInUseByCategories(content theContent)
		{
			bool isInUse = false;
			List<category> inUsedCategories =
							CspDataContext.categories.Where(
								a => a.content_categories.Any(b => b.content_main == theContent.content_main) && a.active == true).ToList();
			if (inUsedCategories.Any() && inUsedCategories.Count > 1)
				isInUse = true;
			return isInUse;
		}

		/// <summary>
		/// Gets the page man parameters.
		/// </summary>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>5/8/2013</datetime>
		protected string GetPageManParameters()
		{
			if (_rootCategoryId <= 0 || _themeIds == null || !_themeIds.Any())
				return String.Empty;
			string jsFiles = ControlPath + "js/jquery.min.js" + "," + _externalJSUrl;
			bool showAll = !String.IsNullOrEmpty(Request.Params["showall"]);
			string pageParams = String.Format("portalid={0}&rootcatid={1}&themeid={2}&ctcategorycontent={3}&lang={4}&jsfiles={5}&showall={6}&pof={7}&tabmoduleid={8}&ctcontentpage={9}", 
											PortalId, _rootCategoryId, _themeIds[0], _ctContentCategoryId, _langId, jsFiles, showAll, PublishOnFly, TabModuleId, _ctContentPageId);
			pageParams = CryptoServiceProvider.Encode(pageParams);
			pageParams = "hash=" + HttpUtility.UrlEncode(pageParams);
			return pageParams;
		}

		/// <summary>
		/// Gets the action string.
		/// </summary>
		/// <param name="actionId">The action id.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>7/7/2013</datetime>
		protected string GetActionString(int actionId)
		{
			string actionString = "";
			string prefix = "LogAction.";
			Logger theLog = new Logger();
			actionString = GetLocalizedText(prefix + theLog.GetLogName(actionId));
			return actionString;
		}

		/// <summary>
		/// Determines whether [is roll backable].
		/// </summary>
		/// <returns>
		///   <c>true</c> if [is roll backable]; otherwise, <c>false</c>.
		/// </returns>
		/// <author>ducuytran</author>
		/// <datetime>7/2/2013</datetime>
		protected bool IsRollBackable()
		{
			return DnnData.CSPShowcase_Stagings.Count(a => a.IsBackupVersion == true && a.PortalId == PortalId && a.TabModuleId == TabModuleId) > 0;
		}

    	public bool IsUrlAvailable(string url)
		{
			try
			{
				HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);


				using (HttpWebResponse rsp = (HttpWebResponse)req.GetResponse())
				{
					if (rsp.StatusCode == HttpStatusCode.OK)
					{
						return true;
					}
				}
			}
			catch (WebException)
			{
				// Eat it because all we want to do is return false
			}


			// Otherwise
			return false;
		}
    }
}