 function Category(cnid, depth, id, pid, text, displayOrder, lineage, level, desc, thumbnail, pn, rid) {
    this.cnid=cnid;
     this.depth = depth;
     this.id = id;
     this.pid = pid;
     this.text = text;
	 this.displayOrder=displayOrder;
	 this.lineage = lineage.split('/');
	 this.level = level;
	 this.desc = desc;
	 this.thumbnail = thumbnail;
	 this.pn = pn;
	 this.rid = rid;
	 this.href = '?p13(ct66000&cd' + id + '):c14(ct31000&cd133i99)[st(ct31000*Status_Sort_Order*)][]&cat=' + id + '&lv=' + level;
	 this.SearchUp = function(level){
		for(var i=0;i<this.lineage.length;i++){
			if(this.lineage[i]==this.id) {
				try{
					return this.lineage[i-level];
				}
				catch(e){
					return null;
				}
			}				
		}
		return null;
	 }
 }
 
 function getParameterByName(name)
{
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.search);
  if(results == null)
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}
 
$(document).ready(function() {
    setTimeout(function () {
        var a = document.getElementById('csp-tout-area');
        if (a && a.style) {
           a.style.height = a.offsetHeight + 'px'
        }    
    }, 500);  
});
 Array.prototype.findParentId = function(attrId, value) {
     for (var i = 0; i < this.length; i++) {
         if (this[i][attrId] == value) return i;
     }
     return -1;
 }
 Array.prototype.findByAttrs = function() {
     if (arguments.length % 2 != 0) throw "invalid name value pair arguments";
     var dict = [], i, j, r = false;
     for (i = 0; i < arguments.length; i = i + 2) {
         dict.push([arguments[i], arguments[i + 1]]);
     }
     for (i = 0; i < this.length; i++) {
         r = true;
         for (j = 0; j < dict.length; j++)
             r = r && (this[i][dict[j][0]] == dict[j][1]);
         if (r) break;
     }
     return (r ? i : -1);
 }
 
 var categories = {};
 (function(c) {
     c.list = [];
     c.Find = function(id) {
         var t = null;
         for (var i = 0; i < this.list.length; i++) {
             t = this.FindInTree(id, this.list[i]);
             if (t != null)
                 return t;
         }
         return null;
     }

     c.FindAll = function(id, level) {
         var t = null, results = [];
         for (var i = 0; i < this.list.length; i++) {
             t = this.FindAllInTree(id, level, this.list[i], results);
         }
         return results;
     }

     c.FindByAttr = function(name, value) {
         var t = null, results = [];
         for (var i = 0; i < this.list.length; i++) {
             this.FindInTreeByAttr(name, value, this.list[i], results);
         }
         return results;
     }
     c.FindInTreeByAttr = function(name, value, tree, results) {
         var _t = tree;
         if (_t == null) _t = this.list;
         if (_t[name] == value)
             results.push(_t);
         if (typeof (_t.children) == "undefined") return null;
         var r = null;
         for (var i = 0; i < _t.children.length; i++) {
             c.FindInTreeByAttr(name, value, _t.children[i], results);
         }
     }
     c.FindAllInTree = function(id, level, tree, results) {
         var _t = tree;
         if (_t == null) _t = this.list;
         if (_t.id == id) {
             if (level != null && _t.level == level)
                 results.push(_t);
             else if (level == null)
                 results.push(_t);
         }
         if (typeof (_t.children) == "undefined") return null;
         var r = null;
         for (var i = 0; i < _t.children.length; i++) {
             c.FindAllInTree(id, level, _t.children[i], results);
         }
     }
     c.FindInTree = function(id, tree) {
         var _t = tree;
         if (_t == null) _t = this.list;
         if (_t.id == id) return _t;
         if (typeof (_t.children) == "undefined") return null;
         var r = null;
         for (var i = 0; i < _t.children.length; i++) {
             r = c.FindInTree(id, _t.children[i]);
             if (r != null)
                 return r;
         }
         return null;
     }
     c.Add = function(cnid, depth, id, pid, text, displayOrder, lineage, level, desc, thumbnail, pn, rid) {
         var len = this.list.length;
         if (len == 0 || (len > 0 && this.list[len - 1].depth <= depth))
             this.list.push(new Category(cnid, depth, id, pid, text, displayOrder, lineage, level, desc, thumbnail, pn, rid));
         else if (len > 0) {
             for (var i = 0; i < len; i++) {
                 if (this.list[i].depth >= depth) {
                     this.list.splice(i, 0, new Category(cnid, depth, id, pid, text, displayOrder, lineage, level, desc, thumbnail, pn, rid));
                     break;
                 }
             }
         }
     }
     c.Nest = function() {
         if (this.list.length > 0) {
             var i = this.list.length - 1, id, j, k;
             while (i >= 0) {
                 id = this.list.findParentId("id", this.list[i].pid);
                 if (id != -1) {
                     var temp = this.list.splice(i, 1);
                     if (typeof (this.list[id]["children"]) == "undefined")
                         this.list[id]["children"] = [];
                     temp[0].parent = this.list[id];
                     this.list[id].children.push(temp[0]);
                 }
                 i--;
             }
             //if we are missing a level, then we will automatically pull them back
             for (i = 0; i < this.list.length; i++) {
                 if (typeof (this.list[i].children) == "undefined") continue;
                 j = this.list[i].children.length - 1;
                 var cat, pcat;
                 while (j >= 0) {
                     cat = this.list[i].children[j];
                     if (cat.level > 1) {
                         // find the sibling that has the same level
                         for (k = 0; k < this.list[i].children.length; k++) {
                             if (this.list[i].children[k].level < cat.level && this.list[i].children[k].id == cat.id)
                                 break;
                             continue;
                         }
                         if (k < this.list[i].children.length) {
                             pcat = this.list[i].children[k];
                             if (typeof (pcat.children) == "undefined") pcat.children = [];
                             cat.parent = pcat;
                             pcat.children.push(cat);
                             this.list[i].children.splice(j, 1);
                         }
                     }
                     j--;
                 }
             }
         }
     }
     c.sort = function(c1, c2) {
         if (c1.displayOrder == c2.displayOrder) return 0;
         if (c1.displayOrder > c2.displayOrder) return 1;
         if (c1.displayOrder < c2.displayOrder) return -1;
         return 0;
     }
     c.Render = function(elementId) {
         c.Nest();
		this.list.sort(this.sort);
		var el = $(elementId);
		if (el.length > 0) {
			var html = "", i = 0, subNavHtml = "", j = 0, k = 0, s = "";
			var cat, ccat, gccat;
			html += '<ul id="main-nav" class="the-main-menu">';
			var rootItems = this.FindByAttr('level', 1);
			for (i = 0; i < rootItems.length; i++) {
				cat = rootItems[i];
				html += '<li class="root-item">';
				html += '<a class="root-item-link" cateid="' + cat.id + '" href="' + cat.href + '">' + cat.text + '</a>';
				if (typeof(cat.children) != "undefined") {
					cat.children.sort(this.sort);
					html += '<div class="sub-menu-wrapper"><ul class="sub-menu">';
					for (j = 0; j < cat.children.length; j++) {
						ccat = cat.children[j];
						html += '<li class="sub-item ctmenu-item"><div class="menu-overlay-section">';
						html += '<h2>';
						html += '<a class="ctmenu-item-link" cateid="' + ccat.id + '" href="' + ccat.href + '">' + ccat.text + '</a>';
						html += '</h2>';
						if (typeof(ccat.children) != "undefined") {
							ccat.children.sort(this.sort);
							html += '<ul class="sub-menu-detail">';
							var classMissingContent = '';
							for (k = 0; k < ccat.children.length; k++) {
								gccat = ccat.children[k];
								html += '<li class="ctmenu-item">';
								html += '<a class="ctmenu-item-link" cateid="' + gccat.id + '" href="' + gccat.href + '">' + gccat.text + '</a>';
								html += '</li>';
							}
							html += '</ul>';
						}
						html += '</div></li>';
					}
					html += '<div class="clear"></div></ul></div></div>';
				}
				html += '</li>';
			}
			html += '</ul>';
			//el.append(html);
			el.html(html);
         }
     }
     c.RenderToutArea = function(elementId) {
         var el = $(elementId);
         if (el.length == 0) return;
         var html = "", i = 0, subNavHtml = "", j = 0, k = 0, s = "";
         var cat, ccat, gccat;
         // only render the product level categories at level 1
         cat = this.Find(3);
         if (cat == null) return;
         if (typeof (cat.children) != "undefined") {
             for (i = 0; i < cat.children.length; i++) {
                 ccat = cat.children[i];
                 if (ccat.level != 1) continue;
                 html += '<div class="csp-box">';
				 if (ccat.displayOrder.indexOf('Missing') == -1) {
					 html += '<a href="' + ccat.href + '" cspobj="REPORT" csptype="LINKS" cspenglishvalue="' + ccat.rid + '">';
					 html += '<h2>' + ccat.text + '</h2>';
					 html += '<img src="global/files/avaya/images/homepage' + ccat.id + '.jpg" />';					 
					 html += '<p>' + ccat.desc + '</p>';
					 html += '</a>';
				 } else {
					 html += '<h2 style=\"color: #5F798F;text-decoration: none;\">' + ccat.text + '</h2>';
					 html += '<img src="global/files/avaya/images/homepage' + ccat.id + '.jpg" />';		
					 html += '<p>' + ccat.desc + '</p>';
				 }
				 html += '</div>';
             }
         }
         el.html(html);//.append(html);
     }
     c.RenderTitle = function(elId, catId, level, pn) {
         var el = $(elId), html = "";
         if (pn) {
             var item = this.FindByAttr("pn", pn);
             if (item.length > 0) {
                 el.append('<span class="csp-title">' + item[0].text + '</span>');
                 el.append('<span cspObj="REPORT" cspType="TITLE" style="display:none">' + item[0].text + '</span>'); // page title
             }
             return;
         }
         var cats = this.FindAll(catId, level);
         if (el.length == 0 || cats.length == 0) return;
         for (var i = 0; i < cats.length; i++) {
			/*
             if (cats[i].parent != null)
                 html = '<span class="csp-title">' + cats[i].parent.text + ': <br /> ' + cats[i].text + '</span>';
             else
                 html = '<span class="csp-title">' + cats[i].text + '</span>';
			*/
			html = '<span class="csp-title">' + cats[i].text + '</span>';
             html += '<span cspObj="REPORT" cspType="TITLE" style="display:none">' + cats[i].text + '</span>'; // page title
			 //$('head').find('title').text(cats[i].text);
			 document.title = cats[i].text;
             break;
         }
         if (html != "")
             el.html(html);//.append(html);
     }
     c.RenderBreadcrumb = function(elId, catId, level) {
         var el = $(elId), cats = this.FindAll(catId), html = "", p = null;
         if (el.length == 0 || cats.length == 0) return;
         for (var i = 0; i < cats.length; i++) {
             if (cats[i].level == level) {
                 if (level == 3) {
                     // look for the level 2 in the same category
                     var lvl2Cat = this.FindAll(catId, 2);
                     if (lvl2Cat.length > 0)
                         html = '<a catid="'+cats[i].id+'" href="' + (typeof (lvl2Cat[0].href) != "undefined" ? lvl2Cat[0].href : "javascript:void(0);") + '">' + lvl2Cat[0].text + '</a>  &raquo;  ' + html;
                 }
                 else if (level == 0)
                     html = '<a catid="'+cats[i].id+'" href="' + (typeof (cats[i].href) != "undefined" ? cats[i].href : "javascript:void(0);") + '">' + cats[i].text + '</a>  &raquo;  ' + html;

                 p = cats[i].parent;
                 while (p != null) {
                     html = '<a catid="'+p.id+'" href="' + (typeof (p.href) != "undefined" ? p.href : "javascript:void(0);") + '">' + p.text + '</a>  &raquo;  ' + html;
                     p = p.parent;
                 }
                 break;
             }
         }
         if (html != "")
             el.html(html);//.append(html);
     }
 })(categories);
