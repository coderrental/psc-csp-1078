var contentOriginalHeight;
var hasBannerText = false;

function initApp() {
	AddExceptionMenuItems();
	categories.Render("#csp-nav");
	
	if (window.location.href.indexOf('home=true') < 0)
		RenderCustomBreadcrumb ("#csp-breadcrumb", crrCatId, (categoryLevel));
	ResetContentImages();
	if (categoryLevel > 1) {
		RenderSideItems();
	}
	else {
		RenderChildrenBlocks ();
		ExpandContentPane();
	}
	
	ContentPageLinksDetect();
	window.setTimeout("initMenu()", 1500);
	if (window.location.href.indexOf('home=true') >= 0 || window.location.href.indexOf('cat=') < 0) {
		$('.root-item').removeClass('selected');
		$('.root-item').first().addClass('selected');
	}
	initLightBox();
	
	$(window).resize(function(){ ResizeColumn();});
}

function ResizeColumn() {
	if ($(window).width() > 480)
		$('#side-col-list').show();
	else
		$('#side-col-list').hide();
}

function ResetContentImages() {
	var sectionWidth = $('#csp-right-col').width();
	$('#csp-right-col').find('img').each(function() {
		if ($(this).parents('#page-banner').length > 0)
			return;
		var newImg = new Image();
		var height = 0;
		var width = 0;
		var crrImgEl = $(this);
		
		newImg.onload = function() {
			height = newImg.height;
			width = newImg.width;
			if (width >= sectionWidth) {
				crrImgEl.attr('width', (sectionWidth - 30) + 'px');
			}
		}

		newImg.src = $(this).attr('src'); // this must be done AFTER setting onload
	});
}

function RenderSideItems() {
	var secondLevelItems = categories.FindByAttr('pid', crrCatId);
	if (secondLevelItems.length > 0) {
		var newSideUL = $('<ul>');
		newSideUL.attr({
			id: 'side-col-list',
			'class': 'csp-sibling-items'
		});
		$('#csp-side-col').append(newSideUL);
		RenderDetailSideList(newSideUL, 'aside-menu-item', secondLevelItems);
		$('#page-banner').find('img').attr('width', $('#csp-right-col').width() + 'px');
		var featureSection = $('#csp-right-col').find('.feature:first');
		featureSection.find('.figure').attr('style', 'width: 245px');
		featureSection.find('.figure img').attr('width', '245px');
		SetContentTableWidth (200);
		
		$('#mobile-left-col-trigger').unbind().click(function() {
			$('#side-col-list').toggle();
		});
	}
	else {
		ExpandContentPane();
		$('#mobile-left-col-trigger').hide();
	}
}

function RenderChildrenBlocks () {
	var childrenList = categories.FindByAttr('pid', crrCatId);
	if (childrenList.length <= 0)
		return;
	$('#sub-category-blocks').remove();	
	var contentWidth = $('#csp-right-col').width();
	var blockWidth = 100 / childrenList.length;
	var blocksWrapper = $('<div id="sub-category-blocks">');
	
	for (var i = 0; i < childrenList.length; i++) {
		var blockDetail = '<div class="sub-category-block" id="block-' + childrenList[i].id + '">';
		blockDetail += '<a class="sub-category-block-figure" href="' + childrenList[i].href + '"><img src="' + childrenList[i].thumbnail + '" height="200px"></a>';
		blockDetail += '<h2><a href="' + childrenList[i].href + '">' + childrenList[i].text + '</a></h2>';
		blockDetail += '<p>' + childrenList[i].desc + '</p></div>';
		blocksWrapper.append(blockDetail);
	}
	blocksWrapper.append('<div class="clear"></div>');
	$('#csp-right-col').append(blocksWrapper);
	$('.sub-category-block').css('width', blockWidth + '%');
}

function ExpandContentPane() {
	$('#csp-side-col').remove();
	$('#csp-right-col').css({
		padding: '0px',
		width: '100%'
	});
	
	$('#page-banner').css({
		width: '658px'
	});
	if ($('#page-banner').find('img').length > 0) {
		$('#page-banner').css({
			width: '660px'
		});
		$('#page-banner').find('img').attr('width', '660px');
	}
}

function SetContentTableWidth (width) {
	$('.layoutColumn').find('table').each(function() {
		var styleValue = $(this).attr('style');
		if (styleValue.indexOf('float') < 0)
			return;
		$(this).width(width);
		$(this).find('img').width(width);
		$(this).find('td').attr('style', '');
	});
}

function RenderDetailSideList(ulParent, liClassName, categoryList) {
	for (var i = 0; i < categoryList.length; i++) {
		var newSiblingLI = $('<li>');
		newSiblingLI.attr({
			id: 'c-' + categoryList[i].id
		});
		if (categoryList[i].id == crrCatId)
			newSiblingLI.addClass('selected');
		var newSiblingA = $('<a>');
		newSiblingA.html(categoryList[i].text);
		newSiblingA.attr({
			'class': liClassName,
			cateid: categoryList[i].id,
			rel: 'CategoryTitleEditor-' + categoryList[i].id,
			href: categoryList[i].href,
			ccid: categoryList[i].cnid
		});
		var newSiblingInput = $('<input type="text">');
		newSiblingInput.val(categoryList[i].text);
		newSiblingInput.attr({
			'class': 'hide',
			id: 'CategoryTitleEditor-' + categoryList[i].id
		});
		newSiblingLI.append(newSiblingA);
		newSiblingLI.append(newSiblingInput);
		ulParent.append(newSiblingLI);
	}
}

function vAlignNavText() {
    $('.root-item-link').each(function () {
        var thisHeight = $(this).height();
        var wrapperHeight = $(this).closest('li').height();
        var topPos = (wrapperHeight - thisHeight) / 2;
    	var newHeight = wrapperHeight - topPos;
        $(this).css({ 'padding-top': (topPos) + 'px', 'height': (newHeight) + 'px' });
    });
}

function hAlignNavItem() {
    //var navWidth = 510;
	var navWidth = $('#csp-nav').width();
    var menuItemCount = $('li.root-item').length;
	$('#main-nav').addClass('main-nav-' + menuItemCount);
    //var itemWidth = (navWidth / menuItemCount) - 1;
    //var crrStyle = $('li.root-item').attr('style');
    //$('li.root-item').css('width', itemWidth + 'px');
   // $('a.root-item-link').attr('style', 'width: ' + parseInt(itemWidth - 10) + 'px !important');
}

function initMenu() {
    menuEffect();
	
    hAlignNavItem();
    
    var subMenuItemCount;
    var widthPercent;
    $('ul.sub-menu').each(function () {
        subMenuItemCount = $(this).find('li.sub-item').length;
        widthPercent = (100 / subMenuItemCount) - 3;
        $(this).find('li.sub-item').attr('style', 'width: ' + widthPercent + '%; padding-right: 3%');
    });

    vAlignNavText();
}

function menuEffect() {
	if (window.location.href.indexOf('&home=true') < 0 && window.location.href.indexOf('&cat=') > 0)
		$('a[cateid="' + crrCatId + '"]').parents('li.root-item').addClass('selected');
	$('li.root-item').unbind('hover').hover(function () {
		$(this).addClass('root-item-hover');
		$(this).find('div.sub-menu-wrapper').show();
		if ($('#csp-content').height() < $(this).find('div.sub-menu-wrapper').height())
			$('#csp-content').height($(this).find('div.sub-menu-wrapper').height());
	}, function () {
		$(this).removeClass('root-item-hover');
		$(this).find('div.sub-menu-wrapper').hide();
	});
}

function AddExceptionMenuItems() {
	var firstItems = categories.FindByAttr('level', 1);
	if (firstItems.length > 2) {
		for (var i = 0; i < firstItems.length - 1; i++) {
			for (var j = i + 1; j < firstItems.length; j++) {
				if (parseInt(firstItems[j].displayOrder) < parseInt(firstItems[i].displayOrder)) {
					var tempItem = firstItems[j];
					firstItems[j] = firstItems[i];
					firstItems[i] = tempItem;
				}
			}
		}
		var homeUrl = firstItems[1].href + '&home=true';
		var contactUrl = 'd1.aspx?p15():c14(ct31000&cd133i99)[st(ct31000*Status_Sort_Order*)]:c16(ct15000)[st(ct15000*Status_Sort_Order*)][]&ticks=' + Math.round((new Date()).getTime() / 1000);
		firstItems[0].href = homeUrl;
		firstItems[firstItems.length - 1].href = contactUrl;
	}
	// var newUl;
	// if ($('#csp-nav').find('#main-nav').length == 0) {
		// newUl = $('<ul id="main-nav" class="the-main-menu">');
		// $('#csp-nav').append(newUl);
	// }
	// else
		// newUl = $('#csp-nav').find('#main-nav');
	// var firstCategories = categories.FindByAttr('level', 1);
	// var homeUrl = firstCategories[0].href + '&home=true';
	// newUl.prepend($('<li class="root-item" id="home-item">').html('<a class="root-item-link exception-item" href="' + homeUrl + '">Home</a>'));
	// var contactUrl = 'javascript: void(0)';
	// var arr = firstCategories[0].href.split("/");
	// contactUrl = 'd1.aspx?p15():c14(ct31000&cd133i99)[st(ct31000*Status_Sort_Order*)]:c16(ct15000)[st(ct15000*Status_Sort_Order*)][]&ticks=' + Math.round((new Date()).getTime() / 1000);
	// newUl.append($('<li class="root-item exception-item">').html('<a class="root-item-link exception-item" href="' + contactUrl + '">Contact</a>'));
}

function RenderCustomBreadcrumb(elId, catId, level) {
	var el = $(elId);
	var categoryEl = categories.Find(catId);
	var theLineage = GetCategoryLineage(catId);
	var html = '';
	var theUrl;
	for (var i = 0; i < theLineage.length; i++) {
		if (theLineage[i] == '') continue;
		if (theLineage[i] == rootCatId) {
			categoryEl = categories.Find(theLineage[i + 1]);
			theUrl = (typeof(categoryEl.href) != "undefined" ? categoryEl.href + '&home=true' : "javascript:void(0);");
			html += '<a href="' + theUrl + '">Home</a>  &raquo;  ';
			continue;
		}
		else if (theLineage[i] == catId) {
			categoryEl = categories.Find(catId);
			if (categoryEl != null)
				html += categoryEl.text;
			continue;
		}
		categoryEl = categories.Find(parseInt(theLineage[i]));
		if (categoryEl == null) continue;
		theUrl = (typeof(categoryEl.href) != "undefined" ? categoryEl.href : "javascript:void(0);");
		html += '<a ccid="' + categoryEl.cnid + '" catid="' + categoryEl.id + '" href="' + theUrl + '">' + categoryEl.text + '</a>  &raquo;  ';
	}
	if (html != "")
		el.html(html);
};

function ContentPageLinksDetect() {
	$('#csp-right-col').find('a.content-page-links').each(function() {
		var idMixed = $(this).attr('id').split('-');
		var linkCategoryId = idMixed.pop();
		var crrCategoryNode = categories.Find(linkCategoryId);
		if (crrCategoryNode == null) {
			$(this).remove();
			return;
		}
		$(this).attr('href', crrCategoryNode.href);
	});
}

function GetCategoryLineage(catId) {
	var categoryItem = categories.Find(catId);
	var lineageArr = [catId];
	if (categoryItem == null)
		return lineageArr;
	p = categories.Find(categoryItem.pid);
	while (p != null) {
		lineageArr.unshift(p.id);
		p = categories.Find(p.pid);
	}
	lineageArr.unshift(rootCatId);
	return lineageArr;
}

function initLightBox () {
	// Open Lightbox
	$('a.play-video').click(function() {
		//$('#screen').css({'display':'block', opacity: 0.75, 'width':$(document).width(), 'height':$(document).height()});
		$('#video').css({'display':'block'});
		$('#video').find('iframe').width(500);
		return false;
	});
	// Close Lightbox
	$('.lightbox .close').click(function() {
		$('#screen, .lightbox').hide();
		$('iframe').attr('src', $('iframe').attr('src'));
	}); 
}