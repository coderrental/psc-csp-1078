﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using CR.DnnModules.ShowcaseContentEntry.Common;
using DotNetNuke.Common.Utilities;
using Telerik.Web.UI;

namespace CR.DnnModules.ShowcaseContentEntry
{
	public partial class PageManagement : System.Web.UI.Page
	{
		private int _portalId, _themeId, _rootCategoryId, _ctContentCategoryId, _langId, _tabModuleId, _ctContentPageId;
		private bool _showAll = false;
		private CspDataContext _cspDataContext;
		private DnnDataContext _dnnDataContext;
		private CspUtils _cspUtils;
		private List<Category> _categories;
		private const int STAGE_PUBLISHED = 50;
		private const int STAGE_UNPUBLISHED = 56;
		private const int STAGE_STAGING = Cons.STAGE_STAGING;
		private const string CTF_CATEGORYCONTENT_STAGING = "Content_Staging";
		private const string CTF_CONTENTPAGE_STAGING = "Content_Staging";
		private string _externalJSUrl = "";
		private bool _pof = false;

		/// <summary>
		/// Handles the Init event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		/// <author>ducuytran</author>
		/// <datetime>5/9/2013</datetime>
		protected void Page_Init(object sender, EventArgs e)
		{
			InitPageResource();
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		/// <author>ducuytran</author>
		/// <datetime>5/9/2013</datetime>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!String.IsNullOrEmpty(_externalJSUrl))
			{
				if (_externalJSUrl.IndexOf(',') == -1)
					Page.ClientScript.RegisterClientScriptInclude("function", _externalJSUrl);
				else
				{
					foreach (string jsUrl in _externalJSUrl.Split(','))
					{
						string jsFileName = jsUrl.Split('/').Last();
						Page.ClientScript.RegisterClientScriptInclude(jsFileName, jsUrl);
					}
				}
			}
		}

		/// <summary>
		/// Inits the page resource.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>5/9/2013</datetime>
		private void InitPageResource()
		{
			GetDataFromQueryString();
			_categories = new List<Category>();
			_cspDataContext = new CspDataContext(Utils.GetConnectionString(_portalId));
			_cspUtils = new CspUtils(_cspDataContext);
			_dnnDataContext = new DnnDataContext(Config.GetConnectionString());
			GetCategoryList();
			if (_categories.Any())
			{
				PageTree.DataSource = _categories;
				PageTree.DataBind();
			}
		}

		/// <summary>
		/// Gets the data from query string.
		/// </summary>
		/// Author: Vu Dinh
		/// 3/24/2013 - 5:34 PM
		private void GetDataFromQueryString()
		{
			string hash = HttpUtility.HtmlDecode(Request.QueryString["hash"]);
			var parameters = CryptoServiceProvider.Decode(hash).Split(new[] { "&" }, StringSplitOptions.None);
			foreach (var parameter in parameters)
			{
				var key = parameter.Split('=')[0];
				var value = parameter.Split('=')[1];
				switch (key)
				{
					case "portalid":
						_portalId = int.Parse(value);
						break;
					case "rootcatid":
						_rootCategoryId = int.Parse(value);
						break;
					case "themeid":
						_themeId = int.Parse(value);
						break;
					case "ctcategorycontent":
						_ctContentCategoryId = int.Parse(value);
						break;
					case "ctcontentpage":
						_ctContentPageId = int.Parse(value);
						break;
					case "lang":
						_langId = int.Parse(value);
						break;
					case "jsfiles":
						_externalJSUrl = value;
						break;
					case "showall":
						_showAll = (value.ToLower() == "false") ? false : true;
						break;
					case "pof":
						_pof = (value.ToLower() == "false") ? false : true;
						break;
					case "tabmoduleid":
						_tabModuleId = int.Parse(value);
						break;
				}
			}
		}

		/// <summary>
		/// Gets the category list.
		/// </summary>
		/// <param name="showAll">if set to <c>true</c> [show all].</param>
		/// <author>ducuytran</author>
		/// <datetime>5/9/2013</datetime>
		/// <update>
		/// _ Get list by staging if Publish on the fly is off - @ducuytran (26/06/2013)
		/// </update>
		public void GetCategoryList()
		{
			List<category> uncheckedCategoryList = new List<category>();
			List<category> rawCategoryList = new List<category>();

			category rootNode =
				_cspDataContext.categories.FirstOrDefault(a => a.categoryId == _rootCategoryId);

			_categories.Add(new Category
			{
				Id = _rootCategoryId,
				ParentId = 0,
				DisplayOrder = 1,
				Text = rootNode.categoryText
			});
			if (_showAll)
			{
				uncheckedCategoryList.AddRange(
					_cspDataContext.categories.Where(a => a.themes_categories.Any(b => b.themes_Id == _themeId) && a.parentId > 0).
						OrderBy(a => a.DisplayOrder).
						ThenByDescending(a => a.categoryId).ToList());
			}
			else
			{
				uncheckedCategoryList.AddRange(
					_cspDataContext.categories.Where(a => a.themes_categories.Any(b => b.themes_Id == _themeId) && a.parentId > 0 && a.active == true).
						OrderBy(a => a.DisplayOrder).
						ThenByDescending(a => a.categoryId).ToList());
			}

			rawCategoryList = uncheckedCategoryList.Distinct().ToList();

			if (rawCategoryList.Count > 0)
			{
				int DisplayOrder = 1;
				foreach (category rawCategoryItem in rawCategoryList)
				{
					category categoryItem = rawCategoryItem;
					int parentId = (int)rawCategoryItem.parentId;
					if (categoryItem.DisplayOrder == null)
						DisplayOrder = GetLastOrder((int)categoryItem.parentId);
					else
					{
						DisplayOrder = (int)categoryItem.DisplayOrder;
					}

					//Begin to get category contents
					content sourceContentCategory = null;
					content categoryContent = null;
					bool getOriginal = true;
					bool isCreatedInStaging = false;
					bool isPublished = false;
					bool isActive = (bool)rawCategoryItem.active;

					//Publish on the fly is currently off
					if (!_pof)
					{
						List<CSPShowcase_Staging> stagingList =
							_dnnDataContext.CSPShowcase_Stagings.Where(a => a.CategoryId == categoryItem.categoryId && a.LanguageId == _langId &&
																		(a.IsBackupVersion == false || a.IsBackupVersion == null) &&
																		(a.IsOriginalVersion == false || a.IsOriginalVersion == null) &&
																		a.PortalId == _portalId && a.TabModuleId == _tabModuleId).OrderByDescending(a => a.DateTime).ToList();
						if (stagingList.Any())
						{
							getOriginal = false;

							CSPShowcase_Staging latestStagingItem = stagingList.FirstOrDefault();
							List<String> DisplayOrderList = latestStagingItem.DisplayOrder.Split('-').ToList();

							sourceContentCategory = _cspDataContext.contents.FirstOrDefault(a => a.content_Id == latestStagingItem.SourceContentCategoryId);
							parentId = latestStagingItem.ParentCategoryId;
							DisplayOrder = DisplayOrderList.IndexOf(categoryItem.categoryId.ToString()) + 1;

							if (latestStagingItem.ContentCategoryId != Guid.Empty)
							{
								categoryContent = _cspDataContext.contents.FirstOrDefault(a => a.content_Id == latestStagingItem.ContentCategoryId);
							}
							else
							{
								categoryContent = sourceContentCategory;
								if (!String.IsNullOrEmpty(_cspUtils.GetContentFieldValue(sourceContentCategory, CTF_CATEGORYCONTENT_STAGING)))
								{
									isCreatedInStaging = true;
									isPublished = true;
								}
							}

							switch (latestStagingItem.Status)
							{
								case (int)LogAction.GoActive:
									isActive = true;
									isPublished = true;
									break;
								case (int)LogAction.GoInactive:
									isActive = false;
									break;
								case (int)LogAction.GoPublish:
									isPublished = true;
									break;
								case (int)LogAction.GoUnpublish:
									isPublished = false;
									break;
								default:
									/*content originalCategoryContent = _cspDataContext.contents.FirstOrDefault(a =>
																												a.content_main.content_categories.Any(b => b.category_Id == categoryItem.categoryId) &&
																												a.content_main.content_types_Id == _ctContentCategoryId &&
																												a.content_types_languages_Id == _langId && a.stage_Id != STAGE_STAGING);*/
									isPublished = sourceContentCategory.stage_Id == STAGE_PUBLISHED;
									break;
							}
						}
					}

					//Publish on the fly is currently on OR the category has no staging item yet
					//Get original version
					if (getOriginal)
					{
						if (!_pof)
						{
							List<String> DisplayOrderList = GetDisplayOrderList((int)categoryItem.parentId, 0).Split('-').ToList();
							DisplayOrder = DisplayOrderList.IndexOf(categoryItem.categoryId.ToString()) + 1;
						}
						categoryContent = _cspDataContext.contents.FirstOrDefault(a =>
								a.content_main.content_categories.Any(b => b.category_Id == categoryItem.categoryId) &&
								a.content_main.content_types_Id == _ctContentCategoryId &&
								a.content_types_languages_Id == _langId && a.stage_Id != STAGE_STAGING);
						sourceContentCategory = categoryContent;
						isPublished = categoryContent != null ? categoryContent.stage_Id == STAGE_PUBLISHED : false;
					}

					if (!_showAll && !isCreatedInStaging)
					{
						if (categoryContent == null || sourceContentCategory.stage_Id != STAGE_PUBLISHED || !isPublished || !isActive)
							continue;
					}
					string categoryTitle = categoryItem.categoryText;
					if (categoryContent != null)
					{
						categoryTitle = _cspUtils.GetContentFieldValue(categoryContent, "Content_Title");
					}
					_categories.Add(new Category
					{
						Id = categoryItem.categoryId,
						ParentId = parentId,
						Text = categoryTitle,
						DisplayOrder = DisplayOrder
					});

					_categories = _categories.OrderBy(a => a.DisplayOrder).ToList();
				}
			}
		}

		/// <summary>
		/// Gets the content by staging.
		/// </summary>
		/// <param name="cateItem">The cate item.</param>
		/// <param name="ctId">The ct id.</param>
		/// <param name="justGetSourceContent"> </param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>7/4/2013</datetime>
		private content GetContentByStaging(category cateItem, int ctId, bool justGetSourceContent = false)
		{
			content theContent = null;
			if (_pof || justGetSourceContent)
			{
				theContent = _cspDataContext.contents.FirstOrDefault(a =>
								a.content_main.content_categories.Any(b => b.category_Id == cateItem.categoryId) &&
								a.content_main.content_types_Id == ctId &&
								a.content_types_languages_Id == _langId && a.stage_Id != STAGE_STAGING);
				if (theContent == null && justGetSourceContent)
				{
					CSPShowcase_Staging latestStagingItem = GetLatestStagingContent(cateItem.categoryId);
					if (latestStagingItem != null)
					{
						if (ctId == _ctContentCategoryId)
							theContent = _cspDataContext.contents.FirstOrDefault(a => a.content_Id == latestStagingItem.SourceContentCategoryId);
						else
							theContent = _cspDataContext.contents.FirstOrDefault(a => a.content_Id == latestStagingItem.SourceContentPageId);
					}
				}
			}
			else if (!_pof)
			{
				CSPShowcase_Staging latestStagingItem = GetLatestStagingContent(cateItem.categoryId);
				if (latestStagingItem != null)
				{
					if (ctId == _ctContentCategoryId)
						theContent = _cspDataContext.contents.FirstOrDefault(a => a.content_Id == (latestStagingItem.ContentCategoryId != Guid.Empty ? latestStagingItem.ContentCategoryId : latestStagingItem.SourceContentCategoryId));
					else
						theContent = _cspDataContext.contents.FirstOrDefault(a => a.content_Id == (latestStagingItem.ContentPageId != Guid.Empty ? latestStagingItem.ContentPageId : latestStagingItem.SourceContentPageId));
				}
				else
				{
					theContent = _cspDataContext.contents.FirstOrDefault(a =>
								a.content_main.content_categories.Any(b => b.category_Id == cateItem.categoryId) &&
								a.content_main.content_types_Id == ctId &&
								a.content_types_languages_Id == _langId && a.stage_Id != STAGE_STAGING);
				}
			}

			return theContent;
		}

		/// <summary>
		/// Gets the content of the latest staging.
		/// </summary>
		/// <param name="categoryId">The category id.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>6/22/2013</datetime>
		private CSPShowcase_Staging GetLatestStagingContent(int categoryId)
		{
			List<CSPShowcase_Staging> stagingList =
				_dnnDataContext.CSPShowcase_Stagings.Where(a => a.CategoryId == categoryId && a.LanguageId == _langId &&
													(a.IsBackupVersion == false || a.IsBackupVersion == null) &&
													(a.IsOriginalVersion == false || a.IsOriginalVersion == null) &&
													a.PortalId == _portalId && a.TabModuleId == _tabModuleId).OrderByDescending(a => a.DateTime).ToList();
			if (!stagingList.Any())
				return null;
			return stagingList.First();
		}

		/// <summary>
		/// Handles the HandleDrop event of the PageTree control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Telerik.Web.UI.RadTreeNodeDragDropEventArgs"/> instance containing the event data.</param>
		/// <author>ducuytran</author>
		/// <datetime>5/9/2013</datetime>
		protected void PageTree_HandleDrop(object sender, RadTreeNodeDragDropEventArgs e)
		{
			RadTreeNode sourceNode = e.SourceDragNode;
			RadTreeNode destNode = e.DestDragNode;
			RadTreeViewDropPosition dropPosition = e.DropPosition;

			if (destNode != null) //drag&drop is performed between trees
			{
				if (sourceNode.TreeView.SelectedNodes.Count <= 1)
				{
					PerformDragAndDrop(dropPosition, sourceNode, destNode);
					UpdateCategoryNode(dropPosition, sourceNode, destNode);
				}
				else if (sourceNode.TreeView.SelectedNodes.Count > 1)
				{
					if (dropPosition == RadTreeViewDropPosition.Below) //Needed to preserve the order of the dragged items
					{
						for (int i = sourceNode.TreeView.SelectedNodes.Count - 1; i >= 0; i--)
						{
							PerformDragAndDrop(dropPosition, sourceNode.TreeView.SelectedNodes[i], destNode);
						}
					}
					else
					{
						foreach (RadTreeNode node in sourceNode.TreeView.SelectedNodes)
						{
							PerformDragAndDrop(dropPosition, node, destNode);
						}
					}
				}

				destNode.Expanded = true;
				sourceNode.TreeView.UnselectAllNodes();
			}
		}

		/// <summary>
		/// Updates the category node.
		/// </summary>
		/// <param name="dropPosition">The drop position.</param>
		/// <param name="sourceNode">The source node.</param>
		/// <param name="destNode">The dest node.</param>
		/// <author>ducuytran</author>
		/// <datetime>5/9/2013</datetime>
		private void UpdateCategoryNode(RadTreeViewDropPosition dropPosition, RadTreeNode sourceNode, RadTreeNode destNode)
		{
			category srcCategory = _cspDataContext.categories.FirstOrDefault(a => a.categoryId == int.Parse(sourceNode.Value));
			category destCategory = _cspDataContext.categories.FirstOrDefault(a => a.categoryId == int.Parse(destNode.Value));
			if (srcCategory == null || destCategory == null)
				return;
			int displayOrder = 1;
			switch (dropPosition)
			{
				case RadTreeViewDropPosition.Over:
					if (_pof)
					{
						displayOrder = GetLastOrder(destCategory.categoryId) + 1;
						srcCategory.parentId = destCategory.categoryId;
						UpdateCategoryOrder(srcCategory, displayOrder);
						_cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
						return;
					}
					UpdateCategoryStaging(srcCategory, destCategory, string.Empty);
					break;

				default:
					RadTreeNode parentNode = destNode.Parent as RadTreeNode;
					List<RadTreeNode> childrenNodes = null;
					if (parentNode == null) return;
					int crrParentId = (int)srcCategory.parentId;
					if (_pof)
					{
						if (crrParentId != int.Parse(parentNode.Value))
						{
							srcCategory.parentId = int.Parse(parentNode.Value);
							displayOrder = ((int) destCategory.DisplayOrder) + 1;
							if (dropPosition == RadTreeViewDropPosition.Above)
								displayOrder = (int) destCategory.DisplayOrder;
							UpdateCategoryOrder(srcCategory, displayOrder);
						}
						childrenNodes =
							parentNode.GetAllNodes().ToList().Where(n => n.Parent == destNode.Parent).ToList();
						foreach (RadTreeNode childNode in childrenNodes)
						{
							if (crrParentId != int.Parse(parentNode.Value) && int.Parse(childNode.Value) == srcCategory.categoryId)
								continue;

							category childCategory =
								_cspDataContext.categories.FirstOrDefault(a => a.categoryId == int.Parse(childNode.Value));

							if (childCategory == null) continue;

							UpdateCategoryOrder(childCategory, childrenNodes.IndexOf(childNode) + 1);
						}
						return;
					}
					category parentCategory =
						_cspDataContext.categories.FirstOrDefault(a => a.categoryId == int.Parse(parentNode.Value));
					if (parentCategory == null)
						return;
					string displayOrderStr = "";
					childrenNodes = parentNode.GetAllNodes().ToList().Where(n => n.Parent == destNode.Parent).ToList();
					foreach (RadTreeNode childNode in childrenNodes)
					{
						if (!String.IsNullOrEmpty(displayOrderStr))
							displayOrderStr += "-";
						displayOrderStr += childNode.Value;
					}
					UpdateCategoryStaging(srcCategory, parentCategory, displayOrderStr);
					//_cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
					break;
			}
		}

		/// <summary>
		/// Updates the category staging.
		/// </summary>
		/// <param name="srcCategory">The SRC category.</param>
		/// <param name="destCategory">The dest category.</param>
		/// <param name="displayOrder"> </param>
		/// <author>ducuytran</author>
		/// <datetime>6/26/2013</datetime>
		private void UpdateCategoryStaging(category srcCategory, category destCategory, string displayOrder)
		{
			CSPShowcase_Staging newStaging = null;
			CSPShowcase_Staging lastStaging = null;
			String DisplayOrder = String.IsNullOrEmpty(displayOrder)
			                      	? GetDisplayOrderList(destCategory.categoryId, 0) + "-" + srcCategory.categoryId
									: displayOrder;
			List<CSPShowcase_Staging> categoryStages = _dnnDataContext.CSPShowcase_Stagings.Where(a => a.CategoryId == srcCategory.categoryId && a.LanguageId == _langId &&
																									(a.IsBackupVersion == false || a.IsBackupVersion == null) &&
																									(a.IsOriginalVersion == false || a.IsOriginalVersion == null) &&
																									a.PortalId == _portalId && a.TabModuleId == _tabModuleId).OrderByDescending(
					a => a.DateTime).ToList();
			int lastParentId = (int) srcCategory.parentId;
			if (categoryStages.Any())
			{
				lastStaging = categoryStages.FirstOrDefault();
				lastParentId = lastStaging.ParentCategoryId;
				newStaging = new CSPShowcase_Staging
				{
					Id = Guid.NewGuid(),
					ParentCategoryId = destCategory.categoryId,
					CategoryId = srcCategory.categoryId,
					DisplayOrder = DisplayOrder,
					SourceContentCategoryId = lastStaging.SourceContentCategoryId,
					ContentCategoryId = lastStaging.ContentCategoryId,
					ContentCategoryMainId = lastStaging.ContentCategoryMainId,
					SourceContentPageId = lastStaging.SourceContentPageId,
					ContentPageId = lastStaging.ContentPageId,
					ContentPageMainId = lastStaging.ContentPageMainId,
					LanguageId = _langId,
					Published = false,
					DateTime = DateTime.Now,
					PortalId = _portalId,
					TabModuleId = _tabModuleId,
					Status = lastStaging.Status
				};
			}
			else
			{
				newStaging = InitFirstStaging(srcCategory.categoryId, DisplayOrder);
				newStaging.ParentCategoryId = destCategory.categoryId;
			}

			if (lastParentId != destCategory.categoryId)
			{
				newStaging.ActionId = (int) LogAction.MoveToNewParent;
			}
			else
			{
				newStaging.ActionId = (int)LogAction.Move;
			}

			_dnnDataContext.CSPShowcase_Stagings.InsertOnSubmit(newStaging);
			_dnnDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
		}

		/// <summary>
		/// Updates the category order.
		/// </summary>
		/// <param name="srcCategory">The SRC category.</param>
		/// <param name="displayOrder">The display order.</param>
		/// <author>ducuytran</author>
		/// <datetime>5/9/2013</datetime>
		private void UpdateCategoryOrder(category srcCategory, int displayOrder)
		{
			srcCategory.DisplayOrder = displayOrder;
			List<content> categoryContents = _cspDataContext.contents.Where(
				a =>
				a.content_main.content_categories.Any(b => b.category_Id == srcCategory.categoryId) &&
				a.content_main.content_types_Id == _ctContentCategoryId).ToList();
			foreach (content categoryContent in categoryContents)
			{
				_cspUtils.SetContentFieldValue(_ctContentCategoryId, categoryContent, "Status_Sort_Order", displayOrder.ToString("0000"));
			}
		}

		/// <summary>
		/// Performs the drag and drop.
		/// </summary>
		/// <param name="dropPosition">The drop position.</param>
		/// <param name="sourceNode">The source node.</param>
		/// <param name="destNode">The dest node.</param>
		/// <author>ducuytran</author>
		/// <datetime>5/9/2013</datetime>
		private static void PerformDragAndDrop(RadTreeViewDropPosition dropPosition, RadTreeNode sourceNode,
												 RadTreeNode destNode)
		{
			if (sourceNode.Equals(destNode) || sourceNode.IsAncestorOf(destNode))
			{
				return;
			}
			
			sourceNode.Owner.Nodes.Remove(sourceNode);

			switch (dropPosition)
			{
				case RadTreeViewDropPosition.Over:
					// child
					if (!sourceNode.IsAncestorOf(destNode))
					{
						destNode.Nodes.Add(sourceNode);
					}
					break;

				case RadTreeViewDropPosition.Above:
					// sibling - above                         
					destNode.InsertBefore(sourceNode);
					break;

				case RadTreeViewDropPosition.Below:
					// sibling - below
					destNode.InsertAfter(sourceNode);
					break;
			}
		}

		/// <summary>
		/// Gets the display order list.
		/// </summary>
		/// <param name="parentId">The parent id.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>6/23/2013</datetime>
		private string GetDisplayOrderList(int parentId, int categoryId)
		{
			string DisplayOrderList = "";

			List<CSPShowcase_Staging> stagingInfos = new List<CSPShowcase_Staging>();
			if (parentId > 0)
				stagingInfos = _dnnDataContext.CSPShowcase_Stagings.Where(a => a.ParentCategoryId == parentId && a.LanguageId == _langId &&
																			(a.IsBackupVersion == false || a.IsBackupVersion == null) &&
																			(a.IsOriginalVersion == false || a.IsOriginalVersion == null) &&
																			a.PortalId == _portalId && a.TabModuleId == _tabModuleId).OrderByDescending(a => a.DateTime).ToList();
			else
			{
				stagingInfos =
					_dnnDataContext.CSPShowcase_Stagings.Where(a => a.CategoryId == categoryId && a.LanguageId == _langId &&
																(a.IsBackupVersion == false || a.IsBackupVersion == null) &&
																(a.IsOriginalVersion == false || a.IsOriginalVersion == null) &&
																a.PortalId == _portalId && a.TabModuleId == _tabModuleId).OrderByDescending(a => a.DateTime).ToList();
				if (!stagingInfos.Any())
				{
					parentId = (int)_cspDataContext.categories.FirstOrDefault(a => a.categoryId == categoryId).parentId;
					return GetDisplayOrderList(parentId, 0);
				}
			}
			if (stagingInfos.Any())
				DisplayOrderList = stagingInfos.First().DisplayOrder;
			else
			{
				List<category> categoriesList = _cspDataContext.categories.Where(a => a.parentId == parentId).OrderBy(a => a.DisplayOrder).ToList();
				if (categoriesList.Any())
				{
					foreach (category categoryItem in categoriesList)
					{
						if (!String.IsNullOrEmpty(DisplayOrderList))
							DisplayOrderList += "-";
						DisplayOrderList += categoryItem.categoryId.ToString();
					}
				}
			}
			return DisplayOrderList;
		}

		/// <summary>
		/// Gets the last order.
		/// </summary>
		/// <param name="parentId">The parent id.</param>
		/// <returns></returns>
		/// <author>ducuytran - 3/10/2013</author>
		private int GetLastOrder(int parentId)
		{
			List<category> childrenCats =
				_cspDataContext.categories.Where(a => a.parentId == parentId).OrderByDescending(a => a.DisplayOrder).ToList();
			if (childrenCats.Count == 0)
				return 0;
			category lastChild = childrenCats.First();
			return Convert.ToInt32(lastChild.DisplayOrder);
		}

		/// <summary>
		/// Inits the first staging.
		/// </summary>
		/// <param name="categoryId">The category id.</param>
		/// <param name="displayOrder">The display order.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>6/24/2013</datetime>
		private CSPShowcase_Staging InitFirstStaging(int categoryId, string displayOrder)
		{
			category crrCategory = _cspDataContext.categories.FirstOrDefault(a => a.categoryId == categoryId);
			if (crrCategory == null)
				return null;
			content srcContentCategory = _cspDataContext.contents.FirstOrDefault(a => a.content_main.content_categories.Any(b => b.category_Id == categoryId)
																		&& a.content_main.content_types_Id == _ctContentCategoryId && a.content_types_languages_Id == _langId
																		&& a.stage_Id != STAGE_STAGING);
			content srcContentPage = _cspDataContext.contents.FirstOrDefault(a => a.content_main.content_categories.Any(b => b.category_Id == categoryId)
																		&& a.content_main.content_types_Id == _ctContentPageId && a.content_types_languages_Id == _langId
																		&& a.stage_Id != STAGE_STAGING);
			CSPShowcase_Staging newStagingItem = new CSPShowcase_Staging
			{
				Id = Guid.NewGuid(),
				ParentCategoryId = (int)crrCategory.parentId,
				CategoryId = categoryId,
				DisplayOrder = displayOrder,
				SourceContentCategoryId = (srcContentCategory != null) ? srcContentCategory.content_Id : Guid.Empty,
				ContentCategoryId = Guid.Empty,
				ContentCategoryMainId = (srcContentCategory != null) ? srcContentCategory.content_main.content_main_Id : Guid.Empty,
				SourceContentPageId = (srcContentPage != null) ? srcContentPage.content_Id : Guid.Empty,
				ContentPageId = Guid.Empty,
				ContentPageMainId = (srcContentPage != null) ? srcContentPage.content_main.content_main_Id : Guid.Empty,
				LanguageId = _langId,
				Published = false,
				DateTime = DateTime.Now,
				PortalId = _portalId,
				TabModuleId = _tabModuleId
			};
			return newStagingItem;
		}
	}
}