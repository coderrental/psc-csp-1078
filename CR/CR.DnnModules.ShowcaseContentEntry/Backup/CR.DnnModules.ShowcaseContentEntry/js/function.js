﻿var contentOriginalHeight;

function initApp() {
	categories.Render("#csp-nav");
	categories.RenderTitle("#csp-page-title", crrCatId, categoryLevel);
	categories.RenderBreadcrumb("#csp-breadcrumb", crrCatId, categoryLevel);
	RenderSideItems();
	contentOriginalHeight = $('#csp-content').height();
	//initListButtons();
	initPageEffect();
	//initPopupWindows();
	if (detectNoCategory())
		return;
	initSideControlsTrigger();
	initSideControls();
	initMenu();
	initContentPageEditor();
	initHeaderTitleEditor();
	initToolTips();
	initContextMenus();
}

function RenderSideItems() {
	var siblingItems = categories.FindByAttr('level', categoryLevel);
	var childrenItems = categories.FindByAttr('pid', crrCatId);
	var crrCatEl = categories.Find(crrCatId);
	if (crrCatEl == null)
		return;
	var crrParentId = crrCatEl.pid;
	if (childrenItems.length > 0) {
		siblingItems = childrenItems;
		crrParentId = crrCatId;
	}
	var newSideUL = $('<ul>');
	if (siblingItems.length > 0) {
		newSideUL.attr({
			id: 'side-col-list',
			'class': 'csp-sibling-items'
		});
		for (var i = 0; i < siblingItems.length; i++) {
			if (siblingItems[i].pid != crrParentId)
				continue;
			var newSiblingLI = $('<li>');
			newSiblingLI.attr({
				id: 'c-' + siblingItems[i].id
			});
			if (siblingItems[i].id == crrCatId)
				newSiblingLI.addClass('selected');
			var newSiblingA = $('<a>');
			newSiblingA.html(siblingItems[i].text);
			newSiblingA.attr({
				'class': 'aside-menu-item',
				cateid: siblingItems[i].id,
				rel: 'CategoryTitleEditor-' + siblingItems[i].id,
				href: siblingItems[i].href,
				ccid: siblingItems[i].cnid
			});
			var newSiblingInput = $('<input type="text">');
			newSiblingInput.val(siblingItems[i].text);
			newSiblingInput.attr({
				'class': 'hide',
				id: 'CategoryTitleEditor-' + siblingItems[i].id
			});
			newSiblingLI.append(newSiblingA);
			newSiblingLI.append(newSiblingInput);
			newSideUL.append(newSiblingLI);
		}

		$('#csp-side-col').append(newSideUL);

		$('#side-col-list').sortable({
			cursor: "move",
			update: function (event, ui) {
				var orderedList = '';
				$('#side-col-list li').each(function () {
					var idMixed = $(this).attr('id').split('-');
					if (orderedList != '')
						orderedList += '_';
					orderedList += idMixed[idMixed.length - 1];
				});
				saveCategoryOrder(orderedList, event, ui);
			}
		});
		$('#side-col-list').disableSelection();
	}
}

function initPageEffect() {
	var pageWidth = $('#dnn_ContentPane').width();
	var cspWrapperWidth = $('#csp-wrapper').width();
	var cspWrapperHeight = $('#csp-wrapper').height();
	$('#csp-wrapper').css({
		'margin-left': (pageWidth - cspWrapperWidth) / 2
	});

	if (showAll == '')
		$('#sci-showactive-active').show();
	else
		$('#sci-showall-active').show();
}

function initSideControlsTrigger() {
	if ($('#side-controls-menu').width() <= 0)
		$('#side-controls-menu').hide();
	$('#side-controls-trigger').unbind().click(function () {
		if (originalCategoryCount == 0) {
			alert('There is no page available, add one first.');
			return;
		}
		var isReady = true;
		if ($('.content-page-detail:hidden').length > 0)
			isReady = false;
		if (!isReady)
			return;
		$(this).unbind();

		var cspWrapperHeight = $('#csp-wrapper').height() - 5;
		if ($.browser.msie && parseInt($.browser.version, 10) === 7)
			cspWrapperHeight = $('#csp-wrapper').height();
		$('#side-controls-menu').height(cspWrapperHeight);

		var moveLeng = 200;
		var controlsWidth = -1;
		var cspWrapperMaginLeft = '';
		var controlTriggerRightPos = '';

		if ($('#side-controls-menu').width() > 0) {
			controlsWidth = 0;
			cspWrapperMaginLeft = '+=' + parseInt(moveLeng / 2);
			controlTriggerRightPos = '+=' + parseInt((moveLeng / 2) + 3);
		} else {
			controlsWidth = 200;
			cspWrapperMaginLeft = '-=' + parseInt(moveLeng / 2);
			controlTriggerRightPos = '-=' + parseInt((moveLeng / 2) + 3);
		}

		if (controlsWidth > -1 && cspWrapperMaginLeft != '') {
			if (controlsWidth > 0) {
				$('#side-controls-menu').show();
				$('#side-controls-menu ul').show();
			}
			$('#side-controls-menu').stop().animate({
				width: controlsWidth
			}, 200, function () {
				if (controlsWidth == 0)
					$('#side-controls-menu').hide();
			});
			$('#list-controls-inner').stop().animate({
				'right': controlTriggerRightPos
			}, 200);
			$('#csp-wrapper').stop().animate({
				'margin-left': cspWrapperMaginLeft
			}, 200, function () {
				initSideControlsTrigger();
			});
		}
	});
}

function initSideControls() {
	//Show/hide control 'Add Missing Contents'
	$('#sci-addmissingcontents').closest('li').hide();
	if (missingContentList.length > 0)
		$('#sci-addmissingcontents').closest('li').show();

	$('#sci-newrootitem').click(function () {
		$('#txtParentId').val(rootCatId);
		$('#txtCategoryId').val('0');
		$('#lblParentCategory').html('Root');
		openCategoryDetailForm();
	});

	$('#sci-addmissingcontents').click(function () {
		var categoryIds = '';
		for (var i = 0; i < missingContentList.length; i++) {
			if (categoryIds != '')
				categoryIds += ';';
			categoryIds += missingContentList[i];
		}

		$.ajax({
			type: 'POST',
			url: document.URL,
			dataType: 'xml',
			data: { ajaxaction: 'add_missing_contents', categoryids: categoryIds },
			beforeSend: function (xhr) {
				xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
				switchLoadingPanel(true);
			},
			success: function (responseMsg) {
				window.location = document.URL;
			}
		});
	});

	$('#sci-publishonfly-switch, #sci-publishonfly').unbind().click(function () {
		var btnSwitch = $('#sci-publishonfly-switch');
		var isOff = btnSwitch.hasClass('is-off');
		if (isOff) {
			btnSwitch.removeClass('is-off');
		} else {
			btnSwitch.addClass('is-off');
		}
		$.ajax({
			url: document.URL,
			type: 'POST',
			data: { ajaxaction: 'pof_state', pofstate: isOff },
			beforeSend: function (xhr) {
				xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
				switchLoadingPanel(true);
			},
			success: function (responseMsg) {
				window.location = document.URL;
			}
		});
	});

	$('#sci-broadcast').unbind().click(function () {
		$.ajax({
			url: document.URL,
			type: 'POST',
			data: { ajaxaction: 'broadcast' },
			beforeSend: function (xhr) {
				xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
				switchLoadingPanel(true);
			},
			success: function (responseMsg) {
				var resultCode = parseInt($(responseMsg).find('resultcode').text());
				if (resultCode == 2) {
					alert('Pages had been already up to date');
				}
				else {
					alert('Publish succeeded');
					$('#sci-rollback').parent('li').show();
				}
				switchLoadingPanel(false);
			}
		});
	});

	$('#sci-rollback').unbind().click(function () {
		$.ajax({
			url: document.URL,
			type: 'POST',
			data: { ajaxaction: 'getrollbacklist' },
			dataType: 'xml',
			beforeSend: function (xhr) {
				xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
				$('#WindowRollbackList').fadeIn(function () {
					$(document).unbind('click').bind("click", function (e) {
						if (!$(e.target).closest('#WindowRollbackList').length) {
							$('#WindowRollbackList').hide();
							$(this).unbind('click');
						}
					});
				});
			},
			success: function (responseMsg) {
				var newData = [];
				$(responseMsg).find('checkpoint').each(function () {
					var checkpointId = $(this).find('id').text();
					var dateTime = $(this).find('datetime').text();
					var comment = $(this).find('comment').text();
					var detail = checkpointId;
					newData.push({
						Id: checkpointId,
						Controls: 'Rollback',
						DateTime: dateTime,
						Comment: comment,
						Detail: detail
					});
				});

				var dataSource = new kendo.data.DataSource({
					data: newData
				});

				gridCheckpoint.setDataSource(dataSource);

				$('.checkpoint-link').unbind().click(function () {
					ShowCheckpointDetail($(this));
				});

				$('.go-rollback').unbind().click(function () {
					GoRollback($(this));
				});

				$('.go-sync-checkpoint').unbind().click(function () {
					GoSyncCheckpoint($(this));
				});

				$('.go-rollback img').qtip({
					content: 'Rollback the showcase to this checkpoint',
					position: {
						my: 'bottom center',
						at: 'top center'
					}
				});

				$('.go-sync-checkpoint img').qtip({
					content: 'Sync this checkpoint to the editor',
					position: {
						my: 'bottom center',
						at: 'top center'
					}
				});
			}
		});
	});

	$('#checkpoint-detail-outter, #checkpoint-detail-close-trigger').unbind().click(function () {
		$('#checkpoint-detail-outter').hide();
		$('#checkpoint-detail').hide();
	});

	$("#grid-checkpoint").kendoGrid({
		autoBind: true,
		height: 250,
		sortable: true,
		rowTemplate: kendo.template($("#checkpointRowTemplate").html()),
		columns: [
			{
				field: "Controls",
				title: "&nbsp;",
				width: 50
			},
			{
				field: "DateTime",
				title: "Date Time",
				width: 100
			},
			{
				field: "Comment",
				title: "Comment",
				width: 200
			},
			{
				field: "Detail",
				title: "Detail",
				width: 100
			}
		]
	});
	gridCheckpoint = $("#grid-checkpoint").data("kendoGrid");

	$("#grid-checkpoint-detail").kendoGrid({
		autoBind: true,
		height: 160,
		width: 400,
		sortable: true,
		rowTemplate: kendo.template($("#checkpointDetailRowTemplate").html()),
		columns: [
			{
				field: "Category",
				title: "Page",
				width: 100
			},
			{
				field: "Action",
				title: "Action",
				width: 150
			},
			{
				field: "DateTime",
				title: "DateTime",
				width: 100
			}
		]
	});

}

function ShowCheckpointDetail(thisEl) {
	$.ajax({
		url: document.URL,
		type: 'POST',
		data: { ajaxaction: 'checkpointdetail', checkpointid: thisEl.attr('rel') },
		dataType: 'xml',
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
		},
		success: function (responseMsg) {
			var newData = [];
			$(responseMsg).find('action').each(function () {
				var categoryTitle = $(this).find('category').text();
				var dateTime = $(this).find('datetime').text();
				var actionLog = $(this).find('detail').text();
				newData.push({
					Category: categoryTitle,
					Action: actionLog,
					DateTime: dateTime
				});
			});

			var dataSource = new kendo.data.DataSource({
				data: newData
			});
			var gridDetailCheckpoint = $("#grid-checkpoint-detail").data("kendoGrid");
			gridDetailCheckpoint.setDataSource(dataSource);

			$('#checkpoint-detail-outter').show();
			$('#checkpoint-detail').show();
		}
	});
}

function GoRollback(thisEl) {
	var checkpointId = thisEl.attr('rel');
	$.ajax({
		url: document.URL,
		type: 'POST',
		data: { ajaxaction: 'rollback', checkpointid: checkpointId },
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
			switchLoadingPanel(true);
		},
		success: function (responseMsg) {
			switchLoadingPanel(false);
		}
	});
}

function GoSyncCheckpoint(thisEl) {
	var checkpointId = thisEl.attr('rel');
	var resetAll = false;
	if ($('.go-sync-checkpoint').index(thisEl) == $('.go-sync-checkpoint').length - 1)
		resetAll = confirm('Do you want to sync to original version?');

	$.ajax({
		url: document.URL,
		type: 'POST',
		data: { ajaxaction: 'synccheckpoint', checkpointid: checkpointId, resetall: resetAll },
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
			switchLoadingPanel(true);
		},
		success: function (responseMsg) {
			//switchLoadingPanel(false);
			window.location = document.URL;
		}
	});
}

function detectNoCategory() {
	if (categories.FindByAttr('pid', rootCatId).length > 0)
		return false;
	if (originalCategoryCount > 0) {
		switchLoadingPanel(true);
		window.location = controlUrl + '?showall=true';
		return;
	}
	var newLi = $('<li>').attr({
		'class': 'root-item',
		style: 'width: 659px !important'
	});
	var innerA = $('<a>').attr({
		'class': 'root-item-link mask-item',
		'href': 'javascript: void(0)',
		style: 'width: 649px ! important; padding-top: 24.5px; height: 39.5px;'
	});
	innerA.html('Click here to add new item');
	innerA.click(function () {
		$('#txtParentId').val(rootCatId);
		$('#txtCategoryId').val('0');
		$('#lblParentCategory').html('Root');
		openCategoryDetailForm();
	});
	newLi.append(innerA);
	$('#csp-nav #main-nav').append(newLi);
	initSideControlsTrigger();
	return true;
}

function initListButtons() {
	if (window.location.href.indexOf('showall') >= 0) {
		$('#btnGoActiveList').closest('a.list-button-link').show();
	}
	else {
		$('#btnGoAllList').closest('a.list-button-link').show();
	}
}

function initPopupWindows() {
	/*var wdDeleteCategoryOption = $('#wdDeleteCategoryOption').kendoWindow({
	actions: ["Close"],
	draggable: true,
	height: "150px",
	width: "350px",
	modal: true,
	resizable: false,
	visible: false,
	title: 'UnPublish Options'
	}).data("kendoWindow");*/

	/*var wdCategoryEditForm = $('#wdCategoryEditForm').kendoWindow({
	actions: ["Close"],
	draggable: true,
	height: "215px",
	width: "420px",
	modal: true,
	resizable: false,
	visible: false,
	title: 'Category Detail'
	}).data("kendoWindow");*/

	/*var wdCategoryDescriptionEditor = $('#wdCategoryDescriptionEditor').kendoWindow({
	actions: ["Close"],
	draggable: true,
	height: "160px",
	width: "350px",
	modal: true,
	resizable: false,
	visible: false,
	title: 'Category Description'
	}).data("kendoWindow");*/

	$('#CategoryThumbnailRow').remove();
}

function initContextMenus() {
	var contextMenuItems = {
		"NewSiblingCategory": {
			name: "New sibling item",
			icon: "add",
			callback: function (key, opt) {
				var cateId = $(opt.$trigger).attr('cateid');
				cateId = parseInt(cateId);
				var categoryNode = categories.Find(cateId);
				$('#txtParentId').val(categoryNode.pid);
				$('#txtCategoryId').val('0');
				if (categoryNode.pid == rootCatId)
					$('#lblParentCategory').html('Root');
				else {
					var parentNode = categories.Find(parseInt(categoryNode.pid));
					$('#lblParentCategory').html(parentNode.text);
				}
				openCategoryDetailForm();
			}
		},
		"NewSubCategory": {
			name: "New sub item",
			icon: "add",
			callback: function (key, opt) {
				var catIdMixed = $(opt.$trigger).attr('rel').split('-');
				$('#txtParentId').val(catIdMixed[catIdMixed.length - 1]);
				$('#txtCategoryId').val('0');
				$('#lblParentCategory').html($(this).html());
				openCategoryDetailForm();
			}
		},
		"Rename": {
			name: "Rename",
			icon: "edit",
			callback: function (key, opt) {
				var reEnableSortableList = function () {
					$('.the-main-menu').sortable('enable');
				};
				openCategoryTitleEditor($(opt.$trigger), reEnableSortableList);
				var editorId = $(opt.$trigger).attr('rel');
				var wrapperHeight = $(opt.$trigger).closest('li.root-item').height();
				var wrapperWidth = $(opt.$trigger).closest('li.root-item').width();
				var editorHeight = $('#' + editorId).height();
				var topSpace = (wrapperHeight - editorHeight) / 2;
				$('#' + editorId).css({
					'margin-top': topSpace + 'px',
					width: wrapperWidth + 'px'
				});
				$('.the-main-menu').sortable('disable');
			}
		},
		"CategoryDescription": {
			name: "Edit Description",
			icon: "edit",
			callback: function (key, opt) {
				var catIdMixed = $(opt.$trigger).attr('rel').split('-');
				var catEl = categories.Find(catIdMixed[catIdMixed.length - 1]);
				$('#txtCategoryIdToEditDesc').val(catIdMixed[catIdMixed.length - 1]);
				$('#lblCategoryOfDesc').html($(this).html());
				$('#textCrrDescription').val(htmlEntitiesDecode(catEl.desc));
				$('#textCrrThumbnail').val(catEl.thumbnail);
				var wdCategoryDescriptionEditor = $find(RWCategoryDescriptionEditor);
				wdCategoryDescriptionEditor.show();
			}
		},
		"Delete": {
			name: "UnPublish",
			icon: "delete",
			callback: function (key, opt) {
				var el = $(opt.$trigger);
				$('#txtDeleteCategoryId').val(el.attr('cateid'));
				var wdDeleteCategoryOption = $find(RWDeleteCategoryOption);
				wdDeleteCategoryOption.show();
			}
		}
	};

	$.contextMenu('destroy', '.root-item-link');
	$.contextMenu({
		selector: '.ctmenu-item-link:not(.inactive-item, .a-missing-content),.aside-menu-item:not(.inactive-item, .a-missing-content),.root-item-link:not(.inactive-item, .a-missing-content)',
		items: contextMenuItems,
		events: {
			show: function (opt) {
				$('li.root-item').unbind('hover');
			},
			hide: function (opt) {
				menuEffect();
			}
		},
		build: function ($trigger, e) {
			if ($($trigger).hasClass('un-published')) {
				this.items.Delete.name = 'Publish';
				this.items.Delete.icon = 'publish';
				this.items.Delete.callback = function (key, opt) {
					restoreCategory(opt.$trigger);
				};
			}
			else {
				this.items.Delete.name = 'UnPublish';
				this.items.Delete.icon = 'delete';
				this.items.Delete.callback = function (key, opt) {
					var el = $(opt.$trigger);
					$('#txtDeleteCategoryId').val(el.attr('cateid'));
					if (languagesCount < 2) {
						deleteCategory(false);
						return;
					}
					var wdDeleteCategoryOption = $find(RWDeleteCategoryOption);
					wdDeleteCategoryOption.show();
				};
			}
			return addDynamicItem($trigger, e);
		}
	});

	$.contextMenu({
		selector: '.a-missing-content:not(.inactive-item)',
		items: {
			"AddContent": { name: "Add Content", icon: "add", callback: function (key, opt) {
				switchLoadingPanel(true);
				var categoryId = $(opt.$trigger).attr('cateid');
				$.ajax({
					type: 'POST',
					url: document.URL,
					dataType: 'xml',
					data: { ajaxaction: "add_content", categoryid: categoryId },
					beforeSend: function (xhr) {
						xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
					},
					success: function (responseMsg) {
						window.location = controlUrl + '?cat=' + categoryId + showAll;
					}
				});
			}
			}
		},
		events: {
			show: function (opt) {
				$('li.root-item').unbind('hover');
			},
			hide: function (opt) {
				menuEffect();
			}
		}
	});

	$.contextMenu({
		selector: '.inactive-item',
		items: {
			"Activate": { name: "Activate", icon: "activate", callback: function (key, opt) {
				var thisEl = $(opt.$trigger);
				var categoryId = thisEl.attr('cateid');
				$.ajax({
					type: 'POST',
					url: document.URL,
					dataType: 'xml',
					data: { ajaxaction: "activate_category", categoryid: categoryId },
					beforeSend: function (xhr) {
						xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
					},
					success: function (responseMsg) {
						var resultCode = parseInt($(responseMsg).find('resultcode').text());
						if (resultCode == 1) {
							$('a[cateid="' + categoryId + '"]').each(function () {
								$(this).removeClass('inactive-item');
								$(this).removeClass('un-published');
								$(this).closest('li').find('a.ctmenu-item-link').removeClass('inactive-item');

								if ($(this).parents('div#csp-nav').length > 0)
									activateParentNodes($(this).closest('li'));
								$(this).qtip('destroy');
							});
							initToolTips();
							initContextMenus();
						}
						else {

						}
					}
				});
			}
			}
		},
		events: {
			show: function (opt) {
				$('li.root-item').unbind('hover');
			},
			hide: function (opt) {
				menuEffect();
			}
		}
	});
}

function activateParentNodes(theNode) {
	if (theNode.hasClass('root-item'))
		return;
	var grandLi = theNode.parents('li:first');
	if (grandLi.length == 0)
		return;
	var targetA = grandLi.find('a:first');
	targetA.removeClass('inactive-item');
	targetA.qtip('destroy');
	activateParentNodes(grandLi);
}

function addDynamicItem($trigger, e) {
	var catId = $($trigger).attr('cateid');
	var categoryNode = categories.FindByAttr('id', catId);
	var itemsObj;
	if (categoryNode.length > 0) {
		if (categoryNode[0].issubscribable) {
			itemsObj = {
				"MakeUnSubscribable": {
					name: "UnSubscribable",
					icon: "unsubscribable",
					callback: function (key, opt) {
						changeSubscribeState(categoryNode[0], false);
					}
				}
			};
		}
		else {
			itemsObj = {
				"MakeSubscribable": {
					name: "Subscribable",
					icon: "subscribable",
					callback: function (key, opt) {
						changeSubscribeState(categoryNode[0], true);
					}
				}
			};
		}
	}
	return { items: itemsObj };
}

function changeSubscribeState(targetCategory, status) {
	switchLoadingPanel(true);
	var statusNo = 1;
	if (status == false)
		statusNo = 0;
	$.ajax({
		type: 'POST',
		url: document.URL,
		dataType: 'xml',
		data: { ajaxaction: "change_subscribe_status", categoryid: targetCategory.id, status: statusNo },
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
		},
		success: function (responseMsg) {
			var resultCode = parseInt($(responseMsg).find('resultcode').text());
			if (resultCode == 1) {
				targetCategory.issubscribable = status;
				if (statusNo == 0)
					changeSubscribeStateForChildren(targetCategory.id, statusNo);
				initContextMenus();
			}
			else {

			}
			switchLoadingPanel(false);
		}
	});

}

function changeSubscribeStateForChildren(categoryId, statusNo) {
	if (categoryId < 1)
		return;
	var targetCategory = categories.FindByAttr('id', categoryId);
	if (targetCategory.length < 1)
		return;
	var isSubscribable = true;
	if (statusNo != 1)
		isSubscribable = false;
	targetCategory[0].issubscribable = isSubscribable;
	if (typeof (targetCategory[0].children) != "undefined") {
		var subCategoriesList = categories.FindByAttr('pid', categoryId);
		if (subCategoriesList.length > 0) {
			for (var i = 0; i < subCategoriesList.length; i++)
				changeSubscribeStateForChildren(subCategoriesList[i].id, statusNo);
		}
	}
}

function deleteCategory(isAll) {
	var ajaxAction = 'delete_category';
	if (!isAll)
		ajaxAction = 'unpublish_category';

	var categoryId = $('#txtDeleteCategoryId').val();
	var thisEl = $('a[cateid="' + categoryId + '"]:first');

	$('a[cateid="' + categoryId + '"]').each(function () {
		if ($(this).hasClass('root-item-link'))
			thisEl = $(this);
	});
	$.ajax({
		type: 'POST',
		url: document.URL,
		dataType: 'xml',
		data: { ajaxaction: ajaxAction, categoryid: categoryId },
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
		},
		success: function (responseMsg) {
			var resultCode = $(responseMsg).find('resultcode').text();
			if (isNaN(resultCode)) {
				alert(resultCode);
				return;
			}
			resultCode = parseInt(resultCode);
			if (resultCode == 1) {
				if (showAll == '') {
					$('[ccid="' + thisEl.attr('ccid') + '"]').closest('li').remove();
					if (categoryId == crrCatId || $('#csp-breadcrumb').find('a[catid="' + categoryId + '"]').length == 1) {
						switchLoadingPanel(true);
						window.location = controlUrl;
						return;
					}
					var parentLi = thisEl.closest('li');
					var isRootLi = parentLi.hasClass('root-item');
					parentLi.remove();
					if (isRootLi) {
						hAlignNavItem();
						vAlignNavText();
					}
				}
				else {
					if (isAll)
						$('a[cateid="' + categoryId + '"]').closest('li').find('a').addClass('inactive-item');
					else
						$('a[cateid="' + categoryId + '"]').closest('li').find('a').addClass('un-published');
					initContextMenus();
					initToolTips();
				}
				closeCategoryDeleteWD(null, null);
			}
			else {
				alert('Delete failed, please try again later.');
			}
		}
	});
}

function restoreCategory(thisEl) {
	var categoryId = $(thisEl).attr('cateid');
	$.ajax({
		type: 'POST',
		url: document.URL,
		dataType: 'xml',
		data: { ajaxaction: 'publish_category', categoryid: categoryId },
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
			switchLoadingPanel(true);
		},
		success: function (responseMsg) {
			var resultCode = $(responseMsg).find('resultcode').text();
			if (isNaN(resultCode)) {
				alert(resultCode);
				return;
			}
			resultCode = parseInt(resultCode);
			if (resultCode == 1) {
				$(thisEl).removeClass('un-published');
				$(thisEl).qtip('destroy');
				var parentList = GetCategoryLineage(categoryId);
				for (var i = 0; i < parentList.length; i++) {
					$('li[cateid="' + parentList[i] + '"]').find('a:first').removeClass('un-published');
					$('li[cateid="' + parentList[i] + '"]').find('a:first').qtip('destroy');
				}
				initContextMenus();
				switchLoadingPanel(false);
			}
			else {

			}
		}
	});
}

function openCategoryTitleEditor(thisEl, postBack) {
	var textEditor = $('#' + thisEl.attr('rel'));
	var tmp = thisEl.attr('rel').split('-');
	var categoryId = tmp[tmp.length - 1];
	var ccid = thisEl.attr('ccid');
	var el = thisEl;
	textEditor.fadeIn(function () {
		$(document).unbind('click').bind("click", function (e) {
			if (!$(e.target).closest('#' + el.attr('rel')).length) {
				var val = textEditor.val();
				if (val == "") {
					textEditor.val(el.html());
				}
				else if (val != el.html()) {
					el.html(val);
					renameCategory(categoryId, ccid, val);
				}
				el.fadeIn();
				textEditor.hide();
				$(this).unbind('click');
				if (postBack !== undefined)
					postBack();
			}
		});

		$(this).unbind('keydown').bind('keydown', function (e) {
			if (e.which == 13) {
				e.preventDefault();
				var val = textEditor.val();
				if (val == "") {
					textEditor.val(el.html());
				}
				else if (val != el.html()) {
					el.html(val);
					renameCategory(categoryId, ccid, val, postBack);
				}
				el.fadeIn();
				textEditor.hide();
				$(this).unbind('keydown');
				if (postBack !== undefined)
					postBack();
			}
		});

		$(this).focus();
		$(this).select();
	});
	el.hide();
}

function renameCategory(catId, ccId, catName, postBack) {
	if (catId < 1 || catName == '') return 0;
	$.ajax({
		url: document.URL,
		type: 'POST',
		dataType: 'xml',
		data: { ajaxaction: 'rename_category', categoryid: catId, categoryname: catName, contentcategoryid: ccId },
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
		},
		success: function (responseMsg) {
			var resultCode = parseInt($(responseMsg).find('resultcode').text());
			if (resultCode != 1)
				alert('Rename failed, please try again later.');
			else {
				//console.log(controlUrl + '?cat=' + catId);
				$('#csp-wrapper').find('a[href^="' + controlUrl + '?cat=' + catId + '&"]').each(function () {
					$(this).html(catName);
					$(this).closest('li').find('input[type="text"]').val(catName);
				});
			}
		}
	});
}

function openCategoryDetailForm(catId) {
	var parentId = parseInt($('#txtParentId').val());
	$('#chkIsSubscribable').removeAttr('disabled');
	if (parentId != rootCatId) {
		var parentCat = categories.Find(parentId);
		if (parentCat.issubscribable == false)
			$('#chkIsSubscribable').attr('disabled', 'disabled');
	}
	$('#wdCategoryEditForm').find('.error-msg').hide();
	$('#wdCategoryEditForm').removeClass('hide').show();
	var oWnd = $find(RWCategoryEditForm);
	oWnd.show();
	$('#txtCategoryText').unbind().keydown(function () {
		if ($('#errormsg-txtCategoryText').is(':visible'))
			$('#errormsg-txtCategoryText').hide();
	});
}

function vAlignNavText() {
	$('.root-item-link').each(function () {
		var thisHeight = $(this).height();
		var wrapperHeight = $(this).closest('li').height();
		var topPos = (wrapperHeight - thisHeight) / 2;
		var newHeight = wrapperHeight - topPos;
		$(this).css({ 'padding-top': topPos + 'px', 'height': newHeight + 'px' });
	});
}

function hAlignNavItem() {
	//var navWidth = 510;
	var navWidth = $('#csp-nav').width();
	var menuItemCount = $('li.root-item').length;
	var itemWidth = (navWidth / menuItemCount) - 1;
	//var crrStyle = $('li.root-item').attr('style');
	$('li.root-item').attr('style', 'width: ' + itemWidth + 'px !important');
	// $('a.root-item-link').attr('style', 'width: ' + parseInt(itemWidth - 10) + 'px !important');
}

function initMenu() {
	menuEffect();

	hAlignNavItem();

	var subMenuItemCount;
	var widthPercent;
	$('ul.sub-menu').each(function () {
		subMenuItemCount = $(this).find('li.sub-item').length;
		widthPercent = (100 / subMenuItemCount) - 3;
		$(this).find('li.sub-item').attr('style', 'width: ' + widthPercent + '% !important; padding-right: 3% !important');
	});

	vAlignNavText();
}

/*function initLeftCol() {
$('.aside-menu-item').each(function () {
var idMixed = $(this).attr('rel').split('-');
var catId = idMixed[idMixed.length - 1];
var categoryNode = categories.FindByAttr("id", catId);
if (categoryNode.length > 0) {
$(this).attr({
'href': categoryNode[0].href,
'ccid': categoryNode[0].cnid
});

if (categoryNode[0].cnid == emptyGuid) {
$(this).addClass('a-missing-content');
$(this).parent('li').addClass('missing-content-small');
}

if (categoryNode[0].active != true)
$(this).addClass('inactive-item');
}
if (catId == crrCatId)
$(this).closest('li').addClass('selected');
});

$('#side-col-list').sortable({
cursor: "move",
update: function (event, ui) {
var orderedList = '';
$('#side-col-list li').each(function () {
var idMixed = $(this).attr('id').split('-');
if (orderedList != '')
orderedList += '_';
orderedList += idMixed[idMixed.length - 1];	
});
saveCategoryOrder(orderedList, event, ui);
}
});
$('#side-col-list').disableSelection();
}*/

function updateMainNavOrder(event, ui) {
	var orderedList = '';
	$('.root-item').each(function () {
		var idMixed = $(this).find('a.root-item-link').attr('cateid');
		if (orderedList != '')
			orderedList += '_';
		orderedList += idMixed;
	});
	saveCategoryOrder(orderedList, event, ui);
}

function updateSubMenuOrder(event, ui) {
	var ulParent = ui.item.closest('ul.sub-menu');
	var orderedList = '';
	ulParent.find('li.sub-item').each(function () {
		var idMixed = $(this).attr('cateid');
		if (orderedList != '')
			orderedList += '_';
		orderedList += idMixed;
	});
	saveCategoryOrder(orderedList, event, ui);
}

function updateSubMenuDetailOrder(event, ui) {
	var ulParent = ui.item.closest('ul.sub-menu-detail');
	var orderedList = '';
	ulParent.find('li.ctmenu-item').each(function () {
		var idMixed = $(this).attr('cateid');
		if (orderedList != '')
			orderedList += '_';
		orderedList += idMixed;
	});
	saveCategoryOrder(orderedList, event, ui);
}

function saveCategoryOrder(theList, event, ui) {
	var categoryId = $(ui.item).attr('cateid');
	$.ajax({
		url: document.URL,
		type: 'POST',
		dataType: 'xml',
		data: { ajaxaction: 'order_category', orderlist: theList, categoryid: categoryId },
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
		},
		success: function (responseMsg) {

		},
		error: function (errorMsg) {
		}
	});
}

function pad(number, length) {
	var str = '' + number;
	while (str.length < length) {
		str = '0' + str;
	}
	return str;
}

function menuEffect() {
	$('a[cateid="' + crrCatId + '"]').parents('li.root-item').addClass('selected');
	$('li.root-item').unbind('hover').hover(function () {
		$(this).addClass('root-item-hover');
		$(this).find('div.sub-menu-wrapper').show();
		if ($('#csp-content').height() < $(this).find('div.sub-menu-wrapper').height())
			$('#csp-content').height($(this).find('div.sub-menu-wrapper').height());
	}, function () {
		$(this).removeClass('root-item-hover');
		$(this).find('div.sub-menu-wrapper').hide();
	});
}

function removeParentCatFromLeftCol() {
	var pIdArr = new Array();
	$('#csp-side-col a').each(function () {
		pIdArr.push($(this).attr('pid'));
	});
	var sorted_arr = pIdArr.sort();

	var results = [];
	for (var i = 0; i < pIdArr.length - 1; i++) {
		if (sorted_arr[i + 1] != sorted_arr[i]) {
			results.push(sorted_arr[i]);
		}
	}

	$('#csp-side-col a[pid="' + results + '"]').closest('li').remove();
}

function initContentPageEditor() {
	$(".content-page-detail").click(function () {
		if ($('#side-controls-menu').width() > 0)
			$('#side-controls-trigger').click();
		var el = $(this);
		var targetObject = $('#' + el.attr("editorid"));
		var contentId = el.attr('contentid');
		$('#csp-content').attr('style', 'height: auto !important');
		resizeRadEditor();
		if (targetObject.length > 0) {
			el.hide();
			$(targetObject).keypress(function (event) {
				if (event.which == 13 && $(this).attr("type") == "text") {
					event.preventDefault();
				} // press enter
			});
			targetObject.fadeIn(function () {
				$(document).unbind('click').bind("click", function (e) {
					if (isRWCategoryTreeOpen) {
						return;
					}
					if (!$(e.target).closest('#' + el.attr("editorid")).length) {
						var editor = $find(el.attr("editorid"));
						var val = editor.get_html();
						if (val == "") val = "No content available";
						el.html(val);
						el.fadeIn();
						targetObject.hide();
						$(this).unbind('click');
						$('#csp-content').removeAttr('style');
						updateContentPage(contentId, val);
					}
				});
			});
			targetObject.focus();
		}
	});
}

function resizeRadEditor() {
	if ($('.RadEditor .reContentArea').is(':visible')) {
		$('.RadEditor .reContentArea').attr('style', 'width:' + ($('#csp-right-col').width() - 15) + 'px !important; height: ' + $('.RadEditor .reContentCell').height() + 'px');
	}
	else if ($('.RadEditor .reContentCell iframe').is(':visible'))
		$('.RadEditor .reContentCell iframe').attr('style', 'width:' + ($('#csp-right-col').width() - 15) + 'px !important; height: ' + $('.RadEditor .reContentCell').height() + 'px');
}

function updateContentPage(contentId, contentText) {
	$.ajax({
		url: document.URL,
		type: 'POST',
		dataType: 'xml',
		data: { ajaxaction: 'save_contentpage', contentid: contentId, contenttext: contentText, categoryid: crrCatId },
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
		},
		success: function (responseMsg) {
			var resultCode = parseInt($(responseMsg).find('resultcode').text());
			if (resultCode != 1)
				alert('Failed, please try again later.');
		}
	});
}

function SaveCategoryForm(sender, args) {
	if ($.trim($('#txtCategoryText').val()) == '') {
		$('#errormsg-txtCategoryText').show();
		return;
	}
	sender.set_enabled(false);

	var isSubscribable = 1;
	if ($('#chkIsSubscribable').is(':checked') == false)
		isSubscribable = 0;
	var categoryData = {
		ajaxaction: 'save_category',
		categoryid: $('#txtCategoryId').val(),
		categorytext: $('#txtCategoryText').val(),
		categorydesc: $('#textDescription').val(),
		categorythumbnail: $('#txtCategoryThumbnail').val(),
		parentid: $('#txtParentId').val(),
		displayorder: isNaN($('#txtDisplayOrder').val()) ? 1 : $('#txtDisplayOrder').val(),
		issubscribable: isSubscribable
	};

	$.ajax({
		url: document.URL,
		type: 'POST',
		dataType: 'xml',
		data: categoryData,
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
			closeCategoryFormWD();
			switchLoadingPanel(true);
		},
		success: function (responseMsg) {
			var newCategory = $(responseMsg).find('category');
			if (newCategory.length == 0) {
				alert('Create failed, please try again.');
				sender.set_enabled(true);
				switchLoadingPanel(false);
				return;
			}
			window.location = controlUrl + '?cat=' + newCategory.find('id').text(); // +'&lvl=' + newCategory.find('level').text();
		},
		error: function () {
			sender.set_enabled(true);
			alert('Create failed, please try again.');
		}
	});
}

function SaveCategoryDescForm(sender, args) {
	sender.set_enabled(false);
	var wdCategoryDescriptionEditor = $find(RWCategoryDescriptionEditor);
	var categoryData = {
		ajaxaction: 'save_category_description',
		categoryid: $('#txtCategoryIdToEditDesc').val(),
		categorydesc: $('#textCrrDescription').val(),
		categorythumbnail: $('#textCrrThumbnail').val()
	};
	$.ajax({
		url: document.URL,
		type: 'POST',
		dataType: 'xml',
		data: categoryData,
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
			wdCategoryDescriptionEditor.close();
			switchLoadingPanel(true);
		},
		success: function (responseMsg) {
			var catEl = categories.Find($('#txtCategoryIdToEditDesc').val());
			catEl.desc = htmlEntities($('#textCrrDescription').val());
			catEl.thumbnail = $('#textCrrThumbnail').val();
			sender.set_enabled(true);
			switchLoadingPanel(false);
		},
		error: function () {
			sender.set_enabled(true);
			switchLoadingPanel(false);
			alert('Update failed, please try again.');
		}
	});
}

function closeCategoryDescFormWD() {
	var oWnd = $find(RWCategoryDescriptionEditor);
	oWnd.close();
}

function clearCategoryForm() {
	$('#txtCategoryId').val('0');
	$('#txtCategoryText').val('');
	$('#txtParentId').val('0');
	$('#txtDisplayOrder').val('');
	$('#chkIsSubscribable').attr('checked', false);
	$('#txtCategoryThumbnail').val('');
	$('#textDescription').val('');
}

function closeCategoryFormWD(sender, args) {
	var oWnd = $find(RWCategoryEditForm);
	oWnd.close();
	clearCategoryForm();
}

function closeCategoryDeleteWD(sender, args) {
	var wdDeleteCategoryOption = $find(RWDeleteCategoryOption);
	wdDeleteCategoryOption.close();
	$('#txtDeleteCategoryId').val('0');
}

function deleteCategoryAllLang(sender, args) {
	deleteCategory(true);
}

function deleteCategoryThisLang(sender, args) {
	deleteCategory(false);
}

function initHeaderTitleEditor() {
	$('#header-category-title').click(function () {
		openCategoryTitleEditor($(this));
	});
}

function initToolTips() {
	$('li.missing-content-small').each(function () {
		if ($(this).find('a.inactive-item').length == 0) {
			$(this).qtip({
				content: ttMsgContentMissing
			});
		}
	});

	$('.inactive-item').qtip({
		content: ttMsgInactiveItem,
		position: {
			my: 'left center',
			at: 'right center'
		}
	});

	$('.un-published:not(.inactive-item)').qtip({
		content: ttMsgUnPublishedItem,
		position: {
			my: 'left center',
			at: 'right center'
		}
	});
}

function RadFileExplorerFileClicked(sender, args) {
	var item = args.get_item();
	if (!item || item.isDirectory()) return;
	args.set_cancel(true);
	if (FileExplorerTrigger == 'CategoryThumbnail' || FileExplorerTrigger == 'CrrCategoryThumbnail') {
		var imageTypes = new Array('jpg', 'jpeg', 'gif', 'png');
		var fileExtension = item.get_extension();
		if ($.inArray(fileExtension, imageTypes) == -1) {
			alert('Please select image only');
			return;
		}
		var fileName = item.get_url();

		if (FileExplorerTrigger == 'CategoryThumbnail')
			$('#txtCategoryThumbnail').val(hostUrl + fileName);
		else
			$('#textCrrThumbnail').val(hostUrl + fileName);
		var radWD = $find(RWBannerFileMan);
		radWD.close();
		FileExplorerTrigger = '';
		return;
	}
	if (FileExplorerTrigger == 'AddDocLink') {

	}
}

function switchLoadingPanel(flag) {
	var loadingPanel = $find(ajaxLoadingPanelId);
	if (loadingPanel == null) {
		return;
	}
	if (flag) {
		loadingPanel.show(mainPane);
		return;
	}
	loadingPanel.hide(mainPane);
}

function htmlEntitiesDecode(str) {
	var decoded = $('<div/>').html(str).text();
	return decoded;
}

function htmlEntities(str) {
	return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

function GetCategoryLineage(catId) {
	var categoryItem = categories.Find(catId);
	var lineageArr = [catId];
	p = categories.Find(categoryItem.pid);
	while (p != null) {
		lineageArr.unshift(p.id);
		p = categories.Find(p.pid);
	}
	lineageArr.unshift(rootCatId);
	return lineageArr;
}