﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CSPShowcase_CheckpointDetail]') AND type in (N'U'))
DROP TABLE [dbo].[CSPShowcase_CheckpointDetail]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CSPShowcase_Checkpoint]') AND type in (N'U'))
DROP TABLE [dbo].[CSPShowcase_Checkpoint]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CSPShowcase_Staging]') AND type in (N'U'))
DROP TABLE [dbo].[CSPShowcase_Staging]
GO

/****** Object:  Table [dbo].[CSPShowcase_Staging]    Script Date: 07/08/2013 13:38:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CSPShowcase_Staging](
	[Id] [uniqueidentifier] NOT NULL,
	[ParentCategoryId] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[DisplayOrder] [varchar](255) NOT NULL,
	[ContentCategoryMainId] [uniqueidentifier] NOT NULL,
	[SourceContentCategoryId] [uniqueidentifier] NOT NULL,
	[ContentCategoryId] [uniqueidentifier] NOT NULL,
	[ContentPageMainId] [uniqueidentifier] NOT NULL,
	[SourceContentPageId] [uniqueidentifier] NOT NULL,
	[ContentPageId] [uniqueidentifier] NOT NULL,
	[LanguageId] [int] NOT NULL,
	[Published] [bit] NOT NULL,
	[DateTime] [datetime] NOT NULL,
	[PortalId] [int] NOT NULL,
	[TabModuleId] [int] NOT NULL,
	[ActionId] [int] NULL,
	[ActionComment] [nvarchar](255) NULL,
	[IsBackupVersion] [bit] NULL,
	[IsOriginalVersion] [bit] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_CSPShowcase_Staging] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[CSPShowcase_Staging] ADD  CONSTRAINT [DF_CSPShowcase_Staging_VersionBackup]  DEFAULT ((0)) FOR [IsBackupVersion]
GO

ALTER TABLE [dbo].[CSPShowcase_Staging] ADD  CONSTRAINT [DF_CSPShowcase_Staging_IsOriginalVersion]  DEFAULT ((0)) FOR [IsOriginalVersion]
GO

/****** Object:  Table [dbo].[CSPShowcase_Checkpoint]    Script Date: 07/09/2013 09:54:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CSPShowcase_Checkpoint](
	[CheckpointId] [uniqueidentifier] NOT NULL,
	[DateTime] [datetime] NOT NULL,
	[Comment] [nvarchar](150) NULL,
	[PortalId] [int] NOT NULL,
	[TabModuleId] [int] NOT NULL,
 CONSTRAINT [PK_CSPShowcase_Checkpoint] PRIMARY KEY CLUSTERED 
(
	[CheckpointId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[CSPShowcase_CheckpointDetail]    Script Date: 07/08/2013 13:38:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CSPShowcase_CheckpointDetail](
	[CheckpointDetailId] [uniqueidentifier] NOT NULL,
	[CheckpointId] [uniqueidentifier] NOT NULL,
	[StagingId] [uniqueidentifier] NOT NULL,
	[IsBackupVersion] [bit] NULL,
 CONSTRAINT [PK_CSPShowcase_CheckpointDetail] PRIMARY KEY CLUSTERED 
(
	[CheckpointDetailId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CSPShowcase_CheckpointDetail]  WITH CHECK ADD  CONSTRAINT [FK_CSPShowcase_CheckpointDetail_CSPShowcase_Checkpoint] FOREIGN KEY([CheckpointId])
REFERENCES [dbo].[CSPShowcase_Checkpoint] ([CheckpointId])
GO

ALTER TABLE [dbo].[CSPShowcase_CheckpointDetail] CHECK CONSTRAINT [FK_CSPShowcase_CheckpointDetail_CSPShowcase_Checkpoint]
GO

ALTER TABLE [dbo].[CSPShowcase_CheckpointDetail]  WITH CHECK ADD  CONSTRAINT [FK_CSPShowcase_CheckpointDetail_CSPShowcase_Staging] FOREIGN KEY([StagingId])
REFERENCES [dbo].[CSPShowcase_Staging] ([Id])
GO

ALTER TABLE [dbo].[CSPShowcase_CheckpointDetail] CHECK CONSTRAINT [FK_CSPShowcase_CheckpointDetail_CSPShowcase_Staging]
GO

ALTER TABLE [dbo].[CSPShowcase_CheckpointDetail] ADD  CONSTRAINT [DF_CSPShowcase_CheckpointDetail_IsBackupVersion]  DEFAULT ((0)) FOR [IsBackupVersion]
GO