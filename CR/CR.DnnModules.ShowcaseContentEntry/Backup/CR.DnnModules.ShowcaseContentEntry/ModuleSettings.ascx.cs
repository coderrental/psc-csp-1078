﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.DnnModules.Common;
using CR.DnnModules.ShowcaseContentEntry.Common;
using DotNetNuke.Entities.Modules;
using CR.ContentObjectLibrary.Data;
using Telerik.Web.UI;

namespace CR.DnnModules.ShowcaseContentEntry
{
	public partial class ModuleSettings : ModuleSettingsBase
	{
		/// <summary>
		/// Loads the settings.
		/// </summary>
		/// <author>ducuytran - 3/6/2013</author>
		public override void LoadSettings()
		{
			
			if (TabModuleSettings[Cons.SETTING_CATEGORYCONTENT_CONTENTTYPE_ID] != null)
			{
				tbxCategoryContentId.Text = TabModuleSettings[Cons.SETTING_CATEGORYCONTENT_CONTENTTYPE_ID].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_PAGE_CONTENTTYPE_ID] != null)
			{
				tbxPageContentId.Text = TabModuleSettings[Cons.SETTING_PAGE_CONTENTTYPE_ID].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_MAIN_MENU_DEPTH] != null)
			{
				tbxMainMenuDepth.Text = TabModuleSettings[Cons.SETTING_MAIN_MENU_DEPTH].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_PUBLISH_ON_THE_FLY] != null)
			{
				chkPublishOnTheFly.Checked = int.Parse(TabModuleSettings[Cons.SETTING_PUBLISH_ON_THE_FLY].ToString()) > 0;
			}
			if (TabModuleSettings[Cons.SETTING_PUBLICATION_SCHEME_ID] != null)
			{
				tbxPublicationSchemeId.Text = TabModuleSettings[Cons.SETTING_PUBLICATION_SCHEME_ID].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_CONTENT_STAGING_SCHEME_ID] != null)
			{
				tbxContentStagingSchemeId.Text = TabModuleSettings[Cons.SETTING_CONTENT_STAGING_SCHEME_ID].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_UPLOAD_FOLDER] != null)
			{
				tbxUploadFolder.Text = TabModuleSettings[Cons.SETTING_UPLOAD_FOLDER].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_IMAGE_URL] != null)
			{
				tbxImageUrl.Text = TabModuleSettings[Cons.SETTING_IMAGE_URL].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_CSS_URL] != null)
			{
				tbxCSSUrl.Text = TabModuleSettings[Cons.SETTING_CSS_URL].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_JS_URL] != null)
			{
				tbxJSUrl.Text = TabModuleSettings[Cons.SETTING_JS_URL].ToString();
			}
			InitCompanyParameterSetting();
		}

		private void InitCompanyParameterSetting()
		{
			CspDataContext cspDataContext = new CspDataContext(Utils.GetConnectionString(PortalId));
			List<companies_parameter_type> coParamTypes = cspDataContext.companies_parameter_types.OrderBy(a => a.parametername).ToList();
			List<CompanyParameter> crrCoParams = new List<CompanyParameter>();
			List<CompanyParameter> srcCoParams = new List<CompanyParameter>();

			if (TabModuleSettings[Cons.SETTING_COMPAMY_PARAMETERS] != null && !String.IsNullOrEmpty(TabModuleSettings[Cons.SETTING_COMPAMY_PARAMETERS].ToString()))
			{
				String crrCoParamsString = TabModuleSettings[Cons.SETTING_COMPAMY_PARAMETERS].ToString();
				foreach (string coParam in crrCoParamsString.Split(','))
				{
					crrCoParams.Add(new CompanyParameter
					                	{
					                		Id = coParam,
											Text = coParam
					                	});
				}
				if (crrCoParams.Any())
				{
					cpListBoxDest.DataSource = crrCoParams;
					cpListBoxDest.DataBind();
				}
			}

			foreach (companies_parameter_type companiesParameterType in	coParamTypes)
			{
				if (crrCoParams.Any())
				{
					bool isExist = false;
					foreach (CompanyParameter crrCoParam in crrCoParams)
					{
						if (crrCoParam.Text == companiesParameterType.parametername)
						{
							isExist = true;
							break;
						}
					}
					if (isExist)
						continue;
				}
				srcCoParams.Add(new CompanyParameter
				                              	{
				                              		Id = companiesParameterType.parametername,
				                              		Text = companiesParameterType.parametername
				                              	});
			}
			cpListBoxSrc.DataSource = srcCoParams;
			cpListBoxSrc.DataBind();
		}

		/// <summary>
		/// Updates the settings.
		/// </summary>
		/// <author>ducuytran - 3/6/2013</author>
		public override void UpdateSettings()
		{
			var moduleController = new ModuleController();
			
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_CATEGORYCONTENT_CONTENTTYPE_ID, tbxCategoryContentId.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_PAGE_CONTENTTYPE_ID, tbxPageContentId.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_MAIN_MENU_DEPTH, tbxMainMenuDepth.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_PUBLISH_ON_THE_FLY, chkPublishOnTheFly.Checked ? "1" : "0");
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_PUBLICATION_SCHEME_ID, tbxPublicationSchemeId.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_CONTENT_STAGING_SCHEME_ID, tbxContentStagingSchemeId.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_UPLOAD_FOLDER, tbxUploadFolder.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_IMAGE_URL, tbxImageUrl.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_CSS_URL, tbxCSSUrl.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_JS_URL, tbxJSUrl.Text);

			//Update Company Parameters Setting
			String newCoParams = "";
			foreach (RadListBoxItem radListBoxItem in cpListBoxDest.Items)
			{
				if (newCoParams != "")
					newCoParams += ",";
				newCoParams += radListBoxItem.Text;
			}

			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_COMPAMY_PARAMETERS, newCoParams);

			ModuleController.SynchronizeModule(ModuleId);
		}
	}

	public class CompanyParameter
	{
		public string Text { get; set; }

		public string Id { get; set; }
	}
}