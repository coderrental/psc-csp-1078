﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PageManagement.aspx.cs" Inherits="CR.DnnModules.ShowcaseContentEntry.PageManagement" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager runat="server" ID="RadScriptMan"></telerik:RadScriptManager>

	<telerik:RadAjaxManager runat="server" ID="RadAjaxManager1">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="PageTreePanel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PageTreePanel" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" Skin="Vista">
    </telerik:RadAjaxLoadingPanel>

    <asp:Panel runat="server" ID="PageTreePanel">
		<telerik:RadTreeView runat="server" ID="PageTree" DataFieldID="Id" DataTextField="Text" DataValueField="Id" DataFieldParentID="ParentId" 
			EnableDragAndDrop="True" MultipleSelect="False" OnNodeDrop="PageTree_HandleDrop"
			OnClientNodeDragStart="ClientNodeDragStart" OnClientNodeDropping="onNodeDropping" OnClientNodeDragging="onNodeDragging"
            EnableDragAndDropBetweenNodes="true">
				
		</telerik:RadTreeView>
    </asp:Panel>
	<telerik:RadScriptBlock runat="server">
		<script type="text/javascript">
			function ClientNodeDragStart(sender, eventArgs) {
				try {
					CustomClientNodeDragStart(sender, eventArgs);
				} catch (e) {
					var node = eventArgs.get_node();
					if (node.get_level() == 0) {
						eventArgs.set_cancel(true);
					}
				}

			}

			function onNodeDragging(sender, args) {
				try {
					CustomNodeDragging(sender, args);
				} catch (e) {
					var target = args.get_htmlElement();
					if (!target) return;
				}
			}

			function onNodeDropping(sender, args) {
				try {
					CustomNodeDropping(sender, args);
				} catch (e) {
					var dest = args.get_destNode();
					if (dest) {
						var clientSide = false;
						if (clientSide) {
							clientSideEdit(sender, args);
							args.set_cancel(true);
							return;
						}
					}
				}
			}

			function clientSideEdit(sender, args) {
				var destinationNode = args.get_destNode();

				if (destinationNode) {
					var firstTreeView = $find('<%= PageTree.ClientID %>');

					firstTreeView.trackChanges();
					//secondTreeView.trackChanges();
					var sourceNodes = args.get_sourceNodes();
					var dropPosition = args.get_dropPosition();

					//Needed to preserve the order of the dragged items
					if (dropPosition == "below") {
						for (var i = sourceNodes.length - 1; i >= 0; i--) {
							var sourceNode = sourceNodes[i];
							sourceNode.get_parent().get_nodes().remove(sourceNode);

							insertAfter(destinationNode, sourceNode);
						}
					}
					else {
						for (var i = 0; i < sourceNodes.length; i++) {
							var sourceNode = sourceNodes[i];
							sourceNode.get_parent().get_nodes().remove(sourceNode);

							if (dropPosition == "over")
								destinationNode.get_nodes().add(sourceNode);
							if (dropPosition == "above")
								insertBefore(destinationNode, sourceNode);
						}
					}
					destinationNode.set_expanded(true);
					firstTreeView.commitChanges();
					//secondTreeView.commitChanges();
				}
			}

			function insertBefore(destinationNode, sourceNode) {
				var destinationParent = destinationNode.get_parent();
				var index = destinationParent.get_nodes().indexOf(destinationNode);
				destinationParent.get_nodes().insert(index, sourceNode);
			}

			function insertAfter(destinationNode, sourceNode) {
				var destinationParent = destinationNode.get_parent();
				var index = destinationParent.get_nodes().indexOf(destinationNode);
				destinationParent.get_nodes().insert(index + 1, sourceNode);
			}
		</script>
	</telerik:RadScriptBlock>
    </form>
</body>
</html>
