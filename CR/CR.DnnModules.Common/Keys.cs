﻿namespace CR.DnnModules.Common
{
	public class Keys
	{
		public static string CspIntegrationKey = "CspId";
		public static string ModuleTitleKey = "ModuleTitle";
		public static string ModuleErrorIntegrationKey = "ModuleError.MissingIntegrationKey";
		public static string ModuleErrorChanelExternalId = "ModuleError.InvalidChannelExternalId";
		public static string ModuleErrorPortalConnectionString = "ModuleError.MissingConnectionString";
		public static int CspPublishStageId = 50;
		public static int CspUnPublishStageId = 51;
	}
}
