﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CR.ContentObjectLibrary.Data;
using Telerik.Web.UI;

namespace CR.DnnModules.Common.CategoryTree
{
	/// <summary>
	/// Custom Category Tree Node
	/// </summary>
	public class CustomCategoryTreeNode
	{
		public int ParentId { get; set; }
		public int Id { get; set; }
		public string Text { get; set; }
		public CustomCategoryTreeNode Parent { get; set; }
		public List<CustomCategoryTreeNode> Children { get; set; }
		public int Count { get; set; }
		public content CategoryContent { get; set; }

		public CustomCategoryTreeNode(int parentId, int id, string text, int count)
		{
			ParentId = parentId;
			Id = id;
			Text = text;
			Parent = null;
			Children = new List<CustomCategoryTreeNode>();
			Count = count;
		}
		public CustomCategoryTreeNode()
			: this(-1, -1, string.Empty, 0)
		{
		}

		public CustomCategoryTreeNode(CustomCategoryTreeNode parent, int id, string text, int count)
		{
			Parent = parent;
			ParentId = parent.Id;
			Id = id;
			Text = text;
			Children = new List<CustomCategoryTreeNode>();
			Count = count;
		}

		/// <summary>
		/// create a nested tree from a flat array
		/// </summary>
		/// <param name="categories">category array</param>
		/// <returns>nested tree node</returns>
		public static CustomCategoryTreeNode Create(List<category> categories)
		{
			if (categories.Count == 0)
				return null;

			//create parent node
			CustomCategoryTreeNode node = new CustomCategoryTreeNode(-1, -1, "All", 0);
			CustomCategoryTreeNode temp = null;
			foreach (category category in categories.OrderBy(a => a.depth))
			{
				temp = node.Find(category.parentId, category.lineage);
				if (temp == null)
					node.Children.Add(new CustomCategoryTreeNode(-1, category.categoryId, category.categoryText, 0));
				else
					temp.Children.Add(new CustomCategoryTreeNode(temp, category.categoryId, category.categoryText, 0));
			}
			return node;
		}

		/// <summary>
		/// create a nested tree from a flat array
		/// </summary>
		/// <param name="categories">category array</param>
		/// <returns>nested tree node</returns>
		public static CustomCategoryTreeNode Create(List<CustomCategory> categories)
		{
			if (categories.Count == 0)
				return null;

			//create parent node
			var node = new CustomCategoryTreeNode(-1, -1, "All", 0);
			CustomCategoryTreeNode temp = null;
			var totalAssetCount = 0;
			foreach (CustomCategory category in categories.OrderBy(a => a.Depth))
			{
				temp = node.Find(category.ParentId, category.Lineage);
				if (temp == null)
				{
					node.Count += category.Count;
					node.Children.Add(new CustomCategoryTreeNode(-1, category.Id, category.Text, category.Count));
				}
				else
				{
					temp.Count += category.Count;
					var customeCategoryTreeNode = new CustomCategoryTreeNode(temp, category.Id, category.Text, category.Count);
					if (category.CategoryContent != null)
						customeCategoryTreeNode.CategoryContent = category.CategoryContent;
					temp.Children.Add(customeCategoryTreeNode);
				}

				totalAssetCount += category.Count;
			}
			node.Count = totalAssetCount;
			return node;
		}

		/// <summary>
		/// recursive find function
		/// </summary>
		/// <param name="parentId">category parent id to look for</param>
		/// <param name="lineage">lineage to help with missing levels</param>
		/// <returns>tree node if found else null</returns>
		private CustomCategoryTreeNode Find(int? parentId, string lineage)
		{
			if (!parentId.HasValue)
				return null;
			if (Id == parentId.Value)
				return this;

			CustomCategoryTreeNode node = null;
			foreach (CustomCategoryTreeNode child in Children)
			{
				node = child.Find(parentId, lineage);
				if (node != null)
					return node;
			}

			//Disable render tree by lineage for correct data
			/*// attempt to find parent node using lineage
			string[] lineages = lineage.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);

			if (lineages.Length > 1)
			{
				for (int i = lineages.Length - 1; i >= 0; i--)
				{
					node = this.Find(Convert.ToInt32(lineages[i]), "");
					if (node != null)
						return node;
				}
			}*/

			return node;
		}

		#region Overrides of Object

		public override string ToString()
		{
			return string.Format("Id: {0}, Parent Id: {1}, [{2} ({3})], Children Count: {4}", Id, ParentId, Text, Count, Children.Count);
		}

		#endregion
	}

	public class CustomCategory
	{
		public string Text { get; set; }
		public int Id { get; set; }
		public int ParentId { get; set; }
		public int Depth { get; set; }
		public string Lineage { get; set; }
		public int Count { get; set; }
		public content CategoryContent { get; set; }

		public CustomCategory(string text, int id, int parentId, int depth, string lineage)
		{
			Text = text;
			Id = id;
			ParentId = parentId;
			Depth = depth;
			Lineage = lineage;
			Count = 0;
		}

		public CustomCategory()
			: this(string.Empty, -1, -1, -1, string.Empty)
		{
		}

		#region Overrides of Object

		public override string ToString()
		{
			return string.Format("Id: {0}, Parent Id: {1}, [{2} ({3})]", Id, ParentId, Text, Count);
		}

		#endregion
	}

	public static class CategoryTreeView
	{
		#region Nested Category Tree
		/// <summary>
		/// if user category exceptions exist:
		///     render user exception categories
		/// else if category exceptions exist
		///     render exception categories
		/// else 
		///     render all categories
		/// </summary>
		public static void RenderCategoryTree(RadTreeView tree, List<category> categories)
		{
			if (categories.Count == 0)
				return;

			CustomCategoryTreeNode root = CustomCategoryTreeNode.Create(categories);
			RenderNestedTree(tree, null, root);
		}

		public static void RenderCategoryTree(RadTreeView tree, List<CustomCategory> categories)
		{
			if (categories.Count == 0)
				return;
			CustomCategoryTreeNode root = CustomCategoryTreeNode.Create(categories);
			RenderNestedTree(tree, null, root);
		}


		/// <summary>
		/// recursive function to create a nest tree
		/// </summary>
		/// <param name="tree"></param>
		/// <param name="radTreeNode">current tree node</param>
		/// <param name="node">data tree node</param>
		public static void RenderNestedTree(RadTreeView tree, RadTreeNode radTreeNode, CustomCategoryTreeNode node)
		{
			RadTreeNode tmp = new RadTreeNode(node.Text, node.Id.ToString());
			tmp.Attributes.Add("cnt", node.Count.ToString());
			tmp.Expanded = true;
			if (radTreeNode == null)
			{
				tree.Nodes.Add(tmp);
			}
			else
			{
				radTreeNode.Nodes.Add(tmp);
			}

			foreach (CustomCategoryTreeNode child in node.Children)
			{
				if (child.Count == 0)
					continue;
				RenderNestedTree(tree, tmp, child);
			}
		}
		#endregion

	}
}
