﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace CR.DnnModules.Common
{
    /// <summary>
    /// 
    /// </summary>
    /// Author: Vu Dinh
    /// 3/24/2013 - 5:19 PM
    public static class CryptoServiceProvider
    {
        private static TripleDESCryptoServiceProvider _tripleDes;
        private static readonly byte[] TripleDesKey = new byte[] { 125, 235, 186, 152, 140, 99, 40, 51, 37, 223, 154, 46, 69, 98, 70, 188, 31, 135, 132, 225, 226, 1, 120, 55 };
        private static readonly byte[] TripleDesVector = new byte[] { 73, 164, 87, 186, 60, 97, 164, 79 };

        /// <summary>
        /// Encodes the specified query string.
        /// </summary>
        /// <param name="queryString">The query string.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 3/24/2013 - 5:20 PM
        public static string Encode(string queryString)
        {
            if (_tripleDes == null)
                _tripleDes = new TripleDESCryptoServiceProvider();
            var memoryStream = new MemoryStream();
            var cryptoStream = new CryptoStream(memoryStream, _tripleDes.CreateEncryptor(TripleDesKey, TripleDesVector), CryptoStreamMode.Write);
            byte[] input = Encoding.UTF8.GetBytes(queryString);
            cryptoStream.Write(input, 0, input.Length);
            cryptoStream.FlushFinalBlock();
            memoryStream.Position = 0;
            var result = new byte[memoryStream.Length];
            memoryStream.Read(result, 0, result.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Convert.ToBase64String(result);
        }

        /// <summary>
        /// Decodes the specified s.
        /// </summary>
        /// <param name="s">The s.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 3/24/2013 - 5:20 PM
        public static string Decode(string s)
        {
            if (_tripleDes == null)
                _tripleDes = new TripleDESCryptoServiceProvider();
            var memoryStream = new MemoryStream();
            var cryptoStream = new CryptoStream(memoryStream, _tripleDes.CreateDecryptor(TripleDesKey, TripleDesVector), CryptoStreamMode.Write);
            byte[] input = Convert.FromBase64String(s);
            cryptoStream.Write(input, 0, input.Length);
            cryptoStream.FlushFinalBlock();
            memoryStream.Position = 0;
            var result = new byte[memoryStream.Length];
            memoryStream.Read(result, 0, result.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(result);

        }
    }
}
