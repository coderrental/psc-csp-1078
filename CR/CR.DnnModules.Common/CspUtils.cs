﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using CR.ContentObjectLibrary;
using CR.ContentObjectLibrary.Data;
using CR.ContentObjectLibrary.Factory;
using CR.ContentObjectLibrary.Interface;

namespace CR.DnnModules.Common
{
    public class CspUtils
    {
        private readonly CspDataContext _cspDataContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="CspUtils"/> class.
        /// </summary>
        /// <param name="cspDataContext">The CSP data context.</param>
        /// Author: Vu Dinh
        /// 3/27/2013 - 3:58 PM
        public CspUtils(CspDataContext cspDataContext)
        {
            _cspDataContext = cspDataContext;
        }

        /// <summary>
        /// Gets the content field value.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 3/27/2013 - 4:00 PM
        public string GetContentFieldValue(content product, string fieldName)
        {
            var result = _cspDataContext.GetContentFieldValue(product.content_Id, fieldName).FirstOrDefault();
            return result == null ? string.Empty : string.IsNullOrEmpty(result.value_text) ? string.Empty : result.value_text;
        }

		/// <summary>
		/// Gets the content field value.
		/// </summary>
		/// <param name="contentId">The content id.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns></returns>
		/// Author: Vu Dinh
		/// 3/27/2013 - 4:00 PM
		public string GetContentFieldValue(Guid contentId, string fieldName)
		{
			var result = _cspDataContext.GetContentFieldValue(contentId, fieldName).FirstOrDefault();
			return result == null ? string.Empty : string.IsNullOrEmpty(result.value_text) ? string.Empty : result.value_text;
		}

        /// <summary>
        /// Updates the content field.
        /// </summary>
        /// <param name="contentTypeId">The content type id.</param>
        /// <param name="content">The content.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="fieldValue">The field value.</param>
        /// Author: Vu Dinh
        /// 3/31/2013 - 2:48 PM
        public void SetContentFieldValue(int contentTypeId, content content, string fieldName, string fieldValue)
        {
            fieldName = fieldName.Trim();
            var contentField = _cspDataContext.content_fields.SingleOrDefault(a => a.content_Id == content.content_Id && a.content_types_field.fieldname == fieldName);
            if (contentField == null) //create new content field
            {
                var contentTypesField = _cspDataContext.content_types_fields.SingleOrDefault(a => a.fieldname == fieldName && a.content_types_Id == contentTypeId);
                if (contentTypesField == null)
                {
                    contentTypesField = new content_types_field
                        {
                            content_types_Id = contentTypeId,
                            default_value = null,
                            fieldname = fieldName,
                            mandatory = false,
                            fieldtype = 1
                        };
                    _cspDataContext.content_types_fields.InsertOnSubmit(contentTypesField);
                    _cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                }
                contentField = new content_field
                {
                    content_Id = content.content_Id,
                    content_types_fields_Id = contentTypesField.content_types_fields_Id,
                    value_text = fieldValue
                };
                _cspDataContext.content_fields.InsertOnSubmit(contentField);
                _cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                return;
            }
            if (contentField.value_text != fieldValue) //update content field
            {
                contentField.value_text = fieldValue;
                _cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
            }
        }

        /// <summary>
        /// Gets the custom content field value.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <param name="companyId">The company id.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 5/7/2013 - 3:08 PM
        public string GetCustomContentFieldValue(content content, int companyId, string fieldName)
        {
            var contentTypeField = _cspDataContext.content_types_fields.FirstOrDefault(a => a.fieldname == fieldName && a.content_types_Id == content.content_main.content_types_Id);
            if (contentTypeField != null)
            {
                var contentField = _cspDataContext.content_fields.FirstOrDefault(a => a.content_types_fields_Id == contentTypeField.content_types_fields_Id && a.content_Id == content.content_Id);
                if (contentField != null)
                {
                    var customContentField = _cspDataContext.custom_content_fields.FirstOrDefault(a => a.content_Id == content.content_Id && a.consumer_Id == companyId && a.content_fields_Id == contentField.content_fields_Id);
                    if (customContentField != null)
                        return customContentField.value_text;
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Sets the custom content field value.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <param name="companyId">The company id.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="fieldValue">The field value.</param>
        /// Author: Vu Dinh
        /// 5/7/2013 - 3:14 PM
        public void SetCustomContentFieldValue(content content, int companyId, string fieldName, string fieldValue)
        {
            fieldName = fieldName.Trim();
            var contentTypeField = _cspDataContext.content_types_fields.SingleOrDefault(a => a.fieldname == fieldName && a.content_types_Id == content.content_main.content_types_Id);
            if (contentTypeField != null)
            {
                var contentField = _cspDataContext.content_fields.SingleOrDefault(a => a.content_types_fields_Id == contentTypeField.content_types_fields_Id && a.content_Id == content.content_Id);
                if (contentField != null)
                {
                    var customContentField = _cspDataContext.custom_content_fields.SingleOrDefault(a => a.content_Id == content.content_Id && a.consumer_Id == companyId && a.content_fields_Id == contentField.content_fields_Id);
                    if (customContentField != null) //update custom field value
                    {
                        customContentField.value_text = fieldValue;
                        _cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                    }
                    else //insert new custom field value record
                    {
                        _cspDataContext.custom_content_fields.InsertOnSubmit(new custom_content_field
                                                                                 {
                                                                                     content_Id = content.content_Id,
                                                                                     consumer_Id = companyId,
                                                                                     content_fields_Id = (int)contentField.content_fields_Id,
                                                                                     value_text = fieldValue
                                                                                 });
                        _cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the company parameter.
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <param name="parameter">The parameter name.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 3/31/2013 - 2:45 PM
        public string GetCompanyParameter(int companyId, string parameter)
        {
            var ctype = _cspDataContext.companies_parameter_types.SingleOrDefault(a => a.parametername == parameter);
            if (ctype != null)
            {
                var cValue = _cspDataContext.companies_parameters.SingleOrDefault(a => a.companies_Id == companyId && a.companies_parameter_types_Id == ctype.companies_parameter_types_Id);
                return cValue != null ? (string.IsNullOrEmpty(cValue.value_text)?string.Empty:cValue.value_text) : string.Empty;
            }
            return string.Empty;
        }

        /// <summary>
        /// Sets the company parameter.
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <param name="parameter">The parameter.</param>
        /// <param name="value">The value.</param>
        public void SetCompanyParameter(int companyId, string parameter, string value)
        {
            var ctype = _cspDataContext.companies_parameter_types.SingleOrDefault(a => a.parametername == parameter);
            if (ctype != null)
            {
                var cValue =_cspDataContext.companies_parameters.SingleOrDefault(a =>a.companies_Id == companyId &&a.companies_parameter_types_Id == ctype.companies_parameter_types_Id);
                if (cValue == null)
                    _cspDataContext.companies_parameters.InsertOnSubmit(new companies_parameter
                        {
                            companies_Id = companyId,
                            companies_parameter_types_Id = ctype.companies_parameter_types_Id,
                            value_text = value.Trim()
                        });
                else
                {
                    cValue.value_text = value.Trim();
                }
                _cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
            }
        }

        /// <summary>
        /// Gets the contents from category id.
        /// </summary>
        /// <param name="langId">The language id.</param>
        /// <param name="contentTypeId">The content type id.</param>
        /// <param name="suppId">The supplier id.</param>
        /// <param name="catId">The category id.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 3/31/2013 - 4:18 PM
        public List<content> GetContentsFromCategoryId(int langId, int contentTypeId, int suppId, int catId)
        {
            var query =
                @"
  select * from content c
  join content_main cm on cm.content_main_Id = c.content_main_Id
  join content_types ct on ct.content_types_Id = cm.content_types_Id
  join content_categories cc on cc.content_main_Id = cm.content_main_Id
  join categories cat on cat.categoryId = cc.category_Id
  where c.content_types_languages_Id = {0} and ct.content_types_Id = {1} and cat.categoryId = {2} and cm.supplier_Id = {3}";
            var result = _cspDataContext.ExecuteQuery<content>(query, langId, contentTypeId, catId, suppId);
            return result.ToList();
        }
        /// <summary>
        /// Gets the contents from category id. Set categoryid = -1 to get all contents
        /// </summary>
        /// <param name="langId">The language id.</param>
        /// <param name="contentTypeId">The content type id.</param>
        /// <param name="suppId">The supplier id.</param>
        /// <param name="catId">The category id (-1 to get all contents).</param>
        /// <param name="isFromAllChildCategories">if set to <c>true</c> [is from all child categories].</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 3/31/2013 - 6:42 PM
        public List<content> GetContentsFromCategoryId(int langId, int contentTypeId, int suppId, int catId, bool isFromAllChildCategories)
        {
            if (isFromAllChildCategories == false)
                return GetContentsFromCategoryId(langId, contentTypeId, suppId, catId);
            var searchFactory = (SimpleSearch)SearchExpressionFactory.CreateSearch();
            searchFactory.Left = SearchExpressionFactory.CreateSearch(_cspDataContext.content_types.FirstOrDefault(a => a.content_types_Id == contentTypeId));
            searchFactory.Operator = SearchOperator.And;
            searchFactory.Right = SearchExpressionFactory.CreateSearch();
            searchFactory.Right.Left = SearchExpressionFactory.CreateSearch(_cspDataContext.companies.FirstOrDefault(a => a.companies_Id == suppId));
            searchFactory.Right.Operator = SearchOperator.And;
            searchFactory.Right.Right = SearchExpressionFactory.CreateSearch(_cspDataContext.languages.FirstOrDefault(a => a.languages_Id == langId));
            IContentAccessFilter caf = (catId == -1) ? ContentAccessFilterFactory.CreateContentAccessFilter(searchFactory, _cspDataContext) : ContentAccessFilterFactory.CreateContentAccessFilter(searchFactory, _cspDataContext, catId);
            return caf.Search().ToList();
        }
    }
}
