﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.IO;
using System.Linq;
using CR.ContentObjectLibrary.Data;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Data;
using DotNetNuke.Entities.Users;

namespace CR.DnnModules.Common
{
	public static class Utils
	{
		/// <summary>
		/// Gets the common resource file.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="localResourceFile">The local resource file.</param>
		/// <returns></returns>
		public static string GetCommonResourceFile(string name, string localResourceFile)
		{
			return localResourceFile.Substring(0, localResourceFile.LastIndexOf("/", System.StringComparison.Ordinal) + 1) + name;
		}

		/// <summary>
		/// Gets the integration key.
		/// </summary>
		/// <param name="userInfo">The user info.</param>
		/// <returns></returns>
		public static int GetIntegrationKey(UserInfo userInfo)
		{
			if (userInfo.Profile.ProfileProperties[Keys.CspIntegrationKey] == null)
				return -1;
			int key = -1;
			return int.TryParse(userInfo.Profile.ProfileProperties[Keys.CspIntegrationKey].PropertyValue, out key) ? key : -1;
		}


		/// <summary>
		/// Get an app key value in web.config
		/// </summary>
		/// <param name="portalId"></param>
		/// <param name="key"></param>
		/// <returns></returns>
		public static string GetSetting(int portalId, string key)
		{
			return Config.GetSetting("Portal" + portalId + key);
		}

		/// <summary>
		/// Get csp connection string
		/// </summary>
		/// <param name="portalId"></param>
		/// <returns></returns>
		public static string GetConnectionString(int portalId)
		{
			string m_Cs = "";

			try
			{
				m_Cs = Config.GetConnectionString("CspPortal" + portalId);
			}
			catch
			{
				m_Cs = "";
			}
			return m_Cs;
		}

		/// <summary>
		/// Check an app setting key to see if the result is either true/false
		/// </summary>
		/// <param name="portalId">Portal id to check</param>
		/// <param name="key">Appsetting key to check</param>
		/// <returns></returns>
		public static bool Check(int portalId, string key)
		{
			string m_V = GetSetting(portalId, key);
			return !string.IsNullOrEmpty(m_V) && m_V.Equals("true", StringComparison.CurrentCultureIgnoreCase);
		}

		/// <summary>
		/// Resize an image
		/// </summary>
		/// <param name="path"></param>
		/// <param name="filename"></param>
		/// <param name="width"></param>
		/// <param name="maxHeight"></param>
		/// <returns></returns>
		public static string ResizeImage(string path, string filename, int width, int maxHeight)
		{
			string newFileName = filename;
			try
			{
				if (width <= 0 || maxHeight <= 0) return filename;

				System.Drawing.Image image = System.Drawing.Image.FromFile(path + "\\" + filename);
				if (image.Width <= width && image.Height <= maxHeight) return filename;

				image.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
				image.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

				int newHeight = image.Height*width/image.Width;
				int newWidth = 0;
				if (newHeight > maxHeight)
				{
					newWidth = image.Width*maxHeight/image.Height;
					newHeight = maxHeight;
				}
				if (newWidth == 0)
					newWidth = width;

				System.Drawing.Image NewImage = image.GetThumbnailImage(newWidth, newHeight, null, IntPtr.Zero);

				// Clear handle to original file so that we can overwrite it if necessary
				image.Dispose();

				// Save resized picture

				newFileName = Path.GetFileNameWithoutExtension(filename) + "r" + Path.GetExtension(filename);
				NewImage.Save(path + "\\" + newFileName);
			}
			catch (Exception ex)
			{
				//Debug("Unable to resize image. Error: " + ex.Message);
				//DnnLog.Error(ex.Message, ex);
			}

			return newFileName;
		}

		/// <summary>
		/// Gets the content field value.
		/// </summary>
		/// <param name="dataContext">The data context.</param>
		/// <param name="product">The product.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns></returns>
		/// Author: Vu Dinh
		/// 11/15/2012 - 2:17 PM
		public static string GetContentFieldValue(CspDataContext dataContext, content product, string fieldName)
		{
			var result = dataContext.GetContentFieldValue(product.content_Id, fieldName).FirstOrDefault();
			return result == null ? string.Empty : string.IsNullOrEmpty(result.value_text) ? string.Empty : result.value_text;
		}


        /// <summary>
        /// Updates the content field.
        /// </summary>
        /// <param name="cspDataContext">The CSP data context.</param>
        /// <param name="contentType">Type of the content.</param>
        /// <param name="content">The content.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="fieldValue">The field value.</param>
        /// Author: Vu Dinh
        /// 3/25/2013 - 4:15 PM
        public static void UpdateContentField(CspDataContext cspDataContext,content_type contentType,content content, string fieldName, string fieldValue )
        {
            var contentField = cspDataContext.content_fields.SingleOrDefault(a => a.content_Id == content.content_Id && a.content_types_field.fieldname == fieldName);
            if (contentField == null) //create new content field
            {
                var contentTypesField = cspDataContext.content_types_fields.Single(a => a.fieldname == fieldName && a.content_types_Id == contentType.content_types_Id);
                contentField = new content_field
                {
                    content_Id = content.content_Id,
                    content_types_fields_Id = contentTypesField.content_types_fields_Id,
                    value_text = fieldValue
                };
                cspDataContext.content_fields.InsertOnSubmit(contentField);
                cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                return;
            }
            if (contentField.value_text != fieldValue) //update content field
            {
                contentField.value_text = fieldValue;
                cspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
            }
        }

		/// <summary>
		/// Gets the content field value.
		/// </summary>
		/// <param name="dataContext">The data context.</param>
		/// <param name="product">The product.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns></returns>
		/// Author: Vu Dinh
		/// 11/15/2012 - 3:20 PM
		public static string GetContentFieldValue(CspDataContext dataContext, object product, string fieldName)
		{
			var content = (content) product;
			var result = dataContext.GetContentFieldValue(content.content_Id, fieldName).FirstOrDefault();
			return result == null ? string.Empty : string.IsNullOrEmpty(result.value_text) ? string.Empty : result.value_text;
		}

		/// <summary>
		/// Missings the mandatory subscription.
		/// </summary>
		/// <param name="dnnContext">The DNN context.</param>
		/// <param name="dataContext">The data context.</param>
		/// <param name="userInfo">The user info.</param>
		/// <param name="portalId">The portal id.</param>
		/// <returns></returns>
		/// Author: ducuytran
		/// 1/9/2013 - 8:08 AM
		public static Guid MissingMandatorySubscription(DnnDataContext dnnContext, CspDataContext dataContext,
		                                               UserInfo userInfo, int portalId)
		{
			int formNo = 1;
			Guid formId = Guid.Empty;
			int cspId = Utils.GetIntegrationKey(userInfo);
			string queryGetTabModuleId =
					@"select tm.TabModuleID from DesktopModules dm
	join ModuleDefinitions md on md.DesktopModuleID = dm.DesktopModuleID
	join Modules mo on mo.ModuleDefID = md.ModuleDefID
	join TabModules tm on tm.ModuleID = mo.ModuleID
	where dm.ModuleName='CspModules.TIEKinetix.CspModules.CSPSubscriptionModule' and PortalID = {0}";
			int theTabModuleId = 0;
			using (var dataReader = DataProvider.Instance().ExecuteSQL(String.Format(queryGetTabModuleId, portalId)))
			{
				while (dataReader.Read())
				{
					theTabModuleId = (int)dataReader[0];
				}
			}
			List<CSPSubscriptionModule_Form> subscriptionForms = new List<CSPSubscriptionModule_Form>();
			List<CSPSubscriptionModule_Workflow> workflowForms = new List<CSPSubscriptionModule_Workflow>();
			List<CSPSubscriptionModule_Field> DnnMandatoryFields = new List<CSPSubscriptionModule_Field>();

			workflowForms = dnnContext.CSPSubscriptionModule_Workflows.Where(a => a.PortalId == portalId && a.TabModuleId == theTabModuleId).ToList();
			if (workflowForms.Count == 0)
				return formId;

			//Get forms tree & mandatory fields from DNN DB
			foreach (CSPSubscriptionModule_Workflow cspSubscriptionModuleWorkflow in workflowForms)
			{
				if (cspSubscriptionModuleWorkflow.ParentFormId == null || cspSubscriptionModuleWorkflow.ParentFormId == Guid.Empty)
				{
					subscriptionForms.Add(
						dnnContext.CSPSubscriptionModule_Forms.FirstOrDefault(a => a.FormId == cspSubscriptionModuleWorkflow.FormId));
					Utils.GetFormTree(dnnContext, (Guid)cspSubscriptionModuleWorkflow.FormId, subscriptionForms, portalId, theTabModuleId);
				}
				List<CSPSubscriptionModule_Field> tmpFields =
					dnnContext.CSPSubscriptionModule_Fields.Where(a => a.FormId == cspSubscriptionModuleWorkflow.FormId && a.Mandatory == true).ToList();
				if (tmpFields.Count > 0)
					DnnMandatoryFields.AddRange(tmpFields);
			}

			if (DnnMandatoryFields.Count == 0)
				return formId;

			//Get missing mandatory fields
			List<CSPSubscriptionModule_Field> missingMandatoryFields = new List<CSPSubscriptionModule_Field>();
			companies_parameter companiesParameter = new companies_parameter();
			companies_parameter_type copTmp = new companies_parameter_type();
			
			foreach (CSPSubscriptionModule_Field cspSubscriptionModuleField in DnnMandatoryFields)
			{
				copTmp =
					dataContext.companies_parameter_types.FirstOrDefault(a => a.parametername == cspSubscriptionModuleField.Name);
				if (copTmp != null)
				{
					companiesParameter =
						dataContext.companies_parameters.FirstOrDefault(
							a => a.companies_parameter_types_Id == copTmp.companies_parameter_types_Id && a.companies_Id == cspId);
					if (companiesParameter == null || String.IsNullOrEmpty(companiesParameter.value_text))
						missingMandatoryFields.Add(cspSubscriptionModuleField);
				}
			}

			//All mandatory fields had been filled
			if (missingMandatoryFields.Count == 0)
				return formId;
			
			int i = 0;
			foreach (CSPSubscriptionModule_Form subscriptionForm in subscriptionForms)
			{
				i++;
				if (missingMandatoryFields.Where(a => a.FormId == subscriptionForm.FormId).ToList().Count > 0)
				{
					formNo = formNo + i;
					formId = subscriptionForm.FormId;
					break;
				}
			}
			return formId;
		}

		/// <summary>
		/// Gets the forms tree.
		/// </summary>
		/// <param name="dnnDataContext">The DNN data context.</param>
		/// <param name="parentId">The parent id.</param>
		/// <param name="formList">The form list.</param>
		/// Author: ducuytran
		/// 1/9/2013 - 1:49 PM
		public static void GetFormTree(DnnDataContext dnnDataContext, Guid parentId, List<CSPSubscriptionModule_Form> formList, int portalId, int tabModuleId)
		{
			CSPSubscriptionModule_Workflow tmpFormItem =
				dnnDataContext.CSPSubscriptionModule_Workflows.FirstOrDefault(a => a.ParentFormId == parentId && a.PortalId == portalId && a.TabModuleId == tabModuleId);	
			if (parentId == Guid.Empty)
				tmpFormItem = dnnDataContext.CSPSubscriptionModule_Workflows.FirstOrDefault(a => a.ParentFormId == null && a.PortalId == portalId && a.TabModuleId == tabModuleId);
			if (tmpFormItem != null)
			{
				formList.Add(dnnDataContext.CSPSubscriptionModule_Forms.FirstOrDefault(a => a.FormId == tmpFormItem.FormId));
				GetFormTree(dnnDataContext, (Guid)tmpFormItem.FormId, formList, portalId, tabModuleId);
			}
		}

		/// <summary>
		/// Forms the step no.
		/// </summary>
		/// <param name="dnnContext">The DNN context.</param>
		/// <param name="dataContext">The data context.</param>
		/// <param name="formId">The form id.</param>
		/// <param name="portalId">The portal id.</param>
		/// <returns></returns>
		/// Author: ducuytran
		/// 1/9/2013 - 6:49 PM
		public static int FormStepNo(DnnDataContext dnnContext, CspDataContext dataContext,
													   Guid formId, int portalId)
		{
			int formNo = 1;
			List<CSPSubscriptionModule_Form> subscriptionForms = new List<CSPSubscriptionModule_Form>();
			List<CSPSubscriptionModule_Workflow> workflowForms = new List<CSPSubscriptionModule_Workflow>();

			workflowForms = dnnContext.CSPSubscriptionModule_Workflows.Where(a => a.PortalId == portalId).ToList();
			if (workflowForms.Count == 0)
				return formNo;

			//Get TabModuleId
			string queryGetTabModuleId =
					@"select tm.TabModuleID from DesktopModules dm
	join ModuleDefinitions md on md.DesktopModuleID = dm.DesktopModuleID
	join Modules mo on mo.ModuleDefID = md.ModuleDefID
	join TabModules tm on tm.ModuleID = mo.ModuleID
	where dm.ModuleName='CspModules.TIEKinetix.CspModules.CSPSubscriptionModule' and PortalID = {0}";
			int theTabModuleId = 0;
			using (var dataReader = DataProvider.Instance().ExecuteSQL(String.Format(queryGetTabModuleId, portalId)))
			{
				while (dataReader.Read())
				{
					theTabModuleId = (int)dataReader[0];
				}
			}
			//Get forms tree & mandatory fields from DNN DB
			foreach (CSPSubscriptionModule_Workflow cspSubscriptionModuleWorkflow in workflowForms)
			{
				if (cspSubscriptionModuleWorkflow.ParentFormId == null || cspSubscriptionModuleWorkflow.ParentFormId == Guid.Empty)
				{
					subscriptionForms.Add(
						dnnContext.CSPSubscriptionModule_Forms.FirstOrDefault(a => a.FormId == cspSubscriptionModuleWorkflow.FormId));
					Utils.GetFormTree(dnnContext, (Guid)cspSubscriptionModuleWorkflow.FormId, subscriptionForms, portalId, theTabModuleId);
					break;
				}
			}

			for (int i = 0; i < subscriptionForms.Count; i++)
			{
				if (subscriptionForms[i].FormId == formId)
				{
					formNo += i + 1;
					break;
				}
			}

			return formNo;
		}
	}
}