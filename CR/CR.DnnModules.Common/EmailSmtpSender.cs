﻿// ***********************************************************************
// Assembly         : CR.DnnModules.Common
// Author           : Vu Dinh
// Created          : 07-24-2014
//
// Last Modified By : Vu Dinh
// Last Modified On : 07-24-2014
// ***********************************************************************
// <copyright file="EmailSmtpSender.cs" company="Coder Rental Team">
//     Copyright (c) Coder Rental Team. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace CR.DnnModules.Common
{
    public class EmailSmtpSender
    {
        private readonly string _host;
        private readonly int _port;
        private readonly bool _enableSsl;
        private readonly string _username;
        private readonly string _password;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailSmtpSender" /> class.
        /// </summary>
        /// <param name="host">The SMTP host.</param>
        /// <param name="port">The SMTP port.</param>
        /// <param name="enableSsl">if set to <c>true</c> [enable SSL].</param>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        public EmailSmtpSender(string host, int port, bool enableSsl,string username, string password)
        {
            _host = host;
            _port = port;
            _enableSsl = enableSsl;
            _username = username;
            _password = password;
        }

        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="recipients">The recipients.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <author>Vu Dinh</author>
        /// <modified>07/24/2014 08:50:08</modified>
        public void SendEmail(string from, string recipients, string subject, string body)
        {
            try
            {
                // ltu 7/30 updated the code to handle cases without port or credential
                using (var client = new SmtpClient(_host))
                {
                    if (_port != -1) client.Port = _port;
                    if (!string.IsNullOrEmpty(_username))
                    {
                        client.Credentials = new NetworkCredential(_username, _password);
                        client.EnableSsl = _enableSsl;
                    }
                    using (var message = new MailMessage(from, recipients, subject, body))
                    {
                        message.Priority = MailPriority.High;
                        message.IsBodyHtml = true;
                        client.Send(message);
                    }
                }
            }
            catch
            {
            }
        }
    }
}
