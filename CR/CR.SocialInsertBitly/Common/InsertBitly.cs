﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using CR.ContentObjectLibrary.Data;
using CR.SocialInsertBitly.Data;
using CR.DnnModules.Common;
using CR.SocialLib.Bitly;
using log4net;
using DnnDataContext = CR.SocialInsertBitly.Data.DnnDataContext;

namespace CR.SocialInsertBitly.Common
{
    public class InsertBitly
    {
        private string _dnnConnectionString,
                      _cspConnectionString,
                      _bitlyAccessToken;

        private int _portalId;
        private DnnDataContext _dnnDataContext;
        private CspDbDatacontext _cspDbDataContext;
        private CspDataContext _cspDataContext;
        private CspUtils _cspUtils;
        public static readonly ILog log = LogManager.GetLogger("SocialInsertBitly");

        /// <summary>
        /// Inits the resources.
        /// </summary>
        // Author  : Long Kim Vu
        // Created : 06-27-2014
        private void InitResources()
        {
            try
            {
                _cspConnectionString = ConfigurationManager.ConnectionStrings["CspConnectionString"].ConnectionString;
                _dnnConnectionString = ConfigurationManager.ConnectionStrings["DnnConnectionString"].ConnectionString;
                _bitlyAccessToken = ConfigurationManager.AppSettings["BitLyAccessToken"];
                _portalId = Int32.Parse(ConfigurationManager.AppSettings["PortalId"]);
                _cspDbDataContext = new CspDbDatacontext(_cspConnectionString);
                _cspDataContext = new CspDataContext(_cspConnectionString);
                _dnnDataContext = new DnnDataContext(_dnnConnectionString);
                _cspUtils = new CspUtils(_cspDataContext);
            }
            catch (Exception e)
            {
                
                LogError(e);
            }
            
            Log("Init resources completed");
        }

        public string GetOriginalLink(string bitlyUrl, string bitlyAccessToken)
        {
            try
            {
                var bitlyHelper = new BitlyHelper(bitlyAccessToken);
                return bitlyHelper.GetLongUrl(bitlyUrl).LongUrl;
            }
            catch (Exception exception)
            {
                LogError(exception);
                return string.Empty;
            }
            
        }

        public void GetListAndBitlyLink()
        {
            InitResources();
            int count = 100;
            try
            {
                var listMessage = _dnnDataContext.CSPTSMM_MessageSentStatistics.Where(a => a.SentSuccess && a.PortalId == _portalId);
                foreach (var message in listMessage)
                {
                    try
                    {
                        var content = _cspDataContext.contents.FirstOrDefault(a => a.content_Id == message.MessageId);
                        if (content != null)
                        {
                            var messageContent = _cspUtils.GetCustomContentFieldValue(content, message.CspId, "Message");
                            if (!string.IsNullOrEmpty(messageContent))
                            {
                                var listUrl = ExtractUrl(messageContent);
                                if (listUrl.Any())
                                {
                                    foreach (var url in listUrl)
                                    {
                                        try
                                        {
                                            if (url.Contains("bit.ly"))
                                            {
                                                var bitlyLinkPerMessage =
                                                    _cspDbDataContext.SocialModule_BitlyLinksPerMessages.FirstOrDefault(
                                                        a =>
                                                        a.MessageId == message.MessageId && a.BitlyLink == url &&
                                                        a.CspId == message.CspId);
                                                if (bitlyLinkPerMessage == null)
                                                {
                                                    if (count <= 0)
                                                        break;

                                                    var longUrl = GetOriginalLink(url, _bitlyAccessToken);
                                                    count--;
                                                    if (longUrl != string.Empty)
                                                    {
                                                        if (message.SocialId != null)
                                                        {
                                                            var newbitlyLinkPerMessage = new SocialModule_BitlyLinksPerMessage
                                                            {
                                                                BitlyLink = url,
                                                                OriginalUrl = longUrl,
                                                                CspId = message.CspId,
                                                                MessageId = message.MessageId,
                                                                SocialId = (int)message.SocialId,
                                                                PortalId = _portalId,
                                                            };
                                                            _cspDbDataContext.SocialModule_BitlyLinksPerMessages.InsertOnSubmit(newbitlyLinkPerMessage);
                                                            _cspDbDataContext.SubmitChanges();
                                                            Log("("+count+") Insert Bitly Link: " + url + " to MessageId: " + message.MessageId.ToString() + " and CspId: " + message.CspId.ToString() + " Completed!");
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            
                                            LogError(e);
                                        }
                                        
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        
                        LogError(e);
                    }
                    
                }
                Log("Insert all bitly link done!");
            }
            catch (Exception e)
            {
                LogError(e);
            }
            
        }

        /// <summary>
        /// Logs the specified text.
        /// </summary>
        /// <param name="text">The text.</param>
        // Author  : Long Kim Vu
        // Created : 06-27-2014
        public static void Log(string text)
        {
            Console.WriteLine(text);
            log.Info(text);
        }

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <author>Vu Dinh</author>
        /// <modified>07/02/2014 13:55:02</modified>
        public static void LogError(Exception exception)
        {
            Console.WriteLine(exception.Message);
            log.Error(exception);
        }

        /// <summary>
        /// Extracts the URL.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns>System.Collections.Generic.List{System.String}.</returns>
        /// // Author  : Long Kim Vu
        /// // Created : 01-01-0001
        private List<string> ExtractUrl(string message)
        {
            var urlRx = new Regex(@"(http|ftp|https)://([\w+?\.\w+])+([a-zA-Z0-9\~\|\!\@\#\[\]{\}\$\%\^\&\*\(\)_\-\=\+\\\/\?\.\:\;\'\,]*)?", RegexOptions.IgnoreCase);
            MatchCollection matches = urlRx.Matches(message);
            return (from Match match in matches select match.Value).ToList();
        }
    }
}
