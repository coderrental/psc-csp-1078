﻿using System;
using CR.SocialInsertBitly.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CR.SocialInsertBilyTests
{
    [TestClass]
    public class InsertBitlyTests
    {
        [TestMethod]
        public void GetOriginalLink()
        {
            var program = new InsertBitly();
            var longUrl = program.GetOriginalLink("http://bit.ly/1hWWGeg", "db75a63495a200b228788f89ba273a283414fa1e");
            Assert.AreEqual("http://vnexpress.net/?messageid=f77e6574-33c7-4686-b59a-eaaa38c767f9&cspid=1462&socialid=1",longUrl,null);

            var falseurl = program.GetOriginalLink("http://gamevn.com", "db75a63495a200b228788f89ba273a283414fa1e");
            Assert.AreEqual(string.Empty,falseurl,null);
        }
    }
}
