﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using CR.DnnModules.CreatePortal.Data;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Definitions;
using DotNetNuke.Entities.Portals;
using DotNetNuke.Common;
using DotNetNuke.Entities.Profile;
using DotNetNuke.Entities.Tabs;
using DotNetNuke.Entities.Users;
using DotNetNuke.Security.Permissions;
using CR.DnnModules.CreatePortal.Common;
using DotNetNuke.Security.Roles;
using Ionic.Zip;

namespace CR.DnnModules.CreatePortal
{
    /// <summary>
    /// Class MainView
    /// </summary>
    public partial class MainView : DnnModuleBase
    {
        private string _portalName, _siteAlias, _skinName,_containerName,_projectName,_adminUserName;
        private int _portalId,_cspAdminRoleId,_cspSuperAdminRoleId;
        private TabInfo _tab;
        private TabController _tabController;
        private PortalSettings _portalSettings;
        private UserInfo _adminUser;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        // Author  : Long Kim Vu
        // Created : 04-22-2014
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// Creates the portal.
        /// </summary>
        /// <param name="portalName">Name of the portal.</param>
        /// <param name="siteAlias">The site alias.</param>
        // Author  : Long Kim Vu
        // Created : 04-22-2014
        private void CreatePortal(string portalName, string siteAlias)
        {
            try
            {
                var portalController = new PortalController();
               // UserInfo adminUser = UserInfo;
               // adminUser.Membership.Password = UserController.GetPassword(ref adminUser, String.Empty);
                _adminUser = new UserInfo
                    {
                        FirstName = _adminUserName,
                        LastName = _adminUserName,
                        Username = _adminUserName,
                        DisplayName = _adminUserName,
                        Email = Cons.DEFAULT_ADMIN_EMAIL,
                        IsSuperUser = false,
                        Membership = {Approved = true}
                    };


                _adminUser.Membership.Approved = true;
                _adminUser.Membership.Password = Cons.DEFAULT_ADMIN_PASSWORD;
                _adminUser.Membership.PasswordQuestion = "";
                _adminUser.Membership.PasswordAnswer = "";
                _adminUser.Profile.FirstName = _adminUserName;
                _adminUser.Profile.LastName = _adminUserName;

                var folderName = siteAlias.Substring(siteAlias.LastIndexOf('/') + 1);
                var portaltemplate = portalController.GetAvailablePortalTemplates().First(a => a.Name == "Blank Website");
               _portalId =  portalController.CreatePortal(portalName, _adminUser, "", "", portaltemplate, "", siteAlias,
                                              Globals.ApplicationMapPath,
                                              Globals.ApplicationMapPath + "\\" + folderName, true);  
            
            }
            catch (Exception e)
            {

                throw e;
            }
            
        }

        /// <summary>
        /// Deletes the default tab.
        /// </summary>
        // Author  : Long Kim Vu
        // Created : 04-22-2014
        private void DeleteDefaultTab()
        {
             _tab = new TabInfo();
             _tabController = new TabController();

            //Delete default tab
             TabInfo oldTab = _tabController.GetTabByName("Home", _portalId);
            if (oldTab != null)
            {
                if (oldTab.Modules != null)
                {
                    foreach (ModuleInfo mod in oldTab.Modules)
                    {
                        var moduleC = new ModuleController();
                        moduleC.DeleteModule(mod.ModuleID);
                        moduleC.DeleteModuleSettings(mod.ModuleID);
                    }
                }
                _tabController.DeleteTab(oldTab.TabID, _portalId);
                _tabController.DeleteTabSettings(oldTab.TabID);
                DataCache.ClearModuleCache(oldTab.TabID);
            }
        }

        /// <summary>
        /// Creates the page.
        /// </summary>
        /// <param name="pagename">The pagename.</param>
        /// <param name="roleId">The role id.</param>
        /// <param name="parentId">The parent id.</param>
        /// <param name="visible">if set to <c>true</c> [visible].</param>
        /// <param name="moduleName">Name of the module.</param>
        /// <returns>System.Int32.</returns>
        // Author  : Long Kim Vu
        // Created : 04-22-2014
        private int CreatePage(string pagename,int roleId,int? parentId,bool visible,string moduleName,bool? cspAdminTab)
        {

            _tab = new TabInfo();
            _tabController = new TabController();
            _tab.PortalID = _portalId;
            _tab.TabName = pagename;
            _tab.Title = pagename;
            _tab.Description = "";
            _tab.KeyWords = "";
            //works for include in menu option. if true, will be shown in menus, else will not be shown, we have to redirect internally
            _tab.IsVisible = visible;
            _tab.DisableLink = false;
            //if this tab has any parents provide parent tab id, so that it will be shown in parent tab menus sub menu list, else is NULL         //and will be in main menu list
            _tab.IsDeleted = false;
            _tab.Url = "";
            _tab.IsSuperTab = false;
            _tab.IconFile = "";
            if (parentId != null)
            {
                _tab.ParentId = (int) parentId;
            }
            int tabId = _tabController.AddTab(_tab, true);//true to load defalut modules
#region add admin view and edit page
            //LongVk add admin view and edit page
            var objTpcAdminView = new PermissionProvider();
            var tpiAdminView = new TabPermissionInfo();
            tpiAdminView.TabID = tabId;
            tpiAdminView.PermissionID = 3;//for view
            tpiAdminView.PermissionKey = "VIEW";
            tpiAdminView.PermissionName = "View Tab";
            tpiAdminView.PermissionCode = "SYSTEM_TAB";
            tpiAdminView.AllowAccess = true;
            tpiAdminView.RoleID = _portalSettings.AdministratorRoleId;
            _tab.TabPermissions.Add(tpiAdminView, true);
            objTpcAdminView.SaveTabPermissions(_tab);

            var objTpcAdminEdit = new PermissionProvider();
            var tpiAdminEdit = new TabPermissionInfo();
            tpiAdminEdit.TabID = tabId;
            tpiAdminEdit.PermissionID = 4;//for view
            tpiAdminEdit.PermissionKey = "EDIT";
            tpiAdminEdit.PermissionName = "Edit Tab";
            tpiAdminEdit.PermissionCode = "SYSTEM_TAB";
            tpiAdminEdit.AllowAccess = true;
            tpiAdminEdit.RoleID = _portalSettings.AdministratorRoleId; ;
            _tab.TabPermissions.Add(tpiAdminEdit, true);
            objTpcAdminEdit.SaveTabPermissions(_tab);


            var objTpc = new PermissionProvider();
            var tpi = new TabPermissionInfo();
            tpi.TabID = tabId;
            tpi.PermissionID = 3;//for view
            tpi.PermissionKey = "VIEW";
            tpi.PermissionName = "View Tab";
            tpi.PermissionCode = "SYSTEM_TAB";
            tpi.AllowAccess = true;
            tpi.RoleID = roleId;
            _tab.TabPermissions.Add(tpi, true);
            
            objTpc.SaveTabPermissions(_tab);
#endregion
            if (cspAdminTab == true)
            {
                var objTpcCspAdmin = new PermissionProvider();
                var tpiCspAdmin = new TabPermissionInfo();
                tpiCspAdmin.TabID = tabId;
                tpiCspAdmin.PermissionID = 3;//for view
                tpiCspAdmin.PermissionKey = "VIEW";
                tpiCspAdmin.PermissionName = "View Tab";
                tpiCspAdmin.PermissionCode = "SYSTEM_TAB";
                tpiCspAdmin.AllowAccess = true;
                tpiCspAdmin.RoleID = _cspAdminRoleId;
                _tab.TabPermissions.Add(tpiCspAdmin, true);
                objTpcCspAdmin.SaveTabPermissions(_tab);

                var objTpcCspSuperAdmin = new PermissionProvider();
                var tpiCspSuperAdmin = new TabPermissionInfo();
                tpiCspSuperAdmin.TabID = tabId;
                tpiCspSuperAdmin.PermissionID = 3;//for view
                tpiCspSuperAdmin.PermissionKey = "VIEW";
                tpiCspSuperAdmin.PermissionName = "View Tab";
                tpiCspSuperAdmin.PermissionCode = "SYSTEM_TAB";
                tpiCspSuperAdmin.AllowAccess = true;
                tpiCspSuperAdmin.RoleID = _cspSuperAdminRoleId;
                _tab.TabPermissions.Add(tpiCspSuperAdmin, true);
                objTpcCspSuperAdmin.SaveTabPermissions(_tab);
            } 
            if (!string.IsNullOrEmpty(moduleName))
            {
                AddModule(moduleName,tabId,"ContentPane");    
            }
            //Clear Cache
            DataCache.ClearModuleCache(tabId);
            DataCache.ClearTabsCache(_portalId);
            DataCache.ClearPortalCache(_portalId, false);
            DataCache.ClearCache();
            DataCache.ClearFolderCache(_portalId);
            return tabId;
        }

        /// <summary>
        /// Adds the update connection string.
        /// </summary>
        /// <param name="name">The name.</param>
        // Author  : Long Kim Vu
        // Created : 04-22-2014
        private void AddUpdateConnectionString(string name)
        {
            bool isNew = false;
            string path = Server.MapPath("~/Web.Config");
            var doc = new XmlDocument();
            doc.Load(path);
            if (doc.DocumentElement != null)
            {
                XmlNodeList list = doc.DocumentElement.SelectNodes(string.Format("connectionStrings/add[@name='{0}']", name));
                XmlNode node = null;
                if (list != null)
                {
                    isNew = list.Count == 0;
                    if (isNew)
                    {
                        node = doc.CreateNode(XmlNodeType.Element, "add", null);
                        XmlAttribute attribute = doc.CreateAttribute("name");
                        attribute.Value = name;
                        if (node.Attributes != null) 
                        {
                            node.Attributes.Append(attribute);
                            attribute = doc.CreateAttribute("connectionString");
                            attribute.Value = "";
                            node.Attributes.Append(attribute);

                            attribute = doc.CreateAttribute("providerName");
                            attribute.Value = "System.Data.SqlClient";
                            node.Attributes.Append(attribute);
                        }

                        
                    }
                    else
                    {
                        node = list[0];
                    }
                }
                if (node != null && node.Attributes != null)
                {
                    if (!string.IsNullOrEmpty(CspConnectionString))
                    {
                        CspConnectionString = CspConnectionString.Replace("{DbName}", tbxDbName.Text);
                        node.Attributes["connectionString"].Value = CspConnectionString;
                    }
                   
                }
                if (isNew)
                {
                    if (doc.DocumentElement != null)
                    {
                        var xmlNodeList = doc.DocumentElement.SelectNodes("connectionStrings");
                        if (xmlNodeList != null)
                            xmlNodeList[0].AppendChild(node);
                    }
                }
            }
            doc.Save(path);
        }

        /// <summary>
        /// Edits the profile properties.
        /// </summary>
        // Author  : Long Kim Vu
        // Created : 04-22-2014
        private void EditProfileProperties()
        {
            var listProfile = ProfileController.GetPropertyDefinitionsByPortal(_portalId);
            foreach (ProfilePropertyDefinition profile in listProfile)
            {
                profile.Visible = false;
                ProfileController.UpdatePropertyDefinition(profile);
            }
            AddProfileDefinition("CspId", "CspId", 350, "-1", 50, false, false, false, _portalId);
            AddProfileDefinition("TermAndConditions", "Term And Conditions", 364, string.Empty, 5000, false, true, false, _portalId);
            AddProfileDefinition("AcceptAgreement", "Accept Agreement", 362, string.Empty, 0, true, true, false, _portalId);
            
        }

        /// <summary>
        /// Adds the profile definition.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="propertyCategory">The property category.</param>
        /// <param name="dataType">Type of the data.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <param name="length">The length.</param>
        /// <param name="required">if set to <c>true</c> [required].</param>
        /// <param name="visible">if set to <c>true</c> [visible].</param>
        /// <param name="readOnly">if set to <c>true</c> [read only].</param>
        /// <param name="portalId">The portal id.</param>
        // Author  : Long Kim Vu
        // Created : 04-22-2014
        private void AddProfileDefinition(string propertyName, string propertyCategory, int dataType,
                                          string defaultValue, int length,
                                          bool required
                                          , bool visible, bool readOnly, int portalId)
        {
            var profileDefinition = new ProfilePropertyDefinition
            {
                PropertyName = propertyName,
                DataType = dataType,
                ModuleDefId = -1,
                Deleted = false,
                DefaultValue = defaultValue,
                PropertyCategory = propertyCategory,
                Length = length,
                Required = required,
                ViewOrder = 0,
                Visible = visible,
                ReadOnly = readOnly,
                PortalId = portalId,
                DefaultVisibility = 0
            };
            ProfileController.AddPropertyDefinition(profileDefinition);

        }

        /// <summary>
        /// Sets the skin login page.
        /// </summary>
        /// <param name="tabLoginPageId">The tab login page id.</param>
        // Author  : Long Kim Vu
        // Created : 04-22-2014
        private void SetSkinLoginPageAndProfile(int tabLoginPageId)
        {
            //_tab = new TabInfo();
            _tabController = new TabController();
            //Delete default tab
            TabInfo loginTab = _tabController.GetTab(tabLoginPageId, _portalId, true);
            loginTab.SkinSrc = string.Format("[L]Skins/{0}/Login.ascx", _skinName);
            _tabController.UpdateTab(loginTab);
            AddModule(Cons.MODULE_LOGIN_NAME,tabLoginPageId,"leftPaneContent");
            AddModule(Cons.MODULE_CSP_HTML_NAME, tabLoginPageId, "rightPaneContent");

            DataCache.ClearTabsCache(_portalId);
            DataCache.ClearPortalCache(_portalId,true);

        }

        /// <summary>
        /// Sets the default page.
        /// </summary>
        /// <param name="tabCompanySettings">The tab company settings.</param>
        /// <param name="tabLogin">The tab login.</param>
        /// <param name="tabRegister">The tab register.</param>
        /// <param name="tabProfile"></param>
        // Author  : Long Kim Vu
        // Created : 04-22-2014
        private void SetDefaultPage(int tabCompanySettings, int tabLogin,int tabRegister,int tabProfile)
        {
            var portalController = new PortalController();
            var portalInfo = portalController.GetPortal(_portalId);
            portalInfo.HomeTabId = tabCompanySettings;
            portalInfo.LoginTabId = tabLogin;
            portalInfo.RegisterTabId = tabRegister;
            portalInfo.UserTabId = tabProfile;
            portalController.UpdatePortalInfo(portalInfo);
            DataCache.ClearPortalCache(_portalId,false);
            DataCache.ClearTabsCache(_portalId);
        }

        /// <summary>
        /// Updates the skin.
        /// </summary>
        // Author  : Long Kim Vu
        // Created : 04-22-2014
        private void UpdateSkin()
        {
            PortalController.UpdatePortalSetting(_portalId, "DefaultPortalSkin",
                                               string.Format("[L]Skins/{0}/Skin.ascx", _skinName));
            PortalController.UpdatePortalSetting(_portalId, "DefaultPortalContainer",
                                               string.Format("[L]Containers/{0}/Container.ascx", _containerName));
            PortalController.UpdatePortalSetting(_portalId, "DefaultAdminSkin",
                                               string.Format("[L]Skins/{0}/Skin.ascx", _skinName));
            PortalController.UpdatePortalSetting(_portalId, "DefaultAdminContainer",
                                               string.Format("[L]Containers/{0}/Container.ascx", _containerName));
            PortalController.UpdatePortalSetting(_portalId, "Security_RequireValidProfile",
                                               "true", true);
        }

        /// <summary>
        /// Assigns the skin to all tab.
        /// </summary>
        // Author  : Long Kim Vu
        // Created : 04-22-2014
        private void AssignSkinToAllTab()
        {
            _tabController = new TabController();
            var listTab = _tabController.GetTabsByPortal(_portalId);
            foreach (var tab in listTab)
            {
                TabInfo oldTab = _tabController.GetTab(tab.Value.TabID, _portalId, true);
                tab.Value.SkinSrc = string.Format("[L]Skins/{0}/skin.ascx", _skinName);
                _tabController.UpdateTab(oldTab);
            }
        }

        /// <summary>
        /// Adds the module.
        /// </summary>
        /// <param name="moduleName">Name of the module.</param>
        /// <param name="tabId">The tab id.</param>
        /// <param name="paneName">Name of the pane.</param>
        // Author  : Long Kim Vu
        // Created : 04-22-2014
        private void AddModule(string moduleName, int tabId, string paneName)
        {
            var moduleController = new ModuleController();
            //var modulepermissionController = new ModulePermissionController();
            //moduleController.GetModuleByDefinition(PortalId, moduleName);

            var desktopModuleInfo = DesktopModuleController.GetDesktopModules(_portalId).FirstOrDefault(a => a.Value.ModuleName == moduleName);
            if (moduleName == Cons.MODULE_CSP_HTML_NAME && desktopModuleInfo.Value == null)
            {
                moduleName = "TIEKinetix.CspModules.CSPHtmlModule";
                desktopModuleInfo = DesktopModuleController.GetDesktopModules(_portalId).FirstOrDefault(a => a.Value.ModuleName == moduleName);
            }
            if (desktopModuleInfo.Value != null)
            {
                KeyValuePair<string, ModuleDefinitionInfo> moduleDefinitionInfo;
                if (moduleName == Cons.MODULE_REGISTER_NAME)
                {
                    moduleDefinitionInfo =
                        ModuleDefinitionController.GetModuleDefinitionsByDesktopModuleID(
                            desktopModuleInfo.Value.DesktopModuleID).First(a => a.Value.FriendlyName == "User Account");
                }
                else
                {
                    moduleDefinitionInfo =
                        ModuleDefinitionController.GetModuleDefinitionsByDesktopModuleID(
                            desktopModuleInfo.Value.DesktopModuleID).First();
                }
                var moduleInfo = new ModuleInfo
                    {
                        PortalID = _portalId,
                        TabID = tabId,
                        ModuleOrder = 1,
                        ModuleTitle = moduleName,
                        PaneName = paneName,
                        ModuleDefID = moduleDefinitionInfo.Value.ModuleDefID,
                        CacheTime = moduleDefinitionInfo.Value.DefaultCacheTime,
                        InheritViewPermissions = true,
                        AllTabs = false,
                        CultureCode = string.Empty,
                        DisplayPrint = false,
                        CacheMethod = "FileModuleCachingProvider",
                        WebSliceTitle = string.Empty,
                    };
                moduleController.AddModule(moduleInfo);

                if (moduleName == Cons.MODULE_CSP_HTML_NAME || moduleName == "TIEKinetix.CspModules.CSPHtmlModule")
                {
                    if (!string.IsNullOrEmpty(HtmlLoginContent))
                    {
                        var currentLanguage = Thread.CurrentThread.CurrentCulture.Name;
                        var content =
                            DnnDataContext.CSPHtmlModule_Contents.SingleOrDefault(
                                a =>
                                a.PortalId == _portalId && a.TabModuleId == moduleInfo.TabModuleID &&
                                a.CultureCode == currentLanguage);
                        if (content == null)
                        {
                            DnnDataContext.CSPHtmlModule_Contents.InsertOnSubmit(new CSPHtmlModule_Content
                                {
                                    Id = Guid.NewGuid(),
                                    Content = HtmlLoginContent,
                                    CultureCode = currentLanguage,
                                    CreatedByUserId = UserId,
                                    PortalId = _portalId,
                                    TabModuleId = moduleInfo.TabModuleID,
                                    Created = DateTime.Now,
                                    Changed = DateTime.Now
                                });
                            DnnDataContext.SubmitChanges();
                        }
                    }
                }
                if (moduleName == Cons.MODULE_CONTENT_SETUP_NAME)
                {
                    var objModules = new ModuleController();
                    objModules.UpdateTabModuleSetting(moduleInfo.TabModuleID, Cons.SETTING_INSTANCE_NAME, tbxProjectName.Text);
                    objModules.UpdateTabModuleSetting(moduleInfo.TabModuleID, Cons.SETTING_REDIRECT_TAB_NAME_COMPANYSETTINGS, Cons.PAGE_COMPANY_SETTING_NAME);
                    objModules.UpdateTabModuleSetting(moduleInfo.TabModuleID, Cons.SETTING_CATEGORY_CONTENT_TYPE_ID, Cons.DEFAULT_CATEGORYID_CONTENTSETUP);
                    ModuleController.SynchronizeModule(moduleInfo.ModuleID);
                }
                if (moduleName == Cons.MODULE_COMPANY_SETTING_NAME)
                {
                    var objModules = new ModuleController();
                    objModules.UpdateTabModuleSetting(moduleInfo.TabModuleID, Cons.SETTING_TAB_REDIRECT_CONTENTSETUP, Cons.PAGE_CONTENT_SETUP_NAME);
                    ModuleController.SynchronizeModule(moduleInfo.ModuleID);
                }
                DataCache.ClearModuleCache(moduleInfo.TabID);
            }
        }

        /// <summary>
        /// Handles the OnClick event of the btnCreatePortal control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        // Author  : Long Kim Vu
        // Created : 04-22-2014
        protected void btnCreatePortal_OnClick(object sender, EventArgs e)
        {
             _portalName = tbxPortalName.Text;
            _siteAlias = @tbxPortalAlias.Text;
            var currentUrl = GetPortalAlias();
            if (_siteAlias.StartsWith("http://"))
            {
                _siteAlias = _siteAlias.Substring("http://".Length);
            }
            _siteAlias = currentUrl + _siteAlias;
            if (CheckValidate())
            {
                try
                {
                    _adminUserName = @tbxPortalAlias.Text+"admin";
                    CreatePortal(_portalName, _siteAlias);
                    _portalSettings = new PortalSettings(_portalId);
                   // _skinName = Path.GetFileNameWithoutExtension(skinUpload.FileName);
                    string filename = Path.GetFileName(skinUpload.FileName);
                            
                    var skinPath = Globals.ApplicationMapPath + string.Format("\\Portals\\{0}\\", _portalId) + filename;
                    skinUpload.SaveAs(skinPath);
                    using (ZipFile zip = ZipFile.Read(skinPath))
                    {
                        foreach (ZipEntry z in zip)
                        {
                            z.Extract(Globals.ApplicationMapPath + string.Format("\\Portals\\{0}\\", _portalId), ExtractExistingFileAction.OverwriteSilently);  // overwrite == true
                        }
                    }

                    var skinFolder = Path.GetDirectoryName(Globals.ApplicationMapPath + string.Format("\\Portals\\{0}\\", _portalId + "\\Skins"));
                    if (skinFolder != null)
                    {
                        var dInfo = new DirectoryInfo(skinFolder);
                        _skinName = dInfo.GetDirectories().First().Name;
                    }

                    var containerFolder = Path.GetDirectoryName(Globals.ApplicationMapPath + string.Format("\\Portals\\{0}\\", _portalId + "\\Containers"));
                    if (containerFolder != null)
                    {
                        var dInfo = new DirectoryInfo(containerFolder);
                        _containerName = dInfo.GetDirectories().First().Name;
                    }
                    if (!string.IsNullOrEmpty(_skinName) || !string.IsNullOrEmpty(_containerName))
                    {
                        //Delete default tab
                        DeleteDefaultTab();
                        //Update default skin
                        UpdateSkin();
                        //Create Pages and Add modules
                        //AssignSkinToAllTab();
                       _cspAdminRoleId =  CreateCspAdminRoles(Cons.SETTING_ROLE_CSPADMIN);
                       _cspSuperAdminRoleId = CreateCspAdminRoles(Cons.SETTING_ROLE_CSPSUPERADMIN);
                        var tabCompanySettings = CreatePage(Cons.PAGE_COMPANY_SETTING_NAME, _portalSettings.RegisteredRoleId, null, true, Cons.MODULE_COMPANY_SETTING_NAME,false);
                        CreatePage(Cons.PAGE_CONTENT_SETUP_NAME, _portalSettings.RegisteredRoleId, null, true, Cons.MODULE_CONTENT_SETUP_NAME, false);
                        var tabRegister = CreatePage(Cons.PAGE_REGISTER_NAME, -1, null, false, Cons.MODULE_REGISTER_NAME, false);
                        var tabCspAdminId = CreatePage(Cons.PAGE_CSP_ADMIN_NAME, _portalSettings.AdministratorRoleId, null, true, Cons.MODULE_CSP_ADMIN_NAME, true);
                        CreatePage(Cons.PAGE_DNN_TRANSLATOR_NAME, _portalSettings.AdministratorRoleId, tabCspAdminId, true, Cons.MODULE_DNN_TRANSLATOR_NAME, true);
                        CreatePage(Cons.PAGE_SHOWCASE_EDITOR_NAME, _portalSettings.AdministratorRoleId, tabCspAdminId, true, Cons.MODULE_SHOWCASE_EDITOR_NAME, true);
                        CreatePage(Cons.PAGE_TRANSLATE_TABS, _portalSettings.AdministratorRoleId, tabCspAdminId, true,Cons.MODULE_EALO_TABS, true);
                        CreatePage(Cons.PAGE_MANAGE_ASSET_NAME, _portalSettings.AdministratorRoleId, tabCspAdminId, true, Cons.MODULE_MANAGE_ASSET_NAME, true);
                        TabInfo tabActivityFeed = _tabController.GetTabByName("Activity Feed", _portalId);
                        var tabUserProfile = CreatePage(Cons.PAGE_USER_PROFILE, _portalSettings.RegisteredRoleId, tabActivityFeed.TabID, false, Cons.MODULE_REGISTER_NAME,false);
                        var tabLoginPage = CreatePage(Cons.PAGE_lOGIN_NAME, -1, null, false, string.Empty, false);

                        // ltu 7/18 add contact support page
                        var tabContactSupport = CreatePage(Cons.PAGE_CONTACT_SUPPORT_NAME, -1, null, false, Cons.MODULE_CONTACT_SUPPORT_NAME, false);

                        SetSkinLoginPageAndProfile(tabLoginPage);
                        SetDefaultPage(tabCompanySettings, tabLoginPage, tabRegister, tabUserProfile);
                        AddUpdateConnectionString("CspPortal" + _portalId.ToString());
                       
                        //AssignSkinToAllTab();
                        //Add Profile Properties
                        EditProfileProperties();
                       
                        //LongVK 07/28 Update CspMaster and CspCreatePortalTask
                        UpdateCspMaster();
                        UpdateCspCreatePortalTask();

                        SetAdminProfile();
                       

                        string radalertscript = "<script language='javascript'> window.onload = function(){radalert('" + GetLocalizedText("Msg.CreatPortalCompleted") + "<br>" + "', 330, 210,'" + GetLocalizedText("Msg.Info") + "');}</script>";
                        Page.ClientScript.RegisterStartupScript(GetType(), "radalert", radalertscript);
                    }
                    else
                    {
                        PushAlertMessage(GetLocalizedText("Msg.InvalidFileZip"));
                    }

                }
                catch (Exception ex)
                {

                    PushAlertMessage(ex.Message);
                }
            }

        }

        /// <summary>
        /// Sets the admin profile.
        /// </summary>
        // Author  : Long Kim Vu
        // Created : 05-31-2014
        private void SetAdminProfile()
        {
            var userAdmin = UserController.GetUserById(_portalId, _adminUser.UserID);
            var acceptAgreementProfile = ProfileController.GetPropertyDefinitionByName(_portalId, "AcceptAgreement");
            var cspProfile = ProfileController.GetPropertyDefinitionByName(_portalId, "CspId");
            userAdmin.Profile.ProfileProperties.Add(cspProfile);
            userAdmin.Profile.ProfileProperties.Add(acceptAgreementProfile);
            userAdmin.Profile.SetProfileProperty("CspId", "-1");
            userAdmin.Profile.SetProfileProperty("AcceptAgreement", "True");
            ProfileController.UpdateUserProfile(userAdmin);
            UserController.UpdateUser(_portalId, userAdmin);
        }

        /// <summary>
        /// Creates the CSP admin roles.
        /// </summary>
        // Author  : Long Kim Vu
        // Created : 05-31-2014
        private int CreateCspAdminRoles(string roleName)
        {
            var oDnnRoleController = new RoleController();
            var oRole = oDnnRoleController.GetRoleByName(_portalId, roleName);
            if (oRole == null)
            {
                oRole = new RoleInfo
                {
                    PortalID = _portalId,
                    RoleName = roleName,
                    IsPublic = false,
                    AutoAssignment = false,
                    RoleGroupID = Null.NullInteger,
                    Status = RoleStatus.Approved
                };
                oDnnRoleController.AddRole(oRole);
            }
            return oRole.RoleID;
        }

        private void UpdateCspCreatePortalTask()
        {
            var newCreatePortalTask = new CSPCreatePortalTask
                {
                    id = Guid.NewGuid(),
                    command = "create",
                    project = _projectName,
                    dbname = tbxDbName.Text,
                    mode = CspMasterMode,
                    portalAlias = _siteAlias,
                    status = -1
                };
            DnnDataContext.CSPCreatePortalTasks.InsertOnSubmit(newCreatePortalTask);
            DnnDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
        }

        /// <summary>
        /// Updates the CSP master.
        /// </summary>
        // Author  : Long Kim Vu
        // Created : 05-31-2014
        private void UpdateCspMaster()
        {
            var connectionEncrypt = GetConnectionEncrypt(CspConnectionString);
            var newConnection = new Connection
                {
                    Mode = CspMasterMode,
                    Connection1 = connectionEncrypt,
                    Project = _projectName,
                    Domain = Domain
                };
            CspMasterDataContext.Connections.InsertOnSubmit(newConnection);
            CspMasterDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);   
        }


        /// <summary>
        /// Gets the connection encrypt.
        /// </summary>
        /// <param name="cspConnectionString">The CSP connection string.</param>
        /// <returns>System.String.</returns>
        // Author  : Long Kim Vu
        // Created : 05-31-2014
        private string GetConnectionEncrypt(string cspConnectionString)
        {
             byte[] des_key = { 20, 21, 24, 32, 1, 11, 12, 22, 23, 13, 14, 34, 89, 31, 56, 15, 16, 17, 18, 19, 35, 59, 2, 3 };
             byte[] des_iv = { 8, 7, 6, 5, 4, 3, 2, 1 };
             var Des = new TripleDes(des_key, des_iv);
             return Des.Encrypt(cspConnectionString);
        }



        /// <summary>
        /// Checks the validate.
        /// </summary>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
        // Author  : Long Kim Vu
        // Created : 05-31-2014
        private bool CheckValidate()
        {
            var portal = new PortalController();
            var listPortal = portal.GetPortals();
            var regexItem = new Regex("^[a-zA-Z0-9]*$");

            foreach (PortalInfo varible in listPortal)
            {
                if (_portalName == varible.PortalName)
                {
                    PushAlertMessage(GetLocalizedText("Msg.InvalidPortalName"));
                    return false;
                }
                var paController = new PortalAliasController();
                var paCollection = paController.GetPortalAliasByPortalID(varible.PortalID);
                var hs = paCollection.GetEnumerator();
                hs.MoveNext();
                var paInfo = (PortalAliasInfo)hs.Entry.Value;
                if (_siteAlias == paInfo.HTTPAlias)
                {
                    PushAlertMessage(GetLocalizedText("Msg.InvalidPortalAlias"));
                    return false;
                }
            }
            if (string.IsNullOrEmpty(tbxDomainName.Text))
            {
                if (string.IsNullOrEmpty(Domain))
                {
                    PushAlertMessage(GetLocalizedText("Msg.InvalidDomain"));
                    return false;
                }
            }
            else
            {
                Domain = tbxDomainName.Text;
            }
            if (string.IsNullOrEmpty(CspMasterMode))
            {
                PushAlertMessage(GetLocalizedText("Msg.InvalidMode"));
                return false;
            }
            if (string.IsNullOrEmpty(tbxProjectName.Text))
            {
                PushAlertMessage(GetLocalizedText("Msg.InvalidProjectName"));
                return false;
            }
            _projectName = tbxProjectName.Text;
            if (string.IsNullOrEmpty(CspConnectionString))
            {
                PushAlertMessage(GetLocalizedText("Msg.InvalidConnectionString"));
                return false;
            }
            CspMasterDataContext = new CspMasterDataContext(CspConnectionString.Replace("{DbName}", "csp_master"));
            if (CspMasterDataContext.Connections.Any(a => a.Project == _projectName))
            {
                PushAlertMessage(GetLocalizedText("Msg.InvalidProjectExist"));
                return false;
            }
            if (string.IsNullOrEmpty(tbxDbName.Text))
            {
                PushAlertMessage(GetLocalizedText("Msg.InvalidDbName"));
                return false;
            }
            if (string.IsNullOrEmpty(tbxPortalAlias.Text))
            {
                PushAlertMessage(GetLocalizedText("Msg.InvalidPortalAlias"));
                return false;
            }
            if (string.IsNullOrEmpty(tbxPortalName.Text))
            {
                PushAlertMessage(GetLocalizedText("Msg.InvalidPortalName"));
                return false;
            }
            if (string.IsNullOrEmpty(skinUpload.FileName) || !skinUpload.FileName.Contains(".zip") || !skinUpload.HasFile)
            {
                PushAlertMessage(GetLocalizedText("Msg.InvalidFileZip"));
                return false;
            }
            if (!regexItem.IsMatch(@tbxPortalAlias.Text))
            {
                PushAlertMessage(GetLocalizedText("Msg.InvalidPortalAlias"));
                return false;
            }            
            return true;
        }

        /// <summary>
        /// Pushes the alert message.
        /// </summary>
        /// <param name="message">The message.</param>
        // Author  : Long Kim Vu
        // Created : 05-31-2014
        private void PushAlertMessage(string message)
        {
            string radalertscript = "<script language='javascript'> window.onload = function(){radalert('" + message + "<br>" + "', 330, 210,'" + GetLocalizedText("Msg.ErrorTitle") + "');}</script>";
            Page.ClientScript.RegisterStartupScript(GetType(), "radalert", radalertscript);
        }

        /// <summary>
        /// Gets the portal alias.
        /// </summary>
        /// <returns>System.String.</returns>
        // Author  : Long Kim Vu
        // Created : 05-31-2014
        public string GetPortalAlias()
        {
            var currentUrl = PortalAlias.HTTPAlias;
            if (PortalId != 0)
            {
                var array = currentUrl.Split(new string[] { "/" }, StringSplitOptions.None);
                currentUrl = string.Empty;
                for (var i = 0; i < array.Count()-1; i++)
                {
                    currentUrl += array[i] + "/";
                }
            }
            if (!currentUrl.EndsWith("/"))
            {
                currentUrl = currentUrl + "/";
            }
            return currentUrl;
        }
    }
}