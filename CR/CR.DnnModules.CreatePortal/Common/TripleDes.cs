using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CR.DnnModules.CreatePortal.Common
{
	internal class TripleDes
	{
		private readonly TripleDESCryptoServiceProvider _des = new TripleDESCryptoServiceProvider();
		private readonly UTF8Encoding _utf8 = new UTF8Encoding();
		private readonly byte[] _mKey;
		private readonly byte[] _mIv;

		public TripleDes(byte[] m_key, byte[] m_iv)
		{
			this._mKey = m_key;
			this._mIv = m_iv;
		}
		public byte[] Encrypt(byte[] input)
		{
			return Transform(input, _des.CreateEncryptor(_mKey, _mIv));
		}
		public byte[] Decrypt(byte[] input)
		{
			return Transform(input, _des.CreateDecryptor(_mKey, _mIv));
		}
		public string Encrypt(string text)
		{
			byte[] input = _utf8.GetBytes(text);
			byte[] output = Transform(input, _des.CreateEncryptor(_mKey, _mIv));
			return System.Convert.ToBase64String(output);
		}
		public string Decrypt(string text)
		{
			byte[] input = System.Convert.FromBase64String(text);
			byte[] output = Transform(input, _des.CreateDecryptor(_mKey, _mIv));
			return _utf8.GetString(output);
		}

		private byte[] Transform(byte[] input, ICryptoTransform CryptoTransform)
		{
			//Create the necessary streams
			MemoryStream memStream = new MemoryStream();
			CryptoStream cryptStream = new CryptoStream(memStream, CryptoTransform, CryptoStreamMode.Write);

			// Transform the bytes as requested
			cryptStream.Write(input, 0, input.Length);
			cryptStream.FlushFinalBlock();

			// Read the memory stream and convert it back into byte array
			memStream.Position = 0;
			byte[] result = new byte[memStream.Length];
			memStream.Read(result, 0, result.Length);

			// Close and release the streams
			memStream.Close();
			cryptStream.Close();

			// Hand back the encrypted buffer
			return result;
		}
	}
}