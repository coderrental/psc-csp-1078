﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CR.DnnModules.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Localization;
using CR.DnnModules.CreatePortal.Data;
namespace CR.DnnModules.CreatePortal.Common
{
    public class DnnModuleBase : PortalModuleBase
    {
        private string _localResourceFile;
        public string CspConnectionString = string.Empty;
        public string HtmlLoginContent = string.Empty;
        public string CspMasterMode = string.Empty;
        public string Domain = string.Empty;
        public string Connection = string.Empty;
        public DnnDataContext DnnDataContext;
        public CspMasterDataContext CspMasterDataContext;

        /// <summary>
        /// Handles the Init event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected virtual void Page_Init(object sender, EventArgs e)
        {
            _localResourceFile = Utils.GetCommonResourceFile("CR.DnnModules.CreatePortal", LocalResourceFile);
            DnnDataContext = new DnnDataContext(Config.GetConnectionString());
            
            //Get value from module settings
            if (Settings[Cons.SETTING_CONNECTION_STRING] != null && Settings[Cons.SETTING_CONNECTION_STRING].ToString() != string.Empty)
            {
                CspConnectionString = Settings[Cons.SETTING_CONNECTION_STRING].ToString();
            }
            if (Settings[Cons.SETTING_HTML_LOGIN_CONTENT] != null && Settings[Cons.SETTING_HTML_LOGIN_CONTENT].ToString() != string.Empty)
            {
                HtmlLoginContent = Settings[Cons.SETTING_HTML_LOGIN_CONTENT].ToString();
            }
            if (Settings[Cons.SETTING_DOMAIN] != null && Settings[Cons.SETTING_DOMAIN].ToString() != string.Empty)
            {
                Domain = Settings[Cons.SETTING_DOMAIN].ToString();
            }
            if (Settings[Cons.SETTTING_CSPMASTERMODE] != null && Settings[Cons.SETTTING_CSPMASTERMODE].ToString() != string.Empty)
            {
                CspMasterMode = Settings[Cons.SETTTING_CSPMASTERMODE].ToString();
            }
        }
        /// <summary>
        /// Get localized text for the current portal language
        /// </summary>
        /// <param name="key">Resource Key</param>
        /// <returns>Localized text</returns>
        public string GetLocalizedText(string key)
        {
            return Localization.GetString(key, _localResourceFile);
        }
    }
}