﻿namespace CR.DnnModules.CreatePortal.Common
{
    public class Cons
    {
        public const string MODULE_COMPANY_SETTING_NAME = "CspModules.TIEKinetix.CspModules.CSPSubscriptionModule";
        public const string PAGE_COMPANY_SETTING_NAME = "Company Settings";

        public const string MODULE_CONTENT_SETUP_NAME = "CspModules.TIEKinetix.CspModules.ContentSetup";
        public const string PAGE_CONTENT_SETUP_NAME = "Syndicate Showcase";

        public const string MODULE_REGISTER_NAME = "Security";
        public const string PAGE_REGISTER_NAME = "Register";

        public const string MODULE_CSP_ADMIN_NAME = "CspModules.CR.DnnModules.ChildPageList";
        public const string PAGE_CSP_ADMIN_NAME = "Csp Admins";

        public const string MODULE_DNN_TRANSLATOR_NAME = "CspModules.TIEKinetix.CspModules.DnnTranslator";
        public const string PAGE_DNN_TRANSLATOR_NAME = "Dnn Translator";

        public const string MODULE_SHOWCASE_EDITOR_NAME = "CspModules.CR.DnnModules.ShowcaseEditor";
        public const string PAGE_SHOWCASE_EDITOR_NAME = "Showcase Builder";

        public const string MODULE_MANAGE_ASSET_NAME = "CspModules.TIEKinetix.CspModules.CSPContentManagementSystem";
        public const string PAGE_MANAGE_ASSET_NAME = "Manage Assets";

        public const string MODULE_LOGIN_NAME = "Authentication";
        public const string PAGE_lOGIN_NAME = "Login";
        public const string MODULE_CSP_HTML_NAME = "CspModules.TIEKinetix.CspModules.CSPHtmlModule";

        public const string SETTING_CONNECTION_STRING = "SettingConnectionString";
        public const string SETTING_HTML_LOGIN_CONTENT = "SettingHTMLLoginContent";
        public const string SETTING_DOMAIN = "SettingDomain";
        public const string SETTTING_CSPMASTERMODE = "SettingCSPMasterMode";

        // ltu 7/18 add support page
        public const string MODULE_CONTACT_SUPPORT_NAME = "CspModules.CSPSupport";
        public const string PAGE_CONTACT_SUPPORT_NAME = "Contact Support";

        public const string PAGE_USER_PROFILE = "User Profile";

        //LongVK add field to Content Setup module settings
        public const string SETTING_INSTANCE_NAME = "SettingInstanceName";
        public const string SETTING_REDIRECT_TAB_NAME_COMPANYSETTINGS = "RedirectTabName";
        public const string SETTING_CATEGORY_CONTENT_TYPE_ID = "CategoryContenttypeID";
        public static string DEFAULT_CATEGORYID_CONTENTSETUP = "31000";

        //LongVK add role to access CSP Admin and it's children
        public const string SETTING_ROLE_CSPADMIN = "CspAdmin";
        public const string SETTING_ROLE_CSPSUPERADMIN = "CspSuperAdmin";

        //LongVK add page Translate Tabs
        public const string PAGE_TRANSLATE_TABS = "Translate Tabs";
        public const string MODULE_EALO_TABS = "effority.Ealo.Tabs";

        //LongVK add fied to Company Settings module settings
        public static string SETTING_TAB_REDIRECT_CONTENTSETUP = "TabName";

        //LongVk add default admin
        public static string DEFAULT_ADMIN_EMAIL = @"lam.tu@tiekinetix.com";
        public static string DEFAULT_ADMIN_PASSWORD = "open4me";
    }
}