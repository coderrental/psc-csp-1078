﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainView.ascx.cs" Inherits="CR.DnnModules.CreatePortal.MainView" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server">  
</telerik:RadWindowManager> 
<div id ="createportal-wrapper">
    <h1 style="padding-bottom: 17px;"><%= GetLocalizedText("Label.CreatePortalInstance") %></h1>
    <table border="0" cellpadding="5" cellspacing="0" style="margin: 0; padding: 0; outline: none;">
        <tr>
            <td><label> <%= GetLocalizedText("Label.PortalName") %> </label></td>
            <td><asp:TextBox runat="server" ID ="tbxPortalName" Width="100"></asp:TextBox></td>
        </tr>
        <tr>
            <td><label><%= GetLocalizedText("Label.PortalAlias") %> </label></td>
            <td><span><%= GetPortalAlias() %></span><asp:TextBox runat="server" ID ="tbxPortalAlias" Width="100" CssClass="tbPortalAlias"></asp:TextBox></td>
        </tr>
        <tr>
            <td><label> <%= GetLocalizedText("Label.DBName") %> </label></td>
            <td><asp:TextBox runat="server" ID ="tbxDbName" Width="218px"></asp:TextBox></td>
        </tr>
         <tr>
            <td><label> <%= GetLocalizedText("Label.ProjectName") %> </label></td>
            <td><asp:TextBox runat="server" ID ="tbxProjectName" Width="218px"></asp:TextBox></td>
        </tr>
         <tr>
            <td><label> <%= GetLocalizedText("Label.Domain") %> </label></td>
            <td><asp:TextBox runat="server" ID ="tbxDomainName" Width="218px"></asp:TextBox></td>
        </tr>
        <tr>
            <td><label><%= GetLocalizedText("Label.SkinPath") %></label></td>
            <td><asp:FileUpload runat="server" ID="skinUpload"/></td>
        </tr>
        <tr>
            <td><asp:Button runat="server" ID="btnCreatePortal" Text="Create Portal" CssClass="formItemButtonBlue" OnClick="btnCreatePortal_OnClick"/></td>
        </tr>
    </table>

</div>
