﻿using CR.DnnModules.CreatePortal.Common;
using DotNetNuke.Entities.Modules;

namespace CR.DnnModules.CreatePortal
{
    public partial class Settings : ModuleSettingsBase
    {
        /// <summary>
        /// Loads the settings.
        /// </summary>
        public override void LoadSettings()
        {
            if (TabModuleSettings[Cons.SETTING_CONNECTION_STRING] != null)
                tbConnectionString.Text = TabModuleSettings[Cons.SETTING_CONNECTION_STRING].ToString();
            if (TabModuleSettings[Cons.SETTING_HTML_LOGIN_CONTENT] != null)
                tbxHtmlLogin.Text = TabModuleSettings[Cons.SETTING_HTML_LOGIN_CONTENT].ToString();
            if (TabModuleSettings[Cons.SETTING_DOMAIN] != null)
                tbDomain.Text = TabModuleSettings[Cons.SETTING_DOMAIN].ToString();
            if (TabModuleSettings[Cons.SETTTING_CSPMASTERMODE] != null)
                tbCspMasterMode.Text = TabModuleSettings[Cons.SETTTING_CSPMASTERMODE].ToString();
        }

        /// <summary>
        /// Updates the settings.
        /// </summary>
        public override void UpdateSettings()
        {
            var objModules = new ModuleController();
            objModules.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_CONNECTION_STRING, tbConnectionString.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_HTML_LOGIN_CONTENT, tbxHtmlLogin.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_DOMAIN, tbDomain.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, Cons.SETTTING_CSPMASTERMODE, tbCspMasterMode.Text);
            ModuleController.SynchronizeModule(ModuleId);
        }
    }
}