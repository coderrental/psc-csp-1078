﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Settings.ascx.cs" Inherits="CR.DnnModules.CreatePortal.Settings" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>
<div class="dnnFormItem">
    <dnn:Label ID="lblConnectionString" runat="server" ControlName="tbConnectionString" Text="CSP Connection String" />
    <asp:TextBox runat="server" ID="tbConnectionString"></asp:TextBox>
    <dnn:Label ID="lblCspMasterMode" runat="server" ControlName="tbCspMasterMode" Text="Csp Master Mode" />
    <asp:TextBox runat="server" ID="tbCspMasterMode"></asp:TextBox>
    <dnn:Label ID="lblDomain" runat="server" ControlName="tbDomain" Text="Domain" />
    <asp:TextBox runat="server" ID="tbDomain"></asp:TextBox>
    <dnn:Label ID="lblCspHtmlContent" runat="server" ControlName="tbxHtmlLogin" Text="Html Login Content" />
    <asp:TextBox runat="server" ID="tbxHtmlLogin" TextMode="MultiLine"></asp:TextBox>
</div>
