﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Data;
using System.Data.Linq;

namespace SocialUpdateTests
{
    [TestClass]
    public class UpdateBitlyClicksTests
    {
        [TestMethod]
        public void GetClicks()
        {
            var updateBitlyClick = new CR.SocialUpdateDaily.Tasks.UpdateBitlyClicks();
            updateBitlyClick.InitResources();
            var response = updateBitlyClick.GetClicks("http://bit.ly/1hWWETH");
            Assert.IsNotNull(response.ClickReponse);
            Assert.IsTrue(response.ClickReponse.BitlyClicks.Count > 0);
            Assert.IsTrue(response.ClickReponse.BitlyClicks[0].Clicks > 0);
        }

        [TestMethod]
        public void UpdateClicks()
        {
            const string connectionString = "Data Source=CRTEAM-PC;Initial Catalog=csp_siemens_dev;Persist Security Info=True;User ID=sa;Password=123456";
            var cspDatacontext = new CR.SocialUpdateDaily.Data.CspDbDataContext(connectionString);
            var updateBitlyClick = new CR.SocialUpdateDaily.Tasks.UpdateBitlyClicks();
            updateBitlyClick.InitResources();
            updateBitlyClick.UpdateClicks("http://bit.ly/1hWWETH",10,DateTime.Now,cspDatacontext);
            var record = cspDatacontext.SocialModule_BitlyLinksPerMessages.SingleOrDefault(a => a.BitlyLink == "http://bit.ly/1hWWETH");
            var updateRecord =cspDatacontext.SocialModule_BitlyLinksTrends.SingleOrDefault(a => a.BitlyLinksPerMessageId == record.BitlyLinksPerMessageId);

            Assert.IsNotNull(record);
            Assert.IsNotNull(updateRecord);
            Assert.IsTrue(updateRecord.Clicks > 0);
        }
    }
}
