﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using CR.DnnModules.Common;
using CR.SocialUpdateDaily.Entities;
using CR.SocialUpdateDaily.Tasks;
using log4net;

namespace CR.SocialUpdateDaily
{
    partial class CrSocialUpdateDaily : ServiceBase
    {
        private System.Timers.Timer _timer;
        private List<Portal> _listPortals;
        private readonly List<TaskBase> _listTasks = new List<TaskBase>();
        private static readonly ILog log = LogManager.GetLogger("SocialUpdateDaily");
        private Thread thread;
        public CrSocialUpdateDaily()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            var delayHour = int.Parse(ConfigurationManager.AppSettings["DelayHours"]);
            log4net.Config.XmlConfigurator.Configure();
            var portalssection = (PortalConfigSection)ConfigurationManager.GetSection("PortalConfigSection");

            _listPortals = (from PortalElement portalconfig in portalssection.PortalItem
                               select new Portal
                               {
                                   PortalId = portalconfig.PortalId,
                                   ConnectionString = portalconfig.ConnectionString,
                                   PortalName = portalconfig.PortalName,
                                   SupplierId = portalconfig.SupplierId
                               }).ToList();
            var listTasks = Assembly.GetExecutingAssembly()
                        .GetTypes()
                        .ToList()
                        .Where(a => a.Namespace == "CR.SocialUpdateDaily.Tasks" && a.BaseType == typeof(TaskBase))
                        .ToList();

            foreach (var task in listTasks)
            {
                var taskInstance = (TaskBase)Activator.CreateInstance(task);
                _listTasks.Add(taskInstance);

            }

           /* RunDailyPrograms();   

            _timer = new System.Timers.Timer { Interval = Convert.ToDouble(delayHour * 60 * 60 * 1000) };

            _timer.Elapsed += TimerElapsed;
            _timer.Enabled = true;*/

            //LongVk 09162014 change update daily program use thread
            double interval = 86400000;
            try
            {
                interval = int.Parse(ConfigurationManager.AppSettings["DelayHours"]) * 1000 * 60 * 60;
            }
            catch
            {
                interval = 86400000;
            }

            thread = new Thread(() => Do(interval));
            thread.Start();
        }

        //LongVk 09162014 change update daily program use thread
        private void LogError(Tuple<TaskBase, AggregateException> error)
        {
            log.Error(error.Item2.Message);
            //Email the exeption to someone to track the error
            var host = ConfigurationManager.AppSettings["SMTPHost"];
            var port = int.Parse(ConfigurationManager.AppSettings["SMTPPort"]);
            var username = ConfigurationManager.AppSettings["SMTPUsername"];
            var password = ConfigurationManager.AppSettings["SMTPPassword"];
            var enableSsl = bool.Parse(ConfigurationManager.AppSettings["SMTPEnableSSL"]);
            var recipient = ConfigurationManager.AppSettings["RecipientEmail"];

            var emailSender = new EmailSmtpSender(host, port, enableSsl, username, password);
            emailSender.SendEmail(username, recipient, "SocialUpdateDaily in task " + error.Item1.TaskName + " - Error: " + DateTime.Now.ToString("MM/dd/yy H:mm"), error.Item2.Message + "<br/>" + error.Item2.ToString());
        }

        private void Do(double interval)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            while (true)
            {
                sw.Restart();
                log.Debug("Begin executing the process. Interval: " + interval);
                var errors = _listTasks.OrderBy(a => a.Order).Select(task =>
                {
                    log.Debug("*** executing: " + task.TaskName);
                    try
                    {
                        task.DoWork(_listPortals);
                        return null;
                    }
                    catch (AggregateException ae)
                    {
                        return Tuple.Create(task, ae);
                    }
                    finally
                    {
                        log.Debug("*** Finished executing: " + task.TaskName);
                    }
                }).Where(a => a != null).ToList();
                log.DebugFormat("Finished executing the process with {0} error(s).", errors.Count);
                if (errors.Count > 0)
                {
                    foreach (var error in errors)
                    {
                        try
                        {
                            LogError(error);
                        }
                        catch (Exception ex)
                        {
                            log.Error(ex.Message, ex);
                        }
                    }
                }

                sw.Stop();
                log.DebugFormat("**** Process takes {0} milliseconds.", sw.ElapsedMilliseconds);
                var n = (int)(interval - sw.ElapsedMilliseconds);
                if (n > 0)
                {
                    log.DebugFormat("Sleep for {0} milliseconds", n);
                    Thread.Sleep(n);
                }
                Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// Timers the elapsed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="elapsedEventArgs">The <see cref="ElapsedEventArgs"/> instance containing the event data.</param>
        /// <author>Vu Dinh</author>
        /// <modified>08/13/2014 16:05:05</modified>
        private void TimerElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            RunDailyPrograms();   
        }

        /// <summary>
        /// Runs the daily programs.
        /// </summary>
        /// <author>Vu Dinh</author>
        /// <modified>08/25/2014 10:11:39</modified>
        private void RunDailyPrograms()
        {
            foreach (var task in _listTasks.OrderBy(a => a.Order))
            {
                //task.DoWork(_listPortals);
                var tmp = task;
                var thread = new Thread(() => tmp.DoWork(_listPortals));
                thread.Start();
            }
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
        }
    }
}
