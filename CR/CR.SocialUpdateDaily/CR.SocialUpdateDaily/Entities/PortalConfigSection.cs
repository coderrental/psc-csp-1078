﻿// ***********************************************************************
// Assembly         : CR.SocialUpdateDaily
// Author           : Vu Dinh
// Created          : 07-02-2014
//
// Last Modified By : Vu Dinh
// Last Modified On : 07-02-2014
// ***********************************************************************
// <copyright file="PortalConfigSection.cs" company="Coder Rental Team">
//     Copyright (c) Coder Rental Team. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Configuration;

namespace CR.SocialUpdateDaily.Entities
{
    #region[PortalConfigSection]
    public class PortalConfigSection : ConfigurationSection
    {
        /// <summary>
        /// Gets the portal item.
        /// </summary>
        /// <value>The portal item.</value>
        [ConfigurationProperty("Portals")]
        public PortalsCollection PortalItem
        {
            get { return ((PortalsCollection)(base["Portals"])); }

        }
    }
    #endregion

    #region [PortalsCollection]
    public class PortalsCollection : ConfigurationElementCollection
    {

        /// <summary>
        /// When overridden in a derived class, creates a new <see cref="T:System.Configuration.ConfigurationElement" />.
        /// </summary>
        /// <returns>A new <see cref="T:System.Configuration.ConfigurationElement" />.</returns>
        /// <author>Vu Dinh</author>
        /// <modified>07/02/2014 11:19:08</modified>
        protected override ConfigurationElement CreateNewElement()
        {
            return new PortalElement();
        }

        /// <summary>
        /// Gets the element key for a specified configuration element when overridden in a derived class.
        /// </summary>
        /// <param name="element">The <see cref="T:System.Configuration.ConfigurationElement" /> to return the key for.</param>
        /// <returns>An <see cref="T:System.Object" /> that acts as the key for the specified <see cref="T:System.Configuration.ConfigurationElement" />.</returns>
        /// <author>Vu Dinh</author>
        /// <modified>07/02/2014 11:19:10</modified>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((PortalElement)(element)).PortalId;
        }

        /// <summary>
        /// Gets or sets a property, attribute, or child element of this configuration element.
        /// </summary>
        /// <param name="idx">The idx.</param>
        /// <returns>PortalElement.</returns>
        public PortalElement this[int idx]
        {

            get
            {
                return (PortalElement)BaseGet(idx);
            }
        }
    }
    #endregion


    #region[PortalElement]
    public class PortalElement : ConfigurationElement
    {
        /// <summary>
        /// Gets or sets the portal id.
        /// </summary>
        /// <value>The portal id.</value>
        [ConfigurationProperty("portalid", DefaultValue = 0, IsKey = true, IsRequired = true)]

        public int PortalId
        {
            get
            {
                return ((int)(base["portalid"]));
            }
            set
            {
                base["portalid"] = value;
            }

        }


        /// <summary>
        /// Gets or sets the name of the portal.
        /// </summary>
        /// <value>The name of the portal.</value>
        [ConfigurationProperty("portalname", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string PortalName
        {
            get
            {
                return ((string)(base["portalname"]));
            }
            set
            {
                base["portalname"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the supplier id.
        /// </summary>
        /// <value>The supplier id.</value>
        [ConfigurationProperty("supplierid", DefaultValue = 12, IsKey = false, IsRequired = false)]
        public int SupplierId
        {
            get
            {
                return ((int)(base["supplierid"]));
            }
            set
            {
                base["supplierid"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>The connection string.</value>
        [ConfigurationProperty("connectionstring", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string ConnectionString
        {
            get
            {
                return ((string)(base["connectionstring"]));
            }
            set
            {
                base["connectionstring"] = value;
            }
        }
    }
    #endregion
}
