﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CR.DnnModules.Common;
using log4net;

namespace CR.SocialUpdateDaily.Common
{
    public static class Log
    {
        private static readonly ILog _log = LogManager.GetLogger("SocialUpdateDaily");

        /// <summary>
        /// Logs the info.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <author>Vu Dinh</author>
        /// <modified>07/01/2014 08:59:56</modified>
        public static void LogInfo(string text)
        {
            _log.Info(text);
        }

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <author>Vu Dinh</author>
        /// <modified>07/01/2014 08:59:57</modified>
        public static void LogError(Exception exception)
        {
            _log.Error(exception);

            //Email the exeption to someone to track the error
            var host = ConfigurationManager.AppSettings["SMTPHost"];
            var port = int.Parse(ConfigurationManager.AppSettings["SMTPPort"]);
            var username = ConfigurationManager.AppSettings["SMTPUsername"];
            var password = ConfigurationManager.AppSettings["SMTPPassword"];
            var enableSsl = bool.Parse(ConfigurationManager.AppSettings["SMTPEnableSSL"]);
            var recipient = ConfigurationManager.AppSettings["RecipientEmail"];

            var emailSender = new EmailSmtpSender(host, port, enableSsl, username, password);
            emailSender.SendEmail(username, recipient, "SocialUpdateDaily - Error: " + DateTime.Now.ToString("MM/dd/yy H:mm"), exception.ToString());
        }
    }
}
