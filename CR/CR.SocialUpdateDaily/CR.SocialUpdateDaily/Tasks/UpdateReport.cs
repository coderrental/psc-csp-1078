﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using CR.SocialUpdateDaily.Common;
using CR.SocialUpdateDaily.Data;
using CR.SocialUpdateDaily.Entities;
using DnnDataContext = CR.SocialUpdateDaily.Data.DnnDataContext;

namespace CR.SocialUpdateDaily.Tasks
{
    public class UpdateReport : TaskBase
    {
        private string _dnnConnectionString;
        private int _portalId;
        private int _supplierId;
        private DnnDataContext _dnnDataContext;
        private CspDbDataContext _cspDbDataContext;
        private CspDataContext _cspDataContext;
        private CspUtils _cspUltil;
        private List<CSPTSMM_MessageSentStatistic> _listTotalMessageStatics;
        private List<content> _listMessages;
        private Guid[] _listContentId, _listMessageStaticsId;
        /// <summary>
        /// Does the work.
        /// </summary>
        /// <param name="listPortals">The list portals.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        /// <author>Vu Dinh</author>
        /// <modified>07/01/2014 09:33:24</modified>
        public override void DoWork(List<Entities.Portal> listPortals)
        {
            Log.LogInfo(string.Format("--------------------------Begin execute program: {0}--------------------------", this.TaskName));
            _dnnConnectionString = ConfigurationManager.ConnectionStrings["DnnConnectionString"].ConnectionString;
            _dnnDataContext = new DnnDataContext(_dnnConnectionString);
            foreach (var portal in listPortals)
            {
                
                InitResources(portal);
                Log.LogInfo(string.Format("------------Update report for portal: {0}------------", portal.PortalId));
                try
                {
                    UpdateMessages();
                    Log.LogInfo(string.Format("------------Update completed table SocialModule_MessageReportTemp for portal: {0}------------", portal.PortalId));
                    UpdatePartners();
                    Log.LogInfo(string.Format("------------Update completed table SocialModule_PartnerReportTemp for portal: {0}------------", portal.PortalId));
                    DeleteTestAccounts();
                    Log.LogInfo(string.Format("------------Delete completed test account report in portal: {0}------------", portal.PortalId));
                    Log.LogInfo(string.Format("------------Update completed report for portal: {0}------------", portal.PortalId));
                    

                }
                catch (Exception e)
                {

                    Log.LogError(e);
                }  
            }
        }

        /// <summary>
        /// Deletes the test accounts.
        /// </summary>
        // Author  : Long Kim Vu
        // Created : 07-01-2014
        private void DeleteTestAccounts()
        {
            var listPartnerReport = _cspDbDataContext.SocialModule_PartnerReportTemps;
            if (listPartnerReport.Any())
            {
                var companyConsumerType = _cspDataContext.companies_parameter_types.FirstOrDefault(a => a.parametername == "ConsumerType");
                if (companyConsumerType != null)
                {
                    foreach (var partner in listPartnerReport)
                    {
                        try
                        {
                            var companyParameter = _cspDataContext.companies_parameters.FirstOrDefault(a => a.companies_parameter_types_Id == companyConsumerType.companies_parameter_types_Id && a.companies_Id == partner.CspId);
                            if (companyParameter != null)
                            {
                                if (companyParameter.value_text.ToUpper() == "T")
                                {
                                    _cspDbDataContext.SocialModule_PartnerReportTemps.DeleteOnSubmit(partner);
                                    DeleteMessageReportTempt(partner.CspId);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Log.LogError(e);
                        }
                      
                    }
                }
               
            }
            var listCompanyTestToDelete = _cspDbDataContext.SocialModule_PartnerReportTemps.Where(a => a.CompanyName.ToLower().StartsWith("test_"));
            if (listCompanyTestToDelete.Any())
            {
                _cspDbDataContext.SocialModule_PartnerReportTemps.DeleteAllOnSubmit(listCompanyTestToDelete);
                foreach (var companyToDelete in listCompanyTestToDelete)
                {
                    try
                    {
                        DeleteMessageReportTempt(companyToDelete.CspId);
                    }
                    catch (Exception e)
                    {
                        Log.LogError(e);
                    }
                   
                }
                _cspDbDataContext.SubmitChanges();    
            }
        }

        /// <summary>
        /// Deletes the message report tempt.
        /// </summary>
        /// <param name="cspId">The CSP id.</param>
        // Author  : Long Kim Vu
        // Created : 07-01-2014
        private void DeleteMessageReportTempt(int cspId)
        {
            var messageReportTempt = _cspDbDataContext.SocialModule_MessageReportTemps.Where(a => a.PartnerId == cspId);
            if (messageReportTempt.Any())
            {
                try
                {
                    _cspDbDataContext.SocialModule_MessageReportTemps.DeleteAllOnSubmit(messageReportTempt);
                    _cspDbDataContext.SubmitChanges();
                }
                catch (Exception e)
                {
                    Log.LogError(e);
                }
                
            }
        }


        /// <summary>
        /// Updates the messages.
        /// </summary>
        // Author  : Long Kim Vu
        // Created : 07-01-2014
        private void UpdateMessages()
        {
            foreach (var messageReport in _listTotalMessageStatics.OrderByDescending(a => a.SentSuccess))
            {
                var message = _cspDataContext.contents.Single(a => a.content_Id == messageReport.MessageId);
                var listcategory = _cspDataContext.content_categories.Where(a => a.content_main_Id == message.content_main_Id).ToList();
                var categoryId = listcategory.Count > 1 ? -1 : listcategory.First().category_Id;
                var language = _cspDataContext.languages.Single(a => a.languages_Id == message.content_types_languages_Id).description;
                var originalMsg = _cspUltil.GetContentFieldValue(message.content_Id, "Message");
                var customMsg = _cspUltil.GetCustomContentFieldValue(message, messageReport.CspId, "Message");
                var clicks = 0;

                if (!string.IsNullOrEmpty(customMsg))
                {
                    var listUrl = ExtractUrl(customMsg);
                    if (listUrl.Any())
                    {
                        foreach (var url in listUrl)
                        {
                            try
                            {
                                var bitlyLink = _cspDbDataContext.SocialModule_BitlyLinksPerMessages.FirstOrDefault(a =>
                                   a.MessageId == messageReport.MessageId && a.CspId == messageReport.CspId &&
                                   a.SocialId == messageReport.SocialId && a.BitlyLink == url && a.PortalId == _portalId);
                                if (bitlyLink != null)
                                {
                                    var bitlyTrend = _cspDbDataContext.SocialModule_BitlyLinksTrends.Where(a => a.BitlyLinksPerMessageId == bitlyLink.BitlyLinksPerMessageId);
                                    if (bitlyTrend.Any())
                                    {
                                        clicks += bitlyTrend.Sum(a => a.Clicks);
                                    }

                                }
                            }
                            catch (Exception exception)
                            {
                                Log.LogError(exception);
                            }
                           

                        }

                    }
                }
               
                var messageReportTemp =
                    _cspDbDataContext.SocialModule_MessageReportTemps.FirstOrDefault(
                        a =>
                        a.ContentId == messageReport.MessageId && a.Language == language &&
                        a.PartnerId == messageReport.CspId && a.CategoryId == categoryId &&
                        a.SocialId == messageReport.SocialId && a.PortalId == _portalId && a.SentSuccess == messageReport.SentSuccess);
                if (messageReportTemp == null)
                {
                    var newMessageReport = new SocialModule_MessageReportTemp
                        {
                            ContentId = messageReport.MessageId,
                            MessageContent = !string.IsNullOrEmpty(customMsg) ? customMsg : originalMsg,
                            SentAt = messageReport.SentAt,
                            CategoryId = categoryId,
                            Clicks = clicks,
                            Language = language,
                            NumberOfReceiver = messageReport.NumOfReceiver,
                            PartnerId = messageReport.CspId,
                            PortalId = _portalId,
                            SentSuccess = messageReport.SentSuccess,
                            SocialId = (int) messageReport.SocialId,
                        };

                    _cspDbDataContext.SocialModule_MessageReportTemps.InsertOnSubmit(newMessageReport);
                    _cspDbDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                }
                else
                {
                    messageReportTemp.MessageContent = !string.IsNullOrEmpty(customMsg) ? customMsg : originalMsg;
                    messageReportTemp.Clicks = clicks;
                    messageReportTemp.NumberOfReceiver = messageReport.NumOfReceiver;
                    messageReportTemp.SentSuccess = messageReport.SentSuccess;
                    _cspDbDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                }
                Log.LogInfo(string.Format("------------Update complete data for messageId: {0} , cspId: {1} ,socialId: {2}  in portal: {3}------------", messageReport.MessageId.ToString(),messageReport.CspId.ToString(),messageReport.SocialId.ToString(),_portalId.ToString()));
            }
        }

        public List<string> ExtractUrl(string message)
        {
            var urlRx = new Regex(@"(http|ftp|https)://([\w+?\.\w+])+([a-zA-Z0-9\~\|\!\@\#\[\]{\}\$\%\^\&\*\(\)_\-\=\+\\\/\?\.\:\;\'\,]*)?", RegexOptions.IgnoreCase);
            MatchCollection matches = urlRx.Matches(message);
            return (from Match match in matches select match.Value).ToList();
        }

        /// <summary>
        /// Updates the partners.
        /// </summary>
        // Author  : Long Kim Vu
        // Created : 07-01-2014
        private void UpdatePartners()
        {
            foreach (var partner in _dnnDataContext.CSPTSMM_Partners.Where(a => a.PortalId == _portalId))
            {
                try
                {
                    var company = _cspDataContext.companies.Single(a => a.companies_Id == partner.CspId);
                    var partnerReport = _cspDbDataContext.SocialModule_PartnerReportTemps.FirstOrDefault(a => a.CspId == partner.CspId && a.SocialId == partner.SocialId && a.PortalId == _portalId);
                    if (partnerReport == null)
                    {

                        var newPartnerReport = new SocialModule_PartnerReportTemp()
                        {
                            CspId = partner.CspId,
                            CompanyName = company.companyname,
                            SubscribedAt = partner.SubscribedAt,
                            TotalMessages = GetTotalMessagesPerPartner(partner.CspId, partner.SocialId),
                            Followers = partner.Followers,
                            Clicks = GetTotalClicksPerPartner(partner.CspId, partner.SocialId),
                            SocialId = (int)partner.SocialId,
                            IsSubscribed = partner.IsSubscribed,
                            PortalId = _portalId,
                            Country = company.country
                        };
                        _cspDbDataContext.SocialModule_PartnerReportTemps.InsertOnSubmit(newPartnerReport);
                        _cspDbDataContext.SubmitChanges();
                    }
                    else
                    {
                        partnerReport.CompanyName = company.companyname;
                        partnerReport.SubscribedAt = partner.SubscribedAt;
                        partnerReport.TotalMessages = GetTotalMessagesPerPartner(partner.CspId, partner.SocialId);
                        partnerReport.Followers = partner.Followers;
                        partnerReport.Clicks = GetTotalClicksPerPartner(partner.CspId, partner.SocialId);
                        partnerReport.IsSubscribed = partner.IsSubscribed;
                        partnerReport.Country = company.country;
                        _cspDbDataContext.SubmitChanges();
                    }
                    Log.LogInfo(string.Format("------------Update complete data for partnerId: {0} , socialId: {1} in portal: {2}------------", partner.CspId.ToString(), partner.SocialId.ToString(), _portalId.ToString()));
                }
                catch (Exception exception)
                {
                    
                    Log.LogError(exception);
                }
                
            }
        }

        /// <summary>
        /// Gets the total clicks per partner.
        /// </summary>
        /// <param name="cspId">The CSP id.</param>
        /// <param name="socialId">The social id.</param>
        /// <returns>System.Int32.</returns>
        // Author  : Long Kim Vu
        // Created : 07-01-2014
        private int GetTotalClicksPerPartner(int cspId, int? socialId)
        {
            int clicks = 0;
            var bitlyLink = _cspDbDataContext.SocialModule_BitlyLinksPerMessages.Where(a =>
                                     a.CspId == cspId &&
                                    a.SocialId == socialId  && a.PortalId == _portalId);
            if (bitlyLink.Any())
            {
                foreach (var socialModuleBitlyLinksPerMessage in bitlyLink)
                {
                        var bitlyTrend = _cspDbDataContext.SocialModule_BitlyLinksTrends.Where(a => socialModuleBitlyLinksPerMessage != null && a.BitlyLinksPerMessageId == socialModuleBitlyLinksPerMessage.BitlyLinksPerMessageId);
                        if (bitlyTrend.Any())
                        {
                            clicks += bitlyTrend.Sum(a => a.Clicks);
                        }
                }
                               

            }
            return clicks;
        }

        /// <summary>
        /// Gets the total messages per partner.
        /// </summary>
        /// <param name="cspId">The CSP id.</param>
        /// <param name="socialId">The social id.</param>
        /// <returns>System.Int32.</returns>
        // Author  : Long Kim Vu
        // Created : 07-01-2014
        private int GetTotalMessagesPerPartner(int cspId , int? socialId)
        {
            return _cspDbDataContext.SocialModule_MessageReportTemps.Count(a => a.PartnerId == cspId && a.SocialId == socialId && a.SentSuccess && a.PortalId ==_portalId);
        }

        /// <summary>
        /// Inits the resources.
        /// </summary>
        /// <param name="portal">The portal.</param>
        // Author  : Long Kim Vu
        // Created : 07-01-2014
        private void InitResources(Portal portal)
        {
            _supplierId = portal.SupplierId;
            _cspDbDataContext = new CspDbDataContext(portal.ConnectionString);
            _cspDataContext = new CspDataContext(portal.ConnectionString);
            _cspUltil = new CspUtils(_cspDataContext);
            _portalId = portal.PortalId;
            _listTotalMessageStatics = new List<CSPTSMM_MessageSentStatistic>();
            _listMessages = GetListContent("CspTweetMessage");
            _listContentId = _listMessages.Select(a => a.content_Id).ToArray();
            foreach (var messageSentStatistics in _dnnDataContext.CSPTSMM_MessageSentStatistics.Where(p => p.PortalId == _portalId).InRange(x => x.MessageId, 1000, _listContentId))
            {
                _listTotalMessageStatics.AddRange(new[] { messageSentStatistics });
            }
            _listMessageStaticsId = _listTotalMessageStatics.Select(a => a.MessageId).ToArray();
        }

        /// <summary>
        /// Gets the content of the list.
        /// </summary>
        /// <param name="contentTypeName">Name of the content type.</param>
        /// <returns>List{content}.</returns>
        /// Author: Vu Dinh
        /// 3/8/2013 - 11:53 AM
        private List<content> GetListContent(string contentTypeName)
        {
            var list = new List<content>();
            var messageCt = _cspDataContext.content_types.Single(a => a.description == contentTypeName);
            foreach (var language in _cspDataContext.languages.Where(a => a.active == true))
            {
                list.AddRange(_cspUltil.GetContentsFromCategoryId(language.languages_Id, messageCt.content_types_Id, _supplierId, -1, true).Distinct());
            }

            return list.OrderByDescending(a => a.content_main.cmID).ToList();
        }
        /// <summary>
        /// Gets or sets the name of the task.
        /// </summary>
        /// <value>The name of the task.</value>
        public override string TaskName
        {
            get { return "Update report nighly"; }
        }


        /// <summary>
        /// Gets the order of the task to execute.
        /// </summary>
        /// <value>The order.</value>
        public override int Order
        {
            get { return 1; }
        }
    }
}
