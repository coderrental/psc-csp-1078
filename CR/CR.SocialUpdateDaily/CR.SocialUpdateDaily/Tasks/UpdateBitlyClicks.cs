﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CR.SocialLib.Bitly;
using CR.SocialLib.Bitly.Entities;
using CR.SocialUpdateDaily.Common;
using CR.SocialUpdateDaily.Data;

namespace CR.SocialUpdateDaily.Tasks
{
    public class UpdateBitlyClicks : TaskBase
    {
        private string _bitLyAccessToken, _unit, _units, _timezone,_shortenUrlApiKey;

        /// <summary>
        /// Gets or sets the name of the task.
        /// </summary>
        /// <value>The name of the task.</value>
        public override string TaskName
        {
            get { return "Update Bitly clicks"; }
        }

        /// <summary>
        /// Gets the order of the task to execute.
        /// </summary>
        /// <value>The order.</value>
        public override int Order
        {
            get { return 0; }
        }

        public void InitResources()
        {
            _shortenUrlApiKey = ConfigurationManager.AppSettings["ShortenUrlApiKey"];
            _bitLyAccessToken = ConfigurationManager.AppSettings["BitLyAccessToken"];
            _unit = "day";
            _units = ConfigurationManager.AppSettings["Units"];
            _timezone = ConfigurationManager.AppSettings["Timezone"];
        }

        public override void DoWork(List<Entities.Portal> listPortals)
        {
            Log.LogInfo(string.Format("--------------------------Begin execute program: {0}--------------------------", this.TaskName));
            InitResources();
            Log.LogInfo("Init resource completed");
            foreach (var portal in listPortals)
            {
                var cspDatacontext = new CspDbDataContext(portal.ConnectionString);
                Log.LogInfo(string.Format("------------Update bitly link for portal: {0}------------", portal.PortalId));
                UpdateDailyClicks(cspDatacontext,portal.PortalId);
                Log.LogInfo(string.Format("------------Update completed bitly link for portal: {0}------------", portal.PortalId));
            }
            Log.LogInfo(string.Format("-------------End execute program {0}-------------", this.TaskName));
        }


        /// <summary>
        /// Updates the daily clicks.
        /// </summary>
        /// <param name="cspDbData">The CSP db data.</param>
        /// <param name="portalId"></param>
        /// <author>Vu Dinh</author>
        /// <modified>07/01/2014 09:04:10</modified>
        private void UpdateDailyClicks(CspDbDataContext cspDbData,int portalId)
        {
            try
            {
                var listBitlyLink = cspDbData.SocialModule_BitlyLinksPerMessages;
                var updateItem = new List<UpdateItem>();
                Parallel.ForEach(listBitlyLink, (link, state) =>
                {
                    if (link.BitlyLink.StartsWith("http://tieki.net/s"))
                    {
                        var item = GetClicksShortenUrlDefault(link.BitlyLink);
                        if (item!=null)
                        {
                            Log.LogInfo(string.Format("Get clicks of link: {0} completed in portal {1}", link.BitlyLink, portalId));
                            updateItem.Add(item);
                        }
                    }
                    var itemReponse = GetClicks(link.BitlyLink);
                    if (itemReponse == null)
                    {
                        Log.LogInfo(string.Format("Link: {0} has no click, continue with next link in portal {1}", link.BitlyLink,portalId));
                        state.Break();
                    }

                    Log.LogInfo(string.Format("Get clicks of link: {0} completed in portal {1}", link.BitlyLink,portalId));

                    if (itemReponse != null)
                        foreach (var record in itemReponse.ClickReponse.BitlyClicks)
                        {
                            foreach (var date in itemReponse.DateRequest)
                            {
                                try
                                {
                                    if (record.Date.Date == date)
                                    {
                                        updateItem.Add(new UpdateItem
                                            {
                                                BitlyLink = link.BitlyLink,
                                                Clicks = record.Clicks,
                                                DateTime = date
                                            });
                                    }
                                }
                                catch (Exception exception)
                                {
                                    Log.LogError(exception);
                                }
                            }
                        }
                });

                if (updateItem.Any())
                {
                    try
                    {
                        foreach (var item in updateItem)
                        {
                            try
                            {
                                UpdateClicks(item.BitlyLink, item.Clicks, item.DateTime, cspDbData);
                                Log.LogInfo(string.Format("Update clicks of link: {0} on {1} completed in portal {2}", item.BitlyLink, item.DateTime,portalId));
                            }
                            catch (Exception exception)
                            {
                                Log.LogError(exception);
                            }
                            
                        }

                    }
                    catch (Exception e)
                    {
                        Log.LogError(e);
                    }
                }
            }
            catch (Exception e)
            {

                Log.LogError(e);
            }

        }

        private UpdateItem GetClicksShortenUrlDefault(string shortenLink)
        {
            var updateItem = new UpdateItem();
            var bitlyHelper = new BitlyHelper(_bitLyAccessToken);
            var response = bitlyHelper.GetClicksDailyShortenUrlDefault(_shortenUrlApiKey, shortenLink);
            if (response!=null)
            {
                updateItem.BitlyLink = shortenLink;
                updateItem.Clicks = int.Parse(response.Visits);
                updateItem.DateTime = DateTime.Now.Date;
            }

            return updateItem;
        }

        public ItemResponse GetClicks(string url)
        {
            var itemResponse = new ItemResponse();
            try
            {
                var bitlyHelper = new BitlyHelper(_bitLyAccessToken);
                if (url != null) itemResponse.ClickReponse = bitlyHelper.GetClicksDaily(url, _unit, _units, _timezone);
                if (itemResponse.ClickReponse != null)
                {
                    foreach (var bitlyClick in itemResponse.ClickReponse.BitlyClicks)
                    {
                        itemResponse.DateRequest.Add(bitlyClick.Date.Date);
                    }
                    return itemResponse;
                }
            }
            catch (Exception e)
            {

                Log.LogError(e);
            }

            return null;
        }

        public void UpdateClicks(string url, int clicks, DateTime date, CspDbDataContext cspDbData)
        {
            try
            {
                // var listBitlyLink = _dnnDataContext.CSPTSMM_BitlyLinks.FirstOrDefault(a => a.BitlyLink == url);
                var listBitlyLink = cspDbData.SocialModule_BitlyLinksPerMessages.FirstOrDefault(a => a.BitlyLink == url);
                if (listBitlyLink != null)
                {
                    if (listBitlyLink.BitlyLink.StartsWith("http://tieki.net/s"))
                    {
                        var clicksdaily = cspDbData.SocialModule_BitlyLinksTrends.FirstOrDefault( a => a.BitlyLinksPerMessageId == listBitlyLink.BitlyLinksPerMessageId);
                        if (clicksdaily != null)
                        {
                            clicksdaily.Clicks = clicks;
                            clicksdaily.Date = date;
                        }
                        else
                        {
                            var newClick = new SocialModule_BitlyLinksTrend
                            {
                                BitlyLinksPerMessageId = listBitlyLink.BitlyLinksPerMessageId,
                                Date = date,
                                Clicks = clicks
                            };
                            cspDbData.SocialModule_BitlyLinksTrends.InsertOnSubmit(newClick);
                            cspDbData.SubmitChanges(ConflictMode.FailOnFirstConflict);
                        }
                    }
                    else
                    {
                        var bitlyClicksDaily = cspDbData.SocialModule_BitlyLinksTrends.FirstOrDefault(a => a.BitlyLinksPerMessageId == listBitlyLink.BitlyLinksPerMessageId && a.Date == date);
                        if (bitlyClicksDaily != null)
                        {
                            bitlyClicksDaily.Clicks = clicks;
                            cspDbData.SubmitChanges(ConflictMode.FailOnFirstConflict);
                        }
                        else
                        {
                            if (cspDbData.SocialModule_BitlyLinksTrends.Count(a => a.BitlyLinksPerMessageId == listBitlyLink.BitlyLinksPerMessageId && a.Date == date) == 0)
                            {
                                var newClick = new SocialModule_BitlyLinksTrend
                                {
                                    BitlyLinksPerMessageId = listBitlyLink.BitlyLinksPerMessageId,
                                    Date = date,
                                    Clicks = clicks
                                };
                                cspDbData.SocialModule_BitlyLinksTrends.InsertOnSubmit(newClick);
                                cspDbData.SubmitChanges(ConflictMode.FailOnFirstConflict);
                            }
                        }
                    }
                    
                }
            }
            catch (Exception e)
            {

                Log.LogError(e);
            }
        }



        private class UpdateItem
        {
            /// <summary>
            /// Gets or sets the bitly link.
            /// </summary>
            /// <value>The bitly link.</value>
            public string BitlyLink { get; set; }

            /// <summary>
            /// Gets or sets the clicks.
            /// </summary>
            /// <value>The clicks.</value>
            public int Clicks { get; set; }

            /// <summary>
            /// Gets or sets the date time.
            /// </summary>
            /// <value>The date time.</value>
            public DateTime DateTime { get; set; }
        }

        public class ItemResponse
        {
            public ItemResponse()
            {
                DateRequest = new List<DateTime>();
            }
            /// <summary>
            /// Gets or sets the click reponse.
            /// </summary>
            /// <value>The click reponse.</value>
            public ClicksResponse ClickReponse { get; set; }

            /// <summary>
            /// Gets or sets the date request.
            /// </summary>
            /// <value>The date request.</value>
            public List<DateTime> DateRequest { get; set; }
        }
    }
}
