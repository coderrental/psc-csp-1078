﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CR.SocialUpdateDaily.Tasks
{
    public abstract class TaskBase : ITask
    {
        public abstract void DoWork(List<Entities.Portal> listPortals);

        /// <summary>
        /// Gets or sets the name of the task.
        /// </summary>
        /// <value>The name of the task.</value>
        public abstract string TaskName { get; }

        /// <summary>
        /// Gets the order of the task to execute.
        /// </summary>
        /// <value>The order.</value>
        public abstract int Order { get; }
    }
}
