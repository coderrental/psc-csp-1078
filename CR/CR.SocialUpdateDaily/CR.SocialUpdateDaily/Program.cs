﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CR.SocialLib.Bitly;
using CR.SocialLib.Bitly.Entities;
using CR.SocialLib.OAuth;
using CR.SocialUpdateDaily.Common;
using CR.SocialUpdateDaily.Data;
using Microsoft.Win32;

namespace CR.SocialUpdateDaily
{
    class Program
    {
        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();

            var servicesToRun = new ServiceBase[] 
                 { 
                     new CrSocialUpdateDaily(),  
                 };
            ServiceBase.Run(servicesToRun);       
        }
    }
}
