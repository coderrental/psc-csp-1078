﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainView.ascx.cs" Inherits="CR.DnnModules.RenewToken.MainView" %>
<div class="headerModule">
    <div class="title left"><%= GetLocalizedText("Label.ModuleTitle") %></div>
    <div class="clear"></div>
</div>
<div>
    <div class="bs-callout bs-callout-danger">
        <h4><%= GetLocalizedText("Label.ResultHeader") %></h4>
        <p><asp:Label runat="server" ID="lblMsg"></asp:Label></p>
     </div>
</div>
