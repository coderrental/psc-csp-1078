﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Settings.ascx.cs" Inherits="CR.DnnModules.RenewToken.Settings" %>
<table>
    <tbody>
        <tr>
            <td><asp:Label ID="lblLinkedInConsumerKey" runat="server" ControlName="tbLinkedInConsumerKey" Text="LinkedIn Consumer Key" /></td>
            <td><asp:TextBox runat="server" ID="tbLinkedInConsumerKey"></asp:TextBox></td>
        </tr>
        <tr>
            <td><asp:Label ID="lblLinkedInConsumerSecret" runat="server" ControlName="tbLinkedInConsumerSecret" Text="LinkedIn Consumer Secret" /></td>
            <td><asp:TextBox runat="server" ID="tbLinkedInConsumerSecret"></asp:TextBox></td>
        </tr>
    </tbody>
</table>