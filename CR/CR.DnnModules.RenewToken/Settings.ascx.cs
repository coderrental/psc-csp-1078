﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.DnnModules.RenewToken.Common;
using DotNetNuke.Entities.Modules;

namespace CR.DnnModules.RenewToken
{
    public partial class Settings : ModuleSettingsBase
    {
        /// <summary>
        /// Loads the settings.
        /// </summary>
        public override void LoadSettings()
        {
            if (TabModuleSettings[Cons.SETTING_LINKEDIN_CONSUMER_KEY] != null)
                tbLinkedInConsumerKey.Text = TabModuleSettings[Cons.SETTING_LINKEDIN_CONSUMER_KEY].ToString();
            if (TabModuleSettings[Cons.SETTING_LINKEDIN_CONSUMER_SECRET] != null)
                tbLinkedInConsumerSecret.Text = TabModuleSettings[Cons.SETTING_LINKEDIN_CONSUMER_SECRET].ToString();
            if (TabModuleSettings[Cons.SETTING_LINKEDIN_CONSUMER_KEY] != null)
                tbLinkedInConsumerKey.Text = TabModuleSettings[Cons.SETTING_LINKEDIN_CONSUMER_KEY].ToString();
        }

        public override void UpdateSettings()
        {
            var objModules = new ModuleController();
            objModules.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_LINKEDIN_CONSUMER_KEY, tbLinkedInConsumerKey.Text);
            objModules.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_LINKEDIN_CONSUMER_SECRET, tbLinkedInConsumerSecret.Text);

            //refresh cache
            ModuleController.SynchronizeModule(ModuleId);
        }
    }
}