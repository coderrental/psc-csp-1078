﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using CR.DnnModules.RenewToken.Data;
using CR.SocialLib;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Localization;
using DnnDataContext = CR.DnnModules.RenewToken.Data.DnnDataContext;

namespace CR.DnnModules.RenewToken.Common
{
    public class DnnModuleBase : PortalModuleBase
    {
        private string _localResourceFile;
        protected DnnDataContext DataContext;
        protected string LinkedInConsumerKey, LinkedInConsumerSecret;

        protected virtual void Page_Init(object sender, EventArgs e)
        {
            #region [Init resource]
            _localResourceFile = Utils.GetCommonResourceFile("RenewToken", LocalResourceFile);
           new CspDataContext(Utils.GetConnectionString(PortalId));
           DataContext = new DnnDataContext(Config.GetConnectionString());

           //Get info from module setting
           if (Settings[Cons.SETTING_LINKEDIN_CONSUMER_KEY] != null && Settings[Cons.SETTING_LINKEDIN_CONSUMER_KEY].ToString() != string.Empty)
           {
               LinkedInConsumerKey = Settings[Cons.SETTING_LINKEDIN_CONSUMER_KEY].ToString();
           }
            if (Settings[Cons.SETTING_LINKEDIN_CONSUMER_SECRET] != null && Settings[Cons.SETTING_LINKEDIN_CONSUMER_SECRET].ToString() != string.Empty)
           {
               LinkedInConsumerSecret = Settings[Cons.SETTING_LINKEDIN_CONSUMER_SECRET].ToString();
           }
            #endregion

            
        }

        /// <summary>
        /// Get localized text for the current portal language
        /// </summary>
        /// <param name="key">Resource Key</param>
        /// <returns>Localized text</returns>
        public string GetLocalizedText(string key)
        {
            return Localization.GetString(key, _localResourceFile);
        }
    }
}