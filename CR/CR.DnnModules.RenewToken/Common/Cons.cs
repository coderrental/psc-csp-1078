﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CR.DnnModules.RenewToken.Common
{
    public class Cons
    {
        public const string SETTING_LINKEDIN_CONSUMER_KEY = "settinglinkedinconsumerkey";
        public const string SETTING_LINKEDIN_CONSUMER_SECRET = "settinglinkedinconsumersecret";
        public const string ACTION_RENEW = "renew";
        public const string ACTION_RENEWSUCCESS = "renewsuccess";
        public const string ACTION_ERROR = "error";
    }
}