﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.DnnModules.RenewToken.Common;
using CR.DnnModules.RenewToken.Data;
using CR.SocialLib;
using DotNetNuke.Common;

namespace CR.DnnModules.RenewToken
{
    public partial class MainView : DnnModuleBase
    {
        private SocialHelper _socialHelper;
        protected override void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender,e);
            if (string.IsNullOrEmpty(LinkedInConsumerKey) || string.IsNullOrEmpty(LinkedInConsumerSecret))
            {
                lblMsg.Text = GetLocalizedText("Label.MissingLinkedConsumerInfo");
                return;
            }
            _socialHelper = new SocialHelper(SocialLib.Common.Socialtype.LinkedIn, LinkedInConsumerKey, LinkedInConsumerSecret, Globals.NavigateURL(""));

            if (!string.IsNullOrEmpty(Request["key"]))
            {
               ExecuteAction();
            }

            if (!string.IsNullOrEmpty(Request["error"]) && Request["error"] == "access_denied") //if user hit cancel LinkedIn authorize
            {
                RedirectAction(string.Format("action={0}&error={1}", Cons.ACTION_ERROR, !string.IsNullOrEmpty(Request["error_description"]) ? Request["error_description"] : "Access Denied"));
            }

            if (Request["code"] != null && Request["state"] != null)
            {
               RenewToken();
            }
        }

        /// <summary>
        /// Executes the action.
        /// </summary>
        /// <author>Vu Dinh</author>
        /// <modified>09/16/2014 10:44:54</modified>
        private void ExecuteAction()
        {
            string encryptedKey;
            try
            {
                encryptedKey = DnnModules.Common.CryptoServiceProvider.Decode(Request["key"]);
            }
            catch (Exception)
            {
                lblMsg.Text = GetLocalizedText("Label.InvalidKey");
                return;
            }

            var query = HttpUtility.ParseQueryString(encryptedKey);
            var action = query["action"];
            switch (action)
            {
                case Cons.ACTION_RENEW:
                    {
                        var cspId = query["cspid"];
                        var subscribeLink = _socialHelper.GetAuthenticationLink(cspId);
                        Response.Redirect(subscribeLink);
                        break;
                    }

                case Cons.ACTION_RENEWSUCCESS:
                    {
                        lblMsg.Text = GetLocalizedText("Label.RenewTokenSuccess");
                        break;
                    }

                case Cons.ACTION_ERROR:
                    {
                        var error = query["error"];
                        lblMsg.Text = error;
                        break;
                    }

                default: break;
            }
        }

        /// <summary>
        /// Renews the token with code from LinkedIn
        /// </summary>
        /// <author>Vu Dinh</author>
        /// <modified>09/16/2014 10:42:49</modified>
        private void RenewToken()
        {
            var renewSuccess = false;
            try
            {
                var cspId = int.Parse(Request["state"]);
                var partnerTokenInfo = DataContext.CSPTSMM_PartnersSubscriptionInfos.SingleOrDefault(a => a.PartnerId == cspId && a.PortalId == PortalId && a.SocialId == (int)SocialLib.Common.Socialtype.LinkedIn);
                var token = _socialHelper.EndAuthenticationAndRegisterTokens();
                if (!string.IsNullOrEmpty(token.Token))
                {
                    var partner = DataContext.CSPTSMM_Partners.FirstOrDefault(a => a.CspId == cspId && a.SocialId == (int)SocialLib.Common.Socialtype.LinkedIn);
                    if (partner != null)
                    {
                        //update accesstoken for this partner
                        partner.Token = token.Token;
                        partner.TokenSecret = string.IsNullOrEmpty(token.TokenSecret) ? string.Empty : token.TokenSecret;
                        DataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);


                        if (partnerTokenInfo != null) //renew the token access
                        {
                            partnerTokenInfo.LastRenewDate = DateTime.Now;
                            partnerTokenInfo.NextRenewDate = DateTime.Now.AddSeconds(token.TokenExpireIn);
                            DataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                            renewSuccess = true;
                        }
                        else //partner from old OAuth system, need to add new record to track token expired
                        {
                            DataContext.CSPTSMM_PartnersSubscriptionInfos.InsertOnSubmit(new CSPTSMM_PartnersSubscriptionInfo
                                {
                                    Id = Guid.NewGuid(),
                                    PartnerId = cspId,
                                    PortalId = PortalId,
                                    SocialId = (int)SocialLib.Common.Socialtype.LinkedIn, //TODO: change hard code
                                    LastRenewDate = DateTime.Now,
                                    NextRenewDate = DateTime.Now.AddSeconds(token.TokenExpireIn)
                                });
                            DataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                            renewSuccess = true;
                        }
                        
                    }
                }
                if (renewSuccess)
                {
                    RedirectAction(string.Format("action={0}&time={1}", Cons.ACTION_RENEWSUCCESS, DateTime.Now.Ticks));
                }
                else
                {
                    RedirectAction(string.Format("action={0}&error={1}", Cons.ACTION_ERROR, GetLocalizedText("Label.CantRenewToken")));
                }
            }
            catch (Exception exception)
            {
                RedirectAction(string.Format("action={0}&error={1}", Cons.ACTION_ERROR, exception.Message));
            }
        }

        /// <summary>
        /// Redirects the action.
        /// </summary>
        /// <param name="param">The param.</param>
        /// <author>Vu Dinh</author>
        /// <modified>09/16/2014 10:41:12</modified>
        private void RedirectAction(string param)
        {
            var paramEncrypted = HttpUtility.UrlEncode(DnnModules.Common.CryptoServiceProvider.Encode(param));
            Response.Redirect(Globals.NavigateURL("") + "?key=" + paramEncrypted,false);
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        /// <author>Vu Dinh</author>
        /// <modified>09/16/2014 10:41:14</modified>
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}