﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.UI.WebControls;
using Telerik.Web.UI;
using CR.DnnModules.ContentMigration.Common;
using CR.ContentObjectLibrary.Data;

namespace CR.DnnModules.ContentMigration
{
	public partial class MainView : DnnModuleBase
	{
		private CspDataContext _cspDataContextSrc;
		private CspDataContext _cspDataContextDest;
		private int _contentTypeIdSrc = 5;
		private int _contentTypeIdDest = 15000;
		private int _categoryId = 0;
		protected List<content_types_field> ContentTypesFieldsDest;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (Request.Headers["X-OFFICIAL-REQUEST"] == "TRUE")
			{
				GoAjaxAction();
				return;
			}
		}

		protected void BtnConnectClick(object sender, EventArgs e)
		{
			if (String.IsNullOrEmpty(tbxConStringSrc.Text) || String.IsNullOrEmpty(tbxContentTypeSrc.Text) || String.IsNullOrEmpty(tbxContentTypeDest.Text))
			{
				ErrorMsg.InnerText = "Please fill the form";
				return;
			}

			String srcConnectionString = "";
			try
			{
				srcConnectionString = Config.GetConnectionString(tbxConStringSrc.Text);
			}
			catch(Exception exc)
			{
				ErrorMsg.InnerText = "Source connection string not found!";
				return;
			}
			
			_cspDataContextSrc = new CspDataContext(srcConnectionString);
			if (_cspDataContextSrc == null)
			{
				ErrorMsg.InnerText = "Source connection string not found!";
				return;
			}
			if (!int.TryParse(tbxContentTypeSrc.Text, out _contentTypeIdSrc))
				_contentTypeIdSrc = 5;
			if (!int.TryParse(tbxConStringDest.Text, out _contentTypeIdDest))
				_contentTypeIdDest = 15000;
			int.TryParse(tbxCategoryId.Text, out _categoryId);
			ErrorMsg.InnerText = "";
			List<content_types_field> contentTypesFieldsSrc =
				_cspDataContextSrc.content_types_fields.Where(a => a.content_types_Id == _contentTypeIdSrc).OrderBy(b => b.fieldname).ToList();
			if (contentTypesFieldsSrc.Any())
			{
				ListContentFieldSrc.DataSource = contentTypesFieldsSrc;
				ListContentFieldSrc.DataBind();

				ContentTypesFieldsDest =
					CspDataContext.content_types_fields.Where(a => a.content_types_Id == _contentTypeIdDest).OrderBy(b => b.fieldname).ToList();
				//cbFieldListDest.Items.Add(new ListItem("", ""));
				cbFieldListDest.DataSource = ContentTypesFieldsDest;
				cbFieldListDest.DataTextField = "fieldname";
				cbFieldListDest.DataValueField = "fieldname";
				cbFieldListDest.DataBind();
				cbFieldListDest.Items.Insert(0, new ListItem("", ""));
				cbFieldListDest.SelectedIndex = 0;

				btnFieldDetect.Enabled = true;
				btnGoMigrate.Enabled = true;
				btnReset.Enabled = true;
				btnConnect.Enabled = false;
				tbxContentTypeDest.Enabled = false;
				tbxContentTypeSrc.Enabled = false;
			}
			String migrateInfo = "_ Content main(s) to migrate: " +
			                     _cspDataContextSrc.content_mains.Count(a => a.content_types_Id == _contentTypeIdSrc).ToString();
			migrateInfo += "<br />_ Content(s) to migrate: " + _cspDataContextSrc.contents.Count(a => a.content_main.content_types_Id == _contentTypeIdSrc).ToString();

			if (_categoryId > 0)
			{
				category categoryNode = CspDataContext.categories.FirstOrDefault(a => a.categoryId == _categoryId);
				if (categoryNode != null)
				{
					migrateInfo += "<br />_ Category to assign to: " + categoryNode.categoryText;
					tbxCategoryId.Enabled = false;
				}
			}
			
			MigateInfo.InnerHtml = migrateInfo;
		}

		private void GoAjaxAction()
		{
			Response.Clear();
			String srcConnectionString = "";
			try
			{
				srcConnectionString = Config.GetConnectionString(Request.Params["connectionstringsrc"]);
			}
			catch(Exception exc)
			{
				Response.Write("Source connection string not found!");
				Response.Flush();
				Response.End();
				return;
			}
			
			_cspDataContextSrc = new CspDataContext(srcConnectionString);
			if (_cspDataContextSrc == null)
			{
				Response.Write("Source connection string not found!");
				Response.Flush();
				Response.End();
				return;
			}
			if (string.IsNullOrEmpty(Request.Params["connectionstringdest"]))
				_cspDataContextDest = CspDataContext;
			if (!int.TryParse(tbxContentTypeSrc.Text, out _contentTypeIdSrc))
				_contentTypeIdSrc = 5;
			if (!int.TryParse(tbxConStringDest.Text, out _contentTypeIdDest))
				_contentTypeIdDest = 15000;
			int.TryParse(Request.Params["categoryid"].ToString(), out _categoryId);
			category categoryNode = null;
			if (_categoryId > 0)
				categoryNode = _cspDataContextDest.categories.FirstOrDefault(a => a.categoryId == _categoryId);
			String[] srcFields = Request.Params["srcfields"].Split(',');
			String[] destFields = Request.Params["destfields"].Split(',');
			List<content_main> contentMainsSrc =
				_cspDataContextSrc.content_mains.Where(a => a.content_types_Id == _contentTypeIdSrc).ToList();
			int migrateCount = 1;
			foreach (content_main contentMain in contentMainsSrc)
			{
				content_main newContentMainDest = new content_main
				                                  	{
														content_main_Id = Guid.NewGuid(),
														cmID = contentMain.cmID,
														consumer_Id = contentMain.consumer_Id,
														content_main_key = contentMain.content_main_key,
														content_types_Id = _contentTypeIdDest,
														content_identifier = contentMain.content_identifier,
														supplier_Id = contentMain.supplier_Id,
														publication_scheme_Id = contentMain.publication_scheme_Id,
														content_staging_scheme_Id = contentMain.content_staging_scheme_Id
				                                  	};
				if (categoryNode != null)
				{
					content_category destContentCategoryNode =
						_cspDataContextDest.content_categories.FirstOrDefault(a => a.content_main.content_types_Id == _contentTypeIdDest);
					newContentMainDest.content_categories.Add(new content_category
					                                          	{
																	content_main = newContentMainDest,
																	category = categoryNode,
																	publication_scheme_Id = (destContentCategoryNode == null) ? 11 : destContentCategoryNode.publication_scheme_Id
					                                          	});
				}
				_cspDataContextDest.content_mains.InsertOnSubmit(newContentMainDest);
				
				List<content> contentsSrc =
					_cspDataContextSrc.contents.Where(a => a.content_main_Id == contentMain.content_main_Id).ToList();
				foreach (content contentSrc in contentsSrc)
				{
					content newContentDest = new content
					                         	{
													content_Id = Guid.NewGuid(),
													content_main = newContentMainDest,
													content_types_languages_Id = contentSrc.content_types_languages_Id,
													stage_Id = contentSrc.stage_Id,
													active = contentSrc.active
					                         	};
					for (int i = 0; i < destFields.Count(); i++)
					{
						content_types_field contentTypesField =
							_cspDataContextDest.content_types_fields.FirstOrDefault(
								a => a.content_types_Id == _contentTypeIdDest && a.fieldname == destFields[i]);
						if (contentTypesField == null)
							continue;
						newContentDest.content_fields.Add(new content_field
						                                  	{
																content_types_field = contentTypesField,
																content = newContentDest,
																value_text = contentSrc[srcFields[i]].value_text
						                                  	});
						_cspDataContextDest.contents.InsertOnSubmit(newContentDest);
					}
					
				}
				_cspDataContextDest.SubmitChanges(ConflictMode.FailOnFirstConflict);
				Response.Write("Migrated item #" + migrateCount.ToString());
				migrateCount++;
			}
			Response.Flush();
			Response.End();
		}
	}
}