﻿using System;
using CR.DnnModules.Common;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;
using DotNetNuke.Services.Log.EventLog;
using CR.ContentObjectLibrary.Data;

namespace CR.DnnModules.ContentMigration.Common
{
	public class DnnModuleBase : PortalModuleBase
	{
		protected EventLogController DnnEventLog;
		protected ModuleActionCollection MyActions;
		private string _localResourceFile;
		protected CspDataContext CspDataContext;

		/// <summary>
		/// Handles the Init event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected virtual void Page_Init(object sender, EventArgs e)
		{
			//Init Local Resource file
			_localResourceFile = Utils.GetCommonResourceFile("CR.DnnModules.ContentMigration", LocalResourceFile);
			//Init module title
			ModuleConfiguration.ModuleTitle = GetLocalizedText("ModuleTitle");
			//Init csp data context
			CspDataContext = new CspDataContext(Utils.GetConnectionString(PortalId));
		}

		/// <summary>
		/// Get localized text for the current portal language
		/// </summary>
		/// <param name="key">Resource Key</param>
		/// <returns>Localized text</returns>
		public string GetLocalizedText(string key)
		{
			return Localization.GetString(key, _localResourceFile);
		}

		/// <summary>
		/// Write message to Dnn event view
		/// </summary>
		/// <param name="message">message to write</param>
		public void Log(string message)
		{
			DnnEventLog.AddLog("DEBUG", message, PortalSettings, UserId, EventLogController.EventLogType.ADMIN_ALERT);
		}

		/// <summary>
		/// Gets the content field value.
		/// </summary>
		/// <param name="contentPage">The content page.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns></returns>
		/// <author>ducuytran - 2/27/2013</author>
		public string GetContentFieldValue(content contentPage, string fieldName)
		{
			if (contentPage == null)
				return string.Empty;

			content_field cf = contentPage[fieldName];
			return (cf != null) ? (string.IsNullOrEmpty(cf.value_text) ? string.Empty : cf.value_text) : string.Empty;
		}
	}
}