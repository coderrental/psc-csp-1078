﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainView.ascx.cs" Inherits="CR.DnnModules.ContentMigration.MainView" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Panel runat="server" ID="ModuleMainPage">
	<div id="connection-string-section">
		<div runat="server" ID="ErrorMsg" class="error-msg"></div>
		<table width="100%">
			<tr class="block-label">
				<td>
					SOURCE
				</td>
				<td>
					DESTINATION
				</td>
			</tr>
			<tr>
				<td colspan="2" class="section-label">
					Connection String:
				</td>
			</tr>
			<tr>
				<td>
					<telerik:RadTextBox runat="server" ID="tbxConStringSrc" Width="300px"></telerik:RadTextBox>
				</td>
				<td>
					<telerik:RadTextBox runat="server" ID="tbxConStringDest" Width="300px" EmptyMessage="Leave blank if using current portal connection"></telerik:RadTextBox>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="section-label">
					Default Category Id:
				</td>
			</tr>
			<tr>
				<td>
					
				</td>
				<td>
					<telerik:RadTextBox runat="server" ID="tbxCategoryId" Width="150px" EmptyMessage="Leave blank if not assign"></telerik:RadTextBox>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="section-label">
					Content Type Id:
				</td>
			</tr>
			<tr>
				<td>
					<telerik:RadTextBox runat="server" ID="tbxContentTypeSrc" Width="150px"></telerik:RadTextBox>
				</td>
				<td>
					<telerik:RadTextBox runat="server" ID="tbxContentTypeDest" Width="150px"></telerik:RadTextBox>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="section-label">
					Content Type Fields:
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div id="field-section">
					<asp:Repeater runat="server" ID="ListContentFieldSrc">
						<ItemTemplate>
							<div class="field-row">
								<div class="srcFieldCol" style="float: left; width: 345px;">
									<%# Eval("fieldname").ToString() %>
								</div>
								<div class="destFieldCol" style="float: left; width: 345px;">
								
								</div>
								<div class="clear"></div>
							</div>
						</ItemTemplate>
					</asp:Repeater>
					</div>
					<div class="hide">
						<asp:DropDownList runat="server" ID="cbFieldListDest"></asp:DropDownList>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div id="MigateInfo" runat="server" style="color: #1D5184; background-color: #9EC2E5; padding: 5px; font-size: 14px;"></div>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<telerik:RadButton runat="server" Text="Connect" ID="btnConnect" OnClick="BtnConnectClick"></telerik:RadButton>
					<telerik:RadButton runat="server" Text="Reset" ID="btnReset" AutoPostBack="False" OnClientClicked="GoReset" Enabled="False"></telerik:RadButton>
					<telerik:RadButton runat="server" Text="Fields Detect" ID="btnFieldDetect" AutoPostBack="False" OnClientClicked="AutoDetect" Enabled="False"></telerik:RadButton>
					<telerik:RadButton runat="server" Text="Start Migrating" ID="btnGoMigrate" AutoPostBack="False" OnClientClicked="GoMigrate" Enabled="False"></telerik:RadButton>
				</td>
			</tr>
		</table>
	</div>
	<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" ZIndex="99999" Skin="Vista">
	</telerik:RadAjaxLoadingPanel>
</asp:Panel>
<script type="text/javascript">
	$(function () {
		var fieldList = $('#<%= cbFieldListDest.ClientID %>').clone();
		fieldList.removeAttr('id');
		fieldList.removeAttr('name');
		fieldList.addClass('dest-field-list');
		fieldList.appendTo('.destFieldCol');

		$('.dest-field-list').change(function () {
			var newValue = $(this).find('option:selected').text();
			if (newValue == '') return;
			var valueCount = 0;
			$('#field-section').children().find('option:selected').each(function () {
				if ($(this).text() == newValue)
					valueCount++;
			});
			if (valueCount > 1) {
				$(this).closest('div.field-row').addClass('field-duplicate');
				$('#field-section').children().find('option:selected').each(function () {
					if ($(this).text() == newValue)
						$(this).closest('div.field-row').addClass('field-duplicate');
				});
			}
		});

		/*$('.field-row').hover(function () {
			$(this).css('background-color', '#5892CB');
		}, function () {
			$(this).css('background-color', 'transparent');
		});*/
	});
	
	function AutoDetect() {
		$('.field-row').each(function () {
			var srcFieldText = $.trim($(this).find('.srcFieldCol').html());
			$(this).children().find('option[value="' + srcFieldText + '"]').attr('selected', 'selected');
		});
	}

	function GoMigrate() {
		var conStringSrc = $find('<%= tbxConStringSrc.ClientID %>').get_value();
		var conStringDest = $find('<%= tbxConStringDest.ClientID %>').get_value();
		var categoryId = $find('<%= tbxCategoryId.ClientID %>').get_value();
		var srcFields = '';
		var destFields = '';
		$('.field-row').each(function () {
			if ($.trim($(this).children().find('option:selected').val()) != '' && $.trim($(this).find('.srcFieldCol').html()) != '') {
				if (srcFields != '')
					srcFields += ',';
				if (destFields != '')
					destFields += ',';
				srcFields += $.trim($(this).find('.srcFieldCol').html());
				destFields += $.trim($(this).children().find('option:selected').val());
			}
		});
		if (destFields == '') {
			alert('Please select at least 01 destination content type field');
			return;
		}
		$.ajax({
			type: 'POST',
			url: document.URL,
			dataType: 'html',
			data: { ajaxaction: 'go_migrate', connectionstringsrc: conStringSrc, connectionstringdest: conStringDest, categoryid: categoryId, srcfields: srcFields, destfields: destFields },
			beforeSend: function (xhr) {
				xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
				switchLoadingPanel(true);
			},
			success: function (responseMsg) {
				console.log(responseMsg);
				switchLoadingPanel(false);
				$find('<%= btnGoMigrate.ClientID %>').set_enabled(false);
			}
		});
	}

	function GoReset() {
		switchLoadingPanel(false);
		window.location = window.location.href;
	}

	function switchLoadingPanel(flag) {
		var loadingPanel = $find('<%= RadAjaxLoadingPanel1.ClientID %>');
		if (loadingPanel == null) {
			return;
		}
		if (flag) {
			loadingPanel.show('<%= ModuleMainPage.ClientID %>');
			return;
		}
		loadingPanel.hide('<%= ModuleMainPage.ClientID %>');
	}
</script>