﻿namespace CR.ContentObjectLibrary.Synchronization.Utils
{
	/// <summary>
	/// AgencyError List
	/// 1-50 Agency Profile Port Type
	/// 51-98 Agency Logo Upload Port Type
	/// 101-199 Server Error
	/// </summary>
	public class AgencyError
	{
		public int ErrorCode { get; set; }
		public string ErrorMessage { get; set; }

		public static AgencyError Success { get { return new AgencyError(0, "Success"); } }
		public static AgencyError Unknown { get { return new AgencyError(99, "Unknown"); } }

		public static AgencyError ServerUnknown { get { return new AgencyError(199, "Server Unknown"); } }

		public AgencyError(int code, string message)
		{
			ErrorCode = code;
			ErrorMessage = message;
		}

		

		public static AgencyError AgencyDirectoryId { get { return new AgencyError(1, "Agency Profile: Agency Directory ID is required"); } }
		public static AgencyError ProducerCode { get { return new AgencyError(2, "Agency Profile: Producer Code is required"); } }

		public static AgencyError ExistedCompany { get{return new AgencyError(8, "Company already existed. Invalid Insert Command");} }
		public static AgencyError DeletedCompany { get { return new AgencyError(9, "Company had been deleted. Invalid Delete Command"); } }
		public static AgencyError MissingCompany { get { return new AgencyError(14, "Company doesn't exist. Invalid Command"); } }
		public static AgencyError MissingChannel { get { return new AgencyError(18, "Channel doesn't exist. Invalid Command"); } }
		public static AgencyError NoRecordFoundToDelete { get { return new AgencyError(20, "Company doesn't exist. Invalid Delete Command"); } }


		public static AgencyError MissingAgencyLogo { get { return new AgencyError(51, "Agency Logo doesn't exist. Invalid Command"); } }
		public static AgencyError MissingIndustryPrimaryLogo { get { return new AgencyError(52, "Industry Primary Logo doesn't exist. Invalid Command"); } }
		public static AgencyError MissingIndustrySecondaryLogo { get { return new AgencyError(53, "Industry Secondary Logo doesn't exist. Invalid Command"); } }

		public static AgencyError IncorrectAgencyProfilePortTypeCommand { get { return new AgencyError(101, "Incorrect AgencyProfilePortType Command"); } }
		public static AgencyError IncorrectAgencyLogoCommand { get { return new AgencyError(102, "Incorrect AgencyLogo Command"); } }
		public static AgencyError IncorrectIndustryLogoPrimaryCommand { get { return new AgencyError(103, "Incorrect IndustryLogoPrimary Command"); } }
		public static AgencyError IncorrectIndustryLogoSecondaryCommand { get { return new AgencyError(104, "Incorrect IndustryLogoSecondary Command"); } }
		public static AgencyError IncorrectIndustry { get { return new AgencyError(105, "Invalid Industry or Industry hasn't been implemented"); } }

		public static AgencyError WebServiceNotConfigured { get { return new AgencyError(4420, "TIE Kinetix production web service functionality is operational but configuration has not been finalized"); } }


		public static AgencyError RequiredField (int code, string field)
		{
			return new AgencyError(code, string.Format("Agency Profile Insert Function: [{0}] field doesn't exist in the database.", field));
		}
	}

	/// <summary>
	/// AgentError List
	/// </summary>
	public class AgentError : AgencyError
	{
		public AgentError(int code, string message) : base(code, message)
		{
		}

		public new static AgentError AgencyDirectoryId { get { return new AgentError(1, "Agent Profile: Agency Directory ID is required"); } }
		public new static AgentError ProducerCode { get { return new AgentError(2, "Agency Profile: Producer Code is required"); } }
		public static AgentError MissingStaffId { get { return new AgentError(3, "Agent Profile: StaffID is required"); } }

		public static AgentError NotImplemented
		{
			get { return new AgentError(101, "Invalid AgentProfilePortType Command"); }
		}
	}
}