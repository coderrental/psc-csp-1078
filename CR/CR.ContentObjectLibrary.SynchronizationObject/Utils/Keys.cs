﻿namespace CR.ContentObjectLibrary.Synchronization.Utils
{
	public static class Keys
	{
		public static readonly string AgencyDirectoryID = "Agency Directory ID";
		public static readonly string ProducerCode = "ProducerCode";
		public static readonly string MasterProducerCode = "Master Producer Code";
		public static readonly string VIPAgency = "VIP Agency";
		public static readonly string AARPIndicator = "AARP Indicator";
		public static readonly string ServiceCenterType = "Service Center Type";
		public static readonly string MailingAddress = "Mailing Address";
		public static readonly string MailingCityStateZipCode = "Mailing City State Zip Code";
		public static readonly string AgencyEMailAddress = "AgencyEMailAddress";
		//public static readonly string AgencyEMailAddress = "Personalize_Contact_Emailaddress";
		public static readonly string ContactEmailAddress = "Personalize_Contact_Emailaddress";
		public static readonly string ContactPhoneNumber = "Personalize_Contact_Phonenumber";
		public static readonly string SalesLeadEmailAddress = "Sales Lead Email Address";
		public static readonly string AgencyLogo = "Agency Logo";
		public static readonly string IndustryLogoPrimary = "Industry Logo Primary";
		public static readonly string IndustryLogoSecondary = "Industry Logo Secondary";
		public static readonly string StaffId = "StaffID";
		public static readonly string DeletedAgencyPrefix = "DELETED_";
		public static readonly string EbdUserId = "EBDUserID";
		public static string ProfileType = "ProfileType";

		public static readonly string PersonalizeContactJobtitle = "Personalize_Contact_Jobtitle";
		public static readonly string PersonalizeContactFirstname = "Personalize_Contact_Firstname";
		public static readonly string PersonalizeContactLastname = "Personalize_Contact_Lastname";
		public static readonly string PersonalizeContactEmailaddress = "Personalize_Contact_Emailaddress";
		public static readonly string PersonalizeContactPhonenumber = "Personalize_Contact_Phonenumber";
	}
}