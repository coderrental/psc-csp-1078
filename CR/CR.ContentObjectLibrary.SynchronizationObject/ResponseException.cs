﻿using System;
using CR.ContentObjectLibrary.Synchronization.Utils;

namespace CR.ContentObjectLibrary.Synchronization
{
	public class ResponseException : Exception
	{
		private int _errorCode;
		public int ErrorCode
		{
			get { return _errorCode; }
			set { _errorCode = value; }
		}

		private string _errorMessage;
		public string ErrorMessage
		{
			get { return _errorMessage; }
			set { _errorMessage = value; }
		}

		public ResponseException(int errorCode, string errorMessage)
		{
			_errorCode = errorCode;
			_errorMessage = errorMessage;
		}

		public ResponseException(AgencyError error)
		{
			_errorCode = error.ErrorCode;
			_errorMessage = error.ErrorMessage;
		}
	}
}