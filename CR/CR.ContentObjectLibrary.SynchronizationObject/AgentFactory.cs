using System;
using System.Diagnostics;
using System.Linq;
using System.Transactions;
using CR.ContentObjectLibrary.Data;
using CR.ContentObjectLibrary.Synchronization.Utils;
using log4net;

namespace CR.ContentObjectLibrary.Synchronization
{
	public class AgentFactory
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="dataContext"></param>
		/// <param name="logger"></param>
		/// <param name="staffId"></param>
		/// <returns></returns>
		public static company GetCompanyIfExists(CspDataContext dataContext, ILog logger, string staffId)
		{
			var paramType = dataContext.companies_parameter_types.FirstOrDefault(a => a.parametername == Keys.StaffId);
			if (paramType == null)
			{
				dataContext.companies_parameter_types.InsertOnSubmit(new companies_parameter_type()
				{
					parametername = Keys.StaffId,
					parametertype = 1,
					sortorder = 999,
					IsExport = false
				});
				dataContext.SubmitChanges();
				paramType = dataContext.companies_parameter_types.FirstOrDefault(a => a.parametername == Keys.StaffId);
			}

			var parameter = dataContext.companies_parameters.FirstOrDefault(a => a.companies_parameter_types_Id == paramType.companies_parameter_types_Id && a.value_text == staffId);
			if (parameter == null)
			{
				logger.DebugFormat("Unable to find company with the staffId = {0}", staffId);
				return null;
			}

			return parameter.company;
		}

		public static void Insert(CspDataContext dataContext, DnnDataContext dnnDataContext, ILog logger, int portalId, int dnnVer, string dnnUserPassword, string channelIdentifier, 
			int parentCompanyId, company cpn, string agencyDirectoryId, string agentName, string streetAddress, string cityStateZipcode, string agentWebsiteUrl, string producerCode, 
			string masterProducerCode, string vipIndicator, string aarpIndicator, string serviceCenterType, string mailingAddress, string mailingCityStateZipCode, 
			string salesLeadEmailAddress, string phoneNumber, string agentEmailAddress, string industry, string profileType, string staffId, string ebdUserId)
		{
			if (cpn == null)
			{
				using (var transaction = new TransactionScope())
				{
					company cspCompany;

					logger.DebugFormat("Begin creating a company: {0}", agentName);
					using (var subTrans1 = new TransactionScope(TransactionScopeOption.Required))
					{
						Channel channel = dataContext.Channels.FirstOrDefault(c => c.ExternalIdentifier == channelIdentifier);
						if (channel == null)
							throw new ResponseException(AgencyError.MissingChannel);

						cspCompany = new company
						{
							parent_companies_Id = parentCompanyId,
							companyname = agentName,
							displayname = agentName,
							address1 = streetAddress,
							city = cityStateZipcode,
							website_url = agentWebsiteUrl,
							is_consumer = true,
							is_supplier = false,
							created = DateTime.Now,
							date_changed = DateTime.Now
						};

						// Add subscription to this company
						int lngId = dataContext.languages.First().languages_Id;

						cspCompany.companies_consumers.Add(new companies_consumer
						{
							active = true,
							base_domain = DateTime.Now.Ticks.ToString() + "." + channel.BaseDomain,
							base_publication_Id = channel.BasePublicationId,
							base_publication_parameters = channel.DefaultParameters,
							default_language_Id = lngId,
							fallback_language_Id = lngId
						});

						if (string.IsNullOrEmpty(industry))
						{
							foreach (ChannelThemeAssocation channelThemeAssocation in dataContext.ChannelThemeAssocations.Where(a => a.ChannelId == channel.Id))
							{
								cspCompany.companies_themes.Add(new companies_theme { themes_Id = channelThemeAssocation.ThemeId });
							}
						}
						else
						{
							var tempTheme = dataContext.themes.FirstOrDefault(t => t.active && t.externalCode == industry);
							if (tempTheme != null)
							{
								cspCompany.companies_themes.Clear();
								cspCompany.companies_themes.Add(new companies_theme
								{
									themes_Id = tempTheme.themes_Id
								});
							}
							else
								throw new ResponseException(AgencyError.IncorrectIndustry);
						}

						dataContext.companies.InsertOnSubmit(cspCompany);
						dataContext.SubmitChanges();

						InsertOrUpdateParameter(dataContext, logger, Keys.AgencyDirectoryID, agencyDirectoryId, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.ProducerCode, producerCode, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.MasterProducerCode, masterProducerCode, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.VIPAgency, vipIndicator, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.AARPIndicator, aarpIndicator, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.ServiceCenterType, serviceCenterType, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.MailingAddress, mailingAddress, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.MailingCityStateZipCode, mailingCityStateZipCode, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.AgencyEMailAddress, agentEmailAddress, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.ContactEmailAddress, agentEmailAddress, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.ContactPhoneNumber, phoneNumber, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.SalesLeadEmailAddress, salesLeadEmailAddress, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.ProfileType, profileType, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.StaffId, staffId, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.EbdUserId, ebdUserId, cspCompany);


						dataContext.companies_contacts.InsertOnSubmit(new companies_contact
						{
							companies_Id = cspCompany.companies_Id,
							telephone = phoneNumber,
							emailaddress = agentEmailAddress
						});
						dataContext.SubmitChanges();
						subTrans1.Complete();
					}
					logger.DebugFormat("Finished creating a company: {0}", agentName);

					logger.DebugFormat("Begin creating a new dnn user: {0} for {1}", staffId, agentName);
					using (var subTrans2 = new TransactionScope(TransactionScopeOption.RequiresNew))
					{
						CreateDnnUser(dnnDataContext, logger, portalId, dnnVer, dnnUserPassword, cspCompany.companies_Id, staffId, agentName, agentEmailAddress);
						subTrans2.Complete();
					}
					logger.DebugFormat("Finished creating a new dnn user: {0} for {1}", staffId, agentName);

					transaction.Complete();
				} // end of the main transaction scope
			} // end of cpy == null : create a new company
			else
			{
				// if the company exists already and not disable then throw example
				var isActive = cpn.companies_consumers.All(a => a.active.HasValue && a.active.Value);

				if (!isActive)
				{
					// check if this company has been disable, if it is then we reactive the company
					logger.DebugFormat("Begin reactivating company: {0} for {1}", cpn.companies_Id, agentName);
					using (var transaction = new TransactionScope())
					{
					    try
					    {
						    Reactivate(dataContext, dnnDataContext, logger, portalId, dnnVer, cpn, staffId);
						    Update(dataContext, dnnDataContext, logger, portalId, dnnVer, dnnUserPassword,
							    channelIdentifier, parentCompanyId, cpn, agencyDirectoryId, agentName, streetAddress, cityStateZipcode, agentWebsiteUrl, producerCode, masterProducerCode, vipIndicator, aarpIndicator, serviceCenterType, mailingAddress, mailingCityStateZipCode, salesLeadEmailAddress, phoneNumber, agentEmailAddress, industry, profileType, staffId, ebdUserId);
						    transaction.Complete();
					    }
					    catch (Exception ex)
					    {
					        logger.Error(ex.Message, ex);
					        transaction.Dispose();
					        throw new Exception(ex.Message, ex);
					    }
					}
					logger.DebugFormat("End reactivating company: {0} for {1}", cpn.companies_Id, agentName);
				}
				else
				{
					throw new ResponseException(AgentError.ExistedCompany);
				}
			}
		}

		private static void CreateDnnUser(DnnDataContext dnnDataContext, ILog logger, int portalId, int dnnVer, string dnnUserPass, int companyId, string userName, string displayName, string email)
		{
			logger.DebugFormat("Begin creating DotnetNuke user [{0}] in database/portal [{1}].", userName, portalId);
			dnnDataContext.ExecuteCommand("EXEC CreateDnnUser3 {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}", userName, displayName, displayName, displayName, dnnUserPass,
				email, portalId, companyId);
			logger.DebugFormat("End creating DotnetNuke user [{0}] in database/portal [{1}].", userName, portalId);
		}

		private static void InsertOrUpdateParameter(CspDataContext dataContext, ILog logger, string key, string value, company cspCompany)
		{
			logger.DebugFormat("Begin insert/update parameter [{0}] to company [{1}/{2}]", key, cspCompany.companies_Id, cspCompany.companyname);
			companies_parameter_type cpt = dataContext.companies_parameter_types.FirstOrDefault(a => a.parametername == key);
			if (cpt == null)
			{
				cpt = new companies_parameter_type
				{
					parametername = key,
					parametertype = 1,
					sortorder = 0
				};

				logger.DebugFormat("* Create parameter [{0}] doesn't exist in the current database.", key);
				dataContext.companies_parameter_types.InsertOnSubmit(cpt);
				dataContext.SubmitChanges();

				cpt = dataContext.companies_parameter_types.First(a => a.parametername == key);
			}

			if (string.IsNullOrEmpty(value))
				value = string.Empty;

			companies_parameter companiesParameter = cspCompany.companies_parameters.SingleOrDefault(cp => cp.companies_parameter_types_Id == cpt.companies_parameter_types_Id);
			if (companiesParameter != null)
			{
				companiesParameter.value_text = value;
			}
			else
			{
				dataContext.companies_parameters.InsertOnSubmit(new companies_parameter
				{
					companies_Id = cspCompany.companies_Id,
					companies_parameter_types_Id = cpt.companies_parameter_types_Id,
					value_text = value
				});
			}
			dataContext.SubmitChanges();
			logger.DebugFormat("End insert/update parameter [{0}] to company [{1}/{2}]", key, cspCompany.companies_Id, cspCompany.companyname);
		}

		private static void Reactivate(CspDataContext dataContext, DnnDataContext dnnDataContext, ILog logger, int portalId, int dnnVer, company cpn, string staffId)
		{
			using (var transaction = new TransactionScope())
			{
				using (var subTrans1 = new TransactionScope(TransactionScopeOption.Required))
				{
					foreach (companies_consumer companiesConsumer in cpn.companies_consumers)
					{
						companiesConsumer.active = true;
						if (companiesConsumer.base_domain.StartsWith(Keys.DeletedAgencyPrefix))
							companiesConsumer.base_domain = companiesConsumer.base_domain.Replace(Keys.DeletedAgencyPrefix, string.Empty);
					}
					dataContext.SubmitChanges();
					subTrans1.Complete();
					logger.DebugFormat("Re-activate company [{0}/{1}] where staffId = [{2}]", cpn.companies_Id, cpn.companyname, staffId);
				}

				using (var subTrans2 = new TransactionScope(TransactionScopeOption.RequiresNew))
				{
					ReactivateDnnUser(dnnDataContext, logger, portalId, staffId);
					subTrans2.Complete();
				}

				transaction.Complete();
			}
		}

		private static void ReactivateDnnUser(DnnDataContext dnnDataContext, ILog logger, int portalId, string userName)
		{
			dnnDataContext.ExecuteCommand(@"
declare @userId int
SELECT @userId = UserId
FROM vw_Users
WHERE Username = {1} AND PortalId = {0}

UPDATE UserPortals
SET IsDeleted = 0
WHERE PortalId = {0} AND UserID = @userId

", portalId, userName);
			//dnnDataContext.ExecuteCommand("EXEC ReactivateDnnUser3 {0}, {1}", userName, portalId);
		}

		public static void Update(CspDataContext dataContext, DnnDataContext dnnDataContext, ILog logger, int portalId, int dnnVer, string dnnUserPassword, string channelIdentifier, 
			int parentCompanyId, company cpn, string agencyDirectoryId, string agentName, string streetAddress, string cityStateZipcode, string agentWebsiteUrl, string producerCode, 
			string masterProducerCode, string vipIndicator, string aarpIndicator, string serviceCenterType, string mailingAddress, string mailingCityStateZipCode, string salesLeadEmailAddress,
			string phoneNumber, string agentEmailAddress, string industry, string profileType, string staffId, string ebdUserId)
		{
			if (cpn != null)
			{
				logger.DebugFormat("Begin updating a company: {0}", agentName);
				using (var transaction = new TransactionScope())
				{
					using (var subTrans1 = new TransactionScope(TransactionScopeOption.Required))
					{
						cpn.parent_companies_Id = parentCompanyId;
						cpn.companyname = agentName;
						cpn.displayname = agentName;
						cpn.address1 = streetAddress;
						cpn.city = cityStateZipcode;
						cpn.website_url = agentWebsiteUrl;
						//cpn.is_consumer = true;
						//cpn.is_supplier = false;
						cpn.date_changed = DateTime.Now;

						if (!string.IsNullOrEmpty(industry))
						{
							theme tempTheme = dataContext.themes.FirstOrDefault(a => a.active && a.externalCode == industry);
							if (tempTheme == null)
								throw new ResponseException(AgencyError.IncorrectIndustry);

							if (cpn.companies_themes.All(a => a.themes_Id != tempTheme.themes_Id))
							{
								dataContext.companies_themes.DeleteAllOnSubmit(cpn.companies_themes);
								cpn.companies_themes.Add(new companies_theme
								{
									companies_Id = cpn.companies_Id,
									themes_Id = tempTheme.themes_Id
								});
							}
						}

						dataContext.SubmitChanges();

						InsertOrUpdateParameter(dataContext, logger, Keys.AgencyDirectoryID, agencyDirectoryId, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.ProducerCode, producerCode, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.MasterProducerCode, masterProducerCode, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.VIPAgency, vipIndicator, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.AARPIndicator, aarpIndicator, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.ServiceCenterType, serviceCenterType, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.MailingAddress, mailingAddress, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.MailingCityStateZipCode, mailingCityStateZipCode, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.AgencyEMailAddress, agentEmailAddress, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.ContactEmailAddress, agentEmailAddress, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.ContactPhoneNumber, phoneNumber, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.SalesLeadEmailAddress, salesLeadEmailAddress, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.PersonalizeContactFirstname, agentName, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.PersonalizeContactEmailaddress, agentEmailAddress, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.PersonalizeContactPhonenumber, phoneNumber, cpn);
						dataContext.SubmitChanges();
		
						companies_contact ct = dataContext.companies_contacts.FirstOrDefault(a => a.companies_Id == cpn.companies_Id);
						if (ct != null)
						{
							ct.telephone = phoneNumber;
							ct.emailaddress = agentEmailAddress;
							dataContext.SubmitChanges();
						}
						subTrans1.Complete();
					}

					logger.DebugFormat("Finished updating a company: {0}", agentName);

					logger.DebugFormat("Begin updating dnn user {0} for company: {1}", staffId, agentName);
					using (var subTrans2 = new TransactionScope(TransactionScopeOption.RequiresNew))
					{
						UpdateDnnUser(dnnDataContext, logger, portalId, staffId, agentName, agentEmailAddress);
						subTrans2.Complete();
					}
					logger.DebugFormat("Begin updating dnn user {0} for company: {1}", staffId, agentName);

					transaction.Complete();
				}

				// make sure we reactive this user too
				Reactivate(dataContext, dnnDataContext, logger, portalId, dnnVer, cpn, staffId);
			}
			else
			{
				throw new ResponseException(AgencyError.MissingCompany);
			}
		}

		private static void UpdateDnnUser(DnnDataContext dnnDataContext, ILog logger, int portalId, string userName, string displayName, string email)
		{
			var procExists = dnnDataContext.ExecuteQuery<string>("SELECT * FROM sys.objects WHERE name = 'UpdateDnnUser3' AND type = 'P'");
			if (procExists.Any() == false)
			{
				dnnDataContext.ExecuteCommand(
					@"CREATE PROCEDURE [dbo].[UpdateDnnUser3]
@UserName nvarchar(256),
@FirstName nvarchar(100),
@LastName nvarchar(100),
@DisplayName nvarchar(256),
@Email nvarchar(256),
@PortalId int
AS
BEGIN
	DECLARE @ASPNETApplicationId uniqueidentifier
	DECLARE @ASPNETUserId uniqueidentifier
	DECLARE @UserId int

	SELECT @ASPNETApplicationId = ApplicationId
	FROM aspnet_Applications
	WHERE ApplicationName = 'DotNetNuke'

	SELECT @ASPNETUserId = UserId
	FROM aspnet_Users
	WHERE ApplicationId = @ASPNETApplicationId AND UserName = @UserName

	UPDATE aspnet_Membership
	SET Email = @Email, LoweredEmail = LOWER(@Email)
	WHERE ApplicationId = @ASPNETApplicationId AND UserId = @ASPNETUserId
	
	SELECT @UserId = UserId
	FROM vw_Users
	WHERE Username = @UserName AND PortalId = @PortalId

	UPDATE Users
	SET Username = @UserName, FirstName = @FirstName, LastName = @LastName, DisplayName = @DisplayName,
		Email = @Email, LastModifiedOnDate = GETDATE()
	WHERE UserID = @UserId
END");
			}

			logger.DebugFormat("Begin Update DotnetNuke user in Database.");
			dnnDataContext.ExecuteCommand("EXEC UpdateDnnUser3 {0}, {1}, {2}, {3}, {4}, {5}", userName, displayName, displayName, displayName, email, portalId);
			logger.DebugFormat("Updated DotnetNuke user in Database.");

		}

		public static void Delete(CspDataContext dataContext, DnnDataContext dnnDataContext, ILog logger, int portalId, company cpn, string staffId)
		{
			if (cpn != null)
			{
				var isActive = cpn.companies_consumers.All(a=>a.active.HasValue&&a.active.Value);

				using (var transaction = new TransactionScope())
				{
					if (isActive)
					{
						logger.DebugFormat("Begin deleting company: {0} with staffId: {1}", cpn.companies_Id, staffId);
						using (var subTrans1 = new TransactionScope(TransactionScopeOption.Required))
						{
							foreach (companies_consumer companiesConsumer in cpn.companies_consumers)
							{
								if (companiesConsumer.active.HasValue && companiesConsumer.active.Value)
								{
									companiesConsumer.active = false;
									if (!companiesConsumer.base_domain.StartsWith(Keys.DeletedAgencyPrefix))
										companiesConsumer.base_domain = Keys.DeletedAgencyPrefix + companiesConsumer.base_domain;
								}
							}
							dataContext.SubmitChanges();
							subTrans1.Complete();
						}
						logger.DebugFormat("Finished deleting company: {0} with staffId: {1}", cpn.companies_Id, staffId);
					}

					// keep this routine here to make sure the dnn user is deleted.

					logger.DebugFormat("Begin deleteing user: {0}", staffId);
					using (var subTrans2 = new TransactionScope(TransactionScopeOption.RequiresNew))
					{
						DeleteDnnUser(dnnDataContext, logger, portalId, staffId);
						subTrans2.Complete();
					}
					logger.DebugFormat("Finished deleteing user: {0}", staffId);
					transaction.Complete();
				}

				if (!isActive)
				{
					throw new ResponseException(AgentError.NoRecordFoundToDelete);
				}
			}
			else
			{
				throw new ResponseException(AgencyError.NoRecordFoundToDelete);
			}
		}

		private static void DeleteDnnUser(DnnDataContext dnnDataContext, ILog logger, int portalId, string userName)
		{
			var procExists = dnnDataContext.ExecuteQuery<string>("SELECT * FROM sys.objects WHERE name = 'DeleteDnnUser3' AND type = 'P'");
			if (procExists.Any() == false)
			{
				dnnDataContext.ExecuteCommand(
					@"CREATE PROCEDURE [dbo].[DeleteDnnUser3]
@UserName nvarchar(256),
@PortalId int
AS
BEGIN
	DECLARE @UserId int

	SELECT @UserId = UserId
	FROM vw_Users
	WHERE Username = @UserName AND PortalId = @PortalId

	UPDATE UserPortals
	SET IsDeleted = 1
	WHERE PortalId = @PortalId AND UserID = @UserId
END");
			}
			logger.DebugFormat("Begin deleting DotnetNuke user in Database.");
			dnnDataContext.ExecuteCommand("EXEC DeleteDnnUser3 {0}, {1}", userName, portalId);
			logger.DebugFormat("Deleted DotnetNuke user in Database.");

		}

		#region Helper Functions
		private static void DebugFormat(ILog logger, string text, params object[] values)
		{
			if (logger != null)
				logger.DebugFormat(text, values);
		}
		#endregion
	}
}