﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Web.Security;
using CR.ContentObjectLibrary.Data;
using CR.ContentObjectLibrary.Synchronization.Utils;
using log4net;

namespace CR.ContentObjectLibrary.Synchronization
{
	public class AgencyFactory
	{
		/// <summary>
		/// Check information about the company already exists in the database or not.
		/// </summary>
		/// <param name="dataContext">Data Context of CSP Database.</param>
		/// <param name="logger">The logger for operations.</param>
		/// <param name="agencyDirectoryId">User identifier associated with the information about the company.</param>
		public static company IsCompanyExists(CspDataContext dataContext, ILog logger, string agencyDirectoryId)
		{
			company cpn = null;
			if (logger != null) logger.DebugFormat("Agency Function: Check if {0}: \"{1}\" already exists in the Database or not.", Keys.AgencyDirectoryID, agencyDirectoryId);
			companies_parameter cp = dataContext.companies_parameters.FirstOrDefault(a => a.value_text == agencyDirectoryId && a.companies_parameter_type.parametername == Keys.AgencyDirectoryID);
			if (cp != null) cpn = cp.company;
			if (logger != null) logger.DebugFormat("Agency Function: Retrieved company's AgencyDirectoryID [{0}].", Keys.AgencyDirectoryID);

			return cpn;
		}
		

		/// <summary>
		/// Insert Agency Profile Port Type to Database, and create DotnetNuke user for it.
		/// </summary>
		/// <param name="dataContext">Data Context of CSP Database.</param>
		/// <param name="dnnDataContext">Data Context of DotNetNuke Database.</param>
		/// <param name="logger">The logger for operations.</param>
		/// <param name="portalId">ID of Portal which the User belongs.</param>
		/// <param name="dnnVer">Version of DotnetNuke is current used.</param>
		/// <param name="dnnUserPass">Password of DotnetNuke User has created.</param>
		/// <param name="channelIdentifier">Identifier of Channel which the User belongs.</param>
		/// <param name="parentCompanyId">Parent ID of the respective companies with users in the database.</param>
		/// <param name="cpn">Object Company corresponding to the User has been created.</param>
		/// <param name="agencyDirectoryId">ID of Agency.</param>
		/// <param name="agencyName">Name of Agency.</param>
		/// <param name="streetAddress">Address of Agency.</param>
		/// <param name="cityStateZipCode">ZIP code of Agency.</param>
		/// <param name="websiteURL">Website address of Agency.</param>
		/// <param name="producerCode">Producer code of Agency.</param>
		/// <param name="masterProducerCode">Master Producer code of Agency.</param>
		/// <param name="VIPAgency">Indicator VIP Agency.</param>
		/// <param name="AARPIndicator">Indicator AARP of Agency.</param>
		/// <param name="serviceCenterType">Indicator Service Centert Type of Agency.</param>
		/// <param name="mailingAddress">Email of Agency.</param>
		/// <param name="mailingCityStateZipCode">Email ZIP code of Agency.</param>
		/// <param name="salesLeadEmailAddress">Lead email of Agency.</param>
		/// <param name="phoneNumber">Phone number of Agency.</param>
		/// <param name="agencyEmailAddress">Contact email of Agency.</param>
		/// <param name="industry"></param>
		/// <param name="profileType"></param>
		public static void InsertAgencyProfile(CspDataContext dataContext, DnnDataContext dnnDataContext, ILog logger, int portalId, int dnnVer, string dnnUserPass, string channelIdentifier, int parentCompanyId, company cpn, string agencyDirectoryId, string agencyName, string streetAddress, string cityStateZipCode, string websiteURL, string producerCode, string masterProducerCode, string VIPAgency, string AARPIndicator, string serviceCenterType, string mailingAddress, string mailingCityStateZipCode, string salesLeadEmailAddress, string phoneNumber, string agencyEmailAddress, string industry, 
			string profileType)
		{
			if (cpn == null)
			{
				using (TransactionScope transaction = new TransactionScope())
				{
					company cspCompany;

					using (TransactionScope subTrans1 = new TransactionScope(TransactionScopeOption.Required))
					{
						if (logger != null) logger.Debug("Agency Profile Insert Function: Begin getting system's channel.");
						Channel channel = dataContext.Channels.FirstOrDefault(c => c.ExternalIdentifier == channelIdentifier);
						if (channel == null)
							throw new ResponseException(AgencyError.MissingChannel);
						if (logger != null) logger.DebugFormat("Agency Profile Insert Function: Received the Channel [{0}/{1}].", channel.Id, channel.Name);

						cspCompany = new company
						{
							parent_companies_Id = parentCompanyId,
							//companyname = agencyDirectoryId,
							companyname = agencyName,
							displayname = agencyName,
							address1 = streetAddress,
							//zipcode = cityStateZipCode,
							city = cityStateZipCode,
							website_url = websiteURL,
							is_consumer = true,
							is_supplier = false,
							created = DateTime.Now,
							date_changed = DateTime.Now
						};

						// Add subscription to this company
						if (logger != null) logger.DebugFormat("Agency Profile Insert Function: Begin adding subscriptions to company [{0}/{1}]", cspCompany.companies_Id, cspCompany.companyname);
						int lngId = dataContext.languages.First().languages_Id;

						cspCompany.companies_consumers.Add(new companies_consumer
						{
							active = true,
							base_domain = DateTime.Now.Ticks.ToString() + "." + channel.BaseDomain,
							base_publication_Id = channel.BasePublicationId,
							base_publication_parameters = channel.DefaultParameters,
							default_language_Id = lngId,
							fallback_language_Id = lngId
						});

						if (string.IsNullOrEmpty(industry))
						{
							foreach (ChannelThemeAssocation channelThemeAssocation in dataContext.ChannelThemeAssocations.Where(a => a.ChannelId == channel.Id))
							{
								cspCompany.companies_themes.Add(new companies_theme {themes_Id = channelThemeAssocation.ThemeId});
							}
						}
						else
						{
							theme tempTheme = dataContext.themes.FirstOrDefault(t => t.active && t.externalCode == industry);
							if (tempTheme != null)
							{
								cspCompany.companies_themes.Clear();
								cspCompany.companies_themes.Add(new companies_theme
								                                	{
								                                		themes_Id = tempTheme.themes_Id
								                                	});
							}
							else
								throw new ResponseException(AgencyError.IncorrectIndustry);
						}

						if (logger != null) logger.DebugFormat("Agency Profile Insert Function: Added subscriptions to company [{0}/{1}]", cspCompany.companies_Id, cspCompany.companyname);

						if (logger != null) logger.DebugFormat("Agency Profile Insert Function: Begin inserting new company [{0}/{1}] in to Database.", cspCompany.companies_Id, cspCompany.companyname);
						dataContext.companies.InsertOnSubmit(cspCompany);
						dataContext.SubmitChanges();
						if (logger != null) logger.DebugFormat("Agency Profile Insert Function: Inserted new company [{0}/{1}] in to Database.", cspCompany.companies_Id, cspCompany.companyname);

						if (logger != null) logger.DebugFormat("Agency Profile Insert Function: Begin inserting parameters of company [{0}/{1}].", cspCompany.companies_Id, cspCompany.companyname);
						InsertOrUpdateParameter(dataContext, logger, Keys.AgencyDirectoryID, agencyDirectoryId, DbFieldType.StringType, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.ProducerCode, producerCode, DbFieldType.StringType, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.MasterProducerCode, masterProducerCode, DbFieldType.StringType, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.VIPAgency, VIPAgency, DbFieldType.StringType, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.AARPIndicator, AARPIndicator, DbFieldType.StringType, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.ServiceCenterType, serviceCenterType, DbFieldType.StringType, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.MailingAddress, mailingAddress, DbFieldType.StringType, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.MailingCityStateZipCode, mailingCityStateZipCode, DbFieldType.StringType, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.AgencyEMailAddress, agencyEmailAddress, DbFieldType.StringType, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.ContactEmailAddress, agencyEmailAddress, DbFieldType.StringType, cspCompany);
                        InsertOrUpdateParameter(dataContext, logger, Keys.ContactPhoneNumber, phoneNumber, DbFieldType.StringType, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.SalesLeadEmailAddress, salesLeadEmailAddress, DbFieldType.StringType, cspCompany);
						InsertOrUpdateParameter(dataContext, logger, Keys.ProfileType, profileType, DbFieldType.StringType, cspCompany);

						if (logger != null) logger.DebugFormat("Agency Profile Insert Function: Inserted insert parameters of company [{0}/{1}].", cspCompany.companies_Id, cspCompany.companyname);

						if (logger != null) logger.DebugFormat("Agency Profile Insert Function: Begin inserting new Company Contact fields \"Phone Number, Agency Email Address\" of company [{0}/{1}] in to Database.",
							cspCompany.companies_Id, cspCompany.companyname);
						dataContext.companies_contacts.InsertOnSubmit(new companies_contact
						{
							companies_Id = cspCompany.companies_Id,
							telephone = phoneNumber,
							emailaddress = agencyEmailAddress
						});
						if (logger != null) logger.DebugFormat("Agency Profile Insert Function: Inserted new Company Contact fields \"Phone Number, Agency Email Address\" of company [{0}/{1}] in to Database.",
							cspCompany.companies_Id, cspCompany.companyname);

						if (logger != null) logger.DebugFormat("Agency Profile Insert Function: Begin inserting fields of company [{0}/{1}] in to Database.", cspCompany.companies_Id, cspCompany.companyname);
						dataContext.SubmitChanges();
						if (logger != null) logger.DebugFormat("Agency Profile Insert Function: Submitted fields of company [{0}/{1}] in to Database.", cspCompany.companies_Id, cspCompany.companyname);

						subTrans1.Complete();
					}

					using (TransactionScope subTrans2 = new TransactionScope(TransactionScopeOption.RequiresNew))
					{
						if (logger != null) logger.DebugFormat("Agency Profile Insert Function: Begin creating a user for company [{0}/{1}] in DotnetNuke Database.", cspCompany.companies_Id, cspCompany.companyname);
						CreateDnnUser(dnnDataContext, logger, portalId, dnnVer, dnnUserPass, cspCompany.companies_Id, agencyDirectoryId, agencyName, agencyEmailAddress);
						if (logger != null) logger.DebugFormat("Agency Profile Insert Function: Created a user for company [{0}/{1}] in DotnetNuke Database.", cspCompany.companies_Id, cspCompany.companyname);

						subTrans2.Complete();
					}

					transaction.Complete();
				}
			}
			else
			{
				bool isActivated = false;
				foreach (companies_consumer companiesConsumer in cpn.companies_consumers)
				{
					if (companiesConsumer.active == true) isActivated = true;
				}

				if (isActivated == false)
				{
					// Update user.
					ReactivateAgencyProfile(dataContext, dnnDataContext, logger, portalId, cpn, agencyDirectoryId);
					UpdateAgencyProfile(dataContext, dnnDataContext, logger, portalId, parentCompanyId, cpn, agencyDirectoryId, agencyName, streetAddress,
						cityStateZipCode, websiteURL, producerCode, masterProducerCode, VIPAgency, AARPIndicator, serviceCenterType, mailingAddress,
						mailingCityStateZipCode, salesLeadEmailAddress, phoneNumber, agencyEmailAddress, industry, profileType);
				}
				else throw new ResponseException(AgencyError.ExistedCompany);
			}
		}

		/// <summary>
		/// Update Agency Profile Port Type in Database, and update DotnetNuke user for it.
		/// </summary>
		/// <param name="dataContext">Data Context of CSP Database.</param>
		/// <param name="dnnDataContext">Data Context of DotNetNuke Database.</param>
		/// <param name="logger">The logger for operations.</param>
		/// <param name="portalId">ID of Portal which the User belongs.</param>
		/// <param name="parentCompanyId">Parent ID of the respective companies with users in the database.</param>
		/// <param name="cpn">Object Company corresponding to the User has been created.</param>
		/// <param name="agencyDirectoryId">ID of Agency.</param>
		/// <param name="agencyName">Name of Agency.</param>
		/// <param name="streetAddress">Address of Agency.</param>
		/// <param name="cityStateZipCode">ZIP code of Agency.</param>
		/// <param name="websiteURL">Website address of Agency.</param>
		/// <param name="producerCode">Producer code of Agency.</param>
		/// <param name="masterProducerCode">Master Producer code of Agency.</param>
		/// <param name="VIPAgency">Indicator VIP Agency.</param>
		/// <param name="AARPIndicator">Indicator AARP of Agency.</param>
		/// <param name="serviceCenterType">Indicator Service Centert Type of Agency.</param>
		/// <param name="mailingAddress">Email of Agency.</param>
		/// <param name="mailingCityStateZipCode">Email ZIP code of Agency.</param>
		/// <param name="salesLeadEmailAddress">Lead email of Agency.</param>
		/// <param name="phoneNumber">Phone number of Agency.</param>
		/// <param name="agencyEmailAddress">Contact email of Agency.</param>
		/// <param name="industry"></param>
		public static void UpdateAgencyProfile(CspDataContext dataContext, DnnDataContext dnnDataContext, ILog logger, int portalId, int parentCompanyId, company cpn, 
			string agencyDirectoryId, string agencyName, string streetAddress, string cityStateZipCode, string websiteURL, string producerCode, string masterProducerCode, 
			string VIPAgency, string AARPIndicator, string serviceCenterType, string mailingAddress, string mailingCityStateZipCode, string salesLeadEmailAddress, 
			string phoneNumber, string agencyEmailAddress, string industry, string profileType)
		{
			if (cpn != null)
			{
				using (TransactionScope transaction = new TransactionScope())
				{
					using (TransactionScope subTrans1 = new TransactionScope(TransactionScopeOption.Required))
					{
						if (logger != null) logger.DebugFormat("Agency Profile Update Function: Begin updating company [{0}/{1}] in Database.", cpn.companies_Id, cpn.companyname);
						cpn.parent_companies_Id = parentCompanyId;
						//cpn.companyname = agencyDirectoryId;
						cpn.companyname = agencyName;
						cpn.displayname = agencyName;
						cpn.address1 = streetAddress;
						//cpn.zipcode = cityStateZipCode;
						cpn.city = cityStateZipCode;
						cpn.website_url = websiteURL;
						cpn.is_consumer = true;
						cpn.is_supplier = false;
						cpn.date_changed = DateTime.Now;

						if (!string.IsNullOrEmpty(industry))
						{
							theme tempTheme = dataContext.themes.FirstOrDefault(a => a.active && a.externalCode == industry);
							if (tempTheme == null)
								throw new ResponseException(AgencyError.IncorrectIndustry);
							if (tempTheme != null && !cpn.companies_themes.Any(a => a.themes_Id == tempTheme.themes_Id))
							{
								dataContext.companies_themes.DeleteAllOnSubmit(cpn.companies_themes);
								cpn.companies_themes.Add(new companies_theme
								                         	{
								                         		companies_Id = cpn.companies_Id,
																themes_Id = tempTheme.themes_Id
								                         	});
								if (logger != null) logger.DebugFormat("Update theme {0} for company {1}", industry, cpn.companyname + " / " + cpn.companies_Id);
							}
						}

						dataContext.SubmitChanges();
						if (logger != null) logger.DebugFormat("Agency Profile Update Function: Submitted update company [{0}/{1}] in Database.", cpn.companies_Id, cpn.companyname);

						if (logger != null) logger.DebugFormat("Agency Profile Update Function: Begin updating parameters of company [{0}/{1}].", cpn.companies_Id, cpn.companyname);
						InsertOrUpdateParameter(dataContext, logger, Keys.AgencyDirectoryID, agencyDirectoryId, DbFieldType.StringType, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.ProducerCode, producerCode, DbFieldType.StringType, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.MasterProducerCode, masterProducerCode, DbFieldType.StringType, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.VIPAgency, VIPAgency, DbFieldType.StringType, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.AARPIndicator, AARPIndicator, DbFieldType.StringType, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.ServiceCenterType, serviceCenterType, DbFieldType.StringType, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.MailingAddress, mailingAddress, DbFieldType.StringType, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.MailingCityStateZipCode, mailingCityStateZipCode, DbFieldType.StringType, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.AgencyEMailAddress, agencyEmailAddress, DbFieldType.StringType, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.ContactEmailAddress, agencyEmailAddress, DbFieldType.StringType, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.ContactPhoneNumber, phoneNumber, DbFieldType.StringType, cpn);
						InsertOrUpdateParameter(dataContext, logger, Keys.SalesLeadEmailAddress, salesLeadEmailAddress, DbFieldType.StringType, cpn);
						if (logger != null) logger.DebugFormat("Agency Profile Update Function: Updated parameters of company [{0}/{1}].", cpn.companies_Id, cpn.companyname);

						if (logger != null) logger.DebugFormat("Agency Profile Update Function: Begin updating fields of company [{0}/{1}] in Database.", cpn.companies_Id, cpn.companyname);
						dataContext.SubmitChanges();
						if (logger != null) logger.DebugFormat("Agency Profile Update Function: Submitted fields of company [{0}/{1}] in Database.", cpn.companies_Id, cpn.companyname);

						companies_contact ct = dataContext.companies_contacts.FirstOrDefault(a => a.companies_Id == cpn.companies_Id);
						if (ct != null)
						{
							ct.telephone = phoneNumber;
							ct.emailaddress = agencyEmailAddress;
							if (logger != null) logger.DebugFormat("Agency Profile Update Function: Begin updating Company Contact fields: \"Phone Number, Agency Email Address\" of company [{0}/{1}] in Database.",
								cpn.companies_Id, cpn.companyname);
							dataContext.SubmitChanges();
							if (logger != null) logger.DebugFormat("Agency Profile Update Function: Submitted fields of company [{0}/{1}] in Database.", cpn.companies_Id, cpn.companyname);
						}
						else
							if (logger != null) logger.DebugFormat("Agency Profile Update Function: Coundn't find Company Contact fields \"Phone Number, Agency Email Address\" of company [{0}/{1}] in Database.",
								cpn.companies_Id, cpn.companyname);

						subTrans1.Complete();
					}

					using (TransactionScope subTrans2 = new TransactionScope(TransactionScopeOption.RequiresNew))
					{
						if (logger != null) logger.DebugFormat("Agency Profile Update Function: Begin updating existed user of company [{0}/{1}] in Dnn Database.", cpn.companies_Id, cpn.companyname);
						UpdateDnnUser(dnnDataContext, logger, portalId, agencyDirectoryId, agencyName, agencyEmailAddress);
						if (logger != null) logger.DebugFormat("Agency Profile Update Function: Updated existing user of company [{0}/{1}] in Dnn Database.", cpn.companies_Id, cpn.companyname);

						subTrans2.Complete();
					}

					transaction.Complete();
				}

				ReactivateAgencyProfile(dataContext, dnnDataContext, logger, portalId, cpn, agencyDirectoryId);
			}
			else
			{
				throw new ResponseException(AgencyError.MissingCompany);
			}
		}

		/// <summary>
		/// Delete Agency Profile Port Type in Database, and delete DotnetNuke user for it.
		/// </summary>
		/// <param name="dataContext">Data Context of CSP Database.</param>
		/// <param name="dnnDataContext">Data Context of DotNetNuke Database.</param>
		/// <param name="logger">The logger for operations.</param>
		/// <param name="portalId">ID of Portal which the User belongs.</param>
		/// <param name="cpn">Object Company corresponding to the User has been created.</param>
		/// <param name="agencyDirectoryId">ID of Agency.</param>
		public static void DeleteAgencyProfile(CspDataContext dataContext, DnnDataContext dnnDataContext, ILog logger, int portalId, company cpn, string agencyDirectoryId)
		{
			if (cpn != null)
			{
				var isActive = cpn.companies_consumers.All(a => a.active.HasValue && a.active.Value);

				if (isActive)
				{
					using (TransactionScope transaction = new TransactionScope())
					{
						using (TransactionScope subTrans1 = new TransactionScope(TransactionScopeOption.Required))
						{
							if (logger != null) logger.DebugFormat("Agency Profile Delete Function: Begin deactiving Company Consumer fields of company [{0}/{1}] in Database.", cpn.companies_Id, cpn.companyname);
							foreach (companies_consumer companiesConsumer in cpn.companies_consumers)
							{
								if (companiesConsumer.active.HasValue && companiesConsumer.active.Value)
								{
									companiesConsumer.active = false;
									if (!companiesConsumer.base_domain.StartsWith(Keys.DeletedAgencyPrefix))
										companiesConsumer.base_domain = Keys.DeletedAgencyPrefix + companiesConsumer.base_domain;
								}
							}
							dataContext.SubmitChanges();
							//if(count == cpn.companies_consumers.Count) throw new ResponseException(AgencyError.DeletedCompany);

							if (logger != null) logger.DebugFormat("Agency Profile Delete Function: Completed deactiving Company Consumer fields of company [{0}/{1}] in Database.", cpn.companies_Id, cpn.companyname);

							subTrans1.Complete();
						}

						using (TransactionScope subTrans2 = new TransactionScope(TransactionScopeOption.RequiresNew))
						{
							if (logger != null) logger.DebugFormat("Agency Profile Delete Function: Begin deleting existed user of company [{0}/{1}] in Dnn Database.", cpn.companies_Id, cpn.companyname);
							DeleteDnnUser(dnnDataContext, logger, portalId, agencyDirectoryId);
							if (logger != null) logger.DebugFormat("Agency Profile Delete Function: Deleted existing user of company [{0}/{1}] in Dnn Database.", cpn.companies_Id, cpn.companyname);

							subTrans2.Complete();
						}

						transaction.Complete();
					}
				}
				else
					throw new ResponseException(AgencyError.NoRecordFoundToDelete);
			}
			else
			{
				throw new ResponseException(AgencyError.NoRecordFoundToDelete);
			}
		}

		/// <summary>
		/// Reactivate Agency Profile Port Type in Database.
		/// </summary>
		/// <param name="dataContext">Data Context of CSP Database.</param>
		/// <param name="dnnDataContext">Data Context of DotNetNuke Database.</param>
		/// <param name="logger">The logger for operations.</param>
		/// <param name="portalId">ID of Portal which the User belongs.</param>
		/// <param name="cpn">Object Company corresponding to the User has been created.</param>
		/// <param name="agencyDirectoryId">ID of Agency.</param>
		private static void ReactivateAgencyProfile(CspDataContext dataContext, DnnDataContext dnnDataContext, ILog logger, int portalId, company cpn, string agencyDirectoryId)
		{
			using (TransactionScope transaction = new TransactionScope())
			{
				using (TransactionScope subTrans1 = new TransactionScope(TransactionScopeOption.Required))
				{
					if (logger != null) logger.DebugFormat("Agency Profile Function: Begin reactivate Company Consumer fields of company [{0}/{1}] in Database.", cpn.companies_Id, cpn.companyname);
					foreach (companies_consumer companiesConsumer in cpn.companies_consumers)
					{
						companiesConsumer.active = true;
						if (companiesConsumer.base_domain.StartsWith(Keys.DeletedAgencyPrefix))
							companiesConsumer.base_domain = companiesConsumer.base_domain.Replace(Keys.DeletedAgencyPrefix, string.Empty);
					}
					dataContext.SubmitChanges();
					if (logger != null) logger.DebugFormat("Agency Profile Function: Completed reactivate Company Consumer fields of company [{0}/{1}] in Database.", cpn.companies_Id, cpn.companyname);

					subTrans1.Complete();
				}

				using (TransactionScope subTrans2 = new TransactionScope(TransactionScopeOption.RequiresNew))
				{
					if (logger != null) logger.DebugFormat("Agency Profile Function: Begin reactivate existed user of company [{0}/{1}] in Dnn Database.", cpn.companies_Id, cpn.companyname);
					ReactivateDnnUser(dnnDataContext, logger, portalId, agencyDirectoryId);
					if (logger != null) logger.DebugFormat("Agency Profile Function: Complete reactivate user of company [{0}/{1}] in Dnn Database.", cpn.companies_Id, cpn.companyname);

					subTrans2.Complete();
				}

				transaction.Complete();
			}
		}

		/// <summary>
		/// Create or Update Logo in Database.
		/// </summary>
		/// <param name="dataContext">Data Context of CSP Database.</param>
		/// <param name="logger">The logger for operations.</param>
		/// <param name="logoData">Raw data of the Logo.</param>
		/// <param name="urlPath">Url to the logo file on the server.</param>
		/// <param name="path">Path to the logo file on the server.</param>
		/// <param name="cpn">Object Company corresponding to the User has been created.</param>
		public static void CreateOrUpdateLogo(CspDataContext dataContext, ILog logger, byte[] logoData, string logoFieldName, string urlPath, string path, company cpn)
		{
			if (logoData != null && logoData.Length > 0)
			{
				string filePath, fileName;
				MemoryStream imgStream;
				Image image;

				DeleteOldLogoFile(dataContext, cpn, logoFieldName, path);

				using (TransactionScope transaction = new TransactionScope())
				{
					string agencyDirectoryId = cpn.companies_parameters.FirstOrDefault(a => a.companies_parameter_type.parametername == Keys.AgencyDirectoryID).value_text;

					if (logger != null) logger.DebugFormat("Agency Logo Upload Function: Begin creating {0} file of company [{1}/{2}].", logoFieldName, cpn.companies_Id, cpn.companyname);
					fileName = logoFieldName.Replace(" ", string.Empty) + "_" + DateTime.Now.Ticks.ToString() + ".gif";
					filePath = path + "\\" + fileName;
					imgStream = new MemoryStream(logoData);
					image = Image.FromStream(imgStream);
					image.Save(filePath, ImageFormat.Gif);
					if (logger != null) 
						logger.DebugFormat("Agency Logo Upload Function: Created {0} file of company [{1}/{2}].", logoFieldName, cpn.companies_Id, cpn.companyname);

					if (logger != null) logger.DebugFormat("Agency Logo Upload Function: Begin inserting / updating field contained {0} of company [{1}/{2}] in to Database.",
						logoFieldName, cpn.companies_Id, cpn.companyname);
					InsertOrUpdateParameter(dataContext, logger, logoFieldName, urlPath + agencyDirectoryId + '/' + fileName, DbFieldType.StringType, cpn);
					dataContext.SubmitChanges();
					if (logger != null) logger.DebugFormat("Agency Logo Upload Function: Submited inserting / updating field contained {0} of company [{1}/{2}] in to Database.",
						logoFieldName, cpn.companies_Id, cpn.companyname);

					transaction.Complete();
				}
			}
			else
			{
				throw new ResponseException(AgencyError.MissingAgencyLogo);
			}
		}

		/// <summary>
		/// Delete Logo in Database.
		/// </summary>
		/// <param name="dataContext">Data Context of CSP Database.</param>
		/// <param name="logger">The logger for operations.</param>
		/// <param name="cpn">Object Company corresponding to the User has been created.</param>
		/// <param name="path">Path to the logo file on the server.</param>
		public static void DeleteLogo(CspDataContext dataContext, ILog logger, company cpn, string logoFieldName, string path)
		{
			using (TransactionScope transaction = new TransactionScope())
			{
				if (logger != null) logger.DebugFormat("Agency Logo Upload Function: Begin deleting field contained {0} of company [{1}/{2}] in to Database.",
					logoFieldName, cpn.companies_Id, cpn.companyname);
				DeleteOldLogoFile(dataContext, cpn, logoFieldName, path);
				DeleteParameter(dataContext, logger, logoFieldName, cpn);
				if (logger != null) logger.DebugFormat("Agency Logo Upload Function: Deleted field contained {0} of company [{1}/{2}] in to Database.",
					logoFieldName, cpn.companies_Id, cpn.companyname);

				transaction.Complete();
			}
		}

		/// <summary>
		/// Delete Old Logo file on disk.
		/// </summary>
		/// <param name="dataContext">Data Context of CSP Database.</param>
		/// <param name="cpn">Object Company corresponding to the User has been created.</param>
		/// <param name="path">Path to the logo file on the server.</param>
		private static void DeleteOldLogoFile(CspDataContext dataContext, company cpn, string logoFieldName, string path)
		{
			companies_parameter_type cpt = dataContext.companies_parameter_types.FirstOrDefault(a => a.parametername == logoFieldName);
			if (cpt == null) throw new ResponseException(AgencyError.RequiredField(3, logoFieldName));
			companies_parameter companiesParameter = cpn.companies_parameters.FirstOrDefault(cp => cp.companies_parameter_types_Id == cpt.companies_parameter_types_Id);
			if (companiesParameter != null)
			{
				String value = companiesParameter.value_text;
				int lastIndex = value.LastIndexOf('/') + 1;
				value = value.Substring(lastIndex, value.Length - lastIndex);
				FileInfo fileInfo = new FileInfo(path + "\\" + value);
				if (fileInfo != null) fileInfo.Delete();
			}
		}

		/// <summary>
		/// Insert or Update Paramter.
		/// </summary>
		/// <param name="dataContext">Data Context of CSP Database.</param>
		/// <param name="logger">The logger for operations.</param>
		/// <param name="fieldName">Field Name to Insert / Update.</param>
		/// <param name="fieldValue">Value.</param>
		/// <param name="fieldType">Type of Field.</param>
		/// <param name="cspCompany">Company Record.</param>
		private static void InsertOrUpdateParameter(CspDataContext dataContext, ILog logger, string fieldName, string fieldValue, DbFieldType fieldType, company cspCompany)
		{
			if (logger != null) logger.DebugFormat("Agency Profile Insert/Update Function: Begin find \"{0}\" parameter type field in Database.", fieldName);
			companies_parameter_type cpt = dataContext.companies_parameter_types.FirstOrDefault(a => a.parametername == fieldName);
			if (cpt == null)
			{
				if (logger != null) logger.DebugFormat("Agency Profile Insert/Update Function: Not found \"{0}\" parameter type field in Database. Create it.", fieldName);
				cpt = new companies_parameter_type
				{
					parametername = fieldName,
					parametertype = (int)fieldType,
					sortorder = 0
				};
				dataContext.companies_parameter_types.InsertOnSubmit(cpt);
				dataContext.SubmitChanges();
				if (logger != null) logger.DebugFormat("Agency Profile Insert/Update Function: Created \"{0}\" parameter type field in Database.", fieldName);
			}
			else if (logger != null) logger.DebugFormat("Agency Profile Insert/Update Function: Found \"{0}\" parameter type field in Database.", fieldName);

			if (string.IsNullOrEmpty(fieldValue))
				fieldValue = string.Empty;
			if (logger != null) logger.DebugFormat("Agency Profile Insert/Update Function: Begin find value of \"{0}\" parameter type field in Database.", fieldName);
			companies_parameter companiesParameter = cspCompany.companies_parameters.SingleOrDefault(cp => cp.companies_parameter_types_Id == cpt.companies_parameter_types_Id);
			if (companiesParameter != null)
			{
				if (logger != null) logger.DebugFormat("Agency Profile Insert/Update Function: Found value of \"{0}\" parameter type field in Database. Update new value of it.", fieldName);
				if (logger != null) logger.DebugFormat("Agency Profile Insert/Update Function: Begin update [{0}] parameter to [{1}]", fieldName, fieldValue);
				companiesParameter.value_text = fieldValue;
				if (logger != null) logger.DebugFormat("Agency Profile Insert/Update Function: Updated [{0}] parameter to [{1}]", fieldName, fieldValue);
			}
			else
			{
				if (logger != null) logger.DebugFormat("Agency Profile Insert/Update Function: Not found value of \"{0}\" parameter type field in Database. Create new value for it", fieldName);
				if (logger != null) logger.DebugFormat("Agency Profile Insert/Update Function: Begin insert [{0}] parameter, value = [{1}]", fieldName, fieldValue);
				dataContext.companies_parameters.InsertOnSubmit(new companies_parameter
				{
					companies_Id = cspCompany.companies_Id,
					companies_parameter_types_Id = cpt.companies_parameter_types_Id,
					value_text = fieldValue
				});
				dataContext.SubmitChanges();
				if (logger != null) logger.DebugFormat("Agency Profile Insert/Update Function: Inserted [{0}] parameter, value = [{1}]", fieldName, fieldValue);
			}
		}

		/// <summary>
		/// Delete Paramter.
		/// </summary>
		/// <param name="dataContext">Data Context of CSP Database.</param>
		/// <param name="logger">The logger for operations.</param>
		/// <param name="fieldName">Field Name to Delete.</param>
		/// <param name="cspCompany">Company Record.</param>
		private static void DeleteParameter(CspDataContext dataContext, ILog logger, string fieldName, company cspCompany)
		{
			if (logger != null) logger.DebugFormat("Agency Profile Delete Function: Begin find \"{0}\" parameter type field in Database.", fieldName);
			companies_parameter_type cpt = dataContext.companies_parameter_types.FirstOrDefault(a => a.parametername == fieldName);
			if (cpt == null) throw new ResponseException(AgencyError.RequiredField(3, fieldName));
			if (logger != null) logger.DebugFormat("Agency Profile Delete Function: Found \"{0}\" parameter type field in Database.", fieldName);

			if (logger != null) logger.DebugFormat("Agency Profile Delete Function: Begin find value of \"{0}\" parameter type field in Database.", fieldName);
			companies_parameter companiesParameter = cspCompany.companies_parameters.FirstOrDefault(cp => cp.companies_parameter_types_Id == cpt.companies_parameter_types_Id);
			if (companiesParameter != null)
			{
				if (logger != null) logger.DebugFormat("Agency Profile Delete Function: Found value of \"{0}\" parameter type field in Database. Delete it.", fieldName);
				if (logger != null) logger.DebugFormat("Agency Profile Delete Function: Begin delete [{0}] parameter.", fieldName);
				dataContext.companies_parameters.DeleteOnSubmit(companiesParameter);
				dataContext.SubmitChanges();
				if (logger != null) logger.DebugFormat("Agency Profile Delete Function: Deleteed [{0}] parameter.", fieldName);
			}
		}

		/// <summary>
		/// Create a DotnetNuke user for Company.
		/// </summary>
		/// <param name="dnnDataContext">Data Context of DotNetNuke Database.</param>
		/// <param name="logger">The logger for operations.</param>
		/// <param name="portalId">Identification of the portal that the user is added.</param>
		/// <param name="dnnVer">Version of DotnetNuke is current used.</param>
		/// <param name="dnnUserPass">Password of DotnetNuke User has created.</param>
		/// <param name="companyId">Identification of Company.</param>
		/// <param name="userName">The name used to log into DotnetNuke.</param>
		/// <param name="displayName">The display name of user has created in DotnetNuke.</param>
		/// <param name="email">The email of user has created in DotnetNuke.</param>
		private static void CreateDnnUser(DnnDataContext dnnDataContext, ILog logger, int portalId, int dnnVer, string dnnUserPass, int companyId, string userName, string displayName, string email)
		{
			// Check exists of Procedure CreateDnnUser2.
			var procExists = dnnDataContext.ExecuteQuery<string>("SELECT * FROM sys.objects WHERE name = 'CreateDnnUser3' AND type = 'P'");
			if (procExists.Any() == false)
			{
				switch (dnnVer)
				{
					case 5:
						dnnDataContext.ExecuteCommand(@"CREATE PROCEDURE [dbo].[CreateDnnUser3]
@UserName nvarchar(256),
@FirstName nvarchar(100),
@LastName nvarchar(100),
@DisplayName nvarchar(256),
@Password nvarchar(128),
@Email nvarchar(256),
@PortalId int,
@CompanyId int
AS
BEGIN
	DECLARE @ApplicationName nvarchar(256)
	SET @ApplicationName = 'DotNetNuke'
	DECLARE @AffiliateId int
	SET @AffiliateId = NULL
	DECLARE @IsSuperUser bit
	SET @IsSuperUser = 0
	DECLARE @PasswordSalt nvarchar(128)
	SET @PasswordSalt = ''
	DECLARE @PasswordQuestion nvarchar(256)
	SET @PasswordQuestion = NULL
	DECLARE @PasswordAnswer nvarchar(128)
	SET @PasswordAnswer = NULL
	DECLARE @UpdatePassword bit
	SET @UpdatePassword = 0
	DECLARE @IsApproved bit
	SET @IsApproved = 1
	DECLARE @Authorised bit
	SET @Authorised = 1
	DECLARE @CurrentTimeUtc datetime
	SET @CurrentTimeUtc = GETDATE()
	DECLARE @CreateDate datetime
	SET @CreateDate = @CurrentTimeUtc
	DECLARE @EffectiveDate datetime
	SET @EffectiveDate = NULL
	DECLARE @ExpiryDate datetime
	SET @ExpiryDate = NULL
	DECLARE @UniqueEmail int
	SET @UniqueEmail = 0
	DECLARE @PasswordFormat int
	SET @PasswordFormat = 0
	DECLARE @CreatedByUserID int
	SET @CreatedByUserID = 1
	Declare @UserId uniqueidentifier
	DECLARE @RoleId int
	DECLARE @DNNUserId int

	EXEC dbo.aspnet_Membership_CreateUser @ApplicationName, @Username, @Password,
		@PasswordSalt, @email, @passwordquestion, @PasswordAnswer,
		@IsApproved, @CurrentTimeUtc, @CreateDate, @UniqueEmail,
		@PasswordFormat, @UserId

	EXEC dbo.AddUser @PortalId, @UserName, @FirstName, @LastName, @AffiliateId,
		@IsSuperUser, @email, @DisplayName, @UpdatePassword, @Authorised, @CreatedByUserId

	SELECT @DNNUserId = UserID
	FROM Users
	WHERE username = @Username

	SELECT @RoleId = RoleID
	FROM Roles
	WHERE RoleName = 'Registered Users' AND PortalID = @PortalId

	EXEC dbo.AddUserRole @PortalId, @DNNUserId, @RoleId, @EffectiveDate, @ExpiryDate, @CreatedByUserID

	DECLARE @PDID int

	SELECT @PDID = PropertyDefinitionID
	FROM ProfilePropertyDefinition
	WHERE PropertyName = 'CspId'

	IF @PDID <> -1
	BEGIN
		IF NOT EXISTS(SELECT * FROM UserProfile WHERE PropertyDefinitionID = @PDID AND UserID = @DNNUserId)
			INSERT INTO UserProfile(UserID, PropertyDefinitionID, PropertyValue, Visibility, LastUpdatedDate)
			VALUES(@DNNUserId, @PDID, @CompanyId, 2, GETDATE())
		ELSE
			UPDATE UserProfile
			SET PropertyValue = @CompanyId
			WHERE PropertyDefinitionID = @PDID AND UserID = @DNNUserId
	END
END");
						break;

					case 6:
						dnnDataContext.ExecuteCommand(@"CREATE PROCEDURE [dbo].[CreateDnnUser3]
@UserName nvarchar(256),
@FirstName nvarchar(100),
@LastName nvarchar(100),
@DisplayName nvarchar(256),
@Password nvarchar(128),
@Email nvarchar(256),
@PortalId int,
@CompanyId int
AS
BEGIN
	DECLARE @ApplicationName nvarchar(256)
	SET @ApplicationName = 'DotNetNuke'
	DECLARE @AffiliateId int
	SET @AffiliateId = NULL
	DECLARE @IsSuperUser bit
	SET @IsSuperUser = 0
	DECLARE @PasswordSalt nvarchar(128)
	SET @PasswordSalt = ''
	DECLARE @PasswordQuestion nvarchar(256)
	SET @PasswordQuestion = NULL
	DECLARE @PasswordAnswer nvarchar(128)
	SET @PasswordAnswer = NULL
	DECLARE @UpdatePassword bit
	SET @UpdatePassword = 0
	DECLARE @IsApproved bit
	SET @IsApproved = 1
	DECLARE @Authorised bit
	SET @Authorised = 1
	DECLARE @CurrentTimeUtc datetime
	SET @CurrentTimeUtc = GETDATE()
	DECLARE @CreateDate datetime
	SET @CreateDate = @CurrentTimeUtc
	DECLARE @EffectiveDate datetime
	SET @EffectiveDate = NULL
	DECLARE @ExpiryDate datetime
	SET @ExpiryDate = NULL
	DECLARE @UniqueEmail int
	SET @UniqueEmail = 0
	DECLARE @PasswordFormat int
	SET @PasswordFormat = 0
	DECLARE @CreatedByUserID int
	SET @CreatedByUserID = 1
	Declare @UserId uniqueidentifier
	DECLARE @RoleId int
	DECLARE @DNNUserId int
  DECLARE @Status int
  SET @Status = 1
  DECLARE @IsOwner bit
  SET @IsOwner = 0

	EXEC dbo.aspnet_Membership_CreateUser @ApplicationName, @Username, @Password,
		@PasswordSalt, @email, @passwordquestion, @PasswordAnswer,
		@IsApproved, @CurrentTimeUtc, @CreateDate, @UniqueEmail,
		@PasswordFormat, @UserId

	EXEC dbo.AddUser @PortalId, @UserName, @FirstName, @LastName, @AffiliateId,
		@IsSuperUser, @email, @DisplayName, @UpdatePassword, @Authorised, @CreatedByUserId

	SELECT @DNNUserId = UserID
	FROM Users
	WHERE username = @Username

	SELECT @RoleId = RoleID
	FROM Roles
	WHERE RoleName = 'Registered Users' AND PortalID = @PortalId

	if @RoleId IS NOT NULL 
		EXEC dbo.AddUserRole @PortalId, @DNNUserId, @RoleId, @Status, @IsOwner, @EffectiveDate, @ExpiryDate, @CreatedByUserID

	SELECT @RoleId = RoleID
	FROM Roles
	WHERE RoleName = 'Subscribers' AND PortalID = @PortalId

	if @RoleId IS NOT NULL 
		EXEC dbo.AddUserRole @PortalId, @DNNUserId, @RoleId, @Status, @IsOwner, @EffectiveDate, @ExpiryDate, @CreatedByUserID

	DECLARE @PDID int

	SELECT @PDID = PropertyDefinitionID
	FROM ProfilePropertyDefinition 
	WHERE PropertyName = 'CspId' and PortalID = @PortalId

	IF @PDID <> -1 AND @PDID IS NOT NULL
	BEGIN
		IF NOT EXISTS(SELECT * FROM UserProfile WHERE PropertyDefinitionID = @PDID AND UserID = @DNNUserId)
			INSERT INTO UserProfile(UserID, PropertyDefinitionID, PropertyValue, Visibility, LastUpdatedDate)
			VALUES(@DNNUserId, @PDID, @CompanyId, 2, GETDATE())
		ELSE
			UPDATE UserProfile
			SET PropertyValue = @CompanyId
			WHERE PropertyDefinitionID = @PDID AND UserID = @DNNUserId
	END
END");
						break;

					default:
						break;
				}
			}

			if (logger != null) logger.Debug("Begin Create DotnetNuke user in Database.");
			dnnDataContext.ExecuteCommand("EXEC CreateDnnUser3 {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}", userName, displayName, displayName, displayName, dnnUserPass,
				email, portalId, companyId);
			//AcceptAgreement(dnnDataContext, userName);
			if (logger != null) logger.Debug("Created DotnetNuke user in Database.");
		}

		/// <summary>
		/// Update a DotnetNuke user of Company.
		/// </summary>
		/// <param name="dnnDataContext">Data Context of DotNetNuke Database.</param>
		/// <param name="logger">The logger for operations.</param>
		/// <param name="portalId">Identification of the portal that the user has added.</param>
		/// <param name="userName">The name used to log into DotnetNuke.</param>
		/// <param name="displayName">The display name of user has created in DotnetNuke.</param>
		/// <param name="email">The email of user has created in DotnetNuke.</param>
		private static void UpdateDnnUser(DnnDataContext dnnDataContext, ILog logger, int portalId, string userName, string displayName, string email)
		{
			// Check exists of Procedure UpdateDnnUser2.
			var procExists = dnnDataContext.ExecuteQuery<string>("SELECT * FROM sys.objects WHERE name = 'UpdateDnnUser3' AND type = 'P'");
			if (procExists.Any() == false)
			{
				dnnDataContext.ExecuteCommand(
					@"CREATE PROCEDURE [dbo].[UpdateDnnUser3]
@UserName nvarchar(256),
@FirstName nvarchar(100),
@LastName nvarchar(100),
@DisplayName nvarchar(256),
@Email nvarchar(256),
@PortalId int
AS
BEGIN
	DECLARE @ASPNETApplicationId uniqueidentifier
	DECLARE @ASPNETUserId uniqueidentifier
	DECLARE @UserId int

	SELECT @ASPNETApplicationId = ApplicationId
	FROM aspnet_Applications
	WHERE ApplicationName = 'DotNetNuke'

	SELECT @ASPNETUserId = UserId
	FROM aspnet_Users
	WHERE ApplicationId = @ASPNETApplicationId AND UserName = @UserName

	UPDATE aspnet_Membership
	SET Email = @Email, LoweredEmail = LOWER(@Email)
	WHERE ApplicationId = @ASPNETApplicationId AND UserId = @ASPNETUserId
	
	SELECT @UserId = UserId
	FROM vw_Users
	WHERE Username = @UserName AND PortalId = @PortalId

	UPDATE Users
	SET Username = @UserName, FirstName = @FirstName, LastName = @LastName, DisplayName = @DisplayName,
		Email = @Email, LastModifiedOnDate = GETDATE()
	WHERE UserID = @UserId
END");
			}

			if (logger != null) logger.Debug("Begin Update DotnetNuke user in Database.");
			dnnDataContext.ExecuteCommand("EXEC UpdateDnnUser3 {0}, {1}, {2}, {3}, {4}, {5}", userName, displayName, displayName, displayName, email, portalId);
			if (logger != null) logger.Debug("Updated DotnetNuke user in Database.");
		}

		/// <summary>
		/// Delete a DotnetNuke user of Company.
		/// </summary>
		/// <param name="dnnDataContext">Data Context of DotNetNuke Database.</param>
		/// <param name="logger">The logger for operations.</param>
		/// <param name="portalId">Identification of the portal that the user has added.</param>
		/// <param name="userName">The name used to log into DotnetNuke.</param>
		private static void DeleteDnnUser(DnnDataContext dnnDataContext, ILog logger, int portalId, string userName)
		{
			var procExists = dnnDataContext.ExecuteQuery<string>("SELECT * FROM sys.objects WHERE name = 'DeleteDnnUser3' AND type = 'P'");
			if (procExists.Any() == false)
			{
				dnnDataContext.ExecuteCommand(
					@"CREATE PROCEDURE [dbo].[DeleteDnnUser3]
@UserName nvarchar(256),
@PortalId int
AS
BEGIN
	DECLARE @UserId int

	SELECT @UserId = UserId
	FROM vw_Users
	WHERE Username = @UserName AND PortalId = @PortalId

	UPDATE UserPortals
	SET IsDeleted = 1
	WHERE PortalId = @PortalId AND UserID = @UserId
END");
			}

			if (logger != null) logger.Debug("Begin Delete DotnetNuke user in Database.");
			dnnDataContext.ExecuteCommand("EXEC DeleteDnnUser3 {0}, {1}", userName, portalId);
			if (logger != null) logger.Debug("Deleted DotnetNuke user in Database.");
		}

		/// <summary>
		/// Reactivate a DotnetNuke user of Company.
		/// </summary>
		/// <param name="dnnDataContext">Data Context of DotNetNuke Database.</param>
		/// <param name="logger">The logger for operations.</param>
		/// <param name="portalId">Identification of the portal that the user has added.</param>
		/// <param name="userName">The name used to log into DotnetNuke.</param>
		private static void ReactivateDnnUser(DnnDataContext dnnDataContext, ILog logger, int portalId, string userName)
		{
			var procExists = dnnDataContext.ExecuteQuery<string>("SELECT * FROM sys.objects WHERE name = 'ReactivateDnnUser3' AND type = 'P'");
			if (procExists.Any() == false)
			{
				dnnDataContext.ExecuteCommand(
					@"CREATE PROCEDURE [dbo].[ReactivateDnnUser3]
@UserName nvarchar(256),
@PortalId int
AS
BEGIN
	DECLARE @UserId int

	SELECT @UserId = UserId
	FROM vw_Users
	WHERE Username = @UserName AND PortalId = @PortalId

	UPDATE UserPortals
	SET IsDeleted = 0
	WHERE PortalId = @PortalId AND UserID = @UserId
END");
			}

			if (logger != null) logger.Debug("Begin reactivate DotnetNuke user in Database.");
			dnnDataContext.ExecuteCommand("EXEC ReactivateDnnUser3 {0}, {1}", userName, portalId);
			if (logger != null) logger.Debug("Complete reactivate DotnetNuke user in Database.");
		}

		/// <summary>
		/// Make WebService to auto accept agreement when Insert a new user.
		/// </summary>
		/// <param name="dnnDataContext">Data Context of DotNetNuke Database.</param>
		/// <param name="userName">The name used to log into DotnetNuke.</param>
		private static void AcceptAgreement(DnnDataContext dnnDataContext, string userName)
		{
			string sContent = "Effective March 1, 2011 TIE KINETIX CONTENT SYNDICATION PLATFORM TERMS AND CONDITIONS  TIE Commerce, Inc. (\"TIE\", \"We\" or \"Us\") makes this Content Syndication Platform Service (\"TIE Kinetix CSP\") available to You (\"You\" or \"Your\") subject to the following terms and conditions (\"Terms and Conditions\" or \"Agreement\").  Please read these Terms and Conditions carefully.  By clicking the \"I agree\" box as part of the registration process or by using the TIE Service, You certify that you have read, understood and agree to be bound by these Terms and Conditions.  IF YOU ARE ENTERING INTO THIS AGREEMENT ON BEHALF OF A COMPANY OR OTHER LEGAL ENTITY, YOU REPRESENT THAT YOU HAVE THE AUTHORITY TO BIND SUCH ENTITY TO THESE TERMS AND CONDITIONS, IN WHICH CASE THE TERMS \"YOU\" OR \"YOUR\" SHALL REFER TO SUCH ENTITY. IF YOU DO NOT HAVE SUCH AUTHORITY, OR IF YOU DO NOT AGREE WITH THESE TERMS AND CONDITIONS, DO NOT COMPLETE THE REGISTRATION PROCESS AND USE THE TIE KINETIX CSP. 1. TIE Kinetix CSP.  1.1 Scope of Service. TIE Kinetix CSP allows You to access a web application and platform whereby You can automatically embed and display the Selected Content on an internet accessible website or device.  For the purpose of these Terms and Conditions, \"Selected Content\" may include: (i) content from a third party used to describe its products, services including without limitation, part numbers, descriptions, specifications, HTML, graphics, ads, flash banners, public price catalogs, images, key selling points, user manuals, marketing  materials, product tours, videos, logos, distinct brand elements, trademarks and other creative assets related to such products and services which such third party (or parties) have provided to TIE,  and/or are otherwise publically available. 1.2. Routine Maintenance. TIE schedules regular maintenance on a weekly basis to apply maintenance or make system changes as necessary.  2. Grant of Rights.  Subject to any third party licenses, TIE hereby grants You a limited, non-exclusive, non-transferable, non-sublicensable, non-assignable, revocable license during the term of this Agreement to: (i) access and use the TIE Kinetix CSP; and (ii) use, reproduce, and publicly display the Selected Content on your website.  All rights not expressly granted in these Terms and Conditions are reserved by TIE. 3 Use Restrictions. 3.1 In the event third party marks are included within the distributed content, You are responsible for obtaining the applicable trademark and logo guidelines and will at all times use the third party marks in accordance with such trademark and logo guidelines.  You will not use the third party marks to diminish or otherwise damage the third parties goodwill in its respective marks and/or logos.  3.2 Your website on which you display the Selected Content will meet or exceed the standard of quality and performance generally accepted in the industry and will comply with all applicable laws, rules and regulations.  3.3 You will not use the Selected Content and/or the TIE Kinetix CSP in connection with activities that could be deemed obscene, pornographic, excessively violent or unlawful. 3.4 The Selected Content is strictly limited to the TIE Kinetix CSP and solely intended for display on Your internet connected website or device.  You shall not license, sublicense, sell, resell, transfer, assign, distribute or otherwise commercially exploit or make available to any third party the TIE Kinetix CSP or the Selected Content in any way.  You shall not modify or make derivative works based upon the TIE Kineitix CSP, the Selected Content or attempt to reverse engineer the TIE Kinetix CSP. 4. Ownership of Intellectual Property.  You acknowledge and agree that as between You, TIE and any third party content providers that (a) the third party content owners own all right, title and interest in and to their respective Content; (b) TIE owns all right, title and interest in and to the TIE Kinetix CSP and any content generated directly by TIE, and its trademarks, service marks, logos and other distinctive brand features used within the TIE Kinetix CSP or the selected content; and (c) TIE will collect and The Hartford will own all data generated and collected through Your use of the TIE Kinetix CSP.  Nothing in these Terms and Conditions shall confer in You any right of ownership in the foregoing including but not limited to the selected content or the TIE Kinetix CSP. 5. Representations and Warranties. You represent and warrant that: (a) You have the power and authority to enter into and perform the obligations under this Agreement; (b) You are in full compliance and will continue to comply with these Terms and Conditions and all applicable laws and regulations; and (c) You will not use the TIE Kinetix CSP for any purpose other than as expressly authorized under this Agreement. 6. DISCLAIMERS. TIE DOES NOT WARRANT THAT THE TIE KINETIX CSP OR ANY OF THE SELECTED CONTENT WILL BE SECURE, TIMELY, UNINTERRUPTED OR ERROR-FREE OR OPERATE IN COMBINATION WITH OTHER HARDWARE, SOFTWARE, SYSTEM OR DATA. IN ADDITION, TIE DOES NOT MAKE ANY WARRANTIES AS TO THE RESULTS TO BE OBTAINED FROM USE OF THE TIE KINETIX CSP.  THE TIE SERVICE AND SELECTED CONTENT ARE DISTRIBUTED ON AN \"AS IS, AS AVAILABLE\" BASIS. ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE TIE KINETIX CSP IS DONE AT YOUR OWN DISCRETION AND RISK, AND YOU WILL BE SOLELY RESPONSIBLE FOR ANY POTENTIAL DAMAGES TO YOUR COMPUTER SYSTEM OR LOSS OF DATA THAT RESULTS FROM THE DOWNLOAD OF ANY SUCH MATERIAL. TIE DOES NOT MAKE ANY WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, WARRANTIES OF TITLE, OR IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, WITH RESPECT TO THE TIE KINETIX CSP, THE SELECTED CONTENT, OR ANY PRODUCTS OR DATA AVAILABLE THROUGH THE TIE KINETIX CSP. YOU EXPRESSLY AGREE THAT YOU WILL ASSUME THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE TIE KINETIX CSP.  7. LIMITATION OF LIABILITY.  TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, TIE WILL NOT BE LIABLE FOR ANY LOST PROFITS, LOST BUSINESS, INTERRUPTION OF BUSINESS, COSTS OF PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES, OR FOR ANY OTHER INDIRECT, SPECIAL, INCIDENTAL, PUNITIVE OR CONSEQUENTIAL DAMAGES ARISING OUT OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THE TIE KINETIX CSP, THE SELECTED CONTENT OR IN GENERAL IN CONNECTION WITH THE SUBJECT MATTER OF THIS AGREEMENT HOWEVER CAUSED, AND UNDER WHATEVER CAUSE OF ACTION OR THEORY OF LIABILITY BROUGHT (INCLUDING, WITHOUT LIMITATION, UNDER ANY CONTRACT, NEGLIGENCE OR OTHER TORT THEORY OF LIABILITY) EVEN IF TIE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. TIE’S AGGREGATE LIABILITY FOR DIRECT DAMAGES UNDER THESE TERMS AND CONDITIONS SHALL BE LIMITED TO ONE HUNDRED DOLLARS ($100.00). THE PARTIES ACKNOWLEDGE THAT THE LIMITATIONS SET FORTH IN THIS SECTION ARE INTEGRAL TO THE AMOUNT OF CONSIDERATION LEVIED UNDER THESE TERMS AND CONDITIONS. CERTAIN JURISDICTIONS DO NOT ALLOW LIMITATIONS ON IMPLIED WARRANTIES OR THE EXCLUSION OR LIMITATION OF CERTAIN DAMAGES.  IF YOU RESIDE IN SUCH A JURISDICTION, SOME OR ALL OF THE ABOVE DISCLAIMERS, EXCLUSIONS, OR LIMITATIONS MAY NOT APPLY TO YOU, AND YOU MAY HAVE ADDITIONAL RIGHTS.  THE LIMITATIONS OR EXCLUSIONS OF WARRANTIES, REMEDIES, OR LIABILITY CONTAINED IN THESE TERMS AND CONDITIONS APPLY TO YOU TO THE FULLEST EXTENT SUCH LIMITATIONS OR EXCLUSIONS ARE PERMITTED UNDER THE LAWS OF THE JURISDICTION IN WHICH YOU ARE LOCATED. 8. Modifications to these Terms and Conditions and the TIE KINETIX CSP. These Terms and Conditions (including the TIE Kinetix CSP) may not  be modified from time to time without notice to You. TIE may change, suspend, or discontinue all or any aspect of the TIE Kinetix CSP at any time, including the availability of any TIE Kinetix CSP feature, database, or content, without prior notice or liability. After receiving notice about a change in this Agreement, Your continued use of the TIE Kinetix CSP will constitute Your binding acceptance of these Terms and Conditions, including all changes and modifications. 9. Password and Registration.  You agree to provide true, accurate, current, and complete information about Yourself as requested during the TIE Kinetix CSP registration process and agree to update Your information within ten (10) days of any changes. You may not reveal Your TIE Kinetix CSP login credentials to anyone outside of your insurance agency.  ,. You are responsible for maintaining the confidentiality of Your password within Your insurance agency. 10. Contact and Communications. TIE may contact You via e-mail, phone or postal mail to validate Your account, provide You with information about the TIE Kinetix CSP, Your account or to provide You with information about special offers available only to TIE Kinetix CSP subscribers. Moreover in order to facilitate Your use of the third party content during the term of this Agreement, TIE reserves the right to provide Your contact information to solely to The Hartford.  TIE agrees not to provide any third party (excluding The Hartford) any information about You or Your insurance Agency. . 11. Term; Effect of Termination.  Your use of the TIE Kinetix CSP will commence when You have accepted these Terms and Conditions or otherwise used the TIE Kinetix CSP and shall continue until terminated as provided in these Terms and Conditions.  TIE reserves the right, in its sole discretion, to restrict, suspend, or terminate Your access to all or any part of the TIE Kinetix CSP at any time for any reason or no reason at all without prior notice or liability. You may terminate Your use of the TIE Kinetix CSP for any reason or no reason at all upon thirty (30) days prior written notice to TIE.  Upon termination, You shall immediately cease use of the TIE Kinetix CSP and delete all Selected Content from Your website.  The following provisions shall survive any termination of these Terms and Conditions: Sections 5, 6, , 8, 9, 13 and 14.  12. Miscellaneous.  You may not assign your participation in the TIE Kinetix CSP, by operation of law or otherwise, without TIE’s prior written consent; any assignment in violation of this requirement shall be null and void.  TIE’s failure to enforce Your strict performance of any provision of these Terms and Conditions will not constitute a waiver of their respective rights to subsequently enforce such provision or any other provision of these Terms and Conditions.  The parties hereto are independent contractors and nothing contained herein or done in pursuance of these Terms and Conditions shall constitute either party as the agent or employee of the other party or constitute the parties as partners, joint ventures or franchisor and franchisee.  All notices, requests, consents and other communication under the Program shall be, if to You, at the email address You provide TIE at the time You set up Your account, and if to TIE, in writing to TIE Commerce, Inc., 24 New England Executive Park, Burlington, Massachusetts  U.S.A.  If any provision of these Terms and Conditions is determined by a court or other authority having competent jurisdiction to be invalid, illegal or otherwise unenforceable, that provision shall be enforced to the maximum extent allowed so as to effect the intent of the parties and all other provisions of these Terms and Conditions shall remain in full force and effect and shall not thereby be affected or impaired.  This Agreement sets forth the entire agreement between the parties and supersedes and replaces any prior agreements or understandings of the parties as to such subject matter and shall not be amended except by a writing signed by both parties. Any information provided to TIE in connection with the registration will be kept confidential and will not be shared with any third part, except The Hartford.  TIE will keep all information obtain about You (and Your agency) that it obtains confidential which steps shall be no less rigorous than those used to protect its own similar information, and to restrict the use and distribution of the confidential Information only to the authorized persons.";

			int acceptAgreementPolicyID = 0, acceptAgreementID = 0;
			ProfilePropertyDefinition ppd =
				dnnDataContext.ProfilePropertyDefinitions.FirstOrDefault(
					a => a.PropertyCategory == "Accept Agreement" && a.PropertyName == "AcceptAgreementPolicy");
			if (ppd != null) acceptAgreementPolicyID = ppd.PropertyDefinitionID;
			ppd =
				dnnDataContext.ProfilePropertyDefinitions.FirstOrDefault(
					a => a.PropertyCategory == "Accept Agreement" && a.PropertyName == "AcceptAgreement");
			if (ppd != null) acceptAgreementID = ppd.PropertyDefinitionID;

			if(acceptAgreementPolicyID != 0 && acceptAgreementID != 0)
			{
				User user = dnnDataContext.Users.FirstOrDefault(a => a.Username == userName);
				if (user != null)
				{
					dnnDataContext.UserProfiles.InsertOnSubmit(new UserProfile
					{
						UserID = user.UserID,
						PropertyDefinitionID = acceptAgreementID,
						PropertyValue = "TRUE",
						Visibility = 2,
						LastUpdatedDate = System.DateTime.Now
					});

					dnnDataContext.UserProfiles.InsertOnSubmit(new UserProfile
					{
					  UserID = user.UserID,
						PropertyDefinitionID = acceptAgreementPolicyID,
						PropertyText = sContent,
						Visibility = 2,
						LastUpdatedDate = System.DateTime.Now
					});

					dnnDataContext.SubmitChanges();
				}
			}
		}
	}
}