﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using CR.DnnModules.SelfService.Common;

namespace CR.DnnModules.SelfService
{
	public partial class StepLogo : DnnModuleBase
	{
		protected string UploadFolder = "~/Uploads";
		protected string LogoFolderName = "images";
		protected string UserFolder = "";

		protected override void Page_Init(object sender, EventArgs e)
		{
			base.Page_Init(sender, e);
			InitSettings();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (Request.Headers["X-OFFICIAL-REQUEST"] == "TRUE")
			{
				if (Request.Params["logolink"] != null)
				{
					string staticUrl = "";
					string stringToReplace = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd(Convert.ToChar("/")); //http://localhost/dnn/0602 if server http://cr.syndicationtest.com/devportals/selfservice
					string logoUrl = Request.Params["logolink"], originalLogo = logoUrl;
					if (Settings.ContainsKey(Cons.SETTING_IMAGE_URL) && Settings[Cons.SETTING_IMAGE_URL].ToString() != string.Empty)
					{
						staticUrl = Settings[Cons.SETTING_IMAGE_URL].ToString();
						if (staticUrl.EndsWith("/"))
							staticUrl = staticUrl.TrimEnd('/');
					}
					if (!String.IsNullOrEmpty(staticUrl))
						logoUrl = logoUrl.Replace(stringToReplace, staticUrl);

					Regex rgx = new Regex("[^a-zA-Z0-9_.]");
					string filePath = logoUrl.Substring(0, logoUrl.LastIndexOf('/') + 1);
					string fileName = logoUrl.Split('/').Last();
					fileName = rgx.Replace(fileName, "");// fileName.Replace(" ", string.Empty);
					string fileWithouSpaces = filePath + fileName;

					Uri srcUri = new Uri(logoUrl);
					string srcRelativeUrl = srcUri.AbsolutePath;
					srcRelativeUrl = srcRelativeUrl.Replace("%20", " ");

					Uri descUri = new Uri(fileWithouSpaces);
					string descRelativeUrl = descUri.AbsolutePath;

					if (srcRelativeUrl != descRelativeUrl)
					{
						if (!srcRelativeUrl.StartsWith("~"))
							srcRelativeUrl = "~" + srcRelativeUrl;
						if (!descRelativeUrl.StartsWith("~"))
							descRelativeUrl = "~" + descRelativeUrl;
						File.Copy(Server.MapPath(srcRelativeUrl), Server.MapPath(descRelativeUrl), true);
						File.Delete(Server.MapPath(srcRelativeUrl));
						originalLogo = originalLogo.Substring(0, originalLogo.LastIndexOf("/") + 1) + fileName;
					}

					logoUrl = fileWithouSpaces;

					ResizeLogo(originalLogo);
					UpdateStepValue("StepLogo", logoUrl);
					UpdateCompanyParameter(logoUrl);
				}
				return;
			}
			LoadPageResources();
        }

		private void ResizeLogo(string logoUrl)
		{
			string bannerFile = logoUrl;// Request.Params["logolink"];
			string[] bannerType = new string[]{"200", "90"};
			int newWidth = int.Parse(bannerType[0]);
			int newHeight = int.Parse(bannerType[1]);
			Uri uri = new Uri(bannerFile);
			string relativeUrl = uri.AbsolutePath;
			string serverBannerFile = Server.MapPath(relativeUrl);
			if (File.Exists(serverBannerFile))
			{
				System.Drawing.Image srcImg = System.Drawing.Image.FromFile(serverBannerFile);
				Bitmap newImage = new Bitmap(newWidth, newHeight);
				using (Graphics gr = Graphics.FromImage(newImage))
				{
					gr.SmoothingMode = SmoothingMode.HighQuality;
					gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
					gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
					gr.DrawImage(srcImg, new Rectangle(0, 0, newWidth, newHeight));
				}
				srcImg.Dispose();
				newImage.Save(serverBannerFile);
				newImage.Dispose();
			}
		}

		/// <summary>
		/// Inits the settings.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>5/23/2013</datetime>
		private void InitSettings()
		{
			if (Settings.ContainsKey(Cons.SETTING_UPLOAD_FOLDER) && Settings[Cons.SETTING_UPLOAD_FOLDER].ToString() != string.Empty)
				UploadFolder = Settings[Cons.SETTING_UPLOAD_FOLDER].ToString();
			if (UploadFolder.StartsWith("~/"))
				UploadFolder = UploadFolder.Replace("~/", string.Empty);
			if (Settings.ContainsKey(Cons.SETTING_FOLDER_LOGO) && Settings[Cons.SETTING_FOLDER_LOGO].ToString() != string.Empty)
				LogoFolderName = Settings[Cons.SETTING_FOLDER_LOGO].ToString();
			if (LogoFolderName.EndsWith(@"\") || LogoFolderName.EndsWith("/"))
				LogoFolderName = LogoFolderName.Substring(0, LogoFolderName.Length - 1);
			Regex rgx = new Regex("[^a-zA-Z0-9_]");
			UserFolder = _projectName;// rgx.Replace(_coInfo.companyname.ToLower(), "");
		}

		/// <summary>
		/// Loads the page resources.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>5/23/2013</datetime>
		private void LoadPageResources()
		{
			Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel='stylesheet' href='{0}css/page-logo.css' type='text/css' />", ControlPath)));
			Page.ClientScript.RegisterClientScriptInclude("mainfunction", ControlPath + "js/function.js");
			StepNavigation.GetPageParams(ModuleId, LocalResourceFile, FinishedSteps);
			SetupAsyncUploader();
		}

		/// <summary>
		/// Setups the async uploader.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>5/23/2013</datetime>
		private void SetupAsyncUploader()
		{
            String logoFolderPath = UploadFolder + "/campaign/" + UserFolder + "/" + UserFolder + "microsite" + "/" + langCode +"/" + LogoFolderName;
			logoFolderPath = Server.MapPath(logoFolderPath);
			SetupUploadFolder(logoFolderPath);
			RadAsyncUpload1.TargetFolder = logoFolderPath;
		}

		/// <summary>
		/// Gets the upload relative path.
		/// </summary>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>5/23/2013</datetime>
		protected string GetUploadRelativePath()
		{
            String logoFolderPath = UploadFolder + "/campaign/" + UserFolder + "/" + UserFolder + "microsite" + "/" + langCode + "/" + LogoFolderName + "/";
			//logoFolderPath = Server.MapPath(logoFolderPath).Replace(Request.ServerVariables["APPL_PHYSICAL_PATH"], String.Empty);
			//logoFolderPath = logoFolderPath + @"\";
			//return logoFolderPath.Replace(@"\", "/");
			return logoFolderPath;
		}

		/// <summary>
		/// Gets the logo URL.
		/// </summary>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>5/23/2013</datetime>
		protected string GetLogoUrl()
		{
			return GetStepValue("StepLogo");
		}

		/// <summary>
		/// Updates the company parameter.
		/// </summary>
		/// <param name="logoUrl">The logo URL.</param>
		/// <author>ducuytran</author>
		/// <datetime>5/23/2013</datetime>
		private void UpdateCompanyParameter(string logoUrl)
		{
			string parameterName = "Personalize_PDF_Logo";
			companies_parameter_type parameterType =
				CspDataContext.companies_parameter_types.FirstOrDefault(a => a.parametername == parameterName);
			if (parameterType == null)
				return;

			companies_parameter crrCoParam =
				CspDataContext.companies_parameters.FirstOrDefault(
					a => a.companies_Id == _cspId && a.companies_parameter_types_Id == parameterType.companies_parameter_types_Id);
			if (crrCoParam == null)
			{
				companies_parameter newCompanyParameter = new companies_parameter
				{
					companies_Id = _cspId,
					companies_parameter_type = parameterType,
					value_text = logoUrl
				};
				CspDataContext.companies_parameters.InsertOnSubmit(newCompanyParameter);
			}
			else
			{
				crrCoParam.value_text = logoUrl;
			}

			CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
		}       
	}
}