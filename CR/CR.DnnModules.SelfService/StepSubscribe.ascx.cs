﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.DnnModules.SelfService.Common;

namespace CR.DnnModules.SelfService
{
    public partial class StepSubscribe : DnnModuleBase
    {
        protected override void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel='stylesheet' href='{0}css/page-subscribe.css' type='text/css' />", ControlPath)));
            Page.ClientScript.RegisterClientScriptInclude("mainfunction", ControlPath + "js/function.js");
            Page.ClientScript.RegisterClientScriptInclude("zclip", ControlPath + "js/jquery.zclip.min.js");
			StepNavigation.GetPageParams(ModuleId, LocalResourceFile, FinishedSteps);
        }
        public string GetBaseDomain()
        {
            var companyconsumer = CspDataContext.companies_consumers.FirstOrDefault(a => a.companies_Id == _cspId);
            if (companyconsumer != null)
                return "http://" + companyconsumer.base_domain;
            return "";
        }
    }
}