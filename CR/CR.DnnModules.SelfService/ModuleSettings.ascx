﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleSettings.ascx.cs" Inherits="CR.DnnModules.SelfService.ModuleSettings" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div>
	<table>
		<tr>
			<td>Upload folder:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxUploadFolder"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>Logo folder name:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxLogoFolderName"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>Banner folder name:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxBannerFolderName"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>Image URL:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxImageUrl"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>Hide Description By Default:</td>
			<td><asp:CheckBox runat="server" ID="chkHideDescription"/></td>
		</tr>
		<tr>
			<td>User Role Name:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxUserRoleName"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>CSP DB Connection String:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxCspDnnConnectionString"></telerik:RadTextBox></td>
		</tr>
	</table>
</div>