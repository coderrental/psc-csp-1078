﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using CR.DnnModules.SelfService.Common;
using DotNetNuke.Services.Log.EventLog;
using Ionic.Zip;
using Telerik.Web.UI;

namespace CR.DnnModules.SelfService
{
    public partial class StepContent : DnnModuleBase
    {
        protected string UploadFolder = "~/Uploads";
		protected string WebtrendsFolder = "~/Webtrends";
    	protected string WebtrendsPath = "";
        protected string UserFolder = "";
        protected string LanguageFolderName = "EN";
        protected string ContentFolderPath = "";
        protected string Filename = "";
        protected string ZipFolderPath = "";
        protected string BannerFolderName = "images";
        protected string LogoFolderName = "images";
        protected string ResponseMsg = "";
        protected Dictionary<string, string> bannerField;
        private const int STAGE_PUBLISHED = 50;
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadPageResources();
            if (Request.Headers["X-OFFICIAL-REQUEST"] == "TRUE")
            {
                Filename = Request.Params["fileName"];
                if (Path.GetExtension(Filename)==".zip")
                {
                    FileExtract();
                	//CreatWebtrendsFile();
                    return;                    
                }
                else
                {
                    UploadFilePdf();
                	//CreatWebtrendsFile();
                    return;
                }

            }
            Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel='stylesheet' href='{0}css/page-content.css' type='text/css' />", ControlPath)));
            Page.ClientScript.RegisterClientScriptInclude("mainfunction", ControlPath + "js/function.js");
			StepNavigation.GetPageParams(ModuleId, LocalResourceFile, FinishedSteps);
        }

        protected override void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
            InitSettings();
        }
        private void InitSettings()
        {
            if (Settings.ContainsKey(Cons.SETTING_UPLOAD_FOLDER) && Settings[Cons.SETTING_UPLOAD_FOLDER].ToString() != string.Empty)
                UploadFolder = Settings[Cons.SETTING_UPLOAD_FOLDER].ToString();

            if (UploadFolder.StartsWith("~/"))
                UploadFolder = UploadFolder.Replace("~/", string.Empty);

			if (Settings.ContainsKey(Cons.SETTING_WEBTRENDS_FOLDER) && Settings[Cons.SETTING_WEBTRENDS_FOLDER].ToString() != string.Empty)
				WebtrendsFolder = Settings[Cons.SETTING_WEBTRENDS_FOLDER].ToString();

			if (WebtrendsFolder.StartsWith("~/"))
				WebtrendsFolder = WebtrendsFolder.Replace("~/", string.Empty);


            if (Settings.ContainsKey(Cons.SETTING_FOLDER_BANNER) && Settings[Cons.SETTING_FOLDER_BANNER].ToString() != string.Empty)
                BannerFolderName = Settings[Cons.SETTING_FOLDER_BANNER].ToString();

            if (BannerFolderName.EndsWith(@"\") || BannerFolderName.EndsWith("/"))
                BannerFolderName = BannerFolderName.Substring(0, BannerFolderName.Length - 1);

            if (Settings.ContainsKey(Cons.SETTING_FOLDER_LOGO) && Settings[Cons.SETTING_FOLDER_LOGO].ToString() != string.Empty)
                LogoFolderName = Settings[Cons.SETTING_FOLDER_LOGO].ToString();

            if (LogoFolderName.EndsWith(@"\") || LogoFolderName.EndsWith("/"))
                LogoFolderName = LogoFolderName.Substring(0, LogoFolderName.Length - 1);

            bannerField = new Dictionary<string, string>()
    		                                         	{
    		                                         		{"143 x 60", "Banner_Thumbnail_Image"},
															{"160 x 600", "Wide_Skyscraper_160x600_Image"},
															{"180 x 90", "Button_180x90_Image"},
															{"234 x 60", "Half_Banner_234x60_Image"},
															{"300 x 250", "Medium_Rectangle_300x250_Image"},
															{"468 x 60", "Full_Banner_468x60_Image"},							
    		                                         	};
			Regex rgx = new Regex("[^a-zA-Z0-9_]");
        	UserFolder = _projectName;// rgx.Replace(_coInfo.companyname.ToLower(), "");
            LoadCurrentBanners();
        }
        private void LoadPageResources()
        {
            Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel='stylesheet' href='{0}css/page-content.css' type='text/css' />", ControlPath)));
            Page.ClientScript.RegisterClientScriptInclude("mainfunction", ControlPath + "js/function.js");
            StepNavigation.GetPageParams(ModuleId, LocalResourceFile);
            SetupAsyncUploader();
        }
        private void SetupAsyncUploader()
        {
            ZipFolderPath = UploadFolder + "/campaign/" + UserFolder + "/" + UserFolder + "microsite" + "/";
            ContentFolderPath = Server.MapPath(ZipFolderPath + langCode);
            ZipFolderPath = Server.MapPath(ZipFolderPath);
            SetupUploadFolder(ContentFolderPath);
            RadAsyncUpload1.TargetFolder = ZipFolderPath;
        }
        protected void FileExtract()
        {
            string tempFolder = Server.MapPath(UploadFolder + "/campaign/" + UserFolder + "/TempUploadContent");
            string targetFileName = Path.Combine(ZipFolderPath, Filename);
            string fileName = Path.GetFileNameWithoutExtension(targetFileName);
            var df = new DirectoryInfo(tempFolder);
            try
            {
                using (ZipFile zip = ZipFile.Read(targetFileName))
                {
                    foreach (ZipEntry e in zip)
                    {
                        e.Extract(tempFolder, ExtractExistingFileAction.OverwriteSilently);  // overwrite == true
                    }
                }

            }
            catch (System.Exception ex1)
            {

                System.Console.Error.WriteLine("exception: " + ex1);
            }

            if (!CheckValidate(df))
            {
                df.Delete(true);
                File.Delete(targetFileName);
            }
            else
            {
                var dfc= new DirectoryInfo(ContentFolderPath);
				try
				{
					DeletingFiles(dfc);
				}
				catch (Exception ex)
				{ 
					DotNetNuke.Services.Log.EventLog.EventLogController eventLogController = new EventLogController();
					eventLogController.AddLog("Error", ex.Message, PortalSettings, -1,
					                          DotNetNuke.Services.Log.EventLog.EventLogController.EventLogType.ADMIN_ALERT);
				}
            	CopyContent(tempFolder,ContentFolderPath);
                df.Delete(true);
                string newFileName = fileName;
                newFileName = Path.Combine(ZipFolderPath, newFileName + "_" + langCode + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".zip");
                File.Move(targetFileName, newFileName);// change file name

            }
			Response.End();
        }
        public void DeletingFiles(DirectoryInfo directory)
        {
            //delete files in directory
            foreach (FileInfo file in directory.GetFiles())
            {
            	file.IsReadOnly = false;
				
                file.Delete();
            }
            //delete directories in this directory:
            foreach (DirectoryInfo subDirectory in directory.GetDirectories())
            {
                if (subDirectory.Name != BannerFolderName && subDirectory.Name != LogoFolderName)
                {
                    DeletingFiles(subDirectory);
                    subDirectory.Delete();
                }
            }
        }
        public Boolean CheckValidate(DirectoryInfo directory)
        {
            bool validate;
            int check = directory.GetFiles().Count(file => file.Name.ToLower() == "index.html");
            if (check == 0)
            {
                ResponseMsg = "index.html not found";
                validate = false;
            }
            else
            {
				UpdateStepValue("StepContent", "Done");
				UpdateStepValue("StepSubscribe", "Done");
                validate = true;
            }
            Response.Clear();
            Response.Write(ResponseMsg);
            Response.Flush();
            //Response.End();
            return validate;
        }
        private void LoadCurrentBanners()
        {
            content_main cmBanner = CspDataContext.content_mains.FirstOrDefault(a => a.supplier_Id == _cspId);
            if (cmBanner == null) return;
            content bannerContent = CspDataContext.contents.FirstOrDefault(a => a.content_main == cmBanner && a.content_types_languages_Id == _langId && a.stage_Id == STAGE_PUBLISHED);
            if (bannerContent == null) return;
            List<Banner> bannerList = new List<Banner>();
            foreach (KeyValuePair<string, string> keyValuePair in bannerField)
            {
                string bannerValue = GetContentFieldValue(bannerContent, keyValuePair.Value);
				if (!String.IsNullOrEmpty(bannerValue) && bannerValue.IndexOf("banner_holder_") == -1)
                //if (!String.IsNullOrEmpty(bannerValue))
                {
                    bannerList.Add(new Banner
                    {
                        BannerType = keyValuePair.Key,
                        BannerUrl =  keyValuePair.Value
                    });
                }
            }
            if (bannerList.Any())
            {
                foreach (var banner in bannerList)
                {
                    rcbBanner.Items.Add(new RadComboBoxItem(banner.BannerType,banner.BannerUrl));
                }
            }
        }
        public string GetScriptUrl()
        {
        	CspUtils utils = new CspUtils(CspDataContext);
            string basedomain ="";
            companies_consumer companiesConsumer = CspDataContext.companies_consumers.FirstOrDefault(a => a.companies_Id == _cspId);
            if (companiesConsumer != null)
            {
                basedomain = companiesConsumer.base_domain;
            }
            string banner = rcbBanner.SelectedItem.Value;

			string ParameterTypeName = "CSP_Project_Name";
        	string coProjectName = utils.GetCompanyParameter(_cspId, ParameterTypeName);

			string linkURL = string.Format("http://{0}/Csp/?mfrname={1}&t=campaign&category=1&audience=Consumer&banner={2}&lng={3}&ticks={4}",
						basedomain, coProjectName, banner, langCode,DateTime.Now.Ticks.ToString());
            return linkURL;
        }
        public void CopyContent(string sourceDir, string targetDir)
        {
            Directory.CreateDirectory(targetDir);
            foreach (var file in Directory.GetFiles(sourceDir))
                File.Copy(file, Path.Combine(targetDir, Path.GetFileName(file)),true);

            foreach (var directory in Directory.GetDirectories(sourceDir))
                CopyContent(directory, Path.Combine(targetDir, Path.GetFileName(directory)));
        }
        public void UploadFilePdf()
        {
			Response.Cache.SetCacheability(HttpCacheability.NoCache);

			Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));

			Response.Cache.SetNoStore();
            string sourceFile = Path.Combine(ZipFolderPath, Filename);
            string destinationFile = Path.Combine(ContentFolderPath, Filename);
            string htmlFile = Path.Combine(ContentFolderPath, "index.html");
            var df = new DirectoryInfo(ContentFolderPath);
			try
			{
				DeletingFiles(df);
			}
			catch(Exception ex)
			{
				EventLogController eventLogController = new EventLogController();
				eventLogController.AddLog("Error", ex.Message, PortalSettings, -1,
										  DotNetNuke.Services.Log.EventLog.EventLogController.EventLogType.ADMIN_ALERT);
			}
        	File.Move(sourceFile, destinationFile);
            using (Stream fs = new FileStream(htmlFile, FileMode.CreateNew, FileAccess.ReadWrite))
            {
                using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                {
                    //string content = string.Format(@"<body><iframe style='width:100%;height:100%' src='{0}'></iframe></body>", Filename);
                    string content =
                        string.Format(
                            @"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<script src='/UI/UniversalResources/js/RenderPDFInLightBoxOrThickBox.js?PDFURL={0}&ticks={1}'></script>
</head>
<body>
</body>
</html>",Filename,DateTime.Now.Ticks.ToString());
                    w.WriteLine(content);
                }
            }
            string srcJsFile = Server.MapPath(ControlPath) + @"js\RenderPDFInLightBoxOrThickBox.js";
            string jsFolderRelativePath = UploadFolder + "/campaign/" + UserFolder + "/" + UserFolder + "microsite" + "/" + langCode + "/UI/UniversalResources/js/";
            string jsFolderPath = Server.MapPath(jsFolderRelativePath);
            if (!System.IO.Directory.Exists(jsFolderPath))
            {
                System.IO.Directory.CreateDirectory(jsFolderPath);
            }

            string destJsFile = System.IO.Path.Combine(jsFolderPath, "RenderPDFInLightBoxOrThickBox.js");
            if (System.IO.File.Exists(srcJsFile))
                System.IO.File.Copy(srcJsFile, destJsFile, true);

            using (ZipFile zip = new ZipFile())
            {
                zip.AddDirectory(ContentFolderPath);
                zip.Save(sourceFile + "_" + langCode + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".zip");
            }

			UpdateStepValue("StepContent", "Done");
			UpdateStepValue("StepSubscribe", "Done");
            Response.Clear();
            Response.Write(ResponseMsg);
            Response.Flush();
            Response.End();
        }

    	protected string GetCompanyBaseDomain()
    	{
			string basedomain = "";
			companies_consumer companiesConsumer = CspDataContext.companies_consumers.FirstOrDefault(a => a.companies_Id == _cspId);
			if (companiesConsumer != null)
			{
				basedomain = companiesConsumer.base_domain;
			}
    		return basedomain;
    	}
    }
}