﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StepBanner.ascx.cs" Inherits="CR.DnnModules.SelfService.StepBanner" %>
<%@ Import Namespace="CR.ContentObjectLibrary.Data" %>
<%@ Import Namespace="DotNetNuke.Common" %>
<%@ Import Namespace="DotNetNuke.Services.Localization" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="tagprefix" src="StepNavigation.ascx" tagName="StepNavigation" %>
 <telerik:RadToolTipManager ID="radTooltipManager" runat="server" RelativeTo="Element"
          Position="TopCenter" AutoTooltipify="true" ContentScrolling="Default" Width="180"
          Height="10">
 </telerik:RadToolTipManager>
<div id="step-navigation">
    <tagprefix:StepNavigation ID="StepNavigation" runat="server" />
</div>
<div id="page-content">
	<div id="description-block">
		<div id="description-text">
			<h1 class="darkblue-text"><%= GetLocalizedText("PageBanners.UploadBannersTitle")%></h1>
			<p class="darkblue-text"><%= GetLocalizedText("PageBanners.UploadBannersTitleDescription")%></p>
		</div>
		<div id="description-image">
			<a id="btn-goto-uploadcontent" class="btn-darkblue btn-darkblue-inactive" href="javascript: void(0);"><%= GetLocalizedText("PageContent.UploadContent")%></a>
		</div>
		<div class="clear"></div>
	</div>
	<div id="detail-block">
		<ul id="detail-block-list">
			<li>
				<div class="upload-banners-inactive">
				</div>
			</li>
			<li>
				<div class="block-guideline">
					<h2 class ="darkblue-text"><%= GetLocalizedText("PageBanners.GuidelinesTitle")%></h2>
					<ul id="guideline-banner-detail">
						<%= GetLocalizedText("PageBanners.GuidelinesDetail")%>
					</ul>
				</div>						
			</li>
		</ul>
		<div id="detail-banner">
			<div id="detail-banner-left" class="dashed-blue left banner-trigger progressbar-wrapper" label="160_600" title="<%=GetLocalizedText("Label.ClickToUpload")%>">
				<div class="delete-banner"></div>
				<div class="banner-label"><%= GetLocalizedText("PageBanners.LabelClickHereToUpload")%><%=GetLocalizedText("Dimension.160.600")%></div>
				<div class="progressbar-text"></div>
				<div class="progressbar-percent">
					<div class="progressbar-percent-text">
						<div class="progressbar-percent-text-inner"></div>
					</div>
				</div>
			</div>
			<div id="detail-banner-right" class="left">
				<div id="detail-banner-center-top" class="dashed-blue left banner-trigger progressbar-wrapper" label="300_250" title="<%=GetLocalizedText("Label.ClickToUpload")%>">
					<div class="delete-banner"></div>
					<div class="banner-label"><%= GetLocalizedText("PageBanners.LabelClickHereToUpload")%><%=GetLocalizedText("Dimension.300.250")%></div>
					<div class="progressbar-text"></div>
					<div class="progressbar-percent">
						<div class="progressbar-percent-text">
							<div class="progressbar-percent-text-inner"></div>
						</div>
					</div>
				</div>
				<div id="detail-banner-right-top" class="dashed-blue left banner-trigger progressbar-wrapper" label="143_60" title="<%=GetLocalizedText("Label.ClickToUpload")%>">
					<div class="delete-banner"></div>
					<div class="banner-label"><%= GetLocalizedText("PageBanners.LabelClickHereToUpload")%><%=GetLocalizedText("Dimension.143.60")%></div>
					<div class="progressbar-text"></div>
					<div class="progressbar-percent">
						<div class="progressbar-percent-text">
							<div class="progressbar-percent-text-inner"></div>
						</div>
					</div>
				</div>
				<div id="detail-banner-right-bottom" class="dashed-blue left banner-trigger progressbar-wrapper" label="234_60" title="<%=GetLocalizedText("Label.ClickToUpload")%>">
					<div class="delete-banner"></div>
					<div class="banner-label"><%= GetLocalizedText("PageBanners.LabelClickHereToUpload")%><%=GetLocalizedText("Dimension.234.60")%></div>
					<div class="progressbar-text"></div>
					<div class="progressbar-percent">
						<div class="progressbar-percent-text">
							<div class="progressbar-percent-text-inner"></div>
						</div>
					</div>
				</div>
				<div id="detail-banner-center" class="dashed-blue left banner-trigger progressbar-wrapper" label="468_60" title="<%=GetLocalizedText("Label.ClickToUpload")%>">
					<div class="delete-banner"></div>
					<div class="banner-label"><%= GetLocalizedText("PageBanners.LabelClickHereToUpload")%><%=GetLocalizedText("Dimension.468.60")%></div>
					<div class="progressbar-text"></div>
					<div class="progressbar-percent">
						<div class="progressbar-percent-text">
							<div class="progressbar-percent-text-inner"></div>
						</div>
					</div>
				</div>
				<div id="detail-banner-center-bottom" class="dashed-blue left banner-trigger progressbar-wrapper" label="180_90" title="<%=GetLocalizedText("Label.ClickToUpload")%>">
					<div class="delete-banner"></div>
					<div class="banner-label"><%= GetLocalizedText("PageBanners.LabelClickHereToUpload")%><%=GetLocalizedText("Dimension.180.90")%></div>
					<div class="progressbar-text"></div>
					<div class="progressbar-percent">
						<div class="progressbar-percent-text">
							<div class="progressbar-percent-text-inner"></div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
<div style="visibility: hidden">
	<input type="hidden" runat="server" ID="NewBannerImageName" />
</div>

<telerik:RadWindow runat="server" ID="IEUploadWindow" Behaviors="Close, Move" VisibleStatusbar="False" Width="500" Height="140">
	<ContentTemplate>
		<div id="ie-radupload-content">				 
			
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<asp:Label ID="Label2" runat="server"><%= GetLocalizedText("PageContent.UploadInstruction")%></asp:Label>
					</td>
					 <td>
						 <telerik:RadAsyncUpload runat="server" ID="RadAsyncUpload1" 
							AllowedFileExtensions="jpeg,jpg,gif,png,bmp" MaxFileSize="10000000"
							OnClientProgressUpdating="onClientProgressUpdating" 
							OnClientFileUploaded="clientFileUploaded" OnClientFileSelected="OnClientFileSelected"
							OnClientFileUploading="onClientFileUploading" OnClientValidationFailed="CheckValidate"
							MultipleFileSelection="Disabled" OnFileUploaded="FileUploaded">                   	
						</telerik:RadAsyncUpload>
					</td>
				</tr>
			</table>
		</div>
	</ContentTemplate>
</telerik:RadWindow>

<div style="display: none" id="current-banners-list">
	<asp:Repeater runat="server" ID="rptCurrentBanner">
		<ItemTemplate>
			<div class="crr-banner-item" id="<%# Eval("BannerType").ToString() %>">
				<img src="<%# Eval("BannerUrl").ToString() %>" />
			</div>
		</ItemTemplate>
	</asp:Repeater>
</div>
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
	<script type="text/javascript">
		var baseUrl = '<%= Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd(Convert.ToChar("/")) + "/" %>';
		var bannerFolderUrl = '<%= GetUploadRelativePath() %>';
		//var uploader = $find('<%= RadAsyncUpload1.ClientID %>');
		var bannerInAction = new Array();
		var fileInAction = new Array();
		var invalidRows = new Array();
		var bannerClicked = null;
		var stepValue = '<%= GetStepValue("StepBanner") %>';
		var uploadedBannerCount = '<%= UploadedBannerCount %>';
		var RadAsyncUploader = '<%= RadAsyncUpload1.ClientID %>';
		var ImageExtensions = ['.jpg', '.png', '.gif', '.bmp', '.jpeg'];

		$(function () {
			initApp();
			initBannerUploaders('');
			initBannerDelete();
			initUploadedBanner();
			$('#StepBannerLink').find('.step-block').addClass('active');
			if (parseInt(uploadedBannerCount) > 0)
				ActivatePage();
			if ($.browser.msie && parseInt($.browser.version, 10) <= 8) {
				if (!Array.prototype.indexOf) {
					Array.prototype.indexOf = function (elt /*, from*/) {
						var len = this.length >>> 0;

						var from = Number(arguments[1]) || 0;
						from = (from < 0) ? Math.ceil(from) : Math.floor(from);
						if (from < 0)
							from += len;

						for (; from < len; from++) {
							if (from in this && this[from] === elt)
								return from;
						}
						return -1;
					};
				}
			}
		});
		
		function initUploadedBanner() {
			if ($('.crr-banner-item').length == 0) return;
			$('.crr-banner-item').each(function () {
				var bannerType = $(this).attr('id');
				var bannerDimension = bannerType.split('_');
				var bannerHolder = $('.banner-trigger[label="' + bannerType + '"]');
				$(this).find('img').attr({ 'width': bannerDimension[0] + 'px', 'height': bannerDimension[1] + 'px' }).appendTo(bannerHolder);
				bannerHolder.find('.banner-label').hide();
				bannerHolder.css('border', 'none');
			});
		}
		function CheckValidate() {
			alert("Invalid file specified. Please try again.");
			bannerClicked.find('.progressbar-percent-text').hide();
			bannerClicked.find('.progressbar-text').hide();
			if (bannerClicked.find('img').length == 1) {
				bannerClicked.find('img').show();
				bannerClicked.css('border', 'none');
				bannerClicked.find('.banner-label').hide();
			} else {
				bannerClicked.css('border', '2px dashed #449bc5');
				bannerClicked.find('.banner-label').show();
			}

			initBannerUploaders();
			initBannerDelete();
		}

		function initBannerUploaders(targetBanner) {
			if (targetBanner == '' || targetBanner == null) {
				$('.banner-trigger').unbind().click(function () {
					bannerClicked = $(this);
					if ($.browser.msie && parseInt($.browser.version, 10) <= 9) {
						$find('<%= IEUploadWindow.ClientID %>').show();
						return;
					} else {
						$('#' + RadAsyncUploader).find('input.ruFileInput').click();
					}
				});
				return;
			}
			targetBanner.unbind().click(function () {
				bannerClicked = $(this);
				$('.RadAsyncUpload').find('input.ruFileInput').click();
			});
		}
		
		function initBannerDelete() {
			//init delete button
			$('.banner-trigger').unbind('hover').hover(function () {
				if ($(this).find('img').length == 0)
					return;
				$(this).find('.delete-banner').show();
			}, function () {
				$(this).find('.delete-banner').hide();
			});

			$('.delete-banner').unbind('click').click(function (e) {
				e.stopPropagation();
				var bannerWrapper = $(this).parent('.banner-trigger');
				var bannerType = bannerWrapper.attr('label');
				var uploadedBannerCount = 0;
				var thisEl = $(this);

				$('.banner-trigger').each(function () {
					if ($(this).find('img').length == 1)
						uploadedBannerCount++;
				});

				if (uploadedBannerCount < 2) {
					alert('<%= GetLocalizedText("PageBanners.MsgMustBeAtLeastOneBanner") %>');
					return;
				}

				$.ajax({
					type: 'POST',
					url: document.URL,
					dataType: 'text',
					data: { ajaxaction: 'deletebanner', bannertype: bannerType },
					beforeSend: function (xhr) {
						xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
						//bannerWrapper.unbind('click');
					},
					success: function (responseMsg) {
						thisEl.hide();
						bannerWrapper.find('img').remove();
						//initBannerUploaders(bannerWrapper);
						bannerWrapper.css('border', '2px dashed #449bc5');
						bannerWrapper.find('.banner-label').show();
					}
				});
			});
		}

		function OnClientFileSelected(sender, args) {
			var currentFileName = args.get_fileName();
			if (!ValidateImageExtentions(currentFileName)) return;
			//bannerClicked.unbind('click');
			if (fileInAction.indexOf(currentFileName) >= 0) {
				invalidRows.push(args.get_row());
				alert('This file is currently uploading');
				return;
			}
			$('.banner-trigger').unbind('click');
			$telerik.$(args.get_row()).addClass("ruUploading");
			bannerInAction.push(bannerClicked.attr('label'));
			fileInAction.push(args.get_row());

			if (bannerClicked.find('img').length > 0) {
				bannerClicked.css('border', '2px dashed #449bc5');
				bannerClicked.find('img').hide();
			}

			bannerClicked.find('.banner-label').show();
			bannerClicked.find('.progressbar-percent-text').show();
			bannerClicked.find('.progressbar-percent-text-inner').html('0%').show();
			bannerClicked.find('.progressbar-text').show();
			if (bannerClicked.find('.progressbar-text').html() == '')
				bannerClicked.find('.progressbar-text').html('Uploading...');
		}

		function onClientFileUploading(sender, args) {
			if (invalidRows.indexOf(args.get_row()) >= 0) {
				args.set_cancel(true);
				invalidRows.splice(invalidRows.indexOf(args.get_row()), 1);
			}
		}

		function onClientProgressUpdating(sender, args) {
			if (invalidRows.indexOf(args.get_row()) >= 0) {
				return;
			}
			var data = args.get_data();
			var percents = data.percent;
			var fileSize = data.fileSize;
			var fileName = data.fileName;
			var uploadedBytes = data.uploadedBytes;
			if (percents >= 100) return;
			var crrIndex = fileInAction.indexOf(args.get_row());
			var crrBanner = bannerInAction[crrIndex];
			var crrBannerObj = $('div.progressbar-wrapper[label="' + crrBanner + '"]');
			crrBannerObj.find('.progressbar-percent-text').show();
			crrBannerObj.find('.progressbar-percent-text-inner').show();
			crrBannerObj.find('.progressbar-text').html('Uploading...').show();
			//crrBannerObj.css('width', percents + '%');
			crrBannerObj.find('.progressbar-percent').animate(
				{ width: percents + '%' },
				{
					duration: 500,
					step: function (now, fx) {
						var data = Math.round(now) + '%';
						crrBannerObj.find('.progressbar-percent-text-inner').html(data);
					}
				}
			);
		}

		function clientFileUploaded(sender, args) {
			if ($.browser.msie && parseInt($.browser.version, 10) <= 9) {
				$find('<%= IEUploadWindow.ClientID %>').hide();
			}
			/*if (invalidRows.indexOf(args.get_row()) >= 0) {
				return;
			}*/
			initBannerUploaders();
			var fileName = args.get_fileName();
			var crrIndex = fileInAction.indexOf(args.get_row());
			var crrBanner = bannerInAction[crrIndex];
			var crrBannerObj = $('div.progressbar-wrapper[label="' + crrBanner + '"]');
			var bannerDemension = crrBanner.split('_');

			crrBannerObj.find('.progressbar-percent').animate(
				{ width: 100 + '%' },
				{
					duration: 100,
					step: function (now, fx) {
						var data = Math.round(now) + '%';
						crrBannerObj.find('.progressbar-percent-text-inner').html(data);
					},
					complete: function () {
						crrBannerObj.find('.progressbar-percent').css('width', '0');
						crrBannerObj.find('.progressbar-percent-text').hide();
						crrBannerObj.find('.progressbar-text').hide();
						//crrBannerObj.find('img').remove();
						$.ajax({
							type: "POST",
							url: $('form:first').attr('action'),
							beforeSend: function (xhr) {
								xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
							},
							dataType: 'html',
							data: $('form:first').serialize(),
							success: function (firstResponse) {
								//console.log(firstResponse);
								var bannerUrl = firstResponse; //baseUrl + bannerFolderUrl + args.get_fileInfo().FileName;
								if (bannerUrl.length > 255) {
									alert("Banner " + bannerDemension[0] + " x " + bannerDemension[1] + " upload failed");
									if (crrBannerObj.find('img').length > 0) {
										crrBannerObj.find('img').show();
										crrBannerObj.css('border', 'none');
										crrBannerObj.find('.banner-label').hide();
									}
									return;
								}
								ClearUploadedFile();
								crrBannerObj.find('img').remove();
								SaveBannerDetail(crrBanner, crrIndex, bannerUrl);
							},
							error: function () {
								//SaveBannerDetail(crrBanner, crrIndex, bannerUrl);
							}
						});
					}
				}
			);
		}

		function SaveBannerDetail(crrBanner, crrIndex, bannerUrl) {
			//var bannerUrl = $.trim($('#<%= NewBannerImageName.ClientID %>').val());
			if (bannerUrl == "") {
				console.log("Null banner image");
				return;
			}
			//console.log('begin save banner detail');
			var crrBannerObj = $('div.progressbar-wrapper[label="' + crrBanner + '"]');
			var bannerDemension = crrBanner.split('_');
			$.ajax({
				type: 'POST',
				url: document.URL,
				dataType: 'text',
				data: { stepvalue: stepValue, bannertype: crrBanner, bannerurl: bannerUrl },
				beforeSend: function (xhr) {
					xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
				},
				success: function (responseMsg) {
					if (responseMsg != '')
						stepValue = responseMsg;

					fileInAction.splice(crrIndex, 1);
					bannerInAction.splice(crrIndex, 1);
					LoadNewBannerImage(bannerUrl, crrBannerObj, bannerDemension[0], bannerDemension[1], 0);
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log('inside bug');
					console.log(xhr);
					console.log(thrownError);
				}
			});
		}

		function LoadNewBannerImage(bannerUrl, crrBannerObj, width, height, failedCount) {
			if (failedCount >= 3) {
				window.location = document.URL;
				return;
			}
			var newImg = $('<img>').attr({
				'src': bannerUrl,
				'height': height + 'px',
				'width': width + 'px'
			});
			
			crrBannerObj.css('border', 'none');

			$(newImg).load(function () {
				crrBannerObj.append(newImg);
				crrBannerObj.find('.banner-label').hide();
				ActivatePage();
				initBannerUploaders();
				initBannerDelete();
			});

			$(newImg).error(function () {
			    $(newImg).remove();
			    failedCount = failedCount + 1;
			    LoadNewBannerImage(bannerUrl, crrBannerObj, width, height, failedCount);
			});
		}

		function ClearUploadedFile() {
			var upload = $find("<%= RadAsyncUpload1.ClientID %>");
			var inputs = upload.getUploadedFiles();
			for (i = inputs.length - 1; i >= 0; i--) {
				//if (!upload.isExtensionValid(inputs[i].value))
				upload.deleteFileInputAt(i);
				upload.updateClientState();
			} 
		}
		
		function ActivatePage() {
			var uploadedBannerCount = 0;
			$('.banner-trigger').each(function() {
				if ($(this).find('img').length == 1)
					uploadedBannerCount++;
			});
			//if (uploadedBannerCount < $('.banner-trigger').length) return;
			if (uploadedBannerCount <= 0) return;
			$('#detail-block-list .upload-banners-inactive').addClass('upload-banners-active').removeClass('upload-banners-inactive');
			$('#btn-goto-uploadcontent').removeClass('btn-darkblue-inactive').attr('href', '<%= DotNetNuke.Common.Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "StepContent", "mid", ModuleId.ToString()) %>');
		}

		function ValidateImageExtentions(fileName) {
			var sFileName = fileName;
			var blnValid = false;
			if (sFileName.length > 0) {
				for (var i = 0; i < ImageExtensions.length; i++) {
					var sCurExtension = ImageExtensions[i];
					if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
						blnValid = true;
						break;
					}
				}
			}
			return blnValid;
		}
	</script>
</telerik:RadScriptBlock>