﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DotNetNuke.Common;

namespace CR.DnnModules.SelfService.Common
{
	public class BaseMethods
	{
		public List<SelfServiceStep> GetStepList()
		{
			List<SelfServiceStep> stepList = new List<SelfServiceStep>();

			return stepList;
		}
	}

	public class SelfServiceStep
	{
		public string StepName { get; set; }
		public string StepUrl { get; set; }
	}
}