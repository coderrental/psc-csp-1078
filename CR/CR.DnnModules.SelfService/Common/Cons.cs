﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CR.DnnModules.SelfService.Common
{
	public class Cons
	{
		public const string SETTING_UPLOAD_FOLDER = "SettingUploadFolder";
		public const string SETTING_FOLDER_LOGO = "SettingFolderLogo";
		public const string SETTING_FOLDER_BANNER = "SettingFolderBanner";
		public const string SETTING_IMAGE_URL = "SettingImageUrl";
		public const string SETTING_HIDE_DESCRIPTION_BY_DEFAULT = "SettingHideDescriptionByDefault";
		public const string SETTING_USER_ROLE_NAME = "SettingUserRoleName";
		public const string SETTING_CSPDNN_CONNECTIONSTRING = "SettingCspDnnConnectionString";
		public const string SETTING_WEBTRENDS_FOLDER = "SettingWebtrendsFolder";
	}
}