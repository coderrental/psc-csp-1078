﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using CR.DnnModules.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;
using DotNetNuke.Services.Log.EventLog;
using CR.ContentObjectLibrary.Data;

namespace CR.DnnModules.SelfService.Common
{
	public class DnnModuleBase : PortalModuleBase
	{
		protected EventLogController DnnEventLog;
		protected ModuleActionCollection MyActions;
		private string _localResourceFile;
		protected CspDataContext CspDataContext;
		protected int _cspId;
		protected List<string> SelfServiceSteps; 
		protected List<string> FinishedSteps = new List<string>();
		protected bool AllDone = false;
		protected company _coInfo;
		protected string langCode = "";
		protected int _langId = 1;
		protected string _projectName = "";

		/// <summary>
		/// Handles the Init event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected virtual void Page_Init(object sender, EventArgs e)
		{
			//Init Local Resource file
			_localResourceFile = Utils.GetCommonResourceFile("CR.DnnModules.SelfService", LocalResourceFile);
			//Init module title
			ModuleConfiguration.ModuleTitle = GetLocalizedText("ModuleTitle");
			//Init csp data context
			CspDataContext = new CspDataContext(Utils.GetConnectionString(PortalId));
			if (Settings.ContainsKey(Cons.SETTING_CSPDNN_CONNECTIONSTRING) && !String.IsNullOrEmpty(Settings[Cons.SETTING_CSPDNN_CONNECTIONSTRING].ToString()))
				CspDataContext = new CspDataContext(Config.GetConnectionString(Settings[Cons.SETTING_CSPDNN_CONNECTIONSTRING].ToString()));
			_cspId = Utils.GetIntegrationKey(UserInfo);
			if (_cspId < 0) return;

			langCode = System.Threading.Thread.CurrentThread.CurrentCulture.Name.ToUpper();
			language crrLangObj = CspDataContext.languages.SingleOrDefault(a => a.BrowserLanguageCode.ToLower() == langCode.ToLower());
			if (crrLangObj == null)
				crrLangObj = CspDataContext.languages.SingleOrDefault(a => a.BrowserLanguageCode.ToLower() == "en-us");
			_langId = crrLangObj.languages_Id;
			if (langCode.IndexOf('-') >= 0)
				langCode = langCode.Split('-').First().ToUpper();
			try
			{
				GetFinishedSteps();
				GetCompanyInfo();
				GetProjectName();
			}
			catch (Exception)
			{
				SqlConnection dnnCon = new SqlConnection(Config.GetConnectionString());
				String sqlQuery =
				String.Format("DELETE * FROM CSPSelfService_UserSteps WHERE UserId={0} AND TabModuleId={1} AND PortalId={2}", UserId,
							  TabModuleId, PortalId);
				dnnCon.Open();
				SqlCommand cmd = new SqlCommand(sqlQuery, dnnCon);
				cmd.ExecuteNonQuery();
			}
			
		}

		/// <summary>
		/// Gets the finished steps.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>5/22/2013</datetime>
		private void GetFinishedSteps()
		{
			StepNavigation stepNavigation = new StepNavigation();
			stepNavigation.InitStepList();
			SelfServiceSteps = stepNavigation.StepList;
			SqlConnection dnnCon = new SqlConnection(Config.GetConnectionString());
			DataSet ds = new DataSet("New_DataSet");
			String sqlQuery =
				String.Format("SELECT * FROM CSPSelfService_UserSteps WHERE UserId={0} AND TabModuleId={1} AND PortalId={2}", UserId,
				              TabModuleId, PortalId);
			dnnCon.Open();
			SqlCommand cmd = new SqlCommand(sqlQuery, dnnCon);
			SqlDataAdapter adptr = new SqlDataAdapter();
			adptr.SelectCommand = cmd;
			adptr.Fill(ds);
			dnnCon.Close();

			DataTable dtCSPSelfServiceUserStep = ds.Tables[0];
			if (dtCSPSelfServiceUserStep.Rows.Count == 0) return;

			foreach (DataRow dsrow in dtCSPSelfServiceUserStep.Rows)
			{
				FinishedSteps.Add(dsrow["StepName"].ToString());
				if (dsrow["StepName"].ToString() == stepNavigation.StepList[stepNavigation.StepList.Count - 1])
					AllDone = true;
			}
		}

		/// <summary>
		/// Gets the step value.
		/// </summary>
		/// <param name="stepName">Name of the step.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>5/22/2013</datetime>
		protected string GetStepValue(string stepName)
		{
			string stepValue = "";
			string sqlQuery =
				String.Format(
					"SELECT StepValue FROM CSPSelfService_UserSteps WHERE UserId={0} AND TabModuleId={1} AND PortalId={2} AND StepName='{3}'",
					UserId,
					TabModuleId, PortalId, stepName);
			SqlConnection dnnCon = new SqlConnection(Config.GetConnectionString());
			dnnCon.Open();
			SqlCommand command = new SqlCommand(sqlQuery, dnnCon);
			SqlDataReader Reader = command.ExecuteReader();
			while (Reader.Read())
			{
				stepValue = Reader[0].ToString();
			}
			dnnCon.Close();
			return stepValue;
		}

		/// <summary>
		/// Updates the step value.
		/// </summary>
		/// <param name="stepName">Name of the step.</param>
		/// <param name="stepValue">The step value.</param>
		/// <author>ducuytran</author>
		/// <datetime>5/22/2013</datetime>
		protected void UpdateStepValue(string stepName, string stepValue)
		{
			String sqlUpdate = "";
			int userStepId = 0;
			string sqlQuery =
				String.Format(
					"SELECT Id FROM CSPSelfService_UserSteps WHERE UserId={0} AND TabModuleId={1} AND PortalId={2} AND StepName='{3}'",
					UserId,
					TabModuleId, PortalId, stepName);
			SqlConnection dnnCon = new SqlConnection(Config.GetConnectionString());
			dnnCon.Open();
			SqlCommand command = new SqlCommand(sqlQuery, dnnCon);
			SqlDataReader Reader = command.ExecuteReader();
			if (Reader.HasRows)
			{
				while (Reader.Read())
				{
					userStepId = int.Parse(Reader[0].ToString());
				}
				sqlUpdate = String.Format("UPDATE CSPSelfService_UserSteps SET StepValue='{0}', DateChanged='{1}' WHERE Id={2}", stepValue, DateTime.Now, userStepId);
				
			}
			else
			{
				sqlUpdate = String.Format("INSERT INTO CSPSelfService_UserSteps (UserId, StepName, StepValue, DateCreated, DateChanged, PortalId, TabModuleId) VALUES ({0}, '{1}', '{2}', '{3}', '{4}', {5}, {6})", UserId, stepName, stepValue, DateTime.Now, DateTime.Now, PortalId, TabModuleId);
			}
			Reader.Close();
			//command.Dispose();
			command = new SqlCommand(sqlUpdate, dnnCon);
			command.ExecuteNonQuery();
			dnnCon.Close();
		}

		/// <summary>
		/// Get localized text for the current portal language
		/// </summary>
		/// <param name="key">Resource Key</param>
		/// <returns>Localized text</returns>
		public string GetLocalizedText(string key)
		{
			return Localization.GetString(key, _localResourceFile);
		}

		/// <summary>
		/// Write message to Dnn event view
		/// </summary>
		/// <param name="message">message to write</param>
		public void Log(string message)
		{
			DnnEventLog.AddLog("DEBUG", message, PortalSettings, UserId, EventLogController.EventLogType.ADMIN_ALERT);
		}

		/// <summary>
		/// Gets the content field value.
		/// </summary>
		/// <param name="contentPage">The content page.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns></returns>
		/// <author>ducuytran - 2/27/2013</author>
		public string GetContentFieldValue(content contentPage, string fieldName)
		{
			if (contentPage == null)
				return string.Empty;

			content_field cf = contentPage[fieldName];
			return (cf != null) ? (string.IsNullOrEmpty(cf.value_text) ? string.Empty : cf.value_text) : string.Empty;
		}

		/// <summary>
		/// Setups the upload folder.
		/// </summary>
		/// <param name="folderPath">The folder path.</param>
		/// <author>ducuytran</author>
		/// <datetime>5/20/2013</datetime>
		public void SetupUploadFolder(string folderPath)
		{
			if (String.IsNullOrEmpty(folderPath)) return;
			if (!Directory.Exists(folderPath))
				Directory.CreateDirectory(folderPath);
		}

		/// <summary>
		/// Haves the admin permission.
		/// </summary>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>5/22/2013</datetime>
		public bool HaveAdminPermission()
		{
			if (UserInfo.IsSuperUser || UserInfo.IsInRole(PortalSettings.AdministratorRoleName))
				return true;
			return false;
		}

		public void GetCompanyInfo()
		{
			_coInfo = CspDataContext.companies.FirstOrDefault(a => a.companies_Id == _cspId);
		}

		private void GetProjectName()
		{
			string ParameterTypeName = "CSP_Project_Name";
			CspUtils cspUtils = new CspUtils(CspDataContext);
			_projectName = cspUtils.GetCompanyParameter(_cspId, ParameterTypeName);
		}
	}
}