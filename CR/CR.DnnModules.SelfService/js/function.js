﻿function initApp() {
	$('.step-block').each(function () {
		var stepText = $(this).find('.step-text');
		var stepTextHeight = stepText.height();
		var thisHeight = $(this).height();
		if (stepTextHeight >= thisHeight) return;
		var marginTop = (thisHeight - stepTextHeight) / 2;
		stepText.css('margin-top', marginTop + 'px');
	});
}