
//window.setTimeout(resizer,3000);

/*
Require:
	PDFUrl String = the path to the PDF which you want to be shown
Expects HTML body to be empty

*/

function cspGetThisJSQuerystring(key, default_) {
    if (default_ == null)
        default_ = "";
    key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");

	var scripts = document.getElementsByTagName('script');
	var myScript = scripts[ scripts.length - 1 ];

//    var qs = regex.exec(window.location.href);
    var qs = regex.exec(myScript.src);
    if (qs == null)
        return default_;
    else
        return qs[1];
}


var URL = window.unescape(cspGetThisJSQuerystring('PDFURL'));
//InitPDFLoader(URL);
var NumOfResizes = 0;
var pid = 0;

(function InitPDFLoader(PDFURL) 
{

	window.onload = function() {
		AddSwirly();
		AppendProperlySizedPDFIframe();
		//if (navigator.userAgent.indexOf("IE 7") != -1)
//			{
			window.setTimeout(CheckToSeeIfIframeNeedsAResize,3000);		
	//		}
		//else
			//{
			pid = window.setInterval(CheckToSeeIfIframeNeedsAResize,200);		
			//}
		//window.setTimeout(CheckToSeeIfIframeNeedsAResize,5000)
		};


function CheckToSeeIfIframeNeedsAResize() 
	{	
	NumOfResizes++;
	var pdfIframe = document.getElementById("csp_pdfIframe"); // assume the loader icon is the first child
	if (typeof pdfIframe == 'undefined' && pdfIframe)
		{
		}
	else // if its not undefined and not null
		{
		var WhatTheSizeShouldBe = GetSafeIframeSize() + "px";
		if (pdfIframe.style.height != WhatTheSizeShouldBe)
			{
			pdfIframe.style.height = WhatTheSizeShouldBe;
			}		
		}	
	if (NumOfResizes > 30)
		{
		window.clearInterval(pid);
		}
	}
function AddSwirly()
	{
	// add the swirly to the page
	var swirly = document.createElement("img");
	swirly.src = "/ui/UniversalResources/images/swirlydirly.gif"; // make sure you can assess this from the domain you are using
	swirly.id = "csp_hideme";
	document.body.appendChild(swirly);
	}

function GetSafeIframeSize()
	{
	var db = document.body;
	var dde = document.documentElement;
	var HeightOffset = 20;
	//		navigator.userAgent	"Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)"	String
	if (navigator.userAgent.indexOf("IE 7") != -1)
		{
		HeightOffset = 30;
		} //if it is ie 7 alter the offset a bit. 
	var docHeight = Math.max(db.scrollHeight, dde.scrollHeight, db.offsetHeight, dde.offsetHeight, db.clientHeight, dde.clientHeight);
	return Math.max((docHeight - HeightOffset),100); // add safe default height
	}
	
function AppendProperlySizedPDFIframe()
	{
	var db = document.body;
	var iframe0 = document.createElement("iframe");
	iframe0.id = "csp_pdfIframe";
	iframe0.className = "iframetostyle";
	var size = GetSafeIframeSize() ;
	iframe0.style.height =  size + "px";
	iframe0.style.width = "100%"; 
	iframe0.style.margin = "0px"; 
	iframe0.style.padding = "0px";
	iframe0.style.bottom = "0px"; 
	iframe0.style.overflow = "hidden"; 
	iframe0.scrolling = "no";
	iframe0.frameborder="0";
	//iframe0.src = "/ftp/campaign/homedeco/montis_PDFshowcase/nl/montis.pdf";
	iframe0.src = PDFURL;
	var swirly = document.getElementById("csp_hideme"); // assume the loader icon is the first child
	db.removeChild(swirly);
	db.appendChild(iframe0);
	
	}
})(URL); // In theory calls the init function with the URL passed in
	
/*
Ensure:
	Will drop swirly right away on the page while it waits for the PDF to be ready to load (will remove swirly after pdf is rendered)
	Will have a fixed time based event (3 seconds) to ensure content size's properly when loaded via a Thickbox from a CSP script
	Will Render the PDF inside the body of the html script is embedded on and size it properly in all cases (including IE 7)	
	Will add the style as needed for the iframe
			.iframetostyle 
			{
			width: 100%; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; bottom: 0px; overflow-x: hidden; overflow-y: hidden; 
			}
*/

/*Future:
	Add flag to pop pdf in new tab instead and close the calling lightbox / thickbox
*/