﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StepNavigation.ascx.cs" Inherits="CR.DnnModules.SelfService.StepNavigation" %>
<%@ Import Namespace="System.ComponentModel" %>
<%@ Import Namespace="DotNetNuke.Entities.Portals" %>
<ul>
	<asp:Repeater runat="server" ID="rptStepList">
		<ItemTemplate>
			<li>
				<a id="<%# Container.DataItem %>Link" href="<%# GetStepUrl(Container.DataItem) %>">
					<div class="step-block">
						<span class="step-text">
						<%# GetStepLabel(Container.DataItem) %>
						</span>
					</div>
				</a>
			</li>
		</ItemTemplate>
	</asp:Repeater>
</ul>