﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StepContent.ascx.cs" Inherits="CR.DnnModules.SelfService.StepContent" %>
<%@ Import Namespace="CR.ContentObjectLibrary.Data" %>
<%@ Import Namespace="DotNetNuke.Common" %>
<%@ Import Namespace="DotNetNuke.Services.Localization" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="tagprefix" src="StepNavigation.ascx" tagName="StepNavigation" %>
<div id="step-navigation">
    <tagprefix:StepNavigation ID="StepNavigation" runat="server" />
</div>
				
<div id="page-content">
	<div id="description-block">
		<div id="description-text">
			<h1 class="darkblue-text"><%= GetLocalizedText("PageContent.Title")%></h1>
			<p class="darkblue-text"><%= GetLocalizedText("PageContent.Description")%></p>
		</div>
		<div id="description-image">
			<a id="btn-goto-subscription" class="btn-darkblue btn-darkblue-inactive" href="javascript: void(0);"><%= GetLocalizedText("PageContent.Subscription")%></a>
		</div>
		<div class="clear"></div>
	</div>
	<div id="detail-block">
		<div id="page-guideline" class="guideline-inactive">
			<h2 class="darkblue-text"><%= GetLocalizedText("PageContent.Guidelines")%></h2>
			<ul id="guideline-content-detail">
				<%= GetLocalizedText("PageContent.GuideLineDetail")%>
			</ul>
		</div>
		<div id="content-upload-section">
			<div id="content-upload-control" class="float-left">
				<a id="btn-uploadcontent" class="btn-lightblue" href="javascript: void(0);"><%= GetLocalizedText("PageContent.BtnUpload")%></a>
			</div>
			<div id="content-upload-progress" class="float-left">
				<div id="content-filename">
				</div>
				<div id="content-upload-progressbar" class="progressbar-wrapper">
					<div class="progressbar-text"></div>
					<div class="progressbar-percent">
						<div class="progressbar-percent-text">
							<div class="progressbar-percent-text-inner"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>		
		</div>
        <div id ="content-banner" class="hide">
            <h2 class="darkblue-text"><%= GetLocalizedText("PageContent.DetailTitle")%></h2>
            <p class="darkblue-text">
              <%= GetLocalizedText("PageContent.DetaiDescription")%>
            </p>
            <div class="clear"></div>
            <div id="content-banner-detail">
                <telerik:RadComboBox ID="rcbBanner" runat="server" AutoPostBack="True">
                </telerik:RadComboBox>
                <div id ="content-banner-images">
                    <script type="text/javascript" src="<%=GetScriptUrl()%>" ></script>
                </div>
            </div>
            
        </div>
	</div>
</div>

<telerik:RadWindow runat="server" ID="UploadWindow" Modal="True" Width="500" Height="200" VisibleStatusbar="False" Behaviors="Close,Move">
    <ContentTemplate>
    <div id="UploadPanel">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <asp:Label runat="server"><%= GetLocalizedText("PageContent.UploadInstruction")%></asp:Label>
                </td>
                 <td>
                     <telerik:RadAsyncUpload runat="server" ID="RadAsyncUpload1" OnClientProgressUpdating="onClientFileUploading"
						 OnClientFileUploaded="clientFileUploaded" OnClientFileSelected="OnClientFileSelected" 
						 MultipleFileSelection="Disabled" AllowedFileExtensions=".zip,.pdf" OnClientValidationFailed="CheckValidate" MaxFileSize="50000000">                     	
                     </telerik:RadAsyncUpload>
                </td>
            </tr>
        </table>
    </div>
    </ContentTemplate>
</telerik:RadWindow>
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
	<script type="text/javascript">
		var detailToggleLinkText = new Array("Hide the details", "Show the details");
		var crrPercent = 0;
		var stepValue = '<%= GetStepValue("StepContent") %>';
		var coBaseDomain = '<%= GetCompanyBaseDomain() %>';

		$(function () {
			if (coBaseDomain.indexOf('http') != 0)
				coBaseDomain = 'http://' + coBaseDomain;
			initApp();
			initUploadButton();
			$('#StepContentLink').find('.step-block').addClass('active');
			if (stepValue != '') {
				ActivatePage();
			}
		});

		function initUploadButton() {
			$('#btn-uploadcontent').unbind().click(function () {
				//$('.progressbar-percent').css('width', '0%');
				$('.progressbar-text').html('');
				$('.progressbar-percent-text-inner').html('');
				$('#content-filename').html('');
				$('#content-filename').hide();
				$('#content-upload-progress').hide();
				if ($.browser.msie && parseInt($.browser.version, 10) <= 9) {
					$find("<%=UploadWindow.ClientID %>").show();
					return;
				}
				$('.RadAsyncUpload').find('input.ruFileInput').click();

			});
		}

		function CheckValidate() {
		    alert("Invalid file specified. Please try again.");
		    $('#btn-uploadcontent').removeClass('btn-darkblue-inactive');
		    initUploadButton();
		    $('#content-filename').hide();
		    $('#content-upload-progress').hide();
		    $('#page-guideline').removeClass('guideline-active').addClass('guideline-inactive');
		    $('#btn-goto-subscription').removeClass('btn-darkblue-active').addClass('btn-darkblue-inactive').removeAttr('href');
		    $('#StepSubscribeLink').removeAttr('href');
		    $('#content-banner').addClass('hide');
		}

		function onClientFileUploading(sender, args) {
			var data = args.get_data();
			var percents = data.percent;
			var fileSize = data.fileSize;
			var fileName = data.fileName;
			var uploadedBytes = data.uploadedBytes;
			crrPercent = percents;
			console.log(uploadedBytes);
			$('.progressbar-percent').css('width', Math.round(percents) + '%');
			$('.progressbar-percent-text-inner').html(Math.round(percents) + '%');

			$('.progressbar-percent-text').show();
			if ($('.progressbar-text').html() == '')
				$('.progressbar-text').html('Uploading...');

        }
        function OnClientFileSelected(sender, args) {
        	$('#content-upload-progress').show();
            var currentFileName = args.get_fileName();
            $('#content-filename').html(currentFileName).show();
            $telerik.$(args.get_row()).addClass("ruUploading");
            //btn-darkblue-inactive
            $('#btn-uploadcontent').addClass('btn-darkblue-inactive');
            $('#btn-uploadcontent').unbind('click');
        }
        function clientFileUploaded(sender, args) {
        	$find("<%=UploadWindow.ClientID %>").close();
            $('.progressbar-percent').css('width', '100%');
            $('.progressbar-percent-text').hide();
            $('.progressbar-text').html('File uploaded');
            $('#btn-uploadcontent').removeClass('btn-darkblue-inactive');
            $('#page-guideline').removeClass('guideline-inactive').addClass('guideline-active');
            $('#btn-goto-subscription').removeClass('btn-darkblue-inactive').attr('href', '<%= DotNetNuke.Common.Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "StepSubscribe", "mid", ModuleId.ToString()) %>');
            initUploadButton();
            $.ajax({
            	type: "POST",
            	url: $('form:first').attr('action'),
            	data: $('form:first').serialize(),
            	success: function (response) {
            		ActivatePage();
            		//console.log(response);
            		$.ajax({
            			type: 'POST',
            			url: document.URL,
            			dataType: 'text',
            			data: { fileName: args.get_fileName() },
            			beforeSend: function (xhr) {
            				xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
            			},
            			success: function (responseMsg) {
            			    if (responseMsg != '') {
            			        if (responseMsg.length > 100) { window.location = document.URL; return; }
            					alert(responseMsg);
            					$('#content-filename').hide();
            					$('#content-upload-progress').hide();
            					$('#page-guideline').removeClass('guideline-active').addClass('guideline-inactive');
            					$('#btn-goto-subscription').removeClass('btn-darkblue-active').addClass('btn-darkblue-inactive').removeAttr('href');
            					$('#StepSubscribeLink').removeAttr('href');
            					$('#content-banner').addClass('hide');
            				}
            				/*if (coBaseDomain != '') {
            					$.post(coBaseDomain + '/Csp/?csp_request_type=reload');
            				}*/
            			}
            		});
            	}
            });
        }

        function ActivatePage() {
            $('#btn-goto-subscription').removeClass('btn-darkblue-inactive').attr('href', '<%= DotNetNuke.Common.Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "StepSubscribe", "mid", ModuleId.ToString()) %>');
            $('#content-banner').removeClass('hide');
            $('#page-guideline').removeClass('guideline-inactive').addClass('guideline-active');
            $('#StepSubscribeLink').attr('href', '<%= DotNetNuke.Common.Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "StepSubscribe", "mid", ModuleId.ToString()) %>');
        }
	</script>
</telerik:RadScriptBlock>