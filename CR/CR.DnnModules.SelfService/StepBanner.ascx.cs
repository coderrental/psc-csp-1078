﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using CR.DnnModules.SelfService.Common;
using Telerik.Web.UI;
using Telerik.Web.UI.Upload;

namespace CR.DnnModules.SelfService
{
    public partial class StepBanner : DnnModuleBase
    {
		protected string UploadFolder = "~/Uploads";
		protected string BannerFolderName = "images";
		protected string UserFolder = "";
    	private int _contentTypeId = 12000;
    	private int _catId = 1;
    	private const int STAGE_PUBLISHED = 50;
    	protected Dictionary<string, string> bannerField;
		protected Dictionary<string, string> bannerHolderFiles;
    	protected int UploadedBannerCount = 0;
    	protected string ProjectName = "";

		protected override void Page_Init(object sender, EventArgs e)
		{
			base.Page_Init(sender, e);
			
			InitSettings();
		}

    	protected void Page_Load(object sender, EventArgs e)
		{
			if (Request.Headers["X-OFFICIAL-REQUEST"] == "TRUE")
			{
				if (Request.Params["stepvalue"] != null)
				{
					UpdateBanner();
				}
				else if (Request.Params["ajaxaction"] == "deletebanner")
					DeleteBanner();
				return;
			}
			LoadPageResources();
    		LoadCurrentBanners();
		}

    	/// <summary>
		/// Updates the banner.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>5/23/2013</datetime>
    	private void UpdateBanner()
    	{
    		string contentMainIdText = Request.Params["stepvalue"];
			content_main contentMain = CspDataContext.content_mains.FirstOrDefault(a => a.supplier_Id == _cspId);
			if (contentMain == null)
			{
				contentMainIdText = CreateContent();
			}
    		UpdateBannerDetail(contentMainIdText);
			UpdateStepValue("StepBanner", contentMainIdText);
			Response.Write(contentMainIdText);
			Response.Flush();
			Response.End();
    	}

		/// <summary>
		/// Deletes the banner.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>6/8/2013</datetime>
		private void DeleteBanner()
		{
			string bannerType = Request.Params["bannertype"];

			content_type contentType = CspDataContext.content_types.FirstOrDefault(a => a.content_types_Id == _contentTypeId);
			content_main contentMain = CspDataContext.content_mains.FirstOrDefault(a => a.supplier_Id == _cspId);
			Guid contentMainId = contentMain.content_main_Id;
			content bannerContent = CspDataContext.contents.FirstOrDefault(a => a.content_main_Id == contentMainId && a.content_types_languages_Id == _langId && a.stage_Id == STAGE_PUBLISHED);

			Utils.UpdateContentField(CspDataContext, contentType, bannerContent, bannerField[bannerType], bannerHolderFiles[bannerType]);
			Response.Write("1");
			Response.Flush();
			Response.End();
		}


		/// <summary>
		/// Updates the banner detail.
		/// </summary>
		/// <param name="contentMainIdText">The content main id text.</param>
		/// <author>ducuytran</author>
		/// <datetime>5/23/2013</datetime>
    	private void UpdateBannerDetail(string contentMainIdText)
    	{
    		string bannerType = Request.Params["bannertype"];
    		
			string staticUrl = "";
			string stringToReplace = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd(Convert.ToChar("/"));
			string bannerUrl = Request.Params["bannerurl"],
			       originalBannerUrl = bannerUrl;
			if (Settings.ContainsKey(Cons.SETTING_IMAGE_URL) && Settings[Cons.SETTING_IMAGE_URL].ToString() != string.Empty)
			{
				staticUrl = Settings[Cons.SETTING_IMAGE_URL].ToString();
				if (staticUrl.EndsWith("/"))
					staticUrl = staticUrl.TrimEnd('/');
			}
			if (!String.IsNullOrEmpty(staticUrl))
				bannerUrl = bannerUrl.Replace(stringToReplace, staticUrl);

			object[] parameters = new object[] { originalBannerUrl, Request.Params["bannertype"] };
			Thread thread = new Thread(new ParameterizedThreadStart(this.GoResizeBanner));
			thread.Priority = ThreadPriority.Lowest;
			thread.IsBackground = true;
			thread.Start(parameters);
    		//GoResizeBanner();

    		content_type contentType = CspDataContext.content_types.FirstOrDefault(a => a.content_types_Id == _contentTypeId);
			content_main contentMain = CspDataContext.content_mains.FirstOrDefault(a => a.supplier_Id == _cspId);
			Guid contentMainId = contentMain.content_main_Id;
    		content bannerContent = CspDataContext.contents.FirstOrDefault(a => a.content_main_Id == contentMainId && a.content_types_languages_Id == _langId && a.stage_Id == STAGE_PUBLISHED);

			if (bannerContent == null)
			{
				content newContent = new content
				{
					content_Id = Guid.NewGuid(),
					content_main_Id = contentMainId,
					active = true,
					stage_Id = STAGE_PUBLISHED,
					content_types_languages_Id = _langId
				};
				CspDataContext.contents.InsertOnSubmit(newContent);
				CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
				SaveNecessaryFields(newContent);
				bannerContent = newContent;
			}

			Utils.UpdateContentField(CspDataContext, contentType, bannerContent, bannerField[bannerType], bannerUrl);
    	}

    	/// <summary>
		/// Goes the resize banner.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>5/23/2013</datetime>
    	private void GoResizeBanner(object args)
    	{
			object[] argsArr = (object[])args;
    		string bannerFile = argsArr[0].ToString();//Request.Params["bannerurl"];
			string[] bannerType = argsArr[1].ToString().Split('_');// Request.Params["bannertype"].Split('_');
    		int newWidth = int.Parse(bannerType[0]);
			int newHeight = int.Parse(bannerType[1]);
    		try
    		{
				Uri uri = new Uri(bannerFile);
				string relativeUrl = uri.AbsolutePath;
				string serverBannerFile = Server.MapPath(relativeUrl);

				if (File.Exists(serverBannerFile))
				{
					System.Drawing.Image srcImg = System.Drawing.Image.FromFile(serverBannerFile);
					Bitmap newImage = new Bitmap(newWidth, newHeight);
					using (Graphics gr = Graphics.FromImage(newImage))
					{
						gr.SmoothingMode = SmoothingMode.HighQuality;
						gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
						gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
						gr.DrawImage(srcImg, new Rectangle(0, 0, newWidth, newHeight));
					}
					srcImg.Dispose();
					newImage.Save(serverBannerFile);
					newImage.Dispose();
				}
    		}
    		catch (Exception)
    		{
    			
    			throw;
    		}
    	}

    	private String CreateContent()
		{
			int supplierId = _coInfo.companies_Id;
			int consumerId = 0;
			int contentStagingSchemeId = 1;
			int publicationSchemeId = 11;
			category cat = CspDataContext.categories.FirstOrDefault(a => a.categoryId == _catId);
			content_main newContentMain = new content_main
			{
				content_main_Id = Guid.NewGuid(),
				content_main_key = _coInfo.companyname + " Microsite",
				content_identifier = _coInfo.companyname + " Microsite",
				content_types_Id = _contentTypeId,
				content_staging_scheme_Id = contentStagingSchemeId,
				publication_scheme_Id = publicationSchemeId,
				supplier_Id = supplierId,
				consumer_Id = consumerId
			};

			content newContent = new content
			{
				content_Id = Guid.NewGuid(),
				content_main = newContentMain,
				active = true,
				stage_Id = STAGE_PUBLISHED,
				content_types_languages_Id = _langId
			};
			newContentMain.content_categories.Add(new content_category
			{
				content_main = newContentMain,
				category = cat,
				publication_scheme_Id = publicationSchemeId
			});
			List<content_types_field> fieldDefs =
				CspDataContext.content_types_fields.Where(a => a.content_types_Id == _contentTypeId).ToList();

			CspDataContext.content_mains.InsertOnSubmit(newContentMain);
			CspDataContext.contents.InsertOnSubmit(newContent);
			CspDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
			SaveNecessaryFields(newContent);
			return newContentMain.content_main_Id.ToString();
		}

		/// <summary>
		/// Saves the necessary fields.
		/// </summary>
		/// <param name="newContent">The new content.</param>
		/// <author>ducuytran</author>
		/// <datetime>5/23/2013</datetime>
		private void SaveNecessaryFields(content newContent)
		{
			content_type contentType = CspDataContext.content_types.FirstOrDefault(a => a.content_types_Id == _contentTypeId);
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "CSP_3:1_Rectangle_300x100_Reporting_ID", "300x100");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "CSP_Button_120x60_Reporting_ID", "120x60");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "CSP_Full_Banner_468x60_Reporting_ID", "468x60");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "CSP_Full_Banner_600x250_Reporting_ID", "600x250");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "CSP_Half_Banner_234x60_Reporting_ID", "234x60");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "CSP_Half_Button_88x31_Reporting_ID", "88x31");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "CSP_Half_Page_Ad_300x600_Reporting_ID", "300x600");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "CSP_Large_Rectangle_336x280_Reporting_ID", "336x280");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "CSP_Leaderboard_728x90_Reporting_ID", "728x90");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "CSP_Medium_Rectangle_300x250_Reporting_ID", "300x250");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "CSP_Microsite_Reporting_ID", "Microsite");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "CSP_Rectangle_180x150_Reporting_ID", "180x150");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "CSP_Skyscraper_120x600_Reporting_ID", "120x600");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "CSP_Square_Banner_125x125_Reporting_ID", "125x125");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "CSP_Square_Pop-Up_250x250_Reporting_ID", "250x250");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "CSP_Vertical_Rectangle _240x400_Reporting_ID", "240x240");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "CSP_Wide_Skyscraper_160x600_Reporting_ID", "160x600");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "Campaigns_Local_Audience", "Consumer");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "Microsite_Link_Url", ProjectName + "microsite");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "Status_End_Date", "01/01/2099");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "Status_Launch_Date", "01/01/2013");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "CSP_Reporting_Id", ProjectName + "_Showcase");
			Utils.UpdateContentField(CspDataContext, contentType, newContent, "CSP_Button_180x90_Reporting_ID", "180x90");

			//Banner holders
			CopyBannerHolders(contentType, newContent);
		}

		/// <summary>
		/// Copies the banner holders.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>5/30/2013</datetime>
		private void CopyBannerHolders(content_type contentType, content newContent)
		{
			string moduleImgFolder = Server.MapPath(ControlPath) + @"images\";
			string bannerFolderRelativePath = UploadFolder + "/campaign/" + UserFolder + "/" + UserFolder + "microsite" + "/" + langCode + "/" + BannerFolderName + "/";
			string bannerFolderPath = Server.MapPath(bannerFolderRelativePath);

			foreach (KeyValuePair<string, string> keyValuePair in bannerField)
			{
				
				string sourceFile = System.IO.Path.Combine(moduleImgFolder, bannerHolderFiles[keyValuePair.Key]);
				string destFile = System.IO.Path.Combine(bannerFolderPath, bannerHolderFiles[keyValuePair.Key]);

				// To copy a folder's contents to a new location: 
				// Create a new target folder, if necessary. 
				if (!System.IO.Directory.Exists(bannerFolderPath))
				{
					System.IO.Directory.CreateDirectory(bannerFolderPath);
				}

				// To copy a file to another location and  
				// overwrite the destination file if it already exists.
				if (System.IO.File.Exists(sourceFile))
				{
					System.IO.File.Copy(sourceFile, destFile, true);

					string staticUrl = "";
					string stringToReplace = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd(Convert.ToChar("/"));
					string bannerUrl = stringToReplace + "/" + bannerFolderRelativePath + bannerHolderFiles[keyValuePair.Key];
					if (Settings.ContainsKey(Cons.SETTING_IMAGE_URL) && Settings[Cons.SETTING_IMAGE_URL].ToString() != string.Empty)
					{
						staticUrl = Settings[Cons.SETTING_IMAGE_URL].ToString();
						if (staticUrl.EndsWith("/"))
							staticUrl = staticUrl.TrimEnd('/');
					}
					if (!String.IsNullOrEmpty(staticUrl))
						bannerUrl = bannerUrl.Replace(stringToReplace, staticUrl);

					Utils.UpdateContentField(CspDataContext, contentType, newContent, keyValuePair.Value, bannerUrl);
				}
			}
		}

    	/// <summary>
		/// Inits the settings.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>5/23/2013</datetime>
    	private void InitSettings()
		{
			if (Settings.ContainsKey(Cons.SETTING_UPLOAD_FOLDER) && Settings[Cons.SETTING_UPLOAD_FOLDER].ToString() != string.Empty)
				UploadFolder = Settings[Cons.SETTING_UPLOAD_FOLDER].ToString();
			if (UploadFolder.StartsWith("~/"))
				UploadFolder = UploadFolder.Replace("~/", string.Empty);
			if (Settings.ContainsKey(Cons.SETTING_FOLDER_BANNER) && Settings[Cons.SETTING_FOLDER_BANNER].ToString() != string.Empty)
				BannerFolderName = Settings[Cons.SETTING_FOLDER_BANNER].ToString();
			if (BannerFolderName.EndsWith(@"\") || BannerFolderName.EndsWith("/"))
				BannerFolderName = BannerFolderName.Substring(0, BannerFolderName.Length - 1);
			Regex rgx = new Regex("[^a-zA-Z0-9_]");
    		UserFolder = _projectName;// rgx.Replace(_coInfo.companyname.ToLower(), "");
    		ProjectName = UserFolder;
			bannerField = new Dictionary<string, string>()
    		                                         	{
    		                                         		{"143_60", "Banner_Thumbnail_Image"},
															{"160_600", "Wide_Skyscraper_160x600_Image"},
															{"180_90", "Button_180x90_Image"},
															{"234_60", "Half_Banner_234x60_Image"},
															{"300_250", "Medium_Rectangle_300x250_Image"},
															{"468_60", "Full_Banner_468x60_Image"},
															
															
    		                                         	};
			bannerHolderFiles = new Dictionary<string, string>()
    		                                         	{
    		                                         		{"143_60", "banner_holder_143_60.png"},
															{"160_600", "banner_holder_160_600.png"},
															{"180_90", "banner_holder_180_90.png"},
															{"234_60", "banner_holder_234_60.png"},
															{"300_250", "banner_holder_300_250.png"},
															{"468_60", "banner_holder_468_60.png"},
															
															
    		                                         	};
			SetupAsyncUploader();
		}

		/// <summary>
		/// Loads the page resources.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>5/23/2013</datetime>
		private void LoadPageResources()
		{
			Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel='stylesheet' href='{0}css/page-banner.css' type='text/css' />", ControlPath)));
			Page.ClientScript.RegisterClientScriptInclude("mainfunction", ControlPath + "js/function.js");
			Page.ClientScript.RegisterClientScriptInclude("jquery-min", ControlPath + "js/jquery.min.js");
			StepNavigation.GetPageParams(ModuleId, LocalResourceFile, FinishedSteps);
		}

		private void LoadCurrentBanners()
		{
			content_main cmBanner = CspDataContext.content_mains.FirstOrDefault(a => a.supplier_Id == _cspId);
			if (cmBanner == null) return;
			content bannerContent = CspDataContext.contents.FirstOrDefault(a => a.content_main == cmBanner && a.content_types_languages_Id == _langId && a.stage_Id == STAGE_PUBLISHED);
			if (bannerContent == null) return;
			List<Banner> bannerList = new List<Banner>();
			foreach (KeyValuePair<string, string> keyValuePair in bannerField)
			{
				string bannerValue = GetContentFieldValue(bannerContent, keyValuePair.Value);
				if (!String.IsNullOrEmpty(bannerValue) && bannerValue.IndexOf("banner_holder_") == -1)
				{
					bannerList.Add(new Banner
					               	{
					               		BannerType = keyValuePair.Key,
					               		BannerUrl = bannerValue
					               	});
				}
			}
			if (bannerList.Any())
			{
				UploadedBannerCount = bannerList.Count;
				rptCurrentBanner.DataSource = bannerList;
				rptCurrentBanner.DataBind();
			}

			Page.ClientScript.RegisterClientScriptInclude("jquery-min", ControlPath + "js/jquery.min.js");
		}

		/// <summary>
		/// Setups the async uploader.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>5/23/2013</datetime>
		private void SetupAsyncUploader()
		{
            String bannerFolderPath = UploadFolder + "/campaign/" + UserFolder + "/" + UserFolder + "microsite" + "/" + langCode +"/" + BannerFolderName;
			bannerFolderPath = Server.MapPath(bannerFolderPath);
			SetupUploadFolder(bannerFolderPath);
			RadAsyncUpload1.TargetFolder = bannerFolderPath;
		}

		/// <summary>
		/// Gets the upload relative path.
		/// </summary>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>5/23/2013</datetime>
		protected string GetUploadRelativePath()
		{
            String bannerFolderPath = UploadFolder + "/campaign/" + UserFolder + "/" + UserFolder + "microsite" + "/" + langCode + "/" + BannerFolderName + "/";
			//bannerFolderPath = bannerFolderPath + @"\";
			//return bannerFolderPath.Replace(@"\", "/");
			return bannerFolderPath;
		}

    	protected void FileUploaded(object sender, FileUploadedEventArgs e)
    	{
    		String uploadedFileName = "";
			UploadedFile NewFile = RadAsyncUpload1.UploadedFiles[RadAsyncUpload1.UploadedFiles.Count - 1];// e.File;

			string strPath = GetPath(RadAsyncUpload1.TargetFolder);
			Regex rgx = new Regex("[^a-zA-Z0-9_.]");
			string uploadedFilePath = strPath + "\\" + rgx.Replace(NewFile.FileName, string.Empty); //NewFile.FileName;
			/*uploadedFilePath = strPath + "\\" + rgx.Replace(NewFile.GetNameWithoutExtension(), string.Empty) + DateTime.Now.ToString("yyMMddHmmss") +
						   NewFile.GetExtension();*/
			if (File.Exists(uploadedFilePath))
			{
				uploadedFilePath = strPath + "\\" + rgx.Replace(NewFile.GetNameWithoutExtension(), string.Empty) + DateTime.Now.ToString("yyMMddHmmss") +
						   NewFile.GetExtension();
				
			}
			uploadedFileName = uploadedFilePath;
			if (!String.IsNullOrEmpty(uploadedFileName))
			{
				string appUploadPathSetting = Settings[Cons.SETTING_UPLOAD_FOLDER].ToString();
				string appUploadPath = appUploadPathSetting;
				if (!appUploadPath.StartsWith("~/"))
					appUploadPath = "~/" + appUploadPath;
				else
				{
					appUploadPathSetting = appUploadPathSetting.Replace("~/", string.Empty);
				}
				appUploadPathSetting = appUploadPathSetting.Replace('/', '\\');
				appUploadPath = System.Web.Hosting.HostingEnvironment.MapPath(appUploadPath);
				appUploadPath = appUploadPath.Replace(appUploadPathSetting, string.Empty);
				string url = uploadedFileName.Replace(appUploadPath, String.Empty).Replace('\\', '/').Insert(0, "~/" + appUploadPathSetting);
				url = url.Replace("//", "/");
				uploadedFileName = string.Format("http://{0}{1}", Request.Url.Host, Page.ResolveUrl(url));
			}

    		try
    		{
				e.File.SaveAs(uploadedFilePath);
    		}
    		catch (Exception)
    		{
				
    		}
			Response.Write(uploadedFileName);
			Response.Flush();
			Response.End();
    	}

		protected string GetPath(string path)
		{
			if (Path.IsPathRooted(path))
			{
				return path;
			}

			return Server.MapPath(path);
		}
    }

	public class Banner
	{
		public string BannerType { get; set; }
		public string BannerUrl { get; set; }
	}
}