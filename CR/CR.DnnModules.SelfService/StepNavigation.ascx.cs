﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.DnnModules.SelfService.Common;
using DotNetNuke.Common;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Portals;
using CR.DnnModules.Common;
using DotNetNuke.Services.Localization;

namespace CR.DnnModules.SelfService
{
    public partial class StepNavigation : System.Web.UI.UserControl
    {
    	protected int ModuleId = 0;
    	protected string LocalResourceFile = "";
		public List<String> StepList = new List<string>();
		public List<String> FinishedSteps = new List<string>(); 
    	
        protected void Page_Load(object sender, EventArgs e)
        {
        	InitStepList();
        	rptStepList.DataSource = StepList;
			rptStepList.DataBind();
        }

		/// <summary>
		/// Inits the step list.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>5/22/2013</datetime>
		public void InitStepList()
    	{
    		StepList.Add("StepLogo");
			StepList.Add("StepBanner");
			StepList.Add("StepContent");
			StepList.Add("StepSubscribe");
    	}

    	/// <summary>
    	/// Gets the page params.
    	/// </summary>
    	/// <param name="moduleId">The module id.</param>
    	/// <param name="localResourceFile">The local resource file.</param>
    	/// <param name="finishedSteps">List of finished step</param>
    	/// <author>ducuytran</author>
    	/// <datetime>5/22/2013</datetime>
    	public void GetPageParams(int moduleId, string localResourceFile, List<String> finishedSteps = null)
		{
			ModuleId = moduleId;
			LocalResourceFile = Utils.GetCommonResourceFile("CR.DnnModules.SelfService", localResourceFile);
    		FinishedSteps = finishedSteps;
		}

		/// <summary>
		/// Gets the localized text.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>5/22/2013</datetime>
		public string GetLocalizedText(string key)
		{
			return Localization.GetString(key, LocalResourceFile);
		}

		/// <summary>
		/// Gets the step URL.
		/// </summary>
		/// <param name="dataItem">The data item.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>5/22/2013</datetime>
    	protected string GetStepUrl(object dataItem)
    	{
    		string stepName = dataItem as String;
			if (FinishedSteps == null || FinishedSteps.IndexOf(stepName) < 0)
			{
				return "javascript: void(0)";
			}
			return DotNetNuke.Common.Globals.NavigateURL(PortalSettings.Current.ActiveTab.TabID, stepName, "mid", ModuleId.ToString());
    	}

		/// <summary>
		/// Gets the step label.
		/// </summary>
		/// <param name="dataItem">The data item.</param>
		/// <returns></returns>
		/// <author>ducuytran</author>
		/// <datetime>5/22/2013</datetime>
    	protected string GetStepLabel(object dataItem)
    	{
			string stepName = "Label." + dataItem as String;
			return GetLocalizedText(stepName);
    	}
    }
}