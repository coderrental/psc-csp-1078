﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.DnnModules.SelfService.Common;
using DotNetNuke.Entities.Users;
using DotNetNuke.Security.Roles;

namespace CR.DnnModules.SelfService
{
	public partial class MainView : DnnModuleBase
	{
		protected bool HideDescriptionByDefault = false;
		private bool _isUserRoleExpired = false;
		private bool _isFullSetup = true;
		private bool _isRoleExpired = false;

		protected override void Page_Init(object sender, EventArgs e)
		{
			base.Page_Init(sender, e);
			CheckRoleExpiredStatus();
			StepNavigation.InitStepList();
			if (Settings.ContainsKey(Cons.SETTING_HIDE_DESCRIPTION_BY_DEFAULT) && Settings[Cons.SETTING_HIDE_DESCRIPTION_BY_DEFAULT].ToString() == "1")
			{
				HideDescriptionByDefault = true;
			}
		}

		/// <summary>
		/// Checks the role expired status.
		/// </summary>
		/// <author>ducuytran</author>
		/// <datetime>6/10/2013</datetime>
		private void CheckRoleExpiredStatus()
		{
			if (Settings.ContainsKey(Cons.SETTING_USER_ROLE_NAME) && !String.IsNullOrEmpty(Settings[Cons.SETTING_USER_ROLE_NAME].ToString()))
			{
				RoleController roleController = new RoleController();
				ArrayList userRoles = roleController.GetUserRoles(PortalId, UserId);
				foreach (UserRoleInfo userRole in userRoles)
				{
					if (userRole.RoleName != Settings[Cons.SETTING_USER_ROLE_NAME].ToString())
						continue;
					if (userRole.ExpiryDate < DateTime.Now)
					{
						_isRoleExpired = true;
					}
				}
			}
			else
			{
				_isFullSetup = false;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			ServerMessage.Visible = false;
			if (_cspId < 0)
			{
				MainPanel.Visible = false;
				PageMessage.InnerHtml = GetLocalizedText("PageMessage.MissingIntegrationKey");
				PageMessage.Visible = true;
				return;
			}
			if (!_isFullSetup)
			{
				MainPanel.Visible = false;
				PageMessage.InnerHtml = GetLocalizedText("PageMessage.FillModuleSettings");
				PageMessage.Visible = true;
				return;
			}
			if (_isRoleExpired)
			{
				MainPanel.Visible = false;
				PageMessage.InnerHtml = GetLocalizedText("PageMessage.UserRoleExpired");
				PageMessage.Visible = true;
				return;
			}
			if (AllDone)
			{
				int lastStep = -1;
				if (FinishedSteps.Count != StepNavigation.StepList.Count)
				{
					for (int index = 0; index < StepNavigation.StepList.Count; index++)
					{
						string stepItem = StepNavigation.StepList[index];
						if (FinishedSteps.IndexOf(stepItem) == -1)
						{
							lastStep = index;
							UpdateStepValue(StepNavigation.StepList[lastStep], "");
							break;
						}
					}
				}
				if (lastStep == -1)
					lastStep = StepNavigation.StepList.Count - 1;
				Response.Redirect(DotNetNuke.Common.Globals.NavigateURL(PortalSettings.ActiveTab.TabID, StepNavigation.StepList[lastStep], "mid",
																			ModuleId.ToString()));
				
				return;
			}
			else if (FinishedSteps.Any())
			{
				for (int index = 0; index < StepNavigation.StepList.Count; index++)
				{
					string stepItem = StepNavigation.StepList[index];
					if (FinishedSteps.IndexOf(stepItem) == -1)
					{
						int lastStep = index - 1;
						if (index == 0)
							lastStep = index;
						Response.Redirect(DotNetNuke.Common.Globals.NavigateURL(PortalSettings.ActiveTab.TabID, StepNavigation.StepList[lastStep], "mid",
																			ModuleId.ToString()));
						break;
					}
				}
				return;
			}
			Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel='stylesheet' href='{0}css/page-welcome.css' type='text/css' />", ControlPath)));
			Page.ClientScript.RegisterClientScriptInclude("mainfunction", ControlPath + "js/function.js");
            StepNavigation.GetPageParams(ModuleId, LocalResourceFile);
		}

		protected string GetDescriptionDefaultState()
		{
			return HideDescriptionByDefault == false ? String.Empty : "hide";
		}
	}
}