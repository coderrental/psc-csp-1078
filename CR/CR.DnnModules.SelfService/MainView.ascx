﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainView.ascx.cs" Inherits="CR.DnnModules.SelfService.MainView" %>
<%@ Import Namespace="CR.ContentObjectLibrary.Data" %>
<%@ Import Namespace="DotNetNuke.Common" %>
<%@ Import Namespace="DotNetNuke.Services.Localization" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="tagprefix" src="StepNavigation.ascx" tagName="StepNavigation" %>
<span runat="server" class="page-message" ID="PageMessage" Visible="False"></span>
<span runat="server" ID="ServerMessage" CssClass="server-message">
</span>
<asp:Panel runat="server" ID="MainPanel">
<div id="step-navigation">
    <tagprefix:StepNavigation ID="StepNavigation" runat="server" />
</div>
<div id="page-content">
	<div id="description-block">
		<div id="description-text">
			<h1 class="darkblue-text"><%= GetLocalizedText("PageWelcome.Title")%></h1>
			<p class="darkblue-text"><%= GetLocalizedText("PageWelcome.WelcomeText")%></p>
			<a class="btn-darkblue" href="<%= DotNetNuke.Common.Globals.NavigateURL(PortalSettings.ActiveTab.TabID, SelfServiceSteps[0], "mid", ModuleId.ToString()) %>"><%= GetLocalizedText("PageWelcome.GetStarted")%></a>
		</div>
		<div id="description-image">
			<img src="<%= ControlPath %>images/description-image.png" />
		</div>
		<div class="clear"></div>
	</div>
	<div id="detail-toggle">
		<a id="detail-toggle-link" class="darkblue-text" href="javascript: void(0);"></a>
	</div>
	<div runat="server" ID="DescriptionDetailBlock">
	<div id="detail-block">
		<ul id="detail-block-list">
			<li>
				<div class="block-title">
					<img src="<%= ControlPath %>images/guideline-img-cologo.png" align="left" />
					<h2 class="darkblue-text"><%= GetLocalizedText("PageWelcome.BlockLogoTitle")%></h2>
					<p><%= GetLocalizedText("PageWelcome.BlockLogoDescription")%></p>
				</div>
				<div class="block-guideline">
					<h4><%= GetLocalizedText("PageWelcome.BlockLogoGuidelines")%></h4>
					<ul id="guideline-logo-detail">
						<%= GetLocalizedText("PageWelcome.BlockLogoGuidelinesDetail")%>
					</ul>
				</div>
			</li>
			<li>
				<div class="block-title">
					<img src="<%= ControlPath %>images/guideline-img-banners.png" align="left" />
					<h2 class="darkblue-text"><%= GetLocalizedText("PageWelcome.BlockBannersTitle")%></h2>
					<p><%= GetLocalizedText("PageWelcome.BlockBannersDescription")%></p>
				</div>
				<div class="block-guideline">
					<h4><%= GetLocalizedText("PageWelcome.BlockBannersGuidelines")%></h4>
					<ul id="guideline-banner-detail">
						<%= GetLocalizedText("PageWelcome.BlockBannersGuidelinesDetail")%>
					</ul>
				</div>
			</li>
			<li>
				<div class="block-title">
					<img src="<%= ControlPath %>images/guideline-img-content.png" align="left" />
					<h2 class="darkblue-text"><%= GetLocalizedText("PageWelcome.BlockContentTitle") %></h2>
					<p><%= GetLocalizedText("PageWelcome.BlockContentDescription")%></p>
				</div>
				<div class="block-guideline">
					<h4><%= GetLocalizedText("PageWelcome.BlockContentGuidelines")%></h4>
					<ul id="guideline-content-detail">
						<%= GetLocalizedText("PageWelcome.BlockContentGuidelinesDetail")%>
					</ul>
				</div>
			</li>
		</ul>
	</div>
	</div>
</div>
<telerik:RadScriptBlock runat="server">
	<script type="text/javascript">
		var detailToggleLinkText = new Array("Hide the details", "Show the details");
		var hideDescriptionByDefault = '<%= GetDescriptionDefaultState() %>';
		$(function () {
			initApp();
			if (hideDescriptionByDefault == 'hide')
				$('#<%= DescriptionDetailBlock.ClientID %>').hide();
			switchToggleLinkText();
			$('#detail-toggle-link').click(function () {
				$('#<%= DescriptionDetailBlock.ClientID %>').toggle();
				switchToggleLinkText();
			});
		});

		function switchToggleLinkText() {
			if ($('#<%= DescriptionDetailBlock.ClientID %>').is(':hidden')) {
				$('#detail-toggle-link').html(detailToggleLinkText[1]);
				$('#detail-toggle-link').removeClass("icon-collapse");
			} else {
				$('#detail-toggle-link').html(detailToggleLinkText[0]);
				$('#detail-toggle-link').addClass("icon-collapse");
			}
		}
	</script>
</telerik:RadScriptBlock>
</asp:Panel>