﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StepLogo.ascx.cs" Inherits="CR.DnnModules.SelfService.StepLogo" %>
<%@ Import Namespace="CR.ContentObjectLibrary.Data" %>
<%@ Import Namespace="DotNetNuke.Common" %>
<%@ Import Namespace="DotNetNuke.Services.Localization" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="tagprefix" src="StepNavigation.ascx" tagName="StepNavigation" %>
 <telerik:RadToolTipManager ID="radTooltipManager" runat="server" RelativeTo="Element"
          Position="TopRight" AutoTooltipify="true" ContentScrolling="Default" Width="180"
          Height="10">
 </telerik:RadToolTipManager>
<div id="step-navigation">
    <tagprefix:StepNavigation ID="StepNavigation" runat="server" />
</div>
				
<div id="page-content">
	<div id="description-block">
		<div id="description-text">
			<h1 class="darkblue-text"><%= GetLocalizedText("PageLogo.UploadLogoTitle")%></h1>
			<p class="darkblue-text"><%= GetLocalizedText("PageLogo.UploadLogoTitleDescription")%></p>
		</div>
		<div id="description-image">
			<a id="btn-goto-uploadbanner" class="btn-darkblue btn-darkblue-inactive" href="javascript: void(0);"><%= GetLocalizedText("PageLogo.BtnUploadBanners")%></a>
		</div>
		<div class="clear"></div>
	</div>
	<div id="detail-block">
		<ul id="detail-block-list">
			<li>
				<div class="company-logo-img-inactive">
				</div>
			</li>
			<li>
				<div class="block-guideline">
					<h2 class ="darkblue-text"><%= GetLocalizedText("PageLogo.GuidelinesTitle")%></h2>
					<ul id="guideline-logo-detail">
						<%= GetLocalizedText("PageLogo.GuidelinesDetail")%>
					</ul>
				</div>						
			</li>
			<li>
				<div class="block-guideline">
					<h2 class ="darkblue-text"><%= GetLocalizedText("PageLogo.LabelCompanyLogo")%></h2>

					<div id="upload-company-logo" class="progressbar-wrapper" title="<%=GetLocalizedText("Label.ClickToUpload")%>">
					    <div class="banner-label"><%= GetLocalizedText("Dimension.200.90")%></div>
						<div class="progressbar-text"></div>
						<div class="progressbar-percent">
							<div class="progressbar-percent-text">
								<div class="progressbar-percent-text-inner"></div>
							</div>
						</div>
					</div>
				</div>	
			</li>
		</ul>
	</div>
</div>
<telerik:RadWindow runat="server" ID="UploadWindow" Modal="True" Width="500" Height="200" VisibleStatusbar="False" Behaviors="Close,Move">
    <ContentTemplate>
    <div id="UploadPanel">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server">Choose file to upload</asp:Label>
                </td>
                 <td>
                     
                </td>
            </tr>
        </table>
    </div>
    </ContentTemplate>
</telerik:RadWindow>
<telerik:RadWindow runat="server" ID="IEUploadWindow" Behaviors="Close, Move" VisibleStatusbar="False" Width="500" Height="140">
	<ContentTemplate>
		<div id="ie-radupload-content">				 
			
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<asp:Label ID="Label2" runat="server"><%= GetLocalizedText("PageContent.UploadInstruction")%></asp:Label>
					</td>
					 <td>
						 <telerik:RadAsyncUpload runat="server" ID="RadAsyncUpload1" OnClientProgressUpdating="onClientFileUploading" 
				OnClientFileUploaded="clientFileUploaded" OnClientFileSelected="OnClientFileSelected" MultipleFileSelection="Disabled" 
				AllowedFileExtensions=".jpg,.gif,.png,.bmp" OnClientValidationFailed="CheckValidate" MaxFileSize="10000000">
				</telerik:RadAsyncUpload>
					</td>
				</tr>
			</table>
		</div>
	</ContentTemplate>
</telerik:RadWindow>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
	<script type="text/javascript">
		var baseUrl = '<%= Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd(Convert.ToChar("/")) + "/" %>';
		var logoFolderUrl = '<%= GetUploadRelativePath() %>';
		var logoLink = '<%= GetLogoUrl() %>';

		$(function () {
			initApp();
			initLogoUploadTrigger();
			$('#StepLogoLink').find('.step-block').addClass('active');
			if (logoLink != '') {
				ActivatePage(logoLink);
			}
		});
		
		function initLogoUploadTrigger() {
			$('#upload-company-logo').unbind().click(function () {
				$('.progressbar-percent').css('width', '0');
				$('.progressbar-text').html('');
				$('.progressbar-percent-text-inner').html('');
				$('#content-filename').html('');
				$('#content-filename').hide();
				//$find("<%=UploadWindow.ClientID %>").show();
				$('#content-upload-progress').hide();
				if ($.browser.msie && parseInt($.browser.version, 10) <= 9) {
					$find('<%= IEUploadWindow.ClientID %>').show();
					return;
				}
				$('.RadAsyncUpload').find('input.ruFileInput').click();
				/*if ($.browser.msie && parseInt($.browser.version, 10) <= 8) {
					$('.RadAsyncUpload').find('.ruBrowse').click();
					//console.log('ie8 clicked');
				}*/
			});
		}

		function CheckValidate() {
			alert("Invalid file specified. Please try again.");
			
			var bannerClicked = $('#upload-company-logo');
			$('.progressbar-percent-text').hide();
			$('.progressbar-text').hide();
			if (bannerClicked.find('img').length == 1) {
				bannerClicked.find('img').show();
				bannerClicked.css('border', 'none');
				bannerClicked.find('.banner-label').hide();
			} else {
				bannerClicked.css('border', '2px dashed #449bc5');
				bannerClicked.find('.banner-label').show();
			}
			initLogoUploadTrigger();
		   /* $('#detail-block-list .company-logo-img-active').addClass('company-logo-img-inactive').removeClass('company-logo-img-active');
		    $('#btn-goto-uploadbanner').removeClass('btn-darkblue-active').addClass('btn-darkblue-inactive').removeAttr('href');
		    initLogoUploadTrigger();
		    $('.progressbar-percent-text').hide();
		    $('.progressbar-text').hide();*/
		}

		function OnClientFileSelected(sender, args) {
			if ($.browser.msie && parseInt($.browser.version, 10) <= 9) {
				//$find('<%= IEUploadWindow.ClientID %>').hide();
			}
			$('#content-upload-progress').show();
			var currentFileName = args.get_fileName();
			$('#content-filename').html(currentFileName).show();
			$telerik.$(args.get_row()).addClass("ruUploading");
			
			$('#upload-company-logo').unbind();
			if ($('#upload-company-logo').find('img').length > 0) {
				$('#upload-company-logo').css('border', '2px dashed #449bc5');
				$('#upload-company-logo').find('img').hide();
			}
            $('.banner-label').show();
			$('.progressbar-percent-text').show();
			$('.progressbar-percent-text-inner').html('0%').show();
			$('.progressbar-text').show();
			if ($('.progressbar-text').html() == '')
				$('.progressbar-text').html('Uploading...');
		}

		function onClientFileUploading(sender, args) {
			var data = args.get_data();
			var percents = data.percent;
			var fileSize = data.fileSize;
			var fileName = data.fileName;
			var uploadedBytes = data.uploadedBytes;
			if (percents >= 100) return;
			$('.progressbar-percent').animate(
				{ width: percents + '%' },
				{
					duration: 500,
					step: function (now, fx) {
						var data = Math.round(now) + '%';
						$('.progressbar-percent-text-inner').html(data);
					}
				}
			);
		}

		function clientFileUploaded(sender, args) {
			if ($.browser.msie && parseInt($.browser.version, 10) <= 9) {
				$find('<%= IEUploadWindow.ClientID %>').close();
			}
			$('.progressbar-percent').animate(
				{ width: 100 + '%' },
				{
					duration: 500,
					step: function (now, fx) {
						var data = Math.round(now) + '%';
						$('.progressbar-percent-text-inner').html(data);
					},
					complete: function () {
						$('.progressbar-percent').css('width', '0');
						$('.progressbar-percent-text').hide();
						$('.progressbar-text').hide();
						initLogoUploadTrigger();
						//$('form:first').submit();
						console.log(args.get_fileInfo());
						$.ajax({
							type: "POST",
							url: $('form:first').attr('action'),
							data: $('form:first').serialize(),
							success: function () {
							    var logoLink = baseUrl + logoFolderUrl + args.get_fileInfo().FileName;
							    $('.banner-label').hide();
								ActivatePage(logoLink);
								$.ajax({
									type: 'POST',
									url: document.URL,
									dataType: 'text',
									data: { logolink: logoLink },
									beforeSend: function (xhr) {
										xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
									},
									success: function (responseMsg) {
									}
								});
							}
						});
					}
				}
			);
			
		}
		
		function ActivatePage(imgUrl) {
			$('#detail-block-list .company-logo-img-inactive').addClass('company-logo-img-active').removeClass('company-logo-img-inactive');
			$('#btn-goto-uploadbanner').removeClass('btn-darkblue-inactive').attr('href', '<%= DotNetNuke.Common.Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "StepBanner", "mid", ModuleId.ToString()) %>');
			var newImg = $('<img height="90px" width="200px">').attr('src', imgUrl);
			$('#upload-company-logo').css('border', 'none');
			$('#upload-company-logo').find('img').remove();
			$('#upload-company-logo').append(newImg);
			$('#upload-company-logo').find('.banner-label').hide();
		}
	</script>
</telerik:RadScriptBlock>