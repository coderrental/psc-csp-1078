﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.DnnModules.Common;
using CR.DnnModules.SelfService.Common;
using DotNetNuke.Entities.Modules;
using CR.ContentObjectLibrary.Data;
using Telerik.Web.UI;

namespace CR.DnnModules.SelfService
{
	public partial class ModuleSettings : ModuleSettingsBase
	{
		public override void LoadSettings()
		{
			if (TabModuleSettings[Cons.SETTING_UPLOAD_FOLDER] != null)
			{
				tbxUploadFolder.Text = TabModuleSettings[Cons.SETTING_UPLOAD_FOLDER].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_FOLDER_LOGO] != null)
			{
				tbxLogoFolderName.Text = TabModuleSettings[Cons.SETTING_FOLDER_LOGO].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_FOLDER_BANNER] != null)
			{
				tbxBannerFolderName.Text = TabModuleSettings[Cons.SETTING_FOLDER_BANNER].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_IMAGE_URL] != null)
			{
				tbxImageUrl.Text = TabModuleSettings[Cons.SETTING_IMAGE_URL].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_HIDE_DESCRIPTION_BY_DEFAULT] != null)
			{
				chkHideDescription.Checked = int.Parse(TabModuleSettings[Cons.SETTING_HIDE_DESCRIPTION_BY_DEFAULT].ToString()) > 0;
			}
			if (TabModuleSettings[Cons.SETTING_USER_ROLE_NAME] != null)
			{
				tbxUserRoleName.Text = TabModuleSettings[Cons.SETTING_USER_ROLE_NAME].ToString();
			}
			if (TabModuleSettings[Cons.SETTING_CSPDNN_CONNECTIONSTRING] != null)
			{
				tbxCspDnnConnectionString.Text = TabModuleSettings[Cons.SETTING_CSPDNN_CONNECTIONSTRING].ToString();
			}
		}

		public override void UpdateSettings()
		{
			var moduleController = new ModuleController();

			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_UPLOAD_FOLDER, tbxUploadFolder.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_FOLDER_LOGO, tbxLogoFolderName.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_FOLDER_BANNER, tbxBannerFolderName.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_IMAGE_URL, tbxImageUrl.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_HIDE_DESCRIPTION_BY_DEFAULT, chkHideDescription.Checked ? "1" : "0");
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_USER_ROLE_NAME, tbxUserRoleName.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_CSPDNN_CONNECTIONSTRING, tbxCspDnnConnectionString.Text);
			ModuleController.SynchronizeModule(ModuleId);
		}
	}
}