﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StepSubscribe.ascx.cs" Inherits="CR.DnnModules.SelfService.StepSubscribe" %>
<%@ Import Namespace="CR.ContentObjectLibrary.Data" %>
<%@ Import Namespace="DotNetNuke.Common" %>
<%@ Import Namespace="DotNetNuke.Services.Localization" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="tagprefix" src="StepNavigation.ascx" tagName="StepNavigation" %>
 <telerik:RadToolTipManager ID="radTooltipManager" runat="server" RelativeTo="Element"
          Position="BottomCenter" AutoTooltipify="true" ContentScrolling="Default" Width="180"
          Height="10">
 </telerik:RadToolTipManager>
<div id="step-navigation">
    <tagprefix:StepNavigation ID="StepNavigation" runat="server" />
</div>
<div id="page-content">
	<div id="description-block">
		<div id="description-text">
			<h1 class="darkblue-text"><%= GetLocalizedText("PageSubscribe.Title")%></h1>
		</div>
		<div id="description-image">
			<a class="btn-darkblue" href="<%=GetBaseDomain() %>" target="_blank"><%= GetLocalizedText("PageSubscribe.BtnDescription")%></a>
		</div>
		<div class="clear"></div>
		<p class="darkblue-text"><%= GetLocalizedText("PageSubscribe.Description")%></p>
	</div>
    <div title="<%=GetLocalizedText("Label.ClickToCopyClipboard") %>">
        <div id="company-link">
		    <a><%=GetBaseDomain()%></a>
	    </div>
    </div>

	<div id="detail-block">
		<div id="detail-block-left">
			<h2 class="darkblue-text"><%= GetLocalizedText("PageSubscribe.NextStep")%></h2>
			<p class="darkblue-text"><%= GetLocalizedText("PageSubscribe.NextStepDescription")%></p>
			<ul id="detail-block-list">
				<li id="detail-campaign">
					<a class="darkblue-text" href="<%= GetLocalizedText("Link.CampaignInBox") %>" target="_blank"><%= GetLocalizedText("PageSubscribe.Campaign")%></a>
				</li>
				<li id="detail-email">
					<a class="darkblue-text" href="<%= GetLocalizedText("Link.EmailTemplates") %>" target="_blank"><%= GetLocalizedText("PageSubscribe.Email")%></a>
				</li>
				<li id="detail-guide">
					<a class="darkblue-text" href="<%= GetLocalizedText("Link.BestPraticesGuide") %>" target="_blank"><%= GetLocalizedText("PageSubscribe.Guide")%></a>
				</li>
			</ul>
		</div>
		<div id="detail-block-right">
			<h2 class="darkblue-text"><%= GetLocalizedText("PageSubscribe.ReportTitle")%></h2>
			<p class="darkblue-text"><%= GetLocalizedText("PageSubscribe.ReportDescription")%></p>
		</div>
	</div>
</div>
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
	<script type="text/javascript">
	    $(function () {
	        initApp();
	        initCopyLink();
	        $('#StepSubscribeLink').find('.step-block').addClass('active');
	        $('#StepSubscribeLink').attr('href', '<%= DotNetNuke.Common.Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "StepSubscribe", "mid", ModuleId.ToString()) %>');
	    });

	    function initCopyLink() {
	    	$("#company-link").zclip({
	    		path: "<%=ControlPath %>" + "js/ZeroClipboard.swf",
	    		copy: function () {
	    			return $.trim($(this).find('a').html());
	    		},
	    		afterCopy: function () {
	    			var newSpan = $('<span style="color: #FFF; display: block; font-size: 24px; margin: 0 auto;">').html('<%= GetLocalizedText("PageSubscribe.CopyToClipboard")%>');
	    			$("#company-link").find('span').remove();
	    			$("#company-link").find('a').hide();
	    			$("#company-link").html($("#company-link").html() + '<span style="color: #FFF; display: block; font-size: 24px; margin: 0 auto;"><%= GetLocalizedText("PageSubscribe.CopyToClipboard")%></span>');
	    			setTimeout("resetCompanyLink()", 2000);
	    		}
	    	});
	    }
	    
		function resetCompanyLink() {
			$("#company-link").find('span').remove();
			$("#company-link").find('a').show();
		}
	</script>
</telerik:RadScriptBlock>