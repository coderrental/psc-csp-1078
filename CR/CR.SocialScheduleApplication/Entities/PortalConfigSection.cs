﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CR.SocialScheduleApplication.Entities
{
    #region[PortalConfigSection]
    public class PortalConfigSection : ConfigurationSection
    {
        /// <summary>
        /// Gets the portal item.
        /// </summary>
        /// <value>The portal item.</value>
        [ConfigurationProperty("Portals")]
        public PortalsCollection PortalItem
        {
            get { return ((PortalsCollection)(base["Portals"])); }

        }
    }
    #endregion

    #region [PortalsCollection]
    public class PortalsCollection : ConfigurationElementCollection
    {

        /// <summary>
        /// When overridden in a derived class, creates a new <see cref="T:System.Configuration.ConfigurationElement" />.
        /// </summary>
        /// <returns>A new <see cref="T:System.Configuration.ConfigurationElement" />.</returns>
        /// <author>Vu Dinh</author>
        /// <modified>07/02/2014 11:19:08</modified>
        protected override ConfigurationElement CreateNewElement()
        {
            return new PortalElement();
        }

        /// <summary>
        /// Gets the element key for a specified configuration element when overridden in a derived class.
        /// </summary>
        /// <param name="element">The <see cref="T:System.Configuration.ConfigurationElement" /> to return the key for.</param>
        /// <returns>An <see cref="T:System.Object" /> that acts as the key for the specified <see cref="T:System.Configuration.ConfigurationElement" />.</returns>
        /// <author>Vu Dinh</author>
        /// <modified>07/02/2014 11:19:10</modified>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((PortalElement)(element)).PortalId;
        }

        /// <summary>
        /// Gets or sets a property, attribute, or child element of this configuration element.
        /// </summary>
        /// <param name="idx">The idx.</param>
        /// <returns>PortalElement.</returns>
        public PortalElement this[int idx]
        {

            get
            {
                return (PortalElement)BaseGet(idx);
            }
        }
    }
    #endregion


    #region[PortalElement]
    public class PortalElement : ConfigurationElement
    {
        /// <summary>
        /// Gets or sets the portal id.
        /// </summary>
        /// <value>The portal id.</value>
        [ConfigurationProperty("portalid", DefaultValue = 0, IsKey = true, IsRequired = true)]

        public int PortalId
        {
            get
            {
                return ((int)(base["portalid"]));
            }
            set
            {
                base["portalid"] = value;
            }

        }


        /// <summary>
        /// Gets or sets the name of the portal.
        /// </summary>
        /// <value>The name of the portal.</value>
        [ConfigurationProperty("portalname", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string PortalName
        {
            get
            {
                return ((string)(base["portalname"]));
            }
            set
            {
                base["portalname"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>The connection string.</value>
        [ConfigurationProperty("connectionstring", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string ConnectionString
        {
            get
            {
                return ((string)(base["connectionstring"]));
            }
            set
            {
                base["connectionstring"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the approve tweet parameter.
        /// </summary>
        /// <value>The name of the approve tweet parameter.</value>
        [ConfigurationProperty("approvetweetparametername", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string ApproveTweetParameterName
        {
            get
            {
                return ((string)(base["approvetweetparametername"]));
            }
            set
            {
                base["approvetweetparametername"] = value;
            }
        }

        [ConfigurationProperty("twitterconsumerkey", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string TwitterConsumerKey
        {
            get
            {
                return ((string)(base["twitterconsumerkey"]));
            }
            set
            {
                base["twitterconsumerkey"] = value;
            }
        }

        [ConfigurationProperty("twitterconsumersecret", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string TwitterConsumerSecret
        {
            get
            {
                return ((string)(base["twitterconsumersecret"]));
            }
            set
            {
                base["twitterconsumersecret"] = value;
            }
        }

        [ConfigurationProperty("linkedinconsumerkey", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string LinkedinConsumerKey
        {
            get
            {
                return ((string)(base["linkedinconsumerkey"]));
            }
            set
            {
                base["linkedinconsumerkey"] = value;
            }
        }

        [ConfigurationProperty("linkedinconsumersecret", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string LinkedinConsumerSecret
        {
            get
            {
                return ((string)(base["linkedinconsumersecret"]));
            }
            set
            {
                base["linkedinconsumersecret"] = value;
            }
        }

        [ConfigurationProperty("facebookconsumerkey", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string FacebookConsumerKey
        {
            get
            {
                return ((string)(base["facebookconsumerkey"]));
            }
            set
            {
                base["facebookconsumerkey"] = value;
            }
        }

        [ConfigurationProperty("facebookconsumersecret", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string FacebookConsumerSecret
        {
            get
            {
                return ((string)(base["facebookconsumersecret"]));
            }
            set
            {
                base["facebookconsumersecret"] = value;
            }
        }

        [ConfigurationProperty("bitlyaccesstoken", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string BitLyAccessToken
        {
            get
            {
                return ((string)(base["bitlyaccesstoken"]));
            }
            set
            {
                base["bitlyaccesstoken"] = value;
            }
        }

        [ConfigurationProperty("instancename", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string InstanceName
        {
            get
            {
                return ((string)(base["instancename"]));
            }
            set
            {
                base["instancename"] = value;
            }
        }

        [ConfigurationProperty("publicationpageid", DefaultValue = 1, IsKey = false, IsRequired = false)]
        public int PublicationPageId
        {
            get
            {
                return ((int)(base["publicationpageid"]));
            }
            set
            {
                base["publicationpageid"] = value;
            }
        }
        [ConfigurationProperty("usebitlyasdefaultshortenurl", DefaultValue = "false", IsKey = false, IsRequired = false)]
        public string UseBitlyAsDefaultShortenUrl
        {
            get
            {
                return ((string)(base["usebitlyasdefaultshortenurl"]));
            }
            set
            {
                base["usebitlyasdefaultshortenurl"] = value;
            }
        }
        [ConfigurationProperty("shortenurlapikey", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string ShortenUrlApiKey
        {
            get
            {
                return ((string)(base["shortenurlapikey"]));
            }
            set
            {
                base["shortenurlapikey"] = value;
            }
        }

    }
    #endregion
}
