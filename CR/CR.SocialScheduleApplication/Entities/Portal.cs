﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CR.SocialScheduleApplication.Entities
{
    public class Portal
    {
        /// <summary>
        /// Gets or sets the name of the portal.
        /// </summary>
        /// <value>The name of the portal.</value>
        public int PortalId { get; set; }

        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>The connection string.</value>

        public string ConnectionString { get; set; }

        /// <summary>
        /// Gets or sets the name of the portal.
        /// </summary>
        /// <value>The name of the portal.</value>
        public string PortalName { get; set; }

        /// <summary>
        /// Gets or sets the name of the approve tweet parameter.
        /// </summary>
        /// <value>The name of the approve tweet parameter.</value>
        
        public string ApproveTweetParameterName { get; set; }
        /// <summary>
        /// Gets or sets the twitter consumer key.
        /// </summary>
        /// <value>The twitter consumer key.</value>

        public string TwitterConsumerKey { get; set; }
        /// <summary>
        /// Gets or sets the twitter consumer secret.
        /// </summary>
        /// <value>The twitter consumer secret.</value>

        public string TwitterConsumerSecret { get; set; }
        /// <summary>
        /// Gets or sets the linkedin consumer key.
        /// </summary>
        /// <value>The linkedin consumer key.</value>

        public string LinkedinConsumerKey { get; set; }
        /// <summary>
        /// Gets or sets the linkedin consumer secret.
        /// </summary>
        /// <value>The linkedin consumer secret.</value>

        public string LinkedinConsumerSecret { get; set; }
        /// <summary>
        /// Gets or sets the facebook consumer key.
        /// </summary>
        /// <value>The facebook consumer key.</value>

        public string FacebookConsumerKey { get; set; }
        /// <summary>
        /// Gets or sets the facebook consumer secret.
        /// </summary>
        /// <value>The facebook consumer secret.</value>
        
        public string FacebookConsumerSecret { get; set; }

        /// <summary>
        /// Gets or sets the bit ly access token.
        /// </summary>
        /// <value>The bit ly access token.</value>
        public string BitLyAccessToken { get; set; }

        /// <summary>
        /// Gets or sets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        public string InstanceName { get; set; }

        /// <summary>
        /// Gets or sets the publication page id.
        /// </summary>
        /// <value>The publication page id.</value>
        public int PublicationPageId { get; set; }

        public string ShortenUrlApiKey { get; set; }
        public string UseBitlyAsDefaultShortenUrl { get; set; }
    }
}
