﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using CR.SocialScheduleApplication.Common;
using CR.SocialScheduleApplication.Entities;
using log4net;

namespace CR.SocialScheduleApplication
{
    public class Process
    {
        private System.Timers.Timer _timer;
        private List<Portal> _listPortals;
        private static readonly ILog log = LogManager.GetLogger("SocialSchedule");


        /// <summary>
        /// When implemented in a derived class, executes when a Start command is sent to the service by the Service Control Manager (SCM) or when the operating system starts (for a service that starts automatically). Specifies actions to take when the service starts.
        /// </summary>
        /// <param name="args">Data passed by the start command.</param>
        /// <author>Vu Dinh</author>
        /// <modified>08/13/2014 16:05:08</modified>
        //public void Do()
        //{


        //    //System.Diagnostics.Debugger.Launch();
        //    var portalssection = (PortalConfigSection)ConfigurationManager.GetSection("PortalConfigSection");
        //    _listPortals = (from PortalElement portalconfig in portalssection.PortalItem
        //                       select new Portal
        //                       {
        //                           PortalId = portalconfig.PortalId,
        //                           ConnectionString = portalconfig.ConnectionString,
        //                           PortalName = portalconfig.PortalName,
        //                           ApproveTweetParameterName = portalconfig.ApproveTweetParameterName,
        //                           TwitterConsumerKey = portalconfig.TwitterConsumerKey,
        //                           TwitterConsumerSecret = portalconfig.TwitterConsumerSecret,
        //                           LinkedinConsumerKey = portalconfig.LinkedinConsumerKey,
        //                           LinkedinConsumerSecret = portalconfig.LinkedinConsumerSecret,
        //                           FacebookConsumerKey = portalconfig.FacebookConsumerKey,
        //                           FacebookConsumerSecret = portalconfig.FacebookConsumerSecret,
        //                           BitLyAccessToken = portalconfig.BitLyAccessToken,
        //                           InstanceName = portalconfig.InstanceName,
        //                           PublicationPageId = portalconfig.PublicationPageId
        //                       }).ToList();


        //    foreach (var portal in _listPortals)
        //    {
        //        var schedule = new Schedule();
        //        var portalTmp = portal;
        //        var thread = new Thread(() => schedule.PostMessage(portalTmp));
        //        thread.Start();
        //    }

        //    double interval = 300000;
        //    try
        //    {
        //        interval = int.Parse(ConfigurationManager.AppSettings["interval"])*1000;
        //    }
        //    catch
        //    {
        //        interval = 300000;
        //    }

        //    _timer = new System.Timers.Timer { Interval = Convert.ToDouble(interval) };

        //    _timer.Elapsed += TimerElapsed;
        //    _timer.Enabled = true;

        //}
        
        /// <summary>
        /// Timers the elapsed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="elapsedEventArgs">The <see cref="ElapsedEventArgs"/> instance containing the event data.</param>
        /// <author>Vu Dinh</author>
        /// <modified>08/13/2014 16:05:05</modified>
        private void TimerElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            foreach (var portal in _listPortals)
            {
                var schedule = new Schedule();
                var portalTmp = portal;
                var thread = new Thread(() => schedule.PostMessage(portalTmp));
                thread.Start();
            }
        }
    }
}
