﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CR.DnnModules.Common;
using CR.SocialScheduleApplication.Common;
using CR.SocialScheduleApplication.Entities;
using Microsoft.Win32;

namespace CR.SocialScheduleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
           log4net.Config.XmlConfigurator.Configure();
           var servicesToRun = new ServiceBase[] 
                 { 
                     new CrSocialScheduler() 
                 };
           ServiceBase.Run(servicesToRun);        
           //new Process().Do();
        }
    }
}
