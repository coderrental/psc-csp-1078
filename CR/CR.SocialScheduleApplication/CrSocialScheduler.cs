﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using CR.DnnModules.Common;
using CR.SocialScheduleApplication.Common;
using CR.SocialScheduleApplication.Entities;
using log4net;
using log4net.Appender;
using log4net.Repository.Hierarchy;

namespace CR.SocialScheduleApplication
{
    partial class CrSocialScheduler : ServiceBase
    {

        private System.Timers.Timer _timer;
        private List<Portal> _listPortals;
        private static readonly ILog log = LogManager.GetLogger("SocialSchedule");
        private Thread thread;

        public CrSocialScheduler()
        {
            InitializeComponent();
        }

        /// <summary>
        /// When implemented in a derived class, executes when a Start command is sent to the service by the Service Control Manager (SCM) or when the operating system starts (for a service that starts automatically). Specifies actions to take when the service starts.
        /// </summary>
        /// <param name="args">Data passed by the start command.</param>
        /// <author>Vu Dinh</author>
        /// <modified>08/13/2014 16:05:08</modified>
        protected override void OnStart(string[] args)
        {

            //System.Diagnostics.Debugger.Launch();
            var portalssection = (PortalConfigSection)ConfigurationManager.GetSection("PortalConfigSection");
            _listPortals = (from PortalElement portalconfig in portalssection.PortalItem
                               select new Portal
                               {
                                   PortalId = portalconfig.PortalId,
                                   ConnectionString = portalconfig.ConnectionString,
                                   PortalName = portalconfig.PortalName,
                                   ApproveTweetParameterName = portalconfig.ApproveTweetParameterName,
                                   TwitterConsumerKey = portalconfig.TwitterConsumerKey,
                                   TwitterConsumerSecret = portalconfig.TwitterConsumerSecret,
                                   LinkedinConsumerKey = portalconfig.LinkedinConsumerKey,
                                   LinkedinConsumerSecret = portalconfig.LinkedinConsumerSecret,
                                   FacebookConsumerKey = portalconfig.FacebookConsumerKey,
                                   FacebookConsumerSecret = portalconfig.FacebookConsumerSecret,
                                   BitLyAccessToken = portalconfig.BitLyAccessToken,
                                   InstanceName = portalconfig.InstanceName,
                                   PublicationPageId = portalconfig.PublicationPageId,
                                   ShortenUrlApiKey = portalconfig.ShortenUrlApiKey,
                                   UseBitlyAsDefaultShortenUrl = portalconfig.UseBitlyAsDefaultShortenUrl
                               }).ToList();

            double interval = 300000;
            try
            {
                interval = int.Parse(ConfigurationManager.AppSettings["interval"]) * 1000;
            }
            catch
            {
                interval = 300000;
            }

            thread = new Thread(() => Do(interval));
            thread.Start();
        }


        private void Do(double interval)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            while (true)
            {
                sw.Restart();

                log.Debug("Begin executing the process. Interval: " + interval);
                var errors = _listPortals.AsParallel().Select(portal =>
                {
                    log.Debug("*** executing: " + portal.InstanceName);
                    try
                    {
                        (new Schedule()).PostMessage(portal);
                        return null;
                    }
                    catch (AggregateException ae)
                    {
                        return Tuple.Create(portal, ae);
                    }
                    finally
                    {
                        log.Debug("*** Finished executing: " + portal.InstanceName);
                    }
                }).Where(a => a != null).ToList();

                log.DebugFormat("Finished executing the process with {0} error(s).", errors.Count);

                if (errors.Count > 0)
                {
                    foreach (var error in errors)
                    {
                        try
                        {
                            LogError(error);
                        }
                        catch (Exception ex)
                        {
                            log.Error(ex.Message, ex);
                        }
                    }
                }

                sw.Stop();
                log.DebugFormat("**** Process takes {0} milliseconds.", sw.ElapsedMilliseconds);
                var n = (int) (interval - sw.ElapsedMilliseconds);
                if (n > 0)
                {
                    log.DebugFormat("Sleep for {0} milliseconds", n);
                    Thread.Sleep(n);
                }
                Thread.Sleep(1000);
            }
        }

        private void LogError(Tuple<Portal, AggregateException> error)
        {            
            log.Error(error.Item2.Message);
            //Email the exeption to someone to track the error
            var host = ConfigurationManager.AppSettings["SMTPHost"];
            var port = int.Parse(ConfigurationManager.AppSettings["SMTPPort"]);
            var username = ConfigurationManager.AppSettings["SMTPUsername"];
            var password = ConfigurationManager.AppSettings["SMTPPassword"];
            var enableSsl = bool.Parse(ConfigurationManager.AppSettings["SMTPEnableSSL"]);
            var recipient = ConfigurationManager.AppSettings["RecipientEmail"];

            var emailSender = new EmailSmtpSender(host, port, enableSsl, username, password);
            emailSender.SendEmail(username, recipient, "SocialScheduler in portal "+ error.Item1.InstanceName +" - Error: " + DateTime.Now.ToString("MM/dd/yy H:mm"), error.Item2.Message + "<br/>" + error.Item2.ToString());
        }


        /// <summary>
        /// Timers the elapsed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="elapsedEventArgs">The <see cref="ElapsedEventArgs"/> instance containing the event data.</param>
        /// <author>Vu Dinh</author>
        /// <modified>08/13/2014 16:05:05</modified>
        private void TimerElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            foreach (var portal in _listPortals)
            {
                var schedule = new Schedule();
                var portalTmp = portal;
                var thread = new Thread(() => schedule.PostMessage(portalTmp));
                thread.Start();
            }
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
            thread.Abort();
        }
    }
}
