﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CR.DnnModules.Common;
using CR.ContentObjectLibrary.Data;
using CR.SocialLib;
using CR.SocialScheduleApplication.Data;
using CR.SocialScheduleApplication.Entities;
using log4net;
using DnnDataContext = CR.SocialScheduleApplication.Data.DnnDataContext;

namespace CR.SocialScheduleApplication.Common
{
    public class Schedule
    {

        private Guid _contentId;
        private content _content;
        private CspDataContext _cspDataContext;
        private CspUtils _cspUtils;
        private string _dnnConnectionString,
                       _cspConnectionString,
                       _cspDbConnectionString,
                       _approveTweetParameterName,
                       _twitterConsumerKey,
                       _twitterConsumerSecret,
                       _linkedInConsumerKey,
                       _linkedInConsumerSecret,
                       _facebookConsumerKey,
                       _facebookConsumerSecret,
                       _instanceName,
                       _bitlyAccessToken,
                       _portalName,
                       _useBitlyAsDefaultShortenUrl,
                       _shortenUrlApikey;
                        
        private int _portalId;
        private int _publicationPageId;
        private static readonly ILog log = LogManager.GetLogger("SocialSchedule");
        private DnnDataContext _dnnDataContext;
        private CspDbDataContext _cspDbDataContext;
        private List<PartnerPostMessageCompleted> _listPartnerPostMessageCompleted;
        private void InitResource(Portal portal)
        {
            try
            {
                _cspDbConnectionString = portal.ConnectionString;
                _dnnConnectionString = ConfigurationManager.ConnectionStrings["DnnConnectionString"].ConnectionString;
                _cspConnectionString = portal.ConnectionString;
                _portalId = portal.PortalId;
                _publicationPageId = portal.PublicationPageId;
                _twitterConsumerKey = portal.TwitterConsumerKey;
                _twitterConsumerSecret = portal.TwitterConsumerSecret;
                _linkedInConsumerKey = portal.LinkedinConsumerKey;
                _linkedInConsumerSecret = portal.LinkedinConsumerSecret;
                _facebookConsumerKey = portal.FacebookConsumerKey;
                _facebookConsumerSecret = portal.FacebookConsumerSecret;
                _bitlyAccessToken = portal.BitLyAccessToken;
                _instanceName = portal.InstanceName;
                _approveTweetParameterName = portal.ApproveTweetParameterName;
                _cspDbDataContext = new CspDbDataContext(_cspDbConnectionString);
                _cspDataContext = new CspDataContext(_cspConnectionString);
                _dnnDataContext = new DnnDataContext(_dnnConnectionString);
                _cspUtils = new CspUtils(_cspDataContext);
                _portalName = portal.PortalName;
                _useBitlyAsDefaultShortenUrl = portal.UseBitlyAsDefaultShortenUrl;
                _shortenUrlApikey = portal.ShortenUrlApiKey;
                Log(string.Format("Init resources portal {0} completed", _portalName));
                /*_cspDbConnectionString = ConfigurationManager.ConnectionStrings["CspConnectionString"].ConnectionString;
                _dnnConnectionString = ConfigurationManager.ConnectionStrings["DnnConnectionString"].ConnectionString;
                _cspConnectionString = ConfigurationManager.ConnectionStrings["CspConnectionString"].ConnectionString;
                _portalId = Int32.Parse(ConfigurationManager.AppSettings["PortalId"]);
                _publicationPageId = Int32.Parse(ConfigurationManager.AppSettings["PublicationPageId"]);
                _twitterConsumerKey = ConfigurationManager.AppSettings["TwitterConsumerKey"];
                _twitterConsumerSecret = ConfigurationManager.AppSettings["TwitterConsumerSecret"];
                _facebookConsumerKey = ConfigurationManager.AppSettings["FacebookConsumerKey"];
                _facebookConsumerSecret = ConfigurationManager.AppSettings["FacebookConsumerSecret"];
                _linkedInConsumerKey = ConfigurationManager.AppSettings["LinkedinConsumerKey"];
                _linkedInConsumerSecret = ConfigurationManager.AppSettings["LinkedinConsumerSecret"];
                _bitlyAccessToken = ConfigurationManager.AppSettings["BitLyAccessToken"];
                _instanceName = ConfigurationManager.AppSettings["InstanceName"];
                _approveTweetParameterName = ConfigurationManager.AppSettings["ApproveTweetParameterName"];
                _cspDbDataContext = new CspDbDataContext(_cspDbConnectionString);
                _cspDataContext = new CspDataContext(_cspConnectionString);
                _dnnDataContext = new DnnDataContext(_dnnConnectionString);
                _cspUtils = new CspUtils(_cspDataContext);*/
                
            }
            catch (Exception e)
            {
                LogError(e);
            }


        }
        private string GetPublicationPage(int cspId, Guid contentId)
        {
            var companyConsumer = _cspDataContext.companies_consumers.FirstOrDefault(a => a.companies_Id == cspId && a.active == true);
            return companyConsumer != null ? String.Format("http://p{0}-{1}/d1.aspx?p{2}(cn{3})[]&i={4}", _instanceName, companyConsumer.base_domain, _publicationPageId, contentId, DateTime.Now.Ticks) : String.Empty;
        }
        private bool PostTweet(content content, List<SocialLib.Common.Socialtype> socialTypes)
        {
            var listSentStatistics = new List<CSPTSMM_MessageSentStatistic>();
            
            foreach (var socialtype in socialTypes)
            {
                try
                {
                    var socialId = (int)socialtype;
                    var socialTypeTmp = socialtype;
                    var portalIdTmp = _portalId;
                    var listPartners = _dnnDataContext.CSPTSMM_Partners.Where(a => a.IsSubscribed && a.PortalId == _portalId && a.SocialId == socialId);
                    var listPartnerInfo = new List<PartnerInfo>();

                    foreach (var partner in listPartners)
                    {
                        try
                        {
                            var link = GetPublicationPage(partner.CspId, content.content_Id);
                            log.DebugFormat("\t** Get social message: {0} for partner: {1}", socialtype.ToString(), partner.CspId);
                            
                            var tweet = new Commons().GetTweetMessage(_dnnDataContext, _cspDbDataContext, partner.CspId, content.content_Id, link, _bitlyAccessToken, socialId, _portalId,_useBitlyAsDefaultShortenUrl,_shortenUrlApikey);
                            listPartnerInfo.Add(new PartnerInfo
                            {
                                ApprovePost = _cspUtils.GetCompanyParameter(partner.CspId, _approveTweetParameterName),
                                MessageSentCount = _dnnDataContext.CSPTSMM_MessageSentStatistics.Count(a => a.MessageId == content.content_Id && a.CspId == partner.CspId && a.SocialId == socialId && a.PortalId == portalIdTmp),
                                Partner = partner,
                                Tweet = tweet //CspUtils.GetContentFieldValue(content, Cons.TWITTER_MESSAGE_MESSAGE)
                            });
                            log.DebugFormat("\t** Get social message: {0} for partner: {1}", socialtype.ToString(), partner.CspId);
                        }
                        catch (Exception ex)
                        {
                            LogError(ex);
                            log.Error(ex.Message, ex);
                            log.ErrorFormat("Additional information for debugging");
                            try
                            {
                                log.Error("\tPartner Id" + partner.CspId);
                                log.Error("\tContent Id" + content.content_Id);
                            }
                            catch { }
                            log.ErrorFormat("++++++++++++++++++++++++++++++++++++++++");
                        }

                    }

                    foreach (var partnerinfo in listPartnerInfo)
                    {
                        if (partnerinfo.MessageSentCount > 1)
                            continue;
                        try
                        {

                            if (partnerinfo.Partner.IsSubscribed)
                            {
                                if (partnerinfo.ApprovePost == "False" || string.IsNullOrEmpty(partnerinfo.ApprovePost))
                                {
                                    Log("PartnerId: " + partnerinfo.Partner.CspId.ToString(CultureInfo.InvariantCulture));
                                    var tweet = partnerinfo.Tweet;
                                    if (!string.IsNullOrEmpty(tweet) && tweet != " ")
                                    {

                                        string consumerKey = null, consumerSecret = null;
                                        switch (socialTypeTmp)
                                        {
                                            case CR.SocialLib.Common.Socialtype.LinkedIn:
                                                {
                                                    consumerKey = _linkedInConsumerKey;
                                                    consumerSecret = _linkedInConsumerSecret;
                                                    break;
                                                }
                                            case CR.SocialLib.Common.Socialtype.Twitter:
                                                {
                                                    consumerKey = _twitterConsumerKey;
                                                    consumerSecret = _twitterConsumerSecret;
                                                    break;
                                                }
                                            case CR.SocialLib.Common.Socialtype.Facebook:
                                                {
                                                    consumerKey = _facebookConsumerKey;
                                                    consumerSecret = _facebookConsumerSecret;
                                                    break;
                                                }
                                        }

                                        var socialHelper = new SocialHelper(socialTypeTmp, consumerKey, consumerSecret, string.Empty);

                                        //check status for this partner
                                        var profileResponse = socialHelper.GetUserProfile(partnerinfo.Partner.Token, partnerinfo.Partner.TokenSecret);
                                        if (profileResponse.StatusCode == HttpStatusCode.OK) // this partner currently subscribe social application
                                        {
                                            var postStatusResult = socialHelper.PostMessage(partnerinfo.Partner.Token, partnerinfo.Partner.TokenSecret, tweet);

                                            switch (postStatusResult.StatusCode)
                                            {
                                                case HttpStatusCode.OK:
                                                    Log("Post message to social " + socialTypeTmp.ToString() + " completed!");
                                                    break;
                                                default:
                                                    Log("Response when post message to social " + socialTypeTmp.ToString() + " " +
                                                        postStatusResult.ResponseText);
                                                    break;
                                            }

                                            string responseText = "";
                                            if (!string.IsNullOrEmpty(postStatusResult.ResponseText))
                                            {
                                                responseText = postStatusResult.ResponseText;
                                            }

                                            listSentStatistics.Add(new CSPTSMM_MessageSentStatistic
                                            {
                                                CspId = partnerinfo.Partner.CspId,
                                                MessageId = content.content_Id,
                                                SentAt = DateTime.Now,
                                                SentSuccess = (postStatusResult.StatusCode == HttpStatusCode.OK),
                                                ResponseMessage = (postStatusResult.StatusCode == HttpStatusCode.OK) ? "Sent Successfully" : responseText,
                                                NumOfReceiver = CR.SocialLib.Common.NumOfFollowers(socialTypeTmp, profileResponse.ResponseText),
                                                Pending = false,
                                                SocialId = socialId,
                                                PortalId = portalIdTmp
                                            });



                                            //Update number of follower
                                            partnerinfo.Follower = CR.SocialLib.Common.NumOfFollowers(socialTypeTmp, profileResponse.ResponseText);
                                            partnerinfo.PostResponse = postStatusResult.StatusCode;
                                            partnerinfo.ProfileResponse = profileResponse.StatusCode;

                                        }
                                        else
                                        {
                                            partnerinfo.ProfileResponse = profileResponse.StatusCode;
                                        }

                                    }
                                }
                                else
                                {
                                    Log("Partner select approve post tweet, Add data to MessageSentStatistic completed!");
                                    listSentStatistics.Add(new CSPTSMM_MessageSentStatistic
                                    {
                                        CspId = partnerinfo.Partner.CspId,
                                        MessageId = content.content_Id,
                                        SentAt = DateTime.Now,
                                        SentSuccess = false,
                                        ResponseMessage = "",
                                        NumOfReceiver = 0,
                                        Pending = true,
                                        SocialId = socialId,
                                        PortalId = portalIdTmp
                                    });
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            LogError(e);
                            string errorMessage = "";
                            if (string.IsNullOrEmpty(e.Message))
                            {
                                errorMessage = e.Message;
                            }

                            listSentStatistics.Add(new CSPTSMM_MessageSentStatistic
                            {
                                CspId = partnerinfo.Partner.CspId,
                                MessageId = content.content_Id,
                                SentAt = DateTime.Now,
                                SentSuccess = false,
                                ResponseMessage = errorMessage,
                                NumOfReceiver = 0,
                                Pending = false,
                                SocialId = socialId,
                                PortalId = portalIdTmp
                            });
                        }
                    }

                    foreach (var partnerInfo in listPartnerInfo)
                    {
                        try
                        {
                            if (partnerInfo.PostResponse == HttpStatusCode.OK)
                            {
                                partnerInfo.Partner.Followers = partnerInfo.Follower;
                                _dnnDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                                Log("Add data to MessageSentStatistic completed!");
                                Log("Update follower completed!");

                                //Longvk 12/09/2014 fix set custom content field, wait to post message all social done!
                                if (!_listPartnerPostMessageCompleted.Any(a => a.CspId == partnerInfo.Partner.CspId && a.Content ==content))
                                {
                                    _listPartnerPostMessageCompleted.Add(new PartnerPostMessageCompleted { Content = content, CspId = partnerInfo.Partner.CspId, Tweet = partnerInfo.Tweet });
                                } 
                               
                                /*//save message content into custom_content_fields table before post to social network
                                _cspUtils.SetCustomContentFieldValue(content, partnerInfo.Partner.CspId, Cons.TWITTER_MESSAGE_MESSAGE, partnerInfo.Tweet);

                                //Add custom content field (tooltip) to this partner
                                _cspUtils.SetCustomContentFieldValue(content, partnerInfo.Partner.CspId, Cons.TWITTER_MESSAGE_TOOLTIP, partnerInfo.Tweet);*/
                            }
                            else if (partnerInfo.ProfileResponse != HttpStatusCode.OK && partnerInfo.ProfileResponse != 0)
                            {
                                log.Error(partnerInfo.PostResponse);
                                log.Error(partnerInfo.ProfileResponse);
                                log.Error("Response Code: " + partnerInfo.ProfileResponse + ".");
                                if (partnerInfo.ProfileResponse == HttpStatusCode.Unauthorized)
                                {
                                    log.Error("We should set this account ["+partnerInfo.Partner.CspId+"] to un-subscribed");
                                    //partnerInfo.Partner.IsSubscribed = false;
                                    //_dnnDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            LogError(e);
                        }

                    }
                }
                catch (Exception e)
                {
                    LogError(e);
                }
            }
            if (listSentStatistics.Any())
            {
                _dnnDataContext.CSPTSMM_MessageSentStatistics.InsertAllOnSubmit(listSentStatistics);
                _dnnDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                return true;
            }
            return false;
        }

        private void Log(string text)
        {
            log.Info(text);
        }

        private void LogError(Exception ex)
        {
            log.Error(ex);
            //Email the exeption to someone to track the error
            var host = ConfigurationManager.AppSettings["SMTPHost"];
            var port = int.Parse(ConfigurationManager.AppSettings["SMTPPort"]);
            var username = ConfigurationManager.AppSettings["SMTPUsername"];
            var password = ConfigurationManager.AppSettings["SMTPPassword"];
            var enableSsl = bool.Parse(ConfigurationManager.AppSettings["SMTPEnableSSL"]);
            var recipient = ConfigurationManager.AppSettings["RecipientEmail"];

            var emailSender = new EmailSmtpSender(host, port, enableSsl, username, password);
            emailSender.SendEmail(username, recipient, "SocialScheduler in portal "+ _portalName +" - Error: " + DateTime.Now.ToString("MM/dd/yy H:mm"), ex.Message + "<br/>" + ex.ToString());
        }
        
        public void PostMessage(Portal portal)
        {
            InitResource(portal);
            CheckMessage();
            //CheckMessageError();
        }
        /*
        private void CheckMessageError()
        {
            try
            {
                var error = "{'errors':[{'message':'Could not authenticate you','code':32}]}";
                error = error.Replace("'", "\"");

                var listMessagesError = _dnnDataContext.CSPTSMM_MessageSentStatistics.Where(
                        a => a.ResponseMessage == error && a.SentAt >= DateTime.Now.AddHours(-2) &&
                        a.SentAt <= DateTime.Now.AddMinutes(-30) && a.PortalId == _portalId && a.SocialId == 1 && !a.SentSuccess).ToList();

                if (listMessagesError.Any())
                {
                    log.DebugFormat("Found {0} messages has Error 32 in portal {1}", listMessagesError.Count, _portalName);
                    foreach (var messagesError in listMessagesError)
                    {
                        try
                        {
                            var content = _cspDataContext.contents.SingleOrDefault(a => a.content_Id == messagesError.MessageId);
                            if (content == null) continue;
                            var partner = _dnnDataContext.CSPTSMM_Partners.FirstOrDefault(a => a.CspId == messagesError.CspId && a.SocialId == messagesError.SocialId && a.PortalId == messagesError.PortalId && a.IsSubscribed);
                            if (partner == null) continue;
                            var link = GetPublicationPage(messagesError.CspId, messagesError.MessageId);
                            var tweet = Commons.GetTweetMessage(_dnnDataContext, _cspDbDataContext, messagesError.CspId, messagesError.MessageId, link, _bitlyAccessToken, messagesError.SocialId, _portalId);
                            if (string.IsNullOrEmpty(tweet)) continue;
                            var socialHelper = new SocialHelper(CR.SocialLib.Common.Socialtype.Twitter, _twitterConsumerKey, _twitterConsumerSecret, string.Empty);

                            //check status for this partner
                            var profileResponse = socialHelper.GetUserProfile(partner.Token, partner.TokenSecret);
                            if (profileResponse.StatusCode == HttpStatusCode.OK)
                            // this partner currently subscribe social application
                            {
                                var postStatusResult = socialHelper.PostMessage(partner.Token, partner.TokenSecret, tweet);

                                switch (postStatusResult.StatusCode)
                                {
                                    case HttpStatusCode.OK:
                                        Log("Post message to social Twitter completed!");
                                        break;
                                    default:
                                        Log("Response when post message to social Twitter: " +
                                            postStatusResult.ResponseText);
                                        break;
                                }
                                string responseText = "";
                                if (!string.IsNullOrEmpty(postStatusResult.ResponseText))
                                {
                                    responseText = postStatusResult.ResponseText;
                                }
                                messagesError.SentAt = DateTime.Now;
                                messagesError.SentSuccess = (postStatusResult.StatusCode == HttpStatusCode.OK);
                                messagesError.ResponseMessage = (postStatusResult.StatusCode == HttpStatusCode.OK) ? "Sent Successfully" : responseText;
                                messagesError.NumOfReceiver = CR.SocialLib.Common.NumOfFollowers(CR.SocialLib.Common.Socialtype.Twitter, profileResponse.ResponseText);

                                _dnnDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                                Log("Update data to MessageSentStatistic completed!");
                                if (postStatusResult.StatusCode == HttpStatusCode.OK)
                                {
                                    //save message content into custom_content_fields table before post to social network
                                    _cspUtils.SetCustomContentFieldValue(content, partner.CspId, Cons.TWITTER_MESSAGE_MESSAGE, tweet);

                                    //Add custom content field (tooltip) to this partner
                                    _cspUtils.SetCustomContentFieldValue(content, partner.CspId, Cons.TWITTER_MESSAGE_TOOLTIP, tweet);
                                }

                            }
                            else
                            {
                                log.Error("Response Code: " + profileResponse.StatusCode + ".");
                                // ltu 9/2/14 comment this out
                                if (profileResponse.StatusCode == HttpStatusCode.Unauthorized)
                                {
                                    log.Error("We should set this account ["+ partner.CspId+"] to un-subscribed");
                                    //partner.IsSubscribed = false;
                                    //_dnnDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                                }
                            }
                        }
                        catch (Exception e)
                        {

                            LogError(e);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                
                LogError(e);
            }
            
        }
         * */

        private void RemoveScheduleMessage(int socialId)
        {
            var messageSchedule = _dnnDataContext.CSPTSMM_MessageScheduleStatistics.Where(a => a.MessageId == _contentId && a.Pending && a.SocialId==socialId && a.PortalId == _portalId).ToList();
            foreach (var message in messageSchedule)
            {
                try
                {
                    message.Pending = false;
                }
                catch (Exception e) 
                {
                    
                    LogError(e);
                }
                
            }
            _dnnDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
        }

        private void CheckMessage()
        {
            try
            {
                var listMessage = _dnnDataContext.CSPTSMM_MessageScheduleStatistics.Where(a => a.Pending && a.PortalId == _portalId).ToList();
                log.DebugFormat("Found {0} messages pending in portal {1}", listMessage.Count,_portalName);
                _listPartnerPostMessageCompleted = new List<PartnerPostMessageCompleted>();
                foreach (var message in listMessage)
                {
                    try
                    {
                        var currentTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, 0);
                        DateTime timeconverted = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(message.SentAt, message.TimeZone, TimeZoneInfo.Local.Id);
                        log.DebugFormat("Current Time: {0}, Message Time: {1}", currentTime.ToString("yyyy-MM-dd HH:mm:ss"), timeconverted.ToString("yyyy-MM-dd HH:mm:ss"));
                        if (timeconverted <= currentTime)
                        {
                            Log("Find message has schedule: " + currentTime.ToString(CultureInfo.InvariantCulture));
                            _contentId = message.MessageId;
                            Log("Message Id: " + _contentId);
                            Log("Social Id: " + message.SocialId);
                            _content = _cspDataContext.contents.SingleOrDefault(a => a.content_Id == _contentId);
                            var messageToPost = _dnnDataContext.CSPTSMM_MessageScheduleStatistics.FirstOrDefault(a => a.MessageId == message.MessageId && a.Pending && a.PortalId == _portalId);
                            var listSocialTypeToPost = new List<CR.SocialLib.Common.Socialtype>();
                            if (messageToPost != null)
                            {
                                switch (messageToPost.SocialId)
                                {
                                    case 1:
                                        listSocialTypeToPost.Add(CR.SocialLib.Common.Socialtype.Twitter);
                                        break;
                                    case 2:
                                        listSocialTypeToPost.Add(CR.SocialLib.Common.Socialtype.LinkedIn);
                                        break;
                                    default:
                                        listSocialTypeToPost.Add(CR.SocialLib.Common.Socialtype.Facebook);
                                        break;
                                }
                                Log(string.Format("Begin Post Message id {0} in portal {1}",_contentId,_portalName));
                                PostTweet(_content, listSocialTypeToPost);
                                Log(string.Format("End Post Message id {0} in portal {1} ",_contentId,_portalName));
                                RemoveScheduleMessage(messageToPost.SocialId);
                            }
                        }
                    }
                    catch (Exception e)
                    {

                        LogError(e); 
                    }                    
                }
                foreach (var partnerPostMessageCompleted in _listPartnerPostMessageCompleted)
                {
                    try
                    {
                        //save message content into custom_content_fields table before post to social network
                        _cspUtils.SetCustomContentFieldValue(partnerPostMessageCompleted.Content, partnerPostMessageCompleted.CspId, Cons.TWITTER_MESSAGE_MESSAGE, partnerPostMessageCompleted.Tweet);

                        //Add custom content field (tooltip) to this partner
                        _cspUtils.SetCustomContentFieldValue(partnerPostMessageCompleted.Content, partnerPostMessageCompleted.CspId, Cons.TWITTER_MESSAGE_TOOLTIP, partnerPostMessageCompleted.Tweet);
                    }
                    catch (Exception e)
                    {
                        
                        LogError(e);
                    }
                    
                }
            }
            catch (Exception e)
            {
                LogError(e);
            }
            
        }
    }
}
