﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CR.SocialScheduleApplication.Common
{
    class Cons
    {
        public static int MAX_CHARACTER_LENGTH = 138;

        public const string TWITTER_MESSAGE_CONTENT_TYPE_NAME = "CspTweetMessage";
        public const string SESSIONSTOREPREVIEWMESSAGES = "PreviewMessageSession";


        public const string ADMIN_ROLE_NAME = "Csp_Tweet_Admin_Role";

        public const string CATEGORY_CONTENT_TYPE_NAME = "CategoryContent";
        public const string CATEGORY_CONTENT_TITLE_FIELD_NAME = "Content_Title";

        public const string TWITTER_MESSAGE_CONTENTID = "ContentId";
        public const string TWITTER_MESSAGE_MESSAGE = "Message";
        public const string TWITTER_MESSAGE_CREATED = "CreatedAt";
        public const string TWITTER_MESSAGE_TOOLTIP = "Tooltip";
        public const string TWITTER_AUTO_POST_PARAMETER = "Twitter_Auto_Post";

        public const int DEFAULT_CONTENT_TYPE_GROUP = 1;
        public const int DEFAULT_SUPPLIER_ID = 12;
        public const int STAGE_PUBLIC = 50;
        public const string DEFAULT_PUBLISH_CB_ID = "1";
        public const string DEFAULT_UNPUBLISH_CB_ID = "2";
        //Long add code 27/03
        public const string DEFAULT_SENT = "1";
        public const string DEFAULT_NOTSENT = "2";

        public const int SUBSTRING_LENGTH = 60;

        public const string SETTING_TWITTER_CONSUMER_KEY = "TwitterConsumerKey";
        public const string SETTING_TWITTER_CONSUMER_SECRET = "TwitterConsumerSecret";
        public const string SETTING_LINKEDIN_CONSUMER_KEY = "LinkedInConsumerKey";
        public const string SETTING_LINKEDIN_CONSUMER_SECRET = "LinkedInConsumerSecret";
        public const string SETTING_FACEBOOK_CONSUMER_KEY = "FacebookConsumerKey";
        public const string SETTING_FACEBOOK_CONSUMER_SECRET = "FacebookConsumerSecret";
        public const string SETTING_PUBLICATION_INSTANCE_NAME = "InstanceName";
        public const string SETTING_PUBLICATION_PAGE_ID = "PublicationPageId";
        public const string SETTING_CUSTOMCONTENTFIELD_PUBLICATION_PAGE_ID = "CustomcontentfieldPublicationPageId";
        public const string SETTING_SUPPLIER_ID = "SupplierId";
        public const string SETTING_BITLY_ACCESSTOKEN = "BitlyAccessToken";
        public const string SETTING_LIST_COMPANY_PARAMETER = "ListCompanyParameters";
        public const string SETTING_BASEDOMAIN_TEST = "BaseDomainForTest";
        public const string SETTING_REFRESH_PARTNER_FOLLOWERS = "RefreshPartnerFollowers";

        public const string SETTING_APPROVE_TWEET_PARAMETER_NAME = "TwitterApprovePost";
        public const string SETTING_CATEGORYCONTENTTYPEID = "CategoryContentTypeId";

        public const string SETTING_RESEND_FAILED_MESSAGE = "ResendFailedMessage";
        public const string SETTING_LIST_RESEND_MESSAGE_IDS = "ListResendMessageIds";
    }
}
