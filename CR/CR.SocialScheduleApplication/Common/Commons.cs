﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CR.DnnModules.Common;
using CR.SocialLib.Bitly;
using CR.SocialScheduleApplication.Data;
using DotNetNuke.Common.Utilities;
using log4net;

namespace CR.SocialScheduleApplication.Common
{
    class Commons
    {
        public string GetTweetMessage(DnnDataContext dataContext, CspDbDataContext cspDbDataContext, int cspId, Guid messageId, string publicationLink, string bitlyAccessToken, int socialId, int PortalId, string useBitlyAsDefaultShortenUrl, string shortenUrlApiKey)
        {
            if (string.IsNullOrEmpty(publicationLink))
                return string.Empty;

            var tweet = string.Empty;
            Log("Get Message For ["+cspId+"]. From: " + publicationLink);
            WebRequest request = WebRequest.Create(publicationLink);
            string localProxyIp = ConfigurationManager.AppSettings["local_proxy_ip"];
            if (!string.IsNullOrEmpty(localProxyIp))
            {
                try
                {
                    request.Proxy = new WebProxy(new Uri(localProxyIp), true);
                }
                catch
                {
                }
            }
            WebResponse response = request.GetResponse();
            var stream = response.GetResponseStream();
            if (stream != null)
            {
                var sr = new StreamReader(stream);
                tweet = sr.ReadToEnd();
                if (!string.IsNullOrEmpty(tweet)) // content found
                {
                    //parse tweet message, shorten url via Bit.ly service
                    log.Debug(string.Format("\tGet message with shorten url: [{0}] for cspId: {1}", tweet, cspId));
                    tweet = GetMessageWithShortUrl(dataContext, cspDbDataContext, cspId, messageId, tweet, bitlyAccessToken, socialId, PortalId,useBitlyAsDefaultShortenUrl,shortenUrlApiKey);
                    log.Debug("\tFinished getting messages");
                }
            }
            Log(string.Format("Message Content: [{0}]", tweet));
            return tweet;
        }

        public List<string> ExtractUrl(string message)
        {
            var urlRx = new Regex(@"(http|ftp|https)://([\w+?\.\w+])+([a-zA-Z0-9\~\|\!\@\#\[\]{\}\$\%\^\&\*\(\)_\-\=\+\\\/\?\.\:\;\'\,]*)?", RegexOptions.IgnoreCase);
            MatchCollection matches = urlRx.Matches(message);
            return (from Match match in matches select match.Value).ToList();
        }

        public string GetMessageWithShortUrl(DnnDataContext dataContext, CspDbDataContext cspDbDataContext, int cspId, Guid messageId, string message, string bitlyAccessToken, int socialId, int PortalId,string useBitlyAsDefaultShortenUrl,string shortenUrlApiKey)
        {
            // fix #csp_page issue - sometime the {consumer:FDeeplink_Landingpage_Url} does not contain / at the end, so the link is broken, eg: www.domain#csp_page
            // Need to check and insert / before #csp_page
            message = FixCspPageInDeepLink(message);
            var listUrl = ExtractUrl(message);
            if (listUrl.Any())
            {
                var bitlyHelper = new BitlyHelper(bitlyAccessToken);
                foreach (var url in listUrl)
                {
                    try
                    {
                        /*if (url.Contains(".ly"))
                            continue;*/

                        string parameterUrl = url.TrimEnd('/') +
                                              string.Format("?messageid={0}&cspid={1}&socialid={2}", messageId, cspId,
                                                  socialId);
                        string shorturl;
                        if (useBitlyAsDefaultShortenUrl == "false" && !string.IsNullOrEmpty(shortenUrlApiKey))
                        {
                            var shortenUrlDefault = bitlyHelper.GetShortenUrlDefault(parameterUrl,shortenUrlApiKey);
                            shorturl = shortenUrlDefault.ShortenUrl;
                        }
                        else
                        {
                            var shortenUrl = bitlyHelper.GetShortenUrl(parameterUrl);
                            shorturl = shortenUrl.ShortenUrl;
                        }

                        message = message.Replace(url, shorturl);
                        if (cspId != 0)
                        {
                            //store bitly link into csp db
                            var linkPerMessage = cspDbDataContext.SocialModule_BitlyLinksPerMessages.SingleOrDefault(a => a.BitlyLink == shorturl && a.PortalId == PortalId && a.MessageId == messageId && a.CspId == cspId && a.SocialId == socialId);
                            if (linkPerMessage == null)
                            {
                                linkPerMessage = new SocialModule_BitlyLinksPerMessage
                                {
                                    CspId = cspId,
                                    MessageId = messageId,
                                    OriginalUrl = parameterUrl,
                                    BitlyLink = shorturl,
                                    SocialId = socialId,
                                    PortalId = PortalId
                                };
                                cspDbDataContext.SocialModule_BitlyLinksPerMessages.InsertOnSubmit(linkPerMessage);
                                cspDbDataContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Log(e.Message);
                    }
                   
                }

                //var bitlyHelper = new BitlyHelper(bitlyAccessToken);
                //return listUrl.Where(url => !url.Contains(".ly")).Aggregate(message, (current, url) => current.Replace(url, bitlyHelper.GetShortenUrl(url).ShortenUrl));
            }

            return message;
        }

        public string FixCspPageInDeepLink(string tweet)
        {
            var listGroupString = tweet.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries).ToList();
            var listNewGroup = new List<string>();
            foreach (var group in listGroupString)
            {
                try
                {
                    var temp = group;
                    if (temp.StartsWith("www.", StringComparison.CurrentCultureIgnoreCase))
                    {
                        temp = "http://" + temp;
                    }
                    if (temp.StartsWith("http://", StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (temp.Contains("#csp_page"))
                        {
                            if (temp.IndexOf("?", StringComparison.Ordinal) < temp.IndexOf("#csp_page", StringComparison.Ordinal))   // something like http://domain/?abc#csp_page... 
                            {
                                // no need to fix
                                listNewGroup.Add(temp);
                            }
                            else
                            {
                                // something like http://domain#csp_page ..., need to insert / before #csp_page
                                temp = temp.Replace("#csp_page", "/#csp_page");
                                temp = temp.Replace("//#csp_page", "/#csp_page");
                                listNewGroup.Add(temp);
                            }

                        }
                        else
                        {
                            listNewGroup.Add(temp);
                        }
                    }
                    else
                    {
                        if (temp.StartsWith("#csp_page", StringComparison.CurrentCultureIgnoreCase)) // something like #csp_page... missing deeplink url, so ignore it
                        {
                            // do nothing
                        }
                        else
                        {
                            listNewGroup.Add(temp);
                        }
                    }
                }
                catch (Exception e)
                {
                    Log(e.Message);
                }
               
            }
            if (listNewGroup.Any())
            {
                return String.Join(" ", listNewGroup);
            }
            return tweet;
        }

        public static readonly ILog log = LogManager.GetLogger("SocialSchedule");
        public static void Log(string text)
        {
            log.Info(text);
        }

        public static void LogError(Exception ex)
        {
            log.Error(ex);
            //Email the exeption to someone to track the error
            var host = ConfigurationManager.AppSettings["SMTPHost"];
            var port = int.Parse(ConfigurationManager.AppSettings["SMTPPort"]);
            var username = ConfigurationManager.AppSettings["SMTPUsername"];
            var password = ConfigurationManager.AppSettings["SMTPPassword"];
            var enableSsl = bool.Parse(ConfigurationManager.AppSettings["SMTPEnableSSL"]);
            var recipient = ConfigurationManager.AppSettings["RecipientEmail"];

            var emailSender = new EmailSmtpSender(host, port, enableSsl, username, password);
            emailSender.SendEmail(username, recipient, "SocialScheduler - Error: " + DateTime.Now.ToString("MM/dd/yy H:mm"), ex.Message + "<br/>" + ex.ToString());
        }
    }

  
}
