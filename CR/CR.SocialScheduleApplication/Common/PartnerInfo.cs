﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CR.SocialScheduleApplication.Data;

namespace CR.SocialScheduleApplication.Common
{
    public class PartnerInfo
    {
        /// <summary>
        /// Gets or sets the partner.
        /// </summary>
        /// <value>The partner.</value>
        public CSPTSMM_Partner Partner { get; set; }

        /// <summary>
        /// Gets or sets the tweet.
        /// </summary>
        /// <value>The tweet.</value>
        public string Tweet { get; set; }

        /// <summary>
        /// Gets or sets the approve post.
        /// </summary>
        /// <value>The approve post.</value>
        public string ApprovePost { get; set; }

        /// <summary>
        /// Gets or sets the follower.
        /// </summary>
        /// <value>The follower.</value>
        public int Follower { get; set; }

        /// <summary>
        /// Gets or sets the message sent count.
        /// </summary>
        /// <value>The message sent count.</value>
        public int MessageSentCount { get; set; }

        /// <summary>
        /// Gets or sets the post response.
        /// </summary>
        /// <value>The post response.</value>
        public HttpStatusCode PostResponse { get; set; }
        public HttpStatusCode ProfileResponse { get; set; }
    }
}
