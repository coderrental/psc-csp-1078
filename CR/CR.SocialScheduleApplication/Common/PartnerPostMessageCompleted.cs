﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CR.ContentObjectLibrary.Data;

namespace CR.SocialScheduleApplication.Common
{
   public class PartnerPostMessageCompleted
   {
        public content Content { get; set; }
        public int CspId { get; set; }
        public string Tweet { get; set; }
   }
}
