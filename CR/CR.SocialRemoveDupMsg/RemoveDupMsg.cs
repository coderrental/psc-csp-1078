﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CR.SocialLib;
using CR.SocialRemoveDupMsg.Data;
using Newtonsoft.Json.Linq;
using log4net;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;

namespace CR.SocialRemoveDupMsg
{
    public class RemoveDupMsg
    {
        private static readonly ILog log = LogManager.GetLogger("SocialRemoveDupMsg");
        private Data.DnnDataContext _dnnDataContext;
        private CspDataContext _cspDataContext;
        private CspUtils _cspUtils;
        private string _consumerKey;
        private string _consumerSecret;
        private int _portalId;

        public void DoTask()
        {
            //Test Post msg

            //const string PartnerToken = "2588725434-1bxGDHF6t9XGwA3lE8Hf2sb3AZQPpCjAwm4vHGX";
            //const string PartnerTokenSecret = "YqWLd1lhSnZt1TtpKFUFx5s0JVozKhuj7Pj0J8XjLt26T";
            //string message = "Le vacanze sono finite! Di nuovo al lavoro e traffico in aumento. ~_= * ()Ma cè una soluzione NW2W \"  http://bit.ly/1ufqtYU";

            //var socialHelper = new SocialLib.SocialHelper(SocialLib.Common.Socialtype.Twitter, "xD5hedQc1EvwqoKIayBoSw","316skWTkNS4CJov2WmXBJOAlttQzVGepbKERiMo", string.Empty);
            //var profileResponse = socialHelper.GetUserProfile(PartnerToken, PartnerTokenSecret);
            //Debug.WriteLine(message);
            //var result = socialHelper.PostMessage(PartnerToken
            //    , PartnerTokenSecret
            //    , message);
            ////, "Le vacanze sono finite! Di nuovo al lavoro e traffico in aumento. ~_= * ()Ma cè una soluzione NW2W \"  http://bit.ly/1ufqtYU");

            //Debug.WriteLine(result.ResponseText);

            Console.WriteLine("Begin Init Resource");
            try
            {
                InitResource();
            }
            catch (Exception exception)
            {

                LogError(exception);
                return;
            }
            //get list content id from ListMsgId.txt file
            //var listContentIds = System.IO.File.ReadAllLines("ListMsgId.txt").ToList();
            var listContentIds = new List<string>()
            {
                "bca0cacc-449c-466f-9711-c83c22b15d27"
            };

            foreach (var contentId in listContentIds)
            {
                RemoveTweet(new Guid(contentId));
            }
            LogInfo("Done!");
        }

        private void RemoveTweet(Guid contentId)
        {
            LogInfo(string.Empty);
            var socialHelper = new SocialLib.SocialHelper(SocialLib.Common.Socialtype.Twitter, _consumerKey, _consumerSecret, string.Empty);
            LogInfo("-Content ID : " + contentId);
            var content = _cspDataContext.contents.SingleOrDefault(a => a.content_Id == contentId);
            if (content != null)
            {
                var partners = new List<int>()
                {
2036
,594
,1662
,1710
,1521
,1943
,1845
,1599
//,2136
//,2137
,1429
,1048
,1717
,808
,1381
//,1706
,2040
                };
                var socialPartners = _dnnDataContext.CSPTSMM_Partners
                    .Where(a => a.IsSubscribed && a.PortalId == _portalId && a.SocialId == (int) SocialLib.Common.Socialtype.Twitter)
                    .ToList()
                    .Where(a => partners.Contains(a.CspId)).ToList();

                LogInfo("Found " + socialPartners.Count + " partners to delete messages");

                foreach (var partner in socialPartners)
                {
                    LogInfo(string.Empty);
                    LogInfo("--PartnerId: " + partner.CspId);

                    var customMsg = _cspUtils.GetCustomContentFieldValue(content, partner.CspId, Common.Cons.TWITTER_MESSAGE_MESSAGE);
                    if (!string.IsNullOrEmpty(customMsg))
                    {
                        LogInfo("---Found custom content field value");
                        var userProfile = socialHelper.GetUserProfile(partner.Token, partner.TokenSecret);
                        if (userProfile.StatusCode == HttpStatusCode.OK)
                        {
                            var tweetUserId = JObject.Parse(userProfile.ResponseText)["id"];
                            LogInfo("Tweet UserId: " + tweetUserId.ToString());
                            //NOTE: because some tweet duplicated but the bitly url are different, so remove the url and let twitter find raw text
                            var msgExtractUrl =ExtractUrl(customMsg);
                            var searchjsonResult = socialHelper.SearchMsg(partner.Token, partner.TokenSecret, msgExtractUrl);
                            if (searchjsonResult.StatusCode == HttpStatusCode.OK)
                            {

                                List<Newtonsoft.Json.Linq.JToken> obj = JObject.Parse(searchjsonResult.ResponseText)["statuses"].ToList();
                                if (obj.Count >= 1) //more than 1 tweet similar content
                                {
                                    try
                                    {
                                        foreach (var tweetobj in obj)
                                        {
                                            var createdDate = DateTime.ParseExact(tweetobj["created_at"].ToString(), "ddd MMM dd HH:mm:ss zzz yyyy", CultureInfo.InvariantCulture);
                                            if (createdDate >= DateTime.Now.AddDays(-3) && tweetobj["user"]["id"].ToString() == tweetUserId.ToString())
                                            {
                                                var tweetId = tweetobj["id"];
                                                LogInfo("\tTweet Id: " + tweetId.ToString());
                                                var result = socialHelper.RemoveMsg(partner.Token, partner.TokenSecret, tweetId.ToString());
                                                LogInfo(string.Format("----Remove tweet id: {0} with result: {1}", tweetId, result.StatusCode));
                                                break;
                                            } // if tweet created between 2 days ago, and post by user,then remove it
                                        }


                                    }
                                    catch (Exception exception)
                                    {
                                        LogError(exception);
                                    }

                                }
                                else
                                {
                                    LogInfo("----Cant find tweet or tweet doest duplicated: " + customMsg);
                                }
                            }
                            else
                            {
                                Console.WriteLine("----Error when try to search tweet, error: " + searchjsonResult.ResponseText);
                            }
                        }
                        else
                        {
                            LogInfo("--Cant authenticate this partner with msg: " + userProfile.ResponseText);
                        }
                    }
                    else
                    {
                        Console.WriteLine("---Not found custom content field value");
                    }
                }
            }
            else
            {
                Console.WriteLine("Content with ID: " + contentId + " doest not exist");
            }
        }

        private void LogError(Exception ex)
        {
            Console.WriteLine("An Error occurred, please view log file for more detail");
            Console.WriteLine(ex.Message);
            log.Error(ex);
        }

        private void LogInfo(string msg)
        {
            Console.WriteLine(msg);
            log.Info(msg);
        }

        public string  ExtractUrl(string message)
        {
            var urlRx = new Regex(@"(http|ftp|https)://([\w+?\.\w+])+([a-zA-Z0-9\~\|\!\@\#\[\]{\}\$\%\^\&\*\(\)_\-\=\+\\\/\?\.\:\;\'\,]*)?", RegexOptions.IgnoreCase);
            MatchCollection matches = urlRx.Matches(message);
            var listUrl = (from Match match in matches select match.Value).ToList();

            foreach (var url in listUrl)
            {
                message = message.Replace(url, string.Empty);
            }
            return message;
        }

        private void InitResource()
        {
            _dnnDataContext = new Data.DnnDataContext(ConfigurationManager.ConnectionStrings["DnnConnectionString"].ConnectionString);
            _cspDataContext = new CspDataContext(ConfigurationManager.ConnectionStrings["CspConnectionString"].ConnectionString);
            _cspUtils = new CspUtils(_cspDataContext);
            _portalId = int.Parse(ConfigurationManager.AppSettings["portalid"]);
            _consumerKey = ConfigurationManager.AppSettings["consumerkey"];
            _consumerSecret = ConfigurationManager.AppSettings["consumersecret"];
        }
    }
}
