﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditCompanyParameters.ascx.cs" Inherits="TIEKinetix.CspModules.ManagePartnerV2.EditCompanyParameters" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>
<style type="text/css">
    #dnnCPWrap {
        display: none;
    }
    .dnnActionMenuBorder {
        display: none;
        border: none !important;
        box-shadow: none !important;
    }
    .dnnFormItem input, .dnnFormItem .dnnFormInput, .dnnFormItem textarea{ width: 300px;}
    #Body{background-color:#fff;} /*reset the background color*/
    
</style>
<asp:Panel runat="server" ID="plButtons" CssClass="heading-form-wraper">
    <asp:LinkButton runat="server" ID="btnSave" OnClick="btnSave_OnClick">
        <span class="button button-left" >
            <span class="button-right">
                <span class="button-mid">
                    <span class="button-text">
                        <%= GetLocalizedText("Label.Save")%>
                    </span>
                </span>
            </span>
        </span>
    </asp:LinkButton>
    <asp:LinkButton runat="server" ID="btnCancel">
        <span class="button button-left" >
            <span class="button-right">
                <span class="button-mid">
                    <span class="button-text">
                        <%= GetLocalizedText("Label.Cancel")%>
                    </span>
                </span>
            </span>
        </span>
    </asp:LinkButton>
    <telerik:RadScriptBlock runat="server">
        <script type="text/javascript">
            jQuery(function () {
                jQuery("#dnnCPWrap").remove();
                jQuery(".dnnActionMenuBorder").remove();
                jQuery(".dnnActionMenuTag").remove();
                jQuery('#<%= btnCancel.ClientID%>').click(function(e) {
                    e.preventDefault();
                    CloseWindow();
                    return false;
                });
            });
            
            function CloseWindow() {
                var oWindow = null;
                if (window.radWindow)
                    oWindow = window.radWindow;
                else if (window.frameElement.radWindow)
                    oWindow = window.frameElement.radWindow;

                if (oWindow != null) {
                    console.log(oWindow);
                    oWindow.set_visibleOnPageLoad(false);
                    oWindow.close();
                }
            }
        </script>
    </telerik:RadScriptBlock>
</asp:Panel>
<asp:Panel runat="server" ID="pnlistTextbox" CssClass="edit-form-wraper">
    
</asp:Panel>