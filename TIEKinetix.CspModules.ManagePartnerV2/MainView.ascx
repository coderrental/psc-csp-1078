﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainView.ascx.cs" Inherits="TIEKinetix.CspModules.ManagePartnerV2.MainView" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadWindow runat="server" ID="radWindow" Modal="true" VisibleStatusbar="False" ShowContentDuringLoad="False" Behaviors="Move,Close" Width="800" Height="700" />
<telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server" Skin="Windows7" BackgroundPosition="Top">
    </telerik:RadAjaxLoadingPanel>
<asp:Panel runat="server" ID="pnMain" CssClass="module_wrap">
    <p class="module_heading"><%= GetLocalizedText("Label.ModuleTitle") %></p>  
    <p runat="server" id="pMsg"></p>
    <asp:Panel runat="server" ID="pnGrid">
        <asp:Panel runat="server" ID="pnButtons" CssClass="buttons-wrap">
            <asp:Button runat="server" OnClick="btnExport_OnClick" ID="btnExport" CssClass="command-button command-button-export"/>
            <asp:Button runat="server" OnClick="btnExportTest_OnClick" ID="btnExportTest" CssClass="command-button command-button-exporttest"/>
            <asp:Button runat="server" CommandName="DeleteTest" OnClick="btnDeleteTest_OnClick" ID="btnDeleteTest" CssClass="command-button command-button-remove"/>
        </asp:Panel>
		<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="loadingPanel">
        <telerik:RadGrid ID="rgListCompanies" runat="server" AllowPaging="True" Skin="Default" AllowSorting="True" AllowFilteringByColumn="True" PageSize="50" OnItemCommand="rgListCompanies_OnItemCommand" OnColumnCreated="rgListCompanies_OnColumnCreated" OnNeedDataSource="rgListCompanies_OnNeedDataSource" OnPreRender="rgListCompanies_OnPreRender">
            <MasterTableView AutoGenerateColumns="True" DataKeyNames="companies_Id">
                <Columns>
                    <telerik:GridButtonColumn ButtonType="ImageButton" UniqueName="edit" CommandName="EditParameter">
            	        <HeaderStyle Width="50"></HeaderStyle>
			        </telerik:GridButtonColumn> 
                    <telerik:GridTemplateColumn UniqueName="checkremove" AllowFiltering="False">
                        <ItemTemplate>
                            <asp:CheckBox runat="server" ID="cbxIsTest"/>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="companies_Id" UniqueName="companies_id_static" ReadOnly="true" Visible="False" ForceExtractValue="Always" AllowSorting="True" AllowFiltering="True"/>
                    <telerik:GridBoundColumn DataField="companyname" UniqueName="companyname_static" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"  FilterControlWidth="150px" ShowFilterIcon="False" />
			        <telerik:GridBoundColumn DataField="state" AutoPostBackOnFilter="true" UniqueName="state_static" CurrentFilterFunction="Contains" ShowFilterIcon="False" />
                    <telerik:GridBoundColumn DataField="country" AutoPostBackOnFilter="true" UniqueName="country_static" CurrentFilterFunction="Contains" ShowFilterIcon="False" />
                    <telerik:GridDateTimeColumn DataField="created" AutoPostBackOnFilter="true" UniqueName="created_static" CurrentFilterFunction="EqualTo" ShowFilterIcon="False" FilterControlWidth="150px"/>                
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
		</telerik:RadAjaxPanel>
        <telerik:RadScriptBlock runat="server">
            <script type="text/javascript">
                jQuery(function() {
                   
                });
                function popupEdit(url) {
                    $find("<%=radWindow.ClientID %>").center();
                    $find("<%=radWindow.ClientID %>").set_navigateUrl(url);
                     $find("<%=radWindow.ClientID %>").show();
                     return false;
                 }
            </script>
        </telerik:RadScriptBlock>
    </asp:Panel>
</asp:Panel>