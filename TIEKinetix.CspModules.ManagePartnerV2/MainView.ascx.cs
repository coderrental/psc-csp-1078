﻿// ***********************************************************************
// Assembly         : TIEKinetix.CspModules.ManagePartnerV2
// Author           : VU DINH
// Created          : 06-13-2013
//
// Last Modified By : VU DINH
// Last Modified On : 06-13-2013
// ***********************************************************************
// <copyright file="MainView.ascx.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.ContentObjectLibrary.Data;
using DocumentFormat.OpenXml.Extensions;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DotNetNuke.Common;
using TIEKinetix.CspModules.ManagePartnerV2.Common;
using Telerik.Web.UI;

namespace TIEKinetix.CspModules.ManagePartnerV2
{
    /// <summary>
    /// Class MainView
    /// </summary>
    public partial class MainView : DnnModuleBase
    {
        private List<company> _listCompanies;
		private List<string> _ignoreColumns;
		private List<string> _numberColumns; 

        /// <summary>
        /// Handles the Init event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected override void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);

            bool isDisplayErrMsg = false;
            if (CspId == -1 && !IsAdmin())
            {
                pMsg.InnerText = GetLocalizedText("Label.MissingCspId");
                isDisplayErrMsg = true;
            }

            if (isDisplayErrMsg)
            {
                pnGrid.Visible = false;
                return;
            }

            LocalizeControls();

            _ignoreColumns = new List<string> { "companies_Id", "companyname", "state", "country", "created","Type"};

            //Init Grid
            //set case sentitive to false to alow filter case insensitive
            rgListCompanies.GroupingSettings.CaseSensitive = false;
            var editButton = (GridButtonColumn)rgListCompanies.Columns[0];
            editButton.ImageUrl = ControlPath + "images/edit-icon.png";
        }

        /// <summary>
        /// Localizes the controls.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        private void LocalizeControls()
        {
            btnDeleteTest.Text = GetLocalizedText("Button.DeleteTest");
            btnExport.Text = GetLocalizedText("Button.Export");
            btnExportTest.Text = GetLocalizedText("Button.ExportTest");
        }

       

        /// <summary>
        /// Inits the list companies.
        /// </summary>
        private void InitListCompanies()
        {
            _listCompanies = new List<company>();
			if(CspId!=-1&&!IsAdmin())
			{
				foreach (var company in CspDataContext.companies.Where(c => c.is_supplier.HasValue &&!c.is_supplier.Value))
				{
					var consumerType = CspUtils.GetCompanyParameter(company.companies_Id, Cons.COMPANYCONSUMERTYPE_PARAMETERNAME);
					if (consumerType == Cons.CONPANYCONSUMERTYPE_TEST)
						continue;
					if (company.parent_companies_Id != CspId)
							continue;
					_listCompanies.Add(company);
				}
			}
        	else
			{
				foreach (var company in CspDataContext.companies)
				{
					var consumerType = CspUtils.GetCompanyParameter(company.companies_Id, Cons.COMPANYCONSUMERTYPE_PARAMETERNAME);
					if (consumerType == Cons.CONPANYCONSUMERTYPE_TEST)
						continue;
					_listCompanies.Add(company);
				}
			}
        }

        /// <summary>
        /// Determines whether this current user is admin.
        /// </summary>
        private bool IsAdmin()
        {
            return UserInfo.IsInRole(PortalSettings.AdministratorRoleName);
        }

        /// <summary>
        /// Exports the data to excel.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="data">The data.</param>
        private void ExportDataToExcel(string filename, DataTable data)
        {
            //Create excel stream
            MemoryStream stream = SpreadsheetReader.Create();
            // Open the document for editing.
            using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(stream, true))
            {
                var sheets = spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>();
                Sheet sheet = sheets.Elements<Sheet>().Single(a => a.Name == "Sheet" + 1);
                sheet.Name = filename;
                spreadSheet.WorkbookPart.Workbook.Save();


                var worksheetPart = SpreadsheetReader.GetWorksheetPartByName(spreadSheet, filename);
                var writer = new WorksheetWriter(spreadSheet, worksheetPart);
                //Write data to sheet. Col name at A1, data from A2
                writer.PasteDataTable(data, "A1");
            }


            // Write to response stream
            Response.Clear();
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}.xlsx", filename));
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            stream.WriteTo(Response.OutputStream);
            Response.End();
        }

        /// <summary>
        /// Gets the data table from list.
        /// </summary>
        /// <param name="listCompanies">The list companies.</param>
        /// <returns>DataTable.</returns>
        private DataTable GetDataTableFromList(IEnumerable<company> listCompanies)
        {
            var listParameters = CspDataContext.companies_parameter_types.Where(a => a.IsExport == true).OrderBy(a => a.sortorder);
            var table = new DataTable();
            var firstRow = table.NewRow();


            var columnsName = new List<string> { "Company Id", "Company Name","Address1","City","State","Zipcode","Country","Website","Vendor Id","Vendor Name"};
			if(IsAdmin())
				columnsName.Add("Type");
        	columnsName.AddRange(listParameters.Select(parameterType => parameterType.parametername));
            foreach (var column in columnsName)
            {
                table.Columns.Add(column);
                firstRow[column] = column;
            }
            table.Rows.Add(firstRow);

            foreach (var company in listCompanies)
            {
				string vendorName = CspDataContext.companies.Single(a => a.companies_Id == company.parent_companies_Id).companyname;
                var row = table.NewRow();
                row["Company Name"] = company.companyname;
                row["Company Id"] = company.companies_Id;
            	row["Address1"] = company.address1;
            	row["City"] = company.city;
            	row["State"] = company.state;
            	row["Zipcode"] = company.zipcode;
            	row["Country"] = company.country;
            	row["Website"] = company.website_url;
            	row["Vendor Id"] = company.parent_companies_Id;
				if (string.IsNullOrEmpty(vendorName))
					row["Vendor Name"] = "";
				else
				{
					row["Vendor Name"] = vendorName;
				}
				if (IsAdmin())
				{
					if (company.is_supplier == true)
						row["Type"] = "Vendor";
					else
						row["Type"] = "Consumer";
				}
            	foreach (var parameterType in listParameters)
                {
                    var value = CspUtils.GetCompanyParameter(company.companies_Id, parameterType.parametername);
                    row[parameterType.parametername] = value;
                }
                table.Rows.Add(row);
            }
            return table;
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Handles the OnItemCommand event of the rgListCompanies control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="GridCommandEventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected void rgListCompanies_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "EditParameter")
            {
                var companyId = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["companies_Id"];
                var url = Globals.NavigateURL(TabId, "EditCompanyParameters", "mid", ModuleId.ToString())+"?companyId=" + companyId + "&SkinSrc=Portals/_Default/Skins/_default/No+Skin";
                var script = "function f(){$find(\"" + radWindow.ClientID + "\").set_navigateUrl(\"" + url +"\");$find(\"" + radWindow.ClientID +"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);  
            }
        }

        /// <summary>
        /// Handles the OnNeedDataSource event of the rgListCompanies control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="GridNeedDataSourceEventArgs"/> instance containing the event data.</param>
        protected void rgListCompanies_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
			InitListCompanies();
			var dtTable = new DataTable();
			var columns = new List<string>();
        	var stringcolumns = new List<string>();
			var numbercolumns = new List<string>();
			var emptycolumns = new List<string>();
        	int n;
			columns.AddRange(_ignoreColumns);
        	Random random = new Random();
			foreach (var companyParameter in ListCompaniesParameterToGrid)
			{
				columns.Add(companyParameter);
			}

			foreach (var column in columns)
			{
				dtTable.Columns.Add(column);
			}
			foreach (var company in _listCompanies)
			{
				var row = dtTable.NewRow();
				row["companies_Id"] = company.companies_Id;
				row["companyname"] = company.companyname;
				row["state"] = company.state;
				row["country"] = company.country;
				row["created"] = company.created;
				if(company.is_supplier==true)
					row["Type"] = "Vendor";
				else
					row["Type"] = "Consumer";
				foreach (var companyParameter in ListCompaniesParameterToGrid)
				{
					row[companyParameter] = CspUtils.GetCompanyParameter(company.companies_Id, companyParameter);
				}
				dtTable.Rows.Add(row);
			}
#region Change datatype of numeric columns
			//Insert string columns
			foreach (DataColumn columntb in dtTable.Columns)
        	{
        		foreach (DataRow rowtb in dtTable.Rows)
        		{
					if(string.IsNullOrEmpty(rowtb[columntb].ToString()))
						continue;
					if (!int.TryParse(rowtb[columntb].ToString(), out n))
					{
						stringcolumns.Add(columntb.ToString());
						break;
					}
        		}
				
        	}
			//Insert numbers columns
			foreach (var column in columns)
			{
				if (!stringcolumns.Contains(column))
					numbercolumns.Add(column);
			}
			//Insert empty columns
			foreach (DataColumn columntb in dtTable.Columns)
			{
				bool colEmpty = true;
				foreach (DataRow rowtb in dtTable.Rows)
				{
					if (!string.IsNullOrEmpty(rowtb[columntb].ToString()))
						colEmpty = false;
					
				}
				if (colEmpty)
					emptycolumns.Add(columntb.ColumnName);
			}
			//Remove empty colums in numbercolumns
        	foreach (var emptycolumn in emptycolumns)
        	{
        		if (numbercolumns.Contains(emptycolumn))
        		{
        			numbercolumns.Remove(emptycolumn);
        		}
        	}

        	DataTable dtResutl = dtTable.Clone();
			//Set DataType number columns 
			foreach (var column in numbercolumns)
			{
				dtResutl.Columns[column].DataType = typeof(int);

			}
			//Insert value 0 into null row
			foreach (DataColumn column in dtTable.Columns)
			{
				foreach (DataRow row in dtTable.Rows)
				{
					if (numbercolumns.Contains(column.ToString()) && string.IsNullOrEmpty(row[column].ToString()))
						row[column] = 0;
				}
			}
        	foreach (DataRow row in dtTable.Rows)
        	{
        		dtResutl.ImportRow(row);
        	}
        	

#endregion
			rgListCompanies.DataSource = dtResutl;
        }

        /// <summary>
        /// Handles the OnClick event of the btnExport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected void btnExport_OnClick(object sender, EventArgs e)
        {
            var listCompanies = new List<company>();
			if (CspId != -1 && !IsAdmin())
			{
				foreach (var company in CspDataContext.companies.Where(c => c.is_supplier.HasValue && !c.is_supplier.Value))
				{
					var consumerType = CspUtils.GetCompanyParameter(company.companies_Id, Cons.COMPANYCONSUMERTYPE_PARAMETERNAME);
					if (consumerType == Cons.CONPANYCONSUMERTYPE_TEST)
						continue;
					if (company.parent_companies_Id != CspId)
						continue;
					listCompanies.Add(company);
				}
			}
			else
			{
				foreach (var company in CspDataContext.companies)
				{
					var consumerType = CspUtils.GetCompanyParameter(company.companies_Id, Cons.COMPANYCONSUMERTYPE_PARAMETERNAME);
					if (consumerType == Cons.CONPANYCONSUMERTYPE_TEST)
						continue;
					listCompanies.Add(company);
				}
			}
            ExportDataToExcel("ExportAccount",GetDataTableFromList(listCompanies));
        }

        /// <summary>
        /// Handles the OnClick event of the btnExportTest control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected void btnExportTest_OnClick(object sender, EventArgs e)
        {
            var listCompanies = new List<company>();
			if (CspId != -1 && !IsAdmin())
			{
				foreach (var company in CspDataContext.companies.Where(c => c.is_supplier.HasValue && !c.is_supplier.Value))
				{
					if (company.parent_companies_Id != CspId)
						continue;
					listCompanies.Add(company);
				}
			}
			else
			{
				foreach (var company in CspDataContext.companies)
				{
					listCompanies.Add(company);
				}
			}
            ExportDataToExcel("ExportTestAccount", GetDataTableFromList(listCompanies));
        }

        /// <summary>
        /// Handles the OnClick event of the btnDeleteTest control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected void btnDeleteTest_OnClick(object sender, EventArgs e)
        {
            GridItemCollection gridRows = rgListCompanies.Items;
            foreach (GridDataItem data in gridRows)
            {
                var checkbox = (CheckBox)data["checkremove"].FindControl("cbxIsTest");
                if (checkbox.Checked)
                {
                    var companyId = int.Parse(data["companies_Id"].Text);
                    CspUtils.SetCompanyParameter(companyId, Cons.COMPANYCONSUMERTYPE_PARAMETERNAME, Cons.CONPANYCONSUMERTYPE_TEST);
                }
            }
            rgListCompanies.Rebind();
        }

        /// <summary>
        /// Handles the OnPreRender event of the rgListCompanies control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void rgListCompanies_OnPreRender(object sender, EventArgs e)
        {
            //Hide edit column when there are no company parameters to edit
            var editColumn = rgListCompanies.MasterTableView.GetColumn("edit") as GridButtonColumn;
            if (editColumn != null && !ListCompanyParameters.Any())
                editColumn.Display = false;

            //Hide all static columns
            foreach (var ignoreColumn in _ignoreColumns)
            {
                var column = rgListCompanies.MasterTableView.GetColumn(ignoreColumn) as GridBoundColumn;
				if (column != null && ignoreColumn != "Type")
                    column.Display = false;
            }
			//Hide type column when user is admin
			if(CspId!=-1&&!IsAdmin())
			{
				var column = rgListCompanies.MasterTableView.GetColumn("Type") as GridBoundColumn;
				if (column != null) column.Display = false;
			}
        }

        /// <summary>
        /// Handles the OnColumnCreated event of the rgListCompanies control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="GridColumnCreatedEventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected void rgListCompanies_OnColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
			e.Column.ShowFilterIcon = false;
			if (ListCompaniesParameterToGrid.Contains(e.Column.UniqueName))
			{
				e.Column.HeaderText = GetParameterTranslated(e.Column.UniqueName);
				e.Column.AutoPostBackOnFilter = true;
				e.Column.CurrentFilterFunction = GridKnownFunction.Contains;
			}
        	var listStaticColumn = new[]
        	                       	{
        	                       		"edit", "checkremove", "companies_id_static", "companyname_static", "state_static",
        	                       		"country_static", "created_static"
        	                       	};
        	foreach (var column in listStaticColumn)
        	{
        		if (rgListCompanies.MasterTableView.Columns.FindByUniqueName(column) != null)
        		{
					rgListCompanies.MasterTableView.Columns.FindByUniqueName(column).HeaderText = GetLocalizedText("Column."+column);
        		}
        	}
        }
    }
}