﻿// ***********************************************************************
// Assembly         : TIEKinetix.CspModules.ManagePartnerV2
// Author           : VU DINH
// Created          : 06-13-2013
//
// Last Modified By : VU DINH
// Last Modified On : 06-13-2013
// ***********************************************************************
// <copyright file="EditCompanyParameters.ascx.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.ContentObjectLibrary.Data;
using TIEKinetix.CspModules.ManagePartnerV2.Common;

namespace TIEKinetix.CspModules.ManagePartnerV2
{
    /// <summary>
    /// Class EditCompanyParameters
    /// </summary>
    public partial class EditCompanyParameters : DnnModuleBase
    {

        private company _company;
        private List<companies_parameter_type> _companyParametersList;

        /// <summary>
        /// Handles the Init event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected override void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);

            var companyId = int.Parse(Request.Params["companyId"]);
            _company = CspDataContext.companies.SingleOrDefault(a => a.companies_Id == companyId);

            if (_company != null)
            {
                _companyParametersList = CspDataContext.companies_parameter_types.Where(a => ListCompanyParameters.Contains(a.parametername)).ToList();
                InitControls();    
            }
        }

        /// <summary>
        /// Inits the controls.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        private void InitControls()
        {
            var tbl = new Table {CssClass = "edit-form-table"};

            //add header row
            var firstRow = new TableRow();
            firstRow.Cells.Add(new TableCell
                {
                    Text = GetLocalizedText("Label.Column.ParameterName"),
                    CssClass = "form-table-heading"
                });
            firstRow.Cells.Add(new TableCell
                {
                    Text = GetLocalizedText("Label.Column.ParameterValue"),
                    CssClass = "form-table-heading"
                });
            tbl.Rows.Add(firstRow);

            foreach (var companiesParameterType in _companyParametersList)
            {
                var tr = new TableRow
                    {
                        CssClass = (_companyParametersList.IndexOf(companiesParameterType) % 2 == 0 ? "row-odd" : "row-even") + " dnnFormItem"
                    };
                tbl.Rows.Add(tr);
                

                var label = new Label {Text = GetParameterTranslated(companiesParameterType.parametername), CssClass = "edit-form-label"};
                var textbox = new TextBox
                    {
                        ID = "tbx" + companiesParameterType.companies_parameter_types_Id,
                        Text = CspUtils.GetCompanyParameter(_company.companies_Id, companiesParameterType.parametername),
                        CssClass = "edit-form-textbox"
                    };

                var leftcell = new TableCell();
                leftcell.Controls.Add(label);
                
                var rightcell = new TableCell();
                rightcell.Controls.Add(textbox);

                tr.Cells.Add(leftcell);
                tr.Cells.Add(rightcell);

            }
            pnlistTextbox.Controls.Add(tbl);
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// Handles the OnClick event of the btnSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            foreach (var companiesParameterType in _companyParametersList)
            {
                var tbx = (TextBox)pnlistTextbox.FindControl("tbx" + companiesParameterType.companies_parameter_types_Id);
                if (tbx != null)
                {
                    CspUtils.SetCompanyParameter(_company.companies_Id,companiesParameterType.parametername,tbx.Text.Trim());
                }
            }
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", "CloseWindow()", true);  
        }
    }
}