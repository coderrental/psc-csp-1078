﻿// ***********************************************************************
// Assembly         : TIEKinetix.CspModules.ManagePartnerV2
// Author           : VU DINH
// Created          : 06-13-2013
//
// Last Modified By : VU DINH
// Last Modified On : 06-13-2013
// ***********************************************************************
// <copyright file="Settings.ascx.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using DotNetNuke.Entities.Modules;
using TIEKinetix.CspModules.ManagePartnerV2.Common;
using TIEKinetix.CspModules.ManagePartnerV2.Entities;
using Telerik.Web.UI;

namespace TIEKinetix.CspModules.ManagePartnerV2
{
    /// <summary>
    /// Class Settings
    /// </summary>
    public partial class Settings : ModuleSettingsBase
    {
        /// <summary>
        /// Loads the settings.
        /// </summary>
       public override void LoadSettings()
        {
            if (TabModuleSettings[Cons.SETTING_CONNECTION_STRING_NAME] != null)
                tbxConnectionStringName.Text = TabModuleSettings[Cons.SETTING_CONNECTION_STRING_NAME].ToString();
            InitCompanyParameterList();
			InitCompanyParameterListDisplay();
       }

       /// <summary>
       /// Inits the company parameter list.
       /// </summary>
        private void InitCompanyParameterList()
        {
            var cspDataContext = new CspDataContext(Utils.GetConnectionString(PortalId));
            List<companies_parameter_type> coParamTypes = cspDataContext.companies_parameter_types.OrderBy(a => a.parametername).ToList();
            var crrCoParams = new List<CompanyParameter>();
            var srcCoParams = new List<CompanyParameter>();

            if (TabModuleSettings[Cons.SETTING_COMPAMY_PARAMETERS] != null && !String.IsNullOrEmpty(TabModuleSettings[Cons.SETTING_COMPAMY_PARAMETERS].ToString()))
            {
                String crrCoParamsString = TabModuleSettings[Cons.SETTING_COMPAMY_PARAMETERS].ToString();
                crrCoParams.AddRange(crrCoParamsString.Split(',').Select(coParam => new CompanyParameter
                {
                    Id = coParam,
                    Text = coParam
                }));
                if (crrCoParams.Any())
                {
                    cpListBoxDest.DataSource = crrCoParams;
                    cpListBoxDest.DataBind();
                }
            }

            foreach (companies_parameter_type companiesParameterType in coParamTypes)
            {
                if (crrCoParams.Any())
                {
                    var isExist = crrCoParams.Any(crrCoParam => crrCoParam.Text == companiesParameterType.parametername);
                    if (isExist)
                        continue;
                }
                srcCoParams.Add(new CompanyParameter
                {
                    Id = companiesParameterType.parametername,
                    Text = companiesParameterType.parametername
                });
            }
            cpListBoxSrc.DataSource = srcCoParams;
            cpListBoxSrc.DataBind();
        }

        /// <summary>
        /// Updates the settings.
        /// </summary>
        public override void UpdateSettings()
        {
            var moduleController = new ModuleController();

            moduleController.UpdateTabModuleSetting(TabModuleId,Cons.SETTING_CONNECTION_STRING_NAME, tbxConnectionStringName.Text);
            
            //Update Company Parameters Setting
			String newCoParams = "";
			foreach (RadListBoxItem radListBoxItem in cpListBoxDest.Items)
			{
				if (newCoParams != "")
					newCoParams += ",";
				newCoParams += radListBoxItem.Text;
			}

			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_COMPAMY_PARAMETERS, newCoParams);

            AddCompanyParameterToResxFile(newCoParams);
			//Update Company Parameters Display
			String newCoParamsDisplay = "";
			foreach (RadListBoxItem radListBoxItem in cpListBoxDestDisplay.Items)
			{
				if (newCoParamsDisplay != "")
					newCoParamsDisplay += ",";
				newCoParamsDisplay += radListBoxItem.Text;
			}
			moduleController.UpdateTabModuleSetting(TabModuleId, Cons.SETTING_COMPAMY_PARAMETERS_DISPLAY, newCoParamsDisplay);
			AddCompanyParameterToResxFile(newCoParamsDisplay);

			ModuleController.SynchronizeModule(ModuleId);
        }

        /// <summary>
        /// Adds the company parameter to RESX file.
        /// </summary>
        /// <param name="list">The list.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        private void AddCompanyParameterToResxFile(string list)
        {
            var listParam = list.Split(new[] {","}, StringSplitOptions.RemoveEmptyEntries).ToList();
            if (listParam.Any())
            {
                var filename = Server.MapPath(ControlPath + "App_LocalResources\\ManagePartnerV2.ascx.resx");
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(filename);

                foreach (var param in listParam)
                {
                    var name = "Parameter." + param;
                    //remove exist nodes
                    XmlNodeList xnList = xmlDoc.SelectNodes("/root/data[@name='" + name + "']");
                    if (xnList != null)
                    {
                        foreach (XmlNode node in xnList)
                        {
                            if (node.ParentNode != null)
                                node.ParentNode.RemoveChild(node);
                        }
                    }
                   
                    //add new node
                    XmlElement newElem = xmlDoc.CreateElement("data");
                    newElem.SetAttribute("name", name);
                    newElem.SetAttribute("xml:space", "preserve");
                    newElem.InnerXml = "<value></value>";
                    var xmlElement = newElem["value"];
                    if (xmlElement != null)
                        xmlElement.InnerText = param;
                    if (xmlDoc.DocumentElement != null)
                        xmlDoc.DocumentElement.AppendChild(newElem);
                }

                xmlDoc.Save(filename);
            }  
        }
		private void InitCompanyParameterListDisplay()
		{
			var cspDataContext = new CspDataContext(Utils.GetConnectionString(PortalId));
			List<companies_parameter_type> coParamTypes = cspDataContext.companies_parameter_types.OrderBy(a => a.parametername).ToList();
			var crrCoParams = new List<CompanyParameter>();
			var srcCoParams = new List<CompanyParameter>();

			if (TabModuleSettings[Cons.SETTING_COMPAMY_PARAMETERS_DISPLAY] != null && !String.IsNullOrEmpty(TabModuleSettings[Cons.SETTING_COMPAMY_PARAMETERS_DISPLAY].ToString()))
			{
				String crrCoParamsString = TabModuleSettings[Cons.SETTING_COMPAMY_PARAMETERS_DISPLAY].ToString();
				crrCoParams.AddRange(crrCoParamsString.Split(',').Select(coParam => new CompanyParameter
				{
					Id = coParam,
					Text = coParam
				}));
				if (crrCoParams.Any())
				{
					cpListBoxDestDisplay.DataSource = crrCoParams;
					cpListBoxDestDisplay.DataBind();
				}
			}

			foreach (companies_parameter_type companiesParameterType in coParamTypes)
			{
				if (crrCoParams.Any())
				{
					var isExist = crrCoParams.Any(crrCoParam => crrCoParam.Text == companiesParameterType.parametername);
					if (isExist)
						continue;
				}
				srcCoParams.Add(new CompanyParameter
				{
					Id = companiesParameterType.parametername,
					Text = companiesParameterType.parametername
				});
			}
			cpListBoxSrcDisplay.DataSource = srcCoParams;
			cpListBoxSrcDisplay.DataBind();
		}
    }
}