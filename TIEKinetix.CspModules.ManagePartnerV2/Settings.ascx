﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Settings.ascx.cs" Inherits="TIEKinetix.CspModules.ManagePartnerV2.Settings" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div>
    <table>
        <tbody>
            <tr>
                <td>Connection String Name:</td>
                <td><telerik:RadTextBox runat="server" ID="tbxConnectionStringName"/></td>
            </tr>
			<tr>
                <td valign="top">
                    Company Parameters for Edit:
                </td>
                <td>
                    <telerik:RadListBox runat="server" ID="cpListBoxSrc" Height="200px" Width="250px" 
					DataTextField="Text" DataValueField="Id" DataKeyField="Id"
					AllowTransfer="true" TransferToID="cpListBoxDest"></telerik:RadListBox>

					 <telerik:RadListBox runat="server" ID="cpListBoxDest" Height="200px" Width="250px" DataTextField="Text" DataValueField="Id" DataKeyField="Id"></telerik:RadListBox>
				</td>
            </tr>
			<tr>
                <td valign="top">
                    Company Parameters for Display:
                </td>
                <td>
                    <telerik:RadListBox runat="server" ID="cpListBoxSrcDisplay" Height="200px" Width="250px" 
					DataTextField="Text" DataValueField="Id" DataKeyField="Id"
					AllowTransfer="true" TransferToID="cpListBoxDestDisplay"></telerik:RadListBox>

					 <telerik:RadListBox runat="server" ID="cpListBoxDestDisplay" Height="200px" Width="250px" DataTextField="Text" DataValueField="Id" DataKeyField="Id"></telerik:RadListBox>
				</td>
            </tr>
		</tbody>
	</table>
</div>