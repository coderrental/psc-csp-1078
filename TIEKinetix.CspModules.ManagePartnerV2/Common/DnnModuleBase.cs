﻿// ***********************************************************************
// Assembly         : TIEKinetix.CspModules.ManagePartnerV2
// Author           : VU DINH
// Created          : 06-13-2013
//
// Last Modified By : VU DINH
// Last Modified On : 06-13-2013
// ***********************************************************************
// <copyright file="DnnModuleBase.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using CR.ContentObjectLibrary.Data;
using CR.DnnModules.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Localization;

namespace TIEKinetix.CspModules.ManagePartnerV2.Common
{
    /// <summary>
    /// Class DnnModuleBase
    /// </summary>
    public class DnnModuleBase : PortalModuleBase
    {
        private string _localResourceFile;
        internal CspDataContext CspDataContext;
        internal CspUtils CspUtils;
		internal List<string> ListCompanyParameters, ListCompaniesParameterToGrid; 
        internal int CspId;
        /// <summary>
        /// Handles the Init event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void Page_Init(object sender, EventArgs e)
        {
            //Init Local Resource file
            _localResourceFile = Utils.GetCommonResourceFile("ManagePartnerV2", LocalResourceFile);
            //Init module title
            ModuleConfiguration.ModuleTitle = GetLocalizedText("Label.ModuleTitle");
            //Add google font for fancy layout
            Page.Header.Controls.Add(new LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"http://fonts.googleapis.com/css?family=Exo\" />"));

            CspId = Utils.GetIntegrationKey(UserInfo);

            //Init Data
            if (Settings[Cons.SETTING_CONNECTION_STRING_NAME] != null &&Settings[Cons.SETTING_CONNECTION_STRING_NAME].ToString() != string.Empty)
            {
                CspDataContext = new CspDataContext(Config.GetConnectionString(Settings[Cons.SETTING_CONNECTION_STRING_NAME].ToString()));
            }
            else
            {
                CspDataContext = new CspDataContext(Utils.GetConnectionString(PortalId));    
            }
            CspUtils = new CspUtils(CspDataContext);

            ListCompanyParameters = new List<string>();
        	ListCompaniesParameterToGrid = new List<string>();
            if (Settings[Cons.SETTING_COMPAMY_PARAMETERS] != null)
            {
                ListCompanyParameters = Settings[Cons.SETTING_COMPAMY_PARAMETERS].ToString().Split(new[] {","},StringSplitOptions.RemoveEmptyEntries).ToList();
            }
			if (Settings[Cons.SETTING_COMPAMY_PARAMETERS_DISPLAY] != null)
			{
				ListCompaniesParameterToGrid = Settings[Cons.SETTING_COMPAMY_PARAMETERS_DISPLAY].ToString().Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
			}
        }

        /// <summary>
        /// Gets the portal external identifier.
        /// </summary>
        /// <returns>System.String.</returns>
        protected string GetPortalExternalIdentifier()
        {
            string s = Utils.GetSetting(PortalId, "ExternalIdentifier");
            return !string.IsNullOrEmpty(s) ? s : Request.Url.Host;
        }

        /// <summary>
        /// Gets the parameter translated.
        /// </summary>
        /// <param name="parametername">The parametername.</param>
        /// <returns>System.String.</returns>
        public string GetParameterTranslated(string parametername)
        {
            var translated = GetLocalizedText("Parameter." + parametername);
            return string.IsNullOrEmpty(translated) ? parametername : translated;
        }

        /// <summary>
        /// Gets the localized text.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>System.String.</returns>
        public string GetLocalizedText(string key)
        {
            return Localization.GetString(key, _localResourceFile);
        }
    }
}