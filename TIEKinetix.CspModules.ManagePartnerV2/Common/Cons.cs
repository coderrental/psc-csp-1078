﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TIEKinetix.CspModules.ManagePartnerV2.Common
{
    public class Cons
    {
        public const string COMPANYCONSUMERTYPE_PARAMETERNAME = "ConsumerType";
        public const string CONPANYCONSUMERTYPE_TEST = "T";
        public const string SETTING_COMPAMY_PARAMETERS = "CompanyParametersList";
		public const string SETTING_COMPAMY_PARAMETERS_DISPLAY = "CompanyParametersListDisplay";
        public const string SETTING_CONNECTION_STRING_NAME = "ConnectionStringName";
    }
}