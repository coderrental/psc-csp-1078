﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace TIEKinetix.CspModules.ManagePartnerV2.Entities
{
    public class DynamicObject
    {
         private dynamic _object;
        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyParameter"/> class.
        /// </summary>
         public DynamicObject()
        {
            _object = new ExpandoObject();
        }

        public void SetProperties(string propertyName, string value)
        {
            _object[propertyName] = value;
        }

        public dynamic Object { get { return _object; } }
    }
}