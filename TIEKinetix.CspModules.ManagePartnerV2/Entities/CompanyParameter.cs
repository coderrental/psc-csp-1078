﻿// ***********************************************************************
// Assembly         : TIEKinetix.CspModules.ManagePartnerV2
// Author           : VU DINH
// Created          : 06-13-2013
//
// Last Modified By : VU DINH
// Last Modified On : 06-13-2013
// ***********************************************************************
// <copyright file="CompanyParameter.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace TIEKinetix.CspModules.ManagePartnerV2.Entities
{
    /// <summary>
    /// Class CompanyParameter
    /// </summary>
    public class CompanyParameter
    {
        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>The text.</value>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        public string Id { get; set; }
    }
}