﻿// ***********************************************************************
// Assembly         : SurfWithDotNet
// Author           : VU DINH
// Created          : 06-25-2013
//
// Last Modified By : VU DINH
// Last Modified On : 06-25-2013
// ***********************************************************************
// <copyright file="ObjectEntity.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;

namespace SurfWithDotNet.Entities
{
    /// <summary>
    /// Class ObjectEntity
    /// </summary>
    class ObjectEntity
    {
        /// <summary>
        /// Gets or sets the object id.
        /// </summary>
        /// <value>The object id.</value>
        public Guid ObjectId { get; set; }

        /// <summary>
        /// Gets or sets the point.
        /// </summary>
        /// <value>The point.</value>
        public Point[] Points { get; set; }

        /// <summary>
        /// Gets or sets the field value.
        /// </summary>
        /// <value>The field value.</value>
        public Hashtable FieldValuePairs { get; set; }

        /// <summary>
        /// Gets or sets the list reseller.
        /// </summary>
        /// <value>The list reseller.</value>
        public List<ResellerEntity> ListResellers { get; set; } 
    }
}
