﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;
using SurfWithDotNet.API;

namespace SurfWithDotNet.Forms
{
    public partial class MainForm : System.Windows.Forms.Form
    {
        private Capture _capture;
        private Image<Bgr, Byte> _object;

        public MainForm()
        {
            InitializeComponent();
            _object = new Image<Bgr, Byte>(Properties.Resources.book);
            imageBox1.Image = _object;
            _capture = new Capture();
            _capture.QueryFrame();
           Application.Idle += FrameCapture;

        }

        /// <summary>
        /// Frames the capture.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 5/17/2013 - 9:28 AM
        private void FrameCapture(object sender, EventArgs e)
        {
            var frame = _capture.QueryFrame();
            var objectDetector = new API.ObjectDetectorHelper();
            var result = objectDetector.DetectObject(_object.ToBitmap(), frame.ToBitmap(),ObjectDetectorHelper.Detector.TemplateMaching);
            imgBox.Image = result;
        }

    }
}
