﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.DirectX.AudioVideoPlayback;

namespace SurfWithDotNet.Forms
{
    public partial class Video : Form
    {
        private Microsoft.DirectX.AudioVideoPlayback.Video _video;
        public Video()
        {
            InitializeComponent();
            tbarTime.Enabled = false;
        }

        #region [ Event Handlers ]
        /// <summary>
        /// Handles the Click event of the btnLoadVideo control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 5/20/2013 - 10:30 AM
        private void BtnLoadVideoClick(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                // store the original size of the panel
                int width = pnVideoPlayer.Width;
                int height = pnVideoPlayer.Height;

                // load the selected video file
                _video = new Microsoft.DirectX.AudioVideoPlayback.Video(openFileDialog.FileName)
                             {
                                 Owner = pnVideoPlayer,
                                 Size = new Size(width, height)
                             };
                // stop the video
                _video.Stop();
                
                tbarTime.Enabled = true;
            }
        }

        /// <summary>
        /// Handles the Click event of the btnPlay control.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 5/20/2013 - 10:34 AM
        private void BtnPlayClick(object sender, EventArgs e)
        {
            if (_video.State != StateFlags.Running)
            {
                _video.Play();
            }
        }

        /// <summary>
        /// Handles the Click event of the btnPause control.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 5/20/2013 - 10:35 AM
        private void BtnPauseClick(object sender, EventArgs e)
        {
            if (_video.State != StateFlags.Paused)
            {
                _video.Pause();
            }
        }

        /// <summary>
        /// Handles the Click event of the btnStop control.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 5/20/2013 - 10:35 AM
        private void BtnStopClick(object sender, EventArgs e)
        {
            if (_video.State != StateFlags.Stopped)
            {
                _video.Stop();
            }
        }
        #endregion

        /// <summary>
        /// Handles the Tick event of the timer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 5/20/2013 - 3:11 PM
        private void timer_Tick(object sender, EventArgs e)
        {
            tbarTime.Maximum = (int) _video.Duration;
            tbarTime.Value = (int) _video.CurrentPosition;
        }

        /// <summary>
        /// Handles the MouseDown event of the tbarTime control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.MouseEventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 5/20/2013 - 3:14 PM
        private void tbarTime_MouseDown(object sender, MouseEventArgs e)
        {
            timer.Enabled = false;
        }

        /// <summary>
        /// Handles the MouseUp event of the tbarTime control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.MouseEventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 5/20/2013 - 3:14 PM
        private void tbarTime_MouseUp(object sender, MouseEventArgs e)
        {
            _video.CurrentPosition = tbarTime.Value;
            timer.Enabled = true;
        }



        
    }
}
