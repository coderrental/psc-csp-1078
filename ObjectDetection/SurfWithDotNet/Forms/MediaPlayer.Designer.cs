﻿namespace SurfWithDotNet.Forms
{
    partial class MediaPlayer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MediaPlayer));
            this.axWMP = new AxWMPLib.AxWindowsMediaPlayer();
            this.tbarTime = new System.Windows.Forms.TrackBar();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnPause = new System.Windows.Forms.Button();
            this.btnPlay = new System.Windows.Forms.Button();
            this.btnLoadVideo = new System.Windows.Forms.Button();
            this.openVideo = new System.Windows.Forms.OpenFileDialog();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.openImage = new System.Windows.Forms.OpenFileDialog();
            this.imageBoxResult = new Emgu.CV.UI.ImageBox();
            ((System.ComponentModel.ISupportInitialize)(this.axWMP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbarTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBoxResult)).BeginInit();
            this.SuspendLayout();
            // 
            // axWMP
            // 
            this.axWMP.Enabled = true;
            this.axWMP.Location = new System.Drawing.Point(12, 12);
            this.axWMP.Name = "axWMP";
            this.axWMP.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axWMP.OcxState")));
            this.axWMP.Size = new System.Drawing.Size(590, 464);
            this.axWMP.TabIndex = 0;
            // 
            // tbarTime
            // 
            this.tbarTime.Location = new System.Drawing.Point(12, 492);
            this.tbarTime.Name = "tbarTime";
            this.tbarTime.Size = new System.Drawing.Size(590, 45);
            this.tbarTime.TabIndex = 10;
            this.tbarTime.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tbarTime_MouseDown);
            this.tbarTime.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tbarTime_MouseUp);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(293, 543);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 9;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnPause
            // 
            this.btnPause.Location = new System.Drawing.Point(199, 542);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(75, 23);
            this.btnPause.TabIndex = 8;
            this.btnPause.Text = "Pause";
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnPlay
            // 
            this.btnPlay.Location = new System.Drawing.Point(105, 543);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(75, 23);
            this.btnPlay.TabIndex = 7;
            this.btnPlay.Text = "Play";
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // btnLoadVideo
            // 
            this.btnLoadVideo.Location = new System.Drawing.Point(11, 543);
            this.btnLoadVideo.Name = "btnLoadVideo";
            this.btnLoadVideo.Size = new System.Drawing.Size(75, 23);
            this.btnLoadVideo.TabIndex = 6;
            this.btnLoadVideo.Text = "Load Video";
            this.btnLoadVideo.UseVisualStyleBackColor = true;
            this.btnLoadVideo.Click += new System.EventHandler(this.btnLoadVideo_Click);
            // 
            // openVideo
            // 
            this.openVideo.FileName = "Open video file";
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // openImage
            // 
            this.openImage.FileName = "Open image file";
            // 
            // imageBoxResult
            // 
            this.imageBoxResult.Location = new System.Drawing.Point(11, 12);
            this.imageBoxResult.Name = "imageBoxResult";
            this.imageBoxResult.Size = new System.Drawing.Size(591, 463);
            this.imageBoxResult.TabIndex = 2;
            this.imageBoxResult.TabStop = false;
            // 
            // MediaPlayer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(618, 658);
            this.Controls.Add(this.imageBoxResult);
            this.Controls.Add(this.tbarTime);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnPause);
            this.Controls.Add(this.btnPlay);
            this.Controls.Add(this.btnLoadVideo);
            this.Controls.Add(this.axWMP);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MediaPlayer";
            this.Text = "MediaPlayer";
            ((System.ComponentModel.ISupportInitialize)(this.axWMP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbarTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBoxResult)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AxWMPLib.AxWindowsMediaPlayer axWMP;
        private System.Windows.Forms.TrackBar tbarTime;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Button btnLoadVideo;
        private System.Windows.Forms.OpenFileDialog openVideo;
        private System.Windows.Forms.Timer timer;
        private Emgu.CV.UI.ImageBox imageBoxResult;
        private System.Windows.Forms.OpenFileDialog openImage;
    }
}