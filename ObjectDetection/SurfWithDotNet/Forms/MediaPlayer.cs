﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;
using Newtonsoft.Json;
using SurfWithDotNet.API;
using System.Configuration;

namespace SurfWithDotNet.Forms
{
    public partial class MediaPlayer : Form
    {

        private Image<Rgb, byte> _originalImage; 
        /// <summary>
        /// Initializes a new instance of the <see cref="MediaPlayer"/> class.
        /// </summary>
        /// Author: Vu Dinh
        /// 5/20/2013 - 5:04 PM
        public MediaPlayer()
        {
            InitializeComponent();
            tbarTime.Enabled = false;
            axWMP.uiMode = "none";
            axWMP.BringToFront();
            
        }

        /// <summary>
        /// Handles the Click event of the btnLoadVideo control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 5/20/2013 - 5:04 PM
        private void btnLoadVideo_Click(object sender, EventArgs e)
        {

            if (openVideo.ShowDialog() == DialogResult.OK)
            {
                // load the selected video file
                axWMP.URL = openVideo.FileName;
                axWMP.Ctlcontrols.stop();
                tbarTime.Enabled = true;
            }
        }

        /// <summary>
        /// Handles the Click event of the btnPlay control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 5/20/2013 - 5:08 PM
        private void btnPlay_Click(object sender, EventArgs e)
        {
            axWMP.Ctlcontrols.play();
            axWMP.Visible = true;
            axWMP.BringToFront();
        }

        /// <summary>
        /// Handles the Click event of the btnPause control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 5/20/2013 - 5:08 PM
        private void btnPause_Click(object sender, EventArgs e)
        {
            axWMP.Ctlcontrols.pause();
            if (axWMP.URL != null)
            {
                var bitmap = new Bitmap(axWMP.Width, axWMP.Height);
                var graphic = Graphics.FromImage(bitmap);
                graphic.CopyFromScreen(axWMP.PointToScreen(new Point()).X,axWMP.PointToScreen(new Point()).Y,0, 0,new Size(axWMP.Width,axWMP.Height));
                var ms = new MemoryStream();

                bitmap.Save(ms, ImageFormat.Jpeg);

                byte[] buffer = ms.ToArray();

                string postData = Convert.ToBase64String(buffer);
                var post = Encoding.UTF8.GetBytes(postData);
                var req = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings.Get("ApiUrl"));

                req.Method = "POST";
                req.ContentType = "application/x-www-form-urlencoded";
                req.ContentLength = post.Length;

                var reqStream = req.GetRequestStream();
                reqStream.Write(post, 0, post.Length);
                reqStream.Close();

                var res = (HttpWebResponse)req.GetResponse();

                Stream responseStream = res.GetResponseStream();
                var responseReader = new StreamReader(responseStream, Encoding.UTF8);
                string responseString = responseReader.ReadToEnd();

                var objResult = JsonConvert.DeserializeObject<Entities.ObjectEntity>(responseString);
               
                if (objResult != null && objResult.ObjectId != Guid.Empty)
                {
                    var detectorHelper = new ObjectDetectorHelper();
                    var result = detectorHelper.GetResult(bitmap, objResult);
                    imageBoxResult.Image = result;
                    imageBoxResult.BringToFront();
                }
                 
                //var detectorHelper = new ObjectDetectorHelper();
                //var result = detectorHelper.Surf(new Bitmap("C:\\vinamilk-fortified-02.jpg"),bitmap, false);
                //imageBoxResult.Image = result;
                //imageBoxResult.BringToFront();
            }
        }

        /// <summary>
        /// Handles the Click event of the btnStop control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 5/20/2013 - 5:08 PM
        private void btnStop_Click(object sender, EventArgs e)
        {
            axWMP.Ctlcontrols.stop();
        }

        /// <summary>
        /// Handles the Tick event of the timer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 5/20/2013 - 5:08 PM
        private void timer_Tick(object sender, EventArgs e)
        {
            tbarTime.Maximum = (int) axWMP.currentMedia.duration;
            tbarTime.Value = (int) axWMP.Ctlcontrols.currentPosition;
        }

        /// <summary>
        /// Handles the MouseDown event of the tbarTime control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.MouseEventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 5/20/2013 - 5:12 PM
        private void tbarTime_MouseDown(object sender, MouseEventArgs e)
        {
            timer.Enabled = false;
        }

        /// <summary>
        /// Handles the MouseUp event of the tbarTime control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.MouseEventArgs"/> instance containing the event data.</param>
        /// Author: Vu Dinh
        /// 5/20/2013 - 5:12 PM
        private void tbarTime_MouseUp(object sender, MouseEventArgs e)
        {
            axWMP.Ctlcontrols.currentPosition = tbarTime.Value;
            timer.Enabled = true;
        }
    }
}
