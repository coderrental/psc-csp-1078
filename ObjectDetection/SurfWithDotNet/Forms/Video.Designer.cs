﻿namespace SurfWithDotNet.Forms
{
    partial class Video
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnVideoPlayer = new System.Windows.Forms.Panel();
            this.btnLoadVideo = new System.Windows.Forms.Button();
            this.btnPlay = new System.Windows.Forms.Button();
            this.btnPause = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.tbarTime = new System.Windows.Forms.TrackBar();
            this.timer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.tbarTime)).BeginInit();
            this.SuspendLayout();
            // 
            // pnVideoPlayer
            // 
            this.pnVideoPlayer.Location = new System.Drawing.Point(12, 12);
            this.pnVideoPlayer.Name = "pnVideoPlayer";
            this.pnVideoPlayer.Size = new System.Drawing.Size(613, 546);
            this.pnVideoPlayer.TabIndex = 0;
            // 
            // btnLoadVideo
            // 
            this.btnLoadVideo.Location = new System.Drawing.Point(12, 616);
            this.btnLoadVideo.Name = "btnLoadVideo";
            this.btnLoadVideo.Size = new System.Drawing.Size(75, 23);
            this.btnLoadVideo.TabIndex = 1;
            this.btnLoadVideo.Text = "Load Video";
            this.btnLoadVideo.UseVisualStyleBackColor = true;
            this.btnLoadVideo.Click += new System.EventHandler(this.BtnLoadVideoClick);
            // 
            // btnPlay
            // 
            this.btnPlay.Location = new System.Drawing.Point(106, 616);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(75, 23);
            this.btnPlay.TabIndex = 2;
            this.btnPlay.Text = "Play";
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.BtnPlayClick);
            // 
            // btnPause
            // 
            this.btnPause.Location = new System.Drawing.Point(200, 615);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(75, 23);
            this.btnPause.TabIndex = 3;
            this.btnPause.Text = "Pause";
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.BtnPauseClick);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(294, 616);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 4;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.BtnStopClick);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // tbarTime
            // 
            this.tbarTime.Location = new System.Drawing.Point(13, 565);
            this.tbarTime.Name = "tbarTime";
            this.tbarTime.Size = new System.Drawing.Size(612, 45);
            this.tbarTime.TabIndex = 5;
            this.tbarTime.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tbarTime_MouseDown);
            this.tbarTime.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tbarTime_MouseUp);
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // Video
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 651);
            this.Controls.Add(this.tbarTime);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnPause);
            this.Controls.Add(this.btnPlay);
            this.Controls.Add(this.btnLoadVideo);
            this.Controls.Add(this.pnVideoPlayer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Video";
            this.Text = "Video Player - Object Detector";
            ((System.ComponentModel.ISupportInitialize)(this.tbarTime)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnVideoPlayer;
        private System.Windows.Forms.Button btnLoadVideo;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.TrackBar tbarTime;
        private System.Windows.Forms.Timer timer;
    }
}