﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Features2D;
using Emgu.CV.GPU;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;
using SurfWithDotNet.Entities;

namespace SurfWithDotNet.API
{
    class ObjectDetectorHelper
    {

        /// <summary>
        /// Detectors
        /// </summary>
        /// Author: Vu Dinh
        /// 5/27/2013 - 9:10 AM
        public enum Detector
        {
            Surf,
            TemplateMaching
        }

        /// <summary>
        /// Detects the object.
        /// </summary>
        /// <param name="originalImage">The original image.</param>
        /// <param name="observedImage">The observed image.</param>
        /// <param name="detector">The detector.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 5/27/2013 - 9:10 AM
        public Image<Bgr, Byte> DetectObject(Bitmap originalImage, Bitmap observedImage, Detector detector)
        {
            switch (detector)
            {
                 case Detector.Surf:
                    return Surf(originalImage, observedImage, false);
                 case Detector.TemplateMaching:
                    return TemplateMaching(originalImage, observedImage);
            }
            return null;
        }

        /// <summary>
        /// Gets the result.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="objectEntity">The object entity.</param>
        /// <returns>Image{BgrByte}.</returns>
        public Image<Bgr, Byte> GetResult(Bitmap image, ObjectEntity objectEntity)
        {
            var result = new Image<Bgr, byte>(image);
            //draw object
            result.DrawPolyline(objectEntity.Points, true, new Bgr(Color.Red), 5);

            //draw info
            //Create the font
            var f = new MCvFont(FONT.CV_FONT_HERSHEY_COMPLEX_SMALL, 1, 1);
            //TODO: need to draw info of all resellers. Currently display only info of 1 reseller
            result.Draw("Name: " + objectEntity.FieldValuePairs["Name"], ref f, new Point(30, 30), new Bgr(0, 255, 0));
            result.Draw("Price: " + objectEntity.FieldValuePairs["Price"] +" "+ objectEntity.FieldValuePairs["Currency"], ref f, new Point(30, 50), new Bgr(0, 255, 0));
            result.Draw("Reseller: " + objectEntity.ListResellers[0].Name, ref f, new Point(30, 70), new Bgr(0, 255, 0));
            result.Draw("Website: " +objectEntity.ListResellers[0].Website, ref f, new Point(30, 90), new Bgr(0, 255, 0));
            
            return result;
        }


        /// <summary>
        /// Detects the object.
        /// </summary>
        /// <param name="originalImage">The original image.</param>
        /// <param name="observedImage">The observed image.</param>
        /// <param name="showTooltip">if set to <c>true</c> [show tooltip].</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 5/20/2013 - 8:28 AM
        public Image<Bgr, Byte> Surf(Bitmap originalImage, Bitmap observedImage, bool showTooltip  )
        {

            var originalObject = (new Image<Gray, Byte>(originalImage));
            var observedObject = (new Image<Gray, Byte>(observedImage));
            HomographyMatrix homography = null;

            var surfCpu = new SURFDetector(500, false);

            //extract features from the object image
            VectorOfKeyPoint modelKeyPoints = surfCpu.DetectKeyPointsRaw(originalObject, null);
            Matrix<float> modelDescriptors = surfCpu.ComputeDescriptorsRaw(originalObject, null, modelKeyPoints);

            // extract features from the observed image
            VectorOfKeyPoint observedKeyPoints = surfCpu.DetectKeyPointsRaw(observedObject, null);
            Matrix<float> observedDescriptors = surfCpu.ComputeDescriptorsRaw(observedObject, null, observedKeyPoints);

            var matcher = new BruteForceMatcher(BruteForceMatcher.DistanceType.L2F32);
            matcher.Add(modelDescriptors);
            int k = 2;
            var indices = new Matrix<int>(observedDescriptors.Rows, k);
            var dist = new Matrix<float>(observedDescriptors.Rows, k);
            matcher.KnnMatch(observedDescriptors, indices, dist, k, null);

            var mask = new Matrix<byte>(dist.Rows, 1);

            mask.SetValue(255);

            Features2DTracker.VoteForUniqueness(dist, 0.8, mask);

            int nonZeroCount = CvInvoke.cvCountNonZero(mask);
            if (nonZeroCount >= 4)
            {
                nonZeroCount = Features2DTracker.VoteForSizeAndOrientation(modelKeyPoints, observedKeyPoints,
                                                                            indices, mask, 1.5, 20);
                if (nonZeroCount >= 4)
                    homography = Features2DTracker.GetHomographyMatrixFromMatchedFeatures(modelKeyPoints,
                                                                                            observedKeyPoints, indices,
                                                                                            mask, 3);
            }


            //Draw the matched keypoints
            //Image<Bgr, Byte> result = Features2DTracker.DrawMatches(originalObject, modelKeyPoints, observedObject,
            //                                                        observedKeyPoints,
            //                                                        indices, new Bgr(255, 255, 255),
            //                                                        new Bgr(255, 255, 255), mask,
            //                                                        Features2DTracker.KeypointDrawType.
            //                                                            NOT_DRAW_SINGLE_POINTS);
            var result = new Image<Bgr, Byte>(observedImage);
            #region draw the projected region on the image

            if (homography != null)
            {
                //draw a rectangle along the projected model
                Rectangle rect = originalObject.ROI;
                var pts = new[]
                                   {
                                       new PointF(rect.Left, rect.Bottom),
                                       new PointF(rect.Right, rect.Bottom),
                                       new PointF(rect.Right, rect.Top),
                                       new PointF(rect.Left, rect.Top)
                                   };
                homography.ProjectPoints(pts);
                var poins = Array.ConvertAll(pts, Point.Round);
                result.DrawPolyline(poins, true, new Bgr(Color.Red), 5);

                if (showTooltip)
                    AddTooltip(ref result, new Point(poins[0].X, poins[0].Y));
            }
            #endregion
            
            observedObject.Dispose();
            originalObject.Dispose();
            return result;
        }

        private Image<Bgr, Byte> TemplateMaching(Bitmap originalImage, Bitmap observedImage)
        {
            var templateImage = new Image<Gray, Byte>(originalImage);
            var imgSource = new Image<Gray, Byte>(observedImage);
            bool success = false;

            //Work out padding array size
            Point dftSize = new Point(imgSource.Width + (templateImage.Width * 2), imgSource.Height + (templateImage.Height * 2));
            //Pad the Array with zeros
            Image<Gray, Byte> pad_array = new Image<Gray, Byte>(dftSize.X, dftSize.Y);

            //copy centre
            pad_array.ROI = new Rectangle(templateImage.Width, templateImage.Height, imgSource.Width, imgSource.Height);
            CvInvoke.cvCopy(imgSource, pad_array, IntPtr.Zero);

            pad_array.ROI = (new Rectangle(0, 0, dftSize.X, dftSize.Y));

            //Match Template
            Image<Gray, float> result_Matrix = pad_array.MatchTemplate(templateImage, TM_TYPE.CV_TM_CCOEFF_NORMED);
          
            Point[] MAX_Loc, Min_Loc;
            double[] min, max;
            //Limit ROI to look for Match

            result_Matrix.ROI = new Rectangle(templateImage.Width, templateImage.Height, imgSource.Width - templateImage.Width, imgSource.Height - templateImage.Height);

            result_Matrix.MinMax(out min, out max, out Min_Loc, out MAX_Loc);

            success = true;
            var results = result_Matrix.Convert<Bgr, Byte>();

    

            return results.Convert<Bgr, Byte>();
        }

        /// <summary>
        /// Adds the tooltip.
        /// </summary>
        /// <param name="backgroundImage">The background image.</param>
        /// <param name="position">The position.</param>
        /// Author: Vu Dinh
        /// 5/21/2013 - 5:11 PM
        private void AddTooltip (ref Image<Bgr, Byte> backgroundImage, Point position )
        {
            var overlayImg = new Image<Bgr, Byte>("C:\\vinamilk.jpg");
            backgroundImage.ROI = new Rectangle(0,0, overlayImg.Width, overlayImg.Height);
            overlayImg.Copy(backgroundImage, null);
            backgroundImage.ROI = Rectangle.Empty;
        }
    }
}
