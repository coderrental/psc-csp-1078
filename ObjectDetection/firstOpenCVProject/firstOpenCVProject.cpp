//firstOpenCVProject.cpp

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <opencv/cvaux.h>
#include <opencv/highgui.h>
#include <opencv/cxcore.h>
#include <stdio.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#include <time.h>
#include <ctype.h>

using namespace std;
using namespace cv;
//////////////////////////////////////////////////////
void detectAndDisplay( Mat frame );
CascadeClassifier face_cascade;
cv::VideoWriter w;

int main(int argc, char* argv[])
{
	if( !face_cascade.load( "C:\\book.xml" ) ){ printf("--(!)Error loading\n");  };
	//if( !face_cascade.load( "C:\\haarcascade_frontalface_alt.xml" ) ){ printf("--(!)Error loading\n");  };
	// Default capture size - 640x480
	CvSize size = cvSize(640,480);
	// open webcam
	CvCapture* capture = cvCaptureFromCAM( 0 );

	if( !capture )
	{
		fprintf( stderr, "ERROR: capture is NULL \n" );
		getchar();
		return -1;
	}


	
	// Create a window in which the captured images will be presented
	cvNamedWindow( "Camera", CV_WINDOW_AUTOSIZE );
	cvNamedWindow("Proccess", CV_WINDOW_AUTOSIZE);
	IplImage* process_frame = cvCreateImage(size, IPL_DEPTH_8U, 1);
	IplImage* imgHSV = cvCreateImage(size, IPL_DEPTH_8U, 3);
	
	while( 1 )
	{
		
		// Get one frame
		IplImage* frame = cvQueryFrame( capture );
		if( !frame )
		{
			fprintf( stderr, "ERROR: frame is null...\n" );
			getchar();
			break;
		}

		//detectAndDisplay(frame);
		
		cvCvtColor(frame, imgHSV, CV_BGR2HSV);

		cvInRangeS(imgHSV, cvScalar(0, 156, 254), cvScalar(29, 256, 256), process_frame);
		CvMemStorage* storage = cvCreateMemStorage(0);


		cvSmooth( process_frame, process_frame, CV_GAUSSIAN, 9, 9 );
		CvSeq* circles = cvHoughCircles(process_frame, storage, CV_HOUGH_GRADIENT, 2,
		process_frame->height/4, 100, 50, 10, 400);


		for (int i = 0; i < circles->total; i++)
		{
			float* p = (float*)cvGetSeqElem( circles, i );
			printf("Ball! x=%f y=%f r=%f\n\r",p[0],p[1],p[2] );
			cvCircle( frame, cvPoint(cvRound(p[0]),cvRound(p[1])),
			3, CV_RGB(0,255,0), -1, 8, 0 );
			cvCircle( frame, cvPoint(cvRound(p[0]),cvRound(p[1])),
			cvRound(p[2]), CV_RGB(255,0,0), 3, 8, 0 );
		}

		cvShowImage( "Camera", frame ); 
		cvShowImage ("Proccess", process_frame);

		cvReleaseMemStorage(&storage);
		

		//If ESC key pressed, close window
		if( (cvWaitKey(10) & 255) == 27 ) break;
	}
	

	// show result
	cvReleaseCapture( &capture );
	cvDestroyWindow( "mywindow" );
	return 0;
}


void detectAndDisplay( Mat frame )
{
   std::vector<Rect> faces;
   Mat frame_gray;

   cvtColor( frame, frame_gray, CV_BGR2GRAY );
   equalizeHist( frame_gray, frame_gray );
   //-- Detect faces
   face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(24, 24) );

   for( int i = 0; i < faces.size(); i++ )
    {
      Point center( faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5 );
      ellipse( frame, center, Size( faces[i].width*0.5, faces[i].height*0.5), 0, 0, 360, Scalar( 0, 0, 0 ), 2, 8, 0 );

      Mat faceROI = frame_gray( faces[i] );
    }
   w.write(frame);
   //-- show result
   imshow( "test", frame_gray );
   imshow( "Camera", frame );
}
