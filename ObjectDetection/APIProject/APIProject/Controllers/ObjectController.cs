﻿// ***********************************************************************
// Assembly         : APIProject
// Author           : VU DINH
// Created          : 05-31-2013
//
// Last Modified By : VU DINH
// Last Modified On : 05-31-2013
// ***********************************************************************
// <copyright file="ObjectController.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using APIProject.Services;
using APIProject.ObjectDetection;

namespace APIProject.Controllers
{
    /// <summary>
    /// Class ObjectController
    /// </summary>
    public class ObjectController : ApiController
    {
        private ObjectRepository _objectRepository;
        /// <summary>
        /// Create new user.
        /// </summary>
        /// <returns>HttpResponseMessage.</returns>
        [HttpPost]
        public HttpResponseMessage PostImage()
        {
            var requestForm = HttpUtility.UrlDecode(Request.Content.ReadAsStringAsync().Result);
            if (requestForm != null)
            {
                requestForm = requestForm.Replace("imgData=data:image/png;base64,", string.Empty);
                var imgData = Convert.FromBase64String(requestForm);
                var ms = new MemoryStream(imgData);
                var img = Image.FromStream(ms);
                var bitmap = new Bitmap(img);
                _objectRepository = new ObjectRepository();
                var ob = _objectRepository.DetectObject(bitmap);

                var response = Request.CreateResponse(HttpStatusCode.OK, ob);
                return response;
            }

            return null;
        }
    }
}
