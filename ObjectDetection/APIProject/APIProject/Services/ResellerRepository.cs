﻿// ***********************************************************************
// Assembly         : APIProject
// Author           : VU DINH
// Created          : 06-25-2013
//
// Last Modified By : VU DINH
// Last Modified On : 06-25-2013
// ***********************************************************************
// <copyright file="Reseller.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using APIProject.Models;

namespace APIProject.Services
{
    /// <summary>
    /// Class Reseller
    /// </summary>
    public class ResellerRepository :BaseRepository
    {
        /// <summary>
        /// Gets the list reseller.
        /// </summary>
        /// <param name="objectId">The object id.</param>
        /// <returns>List{Models.Reseller}.</returns>
        public List<Models.Reseller> GetListReseller(Guid objectId)
        {
            var listReseller = new List<Models.Reseller>();
            var obj = ObjectDetectionData.Objects.SingleOrDefault(a => a.Object_Id == objectId);
            if (obj == null)
                return listReseller;
            
            foreach (var objReseller in ObjectDetectionData.Object_Resellers.Where(a => a.Object_Main_Id == obj.Object_Main_Id))
            {
                var resellerDb =ObjectDetectionData.Resellers.SingleOrDefault(a => a.Reseller_Id == objReseller.Reseller_Id);
                if (resellerDb != null)
                {
                    listReseller.Add(new Reseller
                        {
                            ResellerId = resellerDb.Reseller_Id,
                            Address = resellerDb.Address,
                            City = resellerDb.City,
                            Country = resellerDb.Country,
                            Name = resellerDb.Name,
                            State = resellerDb.State,
                            Website = resellerDb.Website,
                            Zipcode = resellerDb.Zipcode,
                            FieldValuePairs = GetFieldValues(objReseller.Reseller_Id)
                        });
                }
            }
            return listReseller;
        }


        /// <summary>
        /// Gets the field values.
        /// </summary>
        /// <param name="resellerId">The reseller id.</param>
        /// <returns>Hashtable.</returns>
        private Hashtable GetFieldValues(Guid resellerId)
        {
            var fieldValues = new Hashtable();
            foreach (var resellerFieldType in ObjectDetectionData.Reseller_Field_Types)
            {
                var fieldValue = ObjectDetectionData.Reseller_Fields.SingleOrDefault(a => a.Reseller_Id == resellerId && a.Reseller_Field_Type_Id == resellerFieldType.Reseller_Field_Type_Id);
                fieldValues.Add(resellerFieldType.Field_Name, fieldValue != null ? fieldValue.Value : null);
            }
            return fieldValues;
        }
    }
}