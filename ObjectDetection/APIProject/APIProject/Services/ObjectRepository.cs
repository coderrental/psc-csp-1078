﻿// ***********************************************************************
// Assembly         : APIProject
// Author           : VU DINH
// Created          : 05-31-2013
//
// Last Modified By : VU DINH
// Last Modified On : 05-31-2013
// ***********************************************************************
// <copyright file="ObjectRepository.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web;
using APIProject.Models;
using APIProject.ObjectDetection;
using APIProject.Data;


namespace APIProject.Services
{
    /// <summary>
    /// Class ObjectRepository
    /// </summary>
    public class ObjectRepository :BaseRepository
    {
        /// <summary>
        /// Detects the object.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <returns>Models.Object.</returns>
        public Models.Object DetectObject(Bitmap image)
        {
            var detector = new Helper();
            var objects = ObjectDetectionData.Objects.Where(a => a.Stage.Description == "Publish"); //TODO : need to get objects list by specific language
            foreach (var obj in objects)
            {
                var listImages = GetListImages(obj.Object_Id);
                if (listImages.Any())
                {
                    foreach (var img in listImages)
                    {
                        var bm = new Bitmap(img.FullName);
                        var result = detector.Detector(image, bm);
                        if (result != null)
                        {
                            var resellerRepository = new ResellerRepository();
                            var objResult = new Models.Object
                                {
                                    Points = result,
                                    ObjectId = obj.Object_Id,
                                    ListResellers = resellerRepository.GetListReseller(obj.Object_Id),
                                    FieldValuePairs = GetFieldValues(obj.Object_Id),
                                    IsDetected = true
                                };
                            return objResult;
                        }
                    }
                }
            }
            return new Models.Object();
        }

        /// <summary>
        /// Gets the list images.
        /// </summary>
        /// <param name="objectId">The object id.</param>
        /// <returns>IEnumerable{FileInfo}.</returns>
        private List<FileInfo> GetListImages(Guid objectId)
        {
            var imgPath = HttpContext.Current.Server.MapPath("~/Images/ObjectImages/" + objectId.ToString().ToUpper());
            var dir = new DirectoryInfo(imgPath);
            var listImages = new List<FileInfo>();
            listImages.AddRange(dir.GetFiles().Where(file => file.Extension.Contains("jpg")).ToList()); //TODO : need to get images with all images types. Currently get images with jpg extension
            return listImages;
        }

        /// <summary>
        /// Gets the field values.
        /// </summary>
        /// <param name="objectId">The object id.</param>
        /// <returns>Hashtable.</returns>
        private Hashtable GetFieldValues(Guid objectId)
        {
            var fieldValues = new Hashtable();
            foreach (var objectFieldType in ObjectDetectionData.Object_Field_Types)
            {
                var fieldValue = ObjectDetectionData.Object_Fields.SingleOrDefault(a => a.Object_Id == objectId && a.Object_Field_Type_Id == objectFieldType.Object_Field_Type_Id);
                fieldValues.Add(objectFieldType.Field_Name, fieldValue != null ? fieldValue.Value : null);
            }
            return fieldValues;
        }
    }
}