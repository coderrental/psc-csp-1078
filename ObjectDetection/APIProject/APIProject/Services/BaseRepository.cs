﻿// ***********************************************************************
// Assembly         : APIProject
// Author           : VU DINH
// Created          : 06-25-2013
//
// Last Modified By : VU DINH
// Last Modified On : 06-25-2013
// ***********************************************************************
// <copyright file="BaseRepository.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Configuration;
using APIProject.Data;

namespace APIProject.Services
{
    /// <summary>
    /// Class BaseRepository
    /// </summary>
    public class BaseRepository
    {
        /// <summary>
        /// The object detection data
        /// </summary>
        protected internal ObjectDetectionDataContext ObjectDetectionData;

        /// <summary>
        /// Inits the repository.
        /// </summary>
        /// <returns>BaseRepository.</returns>
        public BaseRepository ()
        {
            ObjectDetectionData = new ObjectDetectionDataContext(ConfigurationManager.ConnectionStrings["database"].ConnectionString);
        }
    }
}