﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using Emgu.CV;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using Emgu.CV.Util;

namespace APIProject.ObjectDetection
{
    public class Helper
    {
        public Point[] Detector(Bitmap observeredObject, Bitmap originalObject)
        {
            try
            {
                var obObject = new Image<Gray, Byte>(observeredObject);
                var orObject = new Image<Gray, Byte>(originalObject);
                HomographyMatrix homography = null;

                var surfCpu = new SURFDetector(1200, false);


                //extract features from the object image
                VectorOfKeyPoint modelKeyPoints = surfCpu.DetectKeyPointsRaw(orObject, null);
                Matrix<float> modelDescriptors = surfCpu.ComputeDescriptorsRaw(orObject, null, modelKeyPoints);

                // extract features from the observed image
                VectorOfKeyPoint observedKeyPoints = surfCpu.DetectKeyPointsRaw(obObject, null);
                Matrix<float> observedDescriptors = surfCpu.ComputeDescriptorsRaw(obObject, null, observedKeyPoints);

                var matcher = new BruteForceMatcher(BruteForceMatcher.DistanceType.L2F32);
                matcher.Add(modelDescriptors);
                int k = 2;
                var indices = new Matrix<int>(observedDescriptors.Rows, k);
                var dist = new Matrix<float>(observedDescriptors.Rows, k);
                matcher.KnnMatch(observedDescriptors, indices, dist, k, null);

                var mask = new Matrix<byte>(dist.Rows, 1);

                mask.SetValue(255);

                Features2DTracker.VoteForUniqueness(dist, 0.8, mask);

                int nonZeroCount = CvInvoke.cvCountNonZero(mask);
                if (nonZeroCount >= 15)
                {
                    nonZeroCount = Features2DTracker.VoteForSizeAndOrientation(modelKeyPoints, observedKeyPoints,
                                                                                indices, mask, 1.5, 20);
                    if (nonZeroCount >= 20)
                        homography = Features2DTracker.GetHomographyMatrixFromMatchedFeatures(modelKeyPoints,
                                                                                                observedKeyPoints, indices,
                                                                                                mask, 3);
                }

                //var result = new Image<Bgr, Byte>(observeredObject);
                #region draw the projected region on the image

                if (homography != null)
                {
                    //draw a rectangle along the projected model
                    Rectangle rect = orObject.ROI;
                    var pts = new[]
                                   {
                                       new PointF(rect.Left, rect.Bottom),
                                       new PointF(rect.Right, rect.Bottom),
                                       new PointF(rect.Right, rect.Top),
                                       new PointF(rect.Left, rect.Top)
                                   };
                    homography.ProjectPoints(pts);
                    var poins = Array.ConvertAll(pts, Point.Round);
                    return poins;
                    //result.DrawPolyline(poins, true, new Bgr(Color.Red), 5);
                }

                #endregion

                return null;
                //return result;
            }
            catch (Exception exception)
            {
                return null;
                throw;
            }
        }
    }
}