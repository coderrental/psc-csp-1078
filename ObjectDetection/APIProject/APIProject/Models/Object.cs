﻿// ***********************************************************************
// Assembly         : APIProject
// Author           : VU DINH
// Created          : 06-25-2013
//
// Last Modified By : VU DINH
// Last Modified On : 06-25-2013
// ***********************************************************************
// <copyright file="Object.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;

namespace APIProject.Models
{
    /// <summary>
    /// Class Object
    /// </summary>
    public class Object
    {
        /// <summary>
        /// Gets or sets the object id.
        /// </summary>
        /// <value>The object id.</value>
        public Guid ObjectId { get; set; }

        /// <summary>
        /// Gets or sets the point.
        /// </summary>
        /// <value>The point.</value>
        public Point[] Points { get; set; }

        /// <summary>
        /// Gets or sets the field value.
        /// </summary>
        /// <value>The field value.</value>
        public Hashtable FieldValuePairs { get; set; }

        /// <summary>
        /// Gets or sets the list reseller.
        /// </summary>
        /// <value>The list reseller.</value>
        public List<Reseller> ListResellers { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is detected.
        /// </summary>
        /// <value><c>true</c> if this instance is detected; otherwise, <c>false</c>.</value>
        public bool IsDetected { get; set; }
    }
}