﻿// ***********************************************************************
// Assembly         : APIProject
// Author           : VU DINH
// Created          : 06-25-2013
//
// Last Modified By : VU DINH
// Last Modified On : 06-25-2013
// ***********************************************************************
// <copyright file="Reseller.cs" company="CR TEAM">
//     Copyright (c) CR TEAM. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Collections;

namespace APIProject.Models
{
    /// <summary>
    /// Class Reseller
    /// </summary>
    public class Reseller
    {
        /// <summary>
        /// Gets or sets the reseller id.
        /// </summary>
        /// <value>The reseller id.</value>
        public Guid ResellerId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <value>The address.</value>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>The city.</value>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the zipcode.
        /// </summary>
        /// <value>The zipcode.</value>
        public string Zipcode { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>The state.</value>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>The country.</value>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the website.
        /// </summary>
        /// <value>The website.</value>
        public string Website { get; set; }

        /// <summary>
        /// Gets or sets the field value.
        /// </summary>
        /// <value>The field value.</value>
        public Hashtable FieldValuePairs { get; set; }
    }
}