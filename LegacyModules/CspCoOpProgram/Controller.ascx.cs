﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Security.Roles;
using DotNetNuke.Services.Localization;

namespace TIEKinetix.CSPModules.CspCoOpProgram
{
    public partial class Controller : PortalModuleBase
    {
        public const string CSP_INTEGRATION_KEY = "CspId";
        public const string CSP_Coop_Partner = "CSP_Coop_Partner";

        protected void Page_Init(object sender, EventArgs e)
        {
            ModuleContext.Configuration.ModuleTitle = Localization.GetString("ModuleTitle", LocalResourceFile);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected string HtmlBlock
        {
            get
            {
                return UserInfo.IsInRole(CSP_Coop_Partner) ? Localization.GetString("Coop_Html_Block", LocalResourceFile) : "";
            }
        }
    }
}