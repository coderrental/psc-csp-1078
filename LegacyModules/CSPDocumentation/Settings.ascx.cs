﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Exceptions;

namespace DesktopModules.CSPModules.CSPContentSetup.CSPDocumentation
{
    public partial class Settings : ModuleSettingsBase
    {
        private const string Showcase = "Showcase";
        private const string ExploreProduct = "ExploreProduct";

        #region Overrides of ModuleSettingsBase

        public override void LoadSettings()
        {
            try
            {

                cbShowcase.Checked = TabModuleSettings[Showcase] == null
                                         ? false
                                         : bool.Parse(TabModuleSettings[Showcase].ToString());
                cbExploreProduct.Checked = TabModuleSettings[ExploreProduct] == null
                                               ? false
                                               : bool.Parse(TabModuleSettings[ExploreProduct].ToString());
            }
            catch (Exception exc) { Exceptions.ProcessModuleLoadException(this, exc); }

        }

        public override void UpdateSettings()
        {
            try
            {
                ModuleController objModules = new ModuleController();
                objModules.UpdateTabModuleSetting(TabModuleId, Showcase, cbShowcase.Checked.ToString());
                objModules.UpdateTabModuleSetting(TabModuleId, ExploreProduct, cbExploreProduct.Checked.ToString());

                //refresh cache
                ModuleController.SynchronizeModule(this.ModuleId);
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
            
        }

        #endregion
    }
}

