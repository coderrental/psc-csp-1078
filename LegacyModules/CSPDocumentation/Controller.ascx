<%@ Control Language="C#" Inherits="DesktopModules.CSPModules.CSPContentSetup.CSPDocumentation.Controller"
    CodeFile="Controller.ascx.cs" AutoEventWireup="true" %>
<asp:Panel ID="PnlContainer" runat="server">
    <div id="csp_doc_container" style="float: left; width: 660px;">
        <table width="640" align="left">
            <tr>
                <td colspan="2" bgcolor="#EDE9EA" style="font: 18px/130% Arial, Helvetica, sans-serif;
                    padding: 5px;">
                    <b><asp:Literal ID="lbPickCodeTitle" runat="server" /></b>
                </td>
            </tr>
            <tr>
                <td width="35" align="center" style="padding-top: 6px;">
                    <img src="http://cc.syndicate.cnetcontent.com/Interface/images/stepOne.png" width="24"
                        height="25" />
                </td>
                <td style="font: 17px/130% Arial, Helvetica, sans-serif; padding-top: 4px;">
                    <asp:Literal ID="lbPickCodeStep1" runat="server" />                
                </td>
            </tr>
            <tr>
                <td align="center">
                    &nbsp;
                </td>
                <td valign="top" style="font: 11px/130% Arial, Helvetica, sans-serif;">
                    <asp:Literal ID="lbPickCodeStep1Message" runat="server" />                                
                </td>
            </tr>
            <tr>
                <td align="center" style="padding-top: 6px;">
                    <img src="http://cc.syndicate.cnetcontent.com/Interface/images/stepTwo.png" width="24"
                        height="25" />
                </td>
                <td style="font: 17px/130% Arial, Helvetica, sans-serif; padding-top: 4px;">
                    <asp:Literal ID="lbPickCodeStep2" runat="server" />                                
                </td>
            </tr>
            <tr>
                <td align="center">
                    &nbsp;
                </td>
                <td valign="top" style="font: 11px/130% Arial, Helvetica, sans-serif;">
                    <asp:Literal ID="lbPickCodeStep2Message" runat="server" />                
                </td>
            </tr>
            <tr>
                <asp:Panel runat="server" ID="pShowcase" Visible="false">
                    <td align="center">
                        &nbsp;
                    </td>
                    <td>
                        <div class="title">
                            Showcase</div>
                        <div class="codeBlock">
                            <p style="font: 12px/130% 'Courier New', Courier, monospace; color: #666666;">
                                &lt;!-- <%=GetPortalInstanceName %> Showcase Code --&gt;</p>
                            <p style="font: 12px/130% 'Courier New', Courier, monospace; color: #0000FF;">
                                <asp:Label runat="server" ID="ShowcaseScript" style="color: #0000FF;"></asp:Label></p>
                        </div>
                    </td>
                </asp:Panel>
            </tr>
            <asp:Panel runat="server" ID="pExploreProduct" Visible="false">
            <tr>
                
                    <td align="center">
                        &nbsp;
                    </td>
                    <td>
                        <div class="title">
                            Explore Product</div>
                        <div class="codeBlock">
                            <p style="font: 12px/130% 'Courier New', Courier, monospace; color: #666666;">
                                &lt;!-- Explore <%=GetPortalInstanceName %> Products Code --&gt;</p>
                            <p style="font: 12px/130% 'Courier New', Courier, monospace; color: #0000FF;">
                                <asp:Label runat="server" ID="ExploreProductScript" style="color: #0000FF;"></asp:Label></p>
                        </div>
                    </td>                
            </tr>
            <tr>
                <td>
                </td>
                <td style="font: 12px/130% 'Courier New', Courier, monospace;">
                    <b><asp:Literal runat="server" ID="lbExample"></asp:Literal></b><br />
                    <asp:Label runat="server" ID="Example"></asp:Label>
                </td>
            </tr>
            </asp:Panel>
            <tr>
                <td align="center" style="padding-top: 6px;">
                    <img src="http://cc.syndicate.cnetcontent.com/Interface/images/stepThree.png" width="24"
                        height="25" />
                </td>
                <td style="font: 18px/130% Arial, Helvetica, sans-serif; padding-top: 4px;">
                    <asp:Literal ID="lbPickCodeStep3" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center">
                    &nbsp;
                </td>
                <td valign="top" style="font: 11px/130% Arial, Helvetica, sans-serif;">
                    <asp:Literal ID="lbPickCodeStep3Message" runat="server" />                
                </td>
            </tr>
            <tr>
                <td align="center">
                </td>
                <td style="font: 0px/0%;">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <img src="http://cc.syndicate.cnetcontent.com/Interface/images/icon_info.png" width="16"
                        height="16" />
                </td>
                <td style="font: 11px/130% Arial, Helvetica, sans-serif; padding-top: 10px;">
                    <asp:Literal ID="lbPickCodeStep3Message1" runat="server" />
                </td>
            </tr>
        </table>
         <br /><br />
        <table width="640" align="left">
            <thead>
                <tr>
                    <td colspan="2" bgcolor="#EDE9EA" style="font: 18px/130% Arial, Helvetica, sans-serif;
                        padding: 5px;">
                        <b><asp:Literal runat="server" ID="lbDownloadTitle"></asp:Literal></b>
                    </td>
                </tr>
            </thead>
            <%
                if (!string.IsNullOrEmpty(ShowcaseDocument))
                {
                    %>
                        <tr>
                            <td>
                            </td>
                            <td> 
                                <a href="<%=ShowcaseDocument %>"><%=Localization.GetString("ShowcaseDocumentText", _tmpResourcePath)%></a>
                            </td>
                        </tr>
                    <%
                }
                if (!string.IsNullOrEmpty(ExploreProductDocument))
                {
                    %>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <a href="<%=ExploreProductDocument %>"><%=Localization.GetString("ExploreProductDocumentText", _tmpResourcePath)%></a>
                            </td>
                        </tr>
                    <%
                }
             %>
             
             
        </table>

        <script type="text/javascript">
            // indicator

            $(document).ready(function() {
                $("#dnn_dnnPRIVACY_hypPrivacy").colorbox({ width: "1100px", height: "700px", iframe: true });
                $("#dnn_dnnTERMS_hypTerms").colorbox({ width: "1100px", height: "700px", iframe: true });

            });
</script>

    </div>
</asp:Panel>
<div class="clearfix">
</div>
<asp:Panel ID="PnlMessageContainer" runat="server">
</asp:Panel>
