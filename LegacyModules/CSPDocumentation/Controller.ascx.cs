/*
' DotNetNuke� - http://www.dotnetnuke.com
' Copyright (c) 2002-2006
' by Perpetual Motion Interactive Systems Inc. ( http://www.perpetualmotion.ca )
'
' Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
' documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
' the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
' to permit persons to whom the Software is furnished to do so, subject to the following conditions:
'
' The above copyright notice and this permission notice shall be included in all copies or substantial portions 
' of the Software.
'
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
' DEALINGS IN THE SOFTWARE.
 */

using System;
using System.Linq;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using DesktopModules.CSPModules.CspCommons;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Users;
using DotNetNuke.Services.Localization;

namespace DesktopModules.CSPModules.CSPContentSetup.CSPDocumentation
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The ViewCSPSupports class displays the content
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <history>
    /// </history>
    /// -----------------------------------------------------------------------------
    partial class Controller : PortalModuleBase
    {
        private string _consumerDomain;
        private UserInfo _curUser;
        private int _integrationKey;
        private bool _isConfigured = true;
        protected string _tmpResourcePath;
        protected bool ShowLanguage;

        private const string Showcase = "Showcase";
        private const string ExploreProduct = "ExploreProduct";


        protected string ShowcaseDocument
        {
            get
            {
                string lngCode = Thread.CurrentThread.CurrentCulture.ToString();
                string key = "Portal" + this.PortalId + "ShowcaseDocument-" + lngCode;
                if (!string.IsNullOrEmpty(Config.GetSetting(key))) return Config.GetSetting(key);
                else return Config.GetSetting("Portal" + this.PortalId + "ShowcaseDocument");
            }
        }

        protected string ExploreProductDocument
        {
            get
            {
                string lngCode = Thread.CurrentThread.CurrentCulture.ToString();
                string key = "Portal" + this.PortalId + "EPDocument-" + lngCode;
                if (!string.IsNullOrEmpty(Config.GetSetting(key))) return Config.GetSetting(key);
                return Config.GetSetting("Portal" + this.PortalId + "EPDocument");
            }
        }

        protected string GetPortalInstanceName
        {
            get
            {
                string s = Config.GetSetting("Portal" + this.PortalId + "InstanceProjectName");
                if (string.IsNullOrEmpty(s)) s = Config.GetSetting("Portal" + this.PortalId + "InstanceName");
                if (string.IsNullOrEmpty(s)) s = string.Empty;
                return s;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            string html = "";
            _curUser = UserController.GetCurrentUserInfo();
            string key = Globals.IntegrationKey;

            _tmpResourcePath = LocalResourceFile.Substring(0, LocalResourceFile.LastIndexOf("/")) +
                         "/CSPContentSetup";


            if (!int.TryParse(_curUser.Profile.GetPropertyValue(key), out _integrationKey))
            {
                _integrationKey = -1;
            }

            if (_integrationKey == -1)
            {
                //html += "You haven't completed filling out the mandatory information.<br/>";
                //html += "Please fill out the information in the Company Information.<br/>";
                html += Localization.GetString("MissingMandatoryInfo", _tmpResourcePath);
                if (_isConfigured)
                    _isConfigured = false;
            }
            if (_curUser.IsInRole("Administrators"))
            {
                html += string.Format("Moduule isn't available for administrative accounts.<br/>");
                if (_isConfigured)
                    _isConfigured = false;
            }
            else if (_curUser.Profile.ProfileProperties[Globals.SyndicationImageOption] == null ||
                     _curUser.Profile.ProfileProperties[Globals.SyndicationLinkText] == null ||
                     _curUser.Profile.ProfileProperties[Globals.SyndicationLinkImage] == null ||
                     _curUser.Profile.ProfileProperties[Globals.SyndicationCustomImage] == null ||
                     _curUser.Profile.ProfileProperties[Globals.EntryPoint] == null ||
                     _curUser.Profile.ProfileProperties[Globals.LaunchType] == null)
            {
                //html += "Missing mandatory setup for this module. Please contact admin<br/>";
                html += Localization.GetString("MissingMandatorySetup", _tmpResourcePath);
                if (_isConfigured) _isConfigured = false;
            }

            if (!_isConfigured)
            {
                Globals.ShowErrorMessage(PnlMessageContainer, html);
                PnlContainer.Visible = false;
            }
            else
            {
                //Show Language In Script
                ShowLanguage = true;
                if (!string.IsNullOrEmpty(Config.GetSetting("Portal" + PortalId + "ShowLanguageInScript")))
                    bool.TryParse(Config.GetSetting("Portal" + PortalId + "ShowLanguageInScript"), out ShowLanguage);

                using (
                    CspDataLayerDataContext _context =
                        new CspDataLayerDataContext(Config.GetConnectionString("CspPortal" + PortalId.ToString())))
                //EGO Multiple Portals
                {
                    var consumer =
                        (from c in _context.CompaniesConsumers where c.companies_Id == _integrationKey select c).
                            FirstOrDefault();
                    if (consumer == null)
                    {
                        //throw new DotNetNuke.Services.Exceptions.BasePortalException("Unable to find consumer info");
                        if (_isConfigured) _isConfigured = false;
                        Globals.ShowErrorMessage(PnlMessageContainer, "Unable to find consumer info");
                        PnlContainer.Visible = false;
                    }
                    else
                    {
                        _consumerDomain = consumer.base_domain;
                    }
                }
                UpdateModuleTitle("ModuleName", _tmpResourcePath);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Control docType = Page.FindControl("skinDocType");
            if (docType != null)
            {
                ((Literal)docType).Text =
                    "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";
            }

            if (!Page.IsPostBack && _consumerDomain != null)
            {
                // get current user
                _curUser = UserController.GetCurrentUserInfo();
                // register client side javascript libs
                Page.ClientScript.RegisterClientScriptInclude("jquery",
                                                              "http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js");
                //Page.ClientScript.RegisterClientScriptInclude("jquery-colorbox",
                //                                              ResolveUrl("~/js/external/jquery.colorbox-min.js"));
                //Globals.AddCssReference(this, "colorbox.css");

                if (_consumerDomain.Contains("."))
                {
                    _consumerDomain = _consumerDomain.Substring(0, _consumerDomain.IndexOf(".")); // strip to just ID
                }

                if (!string.IsNullOrEmpty(Config.GetSetting("Portal" + PortalId + "CentralRedirectEngineDomain")))
                    _consumerDomain = _consumerDomain + "." + Config.GetSetting("Portal" + PortalId + "CentralRedirectEngineDomain");
                else if (!string.IsNullOrEmpty(Config.GetSetting("CentralRedirectEngineDomain")))
                    _consumerDomain = _consumerDomain + "." + Config.GetSetting("CentralRedirectEngineDomain");

                //_consumerDomain = _consumerDomain + "." +
                //                  Config.GetSetting(
                //                      "CentralRedirectEngineDomain");

                string syndicationLanguage = string.Empty;
                if (ShowLanguage)
                    syndicationLanguage = "&lng=" + _curUser.Profile.GetPropertyValue(Globals.SyndicationLanguageId);

                ShowcaseScript.Text =
                    Server.HtmlEncode(
                        string.Format(
                            "<script class='CspSyndicationScript' type='text/javascript' src='{0}://{1}/{2}Csp/?mfrname={3}{4}'></script>",
                            Request.Url.Scheme, _consumerDomain, Globals.DefaultScriptPublicationPath,
                            Config.GetSetting("Portal" + PortalId + "InstanceName"),
                            syndicationLanguage));

                string isProxy = Config.GetSetting("Portal" + PortalId + "IsProxy");
                if (!string.IsNullOrEmpty(isProxy) && isProxy.Equals("true", StringComparison.CurrentCultureIgnoreCase))
                {
                    ShowcaseScript.Text += "<br />";//Server.HtmlEncode("<br/>");
                    ShowcaseScript.Text += Server.HtmlEncode("<div class='showcase'></div>");
                }
                string SEOUrl = Config.GetSetting("Portal" + PortalId + "UrlForSEOLink");
                string SEOLinkStyle = Config.GetSetting("Portal" + PortalId + "SEOLinkStyle");
                if (!string.IsNullOrEmpty(SEOUrl))
                {
                    ShowcaseScript.Text += "<br />";
                    ShowcaseScript.Text += Server.HtmlEncode("<a href='" + SEOUrl + "' class='SEOLink'");
                    if (!string.IsNullOrEmpty(SEOLinkStyle)) ShowcaseScript.Text += Server.HtmlEncode(" style='" + SEOLinkStyle + "'");
                    ShowcaseScript.Text += Server.HtmlEncode(">" + Config.GetSetting("Portal" + PortalId + "SEOName") + "</a>");
                }

                ExploreProductScript.Text =
                    string.Format(
                        "&lt;script class=&#39;CspSyndicationScript&#39; type=&#39;text/javascript&#39; src=&#39;{0}://{1}/{2}Csp/?mfrname={3}Manufacturer Name{4}&amp;mfrsku={3}Manufacturer SKU{4}&amp;t=exploreproduct{5}&#39;&gt;&lt;/script&gt;",
                        Request.Url.Scheme, _consumerDomain, Globals.DefaultScriptPublicationPath,
                        "<span style='background-color:#DDEAF7;font-weight:bold;'>",
                        "</span>",
                        Server.HtmlEncode(syndicationLanguage));
                //<script class='CspSyndicationScript' type='text/javascript' src='http://ma-ltu-devw7.cc.syndicate.cnetcontent.com/Csp/?mfrname=Manufacturer Name&mfrsku=Manufacturer SKU&t=exploreproduct'></script>
                //ExploreProductScript.Text = string.Format("<script class='CspSyndicationScript' type='text/javascript' src='{0}://{1}/{2}Csp/?mfrname={3}Manufacturer Name{4}&mfrsku={3}Manufacturer SKU{4}&t=exploreproduct{5}'></script>",
                //    Request.Url.Scheme, 
                //    _consumerDomain, 
                //    Globals.DefaultScriptPublicationPath,
                //    "<span style='background-color:#DDEAF7;font-weight:bold;'>",
                //    "</span>",
                //    syndicationLanguage);
                //ExploreProductScript.Text = Server.HtmlEncode(ExploreProductScript.Text);

                string mfpn = string.Empty;
                switch (_curUser.Profile.GetPropertyValue(Globals.SyndicationLanguageId))
                {
                    case "en-US":
                        mfpn = Config.GetSetting(string.Format("Portal{0}{1}MFPN", PortalId, "en-US"));
                        break;
                    case "US":
                        mfpn = Config.GetSetting(string.Format("Portal{0}{1}MFPN", PortalId, "en-US"));
                        break;
                    case "en-UK":
                        mfpn = Config.GetSetting(string.Format("Portal{0}{1}MFPN", PortalId, "en-UK"));
                        break;
                    case "UK":
                        mfpn = Config.GetSetting(string.Format("Portal{0}{1}MFPN", PortalId, "en-UK"));
                        break;
                    case "de-DE":
                        mfpn = Config.GetSetting(string.Format("Portal{0}{1}MFPN", PortalId, "de-DE"));
                        break;
                    case "DE":
                        mfpn = Config.GetSetting(string.Format("Portal{0}{1}MFPN", PortalId, "de-DE"));
                        break;
                    case "fr-FR":
                        mfpn = Config.GetSetting(string.Format("Portal{0}{1}MFPN", PortalId, "fr-FR"));
                        break;
                    case "FR":
                        mfpn = Config.GetSetting(string.Format("Portal{0}{1}MFPN", PortalId, "fr-FR"));
                        break;


                    default:
                        //mfpn = Config.GetSetting(string.Format("Portal{0}{1}MFPN", PortalId, "en-US"));
                        mfpn = Config.GetSetting(string.Format("Portal{0}{1}MFPN", PortalId, _curUser.Profile.GetPropertyValue(Globals.SyndicationLanguageId)));
                        if (string.IsNullOrEmpty(mfpn))
                            mfpn = Config.GetSetting(string.Format("Portal{0}{1}MFPN", PortalId, "en-US"));
                        break;

                }

                Example.Text =
                    string.Format(
                        "&lt;script class=&#39;CspSyndicationScript&#39; type=&#39;text/javascript&#39; src=&#39;{0}://{1}/{2}Csp/?mfrname={3}" +
                        Config.GetSetting("Portal" + PortalId + "InstanceName") + "{4}&amp;mfrsku={3}" +
                    //Config.GetSetting("Portal" + PortalId + "SampleMFPN") +
                        mfpn +
                        "{4}{5}&amp;t=exploreproduct&#39;&gt;&lt;/script&gt;",
                        Request.Url.Scheme, _consumerDomain, Globals.DefaultScriptPublicationPath,
                        "<span style='background-color:#DDEAF7;font-weight:bold;'>", "</span>",
                        Server.HtmlEncode(syndicationLanguage));



                // localization
                //string tmpResourcePath = "~/DesktopModules/CSPModules/CSPContentSetup/App_LocalResources/CSPContentSetup";
                if (string.IsNullOrEmpty(_tmpResourcePath))
                    _tmpResourcePath = LocalResourceFile.Substring(0, LocalResourceFile.LastIndexOf("/")) +
                                         "/CSPContentSetup";
                if (!string.IsNullOrEmpty(_tmpResourcePath))
                {
                    lbPickCodeTitle.Text = Localization.GetString(lbPickCodeTitle.ID, _tmpResourcePath);
                    lbPickCodeStep1.Text = Localization.GetString(lbPickCodeStep1.ID, _tmpResourcePath);
                    lbPickCodeStep2.Text = Localization.GetString(lbPickCodeStep2.ID, _tmpResourcePath);
                    lbPickCodeStep3.Text = Localization.GetString(lbPickCodeStep3.ID, _tmpResourcePath);
                    lbPickCodeStep1Message.Text = Localization.GetString(lbPickCodeStep1Message.ID, _tmpResourcePath);
                    lbPickCodeStep2Message.Text = Localization.GetString(lbPickCodeStep2Message.ID, _tmpResourcePath);
                    lbPickCodeStep3Message.Text = Localization.GetString(lbPickCodeStep3Message.ID, _tmpResourcePath,
                                                                         Request.QueryString["l"]);
                    lbPickCodeStep3Message1.Text = Localization.GetString(lbPickCodeStep3Message1.ID, _tmpResourcePath,
                                                                          Request.QueryString["l"]);
                    lbExample.Text = Localization.GetString(lbExample.ID, _tmpResourcePath);
                    lbDownloadTitle.Text = Localization.GetString(lbDownloadTitle.ID, _tmpResourcePath);
                }

                bool ShowShowcase = GetBoolSetting(Showcase);
                bool ShowExploreProduct = GetBoolSetting(ExploreProduct);

                if (ShowShowcase) pShowcase.Visible = true;
                if (ShowExploreProduct) pExploreProduct.Visible = true;


            }
        }

        private bool GetBoolSetting(string name)
        {
            if (Settings[name] == null) return true;
            bool r = false;
            if (bool.TryParse(Settings[name].ToString(), out r)) return r;
            else return false;
        }

        private void UpdateModuleTitle(string title, string resourcePath)
        {
            string controlTitle = Localization.GetString(title, resourcePath);
            if (!string.IsNullOrEmpty(controlTitle))
                ModuleContext.Configuration.ModuleTitle = controlTitle;
        }

    }
}
