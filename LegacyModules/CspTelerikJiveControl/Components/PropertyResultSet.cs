﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TIEKinetix.CspModules.CspTelerikJiveControl.Components
{
    public class PropertyResultSet
    {
        private Guid _projectId, _subscriberId, _propertyGroupId, _propertyId;
        private Guid? _languageId, _parentPropertyGroupId;
        private string _propertyName, _propertyType, _propertyValue, _propertyDefaultValue, _propertyValidationRule, _propertyGroupName;
        bool _required, _visible;

        public PropertyResultSet()
        {
        }

        public PropertyResultSet(Guid projectId, Guid subscriberId, Guid propertyGroupId, Guid propertyId, Guid? languageId, Guid? parentPropertyGroupId, string propertyName, string propertyType, string propertyValue, string propertyDefaultValue, string propertyValidationRule, bool required, bool visible, string propertyGroupName)
        {
            _projectId = projectId;
            _propertyGroupName = propertyGroupName;
            _subscriberId = subscriberId;
            _propertyGroupId = propertyGroupId;
            _propertyId = propertyId;
            _languageId = languageId;
            _parentPropertyGroupId = parentPropertyGroupId;
            _propertyName = propertyName;
            _propertyType = propertyType;
            _propertyValue = propertyValue;
            _propertyDefaultValue = propertyDefaultValue;
            _propertyValidationRule = propertyValidationRule;
            _required = required;
            _visible = visible;
        }

        public string PropertyGroupName
        {
            get { return _propertyGroupName; }
            set { _propertyGroupName = value; }
        }

        public Guid? ParentPropertyGroupId
        {
            get { return _parentPropertyGroupId; }
            set { _parentPropertyGroupId = value; }
        }

        public string PropertyValidationRule
        {
            get { return _propertyValidationRule; }
            set { _propertyValidationRule = value; }
        }

        public Guid ProjectId
        {
            get { return _projectId; }
            set { _projectId = value; }
        }

        public Guid SubscriberId
        {
            get { return _subscriberId; }
            set { _subscriberId = value; }
        }

        public Guid PropertyGroupId
        {
            get { return _propertyGroupId; }
            set { _propertyGroupId = value; }
        }

        public Guid PropertyId
        {
            get { return _propertyId; }
            set { _propertyId = value; }
        }

        public Guid? LanguageId
        {
            get { return _languageId; }
            set { _languageId = value; }
        }

        public string PropertyName
        {
            get { return _propertyName; }
            set { _propertyName = value; }
        }

        public string PropertyType
        {
            get { return _propertyType; }
            set { _propertyType = value; }
        }

        public string PropertyValue
        {
            get { return _propertyValue; }
            set { _propertyValue = value; }
        }

        public string PropertyDefaultValue
        {
            get { return _propertyDefaultValue; }
            set { _propertyDefaultValue = value; }
        }

        public bool Required
        {
            get { return _required; }
            set { _required = value; }
        }

        public bool Visible
        {
            get { return _visible; }
            set { _visible = value; }
        }
    }
}
