namespace TIEKinetix.CspModules.CspTelerikJiveControl.Components
{
    internal class NameValuePair
    {
        public string Name, Value, Desc, DefaultValue, PropertyType;
        public object NameId, ValueId, LanguageId, LinkId, Id, GroupId, CodeListId, ValidationId, IsMandatory;
        public bool? Visible;
    }
}