using System.Collections.Generic;
using TIEKinetix.Data;

namespace TIEKinetix.CspModules.CspTelerikJiveControl.Components
{
    public class NestedGroup {
        private property_group _group;
        private property_group _parentGroup;
        private List<NestedGroup> _children;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public NestedGroup(property_group grp, property_group parentGroup, List<NestedGroup> children, int level)
        {
            _group = grp;
            _children = children;
            _parentGroup = parentGroup;
            Level = level;
            TranslatedName = TranslatedDesc = string.Empty;
        }

        public List<NestedGroup> Children
        {
            get { return _children; }
            set { _children = value; }
        }

        public property_group Group
        {
            get { return _group; }
            set { _group = value; }
        }

        public property_group ParentGroup
        {
            get { return _parentGroup; }
            set { _parentGroup = value; }
        }

        public int Level { get; set; }
        public string TranslatedName { get; set; }
        public string TranslatedDesc { get; set; }
    }
}