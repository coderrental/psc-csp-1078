﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TIEKinetix.CspModules.CspTelerikJiveControl.Components
{
    public class Common
    {
        public static string CountProjectPropertyValueSql = @"
select count(0)
from property_value_dictionary pvd
	join project_subscriber_property_group_link pspgl on 
		pspgl.project_subscriber_property_group_link_id = pvd.link_id
	join project_subscriber_link psl on 
		psl.project_subscriber_link_id = pspgl.project_subscriber_link_id		
	join project_property_group_link ppgl on 
		ppgl.property_group_id = pvd.property_group_id		
	join property_group_property_link pgpl on 
		pgpl.property_group_id = pvd.property_group_id
		and pgpl.property_id = pvd.property_id
	join system_project_link spl on spl.project_id=psl.project_id
where 
	spl.system_id = {0}
	and ppgl.project_id = {1}
	and pvd.property_group_id = {2}
	and pvd.property_id = {3}
    and pvd.value = {4}
";
        public static string CountSystemPropertyValueSql = @"
select COUNT(0)
from system_property_value_dictionary spvd
	join system_project_subscriber_property_group_link spspgl on spspgl.system_project_subscriber_property_group_link_id=spvd.system_project_subscriber_property_group_link_id
	join system_project_link spl on spl.system_id=spspgl.system_id	
where 
	spl.system_id = {0}
	and spl.project_id = {1}
	and spvd.property_group_id = {2}
	and spvd.property_id = {3}
    and spvd.value = {4}
";
        public const string Mambo5IntegrationGroupId = "Mambo5IntegrationPropertyGroupId";
        public const string HeaderField = "header";
        public const string ProjectId = "JiveControlProjectId";
        public const string SystemId = "Mambo5SystemId";


        public const string ShopExistsSql = @"
select count(a.value)
from system_property_value_dictionary a
	join system_project_subscriber_property_group_link b on 
		b.system_project_subscriber_property_group_link_id=a.system_project_subscriber_property_group_link_id and
		b.property_group_id=a.property_group_id		
    join subscriber s on s.subscriber_id = b.subscriber_id
	join property c on c.property_id=a.property_id
	join system_project_link d on d.system_project_link_id=b.system_project_link_id and d.system_id=b.system_id	
	join system_property_group_link g on g.property_group_id=b.property_group_id and g.system_id=b.system_id and g.is_system_only_property_group=0
where	b.system_id={0} and 
		d.project_id={1} and
		b.subscriber_id={2} and
		g.property_group_id={3} and 
		c.property_name='company_id' and
        s.default_language_id = a.language_id
";


        public const string SetPropertyValueSql = @"
            -- make sure we create the link (should be needed but just in case)
            insert into project_subscriber_property_group_link(project_property_group_link_id,project_subscriber_link_id,property_group_id,date_created,date_changed)
            select b.project_property_group_link_id, a.project_subscriber_link_id, b.property_group_id,GETDATE(),GETDATE()
            from project_subscriber_link a
	            join project_property_group_link b on a.project_id=b.project_id	
            where a.project_id={0} and a.subscriber_id={1} and b.property_group_id={2} and not exists (
	            select 1 from project_subscriber_property_group_link pspgl 
	            where pspgl.project_property_group_link_id=b.project_property_group_link_id
		            and pspgl.project_subscriber_link_id=a.project_subscriber_link_id
		            and pspgl.property_group_id=b.property_group_id)

if exists (
select pvd.*
from project_subscriber_property_group_link pspgl
	join project_subscriber_link psl on psl.project_subscriber_link_id = pspgl.project_subscriber_link_id
	join subscriber s on s.subscriber_id = psl.subscriber_id
	join project_property_group_link ppgl on ppgl.project_property_group_link_id = pspgl.project_property_group_link_id
	join property_group_property_link pgpl on pgpl.property_group_id = ppgl.property_group_id
	join property p on p.property_id=pgpl.property_id
	join property_value_dictionary pvd on 
		pvd.link_id = pspgl.project_subscriber_property_group_link_id
		and pvd.property_group_id = pspgl.property_group_id
		and pvd.property_id = pgpl.property_id
		and pvd.language_id = s.default_language_id
where psl.project_id = {0}
	and psl.subscriber_id = {1}
	and pspgl.property_group_id = {2}
	and p.property_name={3})
	--update current value	
	update pvd
	set pvd.value = {4}, pvd.date_changed = GETDATE()
	from project_subscriber_property_group_link pspgl
		join project_subscriber_link psl on psl.project_subscriber_link_id = pspgl.project_subscriber_link_id
		join subscriber s on s.subscriber_id = psl.subscriber_id
		join project_property_group_link ppgl on ppgl.project_property_group_link_id = pspgl.project_property_group_link_id
		join property_group_property_link pgpl on pgpl.property_group_id = ppgl.property_group_id
		join property p on p.property_id=pgpl.property_id
		join property_value_dictionary pvd on 
			pvd.link_id = pspgl.project_subscriber_property_group_link_id
			and pvd.property_group_id = pspgl.property_group_id
			and pvd.property_id = pgpl.property_id
			and pvd.language_id = s.default_language_id
	where psl.project_id = {0}
		and psl.subscriber_id = {1}
		and pspgl.property_group_id = {2}
		and p.property_name={3}	
else
	insert into property_value_dictionary(link_id,property_group_id,property_id,language_id,value,date_created,date_changed)
	select pspgl.project_subscriber_property_group_link_id, pgpl.property_group_id, p.property_id, s.default_language_id, {4}, GETDATE(),GETDATE()
	from project_subscriber_property_group_link pspgl
		join project_subscriber_link psl on psl.project_subscriber_link_id = pspgl.project_subscriber_link_id
		join subscriber s on s.subscriber_id = psl.subscriber_id
		join project_property_group_link ppgl on ppgl.project_property_group_link_id = pspgl.project_property_group_link_id
		join property_group_property_link pgpl on pgpl.property_group_id = ppgl.property_group_id
		join property p on p.property_id=pgpl.property_id
	where psl.project_id = {0}
		and psl.subscriber_id = {1}
		and pspgl.property_group_id = {2}
		and p.property_name={3}	
		and not exists (		
			select 1
			from property_value_dictionary pvd
			where pvd.link_id = pspgl.project_subscriber_property_group_link_id 
				and pgpl.property_group_id = pvd.property_group_id
				and p.property_id = pvd.property_id
				and s.default_language_id= pvd.language_id
				and pvd.value = {4})
        ";

        public const string GetAndCreateLinkSql = @"
            insert into project_subscriber_property_group_link(project_property_group_link_id,project_subscriber_link_id,property_group_id,date_created,date_changed)
            select b.project_property_group_link_id, a.project_subscriber_link_id, b.property_group_id,GETDATE(),GETDATE()
            from project_subscriber_link a
	            join project_property_group_link b on a.project_id=b.project_id	
            where a.project_id={0} and a.subscriber_id={1} and b.property_group_id={2} and not exists (
	            select 1 from project_subscriber_property_group_link pspgl 
	            where pspgl.project_property_group_link_id=b.project_property_group_link_id
		            and pspgl.project_subscriber_link_id=a.project_subscriber_link_id
		            and pspgl.property_group_id=b.property_group_id)

            select a.project_subscriber_property_group_link_id 
            from project_subscriber_property_group_link a
	            join project_property_group_link b on a.project_property_group_link_id=b.project_property_group_link_id
	            join project_subscriber_link c on c.project_subscriber_link_id=a.project_subscriber_link_id
            where  b.project_id={0} and c.subscriber_id={1} and a.property_group_id={2}
            ";
        public const string GetProperties = @"
        select e.link_id as LinkId, p.property_id as NameId, e.value as Value, e.id as ValueId,
            case when tt.value is null or tt.value = '' then p.property_name
                else tt.value
            end as Name, 
            e.language_id as LanguageId, p.property_description as [Desc], a.subscriber_id as [Id], pg.property_group_id as [GroupId],
            p.property_type as [PropertyType],p.code_list_id as [CodeListId], p.validation_id as [ValidationId], pgpl.visible [Visible],
            case when pgpl.default_value is null then p.default_value
                else pgpl.default_value
            end as  [DefaultValue],
            case when pgpl.required is null then p.required
                else pgpl.required
            end as  [IsMandatory]
        from subscriber a
            join project_subscriber_link b on a.subscriber_id=b.subscriber_id
            join project_property_group_link c on c.project_id=b.project_id
            join property_group_property_link pgpl on pgpl.property_group_id=c.property_group_id
            join property p on p.property_id=pgpl.property_id
            join property_group pg on pg.property_group_id=pgpl.property_group_id
            left join project_subscriber_property_group_link d on 
                d.project_subscriber_link_id=b.project_subscriber_link_id 
                and d.project_property_group_link_id=c.project_property_group_link_id
            left join property_value_dictionary e on 
                e.link_id=d.project_subscriber_property_group_link_id 
                and e.property_group_id=c.property_group_id
                and e.language_id=a.default_language_id
                and e.property_id=pgpl.property_id
            left join translated_text tt on tt.translated_item_id=p.property_id
                and a.default_language_id=tt.language_id
            left join translated_text tt1 on tt1.translated_item_id=pg.property_group_id
                and a.default_language_id=tt1.language_id
        where b.project_id={0}
        	and a.subscriber_id={1}
            and pg.property_group_id={2}
        order by pgpl.display_order, p.display_order
";

        public const string GetPropertiesForValidation = @"
select psl.project_id as [ProjectId], psl.subscriber_id as [SubscriberId], pgpl.property_group_id as [PropertyGroupId], pg.parent_property_group_id as [ParentPropertyGroupId],
	pg.property_group_name as [PropertyGroupName], pgpl.property_id as [PropertyId], pvd.language_id as [LanguageId],p.property_name as [PropertyName], p.property_type as [PropertyType], 
    pvd.value as [PropertyValue], v.validation_regexp as [PropertyValidationRule],
    case when pgpl.default_value is null or pgpl.default_value = '' then p.default_value
        else pgpl.default_value
    end as [PropertyDefaultValue],	
    case when pgpl.required is null then 'false'
		else pgpl.required
    end as [Required],
    case when  pgpl.visible is null then 'true'
		else pgpl.visible
    end as [Visible]
from project_subscriber_link psl 
    join subscriber s on s.subscriber_id = psl.subscriber_id
    join project_property_group_link ppgl on ppgl.project_id = psl.project_id
    join property_group_property_link pgpl on pgpl.property_group_id=ppgl.property_group_id
    join property_group pg on pg.property_group_id=pgpl.property_group_id
    join property p on p.property_id=pgpl.property_id	    
    join project_subscriber_property_group_link pspgl on 
        pspgl.project_subscriber_link_id = psl.project_subscriber_link_id 
        and pspgl.project_property_group_link_id = ppgl.project_property_group_link_id
    left join [validation] v on p.validation_id = v.validation_id
    join property_value_dictionary pvd on 
        pvd.link_id = pspgl.project_subscriber_property_group_link_id
        and pvd.language_id = s.default_language_id
        and pvd.property_group_id=ppgl.property_group_id
        and pvd.property_id=p.property_id		
where psl.project_id = {0} and psl.subscriber_id = {1}
order by pg.display_order, pgpl.display_order
";
        public static string GetSharedResourcePath(string path)
        {
            return path.Substring(0, path.LastIndexOf("/") + 1) + "SharedResource";
        }

    }
}
