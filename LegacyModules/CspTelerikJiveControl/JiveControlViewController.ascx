﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JiveControlViewController.ascx.cs" Inherits="TIEKinetix.CspModules.CspTelerikJiveControl.JiveControlViewController" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server"></telerik:RadAjaxLoadingPanel>
<telerik:RadAjaxPanel ID="ajaxPanel" runat="server" LoadingPanelID="loadingPanel">
    <telerik:RadGrid ID="grid" runat="server" AllowPaging="true" AllowFilteringByColumn="true" PageSize="20">
        <MasterTableView AutoGenerateColumns="false" DataKeyNames="subscriber_id">
            <Columns>
                <telerik:GridBoundColumn DataField="companyname" HeaderStyle-Width="50%" HeaderText = "Company Name" FilterControlWidth="100%"></telerik:GridBoundColumn>                
                <telerik:GridBoundColumn DataField="city" HeaderText="City" AllowFiltering="false"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="zipcode" HeaderStyle-Width="100px" HeaderText="Zipcode" AllowFiltering="false"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="country" HeaderStyle-Width="125px" HeaderText="Country" AllowFiltering="false"></telerik:GridBoundColumn>            
            </Columns>
            <NestedViewTemplate>            
            </NestedViewTemplate>
        </MasterTableView>    
    </telerik:RadGrid>
    <telerik:RadNotification runat="server" ID="notification" AutoCloseDelay="6000" Animation="Fade" EnableRoundedCorners="true" EnableShadow="true" CssClass="notification">
    </telerik:RadNotification>
</telerik:RadAjaxPanel>



