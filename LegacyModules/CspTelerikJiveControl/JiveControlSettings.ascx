﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JiveControlSettings.ascx.cs" Inherits="TIEKinetix.CspModules.CspTelerikJiveControl.JiveControlSettings" %>
<table cellpadding="2" cellspacing="2" class="table-settings">
    <tbody>
        <tr>
            <td>Control Panel Project</td>
            <td><asp:TextBox ID="tbProjectId" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Target Integrated System Id</td>
            <td><asp:TextBox ID="tbSystemId" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Mambo5 Integration Property Group Id</td>
            <td><asp:TextBox ID="tbGroupId" runat="server"></asp:TextBox></td>
        </tr>
    </tbody>
</table>