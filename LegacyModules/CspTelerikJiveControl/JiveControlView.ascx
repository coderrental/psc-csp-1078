﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JiveControlView.ascx.cs" Inherits="TIEKinetix.CspModules.JiveControlView" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadFormDecorator ID="formDecorator" runat="server" DecoratedControls="All" />

<telerik:RadGrid ID="grid" runat="server" AllowPaging="true" AllowSorting="true" PageSize="25" OnSelectedIndexChanged="GridSelectedIndexChangedHandler" AllowFilteringByColumn="true">
    <MasterTableView AutoGenerateColumns="false" DataKeyNames="subscriber_id">
        <Columns>
            <telerik:GridBoundColumn DataField="companyname" HeaderText="Company Name"></telerik:GridBoundColumn>
        </Columns>        
    </MasterTableView>
    <ClientSettings EnablePostBackOnRowClick="true">
        <Selecting AllowRowSelect="true" />        
    </ClientSettings>
</telerik:RadGrid>