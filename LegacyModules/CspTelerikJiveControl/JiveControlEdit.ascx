﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JiveControlEdit.ascx.cs" Inherits="TIEKinetix.CspModules.CspTelerikJiveControl.JiveControlEdit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadFormDecorator ID="formDecorator" runat="server" DecoratedControls="All" />
<telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server"></telerik:RadAjaxLoadingPanel>

<telerik:RadAjaxPanel ID="ajaxPanel" runat="server" LoadingPanelID="loadingPanel">

<telerik:RadToolBar runat="server" ID="toolBarTop" Width="100%" OnButtonClick="ToolBarClickHandler">
</telerik:RadToolBar>

<br />
<br />


    <telerik:RadPanelBar runat="server" ID="panelBar" ExpandMode="MultipleExpandedItems" Width="100%">
    </telerik:RadPanelBar>
    <asp:Panel runat="server" ID="propertyGroupPanel" CssClass="property_panel">
    </asp:Panel>

<br />

<telerik:RadToolBar runat="server" ID="toolBarBottom" Width="100%"  OnButtonClick="ToolBarClickHandler">
</telerik:RadToolBar>

</telerik:RadAjaxPanel>

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        $(".property_group_expand").click(function() {
            $(this).parent().next().toggle();
        });

        $(".need_focus:first").focus();
    </script>
</telerik:RadCodeBlock>