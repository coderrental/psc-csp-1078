﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Entities.Modules;
using TIEKinetix.CspModules.CspTelerikJiveControl.Components;

namespace TIEKinetix.CspModules.CspTelerikJiveControl
{
    public partial class JiveControlSettings : ModuleSettingsBase
    {
        public override void LoadSettings()
        {
            if (!IsPostBack)
            {
                if (TabModuleSettings[Common.ProjectId] != null)
                    tbProjectId.Text = TabModuleSettings[Common.ProjectId].ToString();
                if (TabModuleSettings[Common.SystemId] != null)
                    tbSystemId.Text = TabModuleSettings[Common.SystemId].ToString();
                if (TabModuleSettings[Common.Mambo5IntegrationGroupId] != null)
                    tbGroupId.Text = TabModuleSettings[Common.Mambo5IntegrationGroupId].ToString();
            }
        }

        public override void UpdateSettings()
        {
            ModuleController objModules = new ModuleController();
            objModules.UpdateTabModuleSetting(TabModuleId, Common.ProjectId, tbProjectId.Text ?? "");
            objModules.UpdateTabModuleSetting(TabModuleId, Common.SystemId, tbSystemId.Text ?? "");
            objModules.UpdateTabModuleSetting(TabModuleId, Common.Mambo5IntegrationGroupId, tbGroupId.Text ?? "");
            ModuleController.SynchronizeModule(ModuleId);
        }
    }
}