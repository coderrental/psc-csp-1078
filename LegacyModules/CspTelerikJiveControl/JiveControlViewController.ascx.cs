﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Localization;
using DotNetNuke.Services.Log.EventLog;
using Telerik.Web.UI;
using TIEKinetix.CspModules.CspTelerikJiveControl.Components;
using TIEKinetix.Data;

namespace TIEKinetix.CspModules.CspTelerikJiveControl
{
    public partial class JiveControlViewController : PortalModuleBase
    {
        private static Data.CspDataLayerContext _context;
        private Guid _projectId, _systemId, _mambo5IntegrationGroupId;
        private string _sharedResourceFile;
        private List<NestedGroup> _propertyGroups;
        private List<code_list> _codeList;
        private List<validation> _validations;

        protected string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["TIECoreAdmin"].ConnectionString;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            _sharedResourceFile = Common.GetSharedResourcePath(LocalResourceFile);
            //module title
            ModuleConfiguration.ModuleTitle = Localization.GetString("ModuleTitle", _sharedResourceFile);


            if (Settings[Common.ProjectId] == null || Settings[Common.SystemId] == null || Settings[Common.Mambo5IntegrationGroupId] == null)
            {
                Controls.Add(new Literal {Text = "Please configure the module correctly in the module settings. "});
            }
            else
            {
                try
                {
                    _context = new CspDataLayerContext(ConnectionString);
                    _codeList = _context.code_lists.ToList();
                    _validations = _context.validations.ToList();

                    _projectId = new Guid(Settings[Common.ProjectId].ToString());
                    _systemId = new Guid(Settings[Common.SystemId].ToString());
                    _mambo5IntegrationGroupId = new Guid(Settings[Common.Mambo5IntegrationGroupId].ToString());

                    grid.DataSource = (from link in _context.project_subscriber_links.Where(a => a.project_id.Equals(_projectId))
                                       join sub in _context.subscribers on link.subscriber_id equals sub.subscriber_id
                                       select sub).OrderBy(a => a.companyname).ToArray();

                    property_group[] groups = (from link in _context.project_property_group_links.Where(a => a.project_id == _projectId)
                                               join pg in _context.property_groups on link.property_group_id equals pg.property_group_id
                                               select pg).OrderBy(a => a.display_order).ToArray();

                    _propertyGroups = new List<NestedGroup>();
                    foreach (property_group pg in groups.Where(a => !a.parent_property_group_id.HasValue))
                    {
                        _propertyGroups.Add(FindNestedGroup(pg, groups, 0));
                    }
                    grid.ItemCreated += new GridItemEventHandler(GridItemCreated);
                }
                catch(Exception ex)
                {
                    EventLogController c = new EventLogController();
                    c.AddLog(ex.Message, ex.StackTrace, PortalSettings, UserId, EventLogController.EventLogType.ADMIN_ALERT);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private static NestedGroup FindNestedGroup(property_group propertyGroup, property_group[] groups, int level)
        {
            NestedGroup group = new NestedGroup(propertyGroup, null, new List<NestedGroup>(), level);
            GetTranslatedValues(group, System.Threading.Thread.CurrentThread.CurrentCulture.Name);
            if (propertyGroup.parent_property_group_id.HasValue)
                group.ParentGroup = groups.First(a => a.property_group_id == propertyGroup.parent_property_group_id.Value);
            var tempGroups = groups.Where(a => a.parent_property_group_id.HasValue && (a.parent_property_group_id.Value == group.Group.property_group_id));
            if (tempGroups.Count() > 0) level++;
            foreach (property_group childGroup in tempGroups)
            {
                group.Children.Add(FindNestedGroup(childGroup, groups, level));
            }
            return group;
        }

        private static void GetTranslatedValues(NestedGroup grp, string selectedLanguage)
        {
            translated_text[] tt = (from lng in _context.languages.Where(a => a.language_code == selectedLanguage)
                                  join t in _context.translated_texts on lng.language_id equals t.language_id into Temp
                                  from t1 in Temp.Where(a=>a.translated_item_id == grp.Group.property_group_id).DefaultIfEmpty()
                                  select t1).ToArray();
            if (tt.Length > 0)
            {
                grp.TranslatedName = !string.IsNullOrEmpty(tt[0].value) ? tt[0].value : grp.Group.property_group_name;
                grp.TranslatedDesc = !string.IsNullOrEmpty(tt[0].long_value) ? tt[0].long_value : grp.Group.property_group_description;
            }
            else
            {
                grp.TranslatedName = grp.Group.property_group_name;
                grp.TranslatedDesc= grp.Group.property_group_description;
            }
        }

        void GridItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridNestedViewItem)
            {
                RadGrid grid = (sender as RadGrid);
                GridNestedViewItem item = e.Item as GridNestedViewItem;
                if (item.ParentItem.GetDataKeyValue("subscriber_id") == null) return;
                Guid subscriberId = (Guid) item.ParentItem.GetDataKeyValue("subscriber_id");
                NestedGroup pmcGgroup = _propertyGroups.FirstOrDefault(a => a.Group.property_group_name == "PMC");


                RadMultiPage pages = new RadMultiPage { SelectedIndex = 0, ID = subscriberId.ToString() };
                RadTabStrip tabs = new RadTabStrip { SelectedIndex = 0, CausesValidation = false, MultiPageID = subscriberId.ToString()};

                if (pmcGgroup != null)
                {
                    item.NestedViewCell.Controls.Add(tabs);

                    RadToolBar toolBar = new RadToolBar { Width = new Unit("100%") };
                    toolBar.Attributes.Add("sid", subscriberId.ToString());
                    toolBar.ButtonClick += new RadToolBarEventHandler(ToolBarButtonClick);
                    toolBar.Items.Add(new RadToolBarButton { Text = Localization.GetString("ToolBar.Save", _sharedResourceFile), Value = "Save", ImageUrl = ControlPath + "Images/save_as.png" });
                    RadToolBarButton radToolButton = new RadToolBarButton { Text = Localization.GetString("ToolBar.CreateShop", _sharedResourceFile), Value = "CreateShop", ImageUrl = ControlPath + "Images/shoppingcart.png" };
                    
                    if (ShopExists(subscriberId))
                    {
                        radToolButton.Text = Localization.GetString("ToolBar.UpdateShop", _sharedResourceFile);
                        radToolButton.Value = "UpdateShop";
                    }

                    if (_context.on_demand_processes.Count(a => a.project_id == _projectId && a.subscriber_id == subscriberId && a.status_code == 0) > 0)
                    {
                        radToolButton.Enabled = false;
                        radToolButton.ToolTip = Localization.GetString("ToolBar.Processing", _sharedResourceFile);
                    }

                    toolBar.Items.Add(radToolButton);

                    //pv.Controls.Add(toolBar);
                    item.NestedViewCell.Controls.Add(toolBar);

                    item.NestedViewCell.Controls.Add(pages);

                    RenderPmcGroup(pmcGgroup, tabs, pages, subscriberId, false);
                    RenderAffiliateGroups(pmcGgroup.Children, tabs, pages, subscriberId);
                }
            }
        }

        private bool ShopExists(Guid subscriberId)
        {
            var r = _context.ExecuteQuery<int>(Common.ShopExistsSql, _systemId, _projectId, subscriberId, _mambo5IntegrationGroupId).ToArray();
            return (r.Length > 0 && r[0] > 0);
        }

        private void FindAllTextBoxes(Control control, ref List<Control> textBoxes)
        {
            foreach (Control c in control.Controls)
            {
                if (c is TextBox || c is DropDownList || c is RadEditor)
                    textBoxes.Add(c);
                if (c.Controls.Count > 0)
                    FindAllTextBoxes(c, ref textBoxes);
            }
        }

        private void RenderAffiliateGroups(List<NestedGroup> groups, RadTabStrip tabs, RadMultiPage pages, Guid subscriberId)
        {
            if (groups.Count == 0) return;
            foreach (NestedGroup nestedGroup in groups)
            {
                RenderPmcGroup(nestedGroup, tabs,pages, subscriberId, true);
            }
        }

        private void RenderPmcGroup(NestedGroup propertyGroup, RadTabStrip tabs, RadMultiPage pages, Guid subscriberId, bool renderChildren)
        {
            if (propertyGroup == null) return;

            tabs.Tabs.Add(new RadTab {Text = propertyGroup.TranslatedName, PageViewID = "pv"+propertyGroup.Level+propertyGroup.Group.property_group_id});
            RadPageView pv = new RadPageView { ID = "pv" + propertyGroup.Level + propertyGroup.Group.property_group_id, CssClass = "property_page_view" };
            pages.PageViews.Add(pv);
            GeneratePropertyControls(pv, propertyGroup, subscriberId, renderChildren);

            if(renderChildren && propertyGroup.Children.Count > 0)
            {
                foreach (NestedGroup child in propertyGroup.Children)
                {
                    Panel panel = new Panel();
                    panel.CssClass = "child_group level" + child.Level;
                    pv.Controls.Add(panel);
                    panel.Controls.Add(new Literal { Text = "<div class='property_header'>" + child.TranslatedName + "</div>" });
                    GeneratePropertyControls(panel, child, subscriberId, true);
                }
            }
        }

        private void GeneratePropertyControls(Control panelItem, NestedGroup propertyGroup, Guid subscriberId, bool renderChidlren)
        {
            Table tbl = new Table();
            tbl.CssClass = "property_table";
            NameValuePair[] properties = _context.ExecuteQuery<NameValuePair>(Common.GetProperties, _projectId, subscriberId, propertyGroup.Group.property_group_id).ToArray();
            subscriber sub = _context.subscribers.First(a => a.subscriber_id == subscriberId);
            string controlId;
            foreach (NameValuePair prop in properties)
            {
                controlId = string.Format("{0}_{1}_{2}_{3}", prop.NameId, prop.ValueId ?? "", prop.LinkId, prop.GroupId);
                
                TableRow tr = new TableRow();
                tbl.Rows.Add(tr);
                tr.Visible = !prop.Visible.HasValue || prop.Visible.Value;
                TableCell cell = new TableCell();

                //handle the grouping property / header
                if (Common.HeaderField.Equals(prop.PropertyType, StringComparison.OrdinalIgnoreCase))
                {
                    cell.ColumnSpan = 4;
                    cell.CssClass = "property_header";
                    Literal literal = new Literal();
                    cell.Controls.Add(literal);
                    literal.Text = string.Format(@"
<div>    
    <h1>{0}</h1>
</div>
", prop.Name);
                    tr.Cells.Add(cell);
                    continue;
                }

                cell = new TableCell{Width = new Unit(24)};
                tr.Cells.Add(cell);
                cell.Controls.Add(new Image { ImageUrl = ControlPath + "Images/information.png", ID = "info_"+controlId });
                cell.Controls.Add(new RadToolTip { Text = prop.Desc, RelativeTo = ToolTipRelativeDisplay.Element, TargetControlID = "info_" + controlId });

                cell = new TableCell{Width = new Unit("20%")};
                tr.Cells.Add(cell);
                cell.Controls.Add(new Label { Text = prop.Name });

                cell = new TableCell{Width = new Unit("300px")};
                tr.Cells.Add(cell);

                if (prop.CodeListId != null)
                {
                    DropDownList ddl = new DropDownList { ID = controlId };
                    foreach (code_list_value val in _codeList.First(a => a.code_list_id.Equals(prop.CodeListId ?? Guid.Empty)).code_list_values)
                    {
                        ddl.Items.Add(new ListItem { Text = val.code, Value = val.code, Selected = val.code == prop.Value });
                    }
                    cell.Controls.Add(ddl);
                }
                else
                {
                    bool passValidation = true;
                    switch (prop.PropertyType)
                    {
                        case "password":
                            cell.Controls.Add(new TextBox {CssClass = "property_field", Text = prop.Value ?? prop.DefaultValue, ID = controlId, TextMode = TextBoxMode.Password});
                            break;
                        case "editor":
                            cell.Controls.Add(new RadEditor {Content = prop.Value ?? prop.DefaultValue, ID = controlId});
                            break;
                        case "areatext":
                            cell.Controls.Add(new TextBox {CssClass = "property_field", Rows = 4, ID = controlId, Text = prop.Value ?? prop.DefaultValue, TextMode = TextBoxMode.MultiLine});
                            break;
                        default:
                            string v = prop.Value ?? prop.DefaultValue;
                            if (prop.Visible.HasValue && !prop.Visible.Value)
                            { // if visible then we should ignore user input and get values from default values
                                v = prop.DefaultValue ?? "";
                            }
                            else 
                                if (!string.IsNullOrEmpty(prop.PropertyType) && string.IsNullOrEmpty(v))
                            {
                                // try to use values in subscriber / contacts to fill out the missing info
                                try
                                {
                                    string[] temp = prop.PropertyType.Split('.');
                                    if (temp.Length == 2)
                                    {
                                        if (temp[0].Equals("subscriber"))
                                            v = (string) DataBinder.Eval(sub, temp[1]);
                                        else if (temp[0].Equals("contact") && sub.subscriber_contacts.Count > 0)
                                        {
                                            v = (string)DataBinder.Eval(sub.subscriber_contacts[0], temp[1]);
                                        }
                                    }
                                }
                                catch
                                {
                                    v = prop.Value ?? prop.DefaultValue;
                                }
                            }
                            cell.Controls.Add(new TextBox {Text = v, CssClass = "property_field " + (prop.Value == null ? "use_default" : ""), ID = controlId});
                            if (prop.ValidationId != null)
                            {
                                passValidation = System.Text.RegularExpressions.Regex.IsMatch(prop.Value ?? "", _validations.First(a => a.validation_id.Equals(prop.ValidationId ?? Guid.Empty)).validation_regexp);
                            }
                            break;
                    }
                    cell = new TableCell();
                    tr.Cells.Add(cell);
                    if ((bool)prop.IsMandatory)
                    {
                        if (string.IsNullOrEmpty(prop.Value) || !passValidation)
                            cell.Controls.Add(new Label { CssClass = "val_fail", ID = controlId + "_icon" });
                        else
                            cell.Controls.Add(new Label { CssClass = "val_pass", ID = controlId + "_icon" });

                        if ((bool)prop.IsMandatory)
                        {
                            //cell.Controls.Add(new RequiredFieldValidator { ControlToValidate = controlId, Text = "Field is mandatory" });
                        }

                    }
                    else if (!passValidation)
                        cell.Controls.Add(new Label { CssClass = "val_fail", ID = controlId + "_icon" });
                    else if (prop.ValidationId != null)
                        cell.Controls.Add(new Label { CssClass = "val_pass", ID = controlId + "_icon" });
                }
            }
            panelItem.Controls.Add(tbl);
        } // end of generating controls

        #region ToolBar Button Click Handler / Save stuff
        void ToolBarButtonClick(object sender, RadToolBarEventArgs e)
        {
            Guid subscriberId;
            switch (e.Item.Value)
            {
                case "Save":
                    bool isPassed = true;

                    var controls = new List<Control>();
                    FindAllTextBoxes(e.Item.NamingContainer.Parent, ref controls);

                    // validate
                    string validateMessage = "";
                    if (!ValidateUserInput(controls, out validateMessage))
                    {
                        notification.ContentIcon = ControlPath + "Images/24x24/warning.png";
                        notification.Show(validateMessage);
                        break; // fail validation
                    }

                    subscriberId = new Guid(((RadToolBar)e.Item.NamingContainer).Attributes["sid"]);
                    SaveUserInput(subscriberId, controls);
                    break;  //end of Save case
                case "CreateShop":
                    //validate
                    subscriberId = new Guid(((RadToolBar)e.Item.NamingContainer).Attributes["sid"]);
                    if (!ValidateProperties(_projectId, subscriberId))
                    {
                        notification.ContentIcon = ControlPath + "Images/24x24/warning.png";
                        notification.Show(Localization.GetString("Shop.CreateError", _sharedResourceFile));
                    }
                    else
                    {
                        on_demand_process onDemandProcess = new on_demand_process
                                                                {
                                                                    on_demand_process_id = Guid.NewGuid(),
                                                                    command_name = "CreateShop",
                                                                    project_id = _projectId,
                                                                    subscriber_id = subscriberId,
                                                                    status_code = 0,
                                                                    source = "Quantore DNN Portal",
                                                                    date_created = DateTime.Now
                                                                };
                        _context.on_demand_processes.InsertOnSubmit(onDemandProcess);
                        _context.SubmitChanges();

                        
                        notification.ContentIcon = ControlPath + "Images/24x24/check.png";
                        notification.Show(Localization.GetString("Shop.CreateSuccess", _sharedResourceFile));
                        RadToolBarItem item = ((RadToolBar)e.Item.NamingContainer).Items.FindItem(a => a.Value == "CreateShop" || a.Value == "UpdateShop");
                        if (item != null && _context.on_demand_processes.Count(a => a.project_id == _projectId && a.subscriber_id == subscriberId && a.status_code == 0) > 0)
                        {
                            item.Enabled = false;
                            item.ToolTip = Localization.GetString("ToolBar.Processing", _sharedResourceFile);
                        }
                    }
                    break;
                case "UpdateShop":
                    subscriberId = new Guid(((RadToolBar)e.Item.NamingContainer).Attributes["sid"]);
                    if (!ValidateProperties(_projectId, subscriberId))
                    {
                        notification.ContentIcon = ControlPath + "Images/24x24/warning.png";
                        notification.Show(Localization.GetString("Shop.UpdateError", _sharedResourceFile));
                    }
                    else
                    {
                        on_demand_process onDemandProcess = new on_demand_process
                                                                {
                                                                    on_demand_process_id = Guid.NewGuid(),
                                                                    command_name = "Update",
                                                                    project_id = _projectId,
                                                                    subscriber_id = subscriberId,
                                                                    status_code = 0,
                                                                    source = "Quantore DNN Portal",
                                                                    date_created = DateTime.Now
                                                                };
                        _context.on_demand_processes.InsertOnSubmit(onDemandProcess);
                        _context.SubmitChanges();

                        notification.ContentIcon = ControlPath + "Images/24x24/check.png";
                        notification.Show(Localization.GetString("Shop.UpdateSuccess", _sharedResourceFile));
                        RadToolBarItem item = ((RadToolBar) e.Item.NamingContainer).Items.FindItem(a => a.Value == "CreateShop" || a.Value == "UpdateShop");
                        if (item != null && _context.on_demand_processes.Count(a => a.project_id == _projectId && a.subscriber_id == subscriberId && a.status_code == 0) > 0)
                        {
                            item.Enabled = false;
                            item.ToolTip = Localization.GetString("ToolBar.Processing", _sharedResourceFile);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private static bool ValidateProperties(Guid projectId, Guid subscriberId)
        {
            bool flag = true;
            List<PropertyResultSet> resultSets = _context.ExecuteQuery<PropertyResultSet>(Common.GetPropertiesForValidation, projectId, subscriberId).ToList();
            if (resultSets.Count == 0) 
                return false;
            if (resultSets.Count(a=>a.PropertyGroupName.Equals("pmc", StringComparison.OrdinalIgnoreCase)) == 0)
                return false;
            foreach (PropertyResultSet propertyResultSet in resultSets)
            {
                //handle default values for hidden fields.
                if (!propertyResultSet.Visible && string.IsNullOrEmpty(propertyResultSet.PropertyValue)) 
                    propertyResultSet.PropertyValue = propertyResultSet.PropertyDefaultValue;
                
                flag = flag && (!propertyResultSet.Required || !string.IsNullOrEmpty(propertyResultSet.PropertyValue));
                if (!string.IsNullOrEmpty(propertyResultSet.PropertyValidationRule))
                {
                    flag = flag && System.Text.RegularExpressions.Regex.IsMatch(propertyResultSet.PropertyValue, propertyResultSet.PropertyValidationRule);
                }
                if (!flag)
                    break;
            }
            return flag;
        }

        #region private functions

        private void SaveUserInput(Guid subscriberId, ICollection<Control> controls)
        {
            foreach (Control control in controls)
            {
                string[] Ids = control.ID.Split('_');
                if (Ids.Length == 4)
                {
                    //0: Property, 1: Value Id, 2: Link, 3: Group
                    if (Ids[1] == "" && Ids[2] == "")
                    {
                        Guid[] linkIds = _context.ExecuteQuery<Guid>(Common.GetAndCreateLinkSql, _projectId, subscriberId, Ids[3]).ToArray();
                        if (linkIds.Length > 0)
                        {
                            Ids[2] = linkIds[0].ToString();
                        }
                    }

                    string newValue = string.Empty;
                    if (control is TextBox) newValue = ((TextBox) control).Text;
                    else if (control is RadEditor) newValue = ((RadEditor) control).Content;
                    else if (control is DropDownList) newValue = ((DropDownList) control).SelectedValue;

                    property prop = _context.properties.FirstOrDefault(a => a.property_id.Equals(new Guid(Ids[0])));

                    if (prop.property_type == "code")
                    {
                        // if property type is code, that means either we ignore it or handle the logic here 
                        // todo: make this piece generic

                        switch (prop.property_name)
                        {
                            case "MPROJECT_SHOP_TYPE":
                                #region handle special case for shop type
                                switch (newValue)
                                {
                                    case "Open Shop":
                                        SetPropertyValue(_projectId, subscriberId, Ids[3], "DEFAULT_PROC", "RAPRL");
                                        SetPropertyValue(_projectId, subscriberId, Ids[3], "DEFAULT_PROC_NOLOGIN", "");
                                        SetPropertyValue(_projectId, subscriberId, Ids[3], "PMLOGIN_PRICES", "");
                                        break;
                                    case "Half Open Shop":
                                        SetPropertyValue(_projectId, subscriberId, Ids[3], "DEFAULT_PROC", "RHOME");
                                        SetPropertyValue(_projectId, subscriberId, Ids[3], "DEFAULT_PROC_NOLOGIN", "");
                                        SetPropertyValue(_projectId, subscriberId, Ids[3], "PMLOGIN_PRICES", "FALSE");
                                        break;
                                    case "Closed Shop":
                                        SetPropertyValue(_projectId, subscriberId, Ids[3], "DEFAULT_PROC", "RAPRL");
                                        SetPropertyValue(_projectId, subscriberId, Ids[3], "DEFAULT_PROC_NOLOGIN", "RMUSR");
                                        SetPropertyValue(_projectId, subscriberId, Ids[3], "PMLOGIN_PRICES", "");
                                        break;
                                    default:
                                        break;
                                }

                                #endregion
                                break;
                            default:
                                break;
                        }
                        //if (prop.property_name != "MPROJECT_SHOP_TYPE") continue;
                    } // end of special case.
                    else if (prop.property_type == "system-modified")
                    {
                        continue; // ignore
                    }

                    // normal routine
                    if (Ids[1] == "" && Ids[2] != "")
                    {
                        Guid linkId = new Guid(Ids[2]), groupId = new Guid(Ids[3]), propertyId = new Guid(Ids[0]), lngId = _context.subscribers.FirstOrDefault(a => a.subscriber_id == subscriberId).default_language_id;
                        //second check to ensure this doesnt exist yet.
                        if (_context.property_value_dictionaries.Count(a => a.link_id == linkId && a.property_group_id == groupId && a.property_id == propertyId && a.language_id == lngId) == 0)
                        {
                            property_value_dictionary record = new property_value_dictionary
                                                                   {
                                                                       link_id = linkId,
                                                                       property_group_id = groupId,
                                                                       property_id = propertyId,
                                                                       language_id = lngId,
                                                                       value = newValue,
                                                                       date_created = DateTime.Now,
                                                                       date_changed = DateTime.Now
                                                                   };
                            _context.property_value_dictionaries.InsertOnSubmit(record);
                            _context.SubmitChanges(ConflictMode.FailOnFirstConflict);
                        }
                        else
                        {
                            property_value_dictionary record = _context.property_value_dictionaries.FirstOrDefault(a => a.link_id == linkId && a.property_group_id == groupId && a.property_id == propertyId && a.language_id == lngId);
                            if (record != null)
                                record.value = newValue;
                        }
                    }
                    else if (Ids[1] != "")
                    {
                        property_value_dictionary record = _context.property_value_dictionaries.FirstOrDefault(a => a.id == Convert.ToInt32(Ids[1]));
                        if (record != null)
                            record.value = newValue;
                    }
                }
                _context.SubmitChanges(ConflictMode.FailOnFirstConflict);
            } // end of foreach

            if (controls.Count > 0)
            {
                //_context.SubmitChanges(ConflictMode.FailOnFirstConflict);
                //ViewState.Clear();
                notification.ContentIcon = ControlPath + "Images/24x24/check.png";
                notification.Show(Localization.GetString("Save.Success", _sharedResourceFile));
            }            
        }

        private bool ValidateUserInput(List<Control> controls, out string errorMessage)
        {
            errorMessage = "";
            bool isPassed = true;
            foreach (Control control in controls)
            {
                string[] Ids = control.ID.Split('_');
                property prop = _context.properties.FirstOrDefault(a => a.property_id.Equals(new Guid(Ids[0])));
                if (prop == null)
                {
                    errorMessage += "Invalid Property(ies).";
                    isPassed = false;
                    break;
                }

                string propertyName = "", propertyDesc="";
                _context.TranslateProperty(prop.property_id, System.Threading.Thread.CurrentThread.CurrentCulture.Name, ref propertyName, ref propertyDesc);

                // handle custom validation
                if (prop.property_type == "code")
                {
                    //0: Property, 1: Value Id, 2: Link, 3: Group
                    switch (prop.property_name)
                    {
                        case "affiliate.aff_id":
                            TextBox tb = (TextBox)control;
                            Label icon = (Label)tb.Parent.Parent.FindControl(tb.ID + "_icon");

                            if (string.IsNullOrEmpty(Ids[1])) // has no value
                            {
                                if (string.IsNullOrEmpty(tb.Text))
                                {
                                    isPassed = false;
                                    errorMessage += " [<b>" + propertyName + "</b>] is required. <br/>";
                                }
                                else if (_context.ExecuteQuery<int>(Common.CountProjectPropertyValueSql, _systemId, _projectId, Ids[3], Ids[0], tb.Text).First() > 0
                                || _context.ExecuteQuery<int>(Common.CountSystemPropertyValueSql, _systemId, _projectId, Ids[3], Ids[0], tb.Text).First() > 0)
                                {
                                    isPassed = false;
                                    errorMessage += " [<b>" + propertyName + "</b>] already exists. <br/>";
                                }
                            }
                            else // has value
                            {
                                if (string.IsNullOrEmpty(tb.Text))
                                {
                                    isPassed = false;
                                    errorMessage += " [<b>" + propertyName + "</b>] is required. <br/>";
                                }
                                else
                                {
                                    // does it have the same value?
                                    string affId = _context.property_value_dictionaries.First(a => a.id == Convert.ToInt32(Ids[1])).value;
                                    if (affId == tb.Text)
                                    {
                                    }
                                    else if ( affId != tb.Text &&
                                        (_context.ExecuteQuery<int>(Common.CountProjectPropertyValueSql, _systemId, _projectId, Ids[3], Ids[0], tb.Text).First() > 0
                                        || _context.ExecuteQuery<int>(Common.CountSystemPropertyValueSql, _systemId, _projectId, Ids[3], Ids[0], tb.Text).First() > 0))
                                    {
                                        isPassed = false;
                                        errorMessage += " [<b>" + propertyName + "</b>] already exists. <br/>";
                                    }
                                }
                            }

                            if (!isPassed)
                            {
                                icon.CssClass = "val_fail";
                                if (tb.CssClass.IndexOf("need_focus") == -1)
                                    tb.CssClass += " need_focus";
                            }
                            else
                            {
                                icon.CssClass = "val_pass";
                                if (tb.CssClass.IndexOf("need_focus") >= 0)
                                    tb.CssClass = tb.CssClass.Replace("need_focus", "");
                            }

                            break;
                        default:
                            break;
                    }
                }

                if (prop.validation_id.HasValue)
                {
                    if (control is TextBox)
                    {
                        TextBox tb = (TextBox)control;
                        Label icon = (Label)tb.Parent.Parent.FindControl(tb.ID + "_icon");

                        isPassed = isPassed && System.Text.RegularExpressions.Regex.IsMatch(tb.Text, _validations.First(a => a.validation_id.Equals(prop.validation_id ?? Guid.Empty)).validation_regexp);
                        if (!isPassed)
                        {
                            icon.CssClass = "val_fail";
                            if (tb.CssClass.IndexOf("need_focus") == -1)
                                tb.CssClass += " need_focus";
                            errorMessage += " [<b>" + propertyName + "</b>] fails validation. <br/>";
                            continue;
                        }

                        icon.CssClass = "val_pass";
                        if (tb.CssClass.IndexOf("need_focus") >= 0)
                            tb.CssClass = tb.CssClass.Replace("need_focus", "");
                    }
                } // validation

                if (prop.required.HasValue && prop.required.Value)
                {
                    if (control is TextBox)
                    {
                        TextBox tb = (TextBox)control;
                        Label icon = (Label)tb.Parent.Parent.FindControl(tb.ID + "_icon");
                        if (string.IsNullOrEmpty(tb.Text))
                        {
                            icon.CssClass = "val_fail";
                            if (tb.CssClass.IndexOf("need_focus") == -1)
                                tb.CssClass += " need_focus";
                            errorMessage += " [<b>" + propertyName + "</b>] is required. <br/>";
                            continue;
                        }
                        icon.CssClass = "val_pass";
                        if (tb.CssClass.IndexOf("need_focus") >= 0)
                            tb.CssClass = tb.CssClass.Replace("need_focus", "");
                    }
                } // is required
            }
            return isPassed;
        }

        private void SetPropertyValue(Guid projectId, Guid subscriberId, string groupId, string propertyName, string propertyValue)
        {
            _context.ExecuteCommand(Common.SetPropertyValueSql, projectId, subscriberId, groupId, propertyName, propertyValue);
        }
        #endregion

        // end of save events
        #endregion
    }
}