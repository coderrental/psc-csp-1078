﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Localization;
using Telerik.Web.UI;
using TIEKinetix.CspModules.CspTelerikJiveControl.Components;
using TIEKinetix.Data;

namespace TIEKinetix.CspModules.CspTelerikJiveControl
{
    public partial class JiveControlEdit : PortalModuleBase
    {
        private Data.CspDataLayerContext _context;
        private Guid _projectId, _subscriberId;
        List<code_list> _codeList;
        private List<validation> _validations;
        private string _sharedResourceFile;

        protected string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["TIECoreAdmin"].ConnectionString;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            _context = new CspDataLayerContext(ConnectionString);
            

            if (Settings[Common.ProjectId] == null || (Settings[Common.ProjectId] != null && string.IsNullOrEmpty(Settings[Common.ProjectId].ToString())))
            {
                RadPanelItem item = new RadPanelItem("Error");
                item.Controls.Add(new Literal {Text = "Need to specify project id in module settings"});
                panelBar.Items.Add(item);
            }
            else
            {
                _codeList = _context.code_lists.ToList();
                _validations = _context.validations.ToList();

                _projectId = new Guid(Settings[Common.ProjectId].ToString());
                _subscriberId = new Guid(Request.QueryString["sid"]);

                property_group[] groups = (from link in _context.project_property_group_links
                                           join pg in _context.property_groups on link.property_group_id equals pg.property_group_id
                                           select pg
                                          ).OrderBy(a=>a.display_order).ToArray();

                List<NestedGroup> nestedGroups = new List<NestedGroup>();
                foreach (property_group pg in groups.Where(a => !a.parent_property_group_id.HasValue))
                {
                    nestedGroups.Add(FindNestedGroup(pg, groups, 0));
                }

                //RenderNestedGroupWithPanelItem(nestedGroups, null);
                RenderNestedGroup(nestedGroups.Where(a=>a.Group.property_group_name == "PMC").ToList(), null);

                toolBarTop.Items.Add(new RadToolBarButton {Text = "Save", ImageUrl = ControlPath + "Images/disk_blue.png", Value = "Save", Width = new Unit(80)});
                toolBarTop.Items.Add(new RadToolBarButton { Text = "Cancel", ImageUrl = ControlPath + "Images/error.png", Value = "Cancel", Width = new Unit(80), CausesValidation = false});
                toolBarTop.Items.Add(new RadToolBarButton { IsSeparator = true});
                toolBarBottom.Items.Add(new RadToolBarButton { Text = "Save", ImageUrl = ControlPath + "Images/disk_blue.png", Value = "Save", Width = new Unit(80) });
                toolBarBottom.Items.Add(new RadToolBarButton { Text = "Cancel", ImageUrl = ControlPath + "Images/error.png", Value = "Cancel", Width = new Unit(80) });
                toolBarBottom.Items.Add(new RadToolBarButton { IsSeparator = true });

                _sharedResourceFile = LocalResourceFile.Substring(0, LocalResourceFile.LastIndexOf("/") + 1) + "SharedResource";
                ModuleContext.Configuration.ModuleTitle = Localization.GetString("ModuleTitle", _sharedResourceFile);
            }
        }

        private void RenderNestedGroup(List<NestedGroup> groups, Control parentNode)
        {
            foreach (NestedGroup nestedGroup in groups)
            {
                Panel item = new Panel();
                item.CssClass = "property_group level" + nestedGroup.Level;
                if (parentNode == null)
                    propertyGroupPanel.Controls.Add(item);
                else
                    parentNode.Controls.Add(item);

                GenerateControls(item, nestedGroup.Group);

                if (nestedGroup.Children.Count > 0)
                    RenderNestedGroup(nestedGroup.Children, item);
            }
        }

        private void GenerateControls(Panel item, property_group propertyGroup)
        {
            item.Controls.Add(new Literal {Text = "<h1>" + propertyGroup.property_group_name + "<span class=\"property_group_expand\"></span></h1>"});
            GeneratePropertyControls(item, propertyGroup);
        }

        private void RenderNestedGroupWithPanelItem(List<NestedGroup> groups, RadPanelItem panelItem)
        {
            foreach (NestedGroup nestedGroup in groups)
            {
                RadPanelItem item = new RadPanelItem(nestedGroup.Group.property_group_name);
                if (panelItem == null)
                    panelBar.Items.Add(item);
                else 
                    panelItem.Items.Add(item);

                GeneratePropertyControls(item, nestedGroup.Group);

                if (nestedGroup.Children.Count > 0)
                    RenderNestedGroupWithPanelItem(nestedGroup.Children, item);
            }
        }

        private void GeneratePropertyControls(Control panelItem, property_group propertyGroup)
        {
            Table tbl = new Table();
            tbl.CssClass = "property_table";

            //property[] properties = (from link in _context.property_group_property_links.Where(a=>a.property_group_id == propertyGroup.property_group_id)
            //                         join p in _context.properties on link.property_id equals p.property_id
            //                         select p).ToArray();

            Components.NameValuePair[] properties = _context.ExecuteQuery<Components.NameValuePair>(Common.GetProperties,_projectId, _subscriberId, propertyGroup.property_group_id).ToArray();

            Guid id = Guid.NewGuid();
            string controlId;
            foreach (Components.NameValuePair prop in properties)
            {
                controlId = string.Format("{0}_{1}_{2}_{3}", prop.NameId, prop.ValueId ?? "", prop.LinkId, prop.GroupId);

                id = Guid.NewGuid();
                TableRow tr = new TableRow();
                tbl.Rows.Add(tr);

                TableCell cell = new TableCell();
                tr.Cells.Add(cell);
                cell.Controls.Add(new Image {ImageUrl = ControlPath + "Images/information.png", ID = id.ToString()});
                cell.Controls.Add(new RadToolTip{Text = prop.Desc, RelativeTo=ToolTipRelativeDisplay.Element, TargetControlID = id.ToString()});

                cell = new TableCell();
                tr.Cells.Add(cell);
                cell.Controls.Add(new Label {Text = prop.Name});

                cell = new TableCell();
                tr.Cells.Add(cell);

                if (prop.CodeListId != null)
                {
                    DropDownList ddl = new DropDownList{ID=controlId};
                    foreach (code_list_value val in _codeList.First(a => a.code_list_id.Equals(prop.CodeListId??Guid.Empty)).code_list_values)
                    {
                        ddl.Items.Add(new ListItem {Text = val.code, Value = val.code, Selected = val.code == prop.Value});
                    }
                    cell.Controls.Add(ddl);
                }
                else
                {
                    bool passValidation = true;
                    switch (prop.PropertyType)
                    {
                        case "password":
                            cell.Controls.Add(new TextBox { CssClass = "property_field", Text = prop.Value ?? prop.DefaultValue, ID = controlId, TextMode = TextBoxMode.Password });
                            break;
                        case "editor":
                            cell.Controls.Add(new RadEditor { Content = prop.Value ?? prop.DefaultValue, ID = controlId});
                            break;
                        case "areatext":
                            cell.Controls.Add(new TextBox { CssClass = "property_field", Rows = 4, ID = controlId, Text = prop.Value ?? prop.DefaultValue, TextMode = TextBoxMode.MultiLine });
                            break;
                        default:
                            cell.Controls.Add(new TextBox {Text = prop.Value??prop.DefaultValue, CssClass = "property_field " + (prop.Value==null?"use_default":""), ID = controlId});
                            if (prop.ValidationId != null)
                            {
                                passValidation = System.Text.RegularExpressions.Regex.IsMatch(prop.Value??"", _validations.First(a => a.validation_id.Equals(prop.ValidationId ?? Guid.Empty)).validation_regexp);
                            }
                            break;
                    }
                    cell = new TableCell();
                    tr.Cells.Add(cell);
                    if ((bool)prop.IsMandatory)
                    {
                        if (string.IsNullOrEmpty(prop.Value) || !passValidation)
                            //cell.Controls.Add(new Literal { Text = "<span class=\"val_fail\"></span>" });
                            cell.Controls.Add(new Label {CssClass = "val_fail", ID=controlId+"_icon"});
                        else
                            //cell.Controls.Add(new Literal { Text = "<span class=\"val_pass\"></span>" });
                            cell.Controls.Add(new Label {CssClass = "val_pass", ID=controlId+"_icon"});

                        if ((bool) prop.IsMandatory)
                        {
                            cell.Controls.Add(new RequiredFieldValidator {ControlToValidate = controlId, Text = "Field is mandatory"});
                        }

                    }
                    else if (!passValidation)
                        //cell.Controls.Add(new Literal { Text = "<span class=\"val_fail\"></span>" });
                        cell.Controls.Add(new Label { CssClass = "val_fail", ID = controlId + "_icon" });
                    else if (prop.ValidationId != null)
                        cell.Controls.Add(new Label { CssClass = "val_pass", ID = controlId + "_icon" });
                }
            }
            panelItem.Controls.Add(tbl);
        }

        private NestedGroup FindNestedGroup(property_group propertyGroup, property_group[] groups, int level)
        {
            NestedGroup group = new NestedGroup(propertyGroup, null, new List<NestedGroup>(), level);
            if (propertyGroup.parent_property_group_id.HasValue)
                group.ParentGroup = groups.First(a => a.property_group_id == propertyGroup.parent_property_group_id.Value);
            var tempGroups = groups.Where(a => a.parent_property_group_id.HasValue && (a.parent_property_group_id.Value == group.Group.property_group_id));
            if(tempGroups.Count() > 0) level++;
            foreach (property_group childGroup in tempGroups)
            {
                group.Children.Add(FindNestedGroup(childGroup, groups, level));
            }
            return group;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Control docType = this.Page.FindControl("skinDocType");
            if (docType != null)
                //((Literal)docType).Text = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
                ((Literal)docType).Text = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">";

                

        }

        protected void ToolBarClickHandler(object sender, RadToolBarEventArgs e)
        {
            switch (e.Item.Value)
            {
                case "Save" :
                    bool isPassed = true;

                    var controls = new List<Control>();
                    FindAllTextBoxes(propertyGroupPanel, ref controls);

                    // validate

                    foreach (Control control in controls)
                    {
                        string[] Ids = control.ID.Split('_');
                        property prop = _context.properties.FirstOrDefault(a => a.property_id.Equals(new Guid(Ids[0])));
                        if (prop == null)
                        {
                            isPassed = false;
                            break;
                        }

                        if (prop.validation_id.HasValue)
                        {
                            if (control is TextBox)
                            {
                                TextBox tb = (TextBox) control;

                                isPassed = isPassed && System.Text.RegularExpressions.Regex.IsMatch(tb.Text, _validations.First(a => a.validation_id.Equals(prop.validation_id ?? Guid.Empty)).validation_regexp);
                                Label icon = (Label)tb.Parent.Parent.FindControl(tb.ID + "_icon");

                                if (!isPassed)
                                {
                                    icon.CssClass = "val_fail";
                                    if (tb.CssClass.IndexOf("need_focus") == -1)
                                        tb.CssClass += " need_focus";
                                    tb.Focus();
                                    break;
                                }

                                icon.CssClass = "val_pass";
                                if (tb.CssClass.IndexOf("need_focus") >= 0)
                                    tb.CssClass = tb.CssClass.Replace("need_focus", "");
                            }
                        } // validation

                        if (prop.required.HasValue && prop.required.Value)
                        {
                            if (control is TextBox)
                            {
                                TextBox tb = (TextBox)control;
                                Label icon = (Label)tb.Parent.Parent.FindControl(tb.ID + "_icon");

                                if (string.IsNullOrEmpty(tb.Text))
                                {
                                    icon.CssClass = "val_fail";
                                    if (tb.CssClass.IndexOf("need_focus") == -1)
                                        tb.CssClass += " need_focus";
                                    tb.Focus();
                                    break;
                                }
                                icon.CssClass = "val_pass";
                                if (tb.CssClass.IndexOf("need_focus") >= 0)
                                    tb.CssClass = tb.CssClass.Replace("need_focus", "");
                            }
                        } // is required

                    }

                    if (!isPassed) break; // fail validation

                    foreach (Control control in controls)
                    {
                        string[] Ids = control.ID.Split('_');
                        if (Ids.Length == 4)
                        {

                            //0: Name, 1: Value Id, 2: Link, 3: Group
                            if (Ids[1] == "" && Ids[2] == "")
                            {
                                string query = @"
insert into project_subscriber_property_group_link(project_property_group_link_id,project_subscriber_link_id,property_group_id,date_created,date_changed)
select b.project_property_group_link_id, a.project_subscriber_link_id, b.property_group_id,GETDATE(),GETDATE()
from project_subscriber_link a
	join project_property_group_link b on a.project_id=b.project_id	
where a.project_id={0} and a.subscriber_id={1} and b.property_group_id={2} and not exists (
	select 1 from project_subscriber_property_group_link pspgl 
	where pspgl.project_property_group_link_id=b.project_property_group_link_id
		and pspgl.project_subscriber_link_id=a.project_subscriber_link_id
		and pspgl.property_group_id=b.property_group_id)

select a.project_subscriber_property_group_link_id 
from project_subscriber_property_group_link a
	join project_property_group_link b on a.project_property_group_link_id=b.project_property_group_link_id
	join project_subscriber_link c on c.project_subscriber_link_id=a.project_subscriber_link_id
where  b.project_id={0} and c.subscriber_id={1} and a.property_group_id={2}
";
                                Guid[] linkIds = _context.ExecuteQuery<Guid>(query, _projectId, _subscriberId, Ids[3]).ToArray();

                                if (linkIds.Length > 0)
                                {
                                    Ids[2] = linkIds[0].ToString();
                                }
                            }

                            string newValue = string.Empty;
                            if (control is TextBox) newValue = ((TextBox) control).Text;
                            else if (control is RadEditor) newValue = ((RadEditor)control).Content;
                            else if (control is DropDownList) newValue = ((DropDownList)control).SelectedValue;

                            if (Ids[1] == "" && Ids[2] != "")
                            {
                                property_value_dictionary record = new property_value_dictionary
                                {
                                    link_id = new Guid(Ids[2]),
                                    property_group_id = new Guid(Ids[3]),
                                    property_id = new Guid(Ids[0]),
                                    language_id = _context.subscribers.FirstOrDefault(a => a.subscriber_id == _subscriberId).default_language_id,
                                    value = newValue,
                                    date_created = DateTime.Now,
                                    date_changed = DateTime.Now
                                };
                                _context.property_value_dictionaries.InsertOnSubmit(record);
                            }
                            else if (Ids[1] != "")
                            {
                                property_value_dictionary record = _context.property_value_dictionaries.FirstOrDefault(a => a.id == Convert.ToInt32(Ids[1]));
                                if (record != null)
                                    record.value = newValue;
                            }
                        }
                    } // end of foreach

                    if (controls.Count > 0)
                        _context.SubmitChanges(ConflictMode.FailOnFirstConflict);

                    break;
                default:
                    Response.Redirect(DotNetNuke.Common.Globals.NavigateURL(), false);
                    break;
            }
        }

        private void FindAllTextBoxes(Control control, ref List<Control> textBoxes)
        {
            foreach (Control c in control.Controls)
            {
                if (c is TextBox || c is DropDownList || c is RadEditor) 
                    textBoxes.Add(c);
                if (c.Controls.Count > 0)
                    FindAllTextBoxes(c, ref textBoxes);
            }
        }

    }
}