﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Localization;
using Telerik.Web.UI;
using TIEKinetix.CspModules.CspTelerikJiveControl.Components;
using TIEKinetix.Data;


namespace TIEKinetix.CspModules
{
    public partial class JiveControlView : PortalModuleBase
    {
        private Data.CspDataLayerContext _context;
        private Guid _projectId;
        private string _sharedResourceFile;

        protected string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["TIECoreAdmin"].ConnectionString;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Settings[Common.ProjectId] == null)
            {
                Controls.Add(new Literal { Text = "Need project Id." });
            }
            else
            {
                _context = new CspDataLayerContext(ConnectionString);
                _projectId = new Guid(Settings[Common.ProjectId].ToString());
                grid.DataSource = (from link in _context.project_subscriber_links.Where(a => a.project_id.Equals(_projectId))
                                   join sub in _context.subscribers on link.subscriber_id equals sub.subscriber_id
                                   select sub).OrderBy(a => a.companyname).ToArray();

                _sharedResourceFile = LocalResourceFile.Substring(0, LocalResourceFile.LastIndexOf("/") + 1) + "SharedResource";
                ModuleContext.Configuration.ModuleTitle = Localization.GetString("ModuleTitle", _sharedResourceFile);

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Control docType = this.Page.FindControl("skinDocType");
            if (docType != null)
                //((Literal)docType).Text = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
                ((Literal)docType).Text = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">";
        }

        protected void GridSelectedIndexChangedHandler(object sender, EventArgs e)
        {
            Response.Redirect(DotNetNuke.Common.Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "EditWebshop", "mid", ModuleId.ToString(), "sid", (grid.SelectedItems[0] as GridDataItem).GetDataKeyValue("subscriber_id").ToString()), true);            
        }
    }
}