﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="DotNetNuke.Entities.Modules.ModuleSettingsBase" %>
<%@ Import Namespace="CspTelerikManageConsumerProfile.Components" %>
<%@ Import Namespace="DotNetNuke.Entities.Modules"%>

<script runat="server">
	#region Overrides of ModuleSettingsBase
	public override void LoadSettings()
	{
		if (!IsPostBack)
		{
			if (TabModuleSettings[Commons.EDITABLE_PARAMETERS] != null)
				parametersToEdit.Attributes["value"] = TabModuleSettings[Commons.EDITABLE_PARAMETERS].ToString();
		}
	}

	public override void UpdateSettings()
	{
		ModuleController objModules = new ModuleController();
		objModules.UpdateTabModuleSetting(TabModuleId, Commons.EDITABLE_PARAMETERS, parametersToEdit.Attributes["value"] ?? "");
		//refresh cache
		ModuleController.SynchronizeModule(ModuleId);
	}
	#endregion
</script>

<div>
	<table cellpadding="2" cellspacing="2">
		<tbody>
			<tr>
				<td><label>Consumer Parameter Names</label></td>
				<td><input id="parametersToEdit" runat="server" size="100" /></td>
			</tr>
		</tbody>
	</table>
</div>