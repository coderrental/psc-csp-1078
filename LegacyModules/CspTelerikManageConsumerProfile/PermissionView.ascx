﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PermissionView.ascx.cs" Inherits="CspTelerikManageConsumerProfile.PermissionView" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadGrid ID="permissionGrid" runat="server" AllowPaging="true" AllowSorting="true" AllowFilteringByColumn="true" ShowGroupPanel="True" DataSourceID="dsPermissions"
	OnItemCreated="OnGridPermissionItemCreated" OnItemCommand="OnGridPermissionItemCommand">
	<MasterTableView AutoGenerateColumns="false" DataKeyNames="PermissionId" EditMode="EditForms" CommandItemDisplay="Top">
		<Columns>            
			<telerik:GridBoundColumn DataField="DisplayName" HeaderText="Display Name"></telerik:GridBoundColumn>            
			<telerik:GridBoundColumn DataField="DateCreated" HeaderText="Created Date" AllowFiltering="false"></telerik:GridBoundColumn>
			<telerik:GridBoundColumn DataField="DateChanged" HeaderText="Changed Date" AllowFiltering="false"></telerik:GridBoundColumn>
			<telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" HeaderStyle-Width="32"></telerik:GridButtonColumn>
		</Columns>
		<NestedViewTemplate>
			<asp:Panel ID="panel" runat="server" CssClass="CspManageConsumerProfile-permission-subview-container">
				<asp:HiddenField ID="hfpid" runat="server" Value='<%# Eval("PermissionId")  %>' />
				<telerik:RadTabStrip ID="RadTabStrip" runat="server" MultiPageID="RadMultiPage" SelectedIndex="0" ReorderTabsOnSelect="False">
					<Tabs>
						<telerik:RadTab runat="server" Text="Functions" PageViewID="pageFunction"></telerik:RadTab>
					</Tabs>
				</telerik:RadTabStrip>
				<telerik:RadMultiPage ID="MultiPage" runat="server" SelectedIndex="0">
					<telerik:RadPageView ID="pageFunction" runat="server">
						<telerik:RadGrid runat="server" ID="gridFunction" EnableViewState="True"  OnItemCommand="OnFunctionGridItemCommand">
							<MasterTableView AutoGenerateColumns="false" DataKeyNames="FunctionValue" CommandItemDisplay="Top">
								<Columns>
									<telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" HeaderStyle-Width="20px"></telerik:GridButtonColumn>
									<telerik:GridBoundColumn DataField="FunctionText" HeaderText="Function Name"></telerik:GridBoundColumn>
								</Columns>
								<CommandItemSettings AddNewRecordText="Add a new Function" />
								<EditFormSettings EditFormType="WebUserControl">
									<EditColumn ButtonType="ImageButton"></EditColumn>
								</EditFormSettings>
								<NoRecordsTemplate>
									<p>No Functions avaiable.</p>
									<%-- <asp:Literal runat="server" Text='<%# GetLocalizedText("NoFunctionAvaiable") %>'></asp:Literal> --%>
								</NoRecordsTemplate>
							</MasterTableView>
						</telerik:RadGrid>
					</telerik:RadPageView>
				</telerik:RadMultiPage>
			</asp:Panel>
		</NestedViewTemplate>
		<EditFormSettings EditFormType="WebUserControl">
			<EditColumn ButtonType="ImageButton"></EditColumn>
		</EditFormSettings>
	</MasterTableView>
</telerik:RadGrid>

<asp:SqlDataSource ID="dsPermissions" runat="server"></asp:SqlDataSource>
