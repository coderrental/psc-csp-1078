﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewController.ascx.cs" Inherits="CspTelerikManageConsumerProfile.ConsumerProfilerViewController" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Panel ID="navContainer" runat="server">
	<telerik:RadWindowManager ID="RadWindowManager" Behaviors="Move, Close" runat="server" DestroyOnClose="True" />
	<telerik:RadToolBar ID="managerToolBar" runat="server" EnableRoundedCorners="true" EnableShadows="true" Width="100%" CssClass="clear"></telerik:RadToolBar>
	<div class="clear"></div>
	<telerik:RadGrid ID="companyGrid" runat="server" DataSourceID="ds" AllowPaging="true" PageSize="10">
		<MasterTableView AutoGenerateColumns="false" DataKeyNames="companies_Id" AllowFilteringByColumn="true">
			<Columns>
				<telerik:GridBoundColumn DataField="companies_id" HeaderStyle-Width="15px" HeaderText="Id" AllowFiltering="false"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="companyname" HeaderStyle-Width="50%" HeaderText="Company Name" CurrentFilterFunction="Contains"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="city" HeaderText="City" AllowFiltering="false"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="zipcode" HeaderStyle-Width="100px" HeaderText="Zipcode" AllowFiltering="false"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="country" HeaderStyle-Width="125px" HeaderText="Country" AllowFiltering="false"></telerik:GridBoundColumn>            
				<telerik:GridTemplateColumn AllowFiltering="false">
					<ItemTemplate>
						<%# ((bool)Eval("is_supplier")) ? ((bool)Eval("is_consumer") ? "both" : "supplier") : ((bool)Eval("is_consumer") ? "consumer" : "none")%>
					</ItemTemplate>
				</telerik:GridTemplateColumn>
			</Columns>        
		</MasterTableView>    
		<ClientSettings EnableRowHoverStyle="true" EnablePostBackOnRowClick="True">
			<Selecting AllowRowSelect="True" />
		</ClientSettings>
	</telerik:RadGrid>
	<%-- Toolbar --%>
	<br />
	<telerik:RadToolBar ID="toolBar" runat="server" EnableRoundedCorners="true" EnableShadows="true" Width="100%" CssClass="clear"></telerik:RadToolBar>
	<div class="clear"></div>
	<br />

	<%-- Tabs --%>
	<telerik:RadTabStrip runat="server" MultiPageID="pages" ID="tabs" SelectedIndex="-1">
		<Tabs>
			<telerik:RadTab runat="server" PageViewID="pvGeneral" Visible="False"></telerik:RadTab>
			<telerik:RadTab runat="server" PageViewID="pvContacts" Visible="False"></telerik:RadTab>
			<telerik:RadTab runat="server" PageViewID="pvConsumerParameters" Visible="False"></telerik:RadTab>
			<telerik:RadTab runat="server" PageViewID="pvContentSetup" Visible="False"></telerik:RadTab>
			<telerik:RadTab runat="server" PageViewID="pvConsumerProfiles" Visible="False"></telerik:RadTab>
			<telerik:RadTab runat="server" PageViewID="pvConsumer_Parameters" Visible="False"></telerik:RadTab>
		</Tabs>
	</telerik:RadTabStrip>
	<%-- Tab Pages --%>
	<telerik:RadMultiPage runat="server" ID="pages" SelectedIndex="-1">
		<telerik:RadPageView ID="pvGeneral" runat="server">
			<table class="tabContent" cellpadding="2" cellspacing="2">
				<tbody>
					<tr>
						<td>
							<asp:Label ID="label1" runat="server" Text="Company Name"></asp:Label>
						</td>
						<td>
							<asp:HiddenField ID="hfCid" runat="server" />
							<telerik:RadTextBox ID="tbCompanyName" runat="server" Width="400px">
							</telerik:RadTextBox>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Label ID="label2" runat="server" Text="Address 1"></asp:Label>
						</td>
						<td>
							<telerik:RadTextBox ID="tbAddress1" runat="server" Width="400px">
							</telerik:RadTextBox>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Label ID="label3" runat="server" Text="Address 2"></asp:Label>
						</td>
						<td>
							<telerik:RadTextBox ID="tbAddress2" runat="server" Width="400px">
							</telerik:RadTextBox>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Label ID="label6" runat="server" Text="City"></asp:Label>
						</td>
						<td>
							<telerik:RadTextBox ID="tbCity" runat="server" Width="400px">
							</telerik:RadTextBox>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Label ID="label5" runat="server" Text="State"></asp:Label>
						</td>
						<td>
							<telerik:RadTextBox ID="tbState" runat="server">
							</telerik:RadTextBox>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Label ID="label4" runat="server" Text="Zipcode"></asp:Label>
						</td>
						<td>
							<telerik:RadTextBox ID="tbZipcode" runat="server">
							</telerik:RadTextBox>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Label ID="label7" runat="server" Text="Country"></asp:Label>
						</td>
						<td>
							<telerik:RadTextBox ID="tbCountry" runat="server">
							</telerik:RadTextBox>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Label ID="label8" runat="server" Text="Is Supplier"></asp:Label>
						</td>
						<td>
							<telerik:RadButton ID="cbxIsSupplier" runat="server" ButtonType="ToggleButton" ToggleType="CheckBox">
							</telerik:RadButton>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Label ID="label9" runat="server" Text="Is Consumer"></asp:Label>
						</td>
						<td>
							<telerik:RadButton ID="cbxIsConsumer" runat="server" ButtonType="ToggleButton" ToggleType="CheckBox">
							</telerik:RadButton>
						</td>
					</tr>
				</tbody>
			</table>
		</telerik:RadPageView>
		<telerik:RadPageView ID="pvContacts" runat="server">
			<telerik:RadGrid ID="contactsGrid" runat="server" ShowHeader="true" OnNeedDataSource="NeedDataSourceHandler" OnUpdateCommand="GridCommandHandler">
				<MasterTableView AutoGenerateColumns="false" DataKeyNames="companies_contacts_Id,companies_Id" ShowHeader="true" EditMode="EditForms">
					<Columns>
						<telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="ContactEditCmdCol"></telerik:GridEditCommandColumn>
						<telerik:GridBoundColumn DataField="firstname" HeaderText="First Name"></telerik:GridBoundColumn>
						<telerik:GridBoundColumn DataField="middlename"  HeaderText="Middle Name"></telerik:GridBoundColumn>
						<telerik:GridBoundColumn DataField="lastname" HeaderText="Last Name"></telerik:GridBoundColumn>
						<telerik:GridBoundColumn DataField="emailaddress" HeaderText="Email Address"></telerik:GridBoundColumn>
						<telerik:GridBoundColumn DataField="mobile" HeaderText="Mobile"></telerik:GridBoundColumn>
						<telerik:GridBoundColumn DataField="telephone" HeaderText="Telephone"></telerik:GridBoundColumn>
					</Columns>
					<EditFormSettings EditFormType="AutoGenerated"></EditFormSettings>
				</MasterTableView>
			</telerik:RadGrid>        
		</telerik:RadPageView>
		<telerik:RadPageView ID="pvConsumerParameters" runat="server">
			<telerik:RadGrid ID="cpGrid" runat="server" OnNeedDataSource="NeedDataSourceHandler" OnUpdateCommand="GridCommandHandler">
				<MasterTableView AutoGenerateColumns="false" DataKeyNames="companies_consumers_Id,companies_Id,default_language_Id,fallback_language_Id">
					<Columns>
						<telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="CompanyConsumerCmdCol"></telerik:GridEditCommandColumn>
						<telerik:GridTemplateColumn DataField="base_domain" HeaderText="Base Domain">
							<ItemTemplate>
								<%#Eval("base_domain") %>
							</ItemTemplate>
							<EditItemTemplate>
								<asp:TextBox runat="server" ID="tbBaseDomain" Text='<%# Bind("base_domain") %>' Width="400px"></asp:TextBox>
							</EditItemTemplate>
						</telerik:GridTemplateColumn>
						<%--<telerik:GridTemplateColumn DataField="base_publication_Id" HeaderText="Base Publication">
							<ItemTemplate><%#Eval("base_publication_Id")%></ItemTemplate>
						</telerik:GridTemplateColumn>--%>
						<telerik:GridTemplateColumn HeaderText="Base Publication Parameters">
							<ItemTemplate>
								<%# Eval("base_publication_parameters") != DBNull.Value && Eval("base_publication_parameters").ToString().Length > 50 ?  Eval("base_publication_parameters").ToString().Substring(0, 50) + "...": ""%>
							</ItemTemplate>
							<EditItemTemplate>
								<asp:TextBox runat="server" ID="tbBasePublicationParams" Text='<%# Bind("base_publication_parameters") %>' Width="400px"></asp:TextBox>
							</EditItemTemplate>
						</telerik:GridTemplateColumn>
						<telerik:GridTemplateColumn DataField="default_language_Id" HeaderText="Language" UniqueName="DefaultLanguage">
							<ItemTemplate>
								<asp:Label ID="lbDefaultLanguage" runat="server"></asp:Label>
							</ItemTemplate>
							<EditItemTemplate>
								<asp:DropDownList ID="ddlDefaultLanguage" runat="server"></asp:DropDownList>
							</EditItemTemplate>
						</telerik:GridTemplateColumn>                    
						<telerik:GridTemplateColumn DataField="fallback_language_Id" HeaderText="Fallback Language" UniqueName="FallbackLanguage">
							<ItemTemplate>
								<asp:Label ID="lbFallbackLanguage" runat="server" Text='<%#Eval("fallback_language_Id") %>'></asp:Label>
							</ItemTemplate>
							<EditItemTemplate>
								<asp:DropDownList ID="ddlFallbackLanguage" runat="server"></asp:DropDownList>
							</EditItemTemplate>
						</telerik:GridTemplateColumn>
					</Columns>
				</MasterTableView>
			</telerik:RadGrid>
			<telerik:RadToolBar ID="cpToolBar" runat="server" EnableRoundedCorners="true" EnableShadows="true" Width="100%" CssClass="clear"></telerik:RadToolBar>
		</telerik:RadPageView>
		<telerik:RadPageView ID="pvContentSetup" runat="server">
			<telerik:RadGrid ID="contentSetupGrid" runat="server" OnNeedDataSource="NeedDataSourceHandler" OnItemCreated="GridItemCreatedHandler">
				<MasterTableView AutoGenerateColumns="false" DataKeyNames="ThemeId,IsSelected">
					<Columns>                                        
						<telerik:GridTemplateColumn UniqueName="IsThemeSelected">
							<ItemStyle Width="20px" Wrap="False" />
							<ItemTemplate>
								<%-- <telerik:RadButton ID="cbSelectedTheme" runat="server" ButtonType="ToggleButton" ToggleType="CheckBox" OnCheckedChanged="UpdateCompanyTheme" Value="" /> --%>
								<asp:CheckBox runat="server" ID="cbSelectedTheme" Width="50" />
							</ItemTemplate>
						</telerik:GridTemplateColumn>
						<telerik:GridBoundColumn DataField="ThemeName" HeaderText="Theme Name"></telerik:GridBoundColumn>
					</Columns>
				</MasterTableView>
			</telerik:RadGrid>
		</telerik:RadPageView>
		<telerik:RadPageView ID="pvConsumerProfiles" runat="server">
			<telerik:RadGrid ID="consumerProfileGrid" runat="server" OnNeedDataSource="NeedDataSourceHandler" OnItemCommand="OnCpGridItemCommand" OnUpdateCommand="GridCommandHandler" PageSize="50" AllowPaging="true">
				<MasterTableView AutoGenerateColumns="false" ShowHeader="true" EditMode="EditForms" DataKeyNames="ConsumerThemeId,ThemeId,ThemeName,SupplierId,SupplierName,CategoryId,CategoryName" CommandItemDisplay="Top" AllowFilteringByColumn="True">
					<Columns>
						<telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="ConsumerProfileCmdCol"></telerik:GridEditCommandColumn>
						<telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" HeaderStyle-Width="32"></telerik:GridButtonColumn>
						<telerik:GridTemplateColumn DataField="ThemeName" HeaderText="Theme" UniqueName="CpThemeName" AllowFiltering="True">
							<ItemTemplate>
								<asp:Label ID="Label10" runat="server" Text='<%#Eval("ThemeName") %>'></asp:Label>
							</ItemTemplate>
							<EditItemTemplate>
								<telerik:RadComboBox ID="ddCpTheme" runat="server" AutoPostBack="True" Width="200" OnSelectedIndexChanged="OnCpThemeSelectedIndexChanged"></telerik:RadComboBox>
							</EditItemTemplate>
						</telerik:GridTemplateColumn>
						<telerik:GridTemplateColumn DataField="SupplierName" HeaderText="Supplier" UniqueName="CpSupplierName" AllowFiltering="True">
							<ItemTemplate>
								<asp:Label ID="lblCpSupplier" runat="server" Text='<%#Eval("SupplierName") + " (" + Eval("SupplierId") + ")"%>'></asp:Label>
							</ItemTemplate>
							<EditItemTemplate>
								<telerik:RadComboBox ID="ddCpSupplier" runat="server" Width="300"></telerik:RadComboBox>
							</EditItemTemplate>
						</telerik:GridTemplateColumn>                    
						<telerik:GridTemplateColumn DataField="CategoryName" HeaderText="Category" UniqueName="CpCategoryName" AllowFiltering="True">
							<ItemTemplate>
								<asp:Label ID="lblCpCategory" runat="server" Text='<%#Eval("CategoryName") + " (" + Eval("CategoryId") + ")"%>'></asp:Label>
							</ItemTemplate>
							<EditItemTemplate>
								<telerik:RadComboBox ID="ddCpCategory" runat="server" Width="400"></telerik:RadComboBox>
							</EditItemTemplate>
						</telerik:GridTemplateColumn>
					</Columns>
					<EditFormSettings EditFormType="AutoGenerated">
						<EditColumn ButtonType="ImageButton"></EditColumn>
					</EditFormSettings>
				</MasterTableView>
			</telerik:RadGrid>
		</telerik:RadPageView>
		<telerik:RadPageView ID="pvConsumer_Parameters" runat="server">
			<telerik:RadGrid ID="consumerParameterGrid" runat="server" OnNeedDataSource="NeedDataSourceHandler" OnItemCommand="OnCpGridItemCommand" OnUpdateCommand="GridCommandHandler" PageSize="50" AllowPaging="true">
				<MasterTableView AutoGenerateColumns="false" ShowHeader="true" DataKeyNames="Id,CId,Name,Value" CommandItemDisplay="None">
					<Columns>
						<telerik:GridBoundColumn DataField="Name" HeaderText="Field Name" UniqueName="CpgFieldName" />
						<telerik:GridTemplateColumn DataField="Value" HeaderText="Value" UniqueName="CpgValue">
							<ItemTemplate>
								<telerik:RadTextBox ID="txtCpgValue" runat="server" Text='<%# Bind("Value") %>' Width="500">
								</telerik:RadTextBox>
							</ItemTemplate>
						</telerik:GridTemplateColumn>                    
					</Columns>
				</MasterTableView>
			</telerik:RadGrid>
		</telerik:RadPageView>
	</telerik:RadMultiPage>
	
	<asp:Literal ID="log" runat="server" Visible="False"></asp:Literal>
</asp:Panel>

<asp:Panel ID="navErrorContainer" runat="server" Visible="false" Enabled="false" />

<asp:SqlDataSource ID="ds" runat="server"></asp:SqlDataSource>

<telerik:RadNotification ID="commandNotification" runat="server" Width="300" Height="80"
	Animation="Fade" EnableRoundedCorners="True" VisibleTitlebar="False" AutoCloseDelay="9000" OffsetX="-25" OffsetY="-65" Position="BottomRight" ContentScrolling="Default">
</telerik:RadNotification>