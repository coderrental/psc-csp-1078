﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CspTelerikManageConsumerProfile.Components
{
	public class ConsumerTheme
	{
		private int _consumerThemeId;
		public int ConsumerThemeId
		{
			get { return _consumerThemeId; }
			set { _consumerThemeId = value; }
		}

		private int _themeId;
		public int ThemeId
		{
			get { return _themeId; }
			set { _themeId = value; }
		}

		private string _themeName;
		public string ThemeName
		{
			get { return _themeName; }
			set { _themeName = value; }
		}

		private int _supplierId;
		public int SupplierId
		{
			get { return _supplierId; }
			set { _supplierId = value; }
		}

		private string _supplierName;
		public string SupplierName
		{
			get { return _supplierName; }
			set { _supplierName = value; }
		}

		private int _categoryId;
		public int CategoryId
		{
			get { return _categoryId; }
			set { _categoryId = value; }
		}

		private string _categoryName;
		public string CategoryName
		{
			get { return _categoryName; }
			set { _categoryName = value; }
		}
	}
}