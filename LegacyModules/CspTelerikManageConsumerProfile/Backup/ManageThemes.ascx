﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageThemes.ascx.cs" Inherits="CspTelerikManageConsumerProfile.ManageThemes" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadToolBar ID="toolBar" OnButtonClick="toolBar_ButtonClick" runat="server" EnableRoundedCorners="true" EnableShadows="true" Width="100%"></telerik:RadToolBar>
<telerik:RadGrid ID="MngThemeGrid" runat="server" DataSourceID="dsThemes" Width="100%" GridLines="None"
  AutoGenerateColumns="False" PageSize="13" AllowSorting="True" AllowPaging="True" AllowAutomaticDeletes="True" OnItemCommand="MngThemeGrid_ItemCommand"
  OnInsertCommand="MngThemeGrid_InsertCommand" OnUpdateCommand="MngThemeGrid_UpdateCommand" OnDeleteCommand="MngThemeGrid_DeleteCommand" ShowStatusBar="true"
	OnItemDataBound="MngThemeGrid_ItemDataBound">
	<%-- <PagerStyle Mode="NumericPages"></PagerStyle> --%>
  <MasterTableView DataKeyNames="themes_Id" AllowMultiColumnSorting="True" Width="100%" EditMode="EditForms" CommandItemDisplay="Top">
  	<DetailTables>
			<telerik:GridTableView DataKeyNames="ThemeSupplierId,CompanyId" DataSourceID="dsThemesSuppliers" Width="100%"
				runat="server" CommandItemDisplay="Top" Name="ThemesSuppliers" EditMode="EditForms">
				<ParentTableRelation>
						<telerik:GridRelationFields DetailKeyField="themes_Id" MasterKeyField="themes_Id" />
				</ParentTableRelation>
				<Columns>
					<telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="ThemeSupplierCmdCol">
						<HeaderStyle Width="20px" />
					</telerik:GridEditCommandColumn>
					<telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" HeaderStyle-Width="20px"></telerik:GridButtonColumn>
					<telerik:GridBoundColumn SortExpression="ThemeId" HeaderText="Theme" HeaderButtonType="TextButton" DataField="ThemeId" ReadOnly="True" Visible="False" UniqueName="MngThemeName">
					</telerik:GridBoundColumn>
					<telerik:GridTemplateColumn DataField="CompanyId" HeaderText="Company" UniqueName="MngCompanyName">
						<ItemTemplate>
							<asp:Label ID="lbltsCompanyName" runat="server" Text='<%# Eval("CompanyName") %>'></asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<telerik:RadComboBox ID="ddtsCompany" runat="server" AppendDataBoundItems="True" CheckBoxes="True" EnableCheckAllItemsCheckBox="True" DataSourceID="dsCompanies"
								DataTextField="companyname" DataValueField="companies_Id" SelectedValue='<%# Bind("CompanyId")%>' Width="400" MaxHeight="500"></telerik:RadComboBox>
						</EditItemTemplate>
					</telerik:GridTemplateColumn>
				</Columns>
				<EditFormSettings>
					<FormTableItemStyle Width="100%" Height="29px"></FormTableItemStyle>
					<FormTableStyle GridLines="None" CellSpacing="0" CellPadding="2"></FormTableStyle>
					<FormStyle Width="100%" BackColor="#eef2ea"></FormStyle>
					<EditColumn ButtonType="ImageButton" />
				</EditFormSettings>
			</telerik:GridTableView>

			<telerik:GridTableView DataKeyNames="ThemeCategoryId,CategoryId" DataSourceID="dsThemesCategories" Width="100%"
				runat="server" CommandItemDisplay="Top" Name="ThemesCategories" EditMode="EditForms">
				<ParentTableRelation>
						<telerik:GridRelationFields DetailKeyField="themes_Id" MasterKeyField="themes_Id" />
				</ParentTableRelation>
				<Columns>
					<telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="ThemeCategoryCmdCol">
						<HeaderStyle Width="20px" />
					</telerik:GridEditCommandColumn>
					<telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" HeaderStyle-Width="20px"></telerik:GridButtonColumn>
					<telerik:GridBoundColumn SortExpression="ThemeId" HeaderText="Theme" HeaderButtonType="TextButton" DataField="ThemeId" ReadOnly="True" Visible="False" UniqueName="MngTcThemeName">
					</telerik:GridBoundColumn>
					<telerik:GridTemplateColumn DataField="CategoryId" HeaderText="Category" UniqueName="MngCategoryName">
						<ItemTemplate>
							<asp:Label ID="lbltcCategoryName" runat="server" Text='<%# Eval("CategoryName") %>'></asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<%-- <telerik:RadComboBox ID="ddtcCategory" runat="server" AppendDataBoundItems="True" DataSourceID="dsCategories" DataTextField="categoryText" DataValueField="categoryId" SelectedValue='<%# Bind("CategoryId")%>' Width="400" MaxHeight="500"></telerik:RadComboBox> --%>
							<telerik:RadComboBox ID="ddtcCategory" runat="server" ShowToggleImage="True" AutoPostBack="True" ExpandAnimation-Type="None" CollapseAnimation-Type="None" OnClientDropDownOpened="ddtcCategory_OnClientDropDownOpenedHandler" OnClientDropDownClosed="ddtcCategory_OnClientDropDownClosedHandler" Width="500" MaxHeight="500">
								<ItemTemplate>
									<telerik:RadTreeView ID="categoryTree" runat="server" AppendDataBoundItems="True" CheckBoxes="True" DataSourceID="dsCategories" DataTextField="categoryName" DataValueField="CategoryId" DataFieldID="categoryId" DataFieldParentID="parentId"
										OnClientNodeChecked="categoryTree_ClientNodeChecked">
										<DataBindings>
											<telerik:RadTreeNodeBinding Expanded="True" ValueField="CategoryId" />
										</DataBindings>
									</telerik:RadTreeView>
								</ItemTemplate>
								<Items>
									<telerik:RadComboBoxItem Text="" />
								</Items>
							</telerik:RadComboBox>
						</EditItemTemplate>
					</telerik:GridTemplateColumn>
				</Columns>
				<EditFormSettings>
					<FormTableItemStyle Width="100%" Height="29px"></FormTableItemStyle>
					<FormTableStyle GridLines="None" CellSpacing="0" CellPadding="2"></FormTableStyle>
					<FormStyle Width="100%" BackColor="#eef2ea"></FormStyle>
					<EditColumn ButtonType="ImageButton" />
				</EditFormSettings>
			</telerik:GridTableView>
		</DetailTables>
		<Columns>
			<telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
				<HeaderStyle Width="20px" />
			</telerik:GridEditCommandColumn>
			<telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" HeaderStyle-Width="20px"></telerik:GridButtonColumn>
			<telerik:GridTemplateColumn UniqueName="Description" SortExpression="description" HeaderText="Theme Name" DataField="description">
				<ItemTemplate>
					<asp:Label ID="lblMngThemeName" runat="server" Text='<%#Eval("description")%>'></asp:Label>
				</ItemTemplate>
				<EditItemTemplate>
					<telerik:RadTextBox ID="txtMngTheme" runat="server" Width="300" MaxHeight="500" Text='<%#Eval("description")%>'></telerik:RadTextBox>
				</EditItemTemplate>
			</telerik:GridTemplateColumn>
			<telerik:GridTemplateColumn UniqueName="Active" DataField="active" HeaderText="Active" DataType="System.Boolean">
				<ItemTemplate>
					<telerik:RadButton ID="cbxMngThemeActiveView" runat="server" Enabled="False" ButtonType="ToggleButton" ToggleType="CheckBox" Checked='<%# (bool)Eval("active") %>' />
				</ItemTemplate>
				<EditItemTemplate>
					<telerik:RadButton ID="cbxMngThemeActive" runat="server" ButtonType="ToggleButton" ToggleType="CheckBox" Checked='<%# (string.IsNullOrEmpty(Eval("active").ToString())) ? false : (bool)Eval("active") %>' />
				</EditItemTemplate>
			</telerik:GridTemplateColumn>
		</Columns>
		<EditFormSettings>
			<FormTableItemStyle Width="100%" Height="29px"></FormTableItemStyle>
			<FormTableStyle GridLines="None" CellSpacing="0" CellPadding="2"></FormTableStyle>
			<FormStyle Width="100%" BackColor="#eef2ea"></FormStyle>
			<EditColumn ButtonType="ImageButton" />
		</EditFormSettings>
  </MasterTableView>
</telerik:RadGrid>

<asp:SqlDataSource ID="dsThemes" runat="server"
	DeleteCommand="delete from [themes] where themes.[themes_Id] = @themes_Id">
	<DeleteParameters>
		<asp:Parameter Name="themes_Id" Type="Int32" />
	</DeleteParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="dsCompanies" runat="server"></asp:SqlDataSource>
<asp:SqlDataSource ID="dsCategories" runat="server"></asp:SqlDataSource>
<asp:SqlDataSource ID="dsThemesSuppliers" runat="server"
	DeleteCommand="DELETE FROM [themes_suppliers] WHERE themes_suppliers.[themes_suppliers_Id] = @ThemeSupplierId"
	SelectCommand="select ths.[themes_suppliers_Id] as [ThemeSupplierId], ths.[themes_Id] as [ThemeId], th.[description] as [ThemeName], ths.[companies_Id] as [CompanyId], c.[companyname] as [CompanyName]
from [themes] th join [themes_suppliers] ths on th.[themes_Id] = ths.[themes_Id]
	join [companies] c on ths.[companies_Id] = c.[companies_Id]
	and ths.[themes_Id] = @themes_Id
order by [ThemeName], [CompanyName]">
	<SelectParameters>
			<asp:Parameter Name="themes_Id" Type="Int32" />
	</SelectParameters>
	<DeleteParameters>
		<asp:Parameter Name="ThemeSupplierId" DbType="Int32" />
	</DeleteParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="dsThemesCategories" runat="server"
	DeleteCommand="DELETE FROM [themes_categories] WHERE themes_categories.[themes_categories_Id] = @ThemeCategoryId"
	SelectCommand="select thc.[themes_categories_Id] as [ThemeCategoryId], thc.[themes_Id] as [ThemeId], th.[description] as [ThemeName], thc.[category_Id] as [CategoryId], case when (c.[description] is NULL or c.[description] like '') then c.[categoryText] else c.[description] end as [CategoryName]
from [themes] th join [themes_categories] thc on th.[themes_Id] = thc.[themes_Id]
	join [categories] c on thc.[category_Id] = c.[categoryId]
	and thc.[themes_Id] = @themes_Id
order by [ThemeName]">
	<SelectParameters>
			<asp:Parameter Name="themes_Id" Type="Int32" />
	</SelectParameters>
	<DeleteParameters>
		<asp:Parameter Name="ThemeCategoryId" DbType="Int32" />
	</DeleteParameters>
</asp:SqlDataSource>

<telerik:RadNotification ID="commandNotification" runat="server" Width="300" Height="80"
	Animation="Fade" EnableRoundedCorners="True" VisibleTitlebar="False" AutoCloseDelay="9000" OffsetX="-25" OffsetY="-65" Position="BottomRight" ContentScrolling="Default">
</telerik:RadNotification>

<telerik:RadScriptBlock runat="server">
	<script type="text/javascript">
		function ddtcCategory_OnClientDropDownOpenedHandler(sender, eventArgs) {
			var tree = sender.get_items().getItem(0).findControl("categoryTree");
			var selectedNode = tree.get_selectedNode();
			if (selectedNode) {
				selectedNode.get_element().scrollIntoView();
			}
		}

		function ddtcCategory_OnClientDropDownClosedHandler(sender, eventArgs) {
			var tree = sender.get_items().getItem(0).findControl("categoryTree");
			if (tree != null) {
				var selectedNode = tree.get_selectedNode();
				if (selectedNode != null) selectedNode.set_selected(false);
				var nodes = tree.get_checkedNodes();
				if (nodes.length > 0) sender.set_text('<%= GetLocalizedText("MultipleCategoriesSelected") %>');
				else {
					if (selectedNode != null) {
						selectedNode.set_selected(true);
						sender.set_text(selectedNode.get_text());
					}
				}
			}
		}

		function categoryTree_ClientNodeChecked(sender, eventArgs) {
			var node = eventArgs.get_node();
			if (node.get_level() != 0) {
				categoryTree_UpdateAllParent(node.get_parent(), node.get_checked());
				var parentNode = node.get_parent();
				parentNode.set_checked(node.get_checked());
			}
			var childNodes = eventArgs.get_node().get_nodes();
			if (childNodes.get_count() > 0) {
				var isChecked = eventArgs.get_node().get_checked();
				categoryTree_UpdateAllChildren(childNodes, isChecked);
			}
		}
		
		function categoryTree_UpdateAllParent(node, checked) {
			if (node.get_level() != 0) categoryTree_UpdateAllParent(node.get_parent(), checked);
			else node.set_checked(checked);
		}

		function categoryTree_UpdateAllChildren(nodes, checked) {
			var i;
			for (i = 0; i < nodes.get_count(); i++) {
				nodes.getNode(i).set_checked(checked);
				if (nodes.getNode(i).get_nodes().get_count() > 0) {
					categoryTree_UpdateAllChildren(nodes.getNode(i).get_nodes(), checked);
				}
			}
		}
	</script>
</telerik:RadScriptBlock>