﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CspTelerikManageConsumerProfile.Utils
{
	public class Cat
	{
		public Nullable<int> CategoryId { get; set; }
		public Nullable<int> ParentId { get; set; }
		public string CategoryName { get; set; }

		public Cat()
		{
			CategoryId = null;
			ParentId = null;
			CategoryName = null;
		}

		public Cat(Nullable<int> id, Nullable<int> parentId, string text)
		{
			CategoryId = id;
			ParentId = parentId;
			CategoryName = text;
		}
	}
}