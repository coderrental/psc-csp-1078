﻿using System.Collections.Generic;
using DotNetNuke.Services.Localization;

namespace CspTelerikManageConsumerProfile.Utils
{
	public class FuncsMan
	{
		private string _localResourceFile;

		private List<Func> _funcs;
		public List<Func> Funcs
		{
			get { return _funcs; }
		}

		public FuncsMan(string localResourceFile)
		{
			_localResourceFile = localResourceFile;

			_funcs = new List<Func>();
			_funcs.Add(new Func("bAddNewSupplier", GetLocalizedText("Label.AddNewSupplier")));
			_funcs.Add(new Func("bManageCategories", GetLocalizedText("Label.ManageCategories")));
			_funcs.Add(new Func("bManageThemes", GetLocalizedText("Label.ManageThemes")));
			_funcs.Add(new Func("bManageConsumersThemes", GetLocalizedText("Label.ManageConsumersThemes")));
			_funcs.Add(new Func("tGeneral", GetLocalizedText("Label.Tab.General")));
			_funcs.Add(new Func("tContacts", GetLocalizedText("Label.Tab.Contact")));
			_funcs.Add(new Func("tConsumer", GetLocalizedText("Label.Tab.Consumer")));
			_funcs.Add(new Func("tThemes", GetLocalizedText("Label.Tab.Theme")));
			_funcs.Add(new Func("tConsumerProfile", GetLocalizedText("Label.Tab.ConsumerProfile")));
			_funcs.Add(new Func("tConsumerParameters", GetLocalizedText("Label.Tab.ConsumerParameter")));
		}

		private string GetLocalizedText(string key)
		{
			return Localization.GetString(key, _localResourceFile);
		}

		public IEnumerator<Func> GetEnumerator()
		{
			return _funcs.GetEnumerator();
		}
	}
}