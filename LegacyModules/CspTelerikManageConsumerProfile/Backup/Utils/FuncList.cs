﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CspTelerikManageConsumerProfile.Utils
{
	public class FuncList
	{
		private List<Func> _funcs;
		public List<Func> Funcs
		{
			get { return _funcs; }
		}

		public FuncList()
		{
			_funcs = new List<Func>();
		}

		public IEnumerator<Func> GetEnumerator()
		{
			return _funcs.GetEnumerator();
		}

		public void Add(Func func)
		{
			_funcs.Add(func);
		}
	}
}