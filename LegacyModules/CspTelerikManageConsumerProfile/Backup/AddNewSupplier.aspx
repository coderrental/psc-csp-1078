﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddNewSupplier.aspx.cs" Inherits="TIEKinetix.CspModules.CspTelerikManageConsumerProfile.AddNewSupplier" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI, Version=2012.2.703.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title></title>
		<link href="Site.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<form id="form1" runat="server">
			<div class="main-container">
				<table cellpadding="0" cellspacing="5" width="100%">
					<tr>
						<td align="right" width="40%"><asp:Literal runat="server" ID="lSupplierName" /></td>
						<td align="left" width="60%"><asp:TextBox id="txtSupplierName" runat="server" Width="200px" /></td>
					</tr>
					<tr>
						<td align="right" width="40%"><asp:Literal runat="server" ID="lDisplayName" /></td>
						<td align="left" width="60%"><asp:TextBox id="txtDisplayName" runat="server" Width="200px" /></td>
					</tr>
					<tr>
						<td align="right" width="40%"><asp:Literal runat="server" ID="lAddress1" /></td>
						<td align="left" width="60%"><asp:TextBox id="txtAddress1" runat="server" Width="400px" /></td>
					</tr>
					<tr>
						<td align="right" width="40%"><asp:Literal runat="server" ID="lAddress2" /></td>
						<td align="left" width="60%"><asp:TextBox id="txtAddress2" runat="server" Width="400px" /></td>
					</tr>
					<tr>
						<td align="right" width="40%"><asp:Literal runat="server" ID="lCity" /></td>
						<td align="left" width="60%"><asp:TextBox id="txtCity" runat="server" Width="200px" /></td>
					</tr>
					<tr>
						<td align="right" width="40%"><asp:Literal runat="server" ID="lState" /></td>
						<td align="left" width="60%"><asp:TextBox id="txtState" runat="server" Width="100px" /></td>
					</tr>
					<tr>
						<td align="right" width="40%"><asp:Literal runat="server" ID="lZipCode" /></td>
						<td align="left" width="60%"><asp:TextBox id="txtZipCode" runat="server" Width="100px" /></td>
					</tr>
					<tr>
						<td align="right" width="40%"><asp:Literal runat="server" ID="lCountry" /></td>
						<td align="left" width="60%"><asp:TextBox id="txtCountry" runat="server" Width="200px" /></td>
					</tr>
					<tr>
						<td align="right" width="40%"></td>
						<td align="left" width="60%"><asp:Button ID="btnSubmit" runat="server" onclick="AddNewSupplier_Click" /></td>
					</tr>
				</table>
				<p>
					<asp:Label ID="lblOutMsg" runat="server"></asp:Label>
				</p>
			</div>
		</form>
	</body>
</html>
