﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CspTelerikManageConsumerProfile.Utils
{
	public class Func
	{
		private string _value;
		public string Value
		{
			get { return _value; }
			set { _value = value; }
		}

		private string _name;
		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}

		public Func(string value, string name)
		{
			_value = value;
			_name = name;
		}
	}
}