﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CspTelerikManageConsumerProfile.Utils
{
	public class CatList
	{
		private List<Cat> _cats;
		public List<Cat> Cats
		{
			get { return _cats; }
		}

		public CatList()
		{
			_cats = new List<Cat>();
		}

		public IEnumerator<Cat> GetEnumerator()
		{
			return _cats.GetEnumerator();
		}

		public void Add(Cat cat)
		{
			_cats.Add(cat);
		}
	}
}