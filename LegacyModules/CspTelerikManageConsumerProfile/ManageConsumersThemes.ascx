﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageConsumersThemes.ascx.cs" Inherits="CspTelerikManageConsumerProfile.ManageConsumersThemes" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadToolBar ID="toolBar" OnButtonClick="toolBar_ButtonClick" runat="server" EnableRoundedCorners="true" EnableShadows="true" Width="100%"></telerik:RadToolBar>
<table width="100%" cellpadding="5" cellspacing="5">
	<tr>
		<td align="right" width="30%"><asp:Literal runat="server" ID="lThemeName" /></td>
		<td align="left" width="70%">
			<telerik:RadComboBox ID="ddtsTheme" runat="server" AppendDataBoundItems="True" DataSourceID="dsThemes"
				DataTextField="description" DataValueField="themes_Id" Width="400" MaxHeight="500">
			</telerik:RadComboBox>
		</td>
	</tr>
</table>
<table width="100%" cellpadding="5" cellspacing="5">
	<tr>
		<td align="center" width="100%">
			<telerik:RadButton ID="btnGetDataBindToTheme" runat="server" onclick="GetDataBindToTheme_Click" >
			    <Icon PrimaryIconCssClass="rbRefresh" PrimaryIconLeft="4" PrimaryIconTop="4" />
			</telerik:RadButton>
		</td>
	</tr>
</table>
<div id="manage_consumer_theme_wrap">
    <telerik:RadSplitter runat="server" ID="splitter" Width="100%" Height="500px">
    <telerik:RadPane runat="server" Width="32%">
        <asp:Literal runat="server" ID="lSuppliersName" /><br />
		<telerik:RadListBox ID="lbxSuppliersList" runat="server" CheckBoxes="true" onclientitemchecked="onItemChecked" Width="100%"></telerik:RadListBox>
    </telerik:RadPane>
    <telerik:RadSplitBar runat="server" CollapseMode="None"></telerik:RadSplitBar>
    <telerik:RadPane runat="server" Width="32%">
        <asp:Literal runat="server" ID="lCategoriesName" /><br />
		<telerik:RadTreeView ID="categoryTree" runat="server" AppendDataBoundItems="True" CheckBoxes="True" TriStateCheckBoxes="True" Width="100%" Height="480px" DataTextField="CategoryName"
			DataValueField="CategoryId" DataFieldID="CategoryId" DataFieldParentID="ParentId" CssClass="category-tree">
			<DataBindings>
				<telerik:RadTreeNodeBinding Expanded="True" ValueField="CategoryId" />
			</DataBindings>
		</telerik:RadTreeView>
    </telerik:RadPane>
    <telerik:RadSplitBar runat="server" CollapseMode="None"></telerik:RadSplitBar>
    <telerik:RadPane runat="server" Width="32%">
        <asp:Literal runat="server" ID="lConsumersName" /><br />
	    <telerik:RadListBox ID="lbxConsumersList" runat="server" CheckBoxes="true" onclientitemchecked="onItemChecked" Width="100%"></telerik:RadListBox>
    </telerik:RadPane>
</telerik:RadSplitter>
<table width="100%" cellpadding="5" cellspacing="5">
	<tr>
		<td align="center" width="100%">
			<telerik:RadButton ID="btnAddWantedData" runat="server" onclick="AddWantedData_Click">
			    <Icon PrimaryIconCssClass="rbOk" PrimaryIconLeft="4" PrimaryIconTop="4"/>
			</telerik:RadButton>
		</td>
	</tr>
</table>
</div>


<asp:SqlDataSource ID="dsThemes" runat="server"></asp:SqlDataSource>

<telerik:RadNotification ID="commandNotification" runat="server" Width="300" Height="80"
	Animation="Fade" EnableRoundedCorners="True" VisibleTitlebar="False" AutoCloseDelay="9000" OffsetX="-25" OffsetY="-65" Position="BottomRight" ContentScrolling="Default">
</telerik:RadNotification>

<telerik:RadScriptBlock runat="server">
	<script type="text/javascript">
		function onItemChecked(sender, e) {
			var item = e.get_item();
			var items = sender.get_items();
			var checked = item.get_checked();
			var firstItem = sender.getItem(0);
			if (item.get_text() == "All") {
				items.forEach(function (itm) { itm.set_checked(checked); });
			}
			else {
				if (sender.get_checkedItems().length == items.get_count() - 1) {
					firstItem.set_checked(!firstItem.get_checked());
				}
			}
		}
	</script>
</telerik:RadScriptBlock>