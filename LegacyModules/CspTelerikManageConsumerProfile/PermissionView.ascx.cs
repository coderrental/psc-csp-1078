﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using CspTelerikManageConsumerProfile.Components;
using CspTelerikManageConsumerProfile.Utils;
using DesktopModules.CSPModules.CspCommons;
using DotNetNuke.Common.Utilities;
using Telerik.Web.UI;

namespace CspTelerikManageConsumerProfile
{
	public partial class PermissionView : ModuleBase
	{
		protected override void Page_Init(object sender, EventArgs e)
		{
			base.Page_Init(sender, e);
			dsPermissions.ConnectionString = Config.GetConnectionString();
			dsPermissions.SelectCommand = "select a.PermissionId, c.DisplayName, a.DateCreated, a.DateChanged " +
																		"from ManagerComsumerProfile_Permission a join UserPortals b on b.PortalId = a.PortalId and b.UserId = a.UserId " +
																		"join Users c on c.UserID = a.UserId";
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			permissionGrid.MasterTableView.CommandItemSettings.AddNewRecordText = GetLocalizedText("AddNewUser");
			permissionGrid.MasterTableView.NoMasterRecordsText = GetLocalizedText("NoUserAvaiable");
			permissionGrid.MasterTableView.NoDetailRecordsText = GetLocalizedText("NoUserAvaiable");

			Control control = permissionGrid.MasterTableView.FindControl("RadMultiPage");
		}

		protected void OnGridPermissionItemCreated(object sender, GridItemEventArgs e)
		{
			if (e.Item is GridNestedViewItem)
			{
				RadGrid grid = (sender as RadGrid);
				GridNestedViewItem item = e.Item as GridNestedViewItem;
				if (item.ParentItem.GetDataKeyValue("PermissionId") == null) return;

				Guid id = (Guid)item.ParentItem.GetDataKeyValue("PermissionId");

				RadGrid gridFunction = (RadGrid)item.FindControl("gridFunction");
				gridFunction.DataSource = _dnnContext.ManagerComsumerProfile_Functions.Where(a => a.PermissionId == id);
			}
		}

		protected void OnGridPermissionItemCommand(object sender, GridCommandEventArgs e)
		{
			if (e.CommandName == RadGrid.InitInsertCommandName)
			{
				e.Canceled = true;

				((RadGrid)e.Item.OwnerTableView.NamingContainer).EditIndexes.Clear();
				e.Item.OwnerTableView.EditFormSettings.UserControlName = ControlPath + "AddNewPermission.ascx";

				var items = _dnnContext.ExecuteQuery<User>(@"
					select distinct u.*
					from Users u
						join UserPortals up on u.UserID = up.UserId
					where up.PortalId = {0} and (u.UserID not in (
						select urs.UserID
						from Users urs
							join UserRoles urrls on urs.UserID = urrls.UserID
							join Roles rls on urrls.RoleID = rls.RoleID
						where rls.RoleName = 'Administrators')) and (not exists (
						select 1
						from ManagerComsumerProfile_Permission mp
						where mp.PortalId = up.PortalId and mp.UserId = up.UserId)) and up.IsDeleted = 0", PortalId).ToList();
				e.Item.OwnerTableView.InsertItem(items);
			}
			else if (e.CommandName == RadGrid.PerformInsertCommandName)
			{
				GridEditableItem insertedItem = e.Item.OwnerTableView.GetInsertItem();
				UserControl userControl = insertedItem.FindControl(GridEditFormItem.EditFormUserControlID) as UserControl;
				if (userControl == null) return;

				RadComboBox userCombo = (RadComboBox) userControl.FindControl("cbUsers");
				if (userCombo != null)
				{
					var collection = userCombo.CheckedItems;
					foreach (var userItem in collection)
					{
						int id = -1;
						if (int.TryParse(userItem.Value, out id))
						{
							_dnnContext.ManagerComsumerProfile_Permissions.InsertOnSubmit(new ManagerComsumerProfile_Permission
							{
								PermissionId = Guid.NewGuid(),
								Active = true,
								DateCreated = DateTime.Now,
								DateChanged = DateTime.Now,
								PortalId = PortalId,
								TabModuleId = TabModuleId,
								UserId = id
							});
						}
					}
					_dnnContext.SubmitChanges();
				}
			}
			else if (e.CommandName == RadGrid.DeleteCommandName)
			{
				Guid id = new Guid(((GridEditableItem)e.Item).GetDataKeyValue("PermissionId").ToString());

				//_dnnContext.InlineContentEntry_Suppliers.DeleteAllOnSubmit(_dnnContext.InlineContentEntry_Suppliers.Where(a => a.PermissionId == id));
				//_dnnContext.InlineContentEntry_Languages.DeleteAllOnSubmit(_dnnContext.InlineContentEntry_Languages.Where(a => a.PermissionId == id));
				_dnnContext.ManagerComsumerProfile_Functions.DeleteAllOnSubmit(_dnnContext.ManagerComsumerProfile_Functions.Where(a => a.PermissionId == id));
				_dnnContext.ManagerComsumerProfile_Permissions.DeleteOnSubmit(_dnnContext.ManagerComsumerProfile_Permissions.First(a => a.PermissionId == id));
				_dnnContext.SubmitChanges();
				permissionGrid.DataBind();
			}
		}

		protected void OnFunctionGridItemCommand(object sender, GridCommandEventArgs e)
		{
			if (e.CommandName == RadGrid.InitInsertCommandName)
			{
				e.Canceled = true;

				((RadGrid)e.Item.OwnerTableView.NamingContainer).EditIndexes.Clear();
				e.Item.OwnerTableView.EditFormSettings.UserControlName = ControlPath + "AddNewFunction.ascx";
				string permissionId = ((HiddenField)e.Item.OwnerTableView.OwnerGrid.Parent.FindControl("hfpid")).Value;
				Guid id = new Guid(permissionId);

				FuncsMan funcsMan = new FuncsMan(ResourceFilePath);
				var existFuncs = _dnnContext.ManagerComsumerProfile_Functions.Where(a => a.PermissionId == id);
				List<Func> items = new List<Func>();

				foreach (Func func in funcsMan)
				{
					bool bFound = false;
					foreach (ManagerComsumerProfile_Function eFunc in existFuncs)
					{
						if(eFunc.FunctionValue == func.Value)
						{
							bFound = true;
							break;
						}
					}
					if(!bFound) items.Add(func);
				}
				e.Item.OwnerTableView.InsertItem(items);
			}
			else if (e.CommandName == RadGrid.PerformInsertCommandName)
			{
				GridEditableItem insertedItem = e.Item.OwnerTableView.GetInsertItem();
				UserControl userControl = insertedItem.FindControl(GridEditFormItem.EditFormUserControlID) as UserControl;
				if (userControl == null) return;

				Guid permissionId = new Guid(((HiddenField)e.Item.OwnerTableView.OwnerGrid.Parent.FindControl("hfpid")).Value);

				RadComboBox funcCombo = (RadComboBox) userControl.FindControl("cbFunctions");
				if (funcCombo != null)
				{
					var collection = funcCombo.CheckedItems;
					foreach (var funcItem in collection)
					{
						var findItem = _dnnContext.ManagerComsumerProfile_Functions.SingleOrDefault(a => a.PermissionId == permissionId && a.FunctionValue == funcItem.Value);
						if (findItem == null)
						{
							_dnnContext.ManagerComsumerProfile_Functions.InsertOnSubmit(new ManagerComsumerProfile_Function
							{
								Id = Guid.NewGuid(),
								PermissionId = permissionId,
								FunctionValue = funcItem.Value,
								FunctionText = funcItem.Text
							});
						}
					}
					_dnnContext.SubmitChanges();
					UpdateFunctionGrid(permissionId, e);
				}
			}
			else if (e.CommandName == RadGrid.DeleteCommandName)
			{
				GridEditableItem item = (GridEditableItem)e.Item;
				Guid permissionId = new Guid(((HiddenField)e.Item.OwnerTableView.OwnerGrid.Parent.FindControl("hfpid")).Value);
				string functionValue = item.GetDataKeyValue("FunctionValue").ToString();

				if (!string.IsNullOrEmpty(functionValue))
				{
					_dnnContext.ManagerComsumerProfile_Functions.DeleteAllOnSubmit(_dnnContext.ManagerComsumerProfile_Functions.Where(a => a.PermissionId == permissionId && a.FunctionValue == functionValue));
					_dnnContext.SubmitChanges();
					UpdateFunctionGrid(permissionId, e);
				}
			}
		}

		private void UpdateFunctionGrid(Guid permissionId, GridCommandEventArgs e)
		{
			e.Item.OwnerTableView.DataSource = _dnnContext.ManagerComsumerProfile_Functions.Where(a => a.PermissionId == permissionId);
		}
	}
}