﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CspTelerikManageConsumerProfile.Components;
using CspTelerikManageConsumerProfile.Utils;
using DesktopModules.CSPModules.CspCommons;
using Telerik.Web.UI;

namespace CspTelerikManageConsumerProfile
{
	public partial class ManageConsumersThemes : ModuleBase
	{
		private string strConnection;

		private const string SelectThemeQuery = @"select * from themes where active = 1";

		protected override void Page_Init(object sender, EventArgs e)
		{
			base.Page_Init(sender, e);

			toolBar.Items.Add(new RadToolBarButton
			{
				Value = "bBack",
				Text = GetLocalizedText("Label.BackToConsumerProfile"),
				ImageUrl = ControlPath + "images/back.gif",
				ToolTip = GetLocalizedText("Label.BackToConsumerProfile")
			});
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			strConnection = GetConnectionString(PortalId);
			dsThemes.ConnectionString = strConnection;
			dsThemes.SelectCommand = SelectThemeQuery;

			lThemeName.Text = GetLocalizedText("Label.SelectTheme");
			lSuppliersName.Text = GetLocalizedText("Label.SelectSuppliers");
			lCategoriesName.Text = GetLocalizedText("Label.SelectCategories");
			lConsumersName.Text = GetLocalizedText("Label.SelectConsumers");
			btnGetDataBindToTheme.Text = GetLocalizedText("Label.GetDataBindToTheme");
			btnAddWantedData.Text = GetLocalizedText("Label.AddWantedData");
		}

		protected void toolBar_ButtonClick(object sender, RadToolBarEventArgs e)
		{
			RadToolBarItem m_Button = e.Item;
			switch (m_Button.Value)
			{
				case "bBack":
					//Redirect to manage consumer profile page
					Response.Redirect(DotNetNuke.Common.Globals.NavigateURL(TabId, "", "mid", ModuleId.ToString()));
					break;
				default:
					break;
			}
		}

		protected void GetDataBindToTheme_Click(object sender, EventArgs e)
		{
			string themeId = ddtsTheme.SelectedValue;
			if (!string.IsNullOrEmpty(themeId))
			{
				var supplierCollection = _context.ThemesSuppliers.Where(a => a.themes_Id == int.Parse(themeId));
				lbxSuppliersList.Items.Clear();
				lbxSuppliersList.Items.Add(new RadListBoxItem("All", "All"));
				foreach (ThemesSupplier supplier in supplierCollection)
				{
					lbxSuppliersList.Items.Add(new RadListBoxItem(supplier.Company.companyname, supplier.companies_Id.ToString()));
				}

				categoryTree.Nodes.Clear();
				var rawCategories =
					_context.ExecuteQuery<Cat>("select categoryId, case when parentId = 0 then NULL else parentId end as parentId, " +
																		 "case when (description is NULL or description like '') then categoryText else description end as [categoryName] " +
																		 "from categories ct inner join themes_categories thc on ct.categoryId = thc.category_Id " +
																		 "where ct.active = 1 and thc.themes_Id = " + themeId);
				var catList = rawCategories.ToList();
				foreach (var category in catList)
				{
				    int? parentId = GetSkipParentCategoryId(_context.Categories.SingleOrDefault(a => a.categoryId == category.CategoryId));
                    var breakfrag = false;
                    while (!breakfrag)
                    {
                        var parent = _context.ThemesCategories.SingleOrDefault(a => a.themes_Id == int.Parse(themeId) && a.category_Id == parentId);
                        if (parent == null)
                        {
                            parentId = GetSkipParentCategoryId(_context.Categories.SingleOrDefault(a => a.categoryId == parentId));
                            if (parentId == 0)
                            {
                                parentId = null;
                                breakfrag = true;
                            }
                        }
                        else
                        {
                            parentId = parent.category_Id;
                            breakfrag = true;
                        }
                            
                    }
                    category.ParentId = parentId;
				}
				categoryTree.DataSource = catList;
				categoryTree.DataBind();

				var consumerCollection = _context.CompaniesThemes.Where(a => a.themes_Id == int.Parse(themeId));
				lbxConsumersList.Items.Clear();
				lbxConsumersList.Items.Add(new RadListBoxItem("All", "All"));
				foreach (CompaniesTheme consumer in consumerCollection)
				{
					lbxConsumersList.Items.Add(new RadListBoxItem(consumer.Company.companyname, consumer.companies_Id.ToString()));
				}
			}
			else
			{
				// Notification task.
				commandNotification.ContentIcon = ControlPath + "Images/32x32/warning.png";
				commandNotification.Text = GetLocalizedText("ChooseAThemeFirst");
				commandNotification.Show();
			}
		}


        /// <summary>
        /// Gets the skip parent category id.
        /// </summary>
        /// <param name="category">The category.</param>
        /// <returns></returns>
        /// Author: Vu Dinh
        /// 1/10/2013 - 3:55 PM
        protected int GetSkipParentCategoryId(Category category)
        {
            if (category == null)
                return 0;
            Category parentCat = _context.Categories.SingleOrDefault(a => a.categoryId == category.parentId && a.active == true);
            if (parentCat != null)
                return parentCat.categoryId;

            var treePath = category.lineage.Split('/').Where(a => !string.IsNullOrEmpty(a)).ToList();
            treePath.RemoveAt(treePath.Count - 1); // delete last path
            while (treePath.Any())
            {
                var path = treePath.Last();
                parentCat = _context.Categories.SingleOrDefault(a => a.categoryId == int.Parse(path) && a.active == true);
                if (parentCat != null)
                    return parentCat.categoryId;
                treePath.RemoveAt(treePath.Count-1); //skip this level if category is null
            }
            return 0;
        }

        /// <summary>
        /// Handles the Click event of the AddWantedData control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// Author: TNT
        /// Edited by: Vu Dinh
        /// 1/10/2013 - 3:26 PM
		protected void AddWantedData_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(ddtsTheme.SelectedValue) || lbxSuppliersList.CheckedItems.Count == 0 ||
				categoryTree.CheckedNodes.Count == 0 || lbxConsumersList.CheckedItems.Count == 0)
			{
				// Notification task.
				commandNotification.ContentIcon = ControlPath + "Images/32x32/warning.png";
				commandNotification.Text = GetLocalizedText("NeedChoose4ValueToAddConsumersThemes");
				commandNotification.Show();
			}
			else
			{
				try
				{
					using (var transaction = new TransactionScope())
					{
						int themeId = int.Parse(ddtsTheme.SelectedValue);

						foreach (RadListBoxItem supplier in lbxSuppliersList.CheckedItems)
						{
							if (!supplier.Value.Equals("All") && !supplier.Text.Equals("All"))
							{
								int supplierId = int.Parse(supplier.Value);
							    foreach (var consumer in lbxConsumersList.CheckedItems)
							    {
							        if (consumer.Value.Equals("All"))
                                        continue;
							        var consumerId = int.Parse(consumer.Value);
							        foreach (var node in categoryTree.GetAllNodes())
							        {
							            var isChange = false;
							            var catId = int.Parse(node.Value);
							            var consumerTheme = _context.ConsumersThemes.SingleOrDefault( a => a.category_Id == catId && a.consumer_Id == consumerId && a.themes_Id == themeId && a.supplier_Id == supplierId);
                                        if (node.Checked)
                                        {
                                            if (consumerTheme == null) // Insert new consumer theme
                                            {
                                                _context.ConsumersThemes.InsertOnSubmit(new ConsumersTheme
                                                                                            {
                                                                                                consumer_Id = consumerId,
                                                                                                category_Id = catId,
                                                                                                supplier_Id = supplierId,
                                                                                                themes_Id   = themeId
                                                                                            });
                                                isChange = true;
                                            }
                                        }
                                        else
                                        {
                                            if (consumerTheme != null) //Delete old comsumer theme record
                                            {
                                                _context.ConsumersThemes.DeleteOnSubmit(consumerTheme);
                                                isChange = true;
                                            }
                                        }
                                        if(isChange)
                                            _context.SubmitChanges(ConflictMode.FailOnFirstConflict);
							        }
							    }
							}
						}
						transaction.Complete();
					}
					// Notification task.
					commandNotification.ContentIcon = ControlPath + "Images/32x32/information.png";
					commandNotification.Text = GetLocalizedText("DataAddNewed");
					commandNotification.Show();
				}
				catch(Exception)
				{
					// Notification task.
					commandNotification.ContentIcon = ControlPath + "Images/32x32/error.png";
					commandNotification.Text = GetLocalizedText("DataAddNewFailed");
					commandNotification.Show();
				}
			}
		}
	}
}