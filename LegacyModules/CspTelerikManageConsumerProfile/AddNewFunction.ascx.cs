﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CspTelerikManageConsumerProfile.Components;
using CspTelerikManageConsumerProfile.Utils;
using DotNetNuke.Entities.Modules;
using Telerik.Web.UI;

namespace CspTelerikManageConsumerProfile
{
	public partial class AddNewFunction : UserControl
	{
		private object _dataItem = null;
		public object DataItem
		{
			get { return _dataItem; }
			set { _dataItem = value; }
		}

		#region Overrides of UserControl

		protected virtual void Page_Init(object sender, EventArgs e)
		{
			//base.OnInit(e);
			cbFunctions.DataTextField = "Name";
			cbFunctions.DataValueField = "Value";
			DataBinding += new EventHandler(AddNewPermissionDataBinding);
		}

		void AddNewPermissionDataBinding(object sender, EventArgs e)
		{
			var props = DataItem;
			if (props != null)
			{
				cbFunctions.DataSource = props;
			}
		}

		#endregion
	}
}