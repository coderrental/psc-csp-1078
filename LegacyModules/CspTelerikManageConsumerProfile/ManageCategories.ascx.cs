﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Entities.Modules;
using DesktopModules.CSPModules.CspCommons;
using DotNetNuke.Framework;
using DotNetNuke.Services.Localization;
using CspTelerikManageConsumerProfile.Components;
using Telerik.Web.UI;

namespace CspTelerikManageConsumerProfile
{
	public partial class ManageCategories : ModuleBase
	{
		protected override void Page_Init(object sender, EventArgs e)
		{
			base.Page_Init(sender,e);
			//Init data
			InitData();

			//Setting for categories Tree
			InitCategoryTreeSettings();

			//Init toolbar
			InitToolbar();
		}

		/// <summary>
		/// Inits the toolbar.
		/// </summary>
		private void InitToolbar()
		{
			toolBar.Items.Add(new RadToolBarButton
			                  	{
									Value = "bBack",
									Text = GetLocalizedText("Label.BackToConsumerProfile"),
									ImageUrl = ControlPath + "images/back.gif",
									ToolTip = GetLocalizedText("Label.BackToConsumerProfile")
			                  	});
		}

		/// <summary>
		/// Inits the category tree settings.
		/// </summary>
		private void InitCategoryTreeSettings()
		{	
			treeCategories.EditFormSettings.PopUpSettings.Modal = true;
			if (!IsPostBack) //First - expand to one level
				treeCategories.ExpandToLevel(1);
		}

		/// <summary>
		/// Inits the data.
		/// </summary>
		private void InitData()
		{
			/***** Init source for Tree *********/
			var m_SelectQuery = @"select 
			a.*, b.description as [schemeName], b.publication_schemes_Id from categories a
			left join publication_schemes b on b.publication_schemes_Id = a.publication_schemeID order by a.DisplayOrder, a.categoryId
			";

			//UPdate query for drag & drop function
			var m_UpdateQuery = @"update categories set 
parentId = @parentId
where categoryId = @categoryId";

			var m_DeleteQuery = @"DELETE FROM categories WHERE [categoryId] = @categoryId";

			dsCategories.ConnectionString = GetConnectionString(PortalId);
			dsCategories.SelectCommand = m_SelectQuery;
			dsCategories.UpdateCommand = m_UpdateQuery;
			dsCategories.DeleteCommand = m_DeleteQuery;
			dsCategories.DataBind();

			/**** Init source for Scheme combobox ****/
			var m_Query = @"select * from publication_schemes where active = '1'";
			dsScheme.ConnectionString = GetConnectionString(PortalId);
			dsScheme.SelectCommand = m_Query;
			dsCategories.DataBind();
		}

        /// <summary>
        /// Determines whether the specified obj is true.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns>
        ///   <c>true</c> if the specified obj is true; otherwise, <c>false</c>.
        /// </returns>
        /// Author: Vu Dinh
        /// 1/13/2013 - 11:45 AM
        protected bool IsTrue(object obj)
        {
            return obj.ToString() != string.Empty && (bool) obj;
        }



		#region [ Event Handlers ]
		/// <summary>
		/// Handles the OnInsertCommand event of the treeCategories control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Telerik.Web.UI.TreeListCommandEventArgs"/> instance containing the event data.</param>
		protected void treeCategories_OnInsertCommand(object sender, TreeListCommandEventArgs e)
		{
			e.Canceled = true;
			string commandText = @"INSERT into categories ([parentId],[categoryText], [description], [active], [IsSubscribable], [DisplayOrder], [apply_to_childs], [ExternalCategoryCode], [publication_schemeID],[dateAdded]) VALUES (@parentId,@categoryText, @description, @active, @IsSubscribable, @DisplayOrder, @apply_to_childs, @ExternalCategoryCode,@publication_schemeID, @dateAdded)";
			var m_Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CspPortal" + PortalId].ConnectionString);
			var m_Command = new SqlCommand(commandText, m_Conn);
			var m_Table = new Hashtable();

			var m_Item = e.Item as TreeListEditFormInsertItem;
			m_Table["categoryText"] = (m_Item.FindControl("tbxCategoryText") as TextBox).Text;
			m_Table["description"] = (m_Item.FindControl("tbxCategoryDesc") as TextBox).Text;
			m_Table["active"] = (m_Item.FindControl("chbxActive") as CheckBox).Checked;
			m_Table["IsSubscribable"] = (m_Item.FindControl("chbxIsSubscribable") as CheckBox).Checked;
			m_Table["DisplayOrder"] = (m_Item.FindControl("tbxDisplayOrder") as TextBox).Text;
			m_Table["apply_to_childs"] = (m_Item.FindControl("chbxApplyToChild") as CheckBox).Checked;
			m_Table["ExternalCategoryCode"] = (m_Item.FindControl("tbxExternalCode") as TextBox).Text;
			m_Table["publication_schemeID"] = (m_Item.FindControl("cbbShemes") as RadComboBox).SelectedValue;


			m_Command.Parameters.AddWithValue("categoryText", m_Table["categoryText"]);
			m_Command.Parameters.AddWithValue("description", m_Table["description"]);
			m_Command.Parameters.AddWithValue("active", m_Table["active"]);
			m_Command.Parameters.AddWithValue("IsSubscribable", m_Table["IsSubscribable"]);
			m_Command.Parameters.AddWithValue("DisplayOrder", m_Table["DisplayOrder"]);
			m_Command.Parameters.AddWithValue("apply_to_childs", m_Table["apply_to_childs"]);
			m_Command.Parameters.AddWithValue("ExternalCategoryCode", m_Table["ExternalCategoryCode"]);
			m_Command.Parameters.AddWithValue("publication_schemeID", m_Table["publication_schemeID"]);
			m_Command.Parameters.AddWithValue("dateAdded", DateTime.Now);

			object m_ParentValue = m_Item.ParentItem != null ? m_Item.ParentItem.GetDataKeyValue("categoryId") : "0";

			m_Command.Parameters.AddWithValue("parentId", m_ParentValue);

			m_Conn.Open();
			try
			{
				m_Command.ExecuteNonQuery();
			}
			finally
			{
				treeCategories.EditIndexes.Clear();
				treeCategories.Rebind();
				m_Conn.Close();
			}
		}

		/// <summary>
		/// Handles the OnUpdateCommand event of the treeCategories control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Telerik.Web.UI.TreeListCommandEventArgs"/> instance containing the event data.</param>
		protected void treeCategories_OnUpdateCommand(object sender, TreeListCommandEventArgs  e)
		{
			e.Canceled = true;
			string commandText = @"update categories set 
parentId = @parentId,
categoryText = @categoryText,
description = @description,
active = @active,
IsSubscribable = @IsSubscribable,
DisplayOrder = @DisplayOrder,
publication_schemeID = @publication_schemeID,
apply_to_childs = @apply_to_childs,
ExternalCategoryCode = @ExternalCategoryCode
where categoryId = @categoryId";

			var m_Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CspPortal" + PortalId].ConnectionString);
			var m_Command = new SqlCommand(commandText, m_Conn);
			var m_Table = new Hashtable();

			var m_Item = e.Item as TreeListEditFormItem;
			m_Table["categoryText"] = (m_Item.FindControl("tbxCategoryText") as TextBox).Text;
			m_Table["description"] = (m_Item.FindControl("tbxCategoryDesc") as TextBox).Text;
			m_Table["active"] = (m_Item.FindControl("chbxActive") as CheckBox).Checked;
			m_Table["IsSubscribable"] = (m_Item.FindControl("chbxIsSubscribable") as CheckBox).Checked;
			m_Table["DisplayOrder"] = (m_Item.FindControl("tbxDisplayOrder") as TextBox).Text;
			m_Table["apply_to_childs"] = (m_Item.FindControl("chbxApplyToChild") as CheckBox).Checked;
			m_Table["ExternalCategoryCode"] = (m_Item.FindControl("tbxExternalCode") as TextBox).Text;
			m_Table["publication_schemeID"] = (m_Item.FindControl("cbbShemes") as RadComboBox).SelectedValue;


			m_Command.Parameters.AddWithValue("categoryText", m_Table["categoryText"]);
			m_Command.Parameters.AddWithValue("description", m_Table["description"]);
			m_Command.Parameters.AddWithValue("active", m_Table["active"]);
			m_Command.Parameters.AddWithValue("IsSubscribable", m_Table["IsSubscribable"]);
			m_Command.Parameters.AddWithValue("DisplayOrder", m_Table["DisplayOrder"]);
			m_Command.Parameters.AddWithValue("apply_to_childs", m_Table["apply_to_childs"]);
			m_Command.Parameters.AddWithValue("ExternalCategoryCode", m_Table["ExternalCategoryCode"]);
			m_Command.Parameters.AddWithValue("publication_schemeID", m_Table["publication_schemeID"]);
			m_Command.Parameters.AddWithValue("categoryId", m_Item.ParentItem.GetDataKeyValue("categoryId"));

			object m_ParentValue = m_Item.ParentItem["parentId"].Text != "&nbsp;" ? m_Item.ParentItem["parentId"].Text : "0";

			m_Command.Parameters.AddWithValue("parentId", m_ParentValue);

			m_Conn.Open();
			try
			{
				m_Command.ExecuteNonQuery();
			}
			finally
			{
				treeCategories.EditIndexes.Clear();
				treeCategories.Rebind();
				m_Conn.Close();
			}
			
		}


		/// <summary>
		/// Handles the OnItemCommand event of the treeCategories control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void treeCategories_OnItemCommand(object sender, TreeListCommandEventArgs e)
		{
			if (e.CommandName == "MoveRoot")
			{
				int categoryId = 0;
				for (int i = 0; i < e.Item.Cells.Count; i++)
				{
					int.TryParse(e.Item.Cells[i].Text, out categoryId);
					if (categoryId > 0)
					{
						break;
					}
						
				}
				
				string commandText = @"update categories set 
parentId = 0 where categoryId = @categoryId" ;

				var mConn = new SqlConnection(ConfigurationManager.ConnectionStrings["CspPortal" + PortalId].ConnectionString);
				var mCommand = new SqlCommand(commandText, mConn);

				mCommand.Parameters.AddWithValue("categoryId", categoryId);

				mConn.Open();
				try
				{
					mCommand.ExecuteNonQuery();
				}
				finally
				{
					treeCategories.EditIndexes.Clear();
					treeCategories.Rebind();
					mConn.Close();
				}
			}
		}
		/// <summary>
		/// Handles the OnButtonClick event of the toolBar control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Telerik.Web.UI.RadToolBarEventArgs"/> instance containing the event data.</param>
		protected void toolBar_OnButtonClick(object sender, RadToolBarEventArgs e)
		{
			RadToolBarItem m_Button = e.Item;
			switch (m_Button.Value)
			{
				case "bBack":
					//Redirect to manage consumer profile page
					Response.Redirect(DotNetNuke.Common.Globals.NavigateURL(TabId,"","mid",ModuleId.ToString()));
					break;
				default:
					break;
			}
		}
		#endregion
	}
}