namespace CspTelerikManageConsumerProfile.Components
{
    public class CompanyTheme
    {
        private int _themeId;
        private string _themeName;
        private string _isSelected;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public CompanyTheme(int themeId, string themeName, string isSelected)
        {
            _themeId = themeId;
            _themeName = themeName;
            _isSelected = isSelected;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public CompanyTheme() : this(-1, "", "false")
        {
        }

        public int ThemeId
        {
            get { return _themeId; }
            set { _themeId = value; }
        }

        public string ThemeName
        {
            get { return _themeName; }
            set { _themeName = value; }
        }

        public string IsSelected
        {
            get {
                return _isSelected;
            }
            set { 
                _isSelected = value; 
            }
        }
    }
}