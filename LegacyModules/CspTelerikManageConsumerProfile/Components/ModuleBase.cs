﻿using System;
using System.Configuration;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;
using DotNetNuke.Services.Log.EventLog;
using DesktopModules.CSPModules.CspCommons;
using DotNetNuke.UI.Modules;

namespace CspTelerikManageConsumerProfile.Components
{
	public class ModuleBase : PortalModuleBase
	{
		protected EventLogController DnnEventLog;
		protected ModuleActionCollection MyActions;

		private string _localResourceFile;
		public string ResourceFilePath
		{
			get { return _localResourceFile; }
		}

		protected CspDataLayerDataContext _context;
		protected DnnDataLayerDataContext _dnnContext;

		/// <summary>
		/// Handles the Init event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected virtual void Page_Init(object sender, EventArgs e)
		{
			//Init Local Resource file
			_localResourceFile = GetCommonResourceFile("ManagerConsumerProfile", LocalResourceFile);

			//Init Data context
			_context = new CspDataLayerDataContext(Config.GetConnectionString("CspPortal" + PortalId));
			_dnnContext = new DnnDataLayerDataContext(Config.GetConnectionString());

			ModuleConfiguration.ModuleTitle = GetLocalizedText("ModuleTitle");
		}

		/// <summary>
		/// Get localized text for the current portal language
		/// </summary>
		/// <param name="key">Resource Key</param>
		/// <returns>Localized text</returns>
		public string GetLocalizedText(string key)
		{
			return Localization.GetString(key, _localResourceFile);
		}

		/// <summary>
		/// Write message to Dnn event view
		/// </summary>
		/// <param name="message">message to write</param>
		public void Log(string message)
		{
			DnnEventLog.AddLog("DEBUG", message, PortalSettings, UserId, EventLogController.EventLogType.ADMIN_ALERT);
		}

		/// <summary>
		/// Gets the common resource file.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="localResourceFile">The local resource file.</param>
		/// <returns></returns>
		public string GetCommonResourceFile(string name, string localResourceFile)
		{
			return localResourceFile.Substring(0, localResourceFile.LastIndexOf("/") + 1) + name;
		}

		/// <summary>
		/// Gets the connection string.
		/// </summary>
		/// <param name="portalId">The portal id.</param>
		/// <returns></returns>
		public string GetConnectionString(int portalId)
		{
			string m_Cs = "";

			try
			{
				m_Cs = Config.GetConnectionString("CspPortal" + portalId);
			}
			catch
			{
				m_Cs = "";
			}
			return m_Cs;
		}
	}
}