﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CspTelerikManageConsumerProfile.Components
{
	public class ConsumerParameter
	{
		public int Id { get; set; }
		public int CId { get; set;}
		public string Name { get; set; }
		public string Value { get; set; }

		public ConsumerParameter()
		{
			Id = CId = -1;
			Name = Value = "";
		}
	}
}