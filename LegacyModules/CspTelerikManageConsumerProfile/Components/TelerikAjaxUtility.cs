using System;
using System.Web;
using System.Web.UI;
using Telerik.Web.UI;

namespace CspTelerikManageConsumerProfile.Components
{
    /// <summary>
    /// Summary description for TelerikAjaxUtility
    /// </summary>
    public class TelerikAjaxUtility
    {
        private readonly static Page page = (Page)HttpContext.Current.Handler;
        public const string RAD_AJAX_MANAGER_ID = "_radAjaxManager_dnn";

        private TelerikAjaxUtility()
        {

        }

        /// <summary>
        /// Get the RadAjaxManager if existed
        /// </summary>
        /// <returns>null if the RadAjaxManager has not been added to the page parameter</returns>
        public static RadAjaxManager GetCurrentRadAjaxManager()
        {
            if (page == null)
            {
                throw new ArgumentNullException("null Page instance exception");
            }

            //return RadAjaxManager.GetCurrent(page);
            return page.Form.FindControl(RAD_AJAX_MANAGER_ID) as RadAjaxManager;
        }

        /// <summary>
        /// Add the RadAjaxManager the page parameter
        /// </summary>
        public static void InstallRadAjaxManager()
        {
            if (GetCurrentRadAjaxManager() == null)
            {
                RadAjaxManager ajaxManager = new RadAjaxManager();
                //ajaxManager.EnableHistory = true;
                //ajaxManager.EnablePageHeadUpdate = true;
                ajaxManager.ID = RAD_AJAX_MANAGER_ID;
                page.Form.Controls.AddAt(0, ajaxManager);
            }

        }

    }
}