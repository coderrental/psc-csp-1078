﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DesktopModules.CSPModules.CspCommons;
using CspTelerikManageConsumerProfile.Components;
using Telerik.Web.UI;

namespace CspTelerikManageConsumerProfile
{
	public partial class ManageThemes : ModuleBase
	{
		private string strConnection;
		private const string SelectThemeQuery = @"select * from themes";
		private const string SelectCompanyQuery = @"select * from companies where is_supplier = 1";
		private const string SelectCategoryQuery = @"select categoryId, case when parentId = 0 then NULL else parentId end as parentId, case when (description is NULL or description like '') then categoryText else description end as [categoryName] from categories";

		protected override void Page_Init(object sender, EventArgs e)
		{
			base.Page_Init(sender, e);

			toolBar.Items.Add(new RadToolBarButton
			{
				Value = "bBack",
				Text = GetLocalizedText("Label.BackToConsumerProfile"),
				ImageUrl = ControlPath + "images/back.gif",
				ToolTip = GetLocalizedText("Label.BackToConsumerProfile")
			});
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			strConnection = GetConnectionString(PortalId);
			dsThemes.ConnectionString = strConnection;
			dsThemes.SelectCommand = SelectThemeQuery;
			dsCompanies.ConnectionString = strConnection;
			dsCompanies.SelectCommand = SelectCompanyQuery;
			dsThemesCategories.ConnectionString = strConnection;
			dsCategories.ConnectionString = strConnection;
			dsCategories.SelectCommand = SelectCategoryQuery;
			dsThemesSuppliers.ConnectionString = strConnection;

			MngThemeGrid.MasterTableView.CommandItemSettings.AddNewRecordText = GetLocalizedText("AddNewTheme");
			MngThemeGrid.MasterTableView.NoMasterRecordsText = GetLocalizedText("No Themes avaiable.");
			MngThemeGrid.MasterTableView.NoDetailRecordsText = GetLocalizedText("No Themes avaiable.");
			MngThemeGrid.MasterTableView.DetailTables[0].CommandItemSettings.AddNewRecordText = GetLocalizedText("AddNewSupplierToTheme");
			MngThemeGrid.MasterTableView.DetailTables[0].NoMasterRecordsText = GetLocalizedText("NoSupplierAvaiable");
			MngThemeGrid.MasterTableView.DetailTables[0].NoDetailRecordsText = GetLocalizedText("NoSupplierAvaiable");
			MngThemeGrid.MasterTableView.DetailTables[1].CommandItemSettings.AddNewRecordText = GetLocalizedText("AddNewCategoryToTheme");
			MngThemeGrid.MasterTableView.DetailTables[1].NoMasterRecordsText = GetLocalizedText("NoCategoryAvaiable");
			MngThemeGrid.MasterTableView.DetailTables[1].NoDetailRecordsText = GetLocalizedText("NoCategoryAvaiable");
		}

		protected void toolBar_ButtonClick(object sender, RadToolBarEventArgs e)
		{
			RadToolBarItem m_Button = e.Item;
			switch (m_Button.Value)
			{
				case "bBack":
					//Redirect to manage consumer profile page
					Response.Redirect(DotNetNuke.Common.Globals.NavigateURL(TabId, "", "mid", ModuleId.ToString()));
					break;
				default:
					break;
			}
		}

		protected void MngThemeGrid_ItemDataBound(object sender, GridItemEventArgs e)
		{
			if (e.Item.IsInEditMode)
			{
				if (e.Item.OwnerTableView.Name == "ThemesSuppliers")
				{
					GridEditableItem item = e.Item as GridEditableItem;
					RadComboBox combo = item.FindControl("ddtsCompany") as RadComboBox;
					if (combo != null)
					{
						if (e.Item.Edit) // Add new
						{
							int themeId = -1;
							try { themeId = Convert.ToInt32(item.OwnerTableView.ParentItem.GetDataKeyValue("themes_Id")); }
							catch {}

							var suppliers = _context.ExecuteQuery<int>("SELECT companies_Id " +
								"FROM themes_suppliers " +
								"WHERE themes_Id = {0}", themeId);

							foreach (int sl in suppliers)
							{
								combo.Items.FindItemByValue(sl.ToString()).Remove();
							}
						}
						else // Update
						{
							int supplierId = Convert.ToInt32(item.GetDataKeyValue("CompanyId"));
							combo.CheckBoxes = false;
							combo.EnableCheckAllItemsCheckBox = false;
							RadComboBoxItem cbItem = combo.FindItemByValue(supplierId.ToString());
							if (cbItem != null) cbItem.Selected = true;
						}
					}
				}
				else if (e.Item.OwnerTableView.Name == "ThemesCategories")
				{
					try
					{
						GridEditableItem item = e.Item as GridEditableItem;
						RadComboBox combo = item.FindControl("ddtcCategory") as RadComboBox;
						if (combo != null)
						{
							RadTreeView tree = (RadTreeView) combo.Items[0].FindControl("categoryTree");
							if (tree != null)
							{
								if (e.Item.Edit) // Add new
								{
									int themeId = -1;
									try { themeId = Convert.ToInt32(item.OwnerTableView.ParentItem.GetDataKeyValue("themes_Id")); }
									catch {}

									var categories = _context.ExecuteQuery<int>("SELECT category_Id " +
										"FROM themes_categories " +
										"WHERE themes_Id = {0}", themeId);

									foreach (int cat in categories)
									{
									  RadTreeNode treeNode = tree.FindNodeByValue(cat.ToString());
									  if(treeNode != null) treeNode.Remove();
									}
								}
								else // Update
								{
									int categoryId = Convert.ToInt32(item.GetDataKeyValue("CategoryId"));
									tree.CheckBoxes = false;
									RadTreeNode treeNode = tree.FindNodeByValue(categoryId.ToString());
									if (treeNode != null)
									{
										treeNode.Selected = true;
										treeNode.Checked = true;
										combo.SelectedValue = treeNode.Value;
										combo.SelectedItem.Text = treeNode.Text;
									}
								}
							}
						}
					}
					catch (Exception ee)
					{
						;
					}
				}
			}
		}

		protected void MngThemeGrid_ItemCommand(object source, GridCommandEventArgs e)
		{
			//if (e.CommandName == RadGrid.EditCommandName)
			////if (e.CommandName == "Update")
			//{
			//  //int themeId = int.Parse(e.Item.OwnerTableView.DataKeyValues[0].ToString());
			//  GridEditableItem item = e.Item as GridEditableItem;
			//  //GridDataItem item = (GridDataItem) e.Item;
			//  if (item.OwnerTableView.Name == "ThemesCategories")
			//  {
			//    TableCell cell = item["MngTcThemeName"];
			//    int id = -1;
			//    int themeId;
			//    try
			//    {
			//      themeId = Convert.ToInt32(e.Item.OwnerTableView.ParentItem.GetDataKeyValue("themes_Id"));
			//    }
			//    catch
			//    {
			//      themeId = -1;
			//    }
			//    cell = item["MngCategoryName"];
			//    foreach (Control control in item["MngCategoryName"].Controls)
			//    {
			//      String sss = control.NamingContainer.ClientID;
			//    }
			//    RadComboBox cbx = item.FindControl("ddtcCategory") as RadComboBox;
			//    int categoryId = -1;
			//    GridColumn col = e.Item.OwnerTableView.Columns.FindByUniqueNameSafe("MngCategoryName") as GridColumn;
			//    RadComboBox combo = (RadComboBox) cell.FindControl("ddtcCategory");
			//    if (combo != null)
			//    {
			//      //categoryId = Convert.ToInt32(combo.SelectedValue);
			//      RadTreeView tree = (RadTreeView) combo.Items[0].FindControl("categoryTree");
			//      if (tree != null && !String.IsNullOrEmpty(tree.SelectedValue))
			//        categoryId = Convert.ToInt32(tree.SelectedValue);
			//    }
			//  }
			//}
		}

		protected void MngThemeGrid_UpdateCommand(object sender, GridCommandEventArgs e)
		{
			if (sender is RadGrid)
			{
				RadGrid grid = (RadGrid) sender;
				if (grid.ID == "MngThemeGrid")
				{
					if (e.Item.IsInEditMode && e.Item.OwnerTableView.Name == "ThemesSuppliers")
					{
						switch (e.CommandName)
						{
							case "Update":
								GridEditableItem item = (GridEditableItem) e.Item;
								int id = -1;
								try { id = Convert.ToInt32(item.GetDataKeyValue("ThemeSupplierId")); }
								catch {}
								ThemesSupplier ts = _context.ThemesSuppliers.SingleOrDefault(a => a.themes_suppliers_Id == id);
								if (ts != null)
								{
									try
									{
										int oldThemeId = (int)ts.themes_Id, oldCompanyId = (int)ts.companies_Id;
										int newThemeId = oldThemeId, newCompanyId = oldCompanyId;

										item.UpdateValues(ts);
										TableCell cell;
										cell = item["MngThemeName"];
										RadComboBox combo = (RadComboBox) cell.FindControl("ddtsTheme");
										if (combo != null && !String.IsNullOrEmpty(combo.SelectedValue))
										{
											newThemeId = Convert.ToInt32(combo.SelectedValue);
											ts.themes_Id = newThemeId;
										}
										cell = item["MngCompanyName"];
										combo = (RadComboBox) cell.FindControl("ddtsCompany");
										if (combo != null && !String.IsNullOrEmpty(combo.SelectedValue))
										{
											newCompanyId = Convert.ToInt32(combo.SelectedValue);
											ts.companies_Id = newCompanyId;
										}

										DeleteConsumerThemeOfSupplier(oldThemeId, oldCompanyId);
										_context.SubmitChanges();
										AddConsumerThemeOfSupplier(newThemeId, newCompanyId);
										_context.SubmitChanges();

										// Notification task.
										commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/information.png";
										commandNotification.Text = GetLocalizedText("DataUpdated");
										commandNotification.Show();
									}
									catch
									{
										// Notification task.
										commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/error.png";
										commandNotification.Text = GetLocalizedText("DataUpdateFailed");
										commandNotification.Show();
									}
								}
								break;
							default:
								break;
						}
					}
					else if (e.Item.IsInEditMode && e.Item.OwnerTableView.Name == "ThemesCategories")
					{
						switch (e.CommandName)
						{
							case "Update":
								GridEditableItem item = (GridEditableItem)e.Item;
								int id = -1;
								try { id = Convert.ToInt32(item.GetDataKeyValue("ThemeCategoryId")); }
								catch {}
								ThemesCategory tc = _context.ThemesCategories.SingleOrDefault(a => a.themes_categories_Id == id);
								if (tc != null)
								{
									try
									{
										int oldCategoryId = tc.category_Id;
										int newCategoryId = oldCategoryId;

										item.UpdateValues(tc);
										TableCell cell;
										cell = item["MngCategoryName"];
										RadComboBox combo = (RadComboBox)cell.FindControl("ddtcCategory");
										if (combo != null)
										{
											RadTreeView tree = (RadTreeView)combo.Items[0].FindControl("categoryTree");
											if (tree != null && !String.IsNullOrEmpty(tree.SelectedValue))
											{
												newCategoryId = Convert.ToInt32(tree.SelectedValue);
												tc.category_Id = newCategoryId;
											}
										}

										DeleteConsumerThemeOfCategory(tc.themes_Id, oldCategoryId);
										_context.SubmitChanges();
										AddConsumerThemeOfCategory(tc.themes_Id, newCategoryId);
										_context.SubmitChanges();

										// Notification task.
										commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/information.png";
										commandNotification.Text = GetLocalizedText("DataUpdated");
										commandNotification.Show();
									}
									catch
									{
										// Notification task.
										commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/error.png";
										commandNotification.Text = GetLocalizedText("DataUpdateFailed");
										commandNotification.Show();
									}
								}
								break;
							default:
								break;
						}
					}
					else
					{
						switch (e.CommandName)
						{
							case "Update":
								GridEditableItem item = (GridEditableItem)e.Item;
								int id = -1;
								try { id = Convert.ToInt32(item.GetDataKeyValue("themes_Id")); }
								catch {}
								Theme theme = _context.Themes.SingleOrDefault(a => a.themes_Id == id);
								if (theme != null)
								{
									item.UpdateValues(theme);
									TableCell cell;
									cell = item["Description"];
									RadTextBox textBox = (RadTextBox)cell.FindControl("txtMngTheme");
									if (textBox != null && textBox.Text.Trim().Length > 0)
									{
										theme.description = textBox.Text.Trim();
									}
									cell = item["Active"];
									RadButton button = (RadButton)cell.FindControl("cbxMngThemeActive");
									if (button != null)
									{
										theme.active = button.Checked;
									}
									_context.SubmitChanges();

									// Notification task.
									commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/information.png";
									commandNotification.Text = GetLocalizedText("DataUpdated");
									commandNotification.Show();
								}
								break;
							default:
								break;
						}
					}
				}
			}
		}

		protected void MngThemeGrid_InsertCommand(object sender, GridCommandEventArgs e)
		{
			if (sender is RadGrid)
			{
				RadGrid grid = (RadGrid) sender;
				if (grid.ID == "MngThemeGrid")
				{
					if (e.Item.IsInEditMode && e.Item.OwnerTableView.Name == "ThemesSuppliers")
					{
						switch (e.CommandName)
						{
							case "PerformInsert":
								try
								{
									GridEditableItem item = (GridEditableItem) e.Item;
									TableCell cell = item["MngThemeName"];
									int themeId;
									try { themeId = Convert.ToInt32( e.Item.OwnerTableView.ParentItem.GetDataKeyValue("themes_Id")); }
									catch { themeId = -1; }
									cell = item["MngCompanyName"];
									int companyId = -1;
									RadComboBox combo = (RadComboBox) cell.FindControl("ddtsCompany");
									if (combo != null)
									{
										var collection = combo.CheckedItems;
										foreach (var userItem in collection)
										{
											companyId = Convert.ToInt32(userItem.Value);

											if (themeId != -1 && companyId != -1)
											{
												IEnumerable<int> ts = _context.ExecuteQuery<int>("SELECT themes_suppliers_Id FROM themes_suppliers " +
													"WHERE themes_Id = {0} AND companies_Id = {1}", themeId, companyId);
												if (ts == null || ts.Count() == 0)
												{
													int maxId = 0;
													try { maxId = (from ths in _context.ThemesSuppliers select (ths.themes_suppliers_Id)).Max(); }
													catch { }

													_context.ThemesSuppliers.InsertOnSubmit(new ThemesSupplier
													{
														themes_suppliers_Id = maxId + 1,
														themes_Id = themeId,
														companies_Id = companyId
													});

													// Add all consumer has already subscribed to theme in to database.
													AddConsumerThemeOfSupplier(themeId, companyId);

													_context.SubmitChanges();

													// Notification task.
													commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/information.png";
													commandNotification.Text = GetLocalizedText("DataAddNewed");
													commandNotification.Show();
												}
											}
											else
											{
												// Notification task.
												commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/error.png";
												commandNotification.Text = GetLocalizedText("DataAddNewFailed");
												commandNotification.Show();
											}
										}
									}
								}
								catch(Exception ex)
								{
									// Notification task.
									commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/warning.png";
									commandNotification.Text = GetLocalizedText("NeedPickThemeFirst");
									commandNotification.Show();
								}
								break;
						}
					}
					else if (e.Item.IsInEditMode && e.Item.OwnerTableView.Name == "ThemesCategories")
					{
						switch (e.CommandName)
						{
							case "PerformInsert":
								try
								{
									GridEditableItem item = (GridEditableItem)e.Item;
									TableCell cell = item["MngTcThemeName"];
									int themeId;
									try { themeId = Convert.ToInt32(e.Item.OwnerTableView.ParentItem.GetDataKeyValue("themes_Id")); }
									catch { themeId = -1; }
									cell = item["MngCategoryName"];
									int categoryId = -1;
									RadComboBox combo = (RadComboBox)cell.FindControl("ddtcCategory");
									if (combo != null)
									{
										RadTreeView tree = (RadTreeView)combo.Items[0].FindControl("categoryTree");
										if(tree != null)
										{
											bool isAdded = false;
											foreach (RadTreeNode node in tree.CheckedNodes)
											{
												categoryId = Convert.ToInt32(node.Value);
												IEnumerable<int> tc = _context.ExecuteQuery<int>("SELECT themes_categories_Id FROM themes_categories " +
													"WHERE themes_Id = {0} AND category_Id = {1}", themeId, categoryId);
												if (tc == null || tc.Count() == 0)
												{
													int maxId = 0;
													try { maxId = (from ths in _context.ThemesCategories select (ths.themes_categories_Id)).Max(); }
													catch {}

													_context.ThemesCategories.InsertOnSubmit(new ThemesCategory
													{
														themes_categories_Id = maxId + 1,
														themes_Id = themeId,
														category_Id = categoryId
													});

													// Add all consumer has already subscribed to theme in to database.
													AddConsumerThemeOfCategory(themeId, categoryId);
													isAdded = true;
												}
											}

											if (isAdded)
											{
												_context.SubmitChanges();

												// Notification task.
												commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/information.png";
												commandNotification.Text = GetLocalizedText("DataAddNewed");
												commandNotification.Show();
											}
										}
									}
								}
								catch
								{
									// Notification task.
									commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/warning.png";
									commandNotification.Text = GetLocalizedText("NeedPickThemeFirst");
									commandNotification.Show();
								}
								break;
						}
					}
					else
					{
						switch (e.CommandName)
						{
							case "PerformInsert":
								try
								{
									GridEditableItem item = (GridEditableItem)e.Item;
									TableCell cell;
									string description = "";
									cell = item["Description"];
									RadTextBox textBox = (RadTextBox)cell.FindControl("txtMngTheme");
									if(textBox != null && textBox.Text.Trim().Length > 0) description = textBox.Text.Trim();
									bool active = false;
									cell = item["Active"];
									RadButton button = (RadButton)cell.FindControl("cbxMngThemeActive");
									if (button != null) active = button.Checked;
									int maxId = (from t in _context.Themes select (t.themes_Id)).Max();
									_context.Themes.InsertOnSubmit(new Theme
									{
									  themes_Id = maxId + 1,
										description = description,
										active = active
									});
									_context.SubmitChanges();

									// Notification task.
									commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/information.png";
									commandNotification.Text = GetLocalizedText("DataAddNewed");
									commandNotification.Show();
								}
								catch (Exception)
								{
									// Notification task.
									commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/error.png";
									commandNotification.Text = GetLocalizedText("DataAddNewFailed");
									commandNotification.Show();
								}
								break;
							default:
								break;
						}
					}
				}
			}
		}

		protected void MngThemeGrid_DeleteCommand(object sender, GridCommandEventArgs e)
		{
			if (sender is RadGrid)
			{
				RadGrid grid = (RadGrid)sender;
				if (grid.ID == "MngThemeGrid")
				{
					if (e.Item.OwnerTableView.Name == "ThemesSuppliers")
					{
						switch (e.CommandName)
						{
							case "Delete":
								try
								{
									GridEditableItem item = (GridEditableItem)e.Item;
									int themeId = -1;
									try { themeId = Convert.ToInt32(e.Item.OwnerTableView.ParentItem.GetDataKeyValue("themes_Id")); }
									catch {}
									int companyId = -1;
									try { companyId = Convert.ToInt32(item.GetDataKeyValue("CompanyId")); }
									catch {}
									if (themeId != -1 && companyId != -1)
									{
										// Delete all consumer has already subscribed to theme in to database.
										DeleteConsumerThemeOfSupplier(themeId, companyId);

										// Notification task.
										commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/information.png";
										commandNotification.Text = GetLocalizedText("DataDeleted");
										commandNotification.Show();
									}
								}
								catch
								{
									// Notification task.
									commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/warning.png";
									commandNotification.Text = GetLocalizedText("NeedPickThemeFirst");
									commandNotification.Show();
								}
								break;
						}
					}
					else if (e.Item.OwnerTableView.Name == "ThemesCategories")
					{
						switch (e.CommandName)
						{
							case "Delete":
								try
								{
									GridEditableItem item = (GridEditableItem)e.Item;
									int themeId = -1;
									try { themeId = Convert.ToInt32(e.Item.OwnerTableView.ParentItem.GetDataKeyValue("themes_Id")); }
									catch {}
									int categoryId = -1;
									try { categoryId = Convert.ToInt32(item.GetDataKeyValue("CategoryId")); }
									catch {}
									if(themeId != -1 && categoryId != -1)
									{
										// Delete all consumer has already subscribed to theme in to database.
										DeleteConsumerThemeOfCategory(themeId, categoryId);

										// Notification task.
										commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/information.png";
										commandNotification.Text = GetLocalizedText("DataDeleted");
										commandNotification.Show();
									}
								}
								catch
								{
									// Notification task.
									commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/warning.png";
									commandNotification.Text = GetLocalizedText("NeedPickThemeFirst");
									commandNotification.Show();
								}
								break;
						}
					}
					else
					{
						switch (e.CommandName)
						{
							case "Delete":
								try
								{
									GridEditableItem item = (GridEditableItem)e.Item;
									int themeId = -1;
									try { themeId = Convert.ToInt32(item.GetDataKeyValue("themes_Id")); }
									catch {}
									if(themeId != -1)
									{
										_context.ExecuteCommand("DELETE FROM companies_themes WHERE themes_Id = {0}", themeId);
										_context.ExecuteCommand("DELETE FROM consumers_themes WHERE themes_Id = {0}", themeId);
										_context.ExecuteCommand("DELETE FROM themes_categories WHERE themes_Id = {0}", themeId);
										_context.ExecuteCommand("DELETE FROM themes_suppliers WHERE themes_Id = {0}", themeId);
									}

									// Notification task.
									commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/information.png";
									commandNotification.Text = GetLocalizedText("DataDeleted");
									commandNotification.Show();
								}
								catch (Exception)
								{
									// Notification task.
									commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/error.png";
									commandNotification.Text = GetLocalizedText("DataAddNewFailed");
									commandNotification.Show();
								}
								break;
							default:
								break;
						}
					}
				}
			}
		}

		private void AddConsumerThemeOfSupplier(int themeId, int companyId)
		{
			/*
			IEnumerable<int> companiesTheme = _context.ExecuteQuery<int>("SELECT ct.companies_Id " +
				"FROM companies_themes ct join companies c on ct.companies_Id = c.companies_Id " +
				"WHERE ct.themes_Id = {0}", themeId);
			IEnumerable<int> themesCategory = _context.ExecuteQuery<int>("SELECT category_Id FROM themes_categories " +
				"WHERE themes_Id = {0}", themeId);

			List<int> companiesThemeList = companiesTheme.ToList();
			List<int> themesCategoryList = themesCategory.ToList();

			foreach (int cTheme in companiesThemeList)
			{
				foreach (int tCategory in themesCategoryList)
				{
					_context.ConsumersThemes.InsertOnSubmit(new ConsumersTheme
					{
						consumer_Id = cTheme,
						themes_Id = themeId,
						supplier_Id = companyId,
						category_Id = tCategory
					});
				}
			}
			*/
			_context.ExecuteCommand(@"
insert into consumers_themes(category_Id,consumer_Id,supplier_Id,themes_Id)
select tc.category_Id, c.companies_Id,{1},{0}
from companies_themes ct
	join companies c on c.companies_Id=ct.companies_Id
	cross join themes t
	cross join themes_categories tc 
where ct.themes_Id={0} and c.is_consumer=1 and t.active = 1 and t.themes_Id={0} and tc.themes_Id={0} and not exists (
	select 1 from consumers_themes a 
	where a.category_Id=tc.category_Id 
		and a.consumer_Id=c.companies_Id
		and a.supplier_Id={1}
		and a.themes_Id={0})"
				, themeId, companyId);
		}

		private void DeleteConsumerThemeOfSupplier(int themeId, int companyId)
		{
			/*
			IEnumerable<int> companiesTheme = _context.ExecuteQuery<int>("SELECT ct.companies_Id " +
				"FROM companies_themes ct join companies c on ct.companies_Id = c.companies_Id " +
				"WHERE ct.themes_Id = {0}", themeId);
			IEnumerable<int> themesCategory = _context.ExecuteQuery<int>("SELECT category_Id FROM themes_categories " +
				"WHERE themes_Id = {0}", themeId);

			List<int> companiesThemeList = companiesTheme.ToList();
			List<int> themesCategoryList = themesCategory.ToList();

			foreach (int cTheme in companiesThemeList)
			{
				foreach (int tCategory in themesCategoryList)
				{
					_context.ExecuteCommand("DELETE FROM consumers_themes " +
						"WHERE consumer_Id = {0} AND themes_Id = {1} AND supplier_Id = {2} AND category_Id = {3}", cTheme, themeId, companyId, tCategory);
				}
			}
			*/
			_context.ExecuteCommand("delete from consumers_themes where supplier_Id={0} and themes_Id={1}", companyId, themeId);
		}

		private void AddConsumerThemeOfCategory(int themeId, int categoryId)
		{
			IEnumerable<int> companiesTheme = _context.ExecuteQuery<int>("SELECT ct.companies_Id " +
				"FROM companies_themes ct join companies c on ct.companies_Id = c.companies_Id " +
				"WHERE ct.themes_Id = {0}", themeId);
			IEnumerable<int> themesSupplier = _context.ExecuteQuery<int>("SELECT companies_Id FROM themes_suppliers " +
				"WHERE themes_Id = {0}", themeId);

			List<int> companiesThemeList = companiesTheme.ToList();
			List<int> themesSupplierList = themesSupplier.ToList();

			foreach (int cTheme in companiesThemeList)
			{
				foreach (int tSupplier in themesSupplierList)
				{
					_context.ConsumersThemes.InsertOnSubmit(new ConsumersTheme
					{
						consumer_Id = cTheme,
						themes_Id = themeId,
						supplier_Id = tSupplier,
						category_Id = categoryId
					});
				}
			}
		}

		private void DeleteConsumerThemeOfCategory(int themeId, int categoryId)
		{
			IEnumerable<int> companiesTheme = _context.ExecuteQuery<int>("SELECT ct.companies_Id " +
				"FROM companies_themes ct join companies c on ct.companies_Id = c.companies_Id " +
				"WHERE ct.themes_Id = {0}", themeId);
			IEnumerable<int> themesSupplier = _context.ExecuteQuery<int>("SELECT companies_Id FROM themes_suppliers " +
				"WHERE themes_Id = {0}", themeId);

			List<int> companiesThemeList = companiesTheme.ToList();
			List<int> themesSupplierList = themesSupplier.ToList();

			foreach (int cTheme in companiesThemeList)
			{
				foreach (int tSupplier in themesSupplierList)
				{
					_context.ExecuteCommand("DELETE FROM consumers_themes " +
						"WHERE consumer_Id = {0} AND themes_Id = {1} AND supplier_Id = {2} AND category_Id = {3}", cTheme, themeId, tSupplier, categoryId);
				}
			}
		}

		protected string GetModulePathUrl()
		{
			return ControlPath;
		}
	}
}