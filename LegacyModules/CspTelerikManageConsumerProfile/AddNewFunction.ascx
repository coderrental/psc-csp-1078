﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddNewFunction.ascx.cs" Inherits="CspTelerikManageConsumerProfile.AddNewFunction" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div>
	<span>Select a Function: </span>
	<telerik:RadComboBox runat="server" ID="cbFunctions" CheckBoxes="True" EnableCheckAllItemsCheckBox="True" CssClass="CspManageConsumerProfile-permission-inline-combobox" Width="400px" Filter="Contains" />
	<asp:ImageButton ID="btnInsert" CssClass="CspManageConsumerProfile-permission-inline-button" Text="Insert" runat="server" CommandName="PerformInsert" ImageUrl="Images/24x24/add.png"></asp:ImageButton>
	<asp:ImageButton ID="btnCancel" CssClass="CspManageConsumerProfile-permission-inline-button" Text="Cancel" runat="server" CausesValidation="False" CommandName="Cancel" ImageUrl="Images/24x24/delete.png"></asp:ImageButton>
</div>
