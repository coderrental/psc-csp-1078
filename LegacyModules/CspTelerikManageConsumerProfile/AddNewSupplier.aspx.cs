﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CspTelerikManageConsumerProfile.Components;
using DesktopModules.CSPModules.CspCommons;
using DotNetNuke.Services.Localization;

namespace TIEKinetix.CspModules.CspTelerikManageConsumerProfile
{
	public partial class AddNewSupplier : Page
	{
		protected CspDataLayerDataContext _context;

		private string _localResourceFile;

		protected void Page_Init(object sender, EventArgs e)
		{
			_context = new CspDataLayerDataContext(ConfigurationManager.ConnectionStrings["CspPortal0"].ConnectionString);

			//Init Local Resource file
			_localResourceFile = GetCommonResourceFile("ManagerConsumerProfile", Request.Path);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			lSupplierName.Text = GetLocalizedText("SupplierName");
			lDisplayName.Text = GetLocalizedText("DisplayName");
			lAddress1.Text = GetLocalizedText("Address1");
			lAddress2.Text = GetLocalizedText("Address2");
			lCity.Text = GetLocalizedText("City");
			lState.Text = GetLocalizedText("State");
			lZipCode.Text = GetLocalizedText("ZipCode");
			lCountry.Text = GetLocalizedText("Country");
			btnSubmit.Text = GetLocalizedText("Submit");
		}

		protected void AddNewSupplier_Click(object sender, EventArgs e)
		{
			if (Request.IsAuthenticated)
			{
				string strSupplierName = txtSupplierName.Text.Trim(),
							 strDisplayName = txtDisplayName.Text.Trim(),
							 strAddress1 = txtAddress1.Text.Trim(),
							 strAddress2 = txtAddress2.Text.Trim(),
							 strCity = txtCity.Text.Trim(),
							 strState = txtState.Text.Trim(),
							 strZipCode = txtZipCode.Text.Trim(),
							 strCountry = txtCountry.Text.Trim();

				if (!string.IsNullOrEmpty(strSupplierName))
				{
					Company company = _context.Companies.FirstOrDefault(a => a.companyname == strSupplierName);
					if (company == null)
					{
						_context.Companies.InsertOnSubmit(new Company
						{
							companyname = strSupplierName,
							displayname = strDisplayName,
							parent_companies_Id = 0,
							is_supplier = true,
							is_consumer = false,
							address1 = strAddress1,
							address2 = strAddress2,
							city = strCity,
							state = strState,
							zipcode = strZipCode,
							country = strCountry,
							created = DateTime.Now,
							date_changed = DateTime.Now
						});
						_context.SubmitChanges();
						lblOutMsg.Text = "Success add new Supplier.";
					}
					else
					{
						lblOutMsg.Text = "The Supplier with name as <b>" + strSupplierName + "</b> has exists.";
					}
				}
				else lblOutMsg.Text = "Supplier Name does not empty";
			}
			else lblOutMsg.Text = "You must login before run this action";
		}

		/// <summary>
		/// Gets the common resource file.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="localResourceFile">The local resource file.</param>
		/// <returns></returns>
		private string GetCommonResourceFile(string name, string localResourceFile)
		{
			return localResourceFile.Substring(0, localResourceFile.LastIndexOf("/") + 1) + "App_LocalResources/" + name;
		}

		/// <summary>
		/// Get localized text for the current portal language
		/// </summary>
		/// <param name="key">Resource Key</param>
		/// <returns>Localized text</returns>
		private string GetLocalizedText(string key)
		{
			return Localization.GetString(key, _localResourceFile);
		}
	}
}