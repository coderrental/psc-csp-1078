﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddNewPermission.ascx.cs" Inherits="CspTelerikManageConsumerProfile.AddNewPermission" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div>
	<span>Select a User: </span><telerik:RadComboBox runat="server" ID="cbUsers" CheckBoxes="True" EnableCheckAllItemsCheckBox="True" CssClass="CspManageConsumerProfile-permission-inline-combobox" Width="200px" Filter="Contains"></telerik:RadComboBox>
	<asp:ImageButton ID="btnInsert" CssClass="CspManageConsumerProfile-permission-inline-button" Text="Insert" runat="server" CommandName="PerformInsert" ImageUrl="Images/24x24/add.png"></asp:ImageButton>
	<asp:ImageButton ID="btnCancel" CssClass="CspManageConsumerProfile-permission-inline-button" Text="Cancel" runat="server" CausesValidation="False" CommandName="Cancel" ImageUrl="Images/24x24/delete.png"></asp:ImageButton>
</div>