﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CspTelerikManageConsumerProfile.Utils;
using DesktopModules.CSPModules.CspCommons;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Framework;
using DotNetNuke.Security;
using DotNetNuke.Services.Localization;
using CspTelerikManageConsumerProfile.Components;
using Telerik.Web.UI;

namespace CspTelerikManageConsumerProfile
{
	///<summary>
	///</summary>
	public partial class ConsumerProfilerViewController : ModuleBase, IActionable
	{
		private const string SelectCommandQuery = @"select * from companies";
		private Company _company;
		private Language[] _languages;
		private Theme[] _themes;
		private Company[] _suppliers;
		private Category[] _categories;
		private const string GetCompanyThemeQuery = @"
select a.themes_Id as [ThemeId], a.description as [ThemeName], case when b.companies_Id is null then 'false' else 'true' end as [IsSelected]
from themes a 
	left join companies_themes b on a.themes_Id = b.themes_Id and b.companies_Id = {0}
where a.active = 1
order by [ThemeName]
";
		private const string GetConsumerThemeQuery = @"
select cth.consumers_themes_Id as [ConsumerThemeId], th.themes_Id as [ThemeId], th.description as [ThemeName],
	c.companies_Id as [SupplierId], c.companyname as [SupplierName], ct.categoryId as [CategoryId], ct.categoryText as [CategoryName]
from
	themes th join consumers_themes cth on th.themes_Id = cth.themes_Id
	join companies c on cth.supplier_Id = c.companies_Id
	join categories ct on cth.category_Id = ct.categoryId
	and cth.consumer_Id = {0}
where th.active = 1
order by [ThemeName], [SupplierName], [CategoryName]";

		private string _editableParameterNames;

		private FuncsMan _funcsMan;

		protected override void Page_Init(object sender, EventArgs e)
		{
			base.Page_Init(sender, e);
			if (AJAX.IsInstalled())
			{
				AJAX.RegisterScriptManager();
				using (ScriptManager scriptManager = (ScriptManager)AJAX.ScriptManagerControl(Page))
				{
					scriptManager.AsyncPostBackError += new EventHandler<AsyncPostBackErrorEventArgs>(scriptManager_AsyncPostBackError);
				}
			}

			_funcsMan = new FuncsMan(ResourceFilePath);

			_editableParameterNames = Settings[Commons.EDITABLE_PARAMETERS] != null ? Settings[Commons.EDITABLE_PARAMETERS].ToString() : "";
			_languages = _context.Languages.Where(a => a.active.HasValue && a.active.Value).ToArray();
			_themes = _context.Themes.Where(a => a.active == true).ToArray();
			ds.ConnectionString = ConfigurationManager.ConnectionStrings["CspPortal" + PortalId].ConnectionString;
			ds.SelectCommand = SelectCommandQuery;
			companyGrid.GroupingSettings.CaseSensitive = false;
            companyGrid.SelectedIndexChanged += new EventHandler(GridEventHandler);
            cpGrid.ItemDataBound += new GridItemEventHandler(CpGridItemDataBound);
            consumerProfileGrid.ItemDataBound += new GridItemEventHandler(consumerProfileGridItemDataBound);
			InitializeToolbar();
			InitTabs();

			if (UserInfo.IsInRole("Administrators"))
			{
				managerToolBar.Items.FindItemByValue("bAddNewSupplier").Visible = true;
				managerToolBar.Items.FindItemByValue("bManageCategories").Visible = true;
				managerToolBar.Items.FindItemByValue("bManageThemes").Visible = true;
				managerToolBar.Items.FindItemByValue("bManageConsumersThemes").Visible = true;

				foreach (RadTab tab in tabs.Tabs)
				{
					tab.Visible = true;
				}
			}
			else if (UserId == -1 || !Request.IsAuthenticated)
			{
				navContainer.Visible = navContainer.Enabled = false;
				navErrorContainer.Visible = navErrorContainer.Enabled = true;
				navErrorContainer.Controls.Add(new Literal { Text = GetLocalizedText("Label.InSufficientPermission") });
			}
			else
			{
				var permission =
					_dnnContext.ManagerComsumerProfile_Permissions.FirstOrDefault(
						a => a.UserId == UserId && a.PortalId == PortalId && a.TabModuleId == TabModuleId);
				if (permission != null)
				{
					Guid permissionId = permission.PermissionId;
					var funcs = _dnnContext.ManagerComsumerProfile_Functions.Where(a => a.PermissionId == permissionId);
					if (funcs.Count() > 0)
					{
						foreach (ManagerComsumerProfile_Function func in funcs)
						{
							string funcValue = func.FunctionValue;
							if (funcValue[0] == 'b') managerToolBar.Items.FindItemByValue(funcValue).Visible = true;
							else if (funcValue[0] == 't') tabs.Tabs.FindTabByValue(funcValue).Visible = true;
						}
					}
					else
					{
						navContainer.Visible = navContainer.Enabled = false;
						navErrorContainer.Visible = navErrorContainer.Enabled = true;
						navErrorContainer.Controls.Add(new Literal { Text = GetLocalizedText("Label.InSufficientPermission") });
					}
				}
			}

			for (int i = 0; i < tabs.Tabs.Count; i++)
			{
				if (tabs.Tabs[i].Visible)
				{
					tabs.Tabs[i].Selected = true;
					pages.SelectedIndex = i;
					break;
				}
			}
		}

		private void FillCombos(RadComboBox combo)
		{
			int themeId = int.Parse(combo.SelectedValue),
				supplierId, categoryId;
			GridEditableItem editItem = (GridEditableItem)combo.NamingContainer;
			//TextBox txtbx = (TextBox)editItem.FindControl("TextBox1");
			object obj;
			try { obj = editItem.GetDataKeyValue("SupplierId"); }
			catch { obj = null; }
			if (!Int32.TryParse(obj != null ? obj.ToString() : "-1", out supplierId))
				supplierId = -1;
			try { obj = editItem.GetDataKeyValue("CategoryId"); }
			catch { obj = null; }
			if (!Int32.TryParse(obj != null ? obj.ToString() : "-1", out categoryId))
				categoryId = -1;
			_suppliers = _context.Companies.Where(a => a.is_supplier.HasValue && a.is_supplier.Value).ToArray();
			TableCell cell = editItem["CpSupplierName"];
			combo = (RadComboBox)cell.FindControl("ddCpSupplier");
			if (combo != null)
			{
				combo.Items.Clear();
				if (themeId != -1)
				{
					foreach (Company supplier in _suppliers.Where(supp => _context.ThemesSuppliers.Count(a => a.companies_Id == supp.companies_Id && a.themes_Id == themeId) != 0))
					{
						combo.Items.Add(new RadComboBoxItem { Text = supplier.companyname, Value = supplier.companies_Id.ToString(), Selected = (supplier.companies_Id == supplierId) });
					}
				}
				else
				{
					foreach (Company supplier in _suppliers.Where(supp => _context.ThemesSuppliers.Count(a => a.companies_Id == supp.companies_Id) != 0))
					{
						combo.Items.Add(new RadComboBoxItem { Text = supplier.companyname, Value = supplier.companies_Id.ToString(), Selected = (supplier.companies_Id == supplierId) });
					}
				}
			}
			_categories = _context.Categories.Where(a => a.active.HasValue && a.active.Value).ToArray();
			cell = editItem["CpCategoryName"];
			combo = (RadComboBox)cell.FindControl("ddCPCategory");
			if (combo != null)
			{
				combo.Items.Clear();
				if (themeId != -1)
				{
					foreach (Category category in _categories.Where(cat => _context.ThemesCategories.Count(a => a.category_Id == cat.categoryId && a.themes_Id == themeId) != 0))
					{
						combo.Items.Add(new RadComboBoxItem { Text = category.categoryText, Value = category.categoryId.ToString(), Selected = (category.categoryId == categoryId) });
					}
				}
				else
				{
					foreach (Category category in _categories.Where(cat => _context.ThemesCategories.Count(a => a.category_Id == cat.categoryId) != 0))
					{
						combo.Items.Add(new RadComboBoxItem { Text = category.categoryText, Value = category.categoryId.ToString(), Selected = (category.categoryId == categoryId) });
					}
				}
			}
		}

		void scriptManager_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
		{
			Info("scriptManager_AsyncPostBackError: " + sender.ToString() + " .Message: " + e.Exception.Message);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			Control docType = this.Page.FindControl("skinDocType");
			if (docType != null)
				((Literal)docType).Text = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";

			if (RadWindowManager.Windows.Count > 0) RadWindowManager.Windows.RemoveAt(0);

			contactsGrid.MasterTableView.NoMasterRecordsText = GetLocalizedText("NoDataAvaiable");
			contactsGrid.MasterTableView.NoDetailRecordsText = GetLocalizedText("NoDataAvaiable");
			cpGrid.MasterTableView.NoMasterRecordsText = GetLocalizedText("NoDataAvaiable");
			cpGrid.MasterTableView.NoDetailRecordsText = GetLocalizedText("NoDataAvaiable");
			contentSetupGrid.MasterTableView.NoMasterRecordsText = GetLocalizedText("NoThemesAvaiable");
			contentSetupGrid.MasterTableView.NoDetailRecordsText = GetLocalizedText("NoThemesAvaiable");
			consumerProfileGrid.MasterTableView.NoMasterRecordsText = GetLocalizedText("NoDataAvaiable");
			consumerProfileGrid.MasterTableView.NoDetailRecordsText = GetLocalizedText("NoDataAvaiable");
			consumerParameterGrid.MasterTableView.NoMasterRecordsText = GetLocalizedText("NoDataAvaiable");
			consumerParameterGrid.MasterTableView.NoDetailRecordsText = GetLocalizedText("NoDataAvaiable");
		}


		private void InitializeToolbar()
		{
			managerToolBar.Items.Add(new RadToolBarButton
			{
				Text = _funcsMan.Funcs[0].Name,
				ImageUrl = ControlPath + "images/sunny.png",
				Value = _funcsMan.Funcs[0].Value,
				Visible = false
			});
			managerToolBar.Items.Add(new RadToolBarButton { IsSeparator = true });
			managerToolBar.Items.Add(new RadToolBarButton
			{
				Text = _funcsMan.Funcs[1].Name,
				ImageUrl = ControlPath + "images/categories.png",
				Value = _funcsMan.Funcs[1].Value,
				Visible = false
			});
			managerToolBar.Items.Add(new RadToolBarButton { IsSeparator = true });
			managerToolBar.Items.Add(new RadToolBarButton
			{
				Text = _funcsMan.Funcs[2].Name,
				ImageUrl = ControlPath + "images/categories.png",
				Value = _funcsMan.Funcs[2].Value,
				Visible = false
			});
			managerToolBar.Items.Add(new RadToolBarButton { IsSeparator = true });
			managerToolBar.Items.Add(new RadToolBarButton
			{
				Text = _funcsMan.Funcs[3].Name,
				ImageUrl = ControlPath + "images/categories.png",
				Value = _funcsMan.Funcs[3].Value,
				Visible = false
			});
			managerToolBar.EnableRoundedCorners = true;
			managerToolBar.ButtonClick += new RadToolBarEventHandler(ControlEventHandler);

			toolBar.Items.Add(new RadToolBarButton { Text = "Save", Width = new Unit(90), ImageUrl = ControlPath + "images/disk_blue.png", Value = "bSave" });
			//toolBar.Items.Add(new RadToolBarButton { Text = "Export", Width = new Unit(90), ImageUrl = ControlPath + "images/document_out.png", Value = "bExport" });
			toolBar.Items.Add(new RadToolBarButton { IsSeparator = true });
			toolBar.EnableRoundedCorners = true;
			toolBar.ButtonClick += new RadToolBarEventHandler(ControlEventHandler);

			cpToolBar.Items.Add(new RadToolBarButton
			{
				Text = GetLocalizedText("Toolbar.AddNewParam"),
				ImageUrl = ControlPath + "images/add2.png",
				Value = "bCpAddNew"
			});
			cpToolBar.ButtonClick += new RadToolBarEventHandler(CpToolBarEventHandler);
		}

		private void InitTabs()
		{
			for (int i = 0, j = 4; i < tabs.Tabs.Count; i++, j++)
			{
				tabs.Tabs[i].Value = _funcsMan.Funcs[j].Value;
				tabs.Tabs[i].Text = _funcsMan.Funcs[j].Name;
			}
		}

		private static void FindCheckboxes(Control control, ref List<CheckBox> checkBoxes)
		{
			foreach (Control c in control.Controls)
			{
				if (c is CheckBox)
					checkBoxes.Add((CheckBox)c);
				if (c.Controls.Count > 0)
					FindCheckboxes(c, ref checkBoxes);
			}
		}

		#region Event Handlers

		protected void UpdateCompanyTheme(Object sender, EventArgs args)
		{
			CheckBox checkBox = sender as CheckBox;

		}

		void CpGridItemDataBound(object sender, GridItemEventArgs e)
		{
			if (e.Item is GridDataItem)
			{
				GridDataItem item = (GridDataItem)e.Item;
				int defaultLngId, fallbackLngId;
				object obj;
				TableCell cell;
				Language lng;

				obj = item.GetDataKeyValue("default_language_Id");
				if (!Int32.TryParse(obj != null ? obj.ToString() : "-1", out defaultLngId))
					defaultLngId = -1;
				obj = item.GetDataKeyValue("fallback_language_Id");
				if (!Int32.TryParse(obj != null ? obj.ToString() : "-1", out fallbackLngId))
					fallbackLngId = -1;

				lng = _languages.FirstOrDefault(a => a.languages_Id == defaultLngId);
				cell = item["DefaultLanguage"];
				if (lng != null)
					((Label)cell.FindControl("lbDefaultLanguage")).Text = lng.description;

				lng = _languages.FirstOrDefault(a => a.languages_Id == fallbackLngId);
				cell = item["FallbackLanguage"];
				if (lng != null)
					((Label)cell.FindControl("lbFallbackLanguage")).Text = lng.description;

			}
			else if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
			{
				GridEditFormItem item = (GridEditFormItem)e.Item;
				object obj;
				int defaultLngId, fallbackLngId;

				obj = item.GetDataKeyValue("default_language_Id");
				if (!Int32.TryParse(obj != null ? obj.ToString() : "-1", out defaultLngId))
					defaultLngId = -1;
				obj = item.GetDataKeyValue("fallback_language_Id");
				if (!Int32.TryParse(obj != null ? obj.ToString() : "-1", out fallbackLngId))
					fallbackLngId = -1;

				TableCell cell = item["DefaultLanguage"];
				DropDownList ddl = (DropDownList)cell.FindControl("ddlDefaultLanguage");
				if (ddl != null)
				{
					ddl.Items.Clear();
					foreach (Language lng in _languages)
					{
						ddl.Items.Add(new ListItem { Text = lng.description, Value = lng.languages_Id.ToString(), Selected = (lng.languages_Id == defaultLngId) });
					}
				}
				cell = item["FallbackLanguage"];
				ddl = (DropDownList)cell.FindControl("ddlFallbackLanguage");
				if (ddl != null)
				{
					ddl.Items.Clear();
					foreach (Language lng in _languages)
					{
						ddl.Items.Add(new ListItem { Text = lng.description, Value = lng.languages_Id.ToString(), Selected = (lng.languages_Id == fallbackLngId) });
					}
				}
			}
		}

		protected void OnCpThemeSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
		{
			FillCombos((RadComboBox)sender);
		}

		protected void consumerProfileGridItemDataBound(object sender, GridItemEventArgs e)
		{
			if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
			{
				GridEditFormItem item = (GridEditFormItem)e.Item;
				object obj;
				int themeId;

				try { obj = item.GetDataKeyValue("ThemeId"); }
				catch { obj = null; }
				if (!Int32.TryParse(obj != null ? obj.ToString() : "-1", out themeId))
					themeId = -1;

				TableCell cell = item["CpThemeName"];
				RadComboBox combo = (RadComboBox)cell.FindControl("ddCpTheme");
				if (combo != null)
				{
					combo.Items.Clear();
					foreach (Theme theme in _themes)
					{
						combo.Items.Add(new RadComboBoxItem { Text = theme.description, Value = theme.themes_Id.ToString(), Selected = (theme.themes_Id == themeId) });
					}
					if (combo.Items.Count > 0)
					{
						combo.SelectedIndex = 0;
						FillCombos(combo);
					}
				}
			}
		}

		protected void GridItemCreatedHandler(object sender, GridItemEventArgs e)
		{
			//Info("GridItemCreatedHandler " + e.Item.ToString());
			if (e.Item is GridDataItem)
			{
				GridDataItem item = (GridDataItem)e.Item;
				CheckBox checkBox = (item.FindControl("cbSelectedTheme") as CheckBox);
				if (checkBox != null)
				{
					checkBox.Checked = Convert.ToBoolean(item.GetDataKeyValue("IsSelected"));
					checkBox.Attributes.Add("tid", item.GetDataKeyValue("ThemeId").ToString());
				}
			}
		}

		void ControlEventHandler(object sender, EventArgs e)
		{
			if (sender is RadToolBar)
			{
				RadToolBarEventArgs evt = (e as RadToolBarEventArgs);
				if (evt != null)
				{
					RadToolBarItem button = evt.Item;
					switch (button.Value)
					{
						case "bSave":
							if (companyGrid.SelectedItems.Count == 0) return;
							if (String.IsNullOrEmpty(hfCid.Value)) return;

							//save general info
							Info("General Info: (" + hfCid.Value + ")" + tbCompanyName.Text);
							Company company = _context.Companies.FirstOrDefault(a => a.companies_Id == Convert.ToInt32(hfCid.Value));
							if (company != null)
							{
								if (company.companyname != tbCompanyName.Text)
								{
									company.companyname = tbCompanyName.Text;
									company.displayname = tbCompanyName.Text;
								}
								if (company.address1 != tbAddress1.Text)
									company.address1 = tbAddress1.Text;
								if (company.address2 != tbAddress2.Text)
									company.address2 = tbAddress2.Text;
								if (company.city != tbCity.Text)
									company.city = tbCity.Text;
								if (company.state != tbState.Text)
									company.state = tbState.Text;
								if (company.zipcode != tbZipcode.Text)
									company.zipcode = tbZipcode.Text;
								if (company.country != tbCountry.Text)
									company.country = tbCountry.Text;
								if (company.is_supplier != cbxIsSupplier.Checked)
									company.is_supplier = cbxIsSupplier.Checked;
								if (company.is_consumer != cbxIsConsumer.Checked)
									company.is_consumer = cbxIsConsumer.Checked;

								_context.SubmitChanges();

							    Log("HERE ***");
								companyGrid.DataBind();
                                
								GridDataItem gdiItem = companyGrid.MasterTableView.FindItemByKeyValue("companies_Id", Convert.ToInt32(hfCid.Value));
								gdiItem.Selected = true;

								// save companies themes
								//save content setup
								List<CheckBox> checkBoxes = new List<CheckBox>();
								FindCheckboxes(pvContentSetup, ref checkBoxes);
								Info("++Content Setup: " + checkBoxes.Count);
								StringBuilder query = new StringBuilder();
								int j = 0;
								object[] parameters = new object[checkBoxes.Count + 1];
								for (int i = 0; i < checkBoxes.Count; i++)
								{
									j = i + 1;
									if (checkBoxes[i].Checked)
										query.AppendLine("if not exists (select 1 from companies_themes a where a.companies_Id = {0} and a.themes_Id = {" + j + "}) insert into companies_themes(companies_Id,themes_Id) values ({0}, {" + j + "})");
									else
										query.AppendLine("delete from companies_themes where companies_Id = {0} and themes_Id = {" + j + "}");
									parameters[j] = Convert.ToInt32(checkBoxes[i].Attributes["tid"]);
								}

								parameters[0] = company.companies_Id;
								try
								{
									_context.CommandTimeout = 60; // 1 min
									_context.ExecuteCommand(query.ToString(), parameters);
								}
								catch (Exception ex)
								{
									Info(ex.Message);
								}

								// Save Consumer Parameters.
								if (companyGrid.SelectedItems.Count > 0)
								{
									int companyId = (int) ((GridDataItem) companyGrid.SelectedItems[0]).GetDataKeyValue("companies_Id");
									foreach (GridDataItem item in consumerParameterGrid.Items)
									{
										int companyParameterTypeId = int.Parse(item.GetDataKeyValue("Id").ToString());
										string value = ((RadTextBox) item["CpgValue"].FindControl("txtCpgValue")).Text;
										CompaniesParameter cp = _context.CompaniesParameters.FirstOrDefault(
											a => a.companies_Id == companyId && a.companies_parameter_types_Id == companyParameterTypeId);
										if (cp != null)
										{
											_context.ExecuteCommand(
												"update companies_parameters set value_text = {0} where companies_Id = {1} and companies_parameter_types_Id = {2}",
												value, companyId, companyParameterTypeId);
										}
										else
										{
											_context.CompaniesParameters.InsertOnSubmit(new CompaniesParameter
											{
											  companies_Id = companyId,
												companies_parameter_types_Id = companyParameterTypeId,
												value_text = value
											});
										}
									}
									_context.SubmitChanges();
								}

								// Notification task.
								commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/information.png";
								commandNotification.Text = GetLocalizedText("DataUpdated");
								commandNotification.Show();
							}
							break;
						case "bExport":
							break;
						case "bAddNewSupplier":
							//AddNewSupplierWindow.OpenerElementID = button.ClientID;
							OpenAddNewSupplierWindow();
							break;
						case "bManageCategories":
							//Redirect to Manage Categories page
							Response.Redirect(DotNetNuke.Common.Globals.NavigateURL(TabId, "Managecategories", "mid", ModuleId.ToString()), true);
							break;
						case "bManageThemes":
							//Redirect to Manage Themes page
							Response.Redirect(DotNetNuke.Common.Globals.NavigateURL(TabId, "Managethemes", "mid", ModuleId.ToString()), true);
							break;
						case "bManageConsumersThemes":
							//Redirect to Manage Consumers Themes page
							Response.Redirect(DotNetNuke.Common.Globals.NavigateURL(TabId, "Manageconsumersthemes", "mid", ModuleId.ToString()), true);
							break;
						default:
							break;
					}
				}
			}
		}

		private void OpenAddNewSupplierWindow()
		{
			RadWindow newWindow = new RadWindow();
			newWindow.NavigateUrl = "AddNewSupplier.aspx";
			newWindow.Title = _funcsMan.Funcs[0].Name;
			newWindow.IconUrl = ControlPath + "images/sunny.png";
			newWindow.VisibleOnPageLoad = true;
			newWindow.DestroyOnClose = true;
			newWindow.Modal = true;
			newWindow.Width = 550;
			newWindow.Height = 350;
			newWindow.CssClass = "nodot";
			newWindow.VisibleStatusbar = false;
			if(RadWindowManager.Windows.Count > 0) RadWindowManager.Windows.RemoveAt(0);
			RadWindowManager.Windows.Add(newWindow);
		}

		protected void CpToolBarEventHandler(object sender, EventArgs e)
		{
			if (sender is RadToolBar)
			{
				RadToolBarEventArgs evt = (e as RadToolBarEventArgs);
				if (evt != null)
				{
					RadToolBarItem button = evt.Item;
					switch (button.Value)
					{
						case "bCpAddNew":
							try
							{
								int companyId = (int)((GridDataItem)companyGrid.SelectedItems[0]).GetDataKeyValue("companies_Id");
								string externalIdentifier = Config.GetSetting("Portal" + PortalId + "ExternalIdentifier");
								if (string.IsNullOrEmpty(externalIdentifier))
								{
									externalIdentifier = Request.Url.Host;
								}
								Channel channel = _context.Channels.SingleOrDefault(a => a.CompanyId == companyId && (a.ExternalIdentifier == externalIdentifier || (a.ExternalIdentifier != "" && a.ExternalIdentifier != null)));
								if(channel != null)
								{
									string baseDomain = DateTime.Now.Ticks.ToString();
									baseDomain += ".";
									baseDomain += channel.BaseDomain;

									_context.CompaniesConsumers.InsertOnSubmit(new CompaniesConsumer
									{
										companies_Id = companyId,
										base_domain = baseDomain,
										base_publication_Id = channel.BasePublicationId,
										base_publication_parameters = channel.DefaultParameters,
										active = true,
										default_language_Id = _context.Languages.First().languages_Id,
										fallback_language_Id = _context.Languages.First().languages_Id
									});
									_context.SubmitChanges();
									cpGrid.Rebind();

									// Notification task.
									commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/information.png";
									commandNotification.Text = GetLocalizedText("DataAddNewed");
									commandNotification.Show();
								}
							}
							catch (Exception)
							{
								// Notification task.
								commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/error.png";
								commandNotification.Text = GetLocalizedText("DataAddNewFailed");
								commandNotification.Show();
							}
							break;
						default:
							break;
					}
				}
			}
		}

		private void GridEventHandler(object sender, EventArgs e)
		{
			if (sender is RadGrid)
			{
				Info(sender.ToString() + "," + e.ToString(), false);
				GridDataItem item = (sender as RadGrid).SelectedItems[0] as GridDataItem;
				int id = (int)item.GetDataKeyValue("companies_Id");
				_company = _context.Companies.FirstOrDefault(a => a.companies_Id == id);

				//rebind the contact values
				contactsGrid.Rebind();
				contentSetupGrid.Rebind();
				cpGrid.Rebind();
				consumerProfileGrid.Rebind();
				consumerParameterGrid.Rebind();
				AssignGeneralInfo(_company);
			}
		}

		private void AssignGeneralInfo(Company company)
		{
			hfCid.Value = _company.companies_Id.ToString();
			tbCompanyName.Text = _company.companyname;
			tbAddress1.Text = _company.address1;
			tbAddress2.Text = _company.address2;
			tbCity.Text = _company.city;
			tbState.Text = _company.state;
			tbZipcode.Text = _company.zipcode;
			tbCountry.Text = _company.country;
			cbxIsSupplier.Checked = _company.is_supplier.Value;
			cbxIsConsumer.Checked = _company.is_consumer.Value;
		}

		protected void NeedDataSourceHandler(object sender, GridNeedDataSourceEventArgs e)
		{
			Info("NeedDataSourceHandler: " + sender.ToString());
			if (sender is RadGrid)
			{
				RadGrid grid = sender as RadGrid;
				Info(grid.ID);
				if (companyGrid.SelectedItems.Count > 0)
				{
					int id = (int)((GridDataItem)companyGrid.SelectedItems[0]).GetDataKeyValue("companies_Id");
					if (grid.ID == "contactsGrid")
					{
						grid.DataSource = _context.Companies.First(a => a.companies_Id == id).Contacts.ToArray();
					}
					else if (grid.ID == "contentSetupGrid")
					{
						var items = _context.ExecuteQuery<CompanyTheme>(GetCompanyThemeQuery, id).ToArray();
						grid.DataSource = items;
					}
					else if (grid.ID == "cpGrid")
					{
						grid.DataSource = _context.CompaniesConsumers.Where(a => a.companies_Id == id).ToArray();
					}
					else if(grid.ID == "consumerProfileGrid")
					{
						var items = _context.ExecuteQuery<ConsumerTheme>(GetConsumerThemeQuery, id).ToArray();
						grid.DataSource = items;
					}
					else if (grid.ID == "consumerParameterGrid")
					{
						CompaniesParameterType[] cpt = _context.CompaniesParameterTypes.Where(a => a.parametertype == 1).OrderBy(a => a.sortorder).ToArray();
						CompaniesParameter[] parameters = _context.CompaniesParameters.Where(a => a.companies_Id == id).ToArray();
						List<ConsumerParameter> companyParameters = new List<ConsumerParameter>();
						CompaniesParameter tmp;

						// filter by editable parameter array only if the user is in CspAdmin. if users are admin, then they see all
						List<string> editableParameters = new List<string>();
						if (ValidCspAdmin() && !string.IsNullOrEmpty(_editableParameterNames))
						{
							editableParameters.AddRange(_editableParameterNames.Split(','));
						}
						foreach (CompaniesParameterType parameterType in cpt)
						{
							if (ValidCspAdmin() && editableParameters.Count > 0 && editableParameters.Count(a => a.Equals(parameterType.parametername, StringComparison.OrdinalIgnoreCase)) == 0)
								continue;

							tmp = parameters.FirstOrDefault(a => a.companies_parameter_types_Id == parameterType.companies_parameter_types_Id);
							companyParameters.Add(new ConsumerParameter
							{
								Id = parameterType.companies_parameter_types_Id,
								CId = id,
								Name = GetLocalizeString(parameterType.parametername, LocalResourceFile),
								Value = (tmp != null) ? tmp.value_text : ""
							});
						}
						grid.DataSource = companyParameters;
					}
				}
				else
				{
					grid.DataSource = new string[] { };
				}
			}
		}

		protected void OnCpGridItemCommand(object sender, GridCommandEventArgs e)
		{
			if (e.CommandName == RadGrid.PerformInsertCommandName)
			{
				try
				{
					int companyId = (int)((GridDataItem)companyGrid.SelectedItems[0]).GetDataKeyValue("companies_Id");
					GridEditableItem item = (GridEditableItem)e.Item;
					TableCell cell;
					int themeId = -1;
					cell = item["CpThemeName"];
					RadComboBox combo = (RadComboBox)cell.FindControl("ddCpTheme");
					if (combo != null && !String.IsNullOrEmpty(combo.SelectedValue))
					{
						themeId = Convert.ToInt32(combo.SelectedValue);
					}
					int supplierId = -1;
					cell = item["CpSupplierName"];
					combo = (RadComboBox)cell.FindControl("ddCpSupplier");
					if (combo != null && !String.IsNullOrEmpty(combo.SelectedValue))
					{
						supplierId = Convert.ToInt32(combo.SelectedValue);
					}
					int categoryId = -1;
					cell = item["CpCategoryName"];
					combo = (RadComboBox)cell.FindControl("ddCpCategory");
					if (combo != null && !String.IsNullOrEmpty(combo.SelectedValue))
					{
						categoryId = Convert.ToInt32(combo.SelectedValue);
					}
					ConsumersTheme ct =
						_context.ConsumersThemes.SingleOrDefault(
							a => a.consumer_Id == companyId && a.themes_Id == themeId && a.supplier_Id == supplierId && a.category_Id == categoryId);
					if (ct == null)
					{
						int maxId = (from cth in _context.ConsumersThemes select (cth.consumers_themes_Id)).Max();
						_context.ConsumersThemes.InsertOnSubmit(new ConsumersTheme
						{
							consumers_themes_Id = maxId + 1,
							consumer_Id = companyId,
							themes_Id = themeId,
							supplier_Id = supplierId,
							category_Id = categoryId
						});
						_context.SubmitChanges();

						// Notification task.
						commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/information.png";
						commandNotification.Text = GetLocalizedText("DataAddNewed");
						commandNotification.Show();
					}
				}
				catch
				{
					// Notification task.
					commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/warning.png";
					commandNotification.Text = GetLocalizedText("NeedPickCompanyFirst");
					commandNotification.Show();
				}
			}
			else if (e.CommandName == RadGrid.DeleteCommandName)
			{
				GridEditableItem item = (GridEditableItem)e.Item;
				int id;
				try { id = Convert.ToInt32(item.GetDataKeyValue("ConsumerThemeId")); }
				catch { id = -1; }
				_context.ConsumersThemes.DeleteAllOnSubmit(_context.ConsumersThemes.Where(a => a.consumers_themes_Id == id));
				_context.SubmitChanges();

				// Notification task.
				commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/information.png";
				commandNotification.Text = GetLocalizedText("DataDeleted");
				commandNotification.Show();
			}
		}

		protected void GridCommandHandler(object sender, GridCommandEventArgs e)
		{
			if (sender is RadGrid)
			{
				RadGrid grid = (RadGrid)sender;
				if (grid.ID == "contactsGrid")
				{
					switch (e.CommandName)
					{
						case "Update":
							//GridDataItem item = ((GridEditFormItem) e.Item).ParentItem;
							GridEditableItem item = (GridEditableItem)e.Item;
							Contact contact = _context.Contacts.FirstOrDefault(a => a.companies_contacts_Id == (int)item.GetDataKeyValue("companies_contacts_Id") &&
																																			a.companies_Id == (int?)item.GetDataKeyValue("companies_Id"));
							if (contact != null)
							{
								item.UpdateValues(contact);
								_context.SubmitChanges();

								// Notification task.
								commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/information.png";
								commandNotification.Text = GetLocalizedText("DataUpdated");
								commandNotification.Show();
							}
							break;
						default:
							break;
					}
				}
				else if (grid.ID == "cpGrid")
				{
					switch (e.CommandName)
					{
						case "Update":
							GridEditableItem item = (GridEditableItem) e.Item;
							int id = Convert.ToInt32(item.GetDataKeyValue("companies_consumers_Id"));
							CompaniesConsumer cc = _context.CompaniesConsumers.SingleOrDefault(a => a.companies_consumers_Id == id);
							if (cc != null)
							{
								item.UpdateValues(cc);
								TableCell cell;
								DropDownList ddl;

								cell = item["DefaultLanguage"];
								ddl = (DropDownList)cell.FindControl("ddlDefaultLanguage");
								if (ddl != null && !String.IsNullOrEmpty(ddl.SelectedValue))
								{
									cc.default_language_Id = Convert.ToInt32(ddl.SelectedValue);
								}

								cell = item["FallbackLanguage"];
								ddl = (DropDownList)cell.FindControl("ddlFallbackLanguage");
								if (ddl != null && !String.IsNullOrEmpty(ddl.SelectedValue))
								{
									cc.fallback_language_Id = Convert.ToInt32(ddl.SelectedValue);
								}
								_context.SubmitChanges();

								// Notification task.
								commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/information.png";
								commandNotification.Text = GetLocalizedText("DataUpdated");
								commandNotification.Show();
							}
							break;
						default:
							break;
					}
				}
				else if(grid.ID == "consumerProfileGrid")
				{
					switch (e.CommandName)
					{
						case "Update":
							GridEditableItem item = (GridEditableItem) e.Item;
							int id;
							try { id = Convert.ToInt32(item.GetDataKeyValue("ConsumerThemeId")); }
							catch { id = -1; }
							ConsumersTheme ct = _context.ConsumersThemes.SingleOrDefault(a => a.consumers_themes_Id == id);
							if(ct != null)
							{
								item.UpdateValues(ct);
								TableCell cell;
								cell = item["CpThemeName"];
								RadComboBox combo = (RadComboBox)cell.FindControl("ddCpTheme");
								if (combo != null && !String.IsNullOrEmpty(combo.SelectedValue))
								{
									ct.themes_Id = Convert.ToInt32(combo.SelectedValue);
								}
								cell = item["CpSupplierName"];
								combo = (RadComboBox)cell.FindControl("ddCpSupplier");
								if (combo != null && !String.IsNullOrEmpty(combo.SelectedValue))
								{
									ct.supplier_Id = Convert.ToInt32(combo.SelectedValue);
								}
								cell = item["CpCategoryName"];
								combo = (RadComboBox)cell.FindControl("ddCpCategory");
								if (combo != null && !String.IsNullOrEmpty(combo.SelectedValue))
								{
									ct.category_Id = Convert.ToInt32(combo.SelectedValue);
								}
								_context.SubmitChanges();

								// Notification task.
								commandNotification.ContentIcon = GetModulePathUrl() + "Images/32x32/information.png";
								commandNotification.Text = GetLocalizedText("DataUpdated");
								commandNotification.Show();
							}
							break;
						default:
							break;
					}
				}
			}
			Info("GridCommandHandler: " + e.CommandName);
		}
		#endregion

		#region Implementation of IActionable

		public ModuleActionCollection ModuleActions
		{
			get
			{
				ModuleActionCollection Actions = new ModuleActionCollection();
				try
				{
					Actions.Add(
						GetNextActionID(),
						"Settings",
						ModuleActionType.AddContent, "", "", EditUrl("Permissions"), false, SecurityAccessLevel.Edit, true, false);
				}
				catch
				{
					// added exception to prevent error in case you guys dont have the newest Permssion Page
				}
				return Actions;
			}
		}

		#endregion

		#region Utilities
		private void Info(string message)
		{
			Info(message, false);
		}
		private void Info(string message, bool isNew)
		{
			if (isNew)
				log.Text = "<p>" + message + "</p>";
			else
				log.Text += "<p>" + message + "</p>";
		}
		protected string GetLocale(string name)
		{
			return Localization.GetString(name, LocalResourceFile) ?? name;
		}

		protected bool ValidCspAdmin()
		{
			return ValidRoles("CspSuperAdmin,CspAdmin");
		}

		protected bool ValidRoles(string r)
		{
			string[] roles = r.Split(',');
			bool f = false;
			foreach (string role in roles)
			{
				f = f || UserInfo.IsInRole(role);
			}
			return f;
		}

		private static string GetLocalizeString(string parametername, string resourceFile)
		{
			string value = Localization.GetString(parametername, resourceFile);
			return string.IsNullOrEmpty(value) ? parametername : value;
		}
		#endregion

		/// <summary>
		/// Gets the module path URL.
		/// </summary>
		/// <returns></returns>
		protected string GetModulePathUrl()
		{
			return ControlPath;
		}
	}
}