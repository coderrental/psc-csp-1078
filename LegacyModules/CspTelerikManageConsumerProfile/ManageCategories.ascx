﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageCategories.ascx.cs" Inherits="CspTelerikManageConsumerProfile.ManageCategories" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadAjaxLoadingPanel ID="ajaxLoadingPanel" runat="server" CssClass="ajax-loading-panel" IsSticky="True"></telerik:RadAjaxLoadingPanel>
<telerik:RadToolBar ID="toolBar" OnButtonClick="toolBar_OnButtonClick" runat="server" EnableRoundedCorners="true" EnableShadows="true" Width="100%"></telerik:RadToolBar>
<!-- Tree list of categories -->
<telerik:RadAjaxPanel ID="ajaxPanelTree" LoadingPanelID="ajaxLoadingPanel"  runat="server">
	<telerik:RadContextMenu ID="radContextMenu" runat="server" OnClientItemClicked="onClientItemClicked">
        <Items>
            <telerik:RadMenuItem Text="Move to Root category" Value="MoveRoot" />
        </Items>
    </telerik:RadContextMenu>
	<telerik:RadTreeList EditMode="PopUp" AllowRecursiveDelete="true" OnItemCommand="treeCategories_OnItemCommand" OnInsertCommand="treeCategories_OnInsertCommand" OnUpdateCommand="treeCategories_OnUpdateCommand" ID="treeCategories" AutoGenerateColumns="False" DataKeyNames="categoryId" ParentDataKeyNames="parentId" runat="server" DataSourceID="dsCategories">
		<Columns>
			<telerik:TreeListBoundColumn DataField="categoryId" UniqueName="categoryId" HeaderText="Category Id" ReadOnly="True" ForceExtractValue="Always" HeaderStyle-Width="70px" />
			<telerik:TreeListBoundColumn DataField="parentId" UniqueName="parentId" HeaderText="Parent Id" ReadOnly="True" ForceExtractValue="Always" />
			<telerik:TreeListTemplateColumn DataField="categoryText" UniqueName="categoryText" HeaderText="Name" >
				<ItemTemplate>
					<asp:Label ID="lbCategoryText" Text='<%# Eval("categoryText")%>' runat="server" />
					<telerik:RadToolTip ID="ttCategoryText" TargetControlID="lbCategoryText" Text='<%# Eval("categoryText")%>' runat="server"></telerik:RadToolTip>
				</ItemTemplate>
			</telerik:TreeListTemplateColumn>
			<telerik:TreeListTemplateColumn DataField="description" UniqueName="description" HeaderText="Description" >
				<ItemTemplate>
					<asp:Label ID="lbDesc" Text='<%# Eval("description")%>' runat="server" />
					<telerik:RadToolTip ID="ttDesc" TargetControlID="lbDesc" Text='<%# Eval("description")%>' runat="server"></telerik:RadToolTip>
				</ItemTemplate>
			</telerik:TreeListTemplateColumn>
			<telerik:TreeListCheckBoxColumn DefaultInsertValue="true" DataField="active" UniqueName="active" HeaderText="Active" />
			<telerik:TreeListCheckBoxColumn DefaultInsertValue="true" DataField="IsSubscribable" UniqueName="IsSubscribable" HeaderText="Is Subscribable" />
			<telerik:TreeListTemplateColumn DataField="DisplayOrder" UniqueName="DisplayOrder" HeaderText="Display Order" >
				<ItemTemplate>
					<asp:Label ID="lbDisplayOrder" Text='<%# Eval("DisplayOrder")%>' runat="server" />
					<telerik:RadToolTip ID="ttDisplayOrder" TargetControlID="lbDisplayOrder" Text='<%# Eval("DisplayOrder")%>' runat="server"></telerik:RadToolTip>
				</ItemTemplate>
			</telerik:TreeListTemplateColumn>
			<telerik:TreeListTemplateColumn  DataField="publication_schemeID" UniqueName="publication_schemeID" HeaderText="Publication Scheme">
				<ItemTemplate>
					<asp:Label ID="lbSchemeName" Text='<%# Eval("schemeName")%>' runat="server" />
					<telerik:RadToolTip ID="ttSchemeName" TargetControlID="lbSchemeName" Text='<%# Eval("schemeName")%>' runat="server"></telerik:RadToolTip>
				</ItemTemplate>
			</telerik:TreeListTemplateColumn>
			<telerik:TreeListCheckBoxColumn DefaultInsertValue="false" DataField="apply_to_childs" UniqueName="apply_to_childs" HeaderText="Apply to Childs"/>
			<telerik:TreeListTemplateColumn DataField="ExternalCategoryCode" UniqueName="ExternalCategoryCode" HeaderText="External Category Code">
				<ItemTemplate>
					<asp:Label ID="lbExternalCategoryCode" Text='<%# Eval("ExternalCategoryCode")%>' runat="server" />
					<telerik:RadToolTip ID="ttExternalCategoryCode" TargetControlID="lbExternalCategoryCode" Text='<%# Eval("ExternalCategoryCode")%>' runat="server"></telerik:RadToolTip>
				</ItemTemplate>
			</telerik:TreeListTemplateColumn>
			<%--<telerik:TreeListButtonColumn UniqueName="DeleteCommandColumn" Text="Delete" CommandName="Delete" ButtonType="ImageButton" HeaderStyle-Width="30px" />--%>
			<telerik:TreeListEditCommandColumn UniqueName="EditColumn" HeaderText="Edit Category" ButtonType="ImageButton" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Center"/>
		</Columns>
		<EditFormSettings EditFormType="Template">
		    <PopUpSettings Modal="False" ZIndex="0">
		    </PopUpSettings>
            <FormTemplate>
                <table class="listEditForm">
                    <tr class="EditFormHeader">
                        <td colspan="2" style="font-size: small">
                            <b>Category Detail</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top">
                            <table class="module">
                                <tr>
                                    <td class="crlablel">
                                        Category Text:
                                    </td>
                                    <td class="crvalue">
                                    	<asp:TextBox ID="tbxCategoryText" Text='<%# Bind("categoryText") %>' runat="server"/>
										<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tbxCategoryText" ErrorMessage="Please enter value" />
                                    </td>
                                </tr>
                                <tr>
                                    <td  class="crlablel">
                                        Description:
                                    </td>
                                    <td class="crvalue">
                                    	<asp:TextBox ID="tbxCategoryDesc" Text='<%# Bind("description") %>' runat="server"/>
                                    </td>
                                </tr>
								<tr>
                                    <td class="crlablel">
                                        Active :
                                    </td>
                                    <td class="crvalue">
                                    	<asp:CheckBox ID="chbxActive" Checked='<%# IsTrue(Eval("active")) %>' runat="server"/>
                                    </td>
                                </tr>
								<tr>
                                    <td class="crlablel">
                                        Is Subscribable :
                                    </td>
                                    <td class="crvalue">
                                    	<asp:CheckBox ID="chbxIsSubscribable" Checked='<%# IsTrue(Eval("IsSubscribable")) %>' runat="server"/>
                                    </td>
                                </tr>
								<tr>
                                    <td  class="crlablel">
                                        Display Order:
                                    </td>
                                    <td class="crvalue">
                                    	<asp:TextBox ID="tbxDisplayOrder" Text='<%# Bind("DisplayOrder") %>' runat="server"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="crlablel">
                                        Publication Scheme:
                                    </td>
                                    <td class="crvalue">
                                    	<telerik:RadComboBox ID="cbbShemes" runat="server" DataTextField="description" DataValueField="publication_schemes_Id" DataSourceID="dsScheme" AppendDataBoundItems="True" SelectedValue='<%# Bind("publication_schemeID") %>' />
                                    </td>
                                </tr>
								<tr>
                                    <td class="crlablel">
                                        Apply to Childs :
                                    </td>
                                    <td class="crvalue">
                                    	<asp:CheckBox ID="chbxApplyToChild" Checked='<%# IsTrue(Eval("apply_to_childs")) %>' runat="server"/>
                                    </td>
                                </tr>
								<tr>
                                    <td class="crlablel">
                                        External Category Code:
                                    </td>
                                    <td class="crvalue">
                                    	<asp:TextBox ID="tbxExternalCode" Text='<%# Bind("ExternalCategoryCode") %>' runat="server"/>
										<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="tbxExternalCode" ErrorMessage='<%# GetLocalizedText("Label.PleaseEnterValue") %>' />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        	<telerik:RadButton ID="btnUpdate" Text='<%# (Container is TreeListEditFormInsertItem) ? "Insert" : "Update" %>' runat="server" CommandName='<%# (Container is TreeListEditFormInsertItem) ? "PerformInsert" : "Update" %>' Icon-PrimaryIconCssClass="rbOk"/>
                            &nbsp;
                        	<telerik:RadButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False" CommandName="Cancel" Icon-PrimaryIconCssClass="rbCancel"/>        
                        </td>
                    </tr>
                </table>
            </FormTemplate>
        </EditFormSettings>
		<ClientSettings AllowItemsDragDrop="true">
            <Selecting AllowItemSelection="True" />
            <ClientEvents OnItemDropping="itemDropping" OnItemDragging="itemDragging" OnTreeListCreated="treeListCreated" OnItemContextMenu="onItemContextMenu" />
        </ClientSettings>
	</telerik:RadTreeList>
</telerik:RadAjaxPanel>
<!-- Datasource -->
<asp:SqlDataSource ID="dsCategories" runat="server">
	<UpdateParameters>
		<asp:Parameter Name="parentId" Type="Int32"/>
		<asp:Parameter Name="categoryId" Type="Int32" />
    </UpdateParameters>
	<InsertParameters>
		<asp:Parameter Name="parentId" Type="Int32"/>
        <asp:Parameter Name="categoryText" Type="String" />
        <asp:Parameter Name="description" Type="String" />
        <asp:Parameter Name="active" Type="Boolean" />
		<asp:Parameter Name="publication_schemeID" Type="Int32" />
        <asp:Parameter Name="apply_to_childs" Type="Boolean" />
        <asp:Parameter Name="ExternalCategoryCode" Type="String" />
    </InsertParameters>
	<DeleteParameters>
		<asp:Parameter Name="categoryId" Type="Int32" />
	</DeleteParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="dsScheme" runat="server"/>
<!-- Tooltip manager -->
<telerik:RadToolTipManager ID="ttManager" runat="server"/>
<telerik:RadScriptBlock runat="server">
	<script type="text/javascript">
		//center the popup edit form
		(function ($) {
		    $.fn.center = function () {
		        this.css("position", "fixed");
		        //this.css("top", (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");
		        //this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
		        this.css("top", (($(window).height() - this.outerHeight()) / 2) + "px");
		        this.css("left", (($(window).width() - this.outerWidth()) / 2) + "px");
		        return this;
		    }
		})($telerik.$);


            function treeListCreated(sender, args) {

            $telerik.$(".rtlEditForm", sender.get_element()).center();

		    $('<%= treeCategories.ClientID %>_ModalPopUpMask').css("z-index", "-1");
		}

		
		/**************************** Functions for Drag-and-drop ******************************/
		function findParentItem(element) {
			if (element.tagName.toLowerCase() == "html")
				return null;
			while (!(element.id != "" && typeof element.tagName != "undefined" && element.tagName.toLowerCase() == "tr")) {
				if (element.parentNode == null)
					return null;
				element = element.parentNode;
			}
			return element;
		}

		function itemDragging(sender, args) {

			var dropClue = $telerik.findElement(args.get_draggedContainer(), "DropClue");
			args.set_dropClueVisible(true); //drop clue is always visible

			if (!args.get_canDrop()) 
			{
				dropClue.className = "dropClue dropDisabled";
				return;
			}
			var className = "dropEnabled";
			args.set_canDrop(true);
			dropClue.className = "dropClue " + className;
		}

		function itemDropping(sender, args) {
			var targetRow = findParentItem(args.get_destinationHtmlElement());

			if (targetRow == null) {
				args.set_cancel(true);
				return;
			}

			if ($telerik.isDescendant(sender.get_element(), targetRow))
				return;

			var itm = args.get_draggedItems()[0];
			var categoryId = $find(targetRow.id).get_dataKeyValue("categoryId");
			alert(categoryId);
			itm.fireCommand("CustomItemsDropped", categoryId); //Fire custom command
		}

		//Function for display context menu
		var currentIndex = -1;
		function onClientItemClicked(sender, args) {
			var commandName = args.get_item().get_value();
			$find('<%= treeCategories.ClientID %>').fireCommand(commandName, currentIndex);
		}

		function onItemContextMenu(sender, args) {
			var evTarget = args.get_domEvent().target;
			if (evTarget.tagName.toLowerCase() == "td") {
				args.get_item().set_selected(true);
				currentIndex = args.get_item().get_displayIndex();
				$find('<%= radContextMenu.ClientID %>').show(args.get_domEvent());
			}
		}
	</script>
</telerik:RadScriptBlock>

