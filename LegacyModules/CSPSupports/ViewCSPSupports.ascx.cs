/*
' DotNetNuke� - http://www.dotnetnuke.com
' Copyright (c) 2002-2006
' by Perpetual Motion Interactive Systems Inc. ( http://www.perpetualmotion.ca )
'
' Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
' documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
' the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
' to permit persons to whom the Software is furnished to do so, subject to the following conditions:
'
' The above copyright notice and this permission notice shall be included in all copies or substantial portions 
' of the Software.
'
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
' DEALINGS IN THE SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Web;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using DesktopModules.CSPModules.CspCommons;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Users;
using DotNetNuke.Services.Localization;
using DotNetNuke.Services.Mail;

namespace Tie.Kinetix.Modules.CSPSupports
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The ViewCSPSupports class displays the content
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <history>
    /// </history>
    /// -----------------------------------------------------------------------------
    partial class ViewCSPSupports : PortalModuleBase
    {
        private UserInfo _curUser;
        public string tmpResourcePath;
		public string RCaptchaPrivateKey = "";
		public string RCaptchaPublicKey = "";
		//public string needToHide;
		
		protected bool ShowSocialManual
        {
			get
            {
                string s = Config.GetSetting("Portal" + PortalId + "ShowSocialManual");
                return (!string.IsNullOrEmpty(s) && s.Equals("true", StringComparison.OrdinalIgnoreCase));

            }
        }
		
		protected bool ShowBestPractises
        {
			get
            {
                string s = Config.GetSetting("Portal" + PortalId + "ShowBestPractises");
                return (!string.IsNullOrEmpty(s) && s.Equals("true", StringComparison.OrdinalIgnoreCase));

            }
        }
		
		protected bool ShowFaqSection
        {
			get
            {
                string s = Config.GetSetting("Portal" + PortalId + "ShowFaqSection");
                return (!string.IsNullOrEmpty(s) && s.Equals("true", StringComparison.OrdinalIgnoreCase));

            }
        }
		
		protected bool ShowInstructionsVideo
        {
			get
            {
                string s = Config.GetSetting("Portal" + PortalId + "ShowInstructionsVideo");
                return (!string.IsNullOrEmpty(s) && s.Equals("true", StringComparison.OrdinalIgnoreCase));

            }
        }		

        protected bool ShowDownloadInstruction
        {
            get
            {
                string s = Config.GetSetting("Portal" + PortalId + "ShowDownloadInstruction");
                return (!string.IsNullOrEmpty(s) && s.Equals("true", StringComparison.CurrentCultureIgnoreCase));
            }
        }
        protected string DownloadPath
        {
            get { return Localization.GetString("DownloadPath", LocalResourceFile); }
        }

        protected string DownloadImagePath
        {
            get { return Localization.GetString("DownloadImagePath", LocalResourceFile); }
        }		
		
		protected string faqDownloadFilePath
        {
            get { return Localization.GetString("faqDownloadFilePath", LocalResourceFile); }
        }

        protected string faqDownloadButtonPath
        {
            get { return Localization.GetString("faqDownloadButtonPath", LocalResourceFile); }
        }

        protected string instructionsVideoPath
        {
            get { return Localization.GetString("instructionsVideoPath", LocalResourceFile); }
        }
		protected string instructionsVideoButtonPath
        {
            get { return Localization.GetString("instructionsVideoButtonPath", LocalResourceFile); }
        }

        protected string bestPractisesFilePath
        {
            get { return Localization.GetString("bestPractisesFilePath", LocalResourceFile); }
        }
		protected string bestPractisesButtonPath
        {
            get { return Localization.GetString("bestPractisesButtonPath", LocalResourceFile); }
        }		

		protected string socialManualFilePath
        {
            get { return Localization.GetString("socialManualFilePath", LocalResourceFile); }
        }
		protected string socialManualButtonPath
        {
            get { return Localization.GetString("socialManualButtonPath", LocalResourceFile); }
        }	
		
		protected void Page_Init(object sender, EventArgs e)
		{
			if (Settings.ContainsKey("SETTING_RECAPTCHA_PUBLICKEY") && Settings["SETTING_RECAPTCHA_PUBLICKEY"].ToString() != string.Empty)
				RCaptchaPublicKey = Settings["SETTING_RECAPTCHA_PUBLICKEY"].ToString();
			if (Settings.ContainsKey("SETTING_RECAPTCHA_PRIVATEKEY") && Settings["SETTING_RECAPTCHA_PRIVATEKEY"].ToString() != string.Empty)	
				RCaptchaPrivateKey = Settings["SETTING_RECAPTCHA_PRIVATEKEY"].ToString();
		}
		
        protected void Page_Load(object sender, EventArgs e)
        {
			bool captchaBlockState = false;
			tmpResourcePath = LocalResourceFile.Substring(0, LocalResourceFile.LastIndexOf("/")) + "/CSPSupports";
            if (!this.Page.IsPostBack)
            {
                // get current user
                _curUser = UserController.GetCurrentUserInfo();

                Control docType = Page.FindControl("skinDocType");
                if (docType != null)
                {
                    ((Literal) docType).Text =
                        "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";
                }

                // register client side javascript libs
				/*
                Page.ClientScript.RegisterClientScriptInclude("jquery",
                                                              "http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js");
                Page.ClientScript.RegisterClientScriptInclude("jquery-ui.min",
                                                              "http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js");
				*/
				/*
                Page.ClientScript.RegisterClientScriptInclude("jquery-blockui",
                                                              ResolveUrl("~/js/external/blockui.js"));
				*/
				Page.ClientScript.RegisterClientScriptInclude("jquery-blockui", ControlPath + "js/blockui.js");
                //Page.ClientScript.RegisterClientScriptInclude("jquery-colorbox",
                //                                              ResolveUrl("~/js/external/jquery.colorbox-min.js"));

                //Globals.AddCssReference(this, "colorbox.css");

                // localization
                lbSupportMessage.Text = Localization.GetString(lbSupportMessage.ID, tmpResourcePath);
                lbCompanyName.Text = Localization.GetString(lbCompanyName.ID, tmpResourcePath);
                lbContactName.Text = Localization.GetString(lbContactName.ID, tmpResourcePath);
                lbEmail.Text = Localization.GetString(lbEmail.ID, tmpResourcePath);
                lbMessage.Text = Localization.GetString(lbMessage.ID, tmpResourcePath);
                lbPhone.Text = Localization.GetString(lbPhone.ID, tmpResourcePath);
                lbSubject.Text = Localization.GetString(lbSubject.ID, tmpResourcePath);
                bSubmit.Text = Localization.GetString(bSubmit.ID, tmpResourcePath);
				
				lbUserName.Text=Localization.GetString(lbUserName.ID, tmpResourcePath);
				
				if(UserInfo.Username != null)
				{	
					 tbUser.Text= UserInfo.Username;
					//tbUser.Text= lbUserName.Text;
				}
				else
				{
					tbUser.Text=lbUserName.Text;
				}

                Control ctrl = this.ContainerControl.FindControl("dnnTitle");
                if (ctrl != null)
                {
                    string controlTitle = Localization.GetString("lblTitle", tmpResourcePath);
                    Label lblTitle = (Label)ctrl.FindControl("lblTitle");
                    if (lblTitle != null)
                        //if (lblTitle.Text.Equals("") || lblTitle.Text.Equals("&nbsp;"))
                        if (!string.IsNullOrEmpty(controlTitle))
                            lblTitle.Text = controlTitle;
                }
            }
			if (!Request.IsAuthenticated)
			{
				if (!String.IsNullOrEmpty(RCaptchaPublicKey) && !String.IsNullOrEmpty(RCaptchaPrivateKey))
					captchaBlockState = true;
				Page.ClientScript.RegisterClientScriptBlock(GetType(), "reCaptchaTemplate", "var RecaptchaOptions = { theme : 'clean' };", true);
			}
			pluginReCaptcha.Visible = captchaBlockState;
        }

        protected void OnSubmitCickEvent(object sender, EventArgs e)
        {
			//reCaptcha Validation for in-logined user
			if (!Request.IsAuthenticated && !String.IsNullOrEmpty(RCaptchaPublicKey) && !String.IsNullOrEmpty(RCaptchaPrivateKey))
			{
				string reCaptChaResponseString = DoReCaptChaValidation();
				Dictionary<string, string> CaptchaErrorCodes = new Dictionary<string, string>();
				CaptchaErrorCodes.Add("incorrect-captcha-sol", Localization.GetString("Msg.reCaptchaIncorrect", tmpResourcePath));
				CaptchaErrorCodes.Add("invalid-site-private-key", Localization.GetString("Msg.reCaptchaInvalidPrivateKey", tmpResourcePath));
				CaptchaErrorCodes.Add("captcha-timeout", Localization.GetString("Msg.reCaptchaTimeout", tmpResourcePath));
				if (!reCaptChaResponseString.StartsWith("true"))
				{
					string [] errorCode = reCaptChaResponseString.Split();
					if (errorCode.Length > 0)
					{
						//lbCaptchaMsg.Text = CaptchaErrorCodes[errorCode[errorCode.Length - 1]];
						lbCaptchaMsg.Text = CaptchaErrorCodes.ContainsKey(errorCode[errorCode.Length - 1]) == true ? CaptchaErrorCodes[errorCode[errorCode.Length - 1]] : reCaptChaResponseString;
					}
					return;
				}
			}
			
            string msg = string.Format(
                "Name: {0}\nEmail: {1}\nTelephone: {2}\nSubject: {3}\nMessage:\n {4}\nCompany Name:\n {5}\nUser Name:\n{6}",
               tbEmail.Text, tbName.Text, tbTelephone.Text, tbSubject.Text,
                tbMessage.Text,tbCompany.Text,tbUser.Text);
            if (msg.Length > 5000) msg = msg.Substring(0, 5000);
			//Page.ClientScript.RegisterClientScriptBlock(GetType(), "reCaptchaSubmit", String.Format("console.log('{0}', '{1}');", rcChallengeField.Value, rcResponseField.Value), true);

			try
            {
                string supportEmail = Config.GetSetting("Portal"+PortalId+"SupportEmail");
                if (string.IsNullOrEmpty(supportEmail))
                {
                    supportEmail = Config.GetSetting("DefaultPortalSupportEmail");
                    if (string.IsNullOrEmpty(supportEmail)) supportEmail = "lam.tu@tiekinetix.com";
                }
				/*
                Mail.SendMail(supportEmail,
                              supportEmail,
                              "lam.tu@tiekinetix.com", "Support Email",
                              string.Format(
                                  "Name: {0}\nEmail: {1}\nTelephone: {2}\nSubject: {3}\nMessage:\n{4}",
                                  tbEmail.Text, tbName.Text, tbTelephone.Text, tbSubject.Text, tbMessage.Text),
                              "",
                              "", Globals.SMTPHost,
                              Globals.SMTPAuthentication,
                              Globals.SMTPUsername,
                              Globals.SMTPPassword);
				*/
				Mail.SendMail(supportEmail, supportEmail, "lam.tu@tiekinetix.com", "Support Email", string.Format(
                                  "<strong>Name:</strong> &nbsp;{0}<br/><strong>Email:</strong> &nbsp;{1}<br/><strong>Telephone:</strong> &nbsp;{2}<br/><strong>Subject:</strong> &nbsp;{3}<br/><strong>Message:</strong> &nbsp;{4}<br/><strong>Company Name:</strong> &nbsp;{5}<br/><strong>User Name:</strong> &nbsp;{6}",
                                  tbName.Text, tbEmail.Text, tbTelephone.Text, tbSubject.Text, tbMessage.Text, tbCompany.Text, tbUser.Text), "", "HTML", "", "", "", "");
				/*
                using (DnnDataLayerDataContext context = new DnnDataLayerDataContext(Config.GetConnectionString()))
                {
                    if (_curUser == null)
                        _curUser = UserController.GetCurrentUserInfo();

                    CspErrorLog errorLog = new CspErrorLog
                                               {
                                                   userId = _curUser != null? _curUser.UserID : -1,
                                                   date = DateTime.Now,
                                                   error = msg,
                                                   extra = string.Empty
                                               };
                    context.CspErrorLogs.InsertOnSubmit(errorLog);
                    context.SubmitChanges(ConflictMode.ContinueOnConflict);
                }
				*/
				
				Response.Redirect(EditUrl("ServerMessage"), false);
            }
            catch (Exception ex)
            {
                using (DnnDataLayerDataContext context = new DnnDataLayerDataContext(Config.GetConnectionString()))
                {
                    CspErrorLog errorLog = new CspErrorLog
                                               {
                                                   userId = _curUser.UserID,
                                                   date = DateTime.Now,
                                                   error = msg,
                                                   extra = ex.StackTrace
                                               };
                    context.CspErrorLogs.InsertOnSubmit(errorLog);
                    context.SubmitChanges(ConflictMode.ContinueOnConflict);
                }
                lbErrorMsg.Text = Localization.GetString(lbErrorMsg.ID, tmpResourcePath);
            }
        }
		
		private String DoReCaptChaValidation()
		{
			string ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
			string reCaptChaResponseString = "";
            if (string.IsNullOrEmpty(ip))
            {
                ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
			string reCaptchaPrivateKey = RCaptchaPrivateKey;
			 // Create a request using a URL that can receive a post. 
            WebRequest request = WebRequest.Create ("http://www.google.com/recaptcha/api/verify");
            // Set the Method property of the request to POST.
            request.Method = "POST";
            // Create POST data and convert it to a byte array.
            string postData = String.Format("privatekey={0}&remoteip={1}&challenge={2}&response={3}&error=incorrect-captcha-sol", reCaptchaPrivateKey, ip, rcChallengeField.Value, rcResponseField.Value);
            byte[] byteArray = Encoding.UTF8.GetBytes (postData);
            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded";
            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;
            // Get the request stream.
            Stream dataStream = request.GetRequestStream ();
            // Write the data to the request stream.
            dataStream.Write (byteArray, 0, byteArray.Length);
            // Close the Stream object.
            dataStream.Close ();
            // Get the response.
            WebResponse response = request.GetResponse ();
            // Display the status.
            Console.WriteLine (((HttpWebResponse)response).StatusDescription);
            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream ();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader (dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd ();
            // Display the content.
            reCaptChaResponseString = responseFromServer;
            // Clean up the streams.
            reader.Close ();
            dataStream.Close ();
            response.Close ();
			return reCaptChaResponseString;
		}
    }
}
