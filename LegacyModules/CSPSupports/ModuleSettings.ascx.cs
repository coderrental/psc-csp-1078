﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Entities.Modules;

namespace Tie.Kinetix.Modules.CSPSupports
{
	public partial class ModuleSettings : ModuleSettingsBase
	{
		public override void LoadSettings()
		{
			if (TabModuleSettings["SETTING_RECAPTCHA_PUBLICKEY"] != null)
			{
				tbxreCaptchaPubKey.Text = TabModuleSettings["SETTING_RECAPTCHA_PUBLICKEY"].ToString();
			}
			if (TabModuleSettings["SETTING_RECAPTCHA_PRIVATEKEY"] != null)
			{
				tbxreCaptchaPrivateKey.Text = TabModuleSettings["SETTING_RECAPTCHA_PRIVATEKEY"].ToString();
			}
		}

		public override void UpdateSettings()
		{
			var moduleController = new ModuleController();
			moduleController.UpdateTabModuleSetting(TabModuleId, "SETTING_RECAPTCHA_PUBLICKEY", tbxreCaptchaPubKey.Text);
			moduleController.UpdateTabModuleSetting(TabModuleId, "SETTING_RECAPTCHA_PRIVATEKEY", tbxreCaptchaPrivateKey.Text);
			ModuleController.SynchronizeModule(ModuleId);
		}
	}
}