﻿<%@ Control Language="C#" Inherits="Tie.Kinetix.Modules.CSPSupports.ModuleSettings"
    CodeFile="ModuleSettings.ascx.cs" AutoEventWireup="true" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div>
	<table>
		<tr>
			<td>reCaptcha Public Key:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxreCaptchaPubKey"></telerik:RadTextBox></td>
		</tr>
		<tr>
			<td>reCaptcha Private Key:</td>
			<td><telerik:RadTextBox runat="server" ID="tbxreCaptchaPrivateKey"></telerik:RadTextBox></td>
		</tr>
	</table>
</div>