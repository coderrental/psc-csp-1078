<%@ Control Language="C#" Inherits="Tie.Kinetix.Modules.CSPSupports.ServerMessage"
    CodeFile="ServerMessage.ascx.cs" AutoEventWireup="true" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>
<div class="csp_supports_container">
    <div  class="csp_support_wrapper">
        <%=Localization.GetString("ServerMessage", TmpResourcePath)%>
    </div>
    
    <div class="csp_support_register_wrapper">
         <p><asp:Literal ID="SupportMessage" runat="server"></asp:Literal></p>
    </div>
    <div class="clearfix">
    </div>
</div>
