<%@ Control Language="C#" Inherits="Tie.Kinetix.Modules.CSPSupports.ViewCSPSupports"
    CodeFile="ViewCSPSupports.ascx.cs" AutoEventWireup="true" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>
<div class="csp_supports_container">
    <div  class="csp_support_wrapper">
        <div class="csp_block">
            <asp:Literal ID="lbSupportMessage" runat="server"></asp:Literal>
        </div>
        
    </div>
    <div class="clearfix"></div>    
    <div  class="csp_bottom_line"></div>
    
    <div class="csp_support_register_wrapper">
        <div class="csp_support_register">	
			<div>
                    <label>
                        <asp:Literal  ID="lbUserName" runat="server" /><br />
                        <asp:TextBox ID="tbUser" runat="server" MaxLength="50" /></label>
            </div>

            <div>
                    <label>
                        <asp:Literal  ID="lbCompanyName" runat="server" /><br />
                        <asp:TextBox ID="tbCompany" runat="server" MaxLength="50" /></label>
            </div>
            <div>
                    <label>
                        <asp:Literal  ID="lbContactName" runat="server" /><br />
                        <asp:TextBox ID="tbName" runat="server"  MaxLength="50" /></label>
            </div>
            <div>
                    <label>
                        <asp:Literal  ID="lbEmail" runat="server" /><br />
                        <asp:TextBox ID="tbEmail" runat="server"  MaxLength="50" /></label>
            </div>
            <div>
                    <label>
                        <asp:Literal  ID="lbPhone" runat="server" /><br />
                        <asp:TextBox ID="tbTelephone" runat="server"  MaxLength="20" /></label>
            </div>
            <div>
                    <label>
                        <asp:Literal  ID="lbSubject" runat="server" /><br />
                        <asp:TextBox ID="tbSubject" runat="server" MaxLength="100" /></label>
            </div>
            <div>
                    <label>
                        <asp:Literal  ID="lbMessage" runat="server" /><br />
                        <asp:TextBox ID="tbMessage" runat="server"  TextMode="MultiLine" Rows="10"  MaxLength="3000" /></label>
            </div>
			<div runat="server" ID="pluginReCaptcha">
				<!-- ReCaptcha -->
				<script type="text/javascript"
				   src="http://www.google.com/recaptcha/api/challenge?k=<%= RCaptchaPublicKey %>">
				</script>
				<noscript>
				   <iframe src="http://www.google.com/recaptcha/api/noscript?k=<%= RCaptchaPublicKey %>"
					   height="300" width="500" frameborder="0"></iframe><br>
				   <textarea name="recaptcha_challenge_field" rows="3" cols="40">
				   </textarea>
				   <input type="hidden" name="recaptcha_response_field"
					   value="manual_challenge">
				</noscript>
				<input type="hidden" runat="server" ID="rcChallengeField" ClientIDMode="Static" />
				<input type="hidden" runat="server" ID="rcResponseField" ClientIDMode="Static" />
				<asp:Literal ID="lbCaptchaMsg" runat="server"></asp:Literal>
				<!-- EOF ReCaptcha -->
			</div>
            <div>
				<asp:Button runat="server" ID="bSubmit" OnClick="OnSubmitCickEvent" OnClientClick="SyncRCaptchaFields()" />
            </div>
        </div>

<div class="csp_block_right">		
		<% if (ShowDownloadInstruction && !string.IsNullOrEmpty(DownloadPath))
            { %>
        <div id="downloadManualDiv" style="margin-top:15px;">
			<a href="<%=DownloadPath %>" target="_blank" rel="nofollow"><img alt="" src="<%=DownloadImagePath %>" /></a>
        </div>
            <%}%>
			
			
	<%if (ShowFaqSection && !string.IsNullOrEmpty(faqDownloadFilePath))
                {
                    %>
	<div id="faqDiv" style="margin-top:15px;">

			<a id="faqDownload" target="_blank" href="<%=faqDownloadFilePath%>"><img id="faqDownload" src="<%=faqDownloadButtonPath%>" alt="FAQ" /></a>

	</div>
	<%}%>
	
	<%if (ShowInstructionsVideo && !string.IsNullOrEmpty(instructionsVideoPath))
                {
                    %>
	<div id="instructionsVideoDiv" style="margin-top:15px;">
			
			<a style="outline:none;" id="instructionsVideo" target="_blank" href="<%=instructionsVideoPath%>"><img id="instructionsVideoButton" src="<%=instructionsVideoButtonPath%>" alt="Instructions Video" /></a>
									 

	</div>
	<%}%>
	
	<%if (ShowBestPractises && !string.IsNullOrEmpty(bestPractisesFilePath))
                {
                    %>
	<div id="bestPractisesDiv" style="margin-top:15px;">
			
			<a id="bestPractises" target="_blank" href="<%=bestPractisesFilePath%>"><img id="bestPractisesLink" src="<%=bestPractisesButtonPath%>" alt="Best Practises" /></a>
			<br/><br/>
									 
	</div>
	<%}%>	
	
	<%if (ShowSocialManual && !string.IsNullOrEmpty(socialManualFilePath))
                {
                    %>
	<div id="socialManualDiv">
			
			<a id="socialManual" target="_blank" href="<%=socialManualFilePath%>"><img id="socialManualLink" src="<%=socialManualButtonPath%>" alt="socialManual" /></a>
			<br/><br/>
									 
	</div>
	<%}%>
		
	</div>	
        <div class="clearfix">     
            <br />   
              <asp:Label ID="lbErrorMsg" runat="server" Text=""></asp:Label>
        </div>
    </div>
    
    <script type="text/javascript">
		function SyncRCaptchaFields() {
			var challengeKey = $('#recaptcha_challenge_field').val();
			var responseKey = $('#recaptcha_response_field').val();
			$('#rcChallengeField').val(challengeKey);
			$('#rcResponseField').val(responseKey);
		}
		
        $(document).ready(function() {
			/*
			try {
				$("#dnn_dnnPRIVACY_hypPrivacy").colorbox({ width: "1100px", height: "700px", iframe: true });
				$("#dnn_dnnTERMS_hypTerms").colorbox({ width: "1100px", height: "700px", iframe: true });       		
			} catch (err) {}
			*/
            $("#<%=bSubmit.ClientID %>").click(function(ev) {
                var valid = true;
                $("input,textarea", ".csp_support_register").each(function(index, item) {
					if ($(item).attr('id') == 'rcChallengeField' || $(item).attr('id') == 'rcResponseField') return;
                    var _item = $(item);
                    if (_item.val().length < 2) {				
						if($(item).attr('id') == $("#<%=tbUser.ClientID %>").attr('id')) 
						{
						   valid=true;
						   $("#<%=tbUser.ClientID %>").removeClass("error");
						}

						else
						{ 
							if (valid == true) 
								valid = false;
							if (!_item.hasClass("error")) _item.addClass("error");
						}
                    }
					else if($(item).attr('id') == $("#<%=tbEmail.ClientID %>").attr('id')) 
						{
						   var emailpattern = /.+@.+\./;
						   if (emailpattern.test(_item.val()))
								{
								valid=true;
								if (_item.hasClass("error")) _item.removeClass("error");
								}
						   else
								{
								valid=false;
								if (!_item.hasClass("error")) _item.addClass("error");
								}
						}
                    else {
                        if (_item.hasClass("error")) _item.removeClass("error");
                    }
                });
				
				
				
                /*
                $("textarea",".csp_support_register").each(function(index,item){
                    var _item = $(item);
                    if (_item.val().length < 2) {
                        if (valid == true) valid = false;
                    }                    
                });*/
                if (valid == true) {
                }
                else {
                    $("#<%=lbErrorMsg.ClientID %>").text("<%=Localization.GetString("ValidationErrorMessage", tmpResourcePath) %>");
                    ev.preventDefault();
                    return false;
                }
            });
        });
    </script>
	
	<script type="text/javascript">
	var CheckboxIssueInterval;
	$(function () {
		var w = $("#kendoWnd").kendoWindow({ modal: true, visible: false, close: function () { $("#kendoWnd").html(""); } }).data("kendoWindow");
		$("#instructionsVideo").click(function (ev) {
			ev.preventDefault();
			var el = $(this);
			w.refresh({ url: el.attr("href") });
			w.title(el.text());
			w.wrapper.css({ width: 610, height: 415 });
			w.center();
			w.open();
			return false;
		});
	});	
	
	var RecaptchaOptions = {
		theme : 'clean'
	 };
	</script>	
    <div class="clearfix">
    </div>
</div>
