using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DotNetNuke.Entities.Users;

namespace DesktopModules.CSPModules.CspCommons
{

    /// <summary>
    /// Summary description for Globals
    /// </summary>
    public class Globals
    {
        public static string SyndicationCustomImage = "SyndicationCustomImage";
        //public static string SyndicationDefaultImage1 = "https://cc.syndicate.cnetcontent.com/global/images/microbar_88x31.jpg";
        public static string SyndicationDefaultImage1 = "SyndicationDefaultImage1";
        public static string SyndicationDefaultImage2 = "SyndicationDefaultImage2";
        public static string EntryPoint = "EntryPoint";
        public static string LaunchType = "LaunchType";
        public static string SyndicationLinkText = "SyndicationLinkText";
        public static string SyndicationImageOption = "SyndicationImageOption";
        public static string SyndicationExploreProductText = "SyndicationExploreProductText";
        public static string SyndicationExploreProductImageOption = "SyndicationExploreProductImageOption";
        public static string SyndicationExploreProductImage = "SyndicationExploreProductImage";
        public static string SyndicationLinkImage = "SyndicationLinkImage";
        public static string SyndicationExploreProductLt = "SyndicationExploreProductLt";
        public static readonly string SyndicationLanguageId = "SyndicationLanguageId";
        public static readonly string Dnn_UserName = "Dnn_UserName";
       // public static string DefaultScriptPublication = "?p96(ct13)";

		//public static string DefaultEpScriptPublication = "?p58(ct6&fMfPN~1=296342U!):c64(ct2&fgeneralSyndicationScreenTextId~1=providedBy!):c6(ct2&fcontextId~1=cpy!):c12(ct12&fcontextId~1=SeriesEP!)[st(ct12*displayOrder%20asc*)]:c41(ct2&fgeneralSyndicationScreenTextId~1=print!):c10(ct4&fimageId~1=printerIcon!):c67(ct2&fgeneralSyndicationScreenTextId~1=productDescOverviewEP!):c66(ct2&fcontextId~1=overviewEP!):c60(ct6&fMfPN~1=296342U!):c61(ct6&fMfPN~1=296342U!):c62(ct6&fMfPN~1=296342U!):c63(ct6&fMfPN~1=296342U!):c65(ct6&fMfPN~1=296342U!):c56(ct6&fMfPN~1=296342U!)";
		
        //public static string DefaultEpScriptPublication = "?p58(ct6&fproductSku~1=S5918618!):c64(ct2&fgeneralSyndicationScreenTextId~1=providedBy!):c6(ct2&fcontextId~1=cpy!):c12(ct12&fcontextId~1=SeriesEP!)[st(ct12*displayOrder%20asc*)]:c41(ct2&fgeneralSyndicationScreenTextId~1=print!):c10(ct4&fimageId~1=printerIcon!):c67(ct2&fgeneralSyndicationScreenTextId~1=productDescOverviewEP!):c66(ct2&fcontextId~1=overviewEP!):c60(ct6&fproductSku~1=S5918618!):c61(ct6&fproductSku~1=S5918618!):c62(ct6&fproductSku~1=S5918618!):c63(ct6&fproductSku~1=S5918618!):c65(ct6&fproductSku~1=S5918618!):c56(ct6&fproductSku~1=S5918618!)";
        public static string PrivacyAndTermsScript = "<script type=\"text/javascript\"> 	$(document).ready(function() { 		$(\"#dnn_dnnPRIVACY_hypPrivacy\").colorbox({ width: \"800px\", height: \"700px\", iframe: true }); 		$(\"#dnn_dnnTERMS_hypTerms\").colorbox({ width: \"1100px\", height: \"700px\", iframe: true }); 	}); </script> ";
        public static string DefaultScriptPublicationPath = "";
        public static string SyndicationExploreProductEp = "SyndicationExploreProductEp";
        public static long FileSize = 200000; //50Kbs
        public static string SyndicationIFrameTarget = "SyndicationIFrameTarget";
        public static string SMTPHost = "127.0.0.1";
        public static string SMTPAuthentication = "";
        public static string SMTPUsername = "";
        public static string SMTPPassword = "";

        public const string CompanyLogoWebKey = "CompanyLogoWebUrl";
        public const string CompanyLogoPrintKey = "CompanyLogoPrintUrl";
        public const string PublicationRuleKey = "PublicationRule";
        public const string IntegrationKey = "CspId";
        public const string DataSourceId = "DataSourceId";
        public const string ContactUsEmail = "ContactUsEmail";
		public const string SyndicationDefaultExploreProductImage = "https://cc.syndicate.cnetcontent.com/global/images/microButton_88x31.jpg";
        public const string VR_EMAIL_USER = "VR_email_user";
        public const string VR_CERT_NAME = "VR_CERT_NAME";

        public const string DataExportQuery = @"
select *
from (
select a.companies_Id as CompanyId, a.companyname as CompanyName,a.address1 as Address,a.address2 as Address2,a.city as City,a.state as State, a.zipcode as Zipcode, a.country as Country, a.website_url as Website, a.created as Created, d.base_domain as Domain, c.parametername, cast(b.value_text as nvarchar(max)) as v, cc.firstname, cc.lastname, cc.emailaddress, cc.telephone, lng.description as Language
from companies a
	left join companies_parameters b on a.companies_Id = b.companies_Id
	left join companies_parameter_types c on b.companies_parameter_types_Id = c.companies_parameter_types_Id
	left join companies_consumers d on d.companies_Id = a.companies_Id    
	left join companies_contacts cc on cc.companies_Id = a.companies_Id
    left join languages lng on d.default_language_Id = lng.languages_Id
where a.is_supplier = 0
)as data
pivot
(
	max(v)  FOR parametername in ({0})
)as pdata	
";

        public const string DataExportFilteredBySupplierId = @"select *
from (
select a.companies_Id as CompanyId, a.companyname as CompanyName,a.address1 as Address,a.address2 as Address2, a.city as City,a.state as State,a.zipcode as Zipcode, a.country as Country, a.website_url as Website, a.created as Created, d.base_domain as Domain, c.parametername, cast(b.value_text as nvarchar(max)) as v, cc.firstname, cc.lastname, cc.emailaddress, cc.telephone, lng.description as Language
from companies a
	left join companies_parameters b on a.companies_Id = b.companies_Id
	left join companies_parameter_types c on b.companies_parameter_types_Id = c.companies_parameter_types_Id
	left join companies_consumers d on d.companies_Id = a.companies_Id
	left join companies_contacts cc on cc.companies_Id = a.companies_Id
    left join languages lng on d.default_language_Id = lng.languages_Id
where a.parent_companies_Id = @supplierId and a.is_supplier = 0
)as data
pivot
(
	max(v)  FOR parametername in ({0})
)as pdata	";

        public const string GenerateReportQuery = @"
declare @counter int = 0, @dateCol varchar(200), @m int, @y int, @campaign nvarchar(200), @banner nvarchar(200), @totalMonths int = 0
if OBJECT_ID('tempdb..#temp') is not null drop table #temp
create table #temp (
	[Company Id] int not null,	
	[Registration Date] datetime null,
	Brand nvarchar(150) not null,
	Campaign nvarchar(150) not null,
	Banner nvarchar(150) not null,
	[First Impression] datetime null,
	[Last Impression] datetime null,
	[Total Days] int,
	[Total Impressions] int,
	[Total Clicks] int
)

insert into #temp ([Company Id], [Registration Date], brand, campaign, banner, [First Impression], [Last Impression], [Total Days], [Total Impressions], [Total Clicks])
select a.Id, b.created, pc. companyname, a.CampaignName, a.ImageName, min(c.StartDate), MAX(c.EndDate), 0, 0, 0
from TIE_Monthly_Totals a
	join companies b on a.Id = b.companies_Id
	join companies pc on pc.companies_Id = b.parent_companies_Id
	left join TIE_Key_Dates c on c.Id=a.Id and c.CampaignName=a.CampaignName and c.ImageName=a.ImageName
where a.Id = @cid
group by a.Id, b.created, pc. companyname, a.CampaignName, a.ImageName
order by a.Id, a.CampaignName, a.ImageName

select @counter =0, @totalMonths = DATEDIFF(m, MIN(a.StartDate), GETDATE()) from TIE_Key_Dates a
while @counter <= @totalMonths
begin
	select @m = MONTH(DATEADD(MM,@counter*-1,CURRENT_TIMESTAMP)), @y = YEAR(DATEADD(MM,@counter*-1,CURRENT_TIMESTAMP))	
	--select @dateCol = right('0' + cast(@m as varchar(4)), 2)+'-'+cast(@y as varchar(4))
	select @dateCol = DATENAME(month, cast (cast(@y as varchar(4)) + '-' + cast(@m as varchar(4)) + '-01' as datetime)) + ' ' + cast(@y as varchar(4))
	print 'month: '+cast(@m as varchar(2)) + ', year: '+cast(@y as varchar(4)) + ', col: ' + @dateCol
	
	execute('alter table #temp add ['+@dateCol+' Impressions] [int] alter table #temp add ['+@dateCol+' Clicks] [int]')	
	
	execute(N'
		update #temp set ['+@dateCol+' Impressions] = a.Showed, ['+@dateCol+' Clicks] = a.Clicked, [Total Impressions] = [Total Impressions] + a.Showed, 
			[Total Clicks] = [Total Clicks] + a.Clicked , [Total Days] = tkd.Days
		from TIE_Monthly_Totals a			
			join TIE_Key_Dates tkd on tkd.Id = a.Id
				and tkd.CampaignName = a.CampaignName --collate Latin1_General_CI_AS
				and tkd.ImageName = a.ImageName --collate Latin1_General_CI_AS				
			join #temp b on b.[Company Id] = a.Id 
				and b.campaign = a.CampaignName collate Latin1_General_CI_AS
				and b.banner = a.ImageName collate Latin1_General_CI_AS			
		where YEAR(a.ReportDate) = '+@y+' and MONTH(a.ReportDate) = '+@m)
	
	exec('update #temp set ['+@dateCol+' Impressions] = 0 where ['+@dateCol+' Impressions] is null')
	exec('update #temp set ['+@dateCol+' Clicks] = 0 where ['+@dateCol+' Clicks] is null')	
	
	select @counter = @counter + 1
end

select * from #temp
if OBJECT_ID('tempdb..#temp') is not null drop table #temp
";

        public const string GetReportDataQuery = @"
/*
declare @month as varchar(max)
select @month = COALESCE(@month + ',', '') + '['+ cast(a.ReportDate as varchar(12))+ ']'
from TIE_Monthly_Totals a
where a.Id=@cid
group by a.ReportDate
if @month is not null
execute(N'

select * from 
(

	select a.CampaignName, a.ReportDate,SUM(a.Showed) as total
	from TIE_Monthly_Totals a
	where a.Id='+@cid+'
	group by a.CampaignName, a.ReportDate

) as p1

pivot (
	max(total) for ReportDate in ('+@month+')
) as p2
')*/
declare @month as varchar(max), @totalMonths int = 0, @counter int = 0
declare @m int, @y int

select @totalMonths = DATEDIFF(m, MIN(a.StartDate), GETDATE()) from TIE_Key_Dates a
select @counter = @totalMonths

while @counter >= 0
begin
	select @m = MONTH(DATEADD(MM,@counter*-1,CURRENT_TIMESTAMP)), @y = YEAR(DATEADD(MM,@counter*-1,CURRENT_TIMESTAMP))	
	select @month = COALESCE(@month + ',', '') + '['+ cast(@m as varchar(2))+ '-01-' + CAST(@y as varchar(4)) + ']'
	select @counter = @counter - 1
end

if @month is not null
execute(N'
select * from 
(

	select a.CampaignName, a.ReportDate,SUM(a.Showed) as total
	from TIE_Monthly_Totals a
	where a.Id='+@cid+'
	group by a.CampaignName, a.ReportDate

) as p1

pivot (
	max(total) for ReportDate in ('+@month+')
) as p2
')

";


        public static string DeclareCspJsObjectsScript(Uri url)
        {
          
            StringBuilder script = new StringBuilder();
            script.AppendLine("<script type='text/javascript'>");
            script.AppendFormat("var path = \"{0}://{1}/{2}\"; ", url.Scheme, url.Host, "middlelayer/CSPService.svc/json/");
            script.AppendLine();
            script.AppendLine("var proxy = new ServiceProxy(path);");
            script.AppendLine("var cspService = new CSPService(proxy);");
            script.AppendLine("var consumer = null;");
            script.AppendLine("var profile = null;");
            script.AppendLine("</script>");
            return script.ToString();
        }

        public static string CspIndicatorRecordUrl(int portalId)
        {
                int tabId = DotNetNuke.Entities.Tabs.TabController.GetTabByTabPath(portalId, "//CompanyInformation");
                if (tabId != -1)
                    return DotNetNuke.Common.Globals.NavigateURL(tabId);
                else
                    return string.Empty;
        }

        public static string CspIndicatorSetupUrl(int portalId)
        {
            int tabId = DotNetNuke.Entities.Tabs.TabController.GetTabByTabPath(portalId, "//ContentSetup");
            if (tabId != -1)
                return DotNetNuke.Common.Globals.NavigateURL(tabId);
            else
                return string.Empty;
        }

        public static string CspIndicatorDocUrl(int portalId)
        {
            int tabId = DotNetNuke.Entities.Tabs.TabController.GetTabByTabPath(portalId, "//InstallInstructions");
                if (tabId != -1)
                    return DotNetNuke.Common.Globals.NavigateURL(tabId);
                else
                    return string.Empty;
        }
		
        public static string CspIndicatorSupportUrl(int portalId)
        {
            int tabId = DotNetNuke.Entities.Tabs.TabController.GetTabByTabPath(portalId, "//ContactSupport");
            if (tabId != -1)
                return DotNetNuke.Common.Globals.NavigateURL(tabId);
            else
                return string.Empty;
        }

        public static void AddCssReference(UserControl control, string name)
        {
            HtmlLink link = new HtmlLink();
            link.Href = control.ResolveUrl(name);
            link.Attributes.Add("type", "text/css");
            link.Attributes.Add("rel", "stylesheet");
            control.Page.Header.Controls.Add(link);
        }
        public static void ShowErrorMessage(Panel container, string html)
        {

            Label lbl = new Label();
            lbl.Style.Add("padding", "10px");
            lbl.Style.Add("margin", "10px");
            lbl.Style.Add("display", "block");
            lbl.Style.Add("background", "#A65353");
            lbl.Text = html;
            container.Controls.Clear();
            container.Controls.Add(lbl);
            if (!container.Visible) container.Visible = true;
        }


    }


}
