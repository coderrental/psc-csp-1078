﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Text.RegularExpressions;
using DesktopModules.CSPModules.CspCommons;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Profile;
using DotNetNuke.Entities.Users;
using DotNetNuke.Security.Membership;
using DotNetNuke.Security.Roles;
using DotNetNuke.Services.Log.EventLog;

namespace CspCommons
{
    public class CspSynchronizationEngine
    {
        private UserInfo _userInfo;
        private readonly int _currentPortalId;
        private readonly string _requestedHost;
        protected ArrayList Portals { get; set; }
        protected List<UserInfo> Users { get; set; }

        /// <summary>
        /// This dictionary caches the delegates for each 'to-clone' type.
        /// </summary>
        static Dictionary<Type, Delegate> _cachedIL = new Dictionary<Type, Delegate>();

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public CspSynchronizationEngine(UserInfo userInfo, int portalId, string requestedHost)
        {
            _userInfo = userInfo;
            _currentPortalId = portalId;
            _requestedHost = requestedHost;

            RefreshPortalAndUserInfos();
        }
        public void Synchronize()
        {
            AddUserToPortalWhereUserDoesntExist();
            RefreshPortalAndUserInfos();
            SyncCspCompany();
        }

        /// <summary>
        /// update all portals because general info such name name and addresses are changed, we want to sync those too.
        /// </summary>
        /// <param name="company"></param>
        public void UpdateCspCompanyInAllPortals(Company company)
        {
            foreach (UserInfo user in Users)
            {
                if (user != null && !user.IsDeleted)
                    UpdateCspCompanyInPortal(company, user);
            }
        }

        private void UpdateCspCompanyInPortal(Company company, UserInfo user)
        {
            using (CspDataLayerDataContext context = new CspDataLayerDataContext(Config.GetConnectionString("CspPortal" + user.PortalID)))
            {
                int integrationKey = -1;
                if (!int.TryParse(user.Profile.GetPropertyValue(Globals.IntegrationKey), out integrationKey))
                    integrationKey = -1;

                if (integrationKey == -1)
                    return;

                if (Config.GetConnectionString("CspPortal" + user.PortalID) == Config.GetConnectionString("CspPortal" + _currentPortalId) 
                        && company.companies_Id != integrationKey)
                {
                    WriteToEventLog("SynchronizationEngine Conflict: Unable to synch 2 different companies on the same database. Portal: {0}, Users: {1}, {2}", 
                        _currentPortalId, company.companies_Id, integrationKey);
                    return;
                }

                Company c = context.Companies.FirstOrDefault(a => a.companies_Id == integrationKey);
                if (c == null)
                    return;

                c.address1 = company.address1;
                c.address2 = company.address2;
                c.city = company.city;
                c.companyname = company.companyname;
                c.consumer_bin_base_url = company.consumer_bin_base_url;
                c.consumer_bin_local_path = company.consumer_bin_local_path;
                c.content_staging_scheme = company.content_staging_scheme;
                c.country = company.country;
                c.county = company.county;
                c.date_changed = DateTime.Now;
                c.displayname = company.displayname;
                c.global_bin_base_url = company.global_bin_base_url;
                c.global_bin_local_path = company.global_bin_local_path;
                c.is_consumer = company.is_consumer;
                c.is_supplier = company.is_supplier;
                c.state = company.state;
                c.website_url = company.website_url;
                c.zipcode = company.zipcode;
                //c.parent_companies_Id = company.parent_companies_Id;

                foreach (CompaniesParameter p in company.CompaniesParameters)
                {
                    CompaniesParameterType cpt = context.CompaniesParameterTypes.FirstOrDefault(a => a.parametername == p.CompaniesParameterType.parametername);
                    if (cpt != null)
                    {
                        CompaniesParameter cp = c.CompaniesParameters.FirstOrDefault(a => a.companies_parameter_types_Id == cpt.companies_parameter_types_Id);
                        if (cp == null)
                        {
                            context.CompaniesParameters.InsertOnSubmit(new CompaniesParameter
                                                                           {
                                                                               companies_Id = c.companies_Id,
                                                                               companies_parameter_types_Id = cpt.companies_parameter_types_Id,
                                                                               value_text = p.value_text
                                                                           });
                        }
                        else
                            cp.value_text = p.value_text;
                    }
                }

                context.SubmitChanges(ConflictMode.ContinueOnConflict);
            }
        }


        private static void WriteToEventLog(string message, params object[] parameters)
        {
            var controller = new EventLogController();
            controller.AddLog("WARN", string.Format(message, parameters), PortalSettings.Current, -1, EventLogController.EventLogType.ADMIN_ALERT);
        }

        /// <summary>
        /// Synchronize the company information from a source database (using _userInfo.PortalId) to a targetted database (using _currentPortalId)
        /// Find the integration key from source
        ///     if integration key is not found, then do nothing
        ///     if integration key is found
        ///         get the company info from source
        ///         check the company info in target using integration key and company name
        ///             if they are the same: apply themes, create a new base domain if base domains are different
        ///             if they arent the same: create a new company record
        /// </summary>
        public void SyncCspCompany()
        {
            UserInfo user = UserController.GetUserById(_currentPortalId, _userInfo.UserID);
            try
            {
                if (user == null)
                    throw new Exception("User shouldn't be null. ");
                int integrationKey = -1;
                if (!int.TryParse(user.Profile.GetPropertyValue(Globals.IntegrationKey), out integrationKey))
                    integrationKey = -1;

                if (integrationKey == -1)
                {
                    UserInfo userWithIntegrationKey = null;
                    // find a user where the integration key exists
                    foreach (UserInfo userInfo in Users)
                    {
                        if (userInfo != null && !userInfo.IsDeleted && int.TryParse(userInfo.Profile.GetPropertyValue(Globals.IntegrationKey), out integrationKey) && integrationKey >= 0)
                        {
                            userWithIntegrationKey = userInfo;
                            break;
                        }
                    }
                    // sync company
                    if(userWithIntegrationKey != null)
                        CreateCspCompany(userWithIntegrationKey.PortalID, integrationKey);
                }
                else
                {
                    // it is possible that db restore creates a case where integration key exists but the company hasn't been created
                    throw new Exception("Integration Key is not -1.");
                }
            }
            catch (Exception ex)
            {
                CreateLog(user, ex.Message, ex.StackTrace);
            }
        }

        /// <summary>
        ///get the company info from source
        ///check the company info in target using integration key and company name
        ///     if they are the same: apply themes, create a new base domain if base domains are different
        ///     if they arent the same: create a new company record
        /// </summary>
        /// <param name="sourcePortalId"></param>
        /// <param name="integrationKey"></param>
        private void CreateCspCompany(int sourcePortalId, int integrationKey)
        {
            try
            {
                UserInfo user = UserController.GetUserByName(_currentPortalId, _userInfo.Username);
                if (user == null)
                    throw new Exception("User shouldn't be null");

                using (var sourceContext = new CspDataLayerDataContext(Config.GetConnectionString("CspPortal" + sourcePortalId)))
                {
                    Company sc = sourceContext.Companies.Single(a => a.companies_Id == integrationKey);
                    using (var dc = new CspDataLayerDataContext(Config.GetConnectionString("CspPortal"+_currentPortalId)))
                    {

                        bool isAdd = false;
                        string externalIdentifier = Config.GetSetting("Portal" + _currentPortalId + "ExternalIdentifier");
                        if (string.IsNullOrEmpty(externalIdentifier))
                            externalIdentifier = _requestedHost;
                        // get channel
                        Channel channel = dc.Channels.FirstOrDefault(a => a.ExternalIdentifier == externalIdentifier);
                        if (channel == null)
                            return;

                        // check if this company exists using company name and integration key id
                        //Company c = dc.Companies.FirstOrDefault(a => a.companies_Id == sc.companies_Id);
                        Company c = dc.Companies.FirstOrDefault(a => a.companyname == sc.companyname && a.address1 == sc.address1 
                            && a.city == sc.city && a.zipcode == sc.zipcode && a.state == sc.state && a.country == sc.country 
                            && a.is_consumer == sc.is_consumer && a.is_supplier == sc.is_supplier);
                        // check if it is the same company
                        if (c != null && c.companyname == sc.companyname) 
                        {
                        }
                        else
                        {
                            // different company
                            c = new Company
                                    {
                                        address1 = sc.address1,
                                        address2 = sc.address2,
                                        city = sc.city,
                                        companyname = sc.companyname,
                                        consumer_bin_base_url = sc.consumer_bin_base_url,
                                        consumer_bin_local_path = sc.consumer_bin_local_path,
                                        content_staging_scheme = sc.content_staging_scheme,
                                        country = sc.country,
                                        county = sc.county,
                                        displayname = sc.companyname,
                                        externalCode = sc.externalCode,
                                        global_bin_base_url = sc.global_bin_base_url,
                                        global_bin_local_path = sc.global_bin_local_path,
                                        is_consumer = sc.is_consumer,
                                        is_supplier = sc.is_supplier,
                                        created = DateTime.Now,
                                        date_changed = DateTime.Now,
                                        state = sc.state,
                                        zipcode = sc.zipcode,
                                        website_url = sc.website_url,
                                        type = sc.type,
                                        parent_companies_Id = channel.CompanyId
                                    };
                            foreach (Contact contact in sc.Contacts)
                            {
                                c.Contacts.Add(new Contact
                                                   {
                                                       Company = c,
                                                       contact_type = contact.contact_type,
                                                       department = contact.department,
                                                       emailaddress = contact.emailaddress,
                                                       firstname = contact.firstname,
                                                       middlename = contact.middlename,
                                                       lastname = contact.lastname,
                                                       mobile = contact.mobile,
                                                       notes = contact.notes,
                                                       telephone = contact.telephone,
                                                       user_Id = contact.user_Id
                                                   });
                            }
                            foreach (CompaniesParameter parameter in sc.CompaniesParameters)
                            {
                                CompaniesParameterType cpt = dc.CompaniesParameterTypes.FirstOrDefault(a => a.parametername == parameter.CompaniesParameterType.parametername);
                                if (cpt != null)
                                {
                                    c.CompaniesParameters.Add(new CompaniesParameter
                                                                   {
                                                                       Company = c,
                                                                       CompaniesParameterType = cpt,
                                                                       value_text = parameter.value_text
                                                                   });
                                }                                
                            }
                            isAdd = true;
                        }

                        dc.Companies.InsertOnSubmit(c);
                        dc.SubmitChanges();

                        // find base domain
                        CompaniesConsumer cc = null;
                        foreach (CompaniesConsumer companiesConsumer in sc.CompaniesConsumers)
                        {
                            if (Regex.IsMatch(companiesConsumer.base_domain, "\\d{16}")) 
                            {
                                cc = companiesConsumer;
                                break;
                            }
                        }
                        if (cc == null && sc.CompaniesConsumers.Count > 0)
                            cc = sc.CompaniesConsumers.First();

                        //is base domain the same?
                        string baseDomain = "";
                        if (cc != null)
                        {
                            int i = cc.base_domain.IndexOf(".");
                            baseDomain = cc.base_domain.Substring(0, i) + "." + channel.BaseDomain;
                        }
                        //create a new base domain
                        if (cc!=null && dc.CompaniesConsumers.Count(a=>a.base_domain == baseDomain) == 0)
                        {
                            //c.CompaniesConsumers.Add(new CompaniesConsumer
                            //                             {
                            //                                 base_domain = baseDomain,
                            //                                 active = true,
                            //                                 base_publication_parameters = channel.DefaultParameters,
                            //                                 base_publication_Id = channel.BasePublicationId,
                            //                                 Company = c,
                            //                                 default_language_Id = cc.default_language_Id,
                            //                                 fallback_language_Id = cc.fallback_language_Id
                            //                             });

                            // if it is the same db, and the domain is the same, we dont create
                            // if it is diff database and the domain is the same

                            dc.CompaniesConsumers.InsertOnSubmit(new CompaniesConsumer
                                                                     {
                                                                         base_domain = baseDomain,
                                                                         active = true,
                                                                         base_publication_parameters = channel.DefaultParameters,
                                                                         base_publication_Id = channel.BasePublicationId,
                                                                         Company = c,
                                                                         default_language_Id = cc.default_language_Id,
                                                                         fallback_language_Id = cc.fallback_language_Id
                                                                     });
                            dc.SubmitChanges();
                        }

                        //assign themes
                        foreach (ChannelThemeAssocation assocation in dc.ChannelThemeAssocations.Where(a=>a.ChannelId == channel.Id))
                        {
                            if (c.CompaniesThemes.Count(a => a.themes_Id == assocation.ThemeId) == 0)
                                c.CompaniesThemes.Add(new CompaniesTheme
                                                          {
                                                              Company = c,
                                                              themes_Id = assocation.ThemeId
                                                          });
                        }

                        /*
                        if (isAdd)
                            dc.Companies.InsertOnSubmit(c);
                        */
                        dc.SubmitChanges();

                        //update integration key
                        user.Profile.SetProfileProperty(Globals.IntegrationKey, c.companies_Id.ToString());
                        UserController.UpdateUser(_currentPortalId, user);
                    }
                }
            }
            catch(Exception ex)
            {
                CreateLog(null, ex.Message, ex.StackTrace);
            }
        }

        /// <summary>
        /// Create Error Log
        /// </summary>
        /// <param name="user"></param>
        /// <param name="error"></param>
        /// <param name="extra"></param>
        private void CreateLog(UserInfo user, string error, string extra)
        {
            using (DnnDataLayerDataContext ct = new DnnDataLayerDataContext(Config.GetConnectionString()))
            {
                ct.CspErrorLogs.InsertOnSubmit(new CspErrorLog
                                                   {
                                                       userId = user != null ? user.UserID : -1,
                                                       date = DateTime.Now,
                                                       error = error,
                                                       extra = extra
                                                   });
                ct.SubmitChanges(ConflictMode.ContinueOnConflict);
            }
        }

        /// <summary>
        /// refresh all available portals and users
        /// </summary>
        private void RefreshPortalAndUserInfos()
        {
            PortalController portalController = new PortalController();
            Portals = portalController.GetPortals();
            Users = new List<UserInfo>();
            foreach (PortalInfo portal in Portals)
            {
                Users.Add(UserController.GetUserById(portal.PortalID, _userInfo.UserID));
            }
        }


        /// <summary>
        /// create user in all portals where this user doesn't exist
        /// </summary>
        public bool AddUserToPortalWhereUserDoesntExist()
        {
            UserInfo user = UserController.GetUserByName(_currentPortalId, _userInfo.Username);
            if (user == null) // redudant check, if the code reaches here, user shouldn't exist
            {
                user = new UserInfo
                           {
                               PortalID = _currentPortalId,
                               FirstName = _userInfo.FirstName,
                               LastName = _userInfo.LastName,
                               DisplayName = _userInfo.DisplayName,
                               Email = _userInfo.Email,
                               Username = _userInfo.Username,
                               UserID = _userInfo.UserID
                           };
                user.Profile.PreferredLocale = _userInfo.Profile.PreferredLocale;
                user.Profile.TimeZone = _userInfo.Profile.TimeZone;
                user.Profile.Telephone = _userInfo.Profile.Telephone;
                user.Membership.Email = _userInfo.Membership.Email;
                user.Membership.Username = _userInfo.Username;
                user.Membership.Approved = _userInfo.Membership.Approved;
                user.Membership.Password = UserController.GetPassword(ref _userInfo, "");
                
                if (UserCreateStatus.Success == UserController.CreateUser(ref user))
                {
                    RoleController roleController = new RoleController();
                    ArrayList roles = roleController.GetRoles();
                    List<RoleInfo> roleInfos = new List<RoleInfo>();

                    //get roles from source portal
                    foreach (RoleInfo role in roles)
                    {
                        if (role.PortalID == _userInfo.PortalID)
                        {
                            if (_userInfo.IsInRole(role.RoleName))
                            {
                                roleInfos.Add(role);
                            }
                        }
                    }

                    //update roles for user in current portal
                    foreach (RoleInfo role in roleInfos)
                    {
                        RoleInfo roleInfo = roleController.GetRoleByName(_currentPortalId, role.RoleName);
                        if (roleInfo != null) // this role is available in this instance of the portal
                        {
                            roleController.AddUserRole(_currentPortalId, _userInfo.UserID, roleInfo.RoleID, Null.NullDate);
                        }
                    }
                    foreach (ProfilePropertyDefinition property in _userInfo.Profile.ProfileProperties)
                    {
                        if (property.PropertyName == Globals.IntegrationKey)
                            user.Profile.SetProfileProperty(property.PropertyName, "-1");
                        else
                            user.Profile.SetProfileProperty(property.PropertyName, property.PropertyValue);
                    }

                    UserController.UpdateUser(_currentPortalId, user);
                }
                else
                {
                    CreateLog(null, "Error trying to auto add user to other portals user: " + _userInfo.UserID +
                                    " Initating Portal ID: " + _userInfo.PortalID
                                    + " Tried to create user inside portal id: " + user.PortalID, "");
                    return false;
                }
            } // if user is null
            return true;
        }

        /// <summary>
        /// Generic cloning method that clones an object using IL.
        /// Only the first call of a certain type will hold back performance.
        /// After the first call, the compiled IL is executed.
        /// </summary>
        /// <typeparam name="T">Type of object to clone</typeparam>
        /// <param name="myObject">Object to clone</param>
        /// <returns>Cloned object</returns>
        private static T CloneObjectWithIL<T>(T myObject)
        {
            Delegate myExec = null;

            if (!_cachedIL.TryGetValue(typeof(T), out myExec))
            {
                // Create ILGenerator
                DynamicMethod dymMethod = new DynamicMethod("DoClone", typeof(T), new Type[] { typeof(T) }, true);
                ConstructorInfo cInfo = myObject.GetType().GetConstructor(new Type[] { });

                ILGenerator generator = dymMethod.GetILGenerator();

                LocalBuilder lbf = generator.DeclareLocal(typeof(T));
                //lbf.SetLocalSymInfo("_temp");

                generator.Emit(OpCodes.Newobj, cInfo);
                generator.Emit(OpCodes.Stloc_0);
                foreach (FieldInfo field in myObject.GetType().GetFields(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic))
                {
                    // Load the new object on the eval stack... (currently 1 item on eval stack)
                    generator.Emit(OpCodes.Ldloc_0);
                    // Load initial object (parameter)          (currently 2 items on eval stack)
                    generator.Emit(OpCodes.Ldarg_0);
                    // Replace value by field value             (still currently 2 items on eval stack)
                    generator.Emit(OpCodes.Ldfld, field);
                    // Store the value of the top on the eval stack into the object underneath that value on the value stack.
                    //  (0 items on eval stack)
                    generator.Emit(OpCodes.Stfld, field);
                }

                // Load new constructed obj on eval stack -> 1 item on stack
                generator.Emit(OpCodes.Ldloc_0);
                // Return constructed object.   --> 0 items on stack
                generator.Emit(OpCodes.Ret);

                myExec = dymMethod.CreateDelegate(typeof(Func<T, T>));
                _cachedIL.Add(typeof(T), myExec);
            }
            return ((Func<T, T>)myExec)(myObject);
        }
    }
}
