using System.Collections.Generic;

namespace DesktopModules.CSPModules.CspCommons
{
    public class CategoryParameters
    {
        private Dictionary<string, string> _dict;
        private int _count;

        public CategoryParameters(string argument)
        {
            string[] pairs = argument.Split('|');
            _dict = new Dictionary<string, string>();
            
            foreach (string pair in pairs)
            {
                string[] values = pair.Split(',');
                if (values.Length == 2)
                {
                    _dict.Add(values[0].Replace("[", ""), values[1].Replace("]", ""));
                    _count++;
                }
                else
                {
                    _count = 0;
                    break;
                }
            }
        }

        public Dictionary<string, string> Pairs
        {
            get { return _dict; }
            set { _dict = value; }
        }

        public int Count
        {
            get {
                return _count;
            }
            set {
                _count = value;
            }
        }
    }
}