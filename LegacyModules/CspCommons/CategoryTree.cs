﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace DesktopModules.CSPModules.CspCommons
{
    /// <summary>
    /// Summary description for CategoryTree
    /// </summary>
    public class CategoryTree : UserControl
    {
        private IList<CatItem> _categories;
        private IQueryable<Category> _orgCategories;
        
        public CategoryTree(IQueryable<Category> categories)
        {
            //
            // TODO: Add constructor logic here
            //
            _categories = Normalize(categories.OrderByDescending(c=>c.depth));
            _orgCategories = categories;
        }

        

        private IList<CatItem> Normalize(IQueryable<Category> categories)
        {
            IList<CatItem> tmp = new List<CatItem>();
            foreach(Category cat in categories)
            {
                    var items = from c in tmp where c.Cat.parentId == cat.categoryId select c;
                    if (items.Count() > 0)
                    {
                        CatItem p = new CatItem(cat, new List<CatItem>(), null);
                        foreach(CatItem item in items)
                        {
                            p.Children.Add(item);
                            item.Parent = p;
                            item.Delete = true;
                        }
                        tmp.Add(p);
                    }
                    else
                    {
                        tmp.Add(new CatItem(cat, new List<CatItem>(), null));
                    }
            }
            int count = 0;

            while (count < tmp.Count())
            {
                if (tmp[count].Delete)
                     tmp.RemoveAt(count);
                else
                {
                    count++;
                }
            }
            return tmp;
        }

        #region Overrides of UserControl
        protected void Page_Load(object sender, EventArgs e)
        {
            /*
            string html = "";
            PanelHtml p = new PanelHtml("", "");
            p.Html += GenerateHtml(_categories, "parent", 0);
            Controls.Add(p);
             * */
            Panel p = new Panel();
            p.Controls.Add(GenerateControls(_categories));
            Controls.Add(p);
        }

        private Control GenerateControls(IList<CatItem> categories)
        {
            Panel control = new Panel();
            foreach (CatItem item in categories)
            {
                Panel p = new Panel();
                CheckBox cb = new CheckBox();
                Label lb = new Label();
                //cb.Checked = from s in item.Cat.SubscriptionProfiles where s.
                
                cb.ID = item.Cat.categoryId.ToString();
                lb.Text = item.Cat.categoryText;
                p.Style.Add("padding",string.Format("0 0 0 {0}px", item.Cat.depth*15));
                p.Controls.Add(cb);
                p.Controls.Add(lb);
                if (item.Children.Count() > 0) p.Controls.Add(GenerateControls(item.Children));
                control.Controls.Add(p);
            }
            return control;
            
        }


        private string GenerateHtml(IList<CatItem> categories, string css, int level)
        {
            string s = string.Empty;
            foreach (CatItem item in categories)
            {
                s += string.Format("<div class='{1}' alt='{2}' style='padding:0 0 0 {3}px' ><input type='checkbox' id='{4}' checked='{5}' /><span>{0}</span>", item.Cat.categoryText, css, level, item.Cat.depth * 15, item.Cat.categoryId, item.Cat.SubscriptionProfiles.Count);
                s += GenerateHtml(item.Children, "child", ++level);
                s += "</div>";
            }
            return s;
        }

        protected void Page_Init(object sender, EventArgs e)
        {
        }

        #endregion
    }
    
    public class PanelHtml : WebControl
    {
        private string _html;
        private string _cssClass;

        public PanelHtml(string html, string cssClass)
        {
            _html = html;
            _cssClass = cssClass;
        }

        public string Html
        {
            get { return _html; }
            set { _html = value; }
        }

        public string CssClass1
        {
            get { return _cssClass; }
            set { _cssClass = value; }
        }

        #region Overrides of WebControl

        protected override void Render(HtmlTextWriter writer)
        {
            if (!string.IsNullOrEmpty(_cssClass))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, _cssClass);
            }

            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.WriteLine(_html);
            writer.RenderEndTag();//div
        }

        #endregion
    }

    public class CatItem
    {
        private Category _cat;
        private IList<CatItem> _children;
        private CatItem _parent;
        private bool _delete;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public CatItem(Category cat, IList<CatItem> children, CatItem parent)
        {
            _cat = cat;
            _children = children;
            _parent = parent;
            _delete = false;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public CatItem()
        {
            _parent = null;
        }

        public Category Cat
        {
            get { return _cat; }
            set { _cat = value; }
        }

        public bool Delete
        {
            get { return _delete; }
            set { _delete = value; }
        }

        public IList<CatItem> Children
        {
            get { return _children; }
            set { _children = value; }
        }

        public CatItem Parent
        {
            get { return _parent; }
            set { _parent = value; }
        }
    }

    public class  CategoryTreeItem : WebControl
    {
        private readonly CatItem _node;
        private string _cssClass;

        public CategoryTreeItem(CatItem node, string cssClass)
        {
            _node = node;
            _cssClass = cssClass;
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (!string.IsNullOrEmpty(_cssClass))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, _cssClass);
            }
            writer.RenderBeginTag(HtmlTextWriterTag.Li);
            writer.AddAttribute(HtmlTextWriterAttribute.Href, "javascript:void(0);");
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write(string.Format("{0} ({1})",_node.Cat.categoryText, _node.Children.Count));
            writer.RenderEndTag(); // a
            writer.RenderEndTag(); // li
        }
    }

    public class CategoryTreeControl : CompositeDataBoundControl
    {
        private string _activeCssClass;

        public string ActiveItemCssClass
        {
            get { return _activeCssClass; }
            set { _activeCssClass = value; }
        }

        private string _itemCssClass;

        public string ItemCssClass
        {
            get { return _itemCssClass; }
            set { _itemCssClass = value; }
        }


        protected override int CreateChildControls(IEnumerable dataSource, bool dataBinding)
        {
            int count = 0;
            if (dataBinding) {
                foreach (CatItem category in dataSource)
                {
                    count++;
                    CategoryTreeItem current = new CategoryTreeItem(category, "");
                    Controls.Add(current);
                    count += AddChildNodes(category, current);
                }
            }

            return count;            
        }

        private int AddChildNodes(CatItem category, CategoryTreeItem current)
        {
            int count = 0;

            foreach (CatItem childCategory in category.Children)
            {
                count++;
                CategoryTreeItem cur = new CategoryTreeItem(childCategory, "");
                current.Controls.Add(cur);
                count += AddChildNodes(childCategory, cur);
            }
            return count;
        }


        public override void RenderBeginTag(System.Web.UI.HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID);
            writer.RenderBeginTag(HtmlTextWriterTag.Ul);
        }

        public override void RenderEndTag(System.Web.UI.HtmlTextWriter writer)
        {
            writer.RenderEndTag();
        }
    }
   

}



