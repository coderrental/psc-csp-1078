namespace DesktopModules.CSPModules.CspCommons
{
	partial class CspDataLayerDataContext
	{
		partial void OnCreated()
		{
			//Put your desired timeout here.
			this.CommandTimeout = 3600;

			//If you do not want to hard code it, then take it 
			//from Application Settings / AppSettings
			//this.CommandTimeout = Settings.Default.CommandTimeout;
		}
	}
}
