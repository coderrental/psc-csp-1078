using System.Collections.Generic;
using DesktopModules.CSPModules.CspCommons;

namespace DesktopModules.CSPModules.CSPReports
{
    public class ReportDataRow
    {
        private IList<ReportDataRow> _children;
        private WtDataRow _row;
        private string _name;
        private Dictionary<string, string> _measures;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public ReportDataRow(IList<ReportDataRow> children, WtDataRow row, string name, Dictionary<string, string> measures)
        {
            _children = children;
            _row = row;
            _name = name;
            _measures = measures;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public ReportDataRow()
        {
            _measures = new Dictionary<string, string>();
            _name = string.Empty;
            _children = new List<ReportDataRow>();
            _row = new WtDataRow();
        }

        public IList<ReportDataRow> Children
        {
            get { return _children; }
            set { _children = value; }
        }

        public WtDataRow Row
        {
            get { return _row; }
            set { _row = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public Dictionary<string, string> Measures
        {
            get { return _measures; }
            set { _measures = value; }
        }
    }
}