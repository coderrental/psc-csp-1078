using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DesktopModules.CSPModules.CspCommons;

namespace DesktopModules.CSPModules.CSPReports
{
    public class ResellerOverviewReport : Report
    {
        public ResellerOverviewReport(string result)
            : base(result, "DimensionalReport")
        {
        }

        public override void BuildReport(IQueryable<CspCommons.Company> objects)
        {
            
            foreach (CspCommons.Company company in objects)
            {
                ReportDataRow row = new ReportDataRow();
                row.Name = company.companyname;
                row.Row = FindWtDataRow(company.companies_Id.ToString());
                if (row.Row != null)
                {
                    foreach (WtMeasure measure in row.Row.Measures)
                    {
                        row.Measures.Add(measure.Name, measure.Value);
                    }
                }
                else
                {
                    row.Measures.Add("Hits","0");
                    row.Measures.Add("Visits", "0");
                }
                // other known values
                row.Measures.Add("Address",Clean(company.address1));
                row.Measures.Add("City", Clean(company.city));
                row.Measures.Add("State", Clean(company.state));
                row.Measures.Add("Country", Clean(company.country));
                row.Measures.Add("Website", Clean(company.website_url));

                Rows.Add(row);
            }
        }




        public override string CreateJavascript(string tagId)
        {
            StringBuilder sb = new StringBuilder();
            //sb.AppendLine("<script type='javascript/text'>");
            sb.AppendLine("google.load('visualization', '1', { 'packages': ['table'] });");
            sb.AppendLine("google.setOnLoadCallback(data);");
            sb.AppendLine("function data() {");
            sb.AppendLine("var data = new google.visualization.DataTable();");
            sb.AppendLine("data.addColumn('string', 'Consumer Name');");
            sb.AppendLine("data.addColumn('string', 'Address');");
            sb.AppendLine("data.addColumn('string', 'City');");
            sb.AppendLine("data.addColumn('string', 'State');");
            sb.AppendLine("data.addColumn('string', 'Country');");
            sb.AppendLine("data.addColumn('string', 'Web Site');");
            sb.AppendLine("data.addColumn('number', 'Hits');");
            sb.AppendLine("data.addColumn('number', 'Visits'); ");
            sb.AppendLine("debugger;");
            sb.AppendLine("var i = 0;");
            int index = 0;
            sb.AppendLine("data.addRows("+Rows.Count+");");
            foreach (ReportDataRow row in Rows)
            {
                sb.AppendLine("data.setCell("+index+", 0, '"+Clean(row.Name)+"');");
                sb.AppendLine("data.setCell(" + index + ", 1, '" + row.Measures["Address"] + "');");
                sb.AppendLine("data.setCell(" + index + ", 2, '" + row.Measures["City"] + "');");
                sb.AppendLine("data.setCell(" + index + ", 3, '" + row.Measures["State"] + "');");
                sb.AppendLine("data.setCell(" + index + ", 4, '" + row.Measures["Country"] + "');");
                sb.AppendLine("data.setCell(" + index + ", 5, '" + row.Measures["Website"] + "');");
                sb.AppendLine("data.setCell(" + index + ", 6, " + row.Measures["Hits"] + ");");
                sb.AppendLine("data.setCell(" + index + ", 7, " + row.Measures["Visits"] + ");");
                index++;
            }
            sb.AppendLine("var table = new google.visualization.Table(document.getElementById('" + tagId + "'));");
            sb.AppendLine("table.draw(data, {showRowNumber: true});");
            sb.AppendLine("}");
            //sb.AppendLine("</script>");
            return sb.ToString();
        }


    }
}