using System;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using DesktopModules.CSPModules.CspCommons;

namespace DesktopModules.CSPModules.CSPReports
{
    public class ProfileOverviewReport : Report
    {
        public ProfileOverviewReport(string result)
            : base(result, "ProfileStatsReport")
        {
        }

        public override void BuildReport(IQueryable<CspCommons.Company> objects)
        {
            string tmp = string.Empty;
        }

        public override string CreateJavascript(string tagId)
        {
            StringBuilder sb = new StringBuilder();
            //sb.AppendLine("<script type='javascript/text'>");
            sb.AppendLine("google.load('visualization', '1', { 'packages': ['table'] });");
            sb.AppendLine("google.setOnLoadCallback(data);");
            sb.AppendLine("function data() {");
            sb.AppendLine("var data = new google.visualization.DataTable();");
            sb.AppendLine("data.addColumn('string', 'Consumer Name');");
            sb.AppendLine("data.addColumn('string', 'Address');");
            sb.AppendLine("data.addColumn('string', 'City');");
            sb.AppendLine("data.addColumn('string', 'State');");
            sb.AppendLine("data.addColumn('string', 'Country');");
            sb.AppendLine("data.addColumn('string', 'Web Site');");
            sb.AppendLine("data.addColumn('number', 'Hits');");
            sb.AppendLine("data.addColumn('number', 'Visits'); ");
            sb.AppendLine("debugger;");
            sb.AppendLine("var i = 0;");
            int index = 0;
            sb.AppendLine("data.addRows("+Rows.Count+");");
            foreach (ReportDataRow row in Rows)
            {
                sb.AppendLine("data.setCell("+index+", 0, '"+Clean(row.Name)+"');");
                sb.AppendLine("data.setCell(" + index + ", 1, '" + row.Measures["Address"] + "');");
                sb.AppendLine("data.setCell(" + index + ", 2, '" + row.Measures["City"] + "');");
                sb.AppendLine("data.setCell(" + index + ", 3, '" + row.Measures["State"] + "');");
                sb.AppendLine("data.setCell(" + index + ", 4, '" + row.Measures["Country"] + "');");
                sb.AppendLine("data.setCell(" + index + ", 5, '" + row.Measures["Website"] + "');");
                sb.AppendLine("data.setCell(" + index + ", 6, " + row.Measures["Hits"] + ");");
                sb.AppendLine("data.setCell(" + index + ", 7, " + row.Measures["Visits"] + ");");
                index++;
            }
            sb.AppendLine("var table = new google.visualization.Table(document.getElementById('" + tagId + "'));");
            sb.AppendLine("table.draw(data, {showRowNumber: true});");
            sb.AppendLine("}");
            //sb.AppendLine("</script>");
            return sb.ToString();
        }

        #region Overrides of Report

        public override Control CreateHtmlControl(string s)
        {
            Control control = new Control();
            HtmlTable table = new HtmlTable();
            //table.SkinID = "cspReportTable";
            table.Attributes.Add("class", "cspReportTable");
            
            foreach (WtDataRow row in this.Data.DataRow)
            {
                HtmlTableRow tableRow = new HtmlTableRow();
                HtmlTableCell c1 = new HtmlTableCell();
                c1.ColSpan = 2;
                c1.InnerHtml = string.Format("<b>{0}</b>",row.Name);
                tableRow.Cells.Add(c1);
                table.Rows.Add(tableRow);
                int count = 1;
                foreach (WtMeasure attribute in row.Attributes)
                {
                    HtmlTableRow tr = new HtmlTableRow();
                    if (count % 2 == 0) tr.Attributes.Add("class","tbEven");
                    else tr.Attributes.Add("class", "tbOdd");
                    HtmlTableCell cname = new HtmlTableCell();
                    cname.InnerText = attribute.Name;
                    HtmlTableCell cvalue = new HtmlTableCell();
                    cvalue.InnerText = attribute.Value;
                    tr.Cells.Add(cname);
                    tr.Cells.Add(cvalue);
                    table.Rows.Add(tr);
                    count++;
                }

                tableRow = new HtmlTableRow();
                c1 = new HtmlTableCell();
                c1.ColSpan = 2;
                c1.InnerHtml = string.Format("<b>Other Measures</b>");
                tableRow.Cells.Add(c1);
                table.Rows.Add(tableRow);
                foreach (WtMeasure attribute in row.Measures)
                {
                    HtmlTableRow tr = new HtmlTableRow();
                    if (count % 2 == 0) tr.Attributes.Add("class", "tbEven");
                    else tr.Attributes.Add("class", "tbOdd");
                    HtmlTableCell cname = new HtmlTableCell();
                    cname.InnerText = attribute.Name;
                    HtmlTableCell cvalue = new HtmlTableCell();
                    cvalue.InnerText = attribute.Value;
                    tr.Cells.Add(cname);
                    tr.Cells.Add(cvalue);
                    table.Rows.Add(tr);
                    count++;
                }

                
            }
            control.Controls.Add(table);
            return control;
        }

        #endregion
    }
}