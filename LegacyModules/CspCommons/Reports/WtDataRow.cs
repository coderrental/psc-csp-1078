using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace DesktopModules.CSPModules.CspCommons
{
    public class WtDataRow
    {
        private IList<WtMeasure> _measures;
        private IList<WtDataRow> _rows;
        private IList<WtMeasure> _attributes;
        private string _name;

        public WtDataRow()
        {
            _measures = new List<WtMeasure>();
            _rows = new List<WtDataRow>();
            _attributes = new List<WtMeasure>();
            _name = string.Empty;
        }

        public static WtDataRow Parse(XElement element)
        {
            WtDataRow dataRow = new WtDataRow();
            dataRow.Name = element.Attribute("name").Value;
            var lists = from el in element.Elements("list") select el;
            foreach (XElement list in lists)
            {
                XAttribute attribute = list.Attribute("name");
                if (attribute != null)
                {
                    if (attribute.Value.Equals("measures"))
                    {
                        foreach (WtMeasure m in WtMeasure.Parse(list))
                        {
                            dataRow.Measures.Add(m);
                        }
                    }
                    else if (attribute.Value.Equals("SubRows"))
                    {
                        foreach (WtDataRow r in WtDataRow.Parse(list.Elements("DataRow")))
                       {
                           dataRow.Rows.Add(r);
                       }
                    }
                    else if (attribute.Value.Equals("Attributes"))
                    {
                        foreach (WtMeasure m in WtMeasure.Parse(list))
                        {
                            dataRow.Attributes.Add(m);
                        }
                    }
                }
            }
            return dataRow;
        }

        private static IList<WtDataRow> Parse(IEnumerable<XElement> elements)
        {
            List<WtDataRow> rows = new List<WtDataRow>();

            foreach (XElement el in elements)
            {
                WtDataRow dataRow = new WtDataRow();
                dataRow.Name = el.Attribute("name").Value;
                dataRow.Measures = WtMeasure.Parse(el.Element("list"));
                rows.Add(dataRow);
            }
            return rows;
        }

        public IList<WtMeasure> Measures
        {
            get { return _measures; }
            set { _measures = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public IList<WtDataRow> Rows
        {
            get { return _rows; }
            set { _rows = value; }
        }

        public IList<WtMeasure> Attributes
        {
            get { return _attributes; }
            set { _attributes = value; }
        }
    }
}