using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace DesktopModules.CSPModules.CspCommons
{
    public class WtMeasure
    {
        private string _name, _value, _type;
        private object obj;
        public WtMeasure(string name, string value, string type)
        {
            _name = name;
            _value = value;
            _type = type;
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public object Obj
        {
            get { return obj; }
            set { obj = value; }
        }

        public static IList<WtMeasure> Parse(IEnumerable<XElement> element)
        {
            return new List<WtMeasure>();
        }

        public static IList<WtMeasure> Parse(XElement element)
        {
            List<WtMeasure> measures = new List<WtMeasure>();
            foreach (XElement el in element.Descendants())
            {
                measures.Add(new WtMeasure(el.Attribute("name").Value, el.Value, el.Name.ToString()));
            }
            return measures;
        }
    }
}