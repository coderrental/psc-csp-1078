using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace DesktopModules.CSPModules.CspCommons
{
    public class WtDataObj
    {
        private IList<WtDataRow> _dataRow;

        public WtDataObj()
        {
            _dataRow = new List<WtDataRow>();
        }

        public static WtDataObj Parse(string data, string nodeName)
        {
            XDocument doc = XDocument.Parse(data);
            WtDataObj obj = new WtDataObj();

            //parsing
            var elements = from el in doc.Element(nodeName).Element("list").Elements("DataRow") select el;
            foreach (var element in elements)
            {
                obj.DataRow.Add(WtDataRow.Parse(element));
            }
            return obj;
        }

        public IList<WtDataRow> DataRow
        {
            get { return _dataRow; }
            set { _dataRow = value; }
        }
    }
}


