using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using DesktopModules.CSPModules.CspCommons;

namespace DesktopModules.CSPModules.CSPReports
{
    public abstract class Report
    {
        WtDataObj _data;
        private IList<ReportDataRow> _rows;

        public abstract void BuildReport(IQueryable<CspCommons.Company> objects);
        public abstract string CreateJavascript(string tagId);
        

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public Report(string data, string nodeName)
        {
            Data = WtDataObj.Parse(data, nodeName);
            Rows = new List<ReportDataRow>();
        }

        protected Report()
        {
            Data = new WtDataObj();
            Rows = new List<ReportDataRow>();
        }

        public WtDataObj Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public IList<ReportDataRow> Rows
        {
            get { return _rows; }
            set { _rows = value; }
        }
        public string Clean(string s)
        {
            if (string.IsNullOrEmpty(s)) return string.Empty;
            return s.Replace("'", "").Replace("\"", "");
        }

        public virtual WtDataRow FindWtDataRow(string value)
        {
            foreach (WtDataRow row in Data.DataRow)
            {
                foreach (WtDataRow dataRow in row.Rows)
                {
                    if (dataRow.Name.Equals(value))
                    {
                        return dataRow;
                    }

                }
            }
            return null;
        }
        public virtual Control CreateHtmlControl(string s)
        {
            Control control = new Control();
            control.ID = s;
            return control;
        }

    }
}