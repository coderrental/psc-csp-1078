﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DesktopModules.CSPModules.CspCommons
{
    [DefaultProperty("CspScript"), ToolboxData("<{0}:CspScript runat=server></{0}:CspScript>")]
    public partial class CspScript : WebControl
    {
        private int _i = 0;
        public CspScript(int i) : base()
        {
            _i = i;
        }

        #region Overrides of WebControl

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            writer.WriteAttribute("type", "text/javascript");
        }

        protected override void Render(HtmlTextWriter writer)
        {
            string script = "<script type='text/javascript'> test('{0}'); alert('loaded '); </script>";
            //writer.WriteLine("test('123') ;");
            script = "<script type='text/javascript'>{0} // {1}{0}debugger;{0}$.SlideMenu('#slide_menu');{0} </script>";
            //writer.WriteLine(string.Format("//{0}", _i));
            writer.WriteLine(string.Format(script,"\r\n",_i));
        }

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Script;
            }
        }

        protected override string TagName
        {
            get { return "Script"; }
        }

        #endregion
    }

}
