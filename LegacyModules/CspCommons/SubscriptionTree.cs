﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DesktopModules.CSPModules.CspCommons
{
    [Serializable]
    public class SubscriptionTree : UserControl 
    {
        public IQueryable<ThemesCategory> SubscribableCategories { get; set; }
        public IQueryable<SubscriptionProfile> SubscriptionProfiles { get; set; }
        private IList<ProfileItem> _subscribablProfiles;
        Control _p;

        public SubscriptionTree(IQueryable<SubscriptionProfile> subscriptionProfiles)
        {
            SubscriptionProfiles = subscriptionProfiles.OrderByDescending(s=>s.Category.depth);
        }

        public SubscriptionTree(IQueryable<SubscriptionProfile> profiles, IQueryable<ThemesCategory> categories)
        {
            SubscribableCategories = categories.OrderByDescending(c=>c.Category.depth);
            SubscriptionProfiles = profiles.OrderByDescending(s => s.Category.depth);
            _subscribablProfiles = Normalize(SubscribableCategories, SubscriptionProfiles);
            //we want to sort by levels
            
            for (int i = 0; i < _subscribablProfiles.Count();i++ )
            {
                var holder = _subscribablProfiles[i];
                IList<ProfileItem> results =
                    _subscribablProfiles[i].Children.OrderBy(p => p.ThemeCategory.Category.DisplayOrder).ToList<ProfileItem>();
                while (_subscribablProfiles[i].Children.Count() > 0) _subscribablProfiles[i].Children.RemoveAt(0);

                foreach (ProfileItem item in results)
                    _subscribablProfiles[i].Children.Add(item);
            }
             
        }

        private IList<ProfileItem> Sort(IList<ProfileItem> profiles, int level)
        {
            IList<ProfileItem> tmp = new List<ProfileItem>();
            foreach (ProfileItem profile in profiles )
            {
                if (profile.Children.Count() > 0)
                {
                    var obj = profile.Children.OrderBy(p => p.ThemeCategory.Category.DisplayOrder);
                    tmp.Add(obj as ProfileItem);
                }
                else
                {
                    tmp.Add(profile);
                }
            }
            return tmp;
        }


        private IList<ProfileItem> Normalize(IQueryable<ThemesCategory> categories, IQueryable<SubscriptionProfile> profiles)
        {
            IList<ProfileItem> tmp = new List<ProfileItem>();
            foreach (ThemesCategory category in categories)
            {
                var items = from c in tmp where c.ThemeCategory.Category.parentId == category.category_Id 
                            select c;
                if (items.Count() > 0)
                {
                    ProfileItem p = new ProfileItem(category, new List<ProfileItem>(), null);
                    var obj = (from profile in profiles
                               where profile.category_Id == category.category_Id
                              select profile).FirstOrDefault();
                    p.Subscribed = (obj != null);
                    if (p.Subscribed) p.SubscriptionProfile = obj;

                    foreach (ProfileItem item in items)
                    {
                        p.Children.Add(item);
                        item.Parent = p;
                        item.Delete = true;
                    }
                    tmp.Add(p);
                }
                else
                {
                    ProfileItem p = new ProfileItem(category, new List<ProfileItem>(), null);
                    var obj = (from profile in profiles
                               where profile.category_Id == category.category_Id
                               select profile).FirstOrDefault();
                    p.Subscribed = (obj != null);
                    if (p.Subscribed) p.SubscriptionProfile = obj;
                    tmp.Add(p);
                }
            }
            int count = 0;

            while (count < tmp.Count())
            {
                if (tmp[count].Delete)
                    tmp.RemoveAt(count);
                else
                {
                    count++;
                }
            }
            return tmp;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                if (!Page.IsPostBack)
                {
                    //_p = GenerateControls(Normalize(SubscriptionProfiles));
                    //Controls.Add(_p);
                    Controls.Add(GenerateControls(_subscribablProfiles));
                    
                }
                else
                {
                    string tmp = "";
                }
            }
        }
        

        private IList<ProfileItem> Normalize(IQueryable<SubscriptionProfile> profiles)
        {
            IList<ProfileItem> tmp = new List<ProfileItem>();
            foreach (SubscriptionProfile profile in profiles)
            {
                var items = from c in tmp where c.SubscriptionProfile.Category.parentId == profile.category_Id select c;
                if (items.Count() > 0)
                {
                    ProfileItem p = new ProfileItem(profile, new List<ProfileItem>(), null);
                    foreach (ProfileItem item in items)
                    {
                        p.Children.Add(item);
                        item.Parent = p;
                        item.Delete = true;
                    }
                    tmp.Add(p);
                }
                else
                {
                    tmp.Add(new ProfileItem(profile, new List<ProfileItem>(), null));
                }
            }
            int count = 0;

            while (count < tmp.Count())
            {
                if (tmp[count].Delete)
                    tmp.RemoveAt(count);
                else
                {
                    count++;
                }
            }
            return tmp;
        }

        private Control GenerateControls(IList<ProfileItem> profiles)
        {
            Panel control = new Panel();
            foreach (ProfileItem item in profiles)
            {
                Panel p = new Panel();
                CheckBox cb = new CheckBox();
                //cb.Checked = from s in item.Cat.SubscriptionProfiles where s.

                cb.ID = item.ThemeCategory.category_Id.ToString(); //item.SubscriptionProfile.Category.categoryId.ToString();
                cb.Checked = item.Subscribed;
                cb.Text = item.ThemeCategory.Category.categoryText;
                cb.InputAttributes.Add("cid", item.ThemeCategory.category_Id.ToString());
                cb.InputAttributes.Add("pid", item.ThemeCategory.Category.lineage.Replace("/", ","));
                cb.InputAttributes.Add("onclick", "handleCategoryClickEvent(this)");
                p.Style.Add("padding-left", string.Format("{0}px", item.ThemeCategory.Category.depth * 15));
                p.Controls.Add(cb);
                //_p.Controls.Add(lb);
                if (item.Children.Count() > 0) {
                    p.Controls.Add(GenerateControls(item.Children));
                }
                else p.Attributes.Add("class", "cspCategoryDiv");

                control.Controls.Add(p);
            }
            return control;

        }
    }

    internal class ProfileItem
    {
        private SubscriptionProfile _subscriptionProfile;
        private ThemesCategory _themesCategory;
        private IList<ProfileItem> _children;
        private ProfileItem _parent;
        private bool _delete;
        private bool _subscribed;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public ProfileItem(SubscriptionProfile subscriptionProfile, IList<ProfileItem> children, ProfileItem parent)
        {
            _subscriptionProfile = subscriptionProfile;
            _themesCategory = null;
            _children = children;
            _parent = parent;
            _delete = false;
            _subscribed = false;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public ProfileItem(ThemesCategory themesCategory, IList<ProfileItem> children, ProfileItem parent)
        {
            _themesCategory = themesCategory;
            _children = children;
            _parent = parent;
            _subscriptionProfile = null;
            _delete = false;
            _subscribed = false;
        }

        public ThemesCategory ThemeCategory
        {
            get { return _themesCategory; }
            set { _themesCategory = value; }
        }

        public bool Subscribed
        {
            get { return _subscribed; }
            set { _subscribed = value; }
        }

        public SubscriptionProfile SubscriptionProfile
        {
            get { return _subscriptionProfile; }
            set { _subscriptionProfile = value; }
        }

        public IList<ProfileItem> Children
        {
            get { return _children; }
            set { _children = value; }
        }

        public ProfileItem Parent
        {
            get { return _parent; }
            set { _parent = value; }
        }

        public bool Delete
        {
            get { return _delete; }
            set { _delete = value; }
        }
    }
}
