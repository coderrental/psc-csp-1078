﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Users;
using DotNetNuke.Security.Membership;
using DotNetNuke.Security.Roles;

namespace DesktopModules.CSPModules.CspCommons
{
    public class CompanySync
    {
        public string _address1;
        public string _address2;
        public string _baseDomain;
        public string _city;
        public int _companiesId;
        public string _companyLogoPrintUrl;
        public string _companyLogoWebUrl;
        public string _companyname;

        public string _contactUsEmail;
        public string _country;
        public string _dataSourceId;
        public int? _defaultLanguage;
        public string _emailAddress;
        public string _entryPoint;
        public int? _fallbackLanguage;
        public string _firstName;
        public bool _isconsumer;
        public bool _issupplier;
        public string _lastName;
        public string _launchType;
        public string _phone;
        public string _publicationRule;
        public string _state;
        public string _syndicationCustomImage;
        public string _syndicationDefaultExploreProductImage;
        public string _syndicationDefaultImage1;
        public string _syndicationDefaultImage2;
        public string _syndicationExploreProductEp;
        public string _syndicationExploreProductImage;
        public string _syndicationExploreProductImageOption;
        public string _syndicationExploreProductLt;
        public string _syndicationExploreProductText;
        public string _syndicationIframeTarget;
        public string _syndicationImageOption;
        public string _syndicationLinkImage;
        public string _syndicationLinkText;
        public string _websiteurl;
        public string _zipcode;

        public Dictionary<string, string> Parameters { get; set; }


        //private string _global_bin_local_path;

        //    private string _global_bin_base_url;

        //  private string _consumer_bin_local_path;

        //private string _consumer_bin_base_url;

        //    private string _supplier_bin_local_path;

        //  private string _supplier_bin_base_url;

        //private string _displayname;

        //private int _content_staging_scheme;

        //private int _parent_companies_Id;

        // private DateTime _created;

        // private DateTime _date_changed;

        // private string _type;

        // private string _externalCode;

        public CompanySync(int companiesId,
                           string companyname,
                           string address1,
                           string address2,
                           string city,
                           string zipcode,
                           string state,
                           string country,
                           string websiteurl,
                           bool isconsumer,
                           bool issupplier,
                           string contactUsEmail,
                           string dataSourceId,
                           string entryPoint,
                           string syndicationImageOption,
                           string launchType,
                           string syndicationExploreProductEp,
                           string syndicationExploreProductImageOption,
                           string syndicationExploreProductImage,
                           string syndicationDefaultExploreProductImage,
                           string syndicationLinkImage,
                           string phone,
                           string firstName,
                           string lastName,
                           string emailAddress,
                           string baseDomain,
                           int? defaultLanguage,
                           int? fallbackLanguage,
                           string CompanyLogoWebUrl,
                           string CompanyLogoPrintUrl,
                           string PublicationRule,
                           string SyndicationCustomImage,
                           string syndicationDefaultImage1,
                           string syndicationDefaultImage2,
                           string syndicationLinkText,
                           string syndicationExploreProductText,
                           string syndicationIframeTarget,
                           string syndicationExploreProductLt
            )
        {
            Parameters = new Dictionary<string, string>();
            _companiesId = companiesId;
            _companyname = companyname;
            _address1 = address1;
            _address2 = address2;
            _city = city;
            _zipcode = zipcode;
            _state = state;
            _country = country;
            _websiteurl = websiteurl;
            _isconsumer = isconsumer;
            _issupplier = issupplier;

            _emailAddress = emailAddress;
            _baseDomain = baseDomain;
            _defaultLanguage = defaultLanguage;
            _fallbackLanguage = fallbackLanguage;
            _syndicationCustomImage = SyndicationCustomImage;
            _syndicationDefaultImage1 = syndicationDefaultImage1;
            _syndicationDefaultImage2 = syndicationDefaultImage2;
            _publicationRule = PublicationRule;
            _companyLogoPrintUrl = CompanyLogoPrintUrl;
            _companyLogoWebUrl = CompanyLogoWebUrl;
            _lastName = lastName;
            _firstName = firstName;
            _phone = phone;
            _syndicationLinkImage = syndicationLinkImage;
            _syndicationDefaultExploreProductImage = syndicationDefaultExploreProductImage;
            _syndicationExploreProductImage = syndicationExploreProductImage;
            _syndicationExploreProductImageOption = syndicationExploreProductImageOption;
            _syndicationExploreProductEp = syndicationExploreProductEp;
            _launchType = launchType;
            _syndicationImageOption = syndicationImageOption;
            _entryPoint = entryPoint;
            _dataSourceId = dataSourceId;
            _contactUsEmail = contactUsEmail;
            _syndicationLinkText = syndicationLinkText;
            _syndicationExploreProductText = syndicationExploreProductText;
            _syndicationIframeTarget = syndicationIframeTarget;
            _syndicationExploreProductLt = syndicationExploreProductLt;

            if (String.IsNullOrEmpty(_syndicationLinkImage))
            {
                _syndicationLinkImage = "";
            }


            if (String.IsNullOrEmpty(_syndicationDefaultExploreProductImage))
            {
                _syndicationDefaultExploreProductImage = "";
            }
            if (String.IsNullOrEmpty(_syndicationExploreProductImage))
            {
                _syndicationExploreProductImage = "";
            }

            if (String.IsNullOrEmpty(_syndicationExploreProductImageOption))
            {
                _syndicationExploreProductImageOption = "";
            }

            if (String.IsNullOrEmpty(_syndicationExploreProductEp))
            {
                _syndicationExploreProductEp = "";
            }

            if (String.IsNullOrEmpty(_launchType))
            {
                _launchType = "";
            }

            if (String.IsNullOrEmpty(_syndicationImageOption))
            {
                _syndicationImageOption = "";
            }

            if (String.IsNullOrEmpty(_entryPoint))
            {
                _entryPoint = "";
            }

            if (String.IsNullOrEmpty(_dataSourceId))
            {
                _dataSourceId = "";
            }

            if (String.IsNullOrEmpty(_contactUsEmail))
            {
                _contactUsEmail = "";
            }

            if (String.IsNullOrEmpty(_syndicationLinkText))
            {
                _syndicationLinkText = "";
            }
            if (String.IsNullOrEmpty(_syndicationExploreProductText))
            {
                _syndicationExploreProductText = "";
            }
            if (String.IsNullOrEmpty(_syndicationIframeTarget))
            {
                _syndicationIframeTarget = "";
            }
            if (String.IsNullOrEmpty(_syndicationExploreProductLt))
            {
                _syndicationExploreProductLt = "";
            }
        }
    }

    public class CSPSyncSystems
    {
        private ArrayList _portalInfos;
        public string HostUrl;
        private PortalController PC;
        public List<UserInfo> userInfos;

        public CSPSyncSystems(int CurUserId, String hostURL)
        {
            try
            {
                HostUrl = hostURL;
                RefreshPortalAndUserInfos(CurUserId);
            }
            catch (Exception ex)
            {
                using (DnnDataLayerDataContext ct =
                    new DnnDataLayerDataContext(Config.GetConnectionString()))
                {
                    CspErrorLog errorLog = new CspErrorLog
                                               {
                                                   userId = CurUserId,
                                                   date = DateTime.Now,
                                                   error = ex.Message,
                                                   extra = ex.StackTrace
                                               };
                    ct.CspErrorLogs.InsertOnSubmit(errorLog);
                    ct.SubmitChanges(ConflictMode.ContinueOnConflict);
                }
            }
        }

        public void RefreshPortalAndUserInfos(int CurUserId)
        {
            PC = new PortalController();
            _portalInfos = PC.GetPortals();
            userInfos = new List<UserInfo>();
            foreach (PortalInfo portalInfo in _portalInfos)
            {
                //html = html + " Portal " + portalInfo.PortalID.ToString();
                userInfos.Add(UserController.GetUserById(portalInfo.PortalID, CurUserId));
            }
        }

        public void AddUserToPortalsWhereUserDoesntExist(UserInfo InUser)
        {
            try
            {
                foreach (PortalInfo portalInfo in _portalInfos)
                {
                    UserInfo HoldUser = GetUserInfoByPortalId(portalInfo.PortalID);
                    if (HoldUser == null)
                    {
                        HoldUser = new UserInfo();
                        HoldUser.PortalID = portalInfo.PortalID;
                        HoldUser.Membership.Username = InUser.Username;
                        HoldUser.Username = InUser.Username;
                        HoldUser.FirstName = InUser.FirstName;
                        HoldUser.LastName = InUser.LastName;
                        HoldUser.DisplayName = InUser.DisplayName;
                        HoldUser.Membership.Email = InUser.Membership.Email;
                        HoldUser.Membership.Approved = InUser.Membership.Approved;
                        HoldUser.Membership.Password = UserController.GetPassword(ref InUser, "");
                        HoldUser.Email = InUser.Email;
                        HoldUser.Profile.PreferredLocale = InUser.Profile.PreferredLocale;
                        HoldUser.Profile.TimeZone = InUser.Profile.TimeZone;
                        HoldUser.Profile.Telephone = InUser.Profile.Telephone;
                        if (UserCreateStatus.Success == UserController.CreateUser(ref HoldUser))
                        {
                            RoleController RC = new RoleController();
                            ArrayList HoldRoles = RC.GetRoles();
                            List<RoleInfo> InUserHasTheseRoles = new List<RoleInfo>();

                            foreach (RoleInfo holdRole in HoldRoles)
                            {
                                if (holdRole.PortalID == InUser.PortalID)
                                {
                                    if (InUser.IsInRole(holdRole.RoleName))
                                    {
                                        InUserHasTheseRoles.Add(holdRole);
                                    }
                                }
                            }

                            foreach (RoleInfo holdRole in InUserHasTheseRoles)
                            {
                                RoleInfo PortalRole = RC.GetRoleByName(portalInfo.PortalID, holdRole.RoleName);
                                if (PortalRole != null)
                                {
                                    RC.AddUserRole(portalInfo.PortalID, InUser.UserID, PortalRole.RoleID, Null.NullDate);
                                }
                            }
                        }
                        else
                        {
                            throw new Exception("Error trying to auto add user to other portals user: " + InUser.UserID +
                                                " Initating Portal ID: " + InUser.PortalID
                                                + " Tried to create user inside portal id: " + HoldUser.PortalID);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                using (DnnDataLayerDataContext ct =
                    new DnnDataLayerDataContext(Config.GetConnectionString()))
                {
                    CspErrorLog errorLog = new CspErrorLog
                                               {
                                                   userId = InUser.UserID,
                                                   date = DateTime.Now,
                                                   error = ex.Message,
                                                   extra = ex.StackTrace
                                               };
                    ct.CspErrorLogs.InsertOnSubmit(errorLog);
                    ct.SubmitChanges(ConflictMode.ContinueOnConflict);
                }
            }
        }

        public UserInfo GetUserInfoByPortalId(int InPortalID)
        {
            foreach (var userInfo in userInfos)
            {
                if (userInfo != null)
                {
                    if (userInfo.PortalID == InPortalID && userInfo.IsDeleted == false)
                    {
                        return userInfo;
                    }
                }
                else
                {
                    return null;
                }
            }
            return null;
        }

        public int GetIntegrationKey(UserInfo InUser)
        {
            int integrationKey = -1;
            if (InUser != null)
            {
                if (InUser.Profile != null)
                {
                    if (!int.TryParse(InUser.Profile.GetPropertyValue(Globals.IntegrationKey), out integrationKey))
                    {
                        integrationKey = -1;
                    }
                }
            }

            return integrationKey;
        }

        public int GetIntegrationKeyFromPortal(int InPortalId)
        {
            return GetIntegrationKey(GetUserInfoByPortalId(InPortalId));
        }

        private UserInfo GetAUserWhereUserSubscribedAlready(int ThisPortalId)
        {
            // see if you can find any portal where the current user already has an integration key
            foreach (var userInfo in userInfos)
            {
                if (userInfo != null)
                {
                    if (userInfo.PortalID != ThisPortalId && userInfo.IsDeleted == false)
                    {
                        int Key = GetIntegrationKeyFromPortal(userInfo.PortalID);
                        if (Key > 0)
                        {
                            return userInfo;
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// TryAndGetNewUserCompanyInfoFromOtherPortals
        /// </summary>
        /// <param name="ThisPortalId"></param>
        /// <returns>
        /// return true if try and get a user from a different portal
        /// return false if the user is found on the current system
        /// </returns>
        public bool TryAndGetNewUserCompanyInfoFromOtherPortals(int ThisPortalId)
        {
            try
            {
                // if this is a new user (check the integration key)
                int ThisPortalsIntegrationKey = GetIntegrationKeyFromPortal(ThisPortalId);

                if (ThisPortalsIntegrationKey == -1)
                {
                    //I'm a new user locally, try and find any portal where i'm not a new user
                    UserInfo _userInfo = GetAUserWhereUserSubscribedAlready(ThisPortalId);
                    if (_userInfo != null)
                    {
                        // found one!
                        CreateCSPCompanyForPortalA_UsingDataFromPortalB(ThisPortalId, _userInfo.PortalID);
                        return true;
                    }
                    else
                    {
                        // i'm really a new user to the whole system
                    }
                }
                else
                {
                    // probably shoudn't happen
                }
                return false;
            }
            catch (Exception ex)
            {
                using (DnnDataLayerDataContext ct =
                    new DnnDataLayerDataContext(Config.GetConnectionString()))
                {
                    CspErrorLog errorLog = new CspErrorLog
                                               {
                                                   userId = userInfos.First().UserID,
                                                   date = DateTime.Now,
                                                   error = ex.Message,
                                                   extra = ex.StackTrace
                                               };
                    ct.CspErrorLogs.InsertOnSubmit(errorLog);
                    ct.SubmitChanges(ConflictMode.ContinueOnConflict);
                }
            }
            return false;
        }

        private bool CheckAndInsertParam(Company consumer, IQueryable<CompaniesParameterType> propertyTypeQuery,
                                         string key, KeyValuePair<string, string> pair,
                                         CspDataLayerDataContext InContext)
        {
            bool isSubmit = false;
            var obj = (from p in consumer.CompaniesParameters
                       where p.CompaniesParameterType.parametername == key
                       select p).SingleOrDefault();
            if (obj != null)
            {
                if (obj.CompaniesParameterType.parametertype == 1)
                {
                    if (!obj.value_text.Equals(pair.Value))
                    {
                        obj.value_text = pair.Value;
                        //_context.SubmitChanges(ConflictMode.FailOnFirstConflict);
                        if (!isSubmit) isSubmit = true;
                    }
                }
            }
            else
            {
                CompaniesParameterType parameterType =
                    (from pt in propertyTypeQuery where pt.parametername == key select pt).SingleOrDefault();
                if (parameterType != null)
                {
                    var parameter = new CompaniesParameter
                                        {
                                            Company = consumer,
                                            CompaniesParameterType = parameterType,
                                            value_text = pair.Value
                                        };
                    InContext.CompaniesParameters.InsertOnSubmit(parameter);
                    if (!isSubmit) isSubmit = true;
                }
            }
            return isSubmit;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="InContext"></param>
        /// <param name="InCompany"></param>
        /// <param name="InPortalID">Destination Portal Id</param>
        public void CreateAndInsertCompany(CspDataLayerDataContext InContext, CompanySync InCompany, int InPortalID)
        {
            //using InContext and InCompany, create a company record!
            UserInfo _curUser = GetUserInfoByPortalId(InPortalID);
            string externalIdentifier = Config.GetSetting("Portal" + InPortalID + "ExternalIdentifier");
            bool syncAllConsumerParameters = string.Equals(Config.GetSetting("Portal" + InPortalID + "SyncAllConsumerParameters"), "true", StringComparison.OrdinalIgnoreCase);
            if (string.IsNullOrEmpty(externalIdentifier))
                externalIdentifier = HostUrl;

            Channel channel =
                (from c in InContext.Channels where c.ExternalIdentifier == externalIdentifier select c).
                    FirstOrDefault();
            InContext.Channels.Single<Channel>(c => c.ExternalIdentifier == externalIdentifier);
            if (channel != null)
            {
                try
                {
                    Company _company = new Company
                                           {
                                               companyname = InCompany._companyname,
                                               address1 = InCompany._address1,
                                               address2 = InCompany._address2,
                                               city = InCompany._city,
                                               state = InCompany._state,
                                               zipcode = InCompany._zipcode,
                                               country = InCompany._country,
                                               website_url = InCompany._websiteurl,
                                               is_consumer = InCompany._isconsumer,
                                               is_supplier = InCompany._issupplier,
                                               displayname = InCompany._companyname,
                                               date_changed = DateTime.Today,
                                               created = DateTime.Today,
                                               parent_companies_Id = channel.CompanyId
                                           };
                    string BaseDomainHold = "";
                    if (InCompany._baseDomain.Contains("."))
                        BaseDomainHold = string.Format("{0}.{1}",
                                                       InCompany._baseDomain.Substring(0, InCompany._baseDomain.IndexOf(".")),
                                                       channel.BaseDomain);
                    else
                        BaseDomainHold = string.Format("{0}.{1}",
                                                       InCompany._baseDomain,
                                                       channel.BaseDomain);
                    //check  if this base domain exists already
                    if (InContext.CompaniesConsumers.Count(a => a.base_domain == BaseDomainHold) > 0)
                        BaseDomainHold = DateTime.Now.Ticks.ToString() + "." + channel.BaseDomain;

                    CompaniesConsumer _consumer = new CompaniesConsumer
                                                      {
                                                          Company = _company,
                                                          active = true,
                                                          base_domain =
                                                              BaseDomainHold,
                                                          base_publication_Id =
                                                              channel.BasePublicationId,
                                                          base_publication_parameters =
                                                              channel.DefaultParameters,
                                                          default_language_Id = InCompany._defaultLanguage
                                                          ,
                                                          fallback_language_Id =
                                                              InCompany._fallbackLanguage
                                                      };
                    var channelThemes = from t in InContext.ChannelThemeAssocations
                                        where t.ChannelId == channel.Id &&
                                            channel.ExternalIdentifier == externalIdentifier
                                        select t;
                    var propertyTypes = from pt in InContext.CompaniesParameterTypes select pt;
                    foreach (ChannelThemeAssocation themeAssocation in channelThemes)
                    {
                        InContext.CompaniesThemes.InsertOnSubmit(new CompaniesTheme
                                                                     {
                                                                         Company = _company,
                                                                         themes_Id =
                                                                             themeAssocation.ThemeId
                                                                     });
                    }
                    //InContext.Companies.Add(_company);
                    InContext.Companies.InsertOnSubmit(_company);
                    InContext.CompaniesConsumers.InsertOnSubmit(_consumer);

                    //sync all of the other parameters
                    if (syncAllConsumerParameters && InCompany.Parameters.Count > 0)
                    {
                        foreach (KeyValuePair<string, string> parameter in InCompany.Parameters)
                        {
                            CheckAndInsertParam(_company, propertyTypes, parameter.Key, parameter, InContext);
                        }
                    }

                    //add consumer parameters
                    CheckAndInsertParam(_company, propertyTypes, Globals.ContactUsEmail,
                                        new KeyValuePair<string, string>("", InCompany._contactUsEmail), InContext);
                    CheckAndInsertParam(_company, propertyTypes, Globals.DataSourceId,
                                        new KeyValuePair<string, string>("", InCompany._dataSourceId), InContext);
                    CheckAndInsertParam(_company, propertyTypes, Globals.EntryPoint,
                                        new KeyValuePair<string, string>("", InCompany._entryPoint), InContext);
                    CheckAndInsertParam(_company, propertyTypes, Globals.SyndicationImageOption,
                                        new KeyValuePair<string, string>("", InCompany._syndicationImageOption),
                                        InContext);
                    CheckAndInsertParam(_company, propertyTypes, Globals.LaunchType,
                                        new KeyValuePair<string, string>("", InCompany._launchType), InContext);
                    CheckAndInsertParam(_company, propertyTypes, Globals.SyndicationExploreProductEp,
                                        new KeyValuePair<string, string>("", InCompany._syndicationExploreProductEp),
                                        InContext); //default to image
                    CheckAndInsertParam(_company, propertyTypes,
                                        Globals.SyndicationExploreProductImageOption,
                                        new KeyValuePair<string, string>("",
                                                                         InCompany._syndicationExploreProductImageOption),
                                        InContext); //default to image

                    string DefaultEPImage =
                        Config.GetSetting("Portal" + InPortalID + "EPMicrobButton88x31");
                    if (string.IsNullOrEmpty(DefaultEPImage)) DefaultEPImage = string.Empty;
                    CheckAndInsertParam(_company, propertyTypes, Globals.SyndicationExploreProductImage,
                                        new KeyValuePair<string, string>("",
                                                                         DefaultEPImage), InContext);
                    //default to image

                    CheckAndInsertParam(_company, propertyTypes, Globals.SyndicationLinkImage,
                                        new KeyValuePair<string, string>("",
                                                                         "SyndicationDefaultImage1"), InContext);
                    CheckAndInsertParam(_company, propertyTypes, Globals.SyndicationExploreProductLt,
                                        new KeyValuePair<string, string>("",
                                                                         "1"), InContext);

                    CheckAndInsertParam(_company, propertyTypes, Globals.SyndicationExploreProductText,
                                        new KeyValuePair<string, string>("",
                                                                         "Explore Product"), InContext);
                    string DefaultSLinkText =
                        Config.GetSetting("Portal" + InPortalID + "InstanceName");
                    if (string.IsNullOrEmpty(DefaultSLinkText)) DefaultSLinkText = string.Empty;
                    CheckAndInsertParam(_company, propertyTypes, Globals.SyndicationLinkText,
                                        new KeyValuePair<string, string>("",
                                                                         DefaultSLinkText), InContext);

                    CheckAndInsertParam(_company, propertyTypes, Globals.Dnn_UserName, new KeyValuePair<string, string>("", _curUser.Username), InContext);



                    //contact
                    string phone = string.Empty;
                    if (!string.IsNullOrEmpty(InCompany._phone))
                        phone = InCompany._phone;
                    Contact contact = new Contact
                                          {
                                              Company = _company,
                                              firstname = InCompany._firstName,
                                              lastname = InCompany._lastName,
                                              emailaddress = InCompany._emailAddress,
                                              user_Id = new Guid("00000000-0000-0000-0000-000000000000"),
                                              telephone = phone
                                          };
                    InContext.Contacts.InsertOnSubmit(contact);

                    //InContext.Companies.Attach(_company, false);
                    InContext.SubmitChanges(ConflictMode.FailOnFirstConflict);

                    _curUser.Profile.SetProfileProperty(Globals.IntegrationKey,
                                                        _company.companies_Id.ToString());
                    _curUser.Profile.SetProfileProperty(Globals.ContactUsEmail, InCompany._contactUsEmail);
                    _curUser.Profile.SetProfileProperty(Globals.DataSourceId, InCompany._dataSourceId);
                    _curUser.Profile.SetProfileProperty(Globals.EntryPoint, InCompany._entryPoint);
                    _curUser.Profile.SetProfileProperty(Globals.LaunchType, InCompany._launchType);
                    _curUser.Profile.SetProfileProperty(Globals.CompanyLogoWebKey, InCompany._companyLogoWebUrl);
                    _curUser.Profile.SetProfileProperty(Globals.CompanyLogoPrintKey, InCompany._companyLogoPrintUrl);
                    _curUser.Profile.SetProfileProperty(Globals.PublicationRuleKey, InCompany._publicationRule);
                    _curUser.Profile.SetProfileProperty(Globals.SyndicationDefaultImage1,
                                                        InCompany._syndicationDefaultImage1);
                    _curUser.Profile.SetProfileProperty(Globals.SyndicationDefaultImage2,
                                                        InCompany._syndicationDefaultImage2);
                    _curUser.Profile.SetProfileProperty(Globals.SyndicationCustomImage,
                                                        InCompany._syndicationCustomImage);
                    string DefaultSyndicationLinkText =
                        Config.GetSetting("Portal" + InPortalID + "InstanceName");

                    _curUser.Profile.SetProfileProperty(Globals.SyndicationLinkText, DefaultSyndicationLinkText);
                    _curUser.Profile.SetProfileProperty(Globals.SyndicationImageOption,
                                                        InCompany._syndicationImageOption);
                    _curUser.Profile.SetProfileProperty(Globals.SyndicationLinkImage, InCompany._syndicationLinkImage);
                    _curUser.Profile.SetProfileProperty(Globals.SyndicationExploreProductText,
                                                        InCompany._syndicationExploreProductText);
                    _curUser.Profile.SetProfileProperty(Globals.SyndicationExploreProductImageOption,
                                                        InCompany._syndicationExploreProductImageOption);
                    _curUser.Profile.SetProfileProperty(Globals.SyndicationExploreProductImage,
                                                        InCompany._syndicationExploreProductImage);
                    _curUser.Profile.SetProfileProperty(Globals.SyndicationExploreProductEp,
                                                        InCompany._syndicationExploreProductEp);
                    _curUser.Profile.SetProfileProperty(Globals.SyndicationIFrameTarget,
                                                        InCompany._syndicationIframeTarget);
                    _curUser.Profile.SetProfileProperty(Globals.SyndicationExploreProductLt,
                                                        InCompany._syndicationExploreProductLt);

                    UserController.UpdateUser(InPortalID, _curUser);
                }
                catch (Exception ex)
                {
                    using (
                        DnnDataLayerDataContext ct =
                            new DnnDataLayerDataContext(Config.GetConnectionString()))
                    {
                        CspErrorLog errorLog = new CspErrorLog
                                                   {
                                                       userId = _curUser.UserID,
                                                       date = DateTime.Now,
                                                       error = ex.Message,
                                                       extra = ex.StackTrace
                                                   };
                        ct.CspErrorLogs.InsertOnSubmit(errorLog);
                        ct.SubmitChanges(ConflictMode.ContinueOnConflict);
                    }
                }
            }
            else
            {
                // channel not setup for that portal. shoudl never happen
            }
        }

        public CompanySync GetCompanyInfoFromContext(CspDataLayerDataContext InContext, int InPortalId)
        {
            int companiesId = -1;
            string companyname;
            string address1;
            string address2;
            string city;
            string zipcode;
            string state;
            string country;
            string websiteurl;
            bool isconsumer = true;
            bool issupplier = false;
            string contactUsEmail;
            string dataSourceId;
            string entryPoint;
            string syndicationImageOption;
            string launchType;
            string syndicationExploreProductEp;
            string syndicationExploreProductImageOption;
            string syndicationExploreProductImage;
            string syndicationDefaultExploreProductImage;
            string syndicationLinkImage;
            string phone;
            string firstName;
            string lastName;
            string emailAddress;
            string baseDomain = "";
            int? defaultLanguage = null;
            int? fallbackLanguage= null;

            string CompanyLogoWebUrl;
            string CompanyLogoPrintUrl;
            string PublicationRule;
            string SyndicationCustomImage;
            string syndicationDefaultImage1;
            string syndicationDefaultImage2;
            string syndicationLinkText;
            string syndicationExploreProductText;
            string syndicationIframeTarget;
            string syndicationExploreProductLt;

            UserInfo PortalUser = GetUserInfoByPortalId(InPortalId);
            DataLoadOptions options = new DataLoadOptions();
            options.LoadWith<Company>(c => c.displayname);
            InContext.LoadOptions = options;
            int CompanyID = GetIntegrationKey(PortalUser);
            Company company = InContext.Companies.Single<Company>(c => c.companies_Id == CompanyID); // if company id doesnt exist.
            Contact AContact = company.Contacts.FirstOrDefault();
            CompaniesConsumer AConsumer = company.CompaniesConsumers.FirstOrDefault();

            companyname = company.companyname;
            address1 = company.address1;
            address2 = company.address2;
            city = company.city;
            zipcode = company.zipcode;
            state = company.state;
            country = company.country;
            websiteurl = company.website_url;
            contactUsEmail = PortalUser.Profile.GetPropertyValue(Globals.ContactUsEmail);
            dataSourceId = PortalUser.Profile.GetPropertyValue(Globals.DataSourceId);
            entryPoint = PortalUser.Profile.GetPropertyValue(Globals.EntryPoint);
            syndicationImageOption = PortalUser.Profile.GetPropertyValue(Globals.SyndicationImageOption);
            launchType = PortalUser.Profile.GetPropertyValue(Globals.LaunchType);
            syndicationExploreProductEp = PortalUser.Profile.GetPropertyValue(Globals.SyndicationExploreProductEp);
            syndicationExploreProductImageOption =
                PortalUser.Profile.GetPropertyValue(Globals.SyndicationExploreProductImageOption);
            syndicationExploreProductImage = PortalUser.Profile.GetPropertyValue(Globals.SyndicationExploreProductImage);
            syndicationDefaultExploreProductImage = PortalUser.Profile.GetPropertyValue(Globals.SyndicationDefaultImage1);
            syndicationLinkImage = PortalUser.Profile.GetPropertyValue(Globals.SyndicationLinkImage);
            CompanyLogoWebUrl = PortalUser.Profile.GetPropertyValue(Globals.CompanyLogoWebKey);
            CompanyLogoPrintUrl = PortalUser.Profile.GetPropertyValue(Globals.CompanyLogoPrintKey);
            PublicationRule = PortalUser.Profile.GetPropertyValue(Globals.PublicationRuleKey);
            SyndicationCustomImage = PortalUser.Profile.GetPropertyValue(Globals.SyndicationCustomImage);
            syndicationDefaultImage1 = PortalUser.Profile.GetPropertyValue(Globals.SyndicationDefaultImage1);
            syndicationDefaultImage2 = PortalUser.Profile.GetPropertyValue(Globals.SyndicationDefaultImage2);
            syndicationLinkText = PortalUser.Profile.GetPropertyValue(Globals.SyndicationLinkText);
            syndicationExploreProductText = PortalUser.Profile.GetPropertyValue(Globals.SyndicationExploreProductText);
            syndicationIframeTarget = PortalUser.Profile.GetPropertyValue(Globals.SyndicationIFrameTarget);
            syndicationExploreProductLt = PortalUser.Profile.GetPropertyValue(Globals.SyndicationExploreProductLt);
            if (AContact != null)
            {
                phone = AContact.telephone;
                firstName = AContact.firstname;
                lastName = AContact.lastname;
                emailAddress = AContact.emailaddress;
            }
            else
            {
                phone = firstName = lastName = emailAddress = string.Empty;
            }

            if (AConsumer != null)
            {
                baseDomain = AConsumer.base_domain;
                defaultLanguage = AConsumer.default_language_Id;
                fallbackLanguage = AConsumer.fallback_language_Id;
            }            

            CompanySync OutCompany = new CompanySync(companiesId, companyname, address1, address2, city, zipcode, state,
                                                     country, websiteurl, isconsumer, issupplier, contactUsEmail,
                                                     dataSourceId,
                                                     entryPoint, syndicationImageOption, launchType,
                                                     syndicationExploreProductEp, syndicationExploreProductImageOption,
                                                     syndicationExploreProductImage,
                                                     syndicationDefaultExploreProductImage,
                                                     syndicationLinkImage, phone, firstName, lastName, emailAddress,
                                                     baseDomain, defaultLanguage, fallbackLanguage, CompanyLogoWebUrl,
                                                     CompanyLogoPrintUrl,
                                                     PublicationRule, SyndicationCustomImage, syndicationDefaultImage1,
                                                     syndicationDefaultImage2, syndicationLinkText,
                                                     syndicationExploreProductText, syndicationIframeTarget,
                                                     syndicationExploreProductLt);
            foreach (CompaniesParameter parameter in company.CompaniesParameters.Where(a=>a.CompaniesParameterType.parametertype == 1))
            {
                OutCompany.Parameters.Add(parameter.CompaniesParameterType.parametername, parameter.value_text);
            }

            return OutCompany;
        }

        public void CreateCSPCompanyForPortalA_UsingDataFromPortalB(int PortalA, int PortalB)
        {
            // responsible for looking up the company information from Portal B and pushing the content to Portal A's CSP system

            /* the local user infos should all be for one user
             * loop thorugh them to get the right one for each user
             * Connect to the underlying CSP database for PortalB, grab all the relevant information and stick it into PortalA's 
             * 
             */
            // consider reusing Create / update
            try
            {
                using (
                    CspDataLayerDataContext _contextA =
                        new CspDataLayerDataContext(Config.GetConnectionString("CspPortal" + PortalA.ToString()))
                    )
                {
                    using (
                        CspDataLayerDataContext _contextB =
                            new CspDataLayerDataContext(Config.GetConnectionString("CspPortal" + PortalB.ToString())))
                    {
                        CompanySync CompanyB = GetCompanyInfoFromContext(_contextB, PortalB);
                        CreateAndInsertCompany(_contextA, CompanyB, PortalA);
                    }
                }
            }
            catch (Exception ex)
            {
                using (
                    DnnDataLayerDataContext ct =
                        new DnnDataLayerDataContext(Config.GetConnectionString()))
                {
                    CspErrorLog errorLog = new CspErrorLog
                                               {
                                                   userId = userInfos.First().UserID,
                                                   date = DateTime.Now,
                                                   error = ex.Message,
                                                   extra = ex.StackTrace
                                               };
                    ct.CspErrorLogs.InsertOnSubmit(errorLog);
                    ct.SubmitChanges(ConflictMode.ContinueOnConflict);
                }
            }
        }

        private void UpdateCSPCompanyInPortal(CompanySync InCompany, int PortalID)
        {
            using (
                CspDataLayerDataContext _context =
                    new CspDataLayerDataContext(Config.GetConnectionString("CspPortal" + PortalID.ToString())))
            {
                Company _company;
                /*
                _company =
                    _context.Companies.Single<Company>(c => c.companies_Id == GetIntegrationKeyFromPortal(PortalID));*/
                _company =
                    _context.Companies.FirstOrDefault(c => c.companies_Id == GetIntegrationKeyFromPortal(PortalID));
                if (_company != null)
                {
                    bool isChange = false;
                    if (!InCompany._companyname.Equals(_company.companyname))
                    {
                        _company.companyname = InCompany._companyname;
                        if (!isChange) isChange = true;
                    }

                    if (!InCompany._address1.Equals(_company.address1))
                    {
                        _company.address1 = InCompany._address1;
                        if (!isChange) isChange = true;
                    }
                    if (!InCompany._address2.Equals(_company.address2))
                    {
                        _company.address2 = InCompany._address2;
                        if (!isChange) isChange = true;
                    }
                    if (!InCompany._city.Equals(_company.city))
                    {
                        _company.city = InCompany._city;
                        if (!isChange) isChange = true;
                    }
                    if (!InCompany._state.Equals(_company.state))
                    {
                        _company.state = InCompany._state;
                        if (!isChange) isChange = true;
                    }
                    if (!InCompany._zipcode.Equals(_company.zipcode))
                    {
                        _company.zipcode = InCompany._zipcode;
                        if (!isChange) isChange = true;
                    }
                    if (!InCompany._country.Equals(_company.country))
                    {
                        _company.country = InCompany._country;
                        if (!isChange) isChange = true;
                    }

                    if (!InCompany._websiteurl.Equals(_company.website_url))
                    {
                        _company.website_url = InCompany._websiteurl;
                        if (!isChange) isChange = true;
                    }

                    CompaniesConsumer cc = _company.CompaniesConsumers.FirstOrDefault();
                    if (cc != null)
                    {
                        if (InCompany._defaultLanguage != cc.default_language_Id)
                        {
                            cc.default_language_Id = InCompany._defaultLanguage;
                            if (!isChange) isChange = true;
                        }
                    }


                    if (isChange) _context.SubmitChanges(ConflictMode.FailOnFirstConflict);
                    isChange = false;

                    UserInfo _curUser = GetUserInfoByPortalId(PortalID);
                    if (!InCompany._contactUsEmail.Equals(_curUser.Profile.GetPropertyValue(Globals.ContactUsEmail)))
                    {
                        _curUser.Profile.SetProfileProperty(Globals.ContactUsEmail, InCompany._contactUsEmail);
                        if (!isChange) isChange = true;
                    }
                    if (!InCompany._dataSourceId.Equals(_curUser.Profile.GetPropertyValue(Globals.DataSourceId)))
                    {
                        _curUser.Profile.SetProfileProperty(Globals.DataSourceId, InCompany._dataSourceId);
                        if (!isChange) isChange = true;
                    }
                    if (!InCompany._phone.Equals(_curUser.Profile.Telephone))
                    {
                        _curUser.Profile.Telephone = InCompany._phone;
                        if (!isChange) isChange = true;
                    }

                    if (isChange) UserController.UpdateUser(PortalID, _curUser);
                }
            }
        }

        public void UpdateCSPCompanyInAllPortals(CompanySync InCompany)
        {
            /*
             * the local user infos should all be the one user
             * loop through them, and get the right user
             * Connect to the underlying csp DB for PortalB, grab all the relevant information and stick it into portalA's database
             */
            foreach (UserInfo userInfo in userInfos)
            {
                if (userInfo != null)
                {
                    if (userInfo.IsDeleted == false && GetIntegrationKey(userInfo) != -1)
                    {
                        UpdateCSPCompanyInPortal(InCompany, userInfo.PortalID);
                    }
                }
            }
        }
    }
}
