using System;
using System.Collections.Generic;
using System.Text;

namespace DesktopModules.CSPModules.CspCommons.Components
{
    public class PartnerReport
    {
        private List<PartnerReportRecord> _partnerReportRecord;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public PartnerReport()
        {
            _partnerReportRecord = new List<PartnerReportRecord>();
        }

        public void Add(PartnerReportRecord record)
        {
            _partnerReportRecord.Add(record);
        }

        public string ToHtml()
        {
            StringBuilder sb = new StringBuilder();
            foreach (PartnerReportRecord list in _partnerReportRecord)
            {
                sb.AppendLine(list.ToHtml());
            }
            return sb.ToString();
        }
        public string ToCsv()
        {
            StringBuilder sb = new StringBuilder();
            bool first = false;
            foreach (PartnerReportRecord list in _partnerReportRecord)
            {
                if (!first)
                {
                    first = true;
                    sb.AppendLine(list.GetCsvHeader());
                }
                sb.AppendLine(list.ToCsv());
            }
            return sb.ToString();
        }

        public string ToJson()
        {
            StringBuilder sb = new StringBuilder();
            if (_partnerReportRecord.Count == 0) return "{\"items\": []}";
            sb.Append("{\"items\": [");
            foreach (PartnerReportRecord list in _partnerReportRecord)
            {
                sb.Append(list.ToJson() + ",");
            }
            return sb.Remove(sb.Length - 1, 1).Append("]}").ToString();
            
        }
    }
}