﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DesktopModules.CSPModules.CspCommons.Components
{
    public class PartnerReportChartData
    {
        public List<PartnerReportChartDataRow> Rows;
        public readonly List<string> ColumnNames;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public PartnerReportChartData()
        {
            Rows = new List<PartnerReportChartDataRow>();
            ColumnNames = new List<string>();
        }
        public void AddColumn(string columnName)
        {
            ColumnNames.Add(columnName);
        }

        public void Add(PartnerReportChartDataRow row)
        {
            Rows.Add(row);
        }
    }

    public class PartnerReportChartDataRow
    {
        
        public List<int> ColumnData;
        public string CampaignName
        {
            get; set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public PartnerReportChartDataRow()
        {
            ColumnData = new List<int>();
            CampaignName = string.Empty;
        }

        public void AddData(object data)
        {
            int i = Convert.ToInt32(data);
            ColumnData.Add(i);
        }
    }
}