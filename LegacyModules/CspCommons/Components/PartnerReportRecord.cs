using System;
using System.Collections.Generic;
using System.Text;

namespace DesktopModules.CSPModules.CspCommons.Components
{
    public class PartnerReportRecord
    {
        Dictionary<string, object> _dict;

        public PartnerReportRecord()
        {
            _dict = new Dictionary<string, object>();   
        }

        public string ToHtml()
        {
            StringBuilder sb = new StringBuilder();
            return sb.ToString();
        }
        public string GetCsvHeader()
        {
            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<string, object> pair in _dict)
            {
                if (pair.Key == "Company Id") continue;

                if (sb.Length > 0) sb.Append(",");
                sb.Append("\"" + pair.Key + "\"");
            }
            return sb.ToString();
        }

        public string ToCsv()
        {
            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<string, object> pair in _dict)
            {
                if (pair.Key == "Company Id") continue;

                if (sb.Length > 0) sb.Append(",");
                if (pair.Value is DateTime)
                    sb.Append("\"" + ((DateTime)pair.Value).ToShortDateString() + "\"");
                else 
                    sb.Append("\"" + pair.Value + "\"");
            }
            return sb.ToString();
        }

        public string ToJson()
        {
            string json = "";
            int i = 0;
            foreach (KeyValuePair<string, object> pair in _dict)
            {
                if (pair.Key == "Company Id") continue;

                if (!string.IsNullOrEmpty(json)) json += ",";
                if (pair.Value is DateTime)
                    json += "\"" + pair.Key + "\":\"" + ((DateTime)pair.Value).ToShortDateString() + "\"";
                else
                    json += "\"" + pair.Key + "\":\"" + pair.Value + "\"";
                i++;
            }
            return "{" + json + "}";
        }

        public void Add(string name, int value)
        {
            _dict.Add(name, value);
        }

        public void Add(string name, object value)
        {
            _dict.Add(name, value);
        }
    }
}