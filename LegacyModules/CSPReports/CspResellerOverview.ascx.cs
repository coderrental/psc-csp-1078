﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DesktopModules.CSPModules.CspCommons;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;

namespace DesktopModules.CSPModules.CSPReports
{
    public partial class CspResellerOverview : PortalModuleBase
    {
        private const string baseUri = "https://ws.webtrends.com/v2/ReportService/profiles/jo0RvZckdk6/reports/Hh9i4C5Jvk6/?totals=all&period=current_month&format=xml";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Page.ClientScript.RegisterClientScriptInclude("jquery",
                                              "http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js");
                Page.ClientScript.RegisterClientScriptInclude("google_api",
                                              "http://www.google.com/jsapi");

                var webClient = (HttpWebRequest)WebRequest.Create(baseUri);
                webClient.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                webClient.Credentials = new NetworkCredential(@"TIE_Kinetix\lamtu", "#9D347CE$aa");
                var response = webClient.GetResponse();
                var responseStream = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                string result = responseStream.ReadToEnd();
                ResellerOverviewReport reports = new ResellerOverviewReport(result);
                using (CspDataLayerDataContext context = new CspDataLayerDataContext(Config.GetConnectionString("CspPortal" + this.PortalId.ToString()))) //EGO Multiple Portals
                {
                    var companies = from c in context.Companies select c;
                    reports.BuildReport(companies);
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "GoogleApi", reports.CreateJavascript("CspResellerOverview"), true);
                }
            }
        }
    }
    
}

