﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CspReportController.ascx.cs"
    Inherits="DesktopModules.CSPModules.CSPReports.CspReportController" %>
<table cellpadding="0" cellspacing="0" border="0" id="CspReportTable">
    <tr>
        <td>
            <h1>
        Overview</h1>
            <div id="OverviewReport" runat="server">
            </div>
        </td>
        <td align="left" valign="top">
            <h1>
                Other Reports</h1>
            <ul>
                <li><a href="<%=EditUrl("CspResellerOverview") %>">Reseller Overview Report</a></li>
                <li>Top 15 Report</li>
            </ul>
        </td>
    </tr>
</table>
