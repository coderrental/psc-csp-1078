﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DesktopModules.CSPModules.CspCommons.Components;
using DotNetNuke.Common.Utilities;

namespace TIEKinetix.CSPModules.CspResellerList
{
    public partial class ShowPartnerReport : System.Web.UI.Page
    {
        protected string GridLayout = "";
        protected PartnerReport PartnerReport;
        protected PartnerReportChartData ChartData;

        protected void Page_Init(object sender, EventArgs e)
        {
            PartnerReport = new PartnerReport();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            int portalId = -1, consumerId = -1;
            if (!Request.IsAuthenticated || Page.IsPostBack)
                EndRequest();
            if (Request.QueryString["pid"] == null || !(Request.QueryString["pid"] != null && int.TryParse(Request.QueryString["pid"], out portalId)))
                EndRequest();
            if (Request.QueryString["cid"] == null || !(Request.QueryString["cid"] != null && int.TryParse(Request.QueryString["cid"], out consumerId)))
                EndRequest();

            if (portalId > 0 && consumerId > 0)
            {
                Control docType = this.Page.FindControl("skinDocType");
                if (docType != null)
                    ((Literal)docType).Text = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";

                string cs = Config.GetConnectionString("CspPortal" + portalId);
                using (SqlConnection connection = new SqlConnection(cs))
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted);
                    GetGridData(connection, transaction, consumerId);
                    GetChartData(connection, transaction, consumerId);
                    transaction.Commit();
                }
            }
        }

        private  void EndRequest()
        {
            Response.Write("Invalid Request.");
            Response.End();
        }

        private void GetGridData(SqlConnection connection, SqlTransaction transaction, int id)
        {
            using (SqlCommand cmd = new SqlCommand(DesktopModules.CSPModules.CspCommons.Globals.GenerateReportQuery, connection, transaction))
            {
                cmd.Parameters.Clear();
                cmd.ResetCommandTimeout();
                cmd.Parameters.AddWithValue("@cid", id);
                try
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        PartnerReport = new PartnerReport();
                        if (reader != null)
                        {
                            DataTable dt = reader.GetSchemaTable();
                            GridLayout = "";
                            foreach (DataRow row in dt.Rows)
                            {
                                if (row["ColumnName"].Equals("Company Id")) continue;

                                GridLayout += ",{" + string.Format("field: \"{0}\", name: \"{0}\"", row["ColumnName"]) + "}";
                            }
                            if (!string.IsNullOrEmpty(GridLayout))
                                GridLayout = GridLayout.Remove(0, 1);

                            while (reader.Read())
                            {
                                PartnerReportRecord record = new PartnerReportRecord();
                                foreach (DataRow row in dt.Rows)
                                {
                                    record.Add(row["ColumnName"].ToString(), reader[row["ColumnName"].ToString()]);
                                }
                                PartnerReport.Add(record);
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    Log(ex.Message);
                    Log(ex.StackTrace);
                }
            }
        }

        private void GetChartData(SqlConnection connection, SqlTransaction transaction, int id)
        {
            using (SqlCommand cmd = new SqlCommand(DesktopModules.CSPModules.CspCommons.Globals.GetReportDataQuery, connection, transaction))
            {
                cmd.Parameters.Clear();
                cmd.ResetCommandTimeout();
                cmd.Parameters.AddWithValue("@cid", id);
                try
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        ChartData = new PartnerReportChartData();
                        if (reader != null)
                        {
                            DataTable dt = reader.GetSchemaTable();
                            foreach (DataRow row in dt.Rows)
                            {
                                if (row["ColumnName"].Equals("CampaignName")) continue;
                                ChartData.AddColumn(row["ColumnName"].ToString());
                            }

                            while (reader.Read())
                            {
                                PartnerReportChartDataRow chartDataRow = new PartnerReportChartDataRow();
                                foreach (DataRow row in dt.Rows)
                                {
                                    if (row["ColumnName"].Equals("CampaignName"))
                                    {
                                        chartDataRow.CampaignName = reader[row["ColumnName"].ToString()].ToString();
                                        continue;
                                    }
                                    if (reader[row["ColumnName"].ToString()] != DBNull.Value)
                                        chartDataRow.AddData(reader[row["ColumnName"].ToString()]);
                                    else
                                        chartDataRow.AddData(0);
                                }
                                ChartData.Add(chartDataRow);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log(ex.Message);
                    Log(ex.StackTrace);
                }
            }
        }
        protected string GetChartLabels()
        {
            string label = "[";
            try
            {
                for (int i = 0; i < ChartData.ColumnNames.Count; i++)
                {
                    if (i > 0) label += ",";
                    label += "{value: " + (i + 1) + ", text: \"" + ChartData.ColumnNames[i] + "\"}";
                }
            }
            catch (Exception ex)
            {
                Log(ex.Message);
                Log(ex.StackTrace);
            }
            return label + "]";
        }
        protected string GetChartSeries()
        {
            string script = "\n";
            try
            {
                string[] colors = new string[] { "blue", "red", "yellow", "black", "green", "brown", "gold", "darkblue", "darkred", "darkgreen" };
                for (int i = 0; i < ChartData.Rows.Count; i++)
                {
                    script += "chart1.addSeries('" + ChartData.Rows[i].CampaignName + "', [";
                    for (int j = 0; j < ChartData.Rows[i].ColumnData.Count; j++)
                    {
                        if (j > 0) script += ",";
                        script += ChartData.Rows[i].ColumnData[j];
                    }
                    script += "], {plot:\"other\", stroke: {color:\"" + colors[i] + "\"}});\n";
                }
            }
            catch (Exception ex)
            {
                Log(ex.Message);
                Log(ex.StackTrace);
            }
            return script;
        }

        protected string GetReportData()
        {
            return PartnerReport.ToJson();
        }
        public static void Log(string message)
        {
#if DEBUG
            try
            {
                StreamWriter sw = new StreamWriter("C:\\Tmp\\DnnLog.txt", true);
                sw.WriteLine(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss") + ": " + message);
                sw.Close();
            }
            catch
            {
            }
#endif
        }

    }
}
