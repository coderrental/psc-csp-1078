<%@ Control Language="C#" ClassName="Settings" Inherits="DotNetNuke.Entities.Modules.ModuleSettingsBase" %>
<%@ Import Namespace="TIEKinetix.CSPModules.CspResellerList"%>
<%@ Import Namespace="DotNetNuke.Entities.Modules"%>

<script runat="server">
   
    #region Overrides of ModuleSettingsBase

    public override void LoadSettings()
    {
        if (!IsPostBack)
        {
            if (TabModuleSettings[Commons.FILTER_DOMAIN_NAME] != null)
                domainName.Attributes["value"] = TabModuleSettings[Commons.FILTER_DOMAIN_NAME].ToString();
            if (TabModuleSettings[Commons.FILTER_SUPPLIER_ID] != null)
                supplierId.Attributes["value"] = TabModuleSettings[Commons.FILTER_SUPPLIER_ID].ToString();
            if (TabModuleSettings[Commons.EDITABLE_PARAMETERS] != null)
                parametersToEdit.Attributes["value"] = TabModuleSettings[Commons.EDITABLE_PARAMETERS].ToString();
            if (TabModuleSettings[Commons.DEEP_LINK_URL] != null)
                deeplinkUrl.Attributes["value"] = TabModuleSettings[Commons.DEEP_LINK_URL].ToString();
            if (TabModuleSettings[Commons.BING_MAP_KEY] != null)
                bingMapKey.Attributes["value"] = TabModuleSettings[Commons.BING_MAP_KEY].ToString();
            if (TabModuleSettings[Commons.LEAVE_OUT_SUPPLIER] != null)
            {
                try
                {
                    cbSupplier.Checked = bool.Parse(TabModuleSettings[Commons.LEAVE_OUT_SUPPLIER].ToString());
                }
                catch
                {
                    cbSupplier.Checked = false;
                }
            }
            if (TabModuleSettings[Commons.SHOW_SOCIAL] != null)
            {
                try
                {
                    cbShowSocial.Checked = bool.Parse(TabModuleSettings[Commons.SHOW_SOCIAL].ToString());
                }
                catch
                {
                    cbShowSocial.Checked = false;
                }
            }
            if (TabModuleSettings[Commons.CHANNEL_NAME] != null)
                channelName.Attributes["value"] = TabModuleSettings[Commons.CHANNEL_NAME].ToString();

        }
    }

    public override void UpdateSettings()
    {
        ModuleController objModules = new ModuleController();
        objModules.UpdateTabModuleSetting(TabModuleId, Commons.FILTER_DOMAIN_NAME, domainName.Attributes["value"] ?? "");
        objModules.UpdateTabModuleSetting(TabModuleId, Commons.FILTER_SUPPLIER_ID, supplierId.Attributes["value"] ?? "");
        objModules.UpdateTabModuleSetting(TabModuleId, Commons.EDITABLE_PARAMETERS, parametersToEdit.Attributes["value"] ?? "");
        objModules.UpdateTabModuleSetting(TabModuleId, Commons.DEEP_LINK_URL, deeplinkUrl.Attributes["value"] ?? "");
        objModules.UpdateTabModuleSetting(TabModuleId, Commons.BING_MAP_KEY, bingMapKey.Attributes["value"] ?? "");
        objModules.UpdateTabModuleSetting(TabModuleId, Commons.CHANNEL_NAME, channelName.Attributes["value"] ?? "");
        objModules.UpdateTabModuleSetting(TabModuleId, Commons.LEAVE_OUT_SUPPLIER, cbSupplier.Checked.ToString());
        objModules.UpdateTabModuleSetting(TabModuleId, Commons.SHOW_SOCIAL, cbShowSocial.Checked.ToString());
        //refresh cache
        ModuleController.SynchronizeModule(ModuleId);        
    }

    #endregion
</script>

<div>
    <table cellpadding="2" cellspacing="2">
        <tbody>
            <tr>
                <td nowrap="nowrap" style="padding-right: 5px;"><label>Filter Domain Name: </label></td>
                <td><input id="domainName" runat="server" size="100" /></td>
            </tr>
            <tr>
                <td><label>Filter Supplier Id: </label></td>
                <td><input id="supplierId" runat="server" size="100" /></td>
            </tr>
            <tr>
                <td><label>Consumer Parameter Names</label></td>
                <td><input id="parametersToEdit" runat="server" size="100" /></td>
            </tr>
            <tr>
                <td><label>Deeplink Url</label></td>
                <td><input id="deeplinkUrl" runat="server" size="100" /></td>
            </tr>
            <tr>
                <td><label>Bing Map Key</label></td>
                <td><input id="bingMapKey" runat="server" size="100" /></td>
            </tr>
            <tr>
                <td><label>Filter by Channel</label></td>
                <td><input id="channelName" runat="server" size="100" /></td>
            </tr>
            <tr>
                <td><label>Leave out suppliers</label></td>
                <td><asp:CheckBox runat="server" ID="cbSupplier" /></td>
            </tr>
            <tr>
                <td><label>Show Social</label></td>
                <td><asp:CheckBox runat="server" ID="cbShowSocial" /></td>
            </tr>
        </tbody>
    </table>
</div>