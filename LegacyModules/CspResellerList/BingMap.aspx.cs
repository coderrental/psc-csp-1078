using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TIEKinetix.CSPModules.CspResellerList
{
    public partial class BingMap : System.Web.UI.Page
    {
        private int _portalId;
        protected List<Consumer> Consumers;
        protected void Page_Init(object sender, EventArgs e)
        {
            Consumers = new List<Consumer>();

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!int.TryParse(Request["p"], out _portalId) && string.IsNullOrEmpty(Request["cids"]) && !Request.IsAuthenticated) 
                Response.End();
            string[] cIds = Request["cids"].Split(',');
            if (cIds.Length == 0)
                Response.End();

            string ids = "";
            int n;

            //prevent weird injection
            foreach (string cId in cIds)
            {
                if (int.TryParse(cId, out n))
                {
                    if (ids == "") ids = cId;
                    else ids += "," + cId;
                }
            }

            string cs = ConfigurationManager.ConnectionStrings["CspPortal" + _portalId].ConnectionString;
            SqlConnection con = null;
            SqlTransaction transaction = null;
            try
            {
                con = new SqlConnection(cs);
                con.Open();
                transaction = con.BeginTransaction(IsolationLevel.ReadUncommitted);
                SqlCommand com = new SqlCommand(string.Format(Commons.GetCompanyLocationQuery, ids), con, transaction);
                SqlDataReader reader = com.ExecuteReader();
                float lng, lat;
                while (reader.Read())
                {
                    lat = DBNull.Value.Equals(reader[9]) ? 0 : Convert.ToSingle(reader[9]);
                    lng = DBNull.Value.Equals(reader[9]) ? 0 : Convert.ToSingle(reader[9]);
                    if (lat != 0 && lng != 0)
                    {
                        Consumers.Add(new Consumer
                        {
                            Id = DBNull.Value.Equals(reader[0]) ? -1 : reader.GetInt32(0),
                            Name = DBNull.Value.Equals(reader[1]) ? "" : reader.GetString(1),
                            Address1 = DBNull.Value.Equals(reader[2]) ? "" : reader.GetString(2),
                            City = DBNull.Value.Equals(reader[3]) ? "" : reader.GetString(3),
                            State = DBNull.Value.Equals(reader[4]) ? "" : reader.GetString(4),
                            Zipcode = DBNull.Value.Equals(reader[5]) ? "" : reader.GetString(5),
                            Country = DBNull.Value.Equals(reader[6]) ? "" : reader.GetString(6),
                            Website = DBNull.Value.Equals(reader[7]) ? "" : reader.GetString(7),
                            //skip the consumer type (8)
                            Latitude = DBNull.Value.Equals(reader[9]) ? 0 : Convert.ToSingle(reader[9]),
                            Longitude = DBNull.Value.Equals(reader[10]) ? 0 : Convert.ToSingle(reader[10])
                        });
                    }
                }
                reader.Close();
                com.Dispose();
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                if (transaction != null) transaction.Rollback();
            }
            finally
            {
                if (con != null) con.Close();
            }


        }
    }
}
