<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Controller.ascx.cs" Inherits="DesktopModules.CSPModules.CspResellerList.Controller" %>
<%@ Import Namespace="TIEKinetix.CSPModules.CspResellerList"%>
<asp:Panel ID="ResellerListPanel" runat="server" Enabled="false" Visible="false">
    <script type="text/javascript" language="javascript">
       $(document).keydown(function(e) {
           if (e.keyCode == 13) {
               return false;
           }
       });
        dojo.registerModulePath("cspRl","<%=ControlPath %>js/cspRl");
        dojo.require("cspRl.app");
        dojo.require("dojox.grid.EnhancedGrid");
        
        dojo.ready(
            function() {
                var layout = [{
                    defaultCell: {
                        editable: false ,type: dojox.grid.cells._Widget
                    },
                    cells: [
                          <%if (ValidSuperAdminRole()) { %>
                          { field: "Id", name: "Edit", width: "100px", editable: false, formatter: function(value, ri) { 
                             var item = this.grid.getItem(ri);
                              var html = "<a class='cptRLEdit' href='javascript:cspRl.app.cptEdit("+value+")' cid='"+value+"'><img title='Edit Partner Values' src='<%=ControlPath %>images/1317999072_article-edit.png' /></a>"; 
                              <% if (ShowPartnerChart) {%>
                              html += "&nbsp;<a id='showPartnerChart_"+value+"' class='showPartnerChart' onclick='$(\"#showPartnerChart_"+value+"\").colorbox({iframe:true, width:1000, height:600, fastIframe:false});' href='<%=ControlPath %>ShowPartnerReport.aspx?cid="+value+"&pid="+<%=PortalId %>+"'><img src='<%=ControlPath %>images/line-chart.png' /></a>";
                              <%} %>                              
                              <% if (ShowDeepLink) {%>                                
                                if (item.DeeplinkUrl.length > 0 && item.DeeplinkUrl[0] != "")
                                  html = html + "&nbsp;<a class='showDeepLink' href='javascript:cspRl.app.showDeeplink(\""+item.DeeplinkUrl+"\")'><img title='Test Deeplink Url' src='<%=ControlPath %>images/1319206162_news.png' /></a>";
                                //else html = html + "&nbsp;<a class='showDeepLink' href=\"javascript:alert('This Partner has not provided a DeepLinkUrl')\" ><img title='Test Deeplink Url' src='<%=ControlPath %>images/1319206162_news.png' /></a>";
                              <%}%>
                              <% if (ShowCoopPartners) {%>
                              if (item.CoopPartner.length > 0 && item.CoopPartner[0].toLowerCase() == "yes") {
                                //html = html + "&nbsp;<a class='showCoopPartners'><img src='<%=ControlPath %>images/handshake.png' /></a>";
                                if (item.CoopPartnerEmailNotification.length>0&&item.CoopPartnerEmailNotification[0]=="")
                                    html = html + "&nbsp;<a class='showCoopPartnersEmailNotification' title='<%=GetLocalizedText("CoopPartnersEmailNotification.New") %>' href='javascript:cspRl.app.notifyPartner("+value+")'><img src='<%=ControlPath %>images/document_new.png' /></a>";
                                else
                                    html = html + "&nbsp;<a class='showCoopPartnersEmailNotification' title='<%=GetLocalizedText("CoopPartnersEmailNotification.Sent") %>' ><img src='<%=ControlPath %>images/document_ok.png' /></a>";
                              }
                              <%} %>

                              // show social
                              <% if (ShowSocial) {%>
                              if (item.SocialId >= 0) {
                                  html = html + "&nbsp;<a class='showSocial' ><img src='<%=ControlPath %>images/twitter_20x20.png' /></a>";
                                  if (item.ManualApproveSocialMessage == "true") 
                                      html = html + "&nbsp;<a class='manualApproveMessage' ><img src='<%=ControlPath %>images/1401915493_approve.png' width='20px' title='Manually Approve Message' /></a>";
                              }
                              <%} %>
                              return html;
                            }, styles:"text-align: left;"
                          },
                          <%} if (ValidRoles()) {%>
                          { field: "IsTest", name: "Test?", width: "30px",editable: true, cellType: dojox.grid.cells.Bool, styles:"text-align: center;"},
                          <%}%>
                          //{ field: "UserName", name: "UserName", datatype: "text", width: "80px" },
                          { field: "CompanyName", name: "Company Name", datatype: "text", width: "160px", sortDesc: true, 
                              formatter: function(v, ri){
                                  var item = this.grid.getItem(ri);
                                  var html = "", url = item.WebUrl;
                                  if (item.WebUrl) 
                                    html="<a href='"+item.WebUrl+"' target='_blank'>"+v+"</a>";
                                  else
                                    html = v;
                                  return html;
                              }  
                          },
                          { field: "State", name: "State", datatype: "text", width: "70px" },
                          { field: "Country", name: "Country", datatype: "text", width: "75px" },
                          { field: "SubscriptionDate", name: "Date Subscribed", datatype: "date", width: "75px", formatter: function(value) { return value; }, styles:"text-align: right;" }
                          <%if (ShowPageView) { %>
                          ,{ field: "PageView", name: "Impressions", datatype: "number", width: "65px", editable: false, styles:"text-align: right;", formatter: function(v){
                              return dojo.number.format(v);
                            }}
                          <%}%>
                          <%if (ShowMailView) { %>
                          ,{ field: "MailView", name: "Mail Read", datatype: "number", width: "65px", editable: false, styles:"text-align: right;"}
                          <%}%>
                        ]
                }];              
                
                
                var cptLayout = [{
                    defaultCell: {
                        editable: false ,type: dojox.grid.cells._Widget
                    },
                    cells: [
                            { field: "Name", name: "Field Name", datatype: "text", width: "200px" },
                            { field: "Value", name: "Value", datatype: "text", editable: true, width: "200px", widgetClass: dijit.form.TextBox },
                        ]
                	}];
            
            cspRl.app.init(layout, "<%=DotNetNuke.Common.Globals.NavigateURL(TabId) %>");
            cspRl.app.cptLayout = cptLayout;
            cspRl.app.deeplinkUrl = "<%=Settings[Commons.DEEP_LINK_URL]%>";
         });

        (function() {
            var wrapper = dojo.create('div', {
                id: 'loadingOverlay',
                'class': 'pageOverlay',
                innerHTML: '<div class="loadingMessage"><img src="<%=ResolveUrl("~/js/external/dojo.1.6/dijit/themes/claro/images/loadingAnimation.gif")%>" />Loading</div>'
            }, dojo.body());
            var cptDialogHolder = dojo.create('div', {id: 'cptDialogHolder'}, dojo.body());
        })();
    </script>
    
    <div id="appLayout" data-dojo-type="dijit.layout.BorderContainer" data-dojo-props="design: 'headline'">
        <div data-dojo-type="dijit.layout.ContentPane" data-dojo-props="region: 'top'">
            <div data-dojo-type="dijit.Toolbar" id="toolBar">
                <%if (ValidRoles()) {%>
                <button id="rlSave" dojoType="dijit.form.Button" iconClass="dijitEditorIcon dijitEditorIconDelete" showLabel="true" type="button">Remove Test Accounts</button>
                <button id="rlExport" dojoType="dijit.form.Button" iconClass="dijitEditorIcon dijitEditorIconWikiword" showLabel="true" type="button">Export
                    <script type="dojo/connect" data-dojo-event="onClick">
                    window.location = "?request_type=download";
                    </script>
                </button>
                <button id="rlExportAll" dojoType="dijit.form.Button" iconClass="dijitEditorIcon dijitEditorIconWikiword" showLabel="true" type="button">Export With Test Accounts
                    <script type="dojo/connect" data-dojo-event="onClick">
                    window.location = "?request_type=downloadAll";
                    </script>
                </button>
                <%}%>
                <%if (ShowDeepLink) {%>
                <button id="deeplinkEdit" dojoType="dijit.form.Button" iconClass="dijitEditorIcon dijitEditorIconCreateLink" showLabel="true" type="button">Deeplink Syndication Page</button>
                <%}%>
                <%if (ShowMap) {%>
                <button id="showMap" dojoType="dijit.form.Button" iconClass="dijitEditorIcon dijitEditorIconCreateLink" showLabel="true" type="button" value="<%=string.Format("{0}?p={1}",ResolveUrl("BingMap.aspx"),PortalId) %>">Show Map</button>
                <%}%>                
            </div>
        </div>
        <div data-dojo-type="dijit.layout.ContentPane" data-dojo-props="region: 'center'">
            <div style="height:650px">
                <div id="gridContainer"></div>
            </div>	
            <div id="cptDialog" dojoType="dijit.Dialog" title="Edit Partner Values" class="cptDialog">                
                <div class="cptDialogContainer">
                  <div style="text-align:left;">
                    <button id="cptSave" dojoType="dijit.form.Button" iconClass="dijitEditorIcon dijitEditorIconSave" showLabel="true" type="button">Save</button>  
                    <button id="cptCancel" dojoType="dijit.form.Button" iconClass="dijitEditorIcon dijitEditorIconCancel" showLabel="true" type="button" onClick="javascript:dijit.byId('cptDialog').hide();">Cancel</button>  
                  </div>
                  <div id="test"></div>
                </div>
            </div>              
            <div id="notificationDialog" dojoType="dijit.Dialog" title="Notification">
                <div style="padding: 25px;">Loading, please wait...</div>
            </div>
            <div id="updateDeepLinkUrlDialog" dojoType="dijit.Dialog" title="Deep Link Url" execute="javascript:cspRl.app.updateDeeplinkUrl()">
                <table>
                    <tr>
                      <td nowrap="nowrap"><label for="deeplinkUrl">Deep Link Url Example: </label></td>
                      <td><input dojoType="dijit.form.TextBox" type="text" name="deeplinkUrl" id="deeplinkUrl" style="width:400px;" /></td>
                    </tr>
                    <tr>
                      <td colspan="2"><center><button dojoType="dijit.form.Button" type="submit">Update</button></center></td>
                    </tr>
                </table>
            </div>
            <div id="iframeDialog" style="width:700px;height:650px">
                <iframe id="mapiframe" style="width:700px;height:600px;border:1px solid #000;" scrolling="no"></iframe>
            </div>
        </div>
    </div>    
</asp:Panel>
