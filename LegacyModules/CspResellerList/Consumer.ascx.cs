namespace TIEKinetix.CSPModules.CspResellerList
{
    public class Consumer
    {
        public int Id;
        public string Name, Address1, Address2, City, Zipcode, State, Country, Website, LongAddress;
        public float Latitude, Longitude;

        #region Overrides of Object

        public override string ToString()
        {
            return string.Format("{0}, {1}, {2} {3}, {4}", Address1, City, State, Zipcode, Country);
        }

        #endregion
    }
}