﻿dojo.provide("cspRl.app");
cspRl.app = {
    store: null,
    layout: null,
    dataGrid: null,
    postbackUrl: "",
    preloadDelay: 500,
    deeplinkUrl: "",
    parseOnLoad: (function(config) {
        var parseOnLoad = config.parseOnLoad;
        config.parseOnLoad = false;
        return parseOnLoad;
    })(dojo.config),

    init: function(layout, url) {
        this.startLoading();
        dojo.require("cspRl.module");
        this.plugins = {
            filter: {
                itemsName: 'items',
                closeFilterbarButton: false,
                ruleCount: 1
            }
            , pagination: {
                pageSizes: ["50", "100", "250"],
                pageStepper: true,
                defaultRows: 50
            }
        };

        this.layout = layout;
        this.postbackUrl = url;
        this.url = this.postbackUrl;
        dojo.ready(dojo.hitch(this, "startup"));
    },
    startup: function() {
        // wait until UI is complete before taking down the loading overlay
        dojo.connect(this, "initUi", this, "endLoading");

        // build up and initialize the UI
        this.initUi();

        // create the store
        //this.getCompanyInfo();
        //dojo.connect(this, "onResult", this, "endLoading");
    },
    endLoading: function() {
        dojo.fadeOut({
            node: dojo.byId('loadingOverlay'),
            onEnd: function(node) {
                dojo.style(node, 'display', 'none');
            }
        }).play();
    },
    startLoading: function(targetNode) {
        var overlayNode = dojo.byId('loadingOverlay');
        if ("none" == dojo.style(overlayNode, "display")) {
            var coords = dojo.coords(targetNode || dojo.body());
            dojo.marginBox(overlayNode, coords);

            // N.B. this implementation doesn't account for complexities
            // of positioning the overlay when the target node is inside a 
            // position:absolute container
            dojo.style(dojo.byId('loadingOverlay'), {
                display: 'block',
                opacity: 1
            });
        }
    },
    initUi: function() {
        // summary: 
        // 		create and setup the UI with layout and widgets

        dojo.parser.parse(dojo.byId("appLayout"));

        // create a single Lightbox instnace which will get reused
        //this.lightbox = new dojox.image.LightboxNano({});
        //this.dialog = new dijit.Dialog({style: "width: 400px;"});
        this.notificationDialog = dijit.byId("notificationDialog");
        this.cptDialog = dijit.byId("cptDialog");

        //store
        this.store = new dojo.data.ItemFileWriteStore({
            clearOnClose: true,
            urlPreventCache: true,
            //items: response.items,
            url: this.url + "?fetch=companies"
        });
        //grid
        this.dataGrid = new dojox.grid.EnhancedGrid({ id: "grid", plugins: this.plugins, structure: this.layout, store: this.store
            , rowsPerPage: 100
            //, rowHeight: 60
            //,autoHeight: true
        }, dojo.byId("gridContainer"));
        this.dataGrid.startup();
        this.dataGrid.update();



        this.dialog = new dijit.Dialog({
            title: "Map"
        },
            dojo.byId("iframeDialog")
        );
        this.dialog.startup();

        //bind events
        dojo.connect(dijit.byId("rlSave"), "onClick", dojo.hitch(this, "submitUpdates"));
        //dojo.connect(dijit.byId("rlExport"), "onClick", dojo.hitch(this, "exportCsv"));
        //dojo.connect(dijit.byId("rlExportAll"), "onClick", dojo.hitch(this, "exportCsvAll"));
        dojo.connect(dijit.byId("cptSave"), "onClick", dojo.hitch(this, "submitCptUpdates"));
        dojo.connect(dijit.byId("deeplinkEdit"), "onClick", dojo.hitch(this, "editDeepLinkParameter"));
        dojo.connect(dijit.byId("showMap"), "onClick", dojo.hitch(this, "showMap"));
    },
    getCompanyInfo: function() {
        var currentScope = this;
        dojo.xhrPost({
            headers: { "content-type": "application/json; charset=utf-8", "request-type": "GetCmpInfo" },
            //postData: dojo.toJson(modifiedItems),
            handleAs: "json",
            load: function(response) {
                if (response !== null && response.success) {
                    currentScope.store = new dojo.data.ItemFileWriteStore({ data: {
                        identifier: "Id",
                        label: "Id",
                        items: response.items
                    }
                    });
                    currentScope.dataGrid = new dojox.grid.EnhancedGrid({ id: "grid", plugins: currentScope.plugins, structure: currentScope.layout, store: currentScope.store }, dojo.byId("gridContainer"));
                    currentScope.dataGrid.startup();
                }
                else {
                }
                currentScope.endLoading();
            },
            error: function() {
                currentScope.endLoading();
            },
            url: currentScope.postbackUrl
        });
    },
    editDeepLinkParameter: function() {
        var frm = dijit.byId("updateDeepLinkUrlDialog");
        frm.attr("title", "Test");
        dijit.byId("deeplinkUrl").set("value", this.deeplinkUrl);
        frm.show();
    },
    updateDeeplinkUrl: function() {
        this.deeplinkUrl = dijit.byId("deeplinkUrl").get("value");
    },
    showDeeplink: function(url) {
        //if (url[url.length - 1] != '/') url = url + '/';
        window.open(url + "#csp_page:" + this.deeplinkUrl);
    },
    showMap: function() {
        var button = dijit.byId("showMap");
        var n = this.dataGrid.rowCount;
        var p = "";
        for (var i = 0; i < n; i++) {
            try {
                if (p == "") p = this.dataGrid.getItem(i).Id[0];
                else p += "," + this.dataGrid.getItem(i).Id[0];
            } catch (ex) { console.log(ex, i); }
        }
        p = button.value + "&cids=" + p;
        dojo.io.iframe.setSrc(dojo.byId("mapiframe"), p, true);
        this.dialog.show();
    },
    submitCptUpdates: function() {
        var cptGrid = dijit.byId("cptGrid");
        var s = cptGrid.store;
        var currentScope = this;
        s._saveCustom = function(saveCompleteCallback, saveFailedCallback) {
            var modifiedItems = currentScope._getModifiedItems(s);
            dojo.forEach(modifiedItems, function(item, index) {
                for (var key in item) {
                    if (dojo.isArray(item[key]) && item[key].length == 1)
                        item[key] = item[key][0];
                }
                item._S = null;
            });
            //only save if there's data
            if (modifiedItems.length > 0) {
                currentScope.notificationDialog.show();
                dojo.xhrPost({
                    headers: { "content-type": "application/json; charset=utf-8", "request-type": "PostCptDef" },
                    postData: dojo.toJson(modifiedItems),
                    handleAs: "json",
                    load: function(response) {
                        if (response !== null && response.success) {
                            saveCompleteCallback();
                            currentScope.cptDialog.hide();
                            currentScope.UpdateStore("?fetch=companies", currentScope.dataGrid, "grid", true);
                        }
                        else {
                        }
                        currentScope.notificationDialog.hide();
                        //force the refresh
                        //url, dg, id, flag                        
                    },
                    error: function() {
                        saveFailedCallback();
                        currentScope.cptDialog.hide();
                    },
                    url: currentScope.postbackUrl
                });
            }
            else
                currentScope.cptDialog.hide();
        };
        s.save();
    },
    notifyPartner: function(node) {
        var me = this;

        var standby = null;
        standby = this.GetStandBy("grid");
        standby.show();

        dojo.xhrPost({
            headers: { "content-type": "application/json; charset=utf-8", "request-type": "EmailNotification" },
            postData: dojo.toJson(node),
            handleAs: "json",
            url: me.postbackUrl,
            load: function(response) {
                standby.hide();
                me.UpdateStore("?fetch=companies", me.dataGrid, "grid", true);
                if (response !== null && response.success) {
                }
                else {
                    me.cptDialog.attr('content', 'Error');
                }
            },
            error: function(response) {
            }
        });
    },
    cptEdit: function(node) {
        var me = this;
        me.notificationDialog.show();
        dojo.xhrPost({
            headers: { "content-type": "application/json; charset=utf-8", "request-type": "GetCptDef" },
            postData: dojo.toJson(node),
            handleAs: "json",
            url: me.postbackUrl,
            load: function(response) {
                me.notificationDialog.hide();
                if (response !== null && response.success) {
                    me.cptDialog.show();
                    var store = new dojo.data.ItemFileWriteStore({ data: {
                        identifier: "Id",
                        label: "Id",
                        items: response.items
                    }
                    });
                    var cptGrid = dijit.byId("cptGrid");
                    if (cptGrid == null) {
                        cptGrid = new dojox.grid.EnhancedGrid({ id: "cptGrid", plugins: me.plugins, store: store, structure: me.cptLayout, singleClickEdit: true }, dojo.byId("test"));
                        cptGrid.update();
                        cptGrid.startup();
                    }
                    else
                        cptGrid.setStore(store);
                }
                else {
                    me.cptDialog.attr('content', 'Error');
                }
            },
            error: function(response) {
                me.notificationDialog.hide();
                me.cptDialog.attr('content', response);
                me.cptDialog.show();
            }
        });

    },
    exportCsv: function() {
        window.open("?request_type=download");
    },
    exportCsvAll: function() {
        window.open("?request_type=downloadAll");
    },
    submitUpdates: function() {
        var s = this.store;
        var currentScope = this;
        s._saveCustom = function(saveCompleteCallback, saveFailedCallback) {
            var modifiedItems = currentScope._getModifiedItems(s);

            dojo.forEach(modifiedItems, function(item, index) {
                for (var key in item) {
                    if (dojo.isArray(item[key]) && item[key].length == 1)
                        item[key] = item[key][0];
                }
                item._S = null;
            });
            //only save if there's data
            if (modifiedItems.length > 0) {
                currentScope.notificationDialog.show();
                dojo.xhrPost({
                    headers: { "content-type": "application/json; charset=utf-8", "request-type": "post_content" },
                    postData: dojo.toJson(modifiedItems),
                    handleAs: "json",
                    load: function(response) {
                        if (response !== null && response.success) {
                            saveCompleteCallback();
                            //reload data
                            var store = new dojo.data.ItemFileWriteStore({
                                data: {
                                    identifier: "Id",
                                    label: "Id",
                                    items: response.items
                                }
                            });
                            currentScope.store = store;
                            currentScope.dataGrid.setStore(store);
                            currentScope.dataGrid.update();
                        }
                        else {
                        }
                        currentScope.notificationDialog.hide();
                    },
                    error: saveFailedCallback,
                    url: currentScope.postbackUrl
                });
            }
        };
        s.save();
    },
    GetStandBy: function(id) {
        var standby = dijit.byId(id + "_standby");
        if (standby == null) {
            standby = new dojox.widget.Standby({
                id: id + "_standby",
                target: id,
                color: "#ccc",
                zIndex: 999
            });
            document.body.appendChild(standby.domNode);
        }
        return standby;
    },
    UpdateStore: function(url, dg, id, flag) {
        //overlay
        var standby = null;
        if (id) {
            standby = this.GetStandBy(id);
            standby.show();
        }
        var me = this;
        var store = null;
        if (flag) {
            store = new dojo.data.ItemFileWriteStore({ url: me.url + url });
        }
        else
            store = new dojo.data.ItemFileReadStore({ url: me.url + url });
        dg.store.close();
        store.fetch({
            query: {},
            onComplete: function(items, request) {
                dg.setStore(store);
                if (id) standby.hide();
            }
        });
    },
    _getModifiedItems: function(store) {
        var modifiedItems = [];
        if (store !== null && store._pending !== null) {
            if (store._pending._modifiedItems !== null) {
                for (var modifiedItemKey in store._pending._modifiedItems) {
                    if (store._itemsByIdentity) {
                        modifiedItems.push(store._itemsByIdentity[modifiedItemKey]);
                    }
                    else {
                        modifiedItems.push(store._arrayOfAllItems[modifiedItemKey]);
                    }
                }
            }
            if (store._pending._newItems !== null) {
                for (var modifiedItemKey in store._pending._newItems) {
                    if (store._itemsByIdentity) {
                        modifiedItems.push(store._itemsByIdentity[modifiedItemKey]);
                    }
                    else {
                        modifiedItems.push(store._arrayOfAllItems[modifiedItemKey]);
                    }
                }
            }
        }
        return modifiedItems;
    }
};
