﻿dojo.provide("cspRl.module");

// dependencies for the application UI and data layers
dojo.require('dojo.parser');
dojo.require("dijit.layout.BorderContainer");
dojo.require("dijit.layout.TabContainer");
dojo.require("dijit.layout.ContentPane");
dojo.require("dijit.Toolbar");
dojo.require("dijit.form.Button");
dojo.require("dijit.form.CheckBox");
dojo.require("dijit.Dialog");
dojo.require("dojox.image.Lightbox");
//dojo.require("dojox.image.LightboxNano");
//dojo.require("dojox.grid.EnhancedGrid");
dojo.require("dojox.grid.enhanced.plugins.Filter");
dojo.require("dojo.data.ItemFileWriteStore");
//dojo.require("dojox.rpc.Service");
//dojo.require("dojox.data.JsonRestStore");
//dojo.require("dojox.grid.enhanced.plugins.Menu");
dojo.require("dojox.grid.enhanced.plugins.Pagination");
//dojo.require("dojox.grid.enhanced.DataSelection");
dojo.require("dojox.widget.Standby");
dojo.require("dojo.io.iframe");