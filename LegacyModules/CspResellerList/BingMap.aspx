<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BingMap.aspx.cs" Inherits="TIEKinetix.CSPModules.CspResellerList.BingMap" %>
<%@ Import Namespace="TIEKinetix.CSPModules.CspResellerList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="<%=ResolveUrl("module.css") %>" media="screen" />    
    <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"></script>
</head>
<body>
    <form id="formMain" runat="server">
    <div style="height:700px;">
        <div id='mapDiv' style="position:absolute; width:700px; height:600px;background-color:#ccc;">
        </div>
    </div>
    <script type="text/javascript">
        var credentials = "Ao5I1GWI6Gc3ACnfmlUOKqsB9kY_IEfTGgSMMi_EmXr5Q2adK0m2EXJOdluekfr9";
        var map = new Microsoft.Maps.Map(document.getElementById("mapDiv"), { credentials: credentials });
        var locations = [];            
            <% foreach (Consumer consumer in Consumers) { %>
                (function(m){
                    var loc = new Microsoft.Maps.Location(<%=consumer.Latitude %>, <%=consumer.Longitude %>);
                    var pin = new Microsoft.Maps.Pushpin(loc, {
                                    icon: 'images/poi_search.png',
                                    //text: "<%=consumer.Name.Replace("\"","") %>",
                                    visible: true
                                });
                    Microsoft.Maps.Events.addHandler(pin, "mouseover", function(ev) {
                        var p = ev.target;
                        p.setOptions({ icon: 'images/ICN_Bullet_Blue_25x38.gif' });
                        p.infobox.setOptions({ visible: true });
                    });
                    Microsoft.Maps.Events.addHandler(pin, "mouseout", function(ev) {
                        var p = ev.target;
                        p.setOptions({ icon: 'images/poi_search.png' });
                        p.infobox.setOptions({ visible: false });
                    });

                    //add infobox
                    var infobox = new Microsoft.Maps.Infobox(loc, {
                        width: 250, 
                        height: 100, 
                        showCloseButton: false, zIndex: 99, showPointer: false, visible: false, 
                        offset: new Microsoft.Maps.Point(25, 30)
                        ,title: "<%=consumer.Name.Replace("\"","") %>"
                        ,description: "<%=consumer.ToString().Replace("\"","") %>"
                    });
                    pin.infobox = infobox;
                                
                    locations.push(loc);
                    map.entities.push(pin);
                    map.entities.push(infobox);
                })(map);
                if (locations.length>0) {
                    var viewBounds = new Microsoft.Maps.LocationRect.fromLocations(locations);
                    map.setView({ bounds: viewBounds });
                }                
             <%} %>
    </script>    
    </form>
</body>
</html>
