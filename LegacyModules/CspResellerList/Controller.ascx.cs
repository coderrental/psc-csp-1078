﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DesktopModules.CSPModules.CspCommons;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Data;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Profile;
using DotNetNuke.Entities.Users;
using DotNetNuke.Security.Roles;
using DotNetNuke.Services.Localization;
using DotNetNuke.Services.Log.EventLog;
using DotNetNuke.Services.Mail;
using Newtonsoft.Json;
using TIEKinetix.CSPModules.CspResellerList;

namespace DesktopModules.CSPModules.CspResellerList
{
    public partial class Controller : PortalModuleBase
    {
        private const string GetUsernameQuery = @"
select u.Username, up.PropertyValue as CompanyId
from Users u
	join UserProfile up on u.UserID = up.UserID
	join ProfilePropertyDefinition ppd on ppd.PropertyDefinitionID = up.PropertyDefinitionID
where ppd.PortalID = @portalId and ppd.PropertyName = 'CspId'";

        protected bool ShowPageView = true, ShowMailView = true, ShowDeepLink = false, ShowMap = false, ShowCoopPartners = false, ShowPartnerChart = false, ShowSocial = false;
        protected string DeepLinkUrl = string.Empty;
        int _filterSupplierId = -1;
        string _filterDomainName = string.Empty, _editableParameterNames;
        List<int> _companyIdWithSocial = new List<int>();

        protected void Page_Init(object sender, EventArgs e)
        {
            
            #region Check Config
            //do we filter by parent company id
            //todo: get this from the settings.
            //do we show page view
            if (!string.IsNullOrEmpty(Util.GetSetting(PortalId, "ShowPageView")) && Util.GetSetting(PortalId, "ShowPageView").Equals("false")) ShowPageView = false;
            //do we show mail view
            if (!string.IsNullOrEmpty(Util.GetSetting(PortalId, "ShowMailView")) && Util.GetSetting(PortalId, "ShowMailView").Equals("false")) ShowMailView = false;
            //field by supplier id
            if (Settings[Commons.FILTER_SUPPLIER_ID] == null || (Settings[Commons.FILTER_SUPPLIER_ID] != null && !int.TryParse(Settings[Commons.FILTER_SUPPLIER_ID].ToString(), out _filterSupplierId)))
                _filterSupplierId = -1;
            //field by domain name?
            _filterDomainName = Settings[Commons.FILTER_DOMAIN_NAME] != null ? Settings[Commons.FILTER_DOMAIN_NAME].ToString() : "";
            //editable parameters only for CspAdmin
            _editableParameterNames = Settings[Commons.EDITABLE_PARAMETERS] != null ? Settings[Commons.EDITABLE_PARAMETERS].ToString() : "";
            //show deep link
            if (!string.IsNullOrEmpty(Util.GetSetting(PortalId, "ShowDeepLink")) && Util.GetSetting(PortalId, "ShowDeepLink").Equals("true")) ShowDeepLink = true;
            if (!string.IsNullOrEmpty(Util.GetSetting(PortalId, "ShowCoopPartners")) && Util.GetSetting(PortalId, "ShowCoopPartners").Equals("true")) ShowCoopPartners = true;
            if (!string.IsNullOrEmpty(Util.GetSetting(PortalId, "ShowPartnerChart")) && Util.GetSetting(PortalId, "ShowPartnerChart").Equals("true")) ShowPartnerChart = true;
            //ltu 5/8/14
            if (Settings.ContainsKey(Commons.SHOW_SOCIAL) && Settings[Commons.SHOW_SOCIAL] != null)
            {
                if (!bool.TryParse(Settings[Commons.SHOW_SOCIAL].ToString(), out ShowSocial))
                    ShowSocial = false;
            }

            if (ShowDeepLink)
                DeepLinkUrl = Settings[Commons.DEEP_LINK_URL] != null ? Settings[Commons.DEEP_LINK_URL].ToString() : "";
            //show map
            if (!string.IsNullOrEmpty(Util.GetSetting(PortalId, "ShowMap")) && Util.GetSetting(PortalId, "ShowMap").Equals("true")) ShowMap = true;
            #endregion

            //module title
            ModuleContext.Configuration.ModuleTitle = Localization.GetString("ModuleTitle", LocalResourceFile);

            // load the social stuff
            #region [ltu 5/7/14 added social to the resller list]

            using (var reader = DotNetNuke.Data.DataProvider.Instance().ExecuteSQL("select distinct p.CspId FROM CSPTSMM_Partners p where p.IsSubscribed=1 and p.PortalId="+PortalId) as SqlDataReader)
            {
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        _companyIdWithSocial.Add(reader.GetInt32(0));
                    }
                }
            }
            #endregion

        }
        protected override void OnLoad(EventArgs e)
        {
            if (!Page.IsPostBack && !"POST".Equals(Request.HttpMethod, StringComparison.OrdinalIgnoreCase))
            {
                //set doctype to prevent quirk mode
                //<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                Control docType = this.Page.FindControl("skinDocType");
                if (docType != null)
                    ((Literal) docType).Text = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
                //"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";

                // if the request_type = download, then we want to trigger the download instead
                string requestType = Request.QueryString["request_type"];
                if (requestType == null) requestType = "";
                if (requestType == "download" && Request.IsAuthenticated && ValidRoles())
                {
                    // post downloaded csv content
                    Response.ContentType = "application/vnd.csv; charset=utf-8";
                    Response.AddHeader("content-disposition", "attachment;filename=CspContentExport.csv");
                    Response.ContentEncoding = Encoding.UTF8;
                    //foreach (byte b in Encoding.UTF8.GetPreamble())
                    //{
                    //    Response.Write((char) b);
                    //}
                    Response.Write('\uFEFF');
                    Response.Write(GetDownloadedContent(false));
                    Response.End();
                }
                else if (requestType == "downloadAll" && Request.IsAuthenticated && ValidRoles())
                {
                    // post downloaded csv content
                    Response.ContentType = "application/vnd.csv; charset=utf-8";
                    Response.AddHeader("content-disposition", "attachment;filename=CspContentExport.csv");
                    Response.ContentEncoding = Encoding.UTF8;
                    Response.Write('\uFEFF');
                    Response.Write(GetDownloadedContent(true));
                    Response.End();
                }
                else
                {
                    requestType = Request.QueryString.Get("fetch");
                    if (string.IsNullOrEmpty(requestType))
                    {
                        #region Load JS & CSS

                        LoadCss(ResolveUrl("~/js/external/dojo.1.6/dijit/themes/claro/claro.css"));
                        LoadCss(ResolveUrl("~/js/external/dojo.1.6/dojox/grid/enhanced/resources/claro/EnhancedGrid.css"));
                        Page.ClientScript.RegisterClientScriptInclude("dojo.1.6", ResolveUrl("~/js/external/dojo.1.6/dojo/dojo.js.uncompressed.js"));
                        var body = (HtmlGenericControl) Page.FindControl("body");
                        if (body != null)
                            body.Attributes.Add("class", "claro");
                        ResellerListPanel.Enabled = ResellerListPanel.Visible = true;

                        #endregion
                    }
                    else if (requestType == "companies" && Request.IsAuthenticated && ValidRoles())
                    {
                        Response.Write("{items: " + GetCompanies() + "}");
                        Response.End();
                    }
                }
            }
            else if (!Page.IsPostBack && "POST".Equals(Request.HttpMethod, StringComparison.OrdinalIgnoreCase))
            {
                string requestType = Request.Headers["request-type"];
                if (string.IsNullOrEmpty(requestType)) requestType = string.Empty;
                CspDataLayerDataContext db = new CspDataLayerDataContext(ConnectionString);

                try
                {
                    // post the data back to the client
                    if (!(ValidRoles("Administrators,CspSuperAdmin,CspAdmin")) && Request.IsAuthenticated)
                        throw new Exception("Authenticated Request");


                    StreamReader reader = new StreamReader(Request.InputStream);
                    string content = reader.ReadToEnd();
                    reader.Close();

                    content = HttpUtility.UrlDecode(content);

                    if (requestType == "GetCptDef")
                    {
                        #region Get Parameter Definitions
                        int companyId = -1;
                        if (int.TryParse(content, out companyId))
                        {
                            CompaniesParameterType[] cpt = db.CompaniesParameterTypes.Where(a=>a.parametertype == 1).OrderBy(a=>a.sortorder).ToArray();
                            CompaniesParameter[] parameters = db.CompaniesParameters.Where(a => a.companies_Id == companyId).ToArray();
                            List<CustomConsumerParameter> companyParameters = new List<CustomConsumerParameter>();
                            CompaniesParameter tmp;

                            // filter by editable parameter array only if the user is in CspAdmin. if users are admin, then they see all
                            List<string> editableParameters = new List<string>();
                            if (ValidCspAdmin() && !string.IsNullOrEmpty(_editableParameterNames))
                            {
                                editableParameters.AddRange(_editableParameterNames.Split(','));
                            }
                            foreach (CompaniesParameterType parameterType in cpt)
                            {
                                if (ValidCspAdmin() && editableParameters.Count > 0 && editableParameters.Count(a => a.Equals(parameterType.parametername, StringComparison.OrdinalIgnoreCase)) == 0)
                                    continue;
                                
                                tmp = parameters.FirstOrDefault(a => a.companies_parameter_types_Id == parameterType.companies_parameter_types_Id);
                                companyParameters.Add(new CustomConsumerParameter
                                                          {
                                                              Id = parameterType.companies_parameter_types_Id,
                                                              CId = companyId,
                                                              Name = GetLocalizeString(parameterType.parametername, LocalResourceFile),
                                                              Value = tmp != null ? tmp.value_text : ""
                                                          });
                            }

                            Response.Write("{success: true, items: " + JsonConvert.SerializeObject(companyParameters) + "}");
                        }
                        #endregion
                    }
                    else if (requestType == "PostCptDef")
                    {
                        #region Update Dirty Company Content
                        if (Request.IsAuthenticated && (ValidRoles()))
                        {
                            CustomConsumerParameter[] parameters = (CustomConsumerParameter[]) JsonConvert.DeserializeObject(content, typeof (CustomConsumerParameter[]));
                            if (parameters.Length > 0)
                            {
                                //company id should be the same for all
                                Company company = db.Companies.Where(a => a.companies_Id == parameters[0].CId).FirstOrDefault();
                                if (company != null)
                                {
                                    bool enableCoopProgram = false;
                                                                             
                                    bool.TryParse(Util.GetSetting(PortalId, "EnableCoopProgram"), out enableCoopProgram);
                                    Log("EnableCoopProgram: " + enableCoopProgram);
                                    foreach (CustomConsumerParameter parameter in parameters)
                                    {
                                        if (parameter.CId != company.companies_Id)
                                            throw new Exception("Invalid consumer info.");
                                        if (company.CompaniesParameters.Count(a=>a.companies_parameter_types_Id == parameter.Id) == 0)
                                        {
                                            CompaniesParameter cp = new CompaniesParameter
                                                                        {
                                                                            companies_Id = company.companies_Id,
                                                                            companies_parameter_types_Id = parameter.Id,
                                                                            value_text = parameter.Value.Replace("\"", "")
                                                                        };
                                            //Log("CSP_Coop_Partner: " + (cp.CompaniesParameterType == null ? "NULL: " + parameter.Id + ":" + parameter.Name : cp.CompaniesParameterType.parametername));
                                            if (enableCoopProgram && parameter.Name.Equals("CSP_Coop_Partner"))
                                            {
                                                
                                                AssignRoleToUser("CSP_Coop_Partner",parameter.Value.Replace("\"", ""), PortalSettings, company.companies_Id);
                                            }
                                            db.CompaniesParameters.InsertOnSubmit(cp);
                                        }
                                        else
                                        {
                                            CompaniesParameter cp = company.CompaniesParameters.FirstOrDefault(a => a.companies_parameter_types_Id == parameter.Id);
                                            if (cp != null && cp.value_text != parameter.Value)
                                            {
                                                Log("Param Name: " + cp.CompaniesParameterType.parametername + " = " + cp.value_text);
                                                if (enableCoopProgram && cp.CompaniesParameterType.parametername.Equals("CSP_Coop_Partner"))
                                                {
                                                    AssignRoleToUser("CSP_Coop_Partner", parameter.Value.Replace("\"", ""), PortalSettings, company.companies_Id);
                                                }
                                                cp.value_text = parameter.Value.Replace("\"", "");
                                            }
                                        }
                                    }
                                    db.SubmitChanges(ConflictMode.FailOnFirstConflict);
                                    Response.Write("{success:true}");
                                }
                                else
                                    throw new Exception("Unable to find requested consumer.");
                            }
                            else
                                Response.Write("{success:true}"); //nothing to save

                            
                        }
                        else
                        {
                            Response.Write("{success:false, error: 'Not Authenticated.'}");
                        }
                        
                        #endregion
                    }
                    else if (requestType == "GetCmpInfo")
                    {
                        Response.Write("{success: true, items: " + GetCompanies() + "}");
                    }
                    else if (requestType == "EmailNotification")
                    {
                        #region Update Email Notification

                        int companyId = -1;
                        if (int.TryParse(content, out companyId))
                        {
                            Company company = db.Companies.FirstOrDefault(a => a.companies_Id == companyId);
                            if (company != null)
                            {
                                string emailContent = "";
                                if (company.CompaniesConsumers.Count() > 0)
                                    emailContent = GetLocalizedText("CoopPartnerEmailNotification.Content-"+company.CompaniesConsumers.FirstOrDefault().default_language_Id);
                                if (string.IsNullOrEmpty(emailContent))
                                    emailContent = GetLocalizedText("CoopPartnerEmailNotification.Content"); //fallback

                                string emailFrom = GetLocalizedText("CoopPartnerEmailNotification.SendFromAddress");
                                if (string.IsNullOrEmpty(emailFrom))
                                    emailFrom = Util.GetSetting(PortalId, "EmailFromAddressForRegistration");
                                string emailTo = "";

                                try
                                {
                                    UserInfo userInfo = GetUserId(PortalId, companyId);
                                    if (userInfo != null)
                                        emailTo = userInfo.Email;

                                    if (!string.IsNullOrEmpty(emailContent) && !string.IsNullOrEmpty(emailFrom) && !string.IsNullOrEmpty(emailTo))
                                    {
                                        Mail.SendMail(
                                            emailFrom,
                                            emailTo,
                                            "",
                                            "lam.tu@tiekinetix.com;" + GetLocalizedText("CoopPartnerEmailNotification.BccAddresses"),
                                            MailPriority.Normal,
                                            GetLocalizedText("CoopPartnerEmailNotification.Subject"),
                                            MailFormat.Html,
                                            Encoding.UTF8,
                                            emailContent,
                                            "", //attachment
                                            Globals.SMTPHost,
                                            Globals.SMTPAuthentication,
                                            Globals.SMTPUsername,
                                            Globals.SMTPPassword);
                                        CompaniesParameterType def = db.CompaniesParameterTypes.FirstOrDefault(cp => cp.parametername == Commons.CspCoopPartnerEmailNotification);
                                        
                                        if (def != null)
                                        {
                                            CompaniesParameter cp = company.CompaniesParameters.FirstOrDefault(a => a.companies_parameter_types_Id == def.companies_parameter_types_Id);
                                            if (cp == null)
                                                db.CompaniesParameters.InsertOnSubmit(new CompaniesParameter
                                                                                    {
                                                                                        companies_Id = company.companies_Id,
                                                                                        companies_parameter_types_Id = def.companies_parameter_types_Id,
                                                                                        value_text = "0"
                                                                                    });
                                            else
                                                cp.value_text = (Convert.ToInt32(cp.value_text) + 1).ToString();
                                            db.SubmitChanges(ConflictMode.ContinueOnConflict);
                                        }
                                    }
                                    Response.Write("{success:true, msg: 'send from " + emailFrom + " to " + emailTo + "'}");
                                }
                                catch(Exception ex)
                                {
                                    Response.Write("{success:false, msg: '" + ex.ToString() + "'}");
                                }
                            }
                            else
                                Response.Write("{success:false}");
                        }
                        #endregion
                    }
                    else
                    {
                        #region Update Dirty Company Content
                        RlCompany[] companies = (RlCompany[]) JsonConvert.DeserializeObject(content, typeof (RlCompany[]));
                        if (companies.Length > 0)
                        {
                            CompaniesParameterType consumerTypeParamDef = db.CompaniesParameterTypes.FirstOrDefault(cp => cp.parametername == Commons.ConsumerType);
                            foreach (RlCompany company in companies)
                            {
                                if (company.IsTest && ValidRoles() && consumerTypeParamDef != null)
                                {
                                    CompaniesParameter cp = db.CompaniesParameters.FirstOrDefault(p => p.companies_parameter_types_Id == consumerTypeParamDef.companies_parameter_types_Id && p.companies_Id == company.Id);
                                    if (cp == null)
                                    {
                                        db.CompaniesParameters.InsertOnSubmit(new CompaniesParameter
                                                                                  {
                                                                                      companies_Id = company.Id,
                                                                                      companies_parameter_types_Id = consumerTypeParamDef.companies_parameter_types_Id,
                                                                                      value_text = "T"
                                                                                  });
                                    }
                                    else if ("t,p".Contains(cp.value_text))
                                    {
                                        cp.value_text = "T";
                                    }
                                }
                            }
                            db.SubmitChanges(ConflictMode.FailOnFirstConflict);
                        }
                        Response.Write("{success: true, length: " + companies.Length + ", items: " + GetCompanies() + "}");
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    Response.Write("{success: false, error: '" + ex.Message.Replace("'","\"") + "'}");
                }
                finally
                {
                    db.Dispose();
                    Response.End();
                }
            }
        }

        private void AssignRoleToUser(string roleName, string value, PortalSettings portalSettings, int companyId)
        {
            Log("AssignRoleToUser. Role Name" + roleName + ", Value = " + (!string.IsNullOrEmpty(value) ? value : "Empty"));
            bool enableRole = false;
            //simple translation
            if (value.Trim().Equals("yes", StringComparison.OrdinalIgnoreCase)) value = "True";
            else value = "False";
            bool.TryParse(value, out enableRole);
            RoleController roleController = new RoleController();
            RoleInfo roleInfo = roleController.GetRoleByName(portalSettings.PortalId, roleName);
            int roleId = -1;
            if (roleInfo == null)
            {
                roleInfo = new RoleInfo
                               {
                                   AutoAssignment = false,
                                   IsPublic = false,
                                   PortalID = portalSettings.PortalId,
                                   RoleGroupID = -1,
                                   Description = "Allow Co-op partners to see co-op tab",
                                   RoleName = roleName
                               };
                roleId = roleController.AddRole(roleInfo);
            }
            else 
                roleId = roleInfo.RoleID;
            Log("AssignRoleToUser - Role Id: " + roleId);

            UserInfo userInfo = GetUserId(portalSettings.PortalId, companyId);
            if (userInfo == null) return;
            Log("enableRole = " + enableRole.ToString() + "/" + value + ". User In Role = " + userInfo.IsInRole(roleName));

            if (userInfo.IsInRole(roleName) && !enableRole)
            {
                DataCache.ClearPortalCache(portalSettings.PortalId, true);
                roleController.UpdateUserRole(portalSettings.PortalId, userInfo.UserID, roleId, true);
            }
            else if (!userInfo.IsInRole(roleName) && enableRole)
            {
                DataCache.ClearCache();
                roleController.UpdateUserRole(portalSettings.PortalId, userInfo.UserID, roleId);
            }
        }

        private UserInfo GetUserId(int portalId, int companyId)
        {
            string query = @"
select b.UserID 
from ProfilePropertyDefinition a 
	join UserProfile b on a.PropertyDefinitionID = b.PropertyDefinitionID 
where a.PortalID=@portalId and a.PropertyName = @p and b.PropertyValue = @value
";
            UserInfo userInfo = null;
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@portalId", portalId));
            parameters.Add(new SqlParameter("@p", Commons.CSP_INTEGRATION_KEY));
            parameters.Add(new SqlParameter("@value", companyId.ToString()));
            using (SqlDataReader reader = (SqlDataReader) DataProvider.Instance().ExecuteSQL(query, parameters.ToArray()))
            {
                if (reader.Read())
                {
                    Log("Value: " + reader[0]);
                    userInfo = UserController.GetUserById(portalId, reader.GetInt32(0));
                }
            }
            return userInfo;
        }

        private static string GetLocalizeString(string parametername, string resourceFile)
        {
            string value = Localization.GetString(parametername, resourceFile);
            return string.IsNullOrEmpty(value) ? parametername : value;
        }

        protected bool ValidRoles()
        {
            return ValidRoles("Administrators,CspSuperAdmin,CspAdmin");
        }
        protected bool ValidSuperAdminRole()
        {
            return ValidRoles("Administrators,CspSuperAdmin");
        }
        protected bool ValidCspAdmin()
        {
            return ValidRoles("CspSuperAdmin,CspAdmin");
        }
        protected bool ValidDnnAdmin()
        {
            return ValidRoles("Administrators");
        }

        protected bool ValidRoles(string r)
        {
            string[] roles = r.Split(',');
            bool f = false;
            foreach (string role in roles)
            {
                f = f || UserInfo.IsInRole(role);
            }
            return f;
        }
        public void Log(string message)
        {
#if DEBUG
            try
            {
                //StreamWriter sw = new StreamWriter("C:\\Tmp\\DnnLog.txt", true);
                //sw.WriteLine(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss") + ": " + message);
                //sw.Close();
                EventLogController logController = new EventLogController();
                logController.AddLog("CSP Reseller List", message, PortalSettings, UserId, EventLogController.EventLogType.ADMIN_ALERT);
            }catch
            {
            }
#endif
        }

        private string GetDownloadedContent(bool downloadAll)
        {
            string query = Globals.DataExportQuery;
            string pivotFields = string.Empty;

            StringBuilder sb = new StringBuilder();
            using (CspDataLayerDataContext context = new CspDataLayerDataContext(ConnectionString))
            {
                #region ltu 11-13-2013 added logic to filter list by channel name

                string channelName = (Settings[Commons.CHANNEL_NAME] != null ? Settings[Commons.CHANNEL_NAME].ToString() : "");
                Channel channel = context.Channels.FirstOrDefault(c => c.Name == channelName);

                #endregion
                using (SqlConnection connection = (SqlConnection) context.Connection)
                {
                    if (connection.State == ConnectionState.Closed) connection.Open();
                    using (SqlCommand command = new SqlCommand(string.Empty, connection))
                    {
                        SqlDataReader reader;

                        // get the list of parameters
                        command.CommandText = "select * from companies_parameter_types order by sortorder";
                        reader = command.ExecuteReader();
                        if (reader != null)
                        {
                            DataTable schema = reader.GetSchemaTable();
                            bool hasExportColumn = false;
                            foreach (DataRow row in schema.Rows)
                            {
                                if (row["ColumnName"].ToString().Equals(Commons.ExportConsumerParameter, StringComparison.CurrentCultureIgnoreCase))
                                {
                                    hasExportColumn = true;
                                    break;
                                }
                            }
                            bool isExport = true;
                            while (reader.Read())
                            {
                                if (hasExportColumn)
                                {
                                    if (reader[Commons.ExportConsumerParameter] != null && reader[Commons.ExportConsumerParameter] != DBNull.Value)
                                        isExport = (bool)reader[Commons.ExportConsumerParameter];
                                }
                                if (isExport)
                                {
                                    if (string.IsNullOrEmpty(pivotFields))
                                        pivotFields = "[" + reader["parametername"] + "]";
                                    else
                                        pivotFields += ",[" + reader["parametername"] + "]";
                                }
                            }
                            reader.Close();
                            reader.Dispose();
                        }
                        if (_filterSupplierId != -1)
                        {
                            command.CommandText = string.Format(Globals.DataExportFilteredBySupplierId, pivotFields);
                            command.Parameters.Clear();
                            command.Parameters.AddWithValue("@supplierId", _filterSupplierId);
                        }
                        else
                            command.CommandText = string.Format(query, pivotFields);
                        Log("Export Query: " + command.CommandText);
                        Log(pivotFields);

                        reader = command.ExecuteReader();
                        if (reader != null)
                        {
                            DataTable schema = reader.GetSchemaTable();
                            string text = string.Empty;
                            foreach (DataRow row in schema.Rows)
                            {
                                if (string.IsNullOrEmpty(text)) text = "\"" + row["ColumnName"] + "\"";
                                else text += ",\"" + row["ColumnName"] + "\"";
                            }
                            // ltu 5/8/14 added social column
                            if (ShowSocial)
                                text += ",\"Social\"";
                            sb.AppendLine(text);
                            int id = -1;
                            while (reader.Read())
                            {
                                text = string.Empty;
                                id = reader.GetInt32(0);
                                // ltu 11-13-2013 Filter by channel name if available
                                if (channel != null && reader[reader.GetOrdinal("Domain")] != DBNull.Value && !reader.GetString(reader.GetOrdinal("Domain")).EndsWith(channel.BaseDomain))
                                {
                                    continue;
                                }
                                foreach (DataRow row in schema.Rows)
                                {
                                    object value = reader[row["ColumnName"].ToString()];
                                    if (value == DBNull.Value || value == null)
                                        value = "";

                                    // ignore test companies
                                    if (!downloadAll && row["ColumnName"].ToString().Equals(Commons.ConsumerType) && value.ToString().Equals("t", StringComparison.CurrentCultureIgnoreCase))
                                    {
                                        text = string.Empty;
                                        break;
                                    }

                                    // domain name filter
                                    if (row["ColumnName"].ToString().Equals("base_domain", StringComparison.CurrentCultureIgnoreCase)
                                        && value.ToString().IndexOf(_filterDomainName) == -1)
                                    {
                                        text = string.Empty;
                                        break;
                                    }

                                    //ltu 6/4/14 only show twitter approve post if there is social
                                    if (row["ColumnName"].ToString().Equals("TwitterApprovePost", StringComparison.CurrentCultureIgnoreCase) && ShowSocial)
                                    {
                                        if (!_companyIdWithSocial.Contains(id))
                                            value = string.Empty;
                                    }

                                    if (string.IsNullOrEmpty(text)) text = "\"" + Util.EscapeSpecialChars(value) + "\"";
                                    else text += ",\"" + Util.EscapeSpecialChars(value) + "\"";
                                }
                                if (!string.IsNullOrEmpty(text))
                                {
                                    if (ShowSocial)
                                    {
                                        if (_companyIdWithSocial.Contains(id))
                                            text += ",\"Yes\"";
                                        else
                                        {
                                            text += ",\"No\"";
                                        }
                                    }
                                    sb.AppendLine(text);
                                }
                            }
                        }
                        if (reader != null)
                        {
                            reader.Close();
                            reader.Dispose();
                        }
                    }
                    if (connection.State == ConnectionState.Open) connection.Close();
                }
            }
            return sb.ToString();
        }

        protected string ConnectionString
        {
            get
            {
                return Config.GetConnectionString("CspPortal" + PortalId);
            }
        }



        private void LoadCss(string name)
        {
            HtmlLink link = new HtmlLink();
            link.Href = ResolveUrl(name);
            link.Attributes.Add("type", "text/css");
            link.Attributes.Add("rel", "stylesheet");
            Page.Header.Controls.Add(link);
        }

        protected string GetCompanies()
        {
            #region Generate Company Info
            string jsonString = string.Empty;
            List<RlCompany> companies = new List<RlCompany>();

            using (CspDataLayerDataContext context = new CspDataLayerDataContext(ConnectionString))
            {
                CompaniesParameterType consumerTypeParamDef = context.CompaniesParameterTypes.FirstOrDefault(cp => cp.parametername == Commons.ConsumerType);
                CompaniesParameterType last60DaysVisitParamDef = context.CompaniesParameterTypes.FirstOrDefault(cp => cp.parametername == Commons.Last60DaysVisits);
                CompaniesParameterType last60DaysVrHitParamDef = context.CompaniesParameterTypes.FirstOrDefault(cp => cp.parametername == Commons.Last60DaysVrHits);
                CompaniesParameterType deepLinkParamDef = context.CompaniesParameterTypes.FirstOrDefault(cp => cp.parametername == Commons.DeeplinkUrl);
                CompaniesParameterType coopPartnerParamDef = context.CompaniesParameterTypes.FirstOrDefault(cp => cp.parametername == Commons.CspCoopPartner);
                CompaniesParameterType coopPartnerEmailNotificationParamDef = null;

                if (ShowPageView && last60DaysVisitParamDef == null) ShowPageView = false;
                if (ShowMailView && last60DaysVrHitParamDef == null) ShowMailView = false;
                if (ShowCoopPartners)
                    coopPartnerEmailNotificationParamDef = context.CompaniesParameterTypes.FirstOrDefault(cp => cp.parametername == Commons.CspCoopPartnerEmailNotification);

                var tmp = from c in context.Companies
                          where c.CompaniesParameters.Count(cp => cp.companies_parameter_types_Id == consumerTypeParamDef.companies_parameter_types_Id && cp.value_text.StartsWith("T")) == 0
                          select c;
                
                if (Settings[Commons.LEAVE_OUT_SUPPLIER] != null)
                {
                    bool flag = bool.Parse(Settings[Commons.LEAVE_OUT_SUPPLIER].ToString());
                    if (flag)
                        tmp = tmp.Where(a => a.is_supplier.HasValue && a.is_consumer.Value);
                }

                int pv = 0, mv = 0;
                string webUrl = "", deeplinkUrl = "", coopPartnerValue = "", coopPartnerEmailNotificationValue = "";

                DateTimeFormatInfo dateTimeFormatInfo = new DateTimeFormatInfo();
                dateTimeFormatInfo.MonthDayPattern = "m";
                dateTimeFormatInfo.YearMonthPattern = "y";
                Log("here");
                foreach (Company c in tmp)
                {
                    pv = mv = 0;
                    //filter by domain if possible
                    if (!string.IsNullOrEmpty(_filterDomainName) && c.CompaniesConsumers.Count(a=>a.base_domain.IndexOf(_filterDomainName, StringComparison.OrdinalIgnoreCase) >= 0) == 0) continue;
                    //filter by supplier id  if possible
                    if (_filterSupplierId>=0 && ((c.parent_companies_Id.HasValue && c.parent_companies_Id.Value!=_filterSupplierId) || (!c.parent_companies_Id.HasValue))) continue;

					if (last60DaysVisitParamDef != null && ShowPageView && c.CompaniesParameters.Count(a => a.companies_parameter_types_Id == last60DaysVisitParamDef.companies_parameter_types_Id && a.companies_Id == c.companies_Id) > 0)
					{
						try
						{
							pv = Convert.ToInt32(c.CompaniesParameters.FirstOrDefault(a => a.companies_parameter_types_Id == last60DaysVisitParamDef.companies_parameter_types_Id && a.companies_Id == c.companies_Id).value_text);
						}
						catch (Exception)
						{
							pv = 0;
						}
						
					}
					if (last60DaysVrHitParamDef != null && ShowMailView && c.CompaniesParameters.Count(a => a.companies_parameter_types_Id == last60DaysVrHitParamDef.companies_parameter_types_Id && a.companies_Id == c.companies_Id) > 0)
					{
						try
						{
							mv = Convert.ToInt32(c.CompaniesParameters.FirstOrDefault(a => a.companies_parameter_types_Id == last60DaysVrHitParamDef.companies_parameter_types_Id && a.companies_Id == c.companies_Id).value_text);
						}
						catch (Exception)
						{
							mv = 0;
						}
					}

                	//reset deeplinkUrl
                    deeplinkUrl = "";
                    if (ShowDeepLink&&deepLinkParamDef!=null&&c.CompaniesParameters.Count(a => a.companies_parameter_types_Id == deepLinkParamDef.companies_parameter_types_Id && a.companies_Id == c.companies_Id) > 0) {
                        deeplinkUrl = Convert.ToString(c.CompaniesParameters.FirstOrDefault(a => a.companies_parameter_types_Id == deepLinkParamDef.companies_parameter_types_Id && a.companies_Id == c.companies_Id).value_text);
                        if (!string.IsNullOrEmpty(deeplinkUrl )) {
                          deeplinkUrl = deeplinkUrl.StartsWith("http", StringComparison.OrdinalIgnoreCase) ? deeplinkUrl : "http://" + deeplinkUrl;
                        }
                    }
                    webUrl = (!string.IsNullOrEmpty(c.website_url) ? c.website_url : "");

                    coopPartnerValue = "";
                    if (ShowCoopPartners && coopPartnerParamDef!=null&&c.CompaniesParameters.Count(a=>a.companies_parameter_types_Id==coopPartnerParamDef.companies_parameter_types_Id && a.companies_Id == c.companies_Id) > 0)
                        coopPartnerValue = Convert.ToString(c.CompaniesParameters.FirstOrDefault(a => a.companies_parameter_types_Id == coopPartnerParamDef.companies_parameter_types_Id && a.companies_Id == c.companies_Id).value_text);

                    coopPartnerEmailNotificationValue = "";
                    if (ShowCoopPartners && coopPartnerEmailNotificationParamDef != null && c.CompaniesParameters.Count(a => a.companies_parameter_types_Id == coopPartnerEmailNotificationParamDef.companies_parameter_types_Id && a.companies_Id == c.companies_Id) > 0)
                        coopPartnerEmailNotificationValue = Convert.ToString(c.CompaniesParameters.FirstOrDefault(a => a.companies_parameter_types_Id == coopPartnerEmailNotificationParamDef.companies_parameter_types_Id && a.companies_Id == c.companies_Id).value_text);

                    //ltu 6/4/14 add a flag for manual approve post
                    var manualApprovePost = c.CompaniesParameters.FirstOrDefault(a => a.CompaniesParameterType.parametername == "TwitterApprovePost" && a.value_text.ToLower().Equals("true", StringComparison.CurrentCultureIgnoreCase));


                    companies.Add(new RlCompany
                                      {
                                          Id = c.companies_Id,
                                          CompanyName = c.companyname,
                                          Country = !string.IsNullOrEmpty(c.country)?c.country:"",
                                          State = !string.IsNullOrEmpty(c.state) ? c.state : "",
                                          IsTest = false,
                                          SubscriptionDate = c.created.HasValue ? c.created.Value.ToString("yyyy/MM/dd") : "",
                                          MailView = mv,
                                          PageView = pv,
                                          UserName = "",
                                          WebUrl = webUrl.StartsWith("http", StringComparison.OrdinalIgnoreCase) ? webUrl : "http://" + webUrl,
                                          DeeplinkUrl = deeplinkUrl,
                                          CoopPartner = coopPartnerValue,
                                          CoopPartnerEmailNotification = coopPartnerEmailNotificationValue,
                                          SocialId = _companyIdWithSocial.Contains(c.companies_Id)?0:-1,
                                          ManualApproveSocialMessage = manualApprovePost!=null?manualApprovePost.value_text.ToLower():string.Empty
                                      });
                }
            }
            #endregion

            #region Fill out the username
            //var dnn = DotNetNuke.Data.DataProvider.Instance();
            //SqlDataReader reader = null;
            //try
            //{

            //    reader = (SqlDataReader)dnn.ExecuteSQL(GetUsernameQuery, new SqlParameter("@portalId", PortalId));
            //    while (reader.Read())
            //    {
            //        foreach (RlCompany c in companies.Where(a => a.Id == Convert.ToInt32(reader["CompanyId"])))
            //        {
            //            c.UserName = (string) reader["Username"];
            //        } 
            //    }
            //}
            //finally
            //{
            //    if (reader != null) reader.Close();
            //}
            #endregion

            jsonString = JsonConvert.SerializeObject(companies);
            return jsonString;
        }

        protected string GetLocalizedText(string key)
        {
            return Localization.GetString(key, LocalResourceFile);
        }
    }

    #region Classes
    public static class Util
    {
        public static string EscapeSpecialChars(object value)
        {
            return value.ToString().Replace("\"", "'").Replace("\n", "").Replace("\r", "");
        }

        public static string GetSetting(int portalId, string configName)
        {
            return Config.GetSetting("Portal" + portalId + configName);
        }
    }
    public class CustomConsumerParameter
    {
        public int Id { get; set; }
        public int CId { get; set;}
        public string Name { get; set; }
        public string Value { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public CustomConsumerParameter()
        {
            Id = CId = -1;
            Name = Value = "";
        }
    }
    public class RlCompany
    {
        public bool IsTest { get; set; }

        public int Id { get; set; }

        public int PageView { get; set; }

        public int MailView { get; set; }

        public string CompanyName { get; set; }

        public string State { get; set; }

        public string Country { get; set; }

        public string SubscriptionDate { get; set; }

        public string UserName { get; set; }

        public string WebUrl { get; set; }
        
        public string DeeplinkUrl{ get; set; }

        public string CoopPartner { get; set; }

        public string CoopPartnerEmailNotification { get; set; }

        public int SocialId { get; set; }
        public string ManualApproveSocialMessage { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public RlCompany(bool isTest, int id, int pageView, int mailView, string companyName, string state, string country, string subscriptionDate, string userName, string webUrl, int socialId)
        {
            IsTest = isTest;
            Id = id;
            PageView = pageView;
            MailView = mailView;
            CompanyName = companyName;
            State = state;
            Country = country;
            SubscriptionDate = subscriptionDate;
            UserName = userName;
            WebUrl = webUrl;
            SocialId = socialId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public RlCompany()
            : this(false, -1, -1, -1, "", "", "", "", "", "", -1)
        {
        }
    }

    #endregion
}
