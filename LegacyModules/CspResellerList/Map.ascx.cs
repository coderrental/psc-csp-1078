using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;

namespace TIEKinetix.CSPModules.CspResellerList
{
    public partial class Map : PortalModuleBase
    {
        protected List<Consumer> Consumers;
        protected void Page_Init(object sender, EventArgs e)
        {
            Consumers = new List<Consumer>();
            ModuleContext.Configuration.ModuleTitle = Localization.GetString("ModuleTitle", Commons.ResxFile(LocalResourceFile));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Control ctrl = this.ContainerControl.FindControl("dnnTitle");
            //if (ctrl != null)
            //{
            //    string controlTitle = Localization.GetString("ModuleTitle", Commons.ResxFile(LocalResourceFile));
            //    Label lblTitle = (Label)ctrl.FindControl("lblTitle");
            //    if (lblTitle != null)
            //    {
            //        if (!string.IsNullOrEmpty(controlTitle)) 
            //            lblTitle.Text = controlTitle;
            //        else lblTitle.Text = "Missing lblTitle.Text in resx file";
            //    }

            //}

            Page.ClientScript.RegisterClientScriptInclude("BingMapApi v7", "http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0");
            string cIds = Request["cids"];
            if (!string.IsNullOrEmpty(cIds))
            {
                string[] companyIds = cIds.Split(',');
                if (companyIds.Length > 0)
                {
                    string cs = ConfigurationManager.ConnectionStrings["CspPortal" + PortalId].ConnectionString;
                    SqlConnection con = null;
                    SqlTransaction transaction = null;
                    try
                    {
                        con = new SqlConnection(cs);
                        con.Open();
                        transaction = con.BeginTransaction(IsolationLevel.ReadUncommitted);
                        SqlCommand com = new SqlCommand(string.Format(Commons.GetCompanyLocationQuery, cIds), con, transaction);
                        //com.Parameters.AddWithValue("@ids", cIds);
                        SqlDataReader reader = com.ExecuteReader();
                        float lng, lat;
                        while (reader.Read())
                        {
                            lat = DBNull.Value.Equals(reader[9]) ? 0 : Convert.ToSingle(reader[9]);
                            lng = DBNull.Value.Equals(reader[9]) ? 0 : Convert.ToSingle(reader[9]);
                            if (lat != 0 && lng != 0)
                            {
                                Consumers.Add(new Consumer
                                                  {
                                                      Id = DBNull.Value.Equals(reader[0]) ? -1 : reader.GetInt32(0),
                                                      Name = DBNull.Value.Equals(reader[1]) ? "" : reader.GetString(1),
                                                      Address1 = DBNull.Value.Equals(reader[2]) ? "" : reader.GetString(2),
                                                      City = DBNull.Value.Equals(reader[3]) ? "" : reader.GetString(3),
                                                      State = DBNull.Value.Equals(reader[4]) ? "" : reader.GetString(4),
                                                      Zipcode = DBNull.Value.Equals(reader[5]) ? "" : reader.GetString(5),
                                                      Country = DBNull.Value.Equals(reader[6]) ? "" : reader.GetString(6),
                                                      Website = DBNull.Value.Equals(reader[7]) ? "" : reader.GetString(7),
                                                      //skip the consumer type (8)
                                                      Latitude = DBNull.Value.Equals(reader[9]) ? 0 : Convert.ToSingle(reader[9]),
                                                      Longitude = DBNull.Value.Equals(reader[10]) ? 0 : Convert.ToSingle(reader[10])
                                                  });
                            }
                            else
                            {
                                //Response.Write(string.Format("{0}, {1} <br />", lat, lng));
                            }
                        }
                        reader.Close();
                        com.Dispose();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex.Message);
                        if (transaction != null) transaction.Rollback();
                    }
                    finally
                    {
                        if (con != null) con.Close();
                    }
                }
            }
        }
    }
}