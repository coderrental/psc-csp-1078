﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowPartnerReport.aspx.cs" Inherits="TIEKinetix.CSPModules.CspResellerList.ShowPartnerReport" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <link type="text/css" rel="Stylesheet" href="<%=ResolveUrl("~/js/external/dojo.1.6/dijit/themes/claro/claro.css") %>" />
    <link type="text/css" rel="Stylesheet" href="<%=ResolveUrl("~/js/external/dojo.1.6/dojox/grid/enhanced/resources/claro/EnhancedGrid.css") %>" />
    <script type="text/javascript" src="<%=ResolveUrl("~/js/external/dojo.1.6/dojo/dojo.js.uncompressed.js") %>" djConfig="isDebug:true, parseOnLoad: false"></script>
    <% if (ChartData.Rows.Count > 0){%>
    <script type="text/javascript">
        dojo.require("dojox.grid.EnhancedGrid");
        dojo.require("dojox.grid.enhanced.plugins.Filter");
        dojo.require("dojo.data.ItemFileReadStore");
        dojo.require("dojox.charting.Chart");
        dojo.require("dojox.charting.axis2d.Default");
        dojo.require("dojox.charting.plot2d.Lines");
        dojo.require("dojox.charting.widget.Legend");
    </script>
    
    <script type="text/javascript">
        function initUI() {
            // create data store
            var data = <%=GetReportData() %>
            var dataStore = new dojo.data.ItemFileReadStore({
                data: data
            });

        var layout = [{
            defaultCell: { editable: false, type: dojox.grid.cells._Widget },
            cells: [<%=GridLayout %>]
            }];
            
            
            var grid = new dojox.grid.EnhancedGrid({
                id: "grid"
                /*
                , plugins: {
                    filter: {
                        itemsName: 'items',
                        closeFilterbarButton: false,
                        ruleCount: 1
                    }
                }*/
                , structure: layout
                , store: dataStore
                , autoHeight: true
            }, dojo.byId("gridContainer"));
            grid.startup();
            
            // chart
            var chart1 = new dojox.charting.Chart("chart");
            chart1.addPlot("default", { type: "Lines" });
            chart1.addAxis("x", {
                labels: <%=GetChartLabels() %>
            });
            chart1.addAxis("y", { vertical: true });
            <%=GetChartSeries() %>
            chart1.render();
            var c = new dojox.charting.widget.Legend({chart:chart1, horizontal: true, columns: 3 }, "legend");
        }
        dojo.addOnLoad(function() {
            initUI();
        });
    </script>
    <%} %>
 <style type="text/css">
    <!--
#CspShowPartnerReportForm 
{
    font-family: 'Segoe UI',Arial;
    font-size: 12px;
}
.dojoxLegendText 
{
    margin-left: 5px;
}
.dojoxLegendNode
{
    border: 1px solid #000;
    padding:5px;
    margin-top:10px;
}

.csp-chart-container
{
    display:block;
    width:940px;
}    
#legend {width:940px;}
    -->
    </style>
</head>
<body class="claro">
    <div class="csp-chart-container">
    <% if (ChartData.Rows.Count > 0){%>
    <form id="CspShowPartnerReportForm" runat="server">
        <div id="gridContainer">
        </div>
        
        <div id="legend"></div>        
        <div id="chart"></div>
    </form>    
    <%} else
    {%>
     <b>No Report Data Available.</b>
        <%
     }%>
     </div>
</body>
</html>
