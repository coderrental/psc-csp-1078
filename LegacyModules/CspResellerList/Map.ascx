<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Map.ascx.cs" Inherits="TIEKinetix.CSPModules.CspResellerList.Map" %>
<%@ Import Namespace="DotNetNuke.Services.Localization"%>
<%@ Import Namespace="TIEKinetix.CSPModules.CspResellerList"%>
<script type="text/javascript">
    $(document).keydown(function(e) {
        if (e.keyCode == 13) {
            return false;
        }
    });

</script>
<div style="height:700px;">
    <div id='mapDiv' style="position:absolute; width:600px; height:600px;background-color:#ccc;">
    </div>
</div>

<script type="text/javascript">
    var credentials = "Ao5I1GWI6Gc3ACnfmlUOKqsB9kY_IEfTGgSMMi_EmXr5Q2adK0m2EXJOdluekfr9";
    var map = new Microsoft.Maps.Map(document.getElementById("mapDiv"), { credentials: credentials });
    var locations = [];    
    
        <% foreach (Consumer consumer in Consumers) { %>
            (function(m){
                var loc = new Microsoft.Maps.Location(<%=consumer.Latitude %>, <%=consumer.Longitude %>);
                var pin = new Microsoft.Maps.Pushpin(loc, {
                                //icon: 'images/ICN_Bullet_Blue_25x38.gif',
                                //text: "<%=consumer.Name %>",
                                visible: true
                            });   
                locations.push(loc);
                map.entities.push(pin);
            })(map);
            if (locations.length>0) {
                var viewBounds = new Microsoft.Maps.LocationRect.fromLocations(locations);
                map.setView({ bounds: viewBounds });
            }                
         <%} %>
</script>