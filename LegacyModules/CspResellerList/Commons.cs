using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TIEKinetix.CSPModules.CspResellerList
{
    public class Commons
    {
        public static string CHANNEL_NAME = "CHANNEL_NAME";
        public const string LEAVE_OUT_SUPPLIER = "LEAVE_OUT_SUPPLIER";
        public const string FILTER_DOMAIN_NAME = "FILTER_DOMAIN_NAME";
        public const string FILTER_SUPPLIER_ID = "FILTER_SUPPLIER_ID";
        public const string EDITABLE_PARAMETERS = "EDITABLE_PARAMETERS";
        public const string DEEP_LINK_URL = "DEEP_LINK_URL";
        public const string BING_MAP_KEY = "BING_MAP_KEY";
        public const string CSP_INTEGRATION_KEY = "CspId";
        public const string SHOW_SOCIAL = "SHOW_SOCIAL";

        public const string ConsumerType = "ConsumerType";
        public const string DeeplinkUrl = "Deeplink_Landingpage_Url";
        public const string ExportConsumerParameter = "IsExport";
        public const string Last60DaysVisits = "Reporting_WT_last_60_days";
        public const string Last60DaysVrHits = "Reporting_WT_mail_read_last_60_days";
        public const string CspCoopPartner = "CSP_Coop_Partner";
        public const string CspCoopPartnerEmailNotification = "CSP_Coop_Partner_Email_Notification";
        public const string DnnUserName = "Dnn_UserName";
        public const string GetCompanyLocationQuery = @"
select *
from (
select a.companies_Id as Id, a.companyname as Name,a.address1 as Address, a.city as City,a.state as State, a.zipcode as Zipcode, a.country as Country, a.website_url as Website, c.parametername, cast(b.value_text as nvarchar(max)) as v
from companies a
	left join companies_parameters b on a.companies_Id = b.companies_Id
	left join companies_parameter_types c on b.companies_parameter_types_Id = c.companies_parameter_types_Id
	left join companies_consumers d on d.companies_Id = a.companies_Id
)as data
pivot
(
	max(v) FOR parametername in ([ConsumerType], [Latitude], [Longitude])
)as pdata

where 
    ((pdata.ConsumerType is not null and pdata.ConsumerType <> 't') or 
    (pdata.ConsumerType is null)) and (pdata.Id in ({0}))
";
        public static string ResxFile(string fileName)
        {
            int i = fileName.LastIndexOf('/');
            if (i >= 0) i++;
            else i = fileName.Length;
            return fileName.Substring(0, i) + "Controller";
        }
    }
}
