﻿/****** Object:  StoredProcedure [dbo].[GeneratePartnerReportUsingCnetData]    Script Date: 01/12/2012 00:31:20 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GeneratePartnerReportUsingCnetData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GeneratePartnerReportUsingCnetData]
GO

CREATE PROCEDURE [dbo].[GeneratePartnerReportUsingCnetData]
	-- Add the parameters for the stored procedure here
	@cid int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if OBJECT_ID('tempdb..#temp') is not null drop table #temp
	declare @counter int = 0, @dateCol varchar(200), @m int, @y int, @campaign nvarchar(200), @banner nvarchar(200), @totalMonths int = 0
	create table #temp (
		companyId int not null,	
		created datetime null,
		brand nvarchar(150) not null,
		campaign nvarchar(150) not null,
		banner nvarchar(150) not null,
		firstImpression datetime null,
		lastImpression datetime null,
		totalDays int,
		totalImpressions int,
		totalClicks int
	)

	insert into #temp (companyId, created, brand, campaign, banner, firstImpression, lastImpression, totalDays, totalImpressions, totalClicks)
	select a.Id, b.created, pc. companyname, a.CampaignName, a.ImageName, min(c.StartDate), MAX(c.EndDate), 0, 0, 0
	from TIE_Monthly_Totals a
		join companies b on a.Id = b.companies_Id
		join companies pc on pc.companies_Id = b.parent_companies_Id
		left join TIE_Key_Dates c on c.Id=a.Id and c.CampaignName=a.CampaignName and c.ImageName=a.ImageName
	where a.Id = @cid
	group by a.Id, b.created, pc. companyname, a.CampaignName, a.ImageName
	order by a.Id, a.CampaignName, a.ImageName

	select @counter =0, @totalMonths = DATEDIFF(m, MIN(a.StartDate), GETDATE()) from TIE_Key_Dates a
	while @counter <= @totalMonths
	begin
		select @m = MONTH(DATEADD(MM,@counter*-1,CURRENT_TIMESTAMP)), @y = YEAR(DATEADD(MM,@counter*-1,CURRENT_TIMESTAMP))	
		--select @dateCol = right('0' + cast(@m as varchar(4)), 2)+'-'+cast(@y as varchar(4))
		select @dateCol = DATENAME(month, cast (cast(@y as varchar(4)) + '-' + cast(@m as varchar(4)) + '-01' as datetime)) + ' ' + cast(@y as varchar(4))
		print 'month: '+cast(@m as varchar(2)) + ', year: '+cast(@y as varchar(4)) + ', col: ' + @dateCol
		
		execute('alter table #temp add ['+@dateCol+' Impressions] [int] alter table #temp add ['+@dateCol+' Clicks] [int]')	
		
		execute(N'
			update #temp set ['+@dateCol+' Impressions] = a.Showed, ['+@dateCol+' Clicks] = a.Clicked, totalImpressions = totalImpressions + a.Showed, 
				totalClicks = totalClicks + a.Clicked , totalDays = tkd.Days
			from TIE_Monthly_Totals a			
				join TIE_Key_Dates tkd on tkd.Id = a.Id
					and tkd.CampaignName = a.CampaignName --collate Latin1_General_CI_AS
					and tkd.ImageName = a.ImageName --collate Latin1_General_CI_AS				
				join #temp b on b.companyId = a.Id 
					and b.campaign = a.CampaignName collate Latin1_General_CI_AS
					and b.banner = a.ImageName collate Latin1_General_CI_AS			
			where YEAR(a.ReportDate) = '+@y+' and MONTH(a.ReportDate) = '+@m)
		
		exec('update #temp set ['+@dateCol+' Impressions] = 0 where ['+@dateCol+' Impressions] is null')
		exec('update #temp set ['+@dateCol+' Clicks] = 0 where ['+@dateCol+' Clicks] is null')	
		
		select @counter = @counter + 1
	end

	select * from TIE_Key_Dates a where a.Id=@cid
	select * from TIE_Monthly_Totals a where a.Id=@cid

	select * from #temp
	if OBJECT_ID('tempdb..#temp') is not null drop table #temp
END

GO


