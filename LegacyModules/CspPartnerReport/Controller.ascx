﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Controller.ascx.cs" Inherits="TIEKinetix.CSPModules.CspPartnerReport.Controller" %>
<asp:Panel ID="PartnerReportContainer" Visible="false" runat="server">
<script type="text/javascript" language="javascript">

    $(document).keydown(function(e) {
        if (e.keyCode == 13) {
            return false;
        }
    });
   
    dojo.registerModulePath("partnerReport", "<%=ControlPath %>js");
    dojo.require("partnerReport.app");
    dojo.require("dojox.grid.EnhancedGrid");
    dojo.require("dojox.charting.Chart");
    dojo.require("dojox.charting.axis2d.Default");
    dojo.require("dojox.charting.plot2d.Lines");
    dojo.require("dojox.charting.widget.Legend");
    
    dojo.ready(function() {
        var layout = [{
            defaultCell: { editable: false, type: dojox.grid.cells._Widget },
            cells: [<%=CellLayout %>]
            }];
            partnerReport.app.init(layout, "<%=DotNetNuke.Common.Globals.NavigateURL(TabId) %>", "appLayout");
        });

    (function() {
        var wrapper = dojo.create('div', {
            id: 'loadingOverlay',
            'class': 'pageOverlay',
            innerHTML: '<div class="loadingMessage"><img src="<%=ResolveUrl("~/js/external/dojo.1.6/dijit/themes/claro/images/loadingAnimation.gif")%>" />Loading</div>'
        }, dojo.body());
    })();    
    
</script>
<div style="width:100%;">
    <div>
                <button id="rlExport" dojoType="dijit.form.Button" iconClass="dijitEditorIcon dijitEditorIconWikiword" showLabel="true" type="button">Export
                    <script type="dojo/connect" data-dojo-event="onClick">
                    window.location = "?fetch=download";
                    </script>
                </button>
    </div>
    <div id="gridContainer" ></div>    
</div>
<%if (ChartData.Rows.Count > 0)
  {%> 
<div class="csp-chart-container">
    <div id="legend"></div>
    <div id="chart"></div>
</div>
<div class="csp-clear"></div>
<script language="javascript" type="text/javascript">
    var chart1 = new dojox.charting.Chart("chart");
    chart1.addPlot("default", { type: "Lines" });
    chart1.addAxis("x", {
        labels: <%=GetChartLabels() %>
    });
    chart1.addAxis("y", { vertical: true });
    <%=GetChartSeries() %>
    chart1.render();
    var c = new dojox.charting.widget.Legend({chart:chart1, horizontal: true, columns: 3}, "legend");
</script>
<%} %>
</asp:Panel>
<asp:Panel ID="ErrorMessageContainer" Visible="false" runat="server">
<span class="csp-error"><%=GetLocalizationText("ErrorMessage") %></span>
</asp:Panel>