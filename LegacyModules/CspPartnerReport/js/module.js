﻿dojo.provide("partnerReport.module");

// dependencies for the application UI and data layers
dojo.require("dijit.form.Button");
dojo.require("dojox.image.LightboxNano");
dojo.require("dojox.grid.EnhancedGrid");
dojo.require("dojox.grid.enhanced.plugins.Filter");
dojo.require("dojo.data.ItemFileWriteStore");
dojo.require("dojox.grid.enhanced.plugins.Pagination");
dojo.require("dojox.widget.Standby");
