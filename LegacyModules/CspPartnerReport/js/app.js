﻿dojo.provide("partnerReport.app");
partnerReport.app = {
    companyGrid: null,
    contactGrid: null,
    consumerGrid: null,
    layout: null,
    contactLayout: null,
    url: "",
    currentTab: "generalTab",
    isContactGridLoaded: false,
    isConsumerDomainGridLoaded: false,
    selectedCompany: null,
    clientId: "",
    parseOnLoad: (function(config) {
        var parseOnLoad = config.parseOnLoad;
        config.parseOnLoad = false;
        return parseOnLoad;
    })(dojo.config),

    //function
    init: function(layout, url, clientId) {
        //this.startLoading(clientId);
        dojo.require("partnerReport.module");
        this.layout = layout;
        this.url = url;
        this.clientId = clientId;
        dojo.ready(dojo.hitch(this, 'startup'));
    },
    startup: function() {
        dojo.connect(this, "initUi", this, "endLoading");
        this.initUi();
    },
    initUi: function() {
        var me = this;
        dojo.parser.parse(dojo.byId(this.clientId));
        this.plugins = {
            filter: {
                itemsName: 'items',
                closeFilterbarButton: false,
                ruleCount: 1
            }
            /*,
            pagination: {
            pageSizes: ["25", "50", "100", "250"]
            }*/
        };
        this.store = new dojo.data.ItemFileReadStore({
            url: me.url + "?fetch=report",
            clearOnClose: true
        });
        this.companyGrid = new dojox.grid.EnhancedGrid({ id: "grid", plugins: me.plugins, structure: me.layout, store: me.store, autoHeight: true },
            dojo.byId("gridContainer"));

        this.companyGrid.onSelected = function(index) {
            var item = this.getItem(index);
            me.selectedCompany = item;
        };
        this.companyGrid.startup();
    },
    UpdateSelectedTab: function(tab) {
        var me = this;
        this.currentTab = tab.get("id");
        if (this.currentTab == "contactTab") {
            if (!this.isContactGridLoaded && this.selectedCompany) {
                this.contactGrid.store.url = this.url + "?fetch=contacts&cid=" + this.selectedCompany.Id;
                this.contactGrid.startup();
                this.isContactGridLoaded = true;
            }
            else if (this.selectedCompany) {
                me.UpdateStore("?fetch=contacts&cid=" + me.selectedCompany.Id, me.contactGrid, me.currentTab, true);
            }
        }
        else if (this.currentTab == "consumerTab") {
            if (!this.isConsumerDomainGridLoaded && this.selectedCompany) {
                this.consumerGrid.store.url = this.url + "?fetch=consumers&cid=" + this.selectedCompany.Id;
                this.consumerGrid.startup();
                this.isConsumerDomainGridLoaded = true;
            }
            else if (this.selectedCompany)
                this.UpdateStore("?fetch=consumers&cid=" + this.selectedCompany.Id, this.consumerGrid, this.currentTab, true);
        }
    },
    SaveButtonHandler: function() {
        var me = this;
        if (this.currentTab == "generalTab") {
            var standby = this.GetStandBy("centerPane");
            standby.show();
            var data = {
                Id: dojo.byId("generalId").value,
                Name: dijit.byId("generalCompanyName").get("value"),
                Address1: dijit.byId("generalAddress1").get("value"),
                Address2: dijit.byId("generalAddress2").get("value"),
                Zip: dijit.byId("generalZipcode").get("value"),
                State: dijit.byId("generalState").get("value"),
                City: dijit.byId("generalCity").get("value"),
                Country: dijit.byId("generalCountry").get("value"),
                Website: dijit.byId("generalWebsite").get("value"),
                IsConsumer: dijit.byId("generalConsumer").get("checked"),
                IsSupplier: dijit.byId("generalSupplier").get("checked")
            };
            dojo.xhrPost({
                headers: { "content-type": "application/json; charset=utf-8", "request-type": "PostGeneral" },
                postData: dojo.toJson(data),
                handleAs: "json",
                url: me.url,
                load: function(response) {
                    if (response !== null && response.success) {
                        me.UpdateStore("?fetch=companies", me.companyGrid, "topPane", false);
                    }
                    else if (response !== null && !response.success) {
                        alert(response.error);
                    }
                    standby.hide();
                },
                error: function() {
                    standby.hide();
                }
            });
        }
        else if (this.currentTab == "contactTab" && this.isContactGridLoaded) {
            this.SaveStore(this.contactGrid, "?fetch=contacts&cid=" + this.selectedCompany.Id, false, "PostContacts");
        }
        else if (this.currentTab == "consumerTab" && this.isConsumerDomainGridLoaded) {
            this.SaveStore(this.consumerGrid, "?fetch=consumers&cid=" + this.selectedCompany.Id, false, "PostConsumers");
        }
        else {
        }
    },
    //generic functions
    UpdateStore: function(url, dg, id, flag) {
        //overlay
        var standby = null;
        if (id) {
            standby = this.GetStandBy(id);
            standby.show();
        }
        var me = this;
        var store = null;
        if (flag) {
            store = new dojo.data.ItemFileWriteStore({ url: me.url + url });
            //dg.store.revert();
        }
        else
            store = new dojo.data.ItemFileReadStore({ url: me.url + url });
        dg.store.close();
        store.fetch({
            query: {},
            onComplete: function(items, request) {
                dg.setStore(store);
                if (id) standby.hide();
            }
        });
    },
    SaveStore: function(grid, url, overlayId, requestType) {
        var me = this;
        var store = grid.store;
        store._saveCustom = function(saveCompleteCallback, saveFailedCallback) {
            var modifiedItems = me._getModifiedItems(store);
            dojo.forEach(modifiedItems, function(item, index) {
                for (var key in item) {
                    if (dojo.isArray(item[key]) && item[key].length == 1)
                        item[key] = item[key][0];
                }
                item._S = null;
            });
            //only save if there's data
            if (modifiedItems.length > 0) {
                var standby = me.GetStandBy(me.currentTab);
                standby.show();
                dojo.xhrPost({
                    headers: { "content-type": "application/json; charset=utf-8", "request-type": requestType },
                    postData: dojo.toJson(modifiedItems),
                    handleAs: "json",
                    url: me.url,
                    load: function(response) {
                        if (response !== null && response.success) {
                            saveCompleteCallback();
                            me.UpdateStore(url, grid, me.currentTab, true);
                        }
                        else {
                        }
                        standby.hide();
                    },
                    error: function() {
                        saveFailedCallback();
                        standby.hide();
                    }
                });
            }
        };
        store.save();
    },
    startLoading: function(targetNode) {
        var overlayNode = dojo.byId('loadingOverlay');
        if ("none" == dojo.style(overlayNode, "display")) {
            var coords = dojo.coords(targetNode || dojo.body());
            dojo.marginBox(overlayNode, coords);

            dojo.style(dojo.byId('loadingOverlay'), {
                display: 'block',
                opacity: 1
            });
        }
    },
    endLoading: function() {
        dojo.fadeOut({
            node: dojo.byId('loadingOverlay'),
            onEnd: function(node) {
                dojo.style(node, 'display', 'none');
            }
        }).play();
    },
    GetStandBy: function(id) {
        var standby = dijit.byId(id + "_standby");
        if (standby == null) {
            standby = new dojox.widget.Standby({
                id: id + "_standby",
                target: id,
                color: "#ccc",
                zIndex: 999
            });
            document.body.appendChild(standby.domNode);
        }
        return standby;
    },
    _getModifiedItems: function(store) {
        var modifiedItems = [];
        if (store !== null && store._pending !== null) {
            if (store._pending._modifiedItems !== null) {
                for (var modifiedItemKey in store._pending._modifiedItems) {
                    if (store._itemsByIdentity) {
                        modifiedItems.push(store._itemsByIdentity[modifiedItemKey]);
                    }
                    else {
                        modifiedItems.push(store._arrayOfAllItems[modifiedItemKey]);
                    }
                }
            }
            if (store._pending._newItems !== null) {
                for (var modifiedItemKey in store._pending._newItems) {
                    if (store._itemsByIdentity) {
                        modifiedItems.push(store._itemsByIdentity[modifiedItemKey]);
                    }
                    else {
                        modifiedItems.push(store._arrayOfAllItems[modifiedItemKey]);
                    }
                }
            }
        }
        return modifiedItems;
    }
};