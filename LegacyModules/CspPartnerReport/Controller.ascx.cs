﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DesktopModules.CSPModules.CspCommons;
using DesktopModules.CSPModules.CspCommons.Components;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Localization;

namespace TIEKinetix.CSPModules.CspPartnerReport
{
    public partial class Controller : PortalModuleBase
    {
        protected PartnerReport PartnerReport;
        protected PartnerReportChartData ChartData;
        private int _companyId;
        private bool _isModuleValid = false;

        protected void Page_Init(object sender, EventArgs e)
        {
            _isModuleValid = true;
            //module title
            if (!string.IsNullOrEmpty(Localization.GetString("ModuleTitle", LocalResourceFile)))
                ModuleContext.Configuration.ModuleTitle = Localization.GetString("ModuleTitle", LocalResourceFile);
            ConnectionString = Config.GetConnectionString("CspPortal" + PortalId);
            PartnerReport = new PartnerReport();
            //get integration key
            if (!int.TryParse(UserInfo.Profile.GetPropertyValue(Globals.IntegrationKey), out _companyId))
            {
                _isModuleValid = false;
                return;
            }

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted);
                using (SqlCommand cmd = new SqlCommand("", connection, transaction))
                {
                    cmd.Parameters.Clear();
                    cmd.ResetCommandTimeout();
                    cmd.Parameters.AddWithValue("@cid", _companyId);
                    #region Get Partner Data
                    cmd.CommandText = Globals.GenerateReportQuery;
                    Log("Execute Query");
                    try
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader != null)
                            {
                                DataTable dt = reader.GetSchemaTable();
                                CellLayout = "";
                                Log("Generate layout: " +dt.Rows.Count);
                                foreach (DataRow row in dt.Rows)
                                {
                                    if (row["ColumnName"].Equals("Company Id")) continue;
                                
                                    CellLayout += ",{" + string.Format("field: \"{0}\", name: \"{0}\"", row["ColumnName"]) + "}";
                                }
                                Log("Cell Layout: " + CellLayout);
                                if (!string.IsNullOrEmpty(CellLayout))
                                    CellLayout = CellLayout.Remove(0, 1);

                                while (reader.Read())
                                {
                                    PartnerReportRecord record = new PartnerReportRecord();
                                    foreach (DataRow row in dt.Rows)
                                    {
                                        record.Add(row["ColumnName"].ToString(), reader[row["ColumnName"].ToString()]);
                                    }
                                    PartnerReport.Add(record);
                                }
                            }
                            else
                                Log("Reader is null");

                        } // end of reader
                    }
                    catch(Exception ex)
                    {
                        Log(ex.Message);
                        Log(ex.StackTrace);
                        throw ex;
                    }
                    #endregion
                    #region Get Report Data
                    cmd.CommandText = Globals.GetReportDataQuery;
                    try
                    {
                        Log("Get Report Data");
                        ChartData = new PartnerReportChartData();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader != null)
                            {
                                DataTable dt = reader.GetSchemaTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    if (row["ColumnName"].Equals("CampaignName")) continue;
                                    ChartData.AddColumn(row["ColumnName"].ToString());
                                }

                                while (reader.Read())
                                {
                                    PartnerReportChartDataRow chartDataRow = new PartnerReportChartDataRow();
                                    foreach (DataRow row in dt.Rows)
                                    {
                                        if (row["ColumnName"].Equals("CampaignName"))
                                        {
                                            chartDataRow.CampaignName = reader[row["ColumnName"].ToString()].ToString();
                                            continue;
                                        }
                                        if (reader[row["ColumnName"].ToString()] != DBNull.Value)
                                            chartDataRow.AddData(reader[row["ColumnName"].ToString()]);
                                        else
                                            chartDataRow.AddData(0);
                                    }
                                    ChartData.Add(chartDataRow);
                                }
                            }
                        } // end of reader
                    }
                    catch (Exception ex)
                    {
                        Log("Error: " + ex.Message);
                        Log("StackTrace: " +ex.StackTrace);
                        //throw ex;
                    }

                    #endregion
                }
                transaction.Commit();
            } // end of connection
        }

        protected string ConnectionString { get; set; }
        protected string CellLayout { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (_isModuleValid)
                {
                    if (Request.IsAuthenticated && Request.HttpMethod.Equals("GET") && Request.QueryString.Get("fetch") != null)
                    {
                        ProcessFetchRequest(Request.QueryString.Get("fetch"));
                        return;
                    }
                    PageClientInitialize(PartnerReportContainer);
                }
                else
                    PageClientInitialize(ErrorMessageContainer);
            }
        }

        private void ProcessFetchRequest(string fetchType)
        {
            if (fetchType == "report")
            {
                Response.Write(PartnerReport.ToJson());
            }
            else if (fetchType == "download")
            {
                Response.ContentType = "application/vnd.csv; charset=utf-8";
                Response.AddHeader("content-disposition", "attachment;filename=PartnerReport_" + DateTime.Now.ToString("MMddyy_hhmmss") + ".csv");
                Response.Write(PartnerReport.ToCsv());
            }
            Response.End();
        }

        private void PageClientInitialize(Panel container)
        {
            Control docType = this.Page.FindControl("skinDocType");
            if (docType != null)
                ((Literal)docType).Text = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";

            LoadCss(ResolveUrl("~/js/external/dojo.1.6/dijit/themes/claro/claro.css"));
            LoadCss(ResolveUrl("~/js/external/dojo.1.6/dojox/grid/enhanced/resources/claro/EnhancedGrid.css"));
            //LoadCss(ControlPath + "module.css");
            Page.ClientScript.RegisterClientScriptInclude("dojo.1.6", ResolveUrl("~/js/external/dojo.1.6/dojo/dojo.js.uncompressed.js"));
            var body = (HtmlGenericControl)Page.FindControl("body");
            if (body != null)
                body.Attributes.Add("class", "claro");
            container.Enabled = container.Visible = true;

        }
        private void LoadCss(string name)
        {
            HtmlLink link = new HtmlLink();
            link.Href = ResolveUrl(name);
            link.Attributes.Add("type", "text/css");
            link.Attributes.Add("rel", "stylesheet");
            Page.Header.Controls.Add(link);
        }
        public static void Log(string message)
        {
#if DEBUG
            try
            {
                StreamWriter sw = new StreamWriter("C:\\Tmp\\DnnLog.txt", true);
                sw.WriteLine(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss") + ": " + message);
                sw.Close();
            }catch
            {
            }
#endif
        }

        protected string GetLocalizationText(string message)
        {
            return Localization.GetString("ErrorMessage", LocalResourceFile);
        }

        protected string GetChartLabels()
        {
            string label = "[";
            try
            {
                for (int i = 0; i < ChartData.ColumnNames.Count; i++)
                {
                    if (i > 0) label += ",";
                    label += "{value: " + (i + 1) + ", text: \"" + ChartData.ColumnNames[i] + "\"}";
                }
            }
            catch(Exception ex)
            {
                Log(ex.Message);
                Log(ex.StackTrace);
            }
            return label + "]";
        }

        protected string GetChartSeries()
        {
            string script = "\n";
            try
            {
                string[] colors = new string[] {"blue", "red", "yellow", "black", "green", "brown", "gold","darkblue", "darkred", "darkgreen"};
                for (int i = 0; i < ChartData.Rows.Count; i++)
                {
                    script += "chart1.addSeries('" + ChartData.Rows[i].CampaignName + "', [";
                    for (int j = 0; j < ChartData.Rows[i].ColumnData.Count; j++)
                    {
                        if (j > 0) script += ",";
                        script += ChartData.Rows[i].ColumnData[j];
                    }
                    script += "], {plot:\"other\", stroke: {color:\"" + colors[i] + "\"}});\n";
                }
            }
            catch(Exception ex)
            {
                Log(ex.Message);
                Log(ex.StackTrace);
            }
            return script;
        }
    }
}